import re
import sys, getopt



def main(argv):
	f = open("/etc/hosts", 'r')
	fout = open("machines", 'w')
	procs = 1
	if len(argv) > 0:
		procs = argv[0]
	
	ignoreFirt = True
	for line in f:
		if ignoreFirt:
			ignoreFirt = False
		else:
			for m in re.findall(r'[0-9]+.[0-9]+.[0-9]+.[0-9]+ .*', line):
				fout.write(m[m.index(' '):].strip() + ":" + procs + "\n")

if __name__ == "__main__":
    main(sys.argv[1:])
