/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html
*/
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.xindice.client.xmldb.services.CollectionManager;
import org.apache.xindice.xml.dom.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.BinaryResource;
import org.xmldb.api.modules.XMLResource;

public class DBUploader {

    static String port = "8085";
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        Collection col = null;
        try {
            String driver = "org.apache.xindice.client.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);
            
            File dir = new File("db");  
            String[] collections = dir.list();
            for (int i = 0; i < collections.length; i++) {
                createDB("db/" + collections[i]);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try { 
                if (col != null) {
                    col.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    /** 
     * Return the node element for the actual collection following the SchemaStructure XSD. 
     *
     * @param coll          The base collection
     * @param doc           The DOM document base for the xml
     * @return              It returns the root node for the collection following the
     *                      SchemaStructure XSD 
     */
    static void createDB(String coll) {
        try {
            String driver = "org.apache.xindice.client.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);
            System.out.println("Entro en createDB");
            //Element name
            File dir = new File(coll);
            //Create collection if it does not exist
            createCollection(coll);
            
            //List files and directories
            File[] listOfFiles = dir.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
            	System.out.println(listOfFiles[i].toString() + " " + listOfFiles[i].isFile() + " " + listOfFiles[i].isDirectory());
                //Files
              if (listOfFiles[i].isFile()) {
                  String name = listOfFiles[i].toString();
                  System.out.println("ends " + name.endsWith(".xml"));
                  if (name.endsWith(".xml")) {
                      addFile(fileToString(name), listOfFiles[i].toString());
                  }
                  else {
                      if (!name.endsWith("~")) {
                          addX3D(name.substring(name.lastIndexOf("/") + 1), fileToString(name));
                      }
                  }
              }
              //Collections
              else if (listOfFiles[i].isDirectory()) {
                  createDB(listOfFiles[i].toString());
              }
            }
           
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    
    static String addFile(String xmlDoc, String baseCollection) {
        Collection col = null;
        try {
            System.out.println("Voy a añadir file ");
        	
            //Getting the id and collection info
            Document doc = stringToDom(xmlDoc);
            Element root = doc.getDocumentElement();
            removeWhitespaceNodes(root);
        
            String id = ((Element) root.getFirstChild()).getElementsByTagName("mms:id").item(0).getTextContent();
            
            System.out.println(id);
            String collection = baseCollection.substring(baseCollection.indexOf("/") + 1, baseCollection.lastIndexOf("/"));
  
            //Configuring connection to DB
            String driver = "org.apache.xindice.client.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection("xmldb:xindice://localhost:" + port + "/db/" + collection);
System.out.println(collection + " " + col);
            //Setting the xml resource
            XMLResource model = (XMLResource) col.createResource(id, XMLResource.RESOURCE_TYPE);
            
            System.out.println("1");
            
            model.setContent(xmlDoc);
            System.out.println("2");
            col.storeResource(model);
            System.out.println("3");
            
            return id;
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
    
    static String addX3D(String fileName, String xmlDoc) {
        Collection col = null;
        try {
            //Getting the id and collection info
            Document doc = stringToDom(xmlDoc);
            Element root = doc.getDocumentElement();
            removeWhitespaceNodes(root);
        
            //Configuring connection to DB
            String driver = "org.apache.xindice.client.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);

            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);

            col = DatabaseManager.getCollection("xmldb:xindice://localhost:" + port + "/db/x3d");

            //Setting the binary resource
            BinaryResource model = (BinaryResource) col.createResource(fileName, "BinaryResource");
            model.setContent(xmlDoc.getBytes());
            col.storeResource(model);
            
            return fileName;
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        finally {
            try {
                if (col != null) {
                    col.close();
                }
            }
            catch (XMLDBException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
    
    static void createCollection(String collection) {
        try {
            String driver = "org.apache.xindice.client.xmldb.DatabaseImpl";
            Class < ? > c = Class.forName(driver);
            Database database = (Database) c.newInstance();
            DatabaseManager.registerDatabase(database);
            //Remove the previous db
            String colString = collection.substring(collection.indexOf("/") + 1);
            String uri = "xmldb:xindice://localhost:" + port + "/db/";
            
            System.out.println("Voy a crear " + collection);
            
            
            String[] collSplit = colString.split("/");
            Collection col = DatabaseManager.getCollection(uri);
            //Creation of the complete branch of the collection
            //It checks all parent collections, if they do not exist it creates them
            for (int i = 0; i < collSplit.length; i++) {
                System.out.println("uri " + uri);
                col = DatabaseManager.getCollection(uri);
                //Checking if the database already exists
                if (col.getChildCollection(collSplit[i]) == null) {
                    CollectionManager service = (CollectionManager) col.getService("CollectionManager", "1.0");
            
                    // Build up the Collection XML configuration.
                    String collectionConfig =
                        "<collection compressed=\"true\" "
                        + "            name=\"" + collSplit[i] + "\" inline-metadata=\"true\">"
                        + "   <filer class=\"org.apache.xindice.core.filer.BTreeFiler\"/>"
                        + "</collection>";
            
                    System.out.println("creo colección " + collSplit[i] + " en uri " + uri);
                    service.createCollection(collSplit[i], DOMParser.toDocument(collectionConfig));
                }
                col.close();
                uri = uri + collSplit[i] + "/";
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws DDSException     DDS00X - External error
     */
    public static String indent(String document) {
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(4);
            Writer output = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);

            return output.toString();
        }
        catch (Throwable e) {
            e.printStackTrace();
            return "";
        }
    }
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     */
    static Document stringToDom(String str) {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            docFactory.setNamespaceAware(true);
            docFactory.setIgnoringElementContentWhitespace(true);
            docFactory.setIgnoringComments(true);
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
            return docBuilder.parse(is);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    /**
     * Converts a Dom to string.
     * 
     * @param doc               The dom document
     * @return                  The String
     */
    static String domToString(Document doc) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    static String fileToString(String path) {
        try{
        	return FileUtils.readFileToString(new File(path));
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    static String fileToString2(String path) {
        try{
            BufferedReader in = new BufferedReader(new FileReader(path));
            String result = "";
            String str;
            while ((str = in.readLine()) != null) {
                result = result + str + System.getProperty("line.separator");
            }
            in.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
}
