% Data files
datafile_Nw = '/home/mtrias/Feina_Investigacio/IAC3/Papers/Nozzle/outputDir_Newtonian_res02x/dumps.visit';
datafile_CY = '/home/mtrias/Feina_Investigacio/IAC3/Papers/Nozzle/outputDir_NonNewtonian_CY_res02x/dumps.visit';
datafile_Cs = '/home/mtrias/Feina_Investigacio/IAC3/Papers/Nozzle/outputDir_NonNewtonian_Casson_res02x/dumps.visit';

xStart = [0.000 0.020 0.030 0.045];
xEnd   = [0.121 0.140 0.180 0.220 0.300];

%% Stationary
% Computations
jobNw = batch('[NIH_Arora_avg_Nw, NIH_Garon_avg_Nw, NIH_Garon_LIN_avg_Nw, NIH_Trias_avg_Nw, NIH_Arora_Nw, NIH_Garon_Nw, NIH_Trias_Nw, DHbHb_Arora_Nw, DHbHb_Garon_Nw, DHbHb_Trias_Nw] = Hemolysis_stationary(100, datafile_Nw, xStart, xEnd);');
jobCY = batch('[NIH_Arora_avg_CY, NIH_Garon_avg_CY, NIH_Garon_LIN_avg_CY, NIH_Trias_avg_CY, NIH_Arora_CY, NIH_Garon_CY, NIH_Trias_CY, DHbHb_Arora_CY, DHbHb_Garon_CY, DHbHb_Trias_CY] = Hemolysis_stationary(100, datafile_CY, xStart, xEnd);');
jobCs = batch('[NIH_Arora_avg_Cs, NIH_Garon_avg_Cs, NIH_Garon_LIN_avg_Cs, NIH_Trias_avg_Cs, NIH_Arora_Cs, NIH_Garon_Cs, NIH_Trias_Cs, DHbHb_Arora_Cs, DHbHb_Garon_Cs, DHbHb_Trias_Cs] = Hemolysis_stationary(100, datafile_Cs, xStart, xEnd);');
%
wait(jobNw);
load(jobNw);
%
wait(jobCY);
load(jobCY);
%
wait(jobCs);
load(jobCs);


% %% Transients
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_01, NIH_Garon_avg_Nw_01, NIH_Garon_LIN_avg_Nw_01, NIH_Arora_Nw_01, NIH_Garon_Nw_01, DHbHb_Arora_Nw_01, DHbHb_Garon_Nw_01] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 0.05, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_01, NIH_Garon_avg_CY_01, NIH_Garon_LIN_avg_CY_01, NIH_Arora_CY_01, NIH_Garon_CY_01, DHbHb_Arora_CY_01, DHbHb_Garon_CY_01] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 0.05, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_01, NIH_Garon_avg_Cs_01, NIH_Garon_LIN_avg_Cs_01, NIH_Arora_Cs_01, NIH_Garon_Cs_01, DHbHb_Arora_Cs_01, DHbHb_Garon_Cs_01] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 0.05, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
% 
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_02, NIH_Garon_avg_Nw_02, NIH_Garon_LIN_avg_Nw_02, NIH_Arora_Nw_02, NIH_Garon_Nw_02, DHbHb_Arora_Nw_02, DHbHb_Garon_Nw_02] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 0.1, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_02, NIH_Garon_avg_CY_02, NIH_Garon_LIN_avg_CY_02, NIH_Arora_CY_02, NIH_Garon_CY_02, DHbHb_Arora_CY_02, DHbHb_Garon_CY_02] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 0.1, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_02, NIH_Garon_avg_Cs_02, NIH_Garon_LIN_avg_Cs_02, NIH_Arora_Cs_02, NIH_Garon_Cs_02, DHbHb_Arora_Cs_02, DHbHb_Garon_Cs_02] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 0.1, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
% 
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_03, NIH_Garon_avg_Nw_03, NIH_Garon_LIN_avg_Nw_03, NIH_Arora_Nw_03, NIH_Garon_Nw_03, DHbHb_Arora_Nw_03, DHbHb_Garon_Nw_03] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 0.25, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_03, NIH_Garon_avg_CY_03, NIH_Garon_LIN_avg_CY_03, NIH_Arora_CY_03, NIH_Garon_CY_03, DHbHb_Arora_CY_03, DHbHb_Garon_CY_03] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 0.25, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_03, NIH_Garon_avg_Cs_03, NIH_Garon_LIN_avg_Cs_03, NIH_Arora_Cs_03, NIH_Garon_Cs_03, DHbHb_Arora_Cs_03, DHbHb_Garon_Cs_03] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 0.25, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
% 
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_04, NIH_Garon_avg_Nw_04, NIH_Garon_LIN_avg_Nw_04, NIH_Arora_Nw_04, NIH_Garon_Nw_04, DHbHb_Arora_Nw_04, DHbHb_Garon_Nw_04] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 0.5, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_04, NIH_Garon_avg_CY_04, NIH_Garon_LIN_avg_CY_04, NIH_Arora_CY_04, NIH_Garon_CY_04, DHbHb_Arora_CY_04, DHbHb_Garon_CY_04] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 0.5, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_04, NIH_Garon_avg_Cs_04, NIH_Garon_LIN_avg_Cs_04, NIH_Arora_Cs_04, NIH_Garon_Cs_04, DHbHb_Arora_Cs_04, DHbHb_Garon_Cs_04] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 0.5, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
% 
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_05, NIH_Garon_avg_Nw_05, NIH_Garon_LIN_avg_Nw_05, NIH_Arora_Nw_05, NIH_Garon_Nw_05, DHbHb_Arora_Nw_05, DHbHb_Garon_Nw_05] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 0.75, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_05, NIH_Garon_avg_CY_05, NIH_Garon_LIN_avg_CY_05, NIH_Arora_CY_05, NIH_Garon_CY_05, DHbHb_Arora_CY_05, DHbHb_Garon_CY_05] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 0.75, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_05, NIH_Garon_avg_Cs_05, NIH_Garon_LIN_avg_Cs_05, NIH_Arora_Cs_05, NIH_Garon_Cs_05, DHbHb_Arora_Cs_05, DHbHb_Garon_Cs_05] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 0.75, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
% 
% % Computations
% jobNw = batch('[NIH_Arora_avg_Nw_06, NIH_Garon_avg_Nw_06, NIH_Garon_LIN_avg_Nw_06, NIH_Arora_Nw_06, NIH_Garon_Nw_06, DHbHb_Arora_Nw_06, DHbHb_Garon_Nw_06] = Hemolysis_transient(100, datafile_Nw, xStart, xEnd, 1, 1e9);');
% jobCY = batch('[NIH_Arora_avg_CY_06, NIH_Garon_avg_CY_06, NIH_Garon_LIN_avg_CY_06, NIH_Arora_CY_06, NIH_Garon_CY_06, DHbHb_Arora_CY_06, DHbHb_Garon_CY_06] = Hemolysis_transient(100, datafile_CY, xStart, xEnd, 1, 1e9);');
% jobCs = batch('[NIH_Arora_avg_Cs_06, NIH_Garon_avg_Cs_06, NIH_Garon_LIN_avg_Cs_06, NIH_Arora_Cs_06, NIH_Garon_Cs_06, DHbHb_Arora_Cs_06, DHbHb_Garon_Cs_06] = Hemolysis_transient(100, datafile_Cs, xStart, xEnd, 1, 1e9);');
% %
% wait(jobNw);
% load(jobNw);
% %
% wait(jobCY);
% load(jobCY);
% %
% wait(jobCs);
% load(jobCs);
