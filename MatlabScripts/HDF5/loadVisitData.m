function [output, dump_info] = loadVisitData(input, timeStep)

% loadVisitData (07/02/2012 by M.Trias, IAC3)
%
% It loads HDF5 VisIt data over the whole spatial domain, at a particular
% timeStep chosen by the user. In case 'dump_info' was not available, it
% also loads it for future usage.
%
% INPUT:  input     -- it can be either a 'char' string providing the path
%                     to 'dumps.visit' file, or directly the 'structure'
%                     with all dump information from the data set
%                     [ultimately, this data structure must be gotten from
%                     a past call of this same function or a call to
%                     'loadVisitDumpInfo' function].
%         timeStep  -- output time step number of the required data (>= 1)
%
% OUTPUT: output    -- data structure with all the required information at
%                     the particular output time step (timeStep)
%         dump_info -- data structure with dump information of the whole
%                     data set obtained from a call to 'loadVisitDumpInfo'
%                     function
%
% -------------------------------------------------------------------------

if (timeStep < 1)
    error('The second argument of ''loadVisitData'' function (timeStep) must be >= 1');
end

if (ischar(input))
    [dump_info] = loadVisitDumpInfo(input);
elseif (isstruct(input))
    dump_info = input;
else
    error('The first argument of ''loadVisitData'' function (input) must be either a ''char'' or a ''structure''');
end

% Read some basic data in this particular timeStep
nProcessors  = h5read(dump_info.fileList{timeStep},'/BASIC_INFO/number_processors');
nPatches  = h5read(dump_info.fileList{timeStep},'/BASIC_INFO/number_patches_at_level');
patchExtents = h5read(dump_info.fileList{timeStep},'/extents/patch_extents');
patchMap = h5read(dump_info.fileList{timeStep},'/extents/patch_map');

output.timeStepOutput = timeStep;
output.timeStepSim    = h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time_step_number');
output.time           = h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time');
if (timeStep > 1)
    output.dtOutput   =   h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time') - h5read(dump_info.fileList{timeStep-1},'/BASIC_INFO/time');
    output.dtSim      = ( h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time') - h5read(dump_info.fileList{timeStep-1},'/BASIC_INFO/time') ) / double( h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time_step_number') - h5read(dump_info.fileList{timeStep-1},'/BASIC_INFO/time_step_number') );
else
    output.dtOutput   =   h5read(dump_info.fileList{timeStep+1},'/BASIC_INFO/time') - h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time');
    output.dtSim      = ( h5read(dump_info.fileList{timeStep+1},'/BASIC_INFO/time') - h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time') ) / double( h5read(dump_info.fileList{timeStep+1},'/BASIC_INFO/time_step_number') - h5read(dump_info.fileList{timeStep},'/BASIC_INFO/time_step_number') );
end
output.dxVec          = h5read(dump_info.fileList{timeStep},'/BASIC_INFO/dx');

% Load each variable in this particular time step
for iPatch = 0:nPatches-1
    iProc = patchMap.processor_number(iPatch+1)
    sizeX = patchExtents.upper(1,iPatch+1) - patchExtents.lower(1,iPatch+1) + 1;
    sizeY = patchExtents.upper(2,iPatch+1) - patchExtents.lower(2,iPatch+1) + 1;
    sizeZ = patchExtents.upper(3,iPatch+1) - patchExtents.lower(3,iPatch+1) + 1;
    %
    rangeX = patchExtents.lower(1,iPatch+1)+1:patchExtents.upper(1,iPatch+1)+1;
    rangeY = patchExtents.lower(2,iPatch+1)+1:patchExtents.upper(2,iPatch+1)+1;
    rangeZ = patchExtents.lower(3,iPatch+1)+1:patchExtents.upper(3,iPatch+1)+1;
    %
    iPatchStr = int2strFixed(iPatch,5);
    iProcStr = int2strFixed(iProc,5);
    %
    for iZ = rangeZ(1):rangeZ(end)
        output.xGrid(rangeX,rangeY,iZ) = linspace(patchExtents.xlo(1,iPatch+1), patchExtents.xup(1,iPatch+1)-output.dxVec(1), sizeX)' * ones(1,sizeY);
        output.yGrid(rangeX,rangeY,iZ) = ones(sizeX,1) * linspace(patchExtents.xlo(2,iPatch+1), patchExtents.xup(2,iPatch+1)-output.dxVec(2), sizeY);
        if (sizeZ > 1)
            output.zGrid(rangeX,rangeY,iZ) = ones(sizeX,sizeY) * interp1([double(rangeZ(1)) double(rangeZ(end))],[patchExtents.xlo(3,iPatch+1) patchExtents.xup(3,iPatch+1)-output.dxVec(3)], iZ);
        else
            output.zGrid(rangeX,rangeY,iZ) = ones(sizeX,sizeY) * patchExtents.xlo(3,iProc+1);
        end
    end
    %
    for iVar = 1:dump_info.nVars
        loadCmd = sprintf('output.%s(rangeX,rangeY,rangeZ) = double(reshape(h5read([dump_info.dirList{%d} ''processor_cluster.%s.samrai''],''/processor.%s/level.00000/patch.%s/%s''),sizeX,sizeY,sizeZ));',dump_info.varNames{iVar},timeStep,iProcStr,iProcStr,iPatchStr,dump_info.varNames{iVar});
        eval(loadCmd);
    end
end