function [dump_info] = loadVisitDumpInfo(input_file)

% loadVisitDumpInfo (07/02/2012 by M.Trias, IAC3)
%
% It loads dump information of the whole data set found in the 'input_file'
% file path. Such information includes 'number of time steps', 'initial and
% final times', 'dt', 'dx', 'number and name of variables', ...
%
% INPUT:  input_file -- path of the 'dumps.visit' file
%
% OUTPUT: dump_info  -- data structure with dump information of the whole
%                     data set
%
% -------------------------------------------------------------------------

dump_info.workingDir = input_file(1:end-11);

% Open the 'input_file' file
dumps = fopen(input_file);

% Get the file list
aux = textscan(dumps,'%s');
fileListAux = aux{1}; clear aux;
for iaux = 1:size(fileListAux,1)
    fileListAux{iaux} = [dump_info.workingDir fileListAux{iaux}];
end

% Start writing output information
dump_info.iniTime          = h5read(fileListAux{1},'/BASIC_INFO/time');
dump_info.endTime          = h5read(fileListAux{end},'/BASIC_INFO/time');

dump_info.fileList         = fileListAux;
dump_info.dirList          = cell(size(dump_info.fileList));
dump_info.nTimeStepsOutput = size(dump_info.fileList,1);
dump_info.dtOutput_t0      =   h5read(dump_info.fileList{2},'/BASIC_INFO/time') - h5read(dump_info.fileList{1},'/BASIC_INFO/time');
dump_info.nTimeStepsSim    = h5read(dump_info.fileList{end},'/BASIC_INFO/time_step_number');
dump_info.dtSim_t0         = ( h5read(dump_info.fileList{2},'/BASIC_INFO/time') - h5read(dump_info.fileList{1},'/BASIC_INFO/time') ) / double( h5read(dump_info.fileList{2},'/BASIC_INFO/time_step_number') - h5read(dump_info.fileList{1},'/BASIC_INFO/time_step_number') );

for iaux = 1:dump_info.nTimeStepsOutput
    dump_info.dirList{iaux} = dump_info.fileList{iaux}(1:end-14);
end
    
% Read basic data
dump_info.nVars    = h5read(dump_info.fileList{1},'/BASIC_INFO/number_visit_variables');
dump_info.varNames = deblank(h5read(dump_info.fileList{1},'/BASIC_INFO/var_names'));
if (size(dump_info.varNames,1) ~= dump_info.nVars)
    warning('IAC3:HDF5:loadVisitDumpInfo','size(varNames) is different from nVars');
end

dump_info.nProcessors_t0  = h5read(dump_info.fileList{1},'/BASIC_INFO/number_processors');
dump_info.patchExtents_t0 = h5read(dump_info.fileList{1},'/extents/patch_extents');

dxVec                     = h5read(dump_info.fileList{1},'/BASIC_INFO/dx');
dump_info.dx_t0           = dxVec(1);
dump_info.dy_t0           = dxVec(2);
dump_info.dz_t0           = dxVec(3);

