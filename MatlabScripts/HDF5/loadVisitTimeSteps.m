function [timeValues, dump_info] = loadVisitTimeSteps(input)

% loadVisitTimeSteps (08/02/2012 by M.Trias, IAC3)
%
% It loads HDF5 VisIt data over the whole spatial domain, at a particular
% timeStep chosen by the user. In case 'dump_info' was not available, it
% also loads it for future usage.
%
% INPUT:  input     -- it can be either a 'char' string providing tha path
%                     to 'dumps.visit' file, or a directly the 'structure'
%                     with all dump information from the data set
%                     [ultimately, this data structure must be gotten from
%                     a past call of this same function or a call to
%                     'loadVisitDumpInfo' function].
%         timeStep  -- output time step number of the required data (>= 1)
%
% OUTPUT: output    -- data structure with all the required information at
%                     the particular output time step (timeStep)
%         dump_info -- data structure with dump information of the whole
%                     data set obtained from a call to 'loadVisitDumpInfo'
%                     function
%
% -------------------------------------------------------------------------

if (ischar(input))
    [dump_info] = loadVisitDumpInfo(input);
elseif (isstruct(input))
    dump_info = input;
else
    error('The first argument of ''loadVisitData'' function (input) must be either a ''char'' or a ''structure''');
end

timeValues = double(dump_info.nTimeStepsOutput);
for iTimeStep = 1:dump_info.nTimeStepsOutput
    timeValues(iTimeStep) = h5read(dump_info.fileList{iTimeStep},'/BASIC_INFO/time');
end