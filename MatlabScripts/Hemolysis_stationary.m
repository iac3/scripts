function [NIH_Arora_avg, NIH_Garon_avg, NIH_Garon_LIN_avg, NIH_Trias_avg, NIH_Arora, NIH_Garon, NIH_Trias, DHbHb_Arora, DHbHb_Garon, DHbHb_Trias, wght] = Hemolysis_stationary(Npts, datafile, xStart, xEnd)
% Details can be found in 15/10/2012 Miquel's hand-written notes

% Load data
[avTimeValues, dump]   = loadVisitTimeSteps(datafile);
data                   = loadVisitData(dump, size(avTimeValues,2));  % read final, stationary solution

% Fields
vx          = data.rmz./data.rrho;
rho         = data.rho;
dx          = data.dxVec(1);
dy          = data.dxVec(2);
x           = data.xGrid + dx/2;
y           = data.yGrid + dy/2;
yMin        = y(1,1);
yMax        = y(1,end);

% Set xStart and xEnd
if nargin < 3, xStart = x(1,1); end
xStart   = unique(max(sort(xStart), x(1,1)));
n_xStart = max(size(xStart));
%
if nargin < 4, xEnd = x(end,1); end
xEnd   = unique(min(sort(xEnd), x(end,1)));
n_xEnd = max(size(xEnd));

% Loop over inlet particles (flux assumed to be along x-axis)
NIH_Arora_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
NIH_Garon_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
NIH_Garon_LIN_avg(1:n_xEnd, 1:n_xStart)  = 0.0;
NIH_Trias_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
%
wght(1:n_xStart, 1:Npts) = 0.0;
%
DHbHb_Arora(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
DHbHb_Garon(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
DHbHb_Trias(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
NIH_Arora(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;
NIH_Garon(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;
NIH_Trias(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;

for iStart=1:n_xStart
    wght_Acc = 0.0;
    
    % First QUICK SCAN to find the 'r' value closest to 0.006 not giving a
    % null DHbHb result. We shall assume this value to be between 0.005 and
    % 0.006
    y0_QS  = 0.006;
    if (Npts >= 5)
        DHbHb_ok = false;
        iter   = 0;
        dy0_QS = (0.006-0.005)/Npts*5;
        fprintf('Starting initial QuickScan (it may take several minutes) ... ');
        while ((~DHbHb_ok) && (iter<Npts/5))
            iter  = iter+1;
            y0_QS = y0_QS - dy0_QS;
            [~, ~, ~, DHbHb_ok] = computeDHbHb2D(xStart(iStart), y0_QS, 1e9, 1e9, x, y, avTimeValues, dump, 1e4, xEnd);
        end        
        y0_QS = y0_QS + dy0_QS;
        fprintf('done. Found, y0_QS = %.4f \n', y0_QS);
    end
  %
    if (y0_QS < 0.006), y0_vec = 0.006-logspace(log10(0.006-y0_QS),log10(0.006), Npts);
    else                y0_vec = 0.006-logspace(-7,log10(0.006), Npts);  end
    x0_vec(1:Npts) = xStart(iStart);
    
    % Actual LOOP to compute the average
    for i=1:Npts
        
        % Get DHbHb and NIH values
        [DHbHb_Arora(:,iStart,i) DHbHb_Garon(:,iStart,i) DHbHb_Trias(:,iStart,i) DHbHb_ok niter integration_time] = computeDHbHb2D(x0_vec(i), y0_vec(i), 1e9, 1e9, x, y, avTimeValues, dump, 1e4, xEnd);
        NIH_Arora(:,iStart,i) = 100 * DHbHb_Arora(:,iStart,i)*(1.0-0.4)*150.0;
        NIH_Garon(:,iStart,i) = 100 * DHbHb_Garon(:,iStart,i)*(1.0-0.4)*150.0;
        NIH_Trias(:,iStart,i) = 100 * DHbHb_Trias(:,iStart,i)*(1.0-0.4)*150.0;

        % If it is the case, add to the integral    
        if (DHbHb_ok)
            rho_aux  = interp2(x',y',rho',x0_vec(i),y0_vec(i));
            vx_aux   = interp2(x',y',vx',x0_vec(i),y0_vec(i));
            yim1_aux = min(max(y0_vec(max(i-1,1)), yMin), yMax);
            yip1_aux = min(max(y0_vec(min(i+1,Npts)), yMin), yMax);
            Dy_aux   = (yip1_aux - yim1_aux)/2.0;

          % wght(iStart,i) = rho_aux * vx_aux * Dy_aux;                % cartesian
            wght(iStart,i) = rho_aux * vx_aux * y0_vec(i) * Dy_aux;    % cylindrical (axisymmetric)

            NIH_Arora_avg(:,iStart)      = NIH_Arora_avg(:,iStart) + NIH_Arora(:,iStart,i)*wght(iStart,i);
            NIH_Garon_avg(:,iStart)      = NIH_Garon_avg(:,iStart) + NIH_Garon(:,iStart,i)*wght(iStart,i);
            NIH_Trias_avg(:,iStart)      = NIH_Trias_avg(:,iStart) + NIH_Trias(:,iStart,i)*wght(iStart,i);
            NIH_Garon_LIN_avg(:,iStart)  = NIH_Garon_LIN_avg(:,iStart) + power(NIH_Garon(:,iStart,i),1/0.785)*wght(iStart,i);
            wght_Acc                     = wght_Acc + wght(iStart,i);
        end    
        fprintf('Done iStart = %d  (i = %d/%d): niter = %d  ---  int_time = %.2f; DHbHb_ok = %d (%s) \n', iStart, i, Npts, niter, integration_time, DHbHb_ok, datafile);
    end

    NIH_Arora_avg(:,iStart)     = NIH_Arora_avg(:,iStart) / wght_Acc;
    NIH_Garon_avg(:,iStart)     = NIH_Garon_avg(:,iStart) / wght_Acc;
    NIH_Trias_avg(:,iStart)     = NIH_Trias_avg(:,iStart) / wght_Acc;
    NIH_Garon_LIN_avg(:,iStart) = power(NIH_Garon_LIN_avg(:,iStart) / wght_Acc, 0.785);
end