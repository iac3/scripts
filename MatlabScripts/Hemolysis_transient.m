function [NIH_Arora_avg, NIH_Garon_avg, NIH_Garon_LIN_avg, NIH_Trias_avg, NIH_Arora, NIH_Garon, NIH_Trias, DHbHb_Arora, DHbHb_Garon, DHbHb_Trias, wght] = Hemolysis_transient(Npts, datafile, xStart, xEnd, t0, period, plots)

% Hemolysis_transient (20/11/2012 by M.Trias, IAC3)
%
% Computes the average Normalised Index of Hemolysis (NIH) from the output
% of a simulation, assuming a transient result.
%
% All details can be found in 15/10/2012 Miquel's hand-written notes
%
% INPUT:  Npts -- Number of pathlines that shall be considered seeded along 
%                 a transverse line
%         datafile -- 'dumps.visit' results file 
%         xStart -- 'x' value where to start pathlines (it can be a vector)
%         xEnd -- max 'x' value where to stop integrating (it can be a
%                 vector)
%         t0 -- initial simulation time when pathlines are seeded
%         period --if periodic input data, then data from (t0, t0+period)
%                  shall be repeated
%         plots -- 1/0 whether one wants or not to plot some figure results
%
% OUTPUT: NIH_Arora_avg -- NIH value computed à la Arora and averaged over
%                          the tranverse section
%         NIH_Garon_avg -- NIH value computed à la Garon and averaged over
%                          the tranverse section
%         NIH_Garon_LIN_avg -- NIH value computed à la Garon and averaged
%                          (before unlineralization) over the tranverse
%                          section
%         NIH_Trias_avg -- NIH value computed à la Trias and averaged over
%                          the tranverse section
%         NIH_Arora -- NIH values from each pathline computed à la Arora
%         NIH_Garon -- NIH values from each pathline computed à la Garon
%         NIH_Trias -- NIH values from each pathline computed à la Trias
%         DHbHb_Arora -- DHb/Hb values from each pathline, à la Arora
%         DHbHb_Garon -- DHb/Hb values from each pathline, à la Garon
%         DHbHb_Trias -- DHb/Hb values from each pathline, à la Trias
%         wght -- weights used to compute averages over transverse sections
%
% ·········································································

% Load data
[avTimeValues, dump]   = loadVisitTimeSteps(datafile);

% Implementing some controls
if (period < max(avTimeValues))
    fprintf('WARNING: period cannot be smaller than max. available time values');
    period = max(avTimeValues);
end
t0 = max(t0,min(avTimeValues));

periodic = true;
if (period > 1000*max(avTimeValues))  % the factor 1000 is chosen arbitrarily (for safety)
    periodic = false;
end
%if (periodic), t0 = mod(t0,period); end
% -------------------------------------------------------------------------

% Interpolate data from the two 'time_steps' surrounding t0
it_prev   = find(avTimeValues<=t0,1,'last');
t_prev    = avTimeValues(it_prev);
data_prev = loadVisitData(dump, it_prev);
vx_prev   = data_prev.rmz./data_prev.rrho;
rho_prev  = data_prev.rho;
%
if (it_prev < max(avTimeValues))
    it_post   = it_prev + 1;
    t_post    = avTimeValues(it_post);
    data_post = loadVisitData(dump, it_post);
    vx_post   = data_post.rmz./data_post.rrho;
    rho_post  = data_post.rho;
else
    t_post    = t_prev;
    vx_post   = vx_prev;
    rho_post  = rho_prev;
end
%
if (t_prev == t_post)
    vx  = vx_prev;
    rho = rho_prev;
else
    vx  = vx_prev  + (t0-t_prev)/(t_post-t_prev) * (vx_post-vx_prev);
    rho = rho_prev + (t0-t_prev)/(t_post-t_prev) * (rho_post-rho_prev);
end

% Get grid data from any 'time_step', for instance, the first one
data        = loadVisitData(dump, 1);
dx          = data.dxVec(1);
dy          = data.dxVec(2);
x           = data.xGrid + dx/2;
y           = data.yGrid + dy/2;
yMin        = y(1,1);
yMax        = y(1,end);

% Set xStart and xEnd
if nargin < 3, xStart = x(1,1); end
xStart   = unique(max(sort(xStart), x(1,1)));
n_xStart = max(size(xStart));
%
if nargin < 4, xEnd = x(end,1); end
xEnd   = unique(min(sort(xEnd), x(end,1)));
n_xEnd = max(size(xEnd));

% Loop over inlet particles (flux assumed to be along x-axis)
NIH_Arora_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
NIH_Garon_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
NIH_Garon_LIN_avg(1:n_xEnd, 1:n_xStart)  = 0.0;
NIH_Trias_avg(1:n_xEnd, 1:n_xStart)      = 0.0;
%
wght(1:n_xStart, 1:Npts) = 0.0;
%
DHbHb_Arora(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
DHbHb_Garon(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
DHbHb_Trias(1:n_xEnd, 1:n_xStart, 1:Npts) = 0.0;
NIH_Arora(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;
NIH_Garon(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;
NIH_Trias(1:n_xEnd, 1:n_xStart, 1:Npts)   = 0.0;

for iStart=1:n_xStart
    wght_Acc = 0.0;
    
    % First QUICK SCAN to find the 'r' value closest to 0.006 not giving a
    % null DHbHb result. We shall assume this value to be between 0.005 and
    % 0.006
    y0_QS  = 0.006;
    if (Npts >= 5)
        DHbHb_ok = false;
        iter   = 0;
        dy0_QS = (0.006-0.005)/Npts*5;
        fprintf('Starting initial QuickScan (it may take several minutes) ... ');
        while ((~DHbHb_ok) && (iter<Npts/5))
            iter  = iter+1;
            y0_QS = y0_QS - dy0_QS;
            [~, ~, ~, DHbHb_ok] = computeDHbHb2D(xStart(iStart), y0_QS, t0, period, x, y, avTimeValues, dump, 1e4, xEnd);
        end        
        y0_QS = y0_QS + dy0_QS;
        fprintf('done. Found, y0_QS = %.4f \n', y0_QS);
    end
  %
    if (y0_QS < 0.006), y0_vec = 0.006-logspace(log10(0.006-y0_QS),log10(0.006), Npts);
    else                y0_vec = 0.006-logspace(-7,log10(0.006), Npts);  end
    x0_vec(1:Npts) = xStart(iStart);

    if (plots)
        if (n_xStart > 1)
            figure('Position',[2 686+(iStart-1)/(n_xStart-1)*(30-686) 1438 109]);
        else
            figure('Position',[2 686 1438 109]);
        end
        
        pcolor(x',y',data_prev.Segment');
        shading flat;
        axis equal;
        ax_pos = get(gca,'Position');
        set(gca, 'Position',[0.025 ax_pos(2) 0.95 ax_pos(4)], ...
                 'XLim',[0.02 0.22], 'YLim',[0 0.006], 'CLim',[3 5]);
        colormap('autumn');
        find_aux = strfind(datafile,'/');
        datafile_aux = datafile(find_aux(end-1)+1:end-12);
        datafile_auxregexprep(datafile_aux, '_', ' ');
        title([datafile_aux sprintf('  ---  xStart = %.3f  (%d/%d)',xStart(iStart),iStart,n_xStart)]);
        hold on;
    end
    
    % Actual LOOP to compute the average
    for i=1:Npts
        
        % Get DHbHb and NIH values
        [DHbHb_Arora(:,iStart,i) DHbHb_Garon(:,iStart,i) DHbHb_Trias(:,iStart,i) DHbHb_ok niter integration_time, xpart, ypart] = computeDHbHb2D(x0_vec(i), y0_vec(i), t0, period, x, y, avTimeValues, dump, 1e4, xEnd);
        if (plots && DHbHb_ok), plot(xpart,ypart); end
        
        NIH_Arora(:,iStart,i) = 100 * DHbHb_Arora(:,iStart,i)*(1.0-0.4)*150.0;
        NIH_Garon(:,iStart,i) = 100 * DHbHb_Garon(:,iStart,i)*(1.0-0.4)*150.0;
        NIH_Trias(:,iStart,i) = 100 * DHbHb_Trias(:,iStart,i)*(1.0-0.4)*150.0;

        % If it is the case, add to the integral    
        if (DHbHb_ok)
            rho_aux  = interp2(x',y',rho',x0_vec(i),y0_vec(i));
            vx_aux   = interp2(x',y',vx',x0_vec(i),y0_vec(i));
            yim1_aux = min(max(y0_vec(max(i-1,1)), yMin), yMax);
            yip1_aux = min(max(y0_vec(min(i+1,Npts)), yMin), yMax);
            Dy_aux   = (yip1_aux - yim1_aux)/2.0;

          % wght(iStart,i) = rho_aux * vx_aux * Dy_aux;                % cartesian
            wght(iStart,i) = rho_aux * vx_aux * y0_vec(i) * Dy_aux;    % cylindrical (axisymmetric)

            NIH_Arora_avg(:,iStart)      = NIH_Arora_avg(:,iStart) + NIH_Arora(:,iStart,i)*wght(iStart,i);
            NIH_Garon_avg(:,iStart)      = NIH_Garon_avg(:,iStart) + NIH_Garon(:,iStart,i)*wght(iStart,i);
            NIH_Trias_avg(:,iStart)      = NIH_Trias_avg(:,iStart) + NIH_Trias(:,iStart,i)*wght(iStart,i);
            NIH_Garon_LIN_avg(:,iStart)  = NIH_Garon_LIN_avg(:,iStart) + power(NIH_Garon(:,iStart,i),1/0.785)*wght(iStart,i);
            wght_Acc                     = wght_Acc + wght(iStart,i);
        end    
        fprintf('Done iStart = %d  (i = %d/%d): niter = %d  ---  int_time = %.2f; DHbHb_ok = %d (%s) \n', iStart, i, Npts, niter, integration_time, DHbHb_ok, datafile);
    end

    if (wght_Acc == 0), error('wght_Acc = 0; probably because v(t0) = 0 in all points'); end
    NIH_Arora_avg(:,iStart)     = NIH_Arora_avg(:,iStart) / wght_Acc;
    NIH_Garon_avg(:,iStart)     = NIH_Garon_avg(:,iStart) / wght_Acc;
    NIH_Trias_avg(:,iStart)     = NIH_Trias_avg(:,iStart) / wght_Acc;
    NIH_Garon_LIN_avg(:,iStart) = power(NIH_Garon_LIN_avg(:,iStart) / wght_Acc, 0.785);
end