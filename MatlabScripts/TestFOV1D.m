function [FOV] = TestFOV1D(dx)

FOV = max(0.0,min(1.0,(1-dx)/2.0));

return;
end