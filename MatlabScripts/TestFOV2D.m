function [FOV] = TestFOV2D(dx,dy)

X = max(-1e4, min(1e4, (1-dx) + max(1e-10,abs(dy))/max(1e-10,abs(dx))*(1-dy)));
Y = max(-1e4, min(1e4, (1-dy) + max(1e-10,abs(dx))/max(1e-10,abs(dy))*(1-dx)));

FOV = (X*Y/2.0 * ( 1 - (1-max(0,min(1,2/X)))^2 - (1-max(0,min(1,2/Y)))^2 + (1-max(0,min(1,2/X+2/Y)))^2 )) / 4.0;

return;
end