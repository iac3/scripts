function [FOV] = TestFOV3D(dx,dy,dz)

X = max(-1e4, min(1e4, (1-dx) + max(1e-10,abs(dy))/max(1e-10,abs(dx))*(1-dy) + max(1e-10,abs(dz))/max(1e-10,abs(dx))*(1-dz)));
Y = max(-1e4, min(1e4, (1-dy) + max(1e-10,abs(dx))/max(1e-10,abs(dy))*(1-dx) + max(1e-10,abs(dz))/max(1e-10,abs(dy))*(1-dz)));
Z = max(-1e4, min(1e4, (1-dz) + max(1e-10,abs(dx))/max(1e-10,abs(dz))*(1-dx) + max(1e-10,abs(dy))/max(1e-10,abs(dz))*(1-dy)));

FOV = (X*Y*Z/6.0 * ( 1 - (1-max(0,min(1,2/X)))^3 - (1-max(0,min(1,2/Y)))^3 - (1-max(0,min(1,2/Z)))^3 ...
                       + (1-max(0,min(1,2/X+2/Y)))^3 + (1-max(0,min(1,2/X+2/Z)))^3 + (1-max(0,min(1,2/Y+2/Z)))^3 ...
                       - (1-max(0,min(1,2/X+2/Y+2/Z)))^3 )) / 8.0;

return;
end