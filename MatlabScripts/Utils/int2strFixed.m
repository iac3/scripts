function [str_out] = int2strFixed(input,size_out)

% int2strFixed (07/02/2012 by M.Trias, IAC3)
%
% It converts an integer to string with a fixed number of digits, adding
% zeros in front in case the integer is too short, and truncating the first
% digits of the integer in case it is longer.
%
% INPUT:  input    -- integer to be converted to string
%         size_out -- number of digits of the output string
%
% OUTPUT: str_out  -- output string
%
% -------------------------------------------------------------------------

% Initial checkings
if (input < 0)
    error('The first argument of ''int2strFixed'' function (input) must be positive');
end
if (size_out < 1)
    error('The second argument of ''int2strFixed'' function (size) must be greater than 0');
end

input   = double(round(input));
str_out = char(size_out);
iaux    = size_out;

% Compute higher order digit
newIndex    = floor(input/power(10,iaux-1));
newIndexStr = int2str(newIndex);
str_out     = newIndexStr(size(newIndexStr,2));

base        = newIndex*power(10,iaux-1);
iaux        = iaux-1;
    
% Compute the rest of digits
while (iaux > 0)
    newIndex    = floor((input-base)/power(10,iaux-1));
    newIndexStr = int2str(newIndex);
    str_out     = [str_out newIndexStr];
    %
    base        = base + newIndex*power(10,iaux-1);
    iaux        = iaux-1;
end
