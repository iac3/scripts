function [DHbHb_Arora, DHbHb_Garon, DHbHb_Trias, DHbHb_ok, niter, integration_time, xpart, ypart, vxpart, vypart, shearStresspart] = computeDHbHb2D(x0, y0, t0, period, x, y, avTimeValues, dump, max_iter, xEnd)
% Details can be found in 15/10/2012 Miquel's hand-written notes

if nargin < 10, xEnd = x(end,1); end
xEnd   = unique(min(sort(xEnd), x(end,1)));
n_xEnd = max(size(xEnd));

% Constant defs
val_3_0777        = 2416/785;
val_m0_273885     = -43/157;
val_4_891059em9   = 0.785*power(3.62e-7, 1/0.785);
val_6_23065em9    = power(3.62e-7, 1/0.785);
val_2_8417em7     = 3.62e-7 * 0.785;
val_0_726115      = 2 - 1/0.785;
val_1_0286957em13 = val_4_891059em9 * power(3.62e-7, val_0_726115);
val_0_57          = 0.785*val_0_726115;
val_4_832         = val_3_0777 + 2.416*val_0_726115;
val_3_832         = val_4_832 - 1;
val_4_238596      = val_3_0777 / val_0_726115;

% Initial calculations
xMin = x(1,1);
xMax = max(xEnd);
yMin = y(1,1);
yMax = y(1,end);

x0 = min(max(x0, xMin), xMax);
y0 = min(max(y0, yMin), yMax);

if (t0 + period > max(avTimeValues))
    fprintf('WARNING: period cannot be greater than max. available time values');
    t0 = max(avTimeValues) - period;
end
t = max(t0,min(avTimeValues));

periodic = true;
if (period > 1000*max(avTimeValues))  % the factor 1000 is chosen arbitrarily (for safety)
    periodic = false;
end
if (periodic), t = t0 + mod(t-t0,period); end
%
it_prev          = find(avTimeValues<=t,1,'last');
t_prev           = avTimeValues(it_prev);
data_prev        = loadVisitData(dump, it_prev);
vx_prev          = data_prev.rmz./data_prev.rrho;
vy_prev          = data_prev.rmr./data_prev.rrho;
shearStress_prev = data_prev.shearStress;
% 
if (t_prev < max(avTimeValues))
    % still in transient
    stationary       = false;
    it_post          = it_prev + 1;
    t_post           = avTimeValues(it_post);
    data_post        = loadVisitData(dump, it_post);
    vx_post          = data_post.rmz./data_post.rrho;
    vy_post          = data_post.rmr./data_post.rrho;
    shearStress_post = data_post.shearStress;
else
    % already in stationary
    stationary = true;
end
%
vx0 = interp2(x',y',vx_prev',x0,y0);
vy0 = interp2(x',y',vy_prev',x0,y0);
v   = sqrt(vx0*vx0 + vy0*vy0);
L   = max(xMax-xMin, yMax-yMin);
%dt  = (L/v) / 5000;
dt  = 1e-4;

% Trajectory and integration (using a RK4)
i = 1;
k = 1;

DHbHb_Arora(1:n_xEnd) = 0.0;
DHbHb_Garon(1:n_xEnd) = 0.0;
DHbHb_Trias(1:n_xEnd) = 0.0;
DHbHb_Acc_Arora = 0.0;
DHbHb_Acc_Garon = 0.0;
DHbHb_Acc_Trias = 0.0;
xpart(i) = x0;
ypart(i) = y0;

DHbHb_ok        = false;
%if (v>0), final = false;
%else      final = true;   dt = 0;   end
final = false;

while(~final)
    if ( (~stationary) && (t > t_post) )
        it_prev          = find(avTimeValues<=t,1,'last');
        t_prev           = avTimeValues(it_prev);
        data_prev        = loadVisitData(dump, it_prev);
        vx_prev          = data_prev.rmz./data_prev.rrho;
        vy_prev          = data_prev.rmr./data_prev.rrho;
        shearStress_prev = data_prev.shearStress;
      %  
        if (it_prev < max(avTimeValues))
            % still in transient
            stationary       = false;
            it_post          = it_prev + 1;
            t_post           = avTimeValues(it_post);
            data_post        = loadVisitData(dump, it_post);
            vx_post          = data_post.rmz./data_post.rrho;
            vy_post          = data_post.rmr./data_post.rrho;
            shearStress_post = data_post.shearStress;
        else
            % already in stationary
            stationary = true;
            clear it_post data_post vx_post vy_post shearStress_post;
        end
    end
    
    % RK4-p1
        % Interpolate values
        if (~stationary)
            vxpart(i)          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vx_prev',vx_post'), xpart(i),ypart(i),t);
            vypart(i)          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vy_prev',vy_post'), xpart(i),ypart(i),t);
            shearStresspart(i) = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,shearStress_prev',shearStress_post'), xpart(i),ypart(i),t);
        else
            vxpart(i)          = interp2(x',y', vx_prev', xpart(i),ypart(i));
            vypart(i)          = interp2(x',y', vy_prev', xpart(i),ypart(i));
            shearStresspart(i) = interp2(x',y', shearStress_prev', xpart(i),ypart(i));
        end
        
        % Movement
        xpart_k1 = xpart(i) + vxpart(i)*dt/2.0;
        ypart_k1 = ypart(i) + vypart(i)*dt/2.0;
        t_k1     = t + dt/2.0;
        
        % Check to be within computational domain
        if (~((xpart_k1 >= xMin) && (xpart_k1 <= xMax) && (ypart_k1 >= yMin) && (ypart_k1 <= yMax)))
            final = true;
            if (xpart_k1 > xMax)  % in our case, "DHbHb_ok = true" if particle leaves through outlet boundary
                DHbHb_ok = true;
            end
        end
        
    % RK4-p2
    if (~final)
        % Interpolate values
        if (~stationary)
            vxpart_k2          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vx_prev',vx_post'), xpart_k1,ypart_k1,min(t_k1,t_post));
            vypart_k2          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vy_prev',vy_post'), xpart_k1,ypart_k1,min(t_k1,t_post));
            shearStresspart_k2 = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,shearStress_prev',shearStress_post'), xpart_k1,ypart_k1,min(t_k1,t_post));
        else
            vxpart_k2          = interp2(x',y', vx_prev', xpart_k1,ypart_k1);
            vypart_k2          = interp2(x',y', vy_prev', xpart_k1,ypart_k1);
            shearStresspart_k2 = interp2(x',y', shearStress_prev', xpart_k1,ypart_k1);
        end

        % Integrate RK's part of DHbHb
        if (i>1)
            aux = power(shearStresspart(i), val_3_0777) * power(DHbHb_Acc_Trias, val_m0_273885);
            DHbHb_Acc_Trias_k2 = DHbHb_Acc_Trias + val_4_891059em9 * aux*dt/2.0;
        else   % only the case "i=1" has to be treated specially in order to avoid the t=0 divergence
            dtau_dt = (shearStresspart_k2-shearStresspart(i))/(dt/2.0);
            %aux = (power(shearStresspart_k2, val_3_832)+power(shearStresspart(i), val_3_832))/2.0 * power(dt/2.0, val_0_57) * dtau_dt;
            aux = 0.5 * power(shearStresspart_k2, val_3_832) * power(dt/2.0, val_0_57) * dtau_dt;
            DHbHb_Acc_Trias_k2 = DHbHb_Acc_Trias + val_1_0286957em13 * (power(shearStresspart_k2, val_4_832)*power(dt/2.0, val_0_57)/val_0_726115 - val_4_238596*aux*dt/2.0);
        end

        % Movement
        xpart_k2 = xpart(i) + vxpart_k2*dt/2.0;
        ypart_k2 = ypart(i) + vypart_k2*dt/2.0;
        t_k2     = t + dt/2.0;
    
        % Check to be within computational domain
        if (~((xpart_k2 >= xMin) && (xpart_k2 <= xMax) && (ypart_k2 >= yMin) && (ypart_k2 <= yMax)))
            final = true;
            if (xpart_k2 > xMax)  % in our case, "DHbHb_ok = true" if particle leaves through outlet boundary
                DHbHb_ok = true;
            end
        end
    else
        xpart_k2 = xpart_k1;
    end
    
    % RK4-p3
    if (~final)
        % Interpolate values
        if (~stationary)
            vxpart_k3          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vx_prev',vx_post'), xpart_k2,ypart_k2,min(t_k2,t_post));
            vypart_k3          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vy_prev',vy_post'), xpart_k2,ypart_k2,min(t_k2,t_post));
            shearStresspart_k3 = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,shearStress_prev',shearStress_post'), xpart_k2,ypart_k2,min(t_k2,t_post));
        else
            vxpart_k3          = interp2(x',y', vx_prev', xpart_k2,ypart_k2);
            vypart_k3          = interp2(x',y', vy_prev', xpart_k2,ypart_k2);
            shearStresspart_k3 = interp2(x',y', shearStress_prev', xpart_k2,ypart_k2);
        end
    
        % Integrate RK's part of DHbHb
        if (i>1)
            aux = power(shearStresspart_k2, val_3_0777) * power(DHbHb_Acc_Trias_k2, val_m0_273885);
            DHbHb_Acc_Trias_k3 = DHbHb_Acc_Trias + val_4_891059em9 * aux*dt/2.0;
        else   % only the case "i=1" has to be treated specially in order to avoid the t=0 divergence
            dtau_dt = (shearStresspart_k3-shearStresspart(i))/(dt/2.0);
            %aux = (power(shearStresspart_k3, val_3_832)+power(shearStresspart(i), val_3_832))/2.0 * power(dt/2.0, val_0_57) * dtau_dt;
            aux = 0.5 * power(shearStresspart_k3, val_3_832) * power(dt/2.0, val_0_57) * dtau_dt;
            DHbHb_Acc_Trias_k3 = DHbHb_Acc_Trias + val_1_0286957em13 * (power(shearStresspart_k3, val_4_832)*power(dt/2.0, val_0_57)/val_0_726115 - val_4_238596*aux*dt/2.0);
        end

        % Movement
        xpart_k3 = xpart(i) + vxpart_k3*dt;
        ypart_k3 = ypart(i) + vypart_k3*dt;
        t_k3     = t + dt;
    
        % Check to be within computational domain
        if (~((xpart_k3 >= xMin) && (xpart_k3 <= xMax) && (ypart_k3 >= yMin) && (ypart_k3 <= yMax)))
            final = true;
            if (xpart_k3 > xMax)  % in our case, "DHbHb_ok = true" if particle leaves through outlet boundary
                DHbHb_ok = true;
            end
        end
    else
        xpart_k3 = xpart_k2;
    end
    
    % RK4-p4
    if (~final)
        % Interpolate values
        if (~stationary)
            vxpart_k4          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vx_prev',vx_post'), xpart_k3,ypart_k3,min(t_k3,t_post));
            vypart_k4          = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,vy_prev',vy_post'), xpart_k3,ypart_k3,min(t_k3,t_post));
            shearStresspart_k4 = interp3(cat(3,x',x'), cat(3,y',y'), cat(3,t_prev*ones(size(x')),t_post*ones(size(x'))), cat(3,shearStress_prev',shearStress_post'), xpart_k3,ypart_k3,min(t_k3,t_post));
        else
            vxpart_k4          = interp2(x',y', vx_prev', xpart_k3,ypart_k3);
            vypart_k4          = interp2(x',y', vy_prev', xpart_k3,ypart_k3);
            shearStresspart_k4 = interp2(x',y', shearStress_prev', xpart_k3,ypart_k3);
        end
        
        % Integrate RK's part of DHbHb
        if (i>1)
            aux = power(shearStresspart_k3, val_3_0777) * power(DHbHb_Acc_Trias_k3, val_m0_273885);
            DHbHb_Acc_Trias_k4 = DHbHb_Acc_Trias + val_4_891059em9 * aux*dt;
        else   % only the case "i=1" has to be treated specially in order to avoid the t=0 divergence
            dtau_dt = (shearStresspart_k4-shearStresspart(i))/dt;
            %aux = (power(shearStresspart_k4, val_3_832)+power(shearStresspart(i), val_3_832))/2.0 * power(dt, val_0_57) * dtau_dt;
            aux = 0.5 * power(shearStresspart_k4, val_3_832) * power(dt, val_0_57) * dtau_dt;
            DHbHb_Acc_Trias_k4 = DHbHb_Acc_Trias + val_1_0286957em13 * (power(shearStresspart_k4, val_4_832)*power(dt, val_0_57)/val_0_726115 - val_4_238596*aux*dt);
        end

        % Movement
        newX = xpart(i) + dt/6.0 * (vxpart(i) + 2.0*vxpart_k2 + 2.0*vxpart_k3 + vxpart_k4);
        newY = ypart(i) + dt/6.0 * (vypart(i) + 2.0*vypart_k2 + 2.0*vypart_k3 + vypart_k4);
        
        if ((newX >= xMin) && (newX <= xMax) && (newY >= yMin) && (newY <= yMax))
            % Integrate DHb/Hb
              % Arora
                if (i>1)
                    aux_k1 = power(shearStresspart(i), 2.416) * power((i-1 + 0.0)*dt, -0.215);
                    aux_k2 = power(shearStresspart_k2, 2.416) * power((i-1 + 0.5)*dt, -0.215);
                    aux_k3 = power(shearStresspart_k3, 2.416) * power((i-1 + 0.5)*dt, -0.215);
                    aux_k4 = power(shearStresspart_k4, 2.416) * power((i-1 + 1.0)*dt, -0.215);
                    DHbHb_Acc_Arora = DHbHb_Acc_Arora + val_2_8417em7 * dt/6.0 * (aux_k1 + 2.0*aux_k2 + 2.0*aux_k3 + aux_k4);
                else   % only the case "i=1" has to be treated specially in order to avoid the t=0 divergence
                    dtau_dt = (shearStresspart_k4-shearStresspart(i))/dt;
                    aux_k1  = val_3_0777 * power(shearStresspart(i), 1.416) * power((i-1 + 0.0)*dt, 0.785) * dtau_dt;
                    aux_k2  = val_3_0777 * power(shearStresspart_k2, 1.416) * power((i-1 + 0.5)*dt, 0.785) * dtau_dt;
                    aux_k3  = val_3_0777 * power(shearStresspart_k3, 1.416) * power((i-1 + 0.5)*dt, 0.785) * dtau_dt;
                    aux_k4  = val_3_0777 * power(shearStresspart_k4, 1.416) * power((i-1 + 1.0)*dt, 0.785) * dtau_dt;
                    DHbHb_Acc_Arora = val_2_8417em7 * ( power(dt, 0.785)*power(shearStresspart_k4, 2.416)/0.785 - dt/6.0 * (aux_k1 + 2.0*aux_k2 + 2.0*aux_k3 + aux_k4) );
                end
                
              % Garon
                auxG_k1 = power(shearStresspart(i), val_3_0777);
                auxG_k2 = power(shearStresspart_k2, val_3_0777);
                auxG_k3 = power(shearStresspart_k3, val_3_0777);
                auxG_k4 = power(shearStresspart_k4, val_3_0777);
                DHbHb_Acc_Garon = DHbHb_Acc_Garon + val_6_23065em9 * dt/6.0 * (auxG_k1 + 2.0*auxG_k2 + 2.0*auxG_k3 + auxG_k4);
                
              % Trias
                if (i>1)
                    aux_k1 = power(shearStresspart(i), val_3_0777) * power(DHbHb_Acc_Trias,    val_m0_273885);
                    aux_k2 = power(shearStresspart_k2, val_3_0777) * power(DHbHb_Acc_Trias_k2, val_m0_273885);
                    aux_k3 = power(shearStresspart_k3, val_3_0777) * power(DHbHb_Acc_Trias_k3, val_m0_273885);
                    aux_k4 = power(shearStresspart_k4, val_3_0777) * power(DHbHb_Acc_Trias_k4, val_m0_273885);
                    DHbHb_Acc_Trias = DHbHb_Acc_Trias + val_4_891059em9 * dt/6.0 * (aux_k1 + 2.0*aux_k2 + 2.0*aux_k3 + aux_k4);
                else   % only the case "i=1" has to be treated specially in order to avoid the t=0 divergence
                    aux_k1 = 0.0;
                    aux_k2 = power(shearStresspart_k2, val_3_0777) * power(DHbHb_Acc_Trias_k2, val_m0_273885);
                    aux_k3 = power(shearStresspart_k3, val_3_0777) * power(DHbHb_Acc_Trias_k3, val_m0_273885);
                    aux_k4 = power(shearStresspart_k4, val_3_0777) * power(DHbHb_Acc_Trias_k4, val_m0_273885);
                    DHbHb_Acc_Trias = DHbHb_Acc_Trias + val_4_891059em9 * dt/6.0 * (aux_k1 + 2.0*aux_k2 + 2.0*aux_k3 + aux_k4);
                end

            % Update new position
            i = i+1;
            xpart(i) = newX;
            ypart(i) = newY;
            t = t + dt;
            if (periodic && t > period)
                t = mod(t,period);
                
                % Reload 'prev' and 'post' values
                it_prev          = find(avTimeValues<=t,1,'last');
                t_prev           = avTimeValues(it_prev);
                data_prev        = loadVisitData(dump, it_prev);
                vx_prev          = data_prev.rmz./data_prev.rrho;
                vy_prev          = data_prev.rmr./data_prev.rrho;
                shearStress_prev = data_prev.shearStress;
                %  
                if (it_prev < max(avTimeValues))
                    % still in transient
                    stationary       = false;
                    it_post          = it_prev + 1;
                    t_post           = avTimeValues(it_post);
                    data_post        = loadVisitData(dump, it_post);
                    vx_post          = data_post.rmz./data_post.rrho;
                    vy_post          = data_post.rmr./data_post.rrho;
                    shearStress_post = data_post.shearStress;
                else
                    % already in stationary
                    stationary = true;
                    clear it_post data_post vx_post vy_post shearStress_post;
                end
            end
        else
            final = true;
            if (newX > xMax)  % in our case, "DHbHb_ok = true" if particle leaves through outlet boundary
                DHbHb_ok = true;
            end
        end
        
        if (i+1 > max_iter)
            final = true;
            DHbHb_Arora(k:n_xEnd) = 1e9;
            DHbHb_Garon(k:n_xEnd) = 1e9;
            DHbHb_Trias(k:n_xEnd) = 1e9;
        end    
    else
        newX = xpart_k3;
    end
    
    if ( (xpart_k1 > xEnd(k)) || (xpart_k2 > xEnd(k)) || (xpart_k3 > xEnd(k)) || (newX > xEnd(k)) )
        DHbHb_Arora(k) = DHbHb_Acc_Arora;
        DHbHb_Garon(k) = DHbHb_Acc_Garon;
        DHbHb_Trias(k) = DHbHb_Acc_Trias;
        k = k+1;
    end
end
DHbHb_Garon      = power(DHbHb_Garon, 0.785);
niter            = size(xpart,2);
integration_time = niter*dt;