function [output] = orderOfConvergence(input_A, input_B, input_C, timeRef_input, option)

% orderOfConvergence (10/02/2012 by M.Trias, IAC3)
%
% Computes the Order Of Convergence and a Richarson Extrapolation-like
% output of the numerical method at each spatial point and for each
% variable from three results @ different resolutions. The output will be
% a data structure (like the output of 'loadVisitData.m') with all the
% variables evaluated at the chosen grid, plus the computed Order Of
% Convergence values.
%
% INPUT:  input_{A,B,C} -- the three resolution data sets (it can be either
%                          a char string or the structure with all
%                          dump_info) 
%         timeRef_input -- time value (in seconds) where to perform the
%                         calculations. The function will look for the
%                         nearest (shared) timeStep available in the input
%         option -- optional argument that indicates over which grid, the
%                  low-res one ('L'), the mid-res one ('M') or the high-res
%                  one ('H'), the extrapolated variables must be computed.
%                  Notice that 'H' and 'M'implies an interpolation of the
%                  low-res data [default: 'L']
%
% OUTPUT: output -- data structure with all the required information and
%                  variables. It contains the same information as the
%                  output of 'loadVisitData.m', plus some extra information
%                  about the original data used in the extrapolation and
%                  the computed Order Of Convergence (it also contains some
%                  extra statistical information).
%
% ·········································································

% Check number of inputs.
if (nargin > 5)
    error('Too many inputs');
end
% Fill in unset optional values with defaults.
switch nargin
    case 4
        option = 'L';
end
% -------------------------------------------------------------------------

% Get available timeSteps and timeValues in each data set (here we also get
% the dump information of both data sets)
[avTimeValues_A, dump_A] = loadVisitTimeSteps(input_A);
[avTimeValues_B, dump_B] = loadVisitTimeSteps(input_B);
[avTimeValues_C, dump_C] = loadVisitTimeSteps(input_C);

% Make a list of shared variables (the ones that shall be extrapolated) and
% give an error in case there is none
shrVars_A = [];
shrVars_B = [];
shrVars_C = [];
for iVar = 1:dump_A.nVars
    strcmp_AB = strcmp(dump_B.varNames, dump_A.varNames{iVar});
    strcmp_AC = strcmp(dump_C.varNames, dump_A.varNames{iVar});
    if (any(strcmp_AB) && any(strcmp_AC))
        shrVars_A = [shrVars_A  iVar];
        shrVars_B = [shrVars_B find(strcmp_AB,1,'first')];
        shrVars_C = [shrVars_C find(strcmp_AC,1,'first')];
    end
    clear strcmp_AB strcmp_AC;
end

if (isempty(shrVars_A))
    error('There are no shared variables between the two input data sets.');
end

% Compare the arrays of available timeValues and look for the shared ones
% (we do it in a fancy but efficient way)
tol = 1e-8*max([ max(abs(avTimeValues_A)), max(abs(avTimeValues_B)), max(abs(avTimeValues_C)) ]);
%
% (shared between A and B)
shrInd_aux        = find( abs(avTimeValues_A'*ones(size(avTimeValues_B)) - ones(size(avTimeValues_A'))*avTimeValues_B) < tol );
shrInd_Atmp       = mod(shrInd_aux-1,size(avTimeValues_A,2))+1;
shrInd_Btmp       = ceil(shrInd_aux/size(avTimeValues_A,2));
shrTimeValues_AB  = avTimeValues_A(shrInd_Atmp);  % also possible using B results
clear shrInd_aux; 
%
% (shared between AB and C)
shrInd_aux     = find( abs(shrTimeValues_AB'*ones(size(avTimeValues_C)) - ones(size(shrTimeValues_AB'))*avTimeValues_C) < tol );
shrInd_ABtmp   = mod(shrInd_aux-1,size(shrTimeValues_AB,2))+1;
shrInd_C       = ceil(shrInd_aux/size(shrTimeValues_AB,2));
shrInd_A       = shrInd_Atmp(shrInd_ABtmp);
shrInd_B       = shrInd_Btmp(shrInd_ABtmp);
shrTimeValues  = shrTimeValues_AB(shrInd_ABtmp);  % also possible using C results
clear tol shrInd_Atmp shrInd_Btmp shrInd_ABtmp shrInd_aux; 

% Given 'timeRef_input', look for the closest 'shared' time value available
index_nearest = interp1(shrTimeValues,1:size(shrTimeValues,2),timeRef_input,'nearest');

timeStep_A = shrInd_A(index_nearest);
timeStep_B = shrInd_B(index_nearest);
timeStep_C = shrInd_C(index_nearest);

timeRef = avTimeValues_A(shrInd_A(index_nearest)); % also possible using 
                                                   % B or C results
if ( abs(timeRef-timeRef_input) > 5/100*abs(avTimeValues_A(end)-avTimeValues_A(1)) )
    warning('IAC3:orderOfConvergence','Nearest available time value (%.2f)  differs by more than 5%% of the input one (%.2f)', timeRef, timeRef_input);
end

% Load actual data at the corresponding timeStep's
[data_A] = loadVisitData(dump_A, timeStep_A);
[data_B] = loadVisitData(dump_B, timeStep_B);
[data_C] = loadVisitData(dump_C, timeStep_C);

% Get the non-zero resolution values and check consistency on regridding
% factors in each dimension
dxVec_A = data_A.dxVec(data_A.dxVec~=0);
dxVec_B = data_B.dxVec(data_B.dxVec~=0);
dxVec_C = data_C.dxVec(data_C.dxVec~=0);
regriddingFactor_BA  = dxVec_B./dxVec_A;
regriddingFactor_CA  = dxVec_C./dxVec_A;
nDims                = size(regriddingFactor_BA,1);
if ((nDims < 1) || (nDims > 3))
    error('orderOfConvergence function only supports 1D, 2D and 3D data.');
end
clear dxVec_A dxVec_B dxVec_C;

if ( (any(diff(regriddingFactor_BA)) ~= 0) || (any(diff(regriddingFactor_CA)) ~= 0) )
    error('The regridding factor is not the same in all dimensions.');
else
    regriddingFactor_BA = regriddingFactor_BA(1);
    regriddingFactor_CA = regriddingFactor_CA(1);
end

% From the regriddingFactors, sort {A,B,C} data sets from the highest
% resolution to the lowest one (i.e.: h1 < h2 < h3).
sortedABC = 'ABC';
[~, iX] = sort([1 regriddingFactor_BA regriddingFactor_CA], 'ascend');
sortedABC = sortedABC(iX);
%
regriddingFactor_AB = 1/regriddingFactor_BA;
regriddingFactor_AC = 1/regriddingFactor_CA;
regriddingFactor_BC = regriddingFactor_BA/regriddingFactor_CA;
regriddingFactor_CB = 1/regriddingFactor_BC;
%
cmd1 = sprintf('regriddingFactor(1) = regriddingFactor_%s%s;', sortedABC(3), sortedABC(1));   eval(cmd1);   clear cmd1;  % regriddingFactor(1) is the largest: h3/h1
cmd2 = sprintf('regriddingFactor(2) = regriddingFactor_%s%s;', sortedABC(2), sortedABC(1));   eval(cmd2);   clear cmd2;  % regriddingFactor(2) is the smallest: h2/h1
clear regriddingFactor_AB regriddingFactor_AC regriddingFactor_BA regriddingFactor_BC regriddingFactor_CA regriddingFactor_CB;

if (any(regriddingFactor == 1))
    error('regriddingFactor = 1; data sets cannot have the same resolution');
end

pow_aux = log(regriddingFactor(1)) / log(regriddingFactor(2));
if     ( (pow_aux>1.95) && (pow_aux<2.05) )
    regriddingPower = 2;
elseif ( (pow_aux>2.95) && (pow_aux<3.05) )
    regriddingPower = 3;
else
    error('The order of convergence can only be obtained if (h3/h1) = power((h2/h1),n), ''n'' being integer (and currently it is only supported the n={2,3} cases)');
end
clear pow_aux;

% Convert 'option' to the 'on_grid' string
switch (option)
    case {'L','l'}     % on low-res grid [default]
        on_grid     = sortedABC(3);
        not_on_grid = sortedABC(1:2);
    case {'M','m'}     % on mid-res grid (it implies interpolation)
        on_grid     = sortedABC(2);
        not_on_grid = sortedABC([1,3]);
    case {'H','h'}     % on high-res grid (it implies interpolation)
        on_grid     = sortedABC(1);
        not_on_grid = sortedABC(2:3);
    otherwise          % warning msg and on low-res grid [default]
        warning('IAC3:orderOfConvergence','No valid ''option'' (''L'', ''M'', or ''H'') selected. Using the default one (''L''): downsample the high-res result into the low-res grid.');
        on_grid     = sortedABC(3);
        not_on_grid = sortedABC(1:2);
end

% Save complementary information
output.richardsonExtrapolated = true;
cmd1  = sprintf('output.dump_info_highRes = dump_%s;', sortedABC(1));          eval(cmd1);    clear cmd1;
cmd2  = sprintf('output.dump_info_midRes  = dump_%s;', sortedABC(2));          eval(cmd2);    clear cmd2;
cmd3  = sprintf('output.dump_info_lowRes  = dump_%s;', sortedABC(3));          eval(cmd3);    clear cmd3;
%
cmd4  = sprintf('output.timeStepOutput = data_%s.timeStepOutput;', on_grid);   eval(cmd4);    clear cmd4;
cmd5  = sprintf('output.timeStepSim    = data_%s.timeStepSim;', on_grid);      eval(cmd5);    clear cmd5;
cmd6  = sprintf('output.time           = data_%s.time;', on_grid);             eval(cmd6);    clear cmd6;
cmd7  = sprintf('output.dtOutput       = data_%s.dtOutput;', on_grid);         eval(cmd7);    clear cmd7;
cmd8  = sprintf('output.dtSim          = data_%s.dtSim;', on_grid);            eval(cmd8);    clear cmd8;
cmd9  = sprintf('output.dxVec          = data_%s.dxVec;', on_grid);            eval(cmd9);    clear cmd9;
cmd10 = sprintf('output.xGrid          = data_%s.xGrid;', on_grid);            eval(cmd10);   clear cmd10;
cmd11 = sprintf('output.yGrid          = data_%s.yGrid;', on_grid);            eval(cmd11);   clear cmd11;
cmd12 = sprintf('output.zGrid          = data_%s.zGrid;', on_grid);            eval(cmd12);   clear cmd12;

% Start loop over each (shared) variable
for iVar = 1:size(shrVars_A,2)
    varName = dump_A.varNames{shrVars_A(iVar)};
    
    % Compute variable over the same grid
    cmd1 = sprintf('var_%s  = data_%s.%s;', on_grid, on_grid, varName);   eval(cmd1);   clear cmd1;
    switch (nDims)
        case 1
            cmd2 = sprintf('var_%s = interp1(data_%s.xGrid'', data_%s.%s'', data_%s.xGrid'')'';', not_on_grid(1),not_on_grid(1),not_on_grid(1), varName, on_grid);   eval(cmd2);   clear cmd2;
            cmd3 = sprintf('var_%s = interp1(data_%s.xGrid'', data_%s.%s'', data_%s.xGrid'')'';', not_on_grid(2),not_on_grid(2),not_on_grid(2), varName, on_grid);   eval(cmd3);   clear cmd3;
        case 2
            cmd2 = sprintf('var_%s = interp2(data_%s.xGrid'', data_%s.yGrid'', data_%s.%s'', data_%s.xGrid'', data_%s.yGrid'')'';', not_on_grid(1),not_on_grid(1),not_on_grid(1),not_on_grid(1), varName, on_grid,on_grid);   eval(cmd2);   clear cmd2;
            cmd3 = sprintf('var_%s = interp2(data_%s.xGrid'', data_%s.yGrid'', data_%s.%s'', data_%s.xGrid'', data_%s.yGrid'')'';', not_on_grid(2),not_on_grid(2),not_on_grid(2),not_on_grid(2), varName, on_grid,on_grid);   eval(cmd3);   clear cmd3;
        case 3
            cmd2 = sprintf('var_%s = interp3(data_%s.xGrid'', data_%s.yGrid'', data_%s.zGrid'', data_%s.%s'', data_%s.xGrid'', data_%s.yGrid'', data_%s.zGrid'')'';', not_on_grid(1),not_on_grid(1),not_on_grid(1),not_on_grid(1),not_on_grid(1), varName, on_grid,on_grid,on_grid);   eval(cmd2);   clear cmd2;
            cmd3 = sprintf('var_%s = interp3(data_%s.xGrid'', data_%s.yGrid'', data_%s.zGrid'', data_%s.%s'', data_%s.xGrid'', data_%s.yGrid'', data_%s.zGrid'')'';', not_on_grid(2),not_on_grid(2),not_on_grid(2),not_on_grid(2),not_on_grid(2), varName, on_grid,on_grid,on_grid);   eval(cmd3);   clear cmd3;
    end
    
    % Compute the OrderOfConvergence, generate (Richardson) extrapolated
    % variable over the grid and compute the Grid Convergence Index, GCI
    Fs = 3;    % safety factor for the GCI
    switch (regriddingPower)
        case 2
            cmd1 = sprintf('c_aux = (var_%s - var_%s) ./ (var_%s - var_%s);', sortedABC(1), sortedABC(2), sortedABC(1), sortedABC(3));   eval (cmd1); clear cmd1;
            cmd2 = sprintf('output.orderOfConvergence.from%s = log( (1-c_aux) ./ c_aux ) / log( regriddingFactor(2) );', varName);   eval(cmd2);   clear cmd2;
            cmd3 = sprintf('output.%s = ( var_%s .* var_%s - var_%s.^2 ) ./ ( var_%s - 2*var_%s + var_%s );', varName, sortedABC(1),sortedABC(3),sortedABC(2), sortedABC(1),sortedABC(2),sortedABC(3));   eval(cmd3);   clear cmd3;
            cmd4 = sprintf('output.GCI.%s = Fs * ( var_%s - var_%s ).^2 ./ ( 2*var_%s - var_%s - var_%s );', varName, sortedABC(2), sortedABC(1), sortedABC(2), sortedABC(1), sortedABC(3));   eval(cmd4);   clear cmd4;
        case 3
            cmd1  = sprintf('c_aux = (var_%s - var_%s) ./ (var_%s - var_%s);', sortedABC(1), sortedABC(2), sortedABC(1), sortedABC(3));   eval (cmd1); clear cmd1;
            z_aux = -1/2 + sqrt(4*c_aux - 3*c_aux.^2) ./ (2.*c_aux);
            cmd2  = sprintf('output.orderOfConvergence.from%s = log(z_aux) / log(regriddingFactor(2));', varName);   eval(cmd2);   clear cmd2;
            cmd3  = sprintf('output.%s = ( z_aux.*var_%s - var_%s ) ./ ( z_aux - 1 );', varName, sortedABC(1), sortedABC(2));   eval(cmd3);   clear cmd3;
            cmd4  = sprintf('output.GCI.%s = Fs * ( var_%s - var_%s ) ./ ( 1 - z_aux );', varName, sortedABC(2), sortedABC(1));   eval(cmd4);   clear cmd4;
    end
    clear c_aux z_aux;
    
    % Clean orderOfConvergence results from NaNs, Infs, Imag and Negative
    % values. At the moment they are *all* replaced by 'NaN's, so we can
    % compute the stats easily avoiding them, and then they will be
    % replaced by '0's.
    cmd1 = sprintf('output.orderOfConvergence.from%s(isnan(output.orderOfConvergence.from%s)) = nan;',   varName, varName);   eval(cmd1);   clear cmd1;
    cmd2 = sprintf('output.orderOfConvergence.from%s(isinf(output.orderOfConvergence.from%s)) = nan;',   varName, varName);   eval(cmd2);   clear cmd2;
    cmd3 = sprintf('output.orderOfConvergence.from%s(imag(output.orderOfConvergence.from%s)~=0) = nan;', varName, varName);   eval(cmd3);   clear cmd3;
    cmd4 = sprintf('output.orderOfConvergence.from%s(output.orderOfConvergence.from%s < 0) = nan;',      varName, varName);   eval(cmd4);   clear cmd4;
end

% Finally, compute some auxiliary extra information from the
% orderOfConvergence maps, in particular their 'mean' and 'median' values.
% [ we shall avoid zeros in the stats, since they represent wrong values ]
cmd = sprintf('cat_aux = output.orderOfConvergence.from%s;', dump_A.varNames{shrVars_A(1)});   eval(cmd);   clear cmd;
for iVar = 2:size(shrVars_A,2)
    cmd = sprintf('cat_aux = cat(nDims+1, cat_aux, output.orderOfConvergence.from%s);', dump_A.varNames{shrVars_A(iVar)});   eval(cmd);   clear cmd;
end
output.orderOfConvergence.meanMap   = nanmean(cat_aux, nDims+1);
output.orderOfConvergence.medianMap = nanmedian(cat_aux, nDims+1);
output.orderOfConvergence.stdMap    = nanstd(cat_aux, 0, nDims+1);
%
output.orderOfConvergence.mean   = nanmean(reshape(cat_aux,1,[]));
output.orderOfConvergence.median = nanmedian(reshape(cat_aux,1,[]));
output.orderOfConvergence.std    = nanstd(reshape(cat_aux,1,[]));
clear cat_aux;

% After the stats, replace 'NaN's by '0's in the 'orderOfConvergence'
% results
for iVar = 1:size(shrVars_A,2)
    cmd = sprintf('output.orderOfConvergence.from%s(isnan(output.orderOfConvergence.from%s)) = 0;',   dump_A.varNames{shrVars_A(iVar)}, dump_A.varNames{shrVars_A(iVar)});   eval(cmd);   clear cmd;
end