function [output] = richardsonExtrapolation(input_lowRes, input_highRes, timeRef_input, orderOfConvergence, option)

% richardsonExtrapolation (08/02/2012 by M.Trias, IAC3)
%
% Performs Richardson Extrapolation by combining a low-res and a high-res
% result at a given time value, and returns a data structure (like the
% output of 'loadVisitData.m') with all the variables evaluated at the
% chosen grid.
%
% INPUT:  input_lowRes -- low-resolution data set (it can be either a char
%                        string or the structure with all dump_info)
%         input_highRes -- high-resolution data set (same comment as prev.)
%         timeRef_input -- time value (in seconds) where to perform the
%                         extrapolation. The function will look for the
%                         nearest (shared) timeStep available in the input
%         orderOfConvergence -- of the numerical method
%         option -- optional argument that indicates over which grid, the
%                  low-res one ('L') or the high-res one ('H'), the
%                  extrapolated variables must be computed. Notice that 'H'
%                  implies an interpolation of the low-res data 
%                  [default: 'L']
%
% OUTPUT: output -- data structure with all the required information and
%                  variables. It contains the same information as the
%                  output of 'loadVisitData.m', plus some extra information
%                  about the original data used in the extrapolation.
%
% ·········································································

% Check number of inputs.
if (nargin > 5)
    error('Too many inputs');
end
% Fill in unset optional values with defaults.
switch nargin
    case 4
        option = 'L';
end
% -------------------------------------------------------------------------

% Get available timeSteps and timeValues in each data set (here we also get
% the dump information of both data sets)
[avTimeValues_lowRes, dump_lowRes]   = loadVisitTimeSteps(input_lowRes);
[avTimeValues_highRes, dump_highRes] = loadVisitTimeSteps(input_highRes);

% Make a list of shared variables (the ones that shall be extrapolated) and
% give an error in case there is none
shrVars_lowRes  = [];
shrVars_highRes = [];
for iVar = 1:dump_lowRes.nVars
    strcmp_aux = strcmp(dump_highRes.varNames, dump_lowRes.varNames{iVar});
    if (any(strcmp_aux))
        shrVars_lowRes  = [shrVars_lowRes  iVar];
        shrVars_highRes = [shrVars_highRes find(strcmp_aux,1,'first')];
    end
    clear strcmp_aux;
end

if (isempty(shrVars_lowRes))
    error('There are no shared variables between the two input data sets.');
end

% Compare the two arrays of available timeValues and look for the shared
% ones (we do it in a fancy but efficient way)
tol = 1e-8*max( max(abs(avTimeValues_lowRes)), max(abs(avTimeValues_highRes)) );
shrInd_aux     = find( abs(avTimeValues_lowRes'*ones(size(avTimeValues_highRes)) - ones(size(avTimeValues_lowRes'))*avTimeValues_highRes) < tol );
shrInd_lowRes  = mod(shrInd_aux-1,size(avTimeValues_lowRes,2))+1;
shrInd_highRes = ceil(shrInd_aux/size(avTimeValues_lowRes,2));
shrTimeValues  = avTimeValues_lowRes(shrInd_lowRes);  % also possible using 
                                                      % highRes results
clear tol shrInd_aux; 

% Given 'timeRef_input', look for the closest 'shared' time value available
index_nearest = interp1(shrTimeValues,1:size(shrTimeValues,2),timeRef_input,'nearest');

timeStep_lowRes  = shrInd_lowRes(index_nearest);
timeStep_highRes = shrInd_highRes(index_nearest);

timeRef = avTimeValues_lowRes(shrInd_lowRes(index_nearest)); % also possible using 
                                                             % highRes results
if ( abs(timeRef-timeRef_input) > 5/100*abs(avTimeValues_lowRes(end)-avTimeValues_lowRes(1)) )
    warning('IAC3:richardsonExtrapolation','Nearest available time value (%.2f)  differs by more than 5%% of the input one (%.2f)', timeRef, timeRef_input);
end

% Load actual data at the corresponding timeStep's
[data_lowRes]  = loadVisitData(dump_lowRes, timeStep_lowRes);
[data_highRes] = loadVisitData(dump_highRes, timeStep_highRes);

% Get the non-zero resolution values and check consistency on regridding
% factors in each dimension
dxVec_lowRes      = data_lowRes.dxVec(data_lowRes.dxVec~=0);
dxVec_highRes     = data_highRes.dxVec(data_highRes.dxVec~=0);
regriddingFactor  = dxVec_lowRes./dxVec_highRes;
nDims             = size(regriddingFactor,1);
if ((nDims < 1) || (nDims > 3))
    error('richarsonExtrapolation function only supports 1D, 2D and 3D data.');
end
clear dxVec_lowRes dxVec_highRes;

if (any(diff(regriddingFactor)) ~= 0)
    error('The regridding factor is not the same in all dimensions.');
else
    regriddingFactor = regriddingFactor(1);
end

if (regriddingFactor == 1)
    error('regriddingFactor = 1; data sets cannot have the same resolution');
end

if (regriddingFactor < 1)
    warning('IAC3:richardsonExtrapolation','The input ''low-res'' and ''high-res'' datasets were wrong-ordered. It has been automatically corrected,');
    dump_aux        = dump_lowRes;
    data_aux        = data_lowRes;
    shrVars_aux     = shrVars_lowRes;
    
    dump_lowRes     = dump_highRes;
    data_lowRes     = data_highRes;
    shrVars_lowRes  = shrVars_highRes;
    %
    dump_highRes    = dump_aux;
    data_highRes    = data_aux;
    shrVars_highRes = shrVars_aux;
    %
    clear dump_aux data_aux shrVars_aux;
    
    regriddingFactor = 1/regriddingFactor;
end

% Convert 'option' to boolean 'on_lowRes_grid' variable
switch (option)
    case {'L','l'}     % on low-res grid [default]
        on_lowRes_grid = 1;
    case {'H','h'}     % on high-res grid (it implies interpolation)
        on_lowRes_grid = 0;
    otherwise
        warning('IAC3:richardsonExtrapolation','No valid ''option'' (''L'' or ''H'') selected. Using the default one (''L''): downsample the high-res result into the low-res grid.');
        on_lowRes_grid = 1;
end

% Save complementary information
output.richardsonExtrapolated = true;
output.dump_info_lowRes       = dump_lowRes;
output.dump_info_highRes      = dump_highRes;

if (on_lowRes_grid)
    output.timeStepOutput = data_lowRes.timeStepOutput;
    output.timeStepSim    = data_lowRes.timeStepSim;
    output.time           = data_lowRes.time;
    output.dtOutput       = data_lowRes.dtOutput;
    output.dtSim          = data_lowRes.dtSim;
    output.dxVec          = data_lowRes.dxVec;
    output.xGrid          = data_lowRes.xGrid;
    output.yGrid          = data_lowRes.yGrid;
    output.zGrid          = data_lowRes.zGrid;
else
    output.timeStepOutput = data_highRes.timeStepOutput;
    output.timeStepSim    = data_highRes.timeStepSim;
    output.time           = data_highRes.time;
    output.dtOutput       = data_highRes.dtOutput;
    output.dtSim          = data_highRes.dtSim;
    output.dxVec          = data_highRes.dxVec;
    output.xGrid          = data_highRes.xGrid;
    output.yGrid          = data_highRes.yGrid;
    output.zGrid          = data_highRes.zGrid;
end

% Start loop over each (shared) variable
for iVar = 1:size(shrVars_lowRes,2)
    varName = dump_lowRes.varNames{shrVars_lowRes(iVar)};
    
    % Compute variable over the same grid
    if (on_lowRes_grid)
        cmd1 = sprintf('var_lowRes  = data_lowRes.%s;', varName);
        switch (nDims)
            case 1
                cmd2 = sprintf('var_highRes = interp1(data_highRes.xGrid'', data_highRes.%s'', data_lowRes.xGrid'')'';', varName);
            case 2
                cmd2 = sprintf('var_highRes = interp2(data_highRes.xGrid'', data_highRes.yGrid'', data_highRes.%s'', data_lowRes.xGrid'', data_lowRes.yGrid'')'';', varName);
            case 3
                cmd2 = sprintf('var_highRes = interp3(data_highRes.xGrid'', data_highRes.yGrid'', data_highRes.zGrid'', data_highRes.%s'', data_lowRes.xGrid'', data_lowRes.yGrid'', data_lowRes.zGrid'')'';', varName);
        end
    else
        cmd1 = sprintf('var_highRes = data_highRes.%s;', varName);
        switch (nDims)
            case 1
                cmd2 = sprintf('var_lowRes  = interp1(data_lowRes.xGrid'', data_lowRes.%s'', data_highRes.xGrid'', ''spline'')'';', varName);
            case 2
                cmd2 = sprintf('var_lowRes  = interp2(data_lowRes.xGrid'', data_lowRes.yGrid'', data_lowRes.%s'', data_highRes.xGrid'', data_highRes.yGrid'', ''spline'')'';', varName);
            case 3
                cmd2 = sprintf('var_lowRes  = interp3(data_lowRes.xGrid'', data_lowRes.yGrid'', data_lowRes.zGrid'', data_lowRes.%s'', data_highRes.xGrid'', data_highRes.yGrid'', data_highRes.zGrid'', ''spline'')'';', varName);
        end
    end
    eval(cmd1);   eval(cmd2);   clear cmd1 cmd2;
    
    % Generate (Richardson) extrapolated variable over the grid
    cmd = sprintf('output.%s = (power(regriddingFactor, orderOfConvergence)*var_highRes - var_lowRes) / (power(regriddingFactor, orderOfConvergence)-1);', varName);
    eval(cmd);   clear cmd;
    
    % Compute the GCI (Grid Convergence Index) corresponding to the finer
    % grid
    Fs = 3;    % safety factor
    cmd = sprintf('output.GCI.%s = Fs * (var_lowRes - var_highRes) / (1 - power(regriddingFactor, orderOfConvergence));', varName);
    eval(cmd);   clear cmd;
end
