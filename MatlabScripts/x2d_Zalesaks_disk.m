Cx     = 5;
Cy     = 7.5;
R      = 1.5;
w      = 0.5;
length = 2.5;
rotationCx = 5.0;
rotationCy = 5.0;
%
Npts = 100;

Npts = max(2, Npts);
alpha = asin((w/2)/R);

% Static disk
pts(1,1) = Cx - w/2;
pts(1,2) = Cy + length-R;

pts(2,1) = Cx + w/2;
pts(2,2) = Cy + length-R;

angles = linspace(-pi/2+alpha, 3*pi/2-alpha, Npts-2);

for i=1:size(angles,2)
    pts(i+2,1) = Cx + R*cos(angles(i));
    pts(i+2,2) = Cy + R*sin(angles(i));
end

fid = fopen('/home/mtrias/Escritorio/x2d_Zalesaks_disk_static.txt', 'w');
fprintf(fid, '{');
for i=1:size(pts,1)-1
    fprintf(fid, '{%6.3f,%6.3f}, ', pts(i,1), pts(i,2));
end
    i=size(pts,1);
    fprintf(fid, '{%6.3f,%6.3f}} \n', pts(i,1), pts(i,2));
fclose(fid);


% Rotating disk (taking the static one as initial condition)

ang_rtd = linspace(0,2*pi,26);

fid = fopen('/home/mtrias/Escritorio/x2d_Zalesaks_disk_rotating.txt', 'w');
fprintf(fid, '{');
for iang=1:size(ang_rtd,2)-1
    fprintf(fid, '{');
    for i=1:size(pts,1)-1
        pts_rtd(i,1) = rotationCx + cos(ang_rtd(iang))*(pts(i,1)-rotationCx) - sin(ang_rtd(iang))*(pts(i,2)-rotationCy);
        pts_rtd(i,2) = rotationCy + sin(ang_rtd(iang))*(pts(i,1)-rotationCx) + cos(ang_rtd(iang))*(pts(i,2)-rotationCy);
        fprintf(fid, '{%6.3f,%6.3f}, ', pts_rtd(i,1), pts_rtd(i,2));
    end
    %
        i=size(pts,1);
        pts_rtd(i,1) = rotationCx + cos(ang_rtd(iang))*(pts(i,1)-rotationCx) - sin(ang_rtd(iang))*(pts(i,2)-rotationCy);
        pts_rtd(i,2) = rotationCy + sin(ang_rtd(iang))*(pts(i,1)-rotationCx) + cos(ang_rtd(iang))*(pts(i,2)-rotationCy);
        fprintf(fid, '{%6.3f,%6.3f}} , \n ', pts_rtd(i,1), pts_rtd(i,2));
end
%
    iang=size(ang_rtd,2);
    fprintf(fid, '{');
    for i=1:size(pts,1)-1
        pts_rtd(i,1) = rotationCx + cos(ang_rtd(iang))*(pts(i,1)-rotationCx) - sin(ang_rtd(iang))*(pts(i,2)-rotationCy);
        pts_rtd(i,2) = rotationCy + sin(ang_rtd(iang))*(pts(i,1)-rotationCx) + cos(ang_rtd(iang))*(pts(i,2)-rotationCy);
        fprintf(fid, '{%6.3f,%6.3f}, ', pts_rtd(i,1), pts_rtd(i,2));
    end
    %
        i=size(pts,1);
        pts_rtd(i,1) = rotationCx + cos(ang_rtd(iang))*(pts(i,1)-rotationCx) - sin(ang_rtd(iang))*(pts(i,2)-rotationCy);
        pts_rtd(i,2) = rotationCy + sin(ang_rtd(iang))*(pts(i,1)-rotationCx) + cos(ang_rtd(iang))*(pts(i,2)-rotationCy);
        fprintf(fid, '{%6.3f,%6.3f}}} \n', pts_rtd(i,1), pts_rtd(i,2));
fclose(fid);
