Cx = 5;
Cy = 7.5;
R  = 1.5;
%
Npts = 100;

angles = linspace(0,2*pi,Npts+1);
angles(end)=[];

for i=1:size(angles,2)
    pts(i,1) = Cx + R*cos(angles(i));
    pts(i,2) = Cy + R*sin(angles(i));
end

fid = fopen('/home/mtrias/Escritorio/x2d_circle.txt', 'w');
fprintf(fid, '{');
for i=1:size(pts,1)-1
    fprintf(fid, '{%6.3f,%6.3f}, ', pts(i,1), pts(i,2));
end
    i=size(pts,1);
    fprintf(fid, '{%6.3f,%6.3f}} \n', pts(i,1), pts(i,2));
fclose(fid);
