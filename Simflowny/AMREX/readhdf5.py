import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

def writefields2D(folder, time, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes'][0]
    print("Box",boxes)

    size = [(boxes[2] - boxes[0]) + 2 ,(boxes[3] - boxes[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))
    print("Size",size)
    linear_size = size[0] * size[1]
    print("Linear size",linear_size)

    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            if value.decode("utf-8") == field:
                variable_offset = int(name[len('component_')])
                print("Variable offset ",variable_offset)
                tmp = level_data['data:datatype=0'][linear_size*variable_offset:linear_size*variable_offset+linear_size]
                tmp = np.reshape(tmp, (size[0],size[1]))
                data[:,:] = tmp[:,:]

                #Writing the variable to disk
                f = h5py.File(folder + '/' + field + '_' + str(level) + '_t' + str(time) + '.hdf5', "w")
                dset = f.create_dataset(field, data.shape, dtype='float64')
                dset[:,:] = data[:,:]
                f.close()
                f_sum.close()
                return

def writefields3D(folder, time, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes'][0]
    print("Box",boxes)

    size = [(boxes[3] - boxes[0]) + 2,(boxes[4] - boxes[1]) + 2,(boxes[5] - boxes[2]) + 2]
    data = np.zeros(shape=(size[0], size[1], size[2]))
    print("Size",size)
    linear_size = size[0] * size[1] * size[2]
    print("Linear size",linear_size)

    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            if value.decode("utf-8") == field:
                variable_offset = int(name[len('component_'):])
                print("Variable offset ",variable_offset)
                tmp = level_data['data:datatype=0'][linear_size*variable_offset:linear_size*variable_offset+linear_size]
                tmp = np.reshape(tmp, (size[0],size[1],size[2]))
                data[:,:,:] = tmp[:,:,:]

                #Writing the variable to disk
                f = h5py.File(folder + '/' + field + '_' + str(level) + '_t' + str(time) + '.hdf5', "w")
                dset = f.create_dataset(field, data.shape, dtype='float64')
                dset[:,:] = data[:,:]
                f.close()
                f_sum.close()
                return


def main():
    
    for t in range(0, 1, 1):
        writefields2D("./plt_slice", t, "Alpha", 0)

    for t in range(0, 1, 1):
        writefields2D("./plt_sphere", t, "psi4I", 0)

    for t in range(0, 1, 1):
        writefields3D("./plt_full", t, "chi", 1)

if __name__ == "__main__":
    main()
