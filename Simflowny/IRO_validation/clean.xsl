<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mt="http://www.w3.org/1998/Math/MathML" version="2.0">

    <!-- Meta information -->
    <xsl:template match="//mt:math">
        <mt:math>
         <xsl:apply-templates select="./*"/>
        </mt:math>
    </xsl:template>
        
    <!-- Develop power of derivatives -->
    <xsl:template match="//mt:apply[mt:power and mt:exponentiale]">
        <mt:apply>
            <mt:exp/>
                <xsl:apply-templates select="./*[3]"/>
        </mt:apply>
    </xsl:template>

<xsl:template match="mt:cn[ starts-with(text(), '-')]">
    <mt:apply>
        <mt:minus/>
        <mt:cn><xsl:value-of select=" substring(., 2)"></xsl:value-of></mt:cn>
    </mt:apply>
</xsl:template>

    <xsl:template match="mt:cn[mt:sep]">
        <mt:apply>
            <mt:divide/>
            <mt:cn><xsl:value-of select="./text()[1]"></xsl:value-of></mt:cn>
            <mt:cn><xsl:value-of select="./text()[2]"></xsl:value-of></mt:cn>
        </mt:apply>
    </xsl:template>

    <!-- Recursive copy -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- Namespace prefix -->
    <xsl:template match="*">
        <xsl:element name="mt:{name()}" namespace="http://www.w3.org/1998/Math/MathML">
            <xsl:copy-of select="namespace::*"/>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
