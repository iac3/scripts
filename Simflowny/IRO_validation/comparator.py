from sympy import *
from sympy.parsing.sympy_parser import parse_expr

phi_function =  parse_expr('-(A1*exp((-A7 + x)**2/A8**2)*Cos(A5 + A4*x)*Sin(A3 + A2*x))')
pi_function =  parse_expr('(-2*B1*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*Cos(B7 + B6*z))/B3**2 - (2*B1*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*Cos(B7 + B6*z))/B5**2 + B1*B6**2*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*Cos(B7 + B6*z) - (4*B1*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*(-B2 + x)**2*Cos(B7 + B6*z))/B3**4 - (4*B1*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*(-B4 + y)**2*Cos(B7 + B6*z))/B5**4')


phi_initial_data =  parse_expr('B1*exp((-B2 + x)**2/B3**2 + (-B4 + y)**2/B5**2)*Cos(B7 + B6*z)')
pi_initial_data =  parse_expr('A1*exp((-A7 + x)**2/A8**2)*Cos(A5 + A4*x)*Sin(A3 + A2*x)')


x = Symbol('x')
A1 = Symbol('A1')

print N(phi_function).subs(x,4.7).subs(A1,5.7).evalf()
