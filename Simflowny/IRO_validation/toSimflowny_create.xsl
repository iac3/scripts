<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms" xmlns:sml="urn:simml" version="2.0">
  <xsl:template match="mt:math">
     <mms:physicalModel>
         <mms:head>
             <mms:name/>
             <mms:id/>
             <mms:author/>
             <mms:version/>
             <mms:date><xsl:value-of  select="format-date(current-date(), '[Y0001]-[M01]-[D01]')"/></mms:date>
         </mms:head>
         <mms:coordinates>
             <mms:spatialCoordinates>
                 <xsl:for-each select="meta/coord">
                     <mms:spatialCoordinate><xsl:value-of select="./text()"></xsl:value-of></mms:spatialCoordinate>
                 </xsl:for-each>
             </mms:spatialCoordinates>
             <mms:timeCoordinate>t</mms:timeCoordinate>
         </mms:coordinates>
         <mms:fields>
             <xsl:for-each select="//mt:apply[mt:eq]/*[position() = 2 and descendant-or-self::mt:partialdiff]/descendant-or-self::mt:ci[@type = 'variable']">
                <mms:field><xsl:apply-templates select="./text()"/></mms:field>
             </xsl:for-each>
         </mms:fields>
         <xsl:if test="count(//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'variable']) > 0">
            <mms:auxiliaryFields>
                <xsl:for-each select="//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'variable']">
                    <mms:auxiliaryField><xsl:apply-templates select="./text()"/></mms:auxiliaryField>
                </xsl:for-each>
            </mms:auxiliaryFields>
         </xsl:if>
         <xsl:if test="count(//mt:apply[mt:eq]/descendant-or-self::mt:ci[not(@type = 'variable')]) > 0">
             <mms:parameters>
                 <xsl:for-each select="//mt:apply[mt:eq]/descendant-or-self::mt:ci[not(@type = 'variable' or parent::mt:bvar)]">
                     <xsl:if test ="not(preceding::mt:apply[mt:eq]/descendant-or-self::mt:ci[not(@type = 'variable' or parent::mt:bvar)][text() = current()/text()])">
                       <mms:parameter>
                           <mms:name><xsl:apply-templates select="./text()"/></mms:name>
                           <mms:type>REAL</mms:type>
                       </mms:parameter>
                     </xsl:if>
                 </xsl:for-each>
             </mms:parameters>
         </xsl:if>
         <mms:equationSet>
             <mms:evolutionEquations>
                 <xsl:for-each select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']]">
                    <mms:evolutionEquation>
                     <xsl:variable name="rhs" select="./*[position() = 3]"></xsl:variable>
                     <xsl:variable name="rhs_terms" as="element()+">
                         <xsl:call-template name="separateTerms">
                             <xsl:with-param name="rhs" select="$rhs"/>
                         </xsl:call-template>
                     </xsl:variable>
                     <mms:equationName/>
                     <mms:canonicalPDE>
                         <mms:field><xsl:value-of select=".//mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']/mt:ci"></xsl:value-of></mms:field>
                         <xsl:if test="$rhs_terms[descendant::mt:partialdiff]">
                           <mms:fluxes>
                               <xsl:for-each select="$rhs_terms">
                                   <xsl:if test=".[descendant::mt:partialdiff]">
                                       <mms:flux>
                                           <mms:mathML>
                                               <mt:math>
                                                   <xsl:apply-templates select="."/>
                                                   <!--   <xsl:copy-of select="."/> -->
                                               </mt:math>
                                           </mms:mathML>
                                           <mms:coordinate><xsl:value-of select=".//mt:bvar/mt:ci/text()"></xsl:value-of></mms:coordinate>
                                       </mms:flux>
                                   </xsl:if>
                               </xsl:for-each>
                           </mms:fluxes>
                         </xsl:if>
                         <mms:source>
                             <mms:mathML>
                                 <mt:math>
                                     <xsl:choose>
                                         <!-- Select the sources -->
                                         <xsl:when test="count($rhs_terms[not(descendant::mt:partialdiff)]) > 0">
                                             <xsl:apply-templates select="$rhs_terms[not(descendant::mt:partialdiff)]"/>
                                         </xsl:when>
                                         <!-- Set 0 if no sources provided -->
                                         <xsl:otherwise>
                                             <mt:cn>0</mt:cn>
                                         </xsl:otherwise>
                                     </xsl:choose>
                                 </mt:math>
                             </mms:mathML>
                         </mms:source>
                     </mms:canonicalPDE>
                    </mms:evolutionEquation>
                 </xsl:for-each>
             </mms:evolutionEquations>
             <xsl:if test="//mt:apply[*[position()= 1 and local-name() = 'eq'] and not(descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't'])]">
                 <mms:auxiliaryEquations>
                     <xsl:for-each select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and not(descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't'])]">
                        <mms:auxiliaryEquation>
                            <mms:equationName/>
                            <mms:auxiliaryEquationData>
                                <mms:auxiliaryField><xsl:value-of select="./*[position() = 2]"></xsl:value-of></mms:auxiliaryField>
                                <mms:instructionSet>
                                    <sml:simml>
                                        <mt:math>
                                            <xsl:apply-templates select="./*[position() = 3]"/>
                                        </mt:math>
                                    </sml:simml>
                                </mms:instructionSet>
                            </mms:auxiliaryEquationData>
                        </mms:auxiliaryEquation>
                     </xsl:for-each>
                 </mms:auxiliaryEquations>
             </xsl:if>
         </mms:equationSet>
     </mms:physicalModel>
  </xsl:template>
    
    <!-- Recursive copy -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    
    <!-- Removes the partialdiff -->
    <xsl:template match="mt:apply[mt:partialdiff]">
        <xsl:apply-templates select="./*[3]"/>
    </xsl:template>
    
    <!-- separate terms in a RHS -->
    <xsl:template name="separateTerms">
        <xsl:param name="rhs"/>
        <xsl:choose>
            <!-- Only one term without derivatives -->
            <xsl:when test="$rhs[not(descendant::mt:partialdiff)]">
                <xsl:copy-of select="$rhs"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <!-- Only one term with derivatives -->
                    <xsl:when test="$rhs[mt:partialdiff or count(*) = 2]">
                        <xsl:copy-of select="$rhs"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:for-each select="$rhs/mt:apply/*[position() > 1]">
                            <xsl:copy-of select="."/>
                        </xsl:for-each>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
