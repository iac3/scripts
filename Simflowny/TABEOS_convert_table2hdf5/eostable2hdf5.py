import sys
import os
import numpy as np
import codecs
import h5py

output = "Data.h5"
output_init = "InitialData.h5"
output_leakage = "DataLeakage.h5"
output_nulib = "DataNulib.h5"

hdf = h5py.File(output, mode='w')
hdf_init = h5py.File(output_init, mode='w')
hdf_leakage = h5py.File(output_leakage, mode='w')
hdf_nulib = h5py.File(output_nulib, mode='w')

RHOGF = 1.619679366684996e-18
PRESSGF = 1.80213633813095e-39
EPSGF = 1.11265005605362e-21


print(" # of arguments = ",len(sys.argv))
if (len(sys.argv)<2):
    print("Usage: ",sys.argv[0]," <EOStable HDF5 file> [<Nulib HDF5 file>]")
    print(" ")
    print("Output: ",output)
    print("Output for initial data: ",output_init)
    print("Output for leakage: ",output_leakage)
    print("Output for Nulib: ",output_nulib)
    sys.exit()
infile   = sys.argv[1]

if not os.path.isfile(infile):
    print('file %s not found' % infile)
    sys.exit()
if not os.access(infile, os.R_OK):
    print('file %s not readable' % infile)
    sys.exit()

eostable = h5py.File(infile, mode='r')

fields = ['cs2', 'logenergy', 'logpress', 'entropy']
fields_init = ['cs2', 'logenergy', 'logpress', 'mu_e', 'mu_p', 'mu_n']
fields_leakage = ['mu_e', 'mu_p', 'mu_n', 'Xa', 'Xh', 'Xn', 'Xp', 'Abar', 'Zbar', 'entropy']
fields_nulib = ['emissivities', 'absorption_opacity', 'scattering_opacity', 'number_emissivities']

coords = ['logrho', 'logtemp','ye']
coords_nulib = ['rho', 'temp','ye']

nvars = 0
for coord in coords:
    data = np.asarray(eostable[coord])
    if coord == 'logrho':
        data = data * np.log(10.) + np.log(RHOGF);
    if coord == 'logtemp':
        data = data*np.log(10.0);
    dset = hdf.require_dataset('coord' + str(nvars), data.shape, dtype='d')
    dset[:] = data[:]
    dset = hdf_init.require_dataset('coord' + str(nvars), data.shape, dtype='d')
    dset[:] = data[:]
    dset = hdf_leakage.require_dataset('coord' + str(nvars), data.shape, dtype='d')
    dset[:] = data[:]
    nvars = nvars + 1

nvars = 0
for field in fields:
    data = np.asarray(eostable[field])
    if field == 'logpress':
        data = data * np.log(10.0) + np.log(PRESSGF);
    if field == 'logenergy':
        data = data * np.log(10.0) + np.log(EPSGF);
        epsmin = np.exp(data).min()
    if field == 'cs2':
        data = data * EPSGF;
    dset = hdf.require_dataset('var' + str(nvars), data.shape, dtype='d')
    dset[:] = data[:]
    nvars = nvars + 1

nvars_init = 0
for field in fields_init:
    data = np.asarray(eostable[field])
    if field == 'logpress':
        data = data * np.log(10.0) + np.log(PRESSGF);
    if field == 'logenergy':
        data = data * np.log(10.0) + np.log(EPSGF);
        epsmin = np.exp(data).min()
    if field == 'cs2':
        data = data * EPSGF;
    dset = hdf_init.require_dataset('var' + str(nvars_init), data.shape, dtype='d')
    dset[:] = data[:]
    nvars_init = nvars_init + 1

nvars_leakage = 0
for field in fields_leakage:
    data = np.asarray(eostable[field])
    dset = hdf_leakage.require_dataset('var' + str(nvars_leakage), data.shape, dtype='d')
    #dset = hdf_leakage.require_dataset('var' + str(nvars_init), data.shape, dtype='d')
    dset[:] = data[:]
    nvars_leakage = nvars_leakage + 1

energy_shift = np.asarray(eostable['energy_shift']) * EPSGF
print("The minimum energy associated with this EOS is: ", epsmin - energy_shift)
print("The energy_shift associated with this EOS is: ",energy_shift)
print("These values should be entered in your parameter file")
dset = hdf.require_dataset('nvars', (1,), dtype='f')
dset[:] = nvars
hdf.close()
dset = hdf_init.require_dataset('nvars', (1,), dtype='f')
dset[:] = nvars_init
hdf_init.close()
dset = hdf_leakage.require_dataset('nvars', (1,), dtype='f')
dset[:] = nvars_leakage
hdf_leakage.close()
eostable.close()

#Nulib table read
if (len(sys.argv) >= 3):
    infile_nulib   = sys.argv[2]
    if not os.path.isfile(infile_nulib):
        print('file %s not found' % infile_nulib)
        sys.exit()
    if not os.access(infile_nulib, os.R_OK):
        print('file %s not readable' % infile_nulib)
        sys.exit()

    eostable_nulib = h5py.File(infile_nulib, mode='r')

    nvars = 0
    for coord in coords_nulib:
        data = np.asarray(eostable_nulib[coord])
        dset = hdf_nulib.require_dataset('coord' + str(nvars), data.shape, dtype='d')
        dset[:] = data[:]
        nvars = nvars + 1

    nvars_nulib = 0
    for field in fields_nulib:
        data = np.asarray(eostable_nulib[field])
        dset = hdf_nulib.require_dataset('var' + str(nvars_nulib), data.shape, dtype='d')
        dset[:] = data[:]
        nvars_nulib = nvars_nulib + 1
    dset = hdf_nulib.require_dataset('nvars', (1,), dtype='f')
    dset[:] = nvars_nulib
    hdf_nulib.close()


