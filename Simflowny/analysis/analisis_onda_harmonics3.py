import sys, getopt
import os
import numpy as np
import h5py
from scipy import integrate
import math

def readfield(folder, field):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data

def surface_integral2(samples, h_theta, h_phi):
    integral = 0.0
  
    iphi = 0
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi]
     
    iphi = samples.shape[1] 
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta 
    integral = integral + np.sin(phi)*samples[itheta-1, iphi-1]

    iphi = 0
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta-1, iphi]

    iphi = samples.shape[1] 
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi-1]
    
    for iphi in xrange(1, 80):
        phi = iphi*h_phi
        for itheta in xrange(1, 160):
            theta = itheta*h_theta 
            integral = integral + 4.0*np.sin(phi)*samples[itheta, iphi]


    iphi = 0
    phi = iphi*h_phi
    for itheta in xrange(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]

    iphi = samples.shape[1] 
    phi = iphi*h_phi
    for itheta in xrange(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi-1]

    itheta = 0
    theta = itheta*h_theta  
    for iphi in xrange(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]       

    itheta = samples.shape[0] 
    theta = itheta*h_theta    
    for iphi in xrange(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta-1, iphi]           

    return 0.25*h_theta*h_phi*integral
      
def calculate(sphere, rext, hitwithr):
    dt = 1.5
    delta_output = 1
    massI = 6.2514407847291178
    time_min = 0.0
    factor  = 1.0
    resolution_theta = 161
    resolution_phi = 81
    rfactorhit = 1.0
    if hitwithr > 0:
       print 'will hit psi4 with the extraction radius'
       rfactorhit = rext

    times = xrange(442)

    f110 = open(sphere + '/c2m2.dat', 'w')
    f111 = open(sphere + '/c2m1.dat', 'w')
    f112 = open(sphere + '/c20.dat', 'w')
    f113 = open(sphere + '/c2p1.dat', 'w')
    f114 = open(sphere + '/c2p2.dat', 'w')

    f210 = open(sphere + '/c3m3.dat', 'w')
    f211 = open(sphere + '/c3m2.dat', 'w')
    f212 = open(sphere + '/c3m1.dat', 'w')
    f213 = open(sphere + '/c30.dat', 'w')
    f214 = open(sphere + '/c3p1.dat', 'w')
    f215 = open(sphere + '/c3p2.dat', 'w')
    f216 = open(sphere + '/c3p3.dat', 'w')

    f220 = open(sphere + '/c4m4.dat', 'w')
    f221 = open(sphere + '/c4m3.dat', 'w')
    f222 = open(sphere + '/c4m2.dat', 'w')
    f223 = open(sphere + '/c4m1.dat', 'w')
    f224 = open(sphere + '/c40.dat', 'w')
    f225 = open(sphere + '/c4p1.dat', 'w')
    f226 = open(sphere + '/c4p2.dat', 'w')
    f227 = open(sphere + '/c4p3.dat', 'w')
    f228 = open(sphere + '/c4p4.dat', 'w')

    f310 = open(sphere + '/c2m2_inf.dat', 'w')
    f311 = open(sphere + '/c2m1_inf.dat', 'w')
    f312 = open(sphere + '/c20_inf.dat', 'w')
    f313 = open(sphere + '/c2p1_inf.dat', 'w')
    f314 = open(sphere + '/c2p2_inf.dat', 'w')

    f320 = open(sphere + '/c3m3_inf.dat', 'w')
    f321 = open(sphere + '/c3m2_inf.dat', 'w')
    f322 = open(sphere + '/c3m1_inf.dat', 'w')
    f323 = open(sphere + '/c30_inf.dat', 'w')
    f324 = open(sphere + '/c3p1_inf.dat', 'w')
    f325 = open(sphere + '/c3p2_inf.dat', 'w')
    f326 = open(sphere + '/c3p3_inf.dat', 'w')

    f330 = open(sphere + '/c4m4_inf.dat', 'w')
    f331 = open(sphere + '/c4m3_inf.dat', 'w')
    f332 = open(sphere + '/c4m2_inf.dat', 'w')
    f333 = open(sphere + '/c4m1_inf.dat', 'w')
    f334 = open(sphere + '/c40_inf.dat', 'w')
    f335 = open(sphere + '/c4p1_inf.dat', 'w')
    f336 = open(sphere + '/c4p2_inf.dat', 'w')
    f337 = open(sphere + '/c4p3_inf.dat', 'w')
    f338 = open(sphere + '/c4p4_inf.dat', 'w')

    f120 = open(sphere + '/mass.dat', 'w')
    f121 = open(sphere + '/Jz.dat', 'w')
    f122 = open(sphere + '/phase.dat', 'w')

    f130 = open(sphere + '/omega.dat', 'w')
    f131 = open(sphere + '/amp_and_phase.dat', 'w')
    f137 = open(sphere + '/amp_and_phase_inf.dat', 'w')   

    f132 = open(sphere + '/lum_gw.dat', 'w')

    f138 = open(sphere + '/HHamp_and_phase.dat', 'w')
    f139 = open(sphere + '/HHamp_and_phase_inf.dat', 'w')

    f133 = open(sphere + '/d2p2.dat', 'w')
    f134 = open(sphere + '/d2p2_inf.dat', 'w')

    f135 = open(sphere + '/h2p2.dat', 'w')
    f136 = open(sphere + '/h2p2_inf.dat', 'w')

    psi4I = readfield(sphere + "/t0", 'psi4I')
    h_phi      = np.pi/resolution_phi
    h_theta      = 2*np.pi/resolution_theta

    #------computing the spin-weighted spherical harmonics Y'_l,m------------
    #--  Y_l,m = Y'_l,m * exp ( +i*m*phi) ---------------------------------
    #--  Y_l,m = Y'_l,m * [cos( +i*m*phi) + i*np.sin( +i*m*phi)] -------------
    #-- *Y_l,m = Y'_l,m * [cos( +i*m*phi) - i*np.sin( +i*m*phi)] -------------
    Y2m2 = np.zeros((2, resolution_theta, resolution_phi))
    Y2m1 = np.zeros((2, resolution_theta, resolution_phi))
    Y20 = np.zeros((2, resolution_theta, resolution_phi))
    Y2p1 = np.zeros((2, resolution_theta, resolution_phi))
    Y2p2 = np.zeros((2, resolution_theta, resolution_phi))

    Y3m3 = np.zeros((2, resolution_theta, resolution_phi))
    Y3m2 = np.zeros((2, resolution_theta, resolution_phi))
    Y3m1 = np.zeros((2, resolution_theta, resolution_phi))
    Y30 = np.zeros((2, resolution_theta, resolution_phi))
    Y3p1 = np.zeros((2, resolution_theta, resolution_phi))
    Y3p2 = np.zeros((2, resolution_theta, resolution_phi))
    Y3p3 = np.zeros((2, resolution_theta, resolution_phi))

    Y4m4 = np.zeros((2, resolution_theta, resolution_phi))
    Y4m3 = np.zeros((2, resolution_theta, resolution_phi))
    Y4m2 = np.zeros((2, resolution_theta, resolution_phi))
    Y4m1 = np.zeros((2, resolution_theta, resolution_phi))
    Y40 = np.zeros((2, resolution_theta, resolution_phi))
    Y4p1 = np.zeros((2, resolution_theta, resolution_phi))
    Y4p2 = np.zeros((2, resolution_theta, resolution_phi))
    Y4p3 = np.zeros((2, resolution_theta, resolution_phi))
    Y4p4 = np.zeros((2, resolution_theta, resolution_phi))

    c2m2 = np.zeros((2, len(times)))
    c2m1 = np.zeros((2, len(times)))
    c20 = np.zeros((2, len(times)))
    c2p1 = np.zeros((2, len(times)))
    c2p2 = np.zeros((2, len(times)))

    c3m3 = np.zeros((2, len(times)))
    c3m2 = np.zeros((2, len(times)))
    c3m1 = np.zeros((2, len(times)))
    c30 = np.zeros((2, len(times)))
    c3p1 = np.zeros((2, len(times)))
    c3p2 = np.zeros((2, len(times)))
    c3p3 = np.zeros((2, len(times)))

    c4m4 = np.zeros((2, len(times)))
    c4m3 = np.zeros((2, len(times)))
    c4m2 = np.zeros((2, len(times)))
    c4m1 = np.zeros((2, len(times)))
    c40 = np.zeros((2, len(times)))
    c4p1 = np.zeros((2, len(times)))
    c4p2 = np.zeros((2, len(times)))
    c4p3 = np.zeros((2, len(times)))
    c4p4 = np.zeros((2, len(times)))

    phase = np.zeros((len(times)))

    psi4r_tmp = np.zeros((resolution_theta, resolution_phi))
    psi4i_tmp = np.zeros((resolution_theta, resolution_phi))

    for iphi in xrange(resolution_phi):
        theta = iphi*h_phi
        for itheta in xrange(resolution_theta):
            phi = itheta*h_theta 

            tY2m2 = 0.5*np.sqrt(5.0/np.pi) * (np.sin(theta/2.0))**4
            tY2m1 = 0.5*np.sqrt(5.0/np.pi) * np.sin(theta) * (np.sin(theta/2.0))**2
            tY20  = 0.25*np.sqrt(15.0/(2.0*np.pi)) * (np.sin(theta))**2
            tY2p1 = 0.25*np.sqrt(5.0/np.pi) * np.sin(theta) * (1.0 + np.cos(theta)) 
            tY2p2 = 0.5*np.sqrt(5.0/np.pi) * (np.cos(theta/2.0))**4
                                
            Y2m2[0, itheta, iphi] = tY2m2*np.cos(-2.0*phi)
            Y2m1[0, itheta, iphi] = tY2m1*np.cos(-1.0*phi)
            Y20[0, itheta, iphi]  = tY20
            Y2p1[0, itheta, iphi] = tY2p1*np.cos(+1.0*phi)
            Y2p2[0, itheta, iphi] = tY2p2*np.cos(+2.0*phi)

            Y2m2[1, itheta, iphi] =-tY2m2*np.sin(-2.0*phi)
            Y2m1[1, itheta, iphi] =-tY2m1*np.sin(-1.0*phi)
            Y20[1, itheta, iphi]  =-tY20*0.0
            Y2p1[1, itheta, iphi] =-tY2p1*np.sin(+1.0*phi)
            Y2p2[1, itheta, iphi] =-tY2p2*np.sin(+2.0*phi)

            tY3m3 = 0.5*np.sqrt(21.0/(2.0*np.pi)) * np.sin(theta) * (np.sin(theta/2.0))**4 
            tY3m2 = 0.5*np.sqrt(7.0/np.pi) * (2.0 + 3.0*np.cos(theta)) * (np.sin(theta/2.0))**4
            tY3m1 = (1.0/32.0)*np.sqrt(35.0/(2.0*np.pi)) * (np.sin(theta) + 4.0*np.sin(2.0*theta) - 3.0*np.sin(3.0*theta))
            tY30  = 0.25*np.sqrt(105.0/(2.0*np.pi)) * np.cos(theta) * (np.sin(theta))**2
            tY3p1 =-(1.0/32.0)*np.sqrt(35.0/(2.0*np.pi)) * (np.sin(theta) - 4.0*np.sin(2.0*theta) - 3.0*np.sin(3.0*theta))
            tY3p2 = 0.5*np.sqrt(7.0/np.pi) * (-2.0 + 3.0*np.cos(theta)) * (np.cos(theta/2.0))**4
            tY3p3 =-np.sqrt(21.0/(2.0*np.pi)) * np.sin(theta/2.0) * (np.cos(theta/2.0))**5

            Y3m3[0, itheta, iphi] = tY3m3*np.cos(-3.0*phi)                                 
            Y3m2[0, itheta, iphi] = tY3m2*np.cos(-2.0*phi)
            Y3m1[0, itheta, iphi] = tY3m1*np.cos(-1.0*phi)
            Y30[0, itheta, iphi]  = tY30*np.cos(0.0*phi)
            Y3p1[0, itheta, iphi] = tY3p1*np.cos(+1.0*phi)
            Y3p2[0, itheta, iphi] = tY3p2*np.cos(+2.0*phi)
            Y3p3[0, itheta, iphi] = tY3p3*np.cos(+3.0*phi)

            Y3m3[1, itheta, iphi] =-tY3m3*np.sin(-3.0*phi)
            Y3m2[1, itheta, iphi] =-tY3m2*np.sin(-2.0*phi)
            Y3m1[1, itheta, iphi] =-tY3m1*np.sin(-1.0*phi)
            Y30[1, itheta, iphi]  =-tY30*np.sin(0.0*phi)
            Y3p1[1, itheta, iphi] =-tY3p1*np.sin(+1.0*phi)
            Y3p2[1, itheta, iphi] =-tY3p2*np.sin(+2.0*phi)
            Y3p3[1, itheta, iphi] =-tY3p3*np.sin(+3.0*phi)

            tY4m4 = (3.0/4.0)*np.sqrt(7.0/np.pi) * (np.sin(theta))**2 * (np.sin(theta/2.0))**4 
            tY4m3 = 3.0*np.sqrt(7.0/(2.0*np.pi)) * (1.0 + 2.0*np.cos(theta)) * (np.sin(theta/2.0))**5
            tY4m2 = (3.0/4.0)*np.sqrt(1.0/np.pi) * (9.0 + 14.0*np.cos(theta) + 7.0*np.cos(2.0*theta)) * (np.sin(theta/2.0))**4
            tY4m1 = (3.0/32.0)*np.sqrt(1.0/(2.0*np.pi)) * (3.0*np.sin(theta) + 2.0*np.sin(2.0*theta) + 7.0*np.sin(3.0*theta) - 7.0*np.sin(4.0*theta))
            tY40  = (3.0/16.0)*np.sqrt(5.0/(2.0*np.pi)) * (5.0 + 7.0*np.cos(2.0*theta)) * (np.sin(theta))**2
            tY4p1 = (3.0/32.0)*np.sqrt(1.0/(2.0*np.pi)) * (3.0*np.sin(theta) - 2.0*np.sin(2.0*theta) + 7.0*np.sin(3.0*theta) + np.sin(4.0*theta))
            tY4p2 = (3.0/4.0)*np.sqrt(1.0/np.pi) * (9.0 - 14.0*np.cos(theta) + 7.0*np.cos(2.0*theta)) * (np.cos(theta/2.0))**4
            tY4p3 =-3.0*np.sqrt(7.0/(2.0*np.pi)) * (-1.0 + 2.0*np.cos(theta)) * (np.sin(theta/2.0)) * (np.cos(theta/2.0))**5
            tY4p4 = (3.0/4.0)*np.sqrt(7.0/np.pi) * (np.sin(theta))**2 * (np.cos(theta/2.0))**(4) 

            Y4m4[0, itheta, iphi] = tY4m4*np.cos(-4.0*phi)
            Y4m3[0, itheta, iphi] = tY4m3*np.cos(-3.0*phi)
            Y4m2[0, itheta, iphi] = tY4m2*np.cos(-2.0*phi)
            Y4m1[0, itheta, iphi] = tY4m1*np.cos(-1.0*phi)
            Y40[0, itheta, iphi]  = tY40*np.cos(0.0*phi)
            Y4p1[0, itheta, iphi] = tY4p1*np.cos(+1.0*phi)
            Y4p2[0, itheta, iphi] = tY4p2*np.cos(+2.0*phi)
            Y4p3[0, itheta, iphi] = tY4p3*np.cos(+3.0*phi)
            Y4p4[0, itheta, iphi] = tY4p4*np.cos(+4.0*phi)

            Y4m4[1, itheta, iphi] =-tY4m4*np.sin(-4.0*phi)
            Y4m3[1, itheta, iphi] =-tY4m3*np.sin(-3.0*phi)
            Y4m2[1, itheta, iphi] =-tY4m2*np.sin(-2.0*phi)
            Y4m1[1, itheta, iphi] =-tY4m1*np.sin(-1.0*phi)
            Y40[1, itheta, iphi]  =-tY40*np.sin(0.0*phi)
            Y4p1[1, itheta, iphi] =-tY4p1*np.sin(+1.0*phi)
            Y4p2[1, itheta, iphi] =-tY4p2*np.sin(+2.0*phi)
            Y4p3[1, itheta, iphi] =-tY4p3*np.sin(+3.0*phi)
            Y4p4[1, itheta, iphi] =-tY4p4*np.sin(+4.0*phi)

    for t in times:
        psi4r = readfield(sphere + "/t" + str(t*delta_output), 'psi4R')
        psi4i = readfield(sphere + "/t" + str(t*delta_output), 'psi4I')
        M_ADM_surf = readfield(sphere + "/t" + str(t*delta_output), 'M_ADM_surf')
        Jz_ADM_surf = readfield(sphere + "/t" + str(t*delta_output), 'Jz_ADM_surf')

        time = t*dt
        print "time=", time
         
        psi4r = psi4r*factor*rfactorhit
        psi4i = psi4i*factor*rfactorhit

        # l=2            
        temp1 = psi4r*Y2m2[0,:] - psi4i*Y2m2[1,:]
        temp2 = psi4r*Y2m2[1,:] + psi4i*Y2m2[0,:]
        c2m2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2m2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2m1[0,:] - psi4i*Y2m1[1,:]
        temp2 = psi4r*Y2m1[1,:] + psi4i*Y2m1[0,:]
        c2m1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2m1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y20[0,:] - psi4i*Y20[1,:]
        temp2 = psi4r*Y20[1,:] + psi4i*Y20[0,:]
        c20[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c20[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2p1[0,:] - psi4i*Y2p1[1,:]
        temp2 = psi4r*Y2p1[1,:] + psi4i*Y2p1[0,:]
        c2p1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2p1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y2p2[0,:] - psi4i*Y2p2[1,:]
        temp2 = psi4r*Y2p2[1,:] + psi4i*Y2p2[0,:]
        c2p2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c2p2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        # l=3                
        temp1 = psi4r*Y3m3[0,:] - psi4i*Y3m3[1,:]
        temp2 = psi4r*Y3m3[1,:] + psi4i*Y3m3[0,:]
        c3m3[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3m3[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y3m2[0,:] - psi4i*Y3m2[1,:]
        temp2 = psi4r*Y3m2[1,:] + psi4i*Y3m2[0,:]
        c3m2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3m2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y3m1[0,:] - psi4i*Y3m1[1,:]
        temp2 = psi4r*Y3m1[1,:] + psi4i*Y3m1[0,:]
        c3m1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3m1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y30[0,:] - psi4i*Y30[1,:]
        temp2 = psi4r*Y30[1,:] + psi4i*Y30[0,:]
        c30[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c30[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y3p1[0,:] - psi4i*Y3p1[1,:]
        temp2 = psi4r*Y3p1[1,:] + psi4i*Y3p1[0,:]
        c3p1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3p1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y3p2[0,:] - psi4i*Y3p2[1,:]
        temp2 = psi4r*Y3p2[1,:] + psi4i*Y3p2[0,:]
        c3p2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3p2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y3p3[0,:] - psi4i*Y3p3[1,:]
        temp2 = psi4r*Y3p3[1,:] + psi4i*Y3p3[0,:]
        c3p3[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c3p3[1,t] = surface_integral2(temp2, h_theta, h_phi)

        # l=4
        temp1 = psi4r*Y4m4[0,:] - psi4i*Y4m4[1,:]
        temp2 = psi4r*Y4m4[1,:] + psi4i*Y4m4[0,:]
        c4m4[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4m4[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4m3[0,:] - psi4i*Y4m3[1,:]
        temp2 = psi4r*Y4m3[1,:] + psi4i*Y4m3[0,:]
        c4m3[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4m3[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4m2[0,:] - psi4i*Y4m2[1,:]
        temp2 = psi4r*Y4m2[1,:] + psi4i*Y4m2[0,:]
        c4m2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4m2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4m1[0,:] - psi4i*Y4m1[1,:]
        temp2 = psi4r*Y4m1[1,:] + psi4i*Y4m1[0,:]
        c4m1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4m1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y40[0,:] - psi4i*Y40[1,:]
        temp2 = psi4r*Y40[1,:] + psi4i*Y40[0,:]
        c40[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c40[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4p1[0,:] - psi4i*Y4p1[1,:]
        temp2 = psi4r*Y4p1[1,:] + psi4i*Y4p1[0,:]
        c4p1[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4p1[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4p2[0,:] - psi4i*Y4p2[1,:]
        temp2 = psi4r*Y4p2[1,:] + psi4i*Y4p2[0,:]
        c4p2[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4p2[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4p3[0,:] - psi4i*Y4p3[1,:]
        temp2 = psi4r*Y4p3[1,:] + psi4i*Y4p3[0,:]
        c4p3[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4p3[1,t] = surface_integral2(temp2, h_theta, h_phi)

        temp1 = psi4r*Y4p4[0,:] - psi4i*Y4p4[1,:]
        temp2 = psi4r*Y4p4[1,:] + psi4i*Y4p4[0,:]
        c4p4[0,t] = surface_integral2(temp1, h_theta, h_phi)
        c4p4[1,t] = surface_integral2(temp2, h_theta, h_phi)

        tmass = surface_integral2(M_ADM_surf, h_theta, h_phi)
        tJz   = surface_integral2(Jz_ADM_surf, h_theta, h_phi)

        smallnumber =1e-10
        x = c2p2[0,t]
        y = c2p2[1,t]
        if x > smallnumber and y >= 0:
            phase[t] = math.atan(y/x) 
        elif ((x < -smallnumber) and (y >= 0.0)):
            phase[t] = math.atan(y/x)+np.pi
        elif ((x < -smallnumber) and (y <= 0.0)):
            phase[t] = math.atan(y/x)+np.pi
        elif ((x > smallnumber) and (y <= 0.0)):
            phase[t] = math.atan(y/x)+2.*np.pi

        cphase = 0
        if (time > 2 and x > smallnumber):
            if (c2p2[1,t - 1] > 0 and c2p2[1,t] > 0.):
                cphase=cphase-1

            elif(c2p2[1,t - 1] < 0 and c2p2[1,t] > 0.):
                cphase=cphase+1

        phase[t] = phase[t] + (cphase)*2.*np.pi

        print "-------------------------"
        print " mass=", tmass
        print "-------------------------"      
        print "-----ANGULAR MOMENTUM----"
        print " Jz=", tJz      
        print "-------------------------"      
        print "-----L=2 MODES ----------"
        print "-------------------------"
        print "c2m2=",c2m2[0,t],"c2m2=",c2m2[1,t]
        print "c2m1=",c2m1[0,t],"c2m1=",c2m1[1,t]
        print "c20=",c20[0,t],  "c20=",c20[1,t]
        print "c2p1=",c2p1[0,t],"c2p1=",c2p1[1,t]
        print "c2p2=",c2p2[0,t],"c2p2=",c2p2[1,t]

        f110.write(str(time) + " " + str(c2m2[0, t]) + " " + str(c2m2[1, t]) + "\n")
        f111.write(str(time) + " " + str(c2m1[0, t]) + " " + str(c2m1[1, t]) + "\n")
        f112.write(str(time) + " " + str(c20[0, t]) + " " + str(c20[1, t]) + "\n")
        f113.write(str(time) + " " + str(c2p1[0, t]) + " " + str(c2p1[1, t]) + "\n")
        f114.write(str(time) + " " + str(c2p2[0, t]) + " " + str(c2p2[1, t]) + "\n")

        f210.write(str(time) +  " " + str(c3m3[0, t]) + " " + str(c3m3[1, t]) + "\n") 
        f211.write(str(time) +  " " + str(c3m2[0, t]) + " " + str(c3m2[1, t]) + "\n") 
        f212.write(str(time) +  " " + str(c3m1[0, t]) + " " + str(c3m1[1, t]) + "\n") 
        f213.write(str(time) +  " " + str(c30[0, t]) + " " + str(c30[1, t]) + "\n") 
        f214.write(str(time) +  " " + str(c3p1[0, t]) + " " + str(c3p1[1, t]) + "\n") 
        f215.write(str(time) +  " " + str(c3p2[0, t]) + " " + str(c3p2[1, t]) + "\n") 
        f216.write(str(time) +  " " + str(c3p3[0, t]) + " " + str(c3p3[1, t]) + "\n") 

        f220.write(str(time) +  " " + str(c4m4[0, t]) + " " + str(c4m4[1, t]) + "\n") 
        f221.write(str(time) +  " " + str(c4m3[0, t]) + " " + str(c4m3[1, t]) + "\n") 
        f222.write(str(time) +  " " + str(c4m2[0, t]) + " " + str(c4m2[1, t]) + "\n") 
        f223.write(str(time) +  " " + str(c4m1[0, t]) + " " + str(c4m1[1, t]) + "\n") 
        f224.write(str(time) +  " " + str(c40[0, t]) + " " + str(c40[1, t]) + "\n") 
        f225.write(str(time) +  " " + str(c4p1[0, t]) + " " + str(c4p1[1, t]) + "\n") 
        f226.write(str(time) +  " " + str(c4p2[0, t]) + " " + str(c4p2[1, t]) + "\n") 
        f227.write(str(time) +  " " + str(c4p3[0, t]) + " " + str(c4p3[1, t]) + "\n") 
        f228.write(str(time) +  " " + str(c4p4[0, t]) + " " + str(c4p4[1, t]) + "\n")

        f120.write(str(time) + " " + str(tmass) + "\n")
        f121.write(str(time) + " " + str(tJz) + "\n")
        f122.write(str(time) + " " + str(phase[t]) + "\n")


    #-computing the frequency from the dominant mode l=m=2
    #-either by omega=dphase/dt
    # or omega = -1/m*Im [ (d C_{lm}/dt)/ C_{lm}] 
    # (a+i*b)/(c+i*d) = (a*c + b*d + i*(b*c - a*d))/(c^2 + d^2)

    ra        = rext * ( 1.0 + massI/(2.0*rext) )**2
    rr        = rext * ( 1.0 + massI/(2.0*rext) )**2
    C0        = (1.0 - 2.0*massI/ra)
    factorl2  = (1.0*4.0)/(2.0*ra) 
    factorl3  = (2.0*5.0)/(2.0*ra) 
    factorl4  = (3.0*6.0)/(2.0*ra) 

    integralc2m2 = np.zeros((2))
    integralc2m1 = np.zeros((2))
    integralc20 = np.zeros((2))
    integralc20 = np.zeros((2))
    integralc2p1 = np.zeros((2))
    integralc2p2 = np.zeros((2))
    integralc3m3 = np.zeros((2))
    integralc3m2 = np.zeros((2))
    integralc3m1 = np.zeros((2))
    integralc30 = np.zeros((2))
    integralc3p1 = np.zeros((2))
    integralc3p2 = np.zeros((2))
    integralc3p3 = np.zeros((2))
    integralc4m4 = np.zeros((2))
    integralc4m3 = np.zeros((2))
    integralc4m2 = np.zeros((2))
    integralc4m1 = np.zeros((2))
    integralc40 = np.zeros((2))
    integralc4p1 = np.zeros((2))
    integralc4p2 = np.zeros((2))
    integralc4p3 = np.zeros((2))
    integralc4p4 = np.zeros((2))

    c2m2_inf = np.zeros((2, len(times)))
    c2m1_inf = np.zeros((2, len(times)))
    c20_inf = np.zeros((2, len(times)))
    c2p1_inf = np.zeros((2, len(times)))
    c2p2_inf = np.zeros((2, len(times)))

    c3m3_inf = np.zeros((2, len(times)))
    c3m2_inf = np.zeros((2, len(times)))
    c3m1_inf = np.zeros((2, len(times)))
    c30_inf = np.zeros((2, len(times)))
    c3p1_inf = np.zeros((2, len(times)))
    c3p2_inf = np.zeros((2, len(times)))
    c3p3_inf = np.zeros((2, len(times)))

    c4m4_inf = np.zeros((2, len(times)))
    c4m3_inf = np.zeros((2, len(times)))
    c4m2_inf = np.zeros((2, len(times)))
    c4m1_inf = np.zeros((2, len(times)))
    c40_inf = np.zeros((2, len(times)))
    c4p1_inf = np.zeros((2, len(times)))
    c4p2_inf = np.zeros((2, len(times)))
    c4p3_inf = np.zeros((2, len(times)))
    c4p4_inf = np.zeros((2, len(times)))

    for i in xrange(1, len(times) - 1):
        time = i*dt
        time_tort = time - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))

        if time > time_min:
            integralc2m2 = integralc2m2 + c2m2[:, i]*dt 
            integralc2m1 = integralc2m1 + c2m1[:, i]*dt 
            integralc20  = integralc20  + c20[:, i]*dt 
            integralc2p1 = integralc2p1 + c2p1[:, i]*dt 
            integralc2p2 = integralc2p2 + c2p2[:, i]*dt 

            integralc3m3 = integralc3m3 + c3m3[:, i]*dt 
            integralc3m2 = integralc3m2 + c3m2[:, i]*dt 
            integralc3m1 = integralc3m1 + c3m1[:, i]*dt 
            integralc30  = integralc30  + c30[:, i]*dt 
            integralc3p1 = integralc3p1 + c3p1[:, i]*dt 
            integralc3p2 = integralc3p2 + c3p2[:, i]*dt 
            integralc3p3 = integralc3p3 + c3p3[:, i]*dt

            integralc4m4 = integralc4m4 + c4m4[:, i]*dt 
            integralc4m3 = integralc4m3 + c4m3[:, i]*dt 
            integralc4m2 = integralc4m2 + c4m2[:, i]*dt 
            integralc4m1 = integralc4m1 + c4m1[:, i]*dt 
            integralc40  = integralc40  + c40[:, i]*dt 
            integralc4p1 = integralc4p1 + c4p1[:, i]*dt 
            integralc4p2 = integralc4p2 + c4p2[:, i]*dt 
            integralc4p3 = integralc4p3 + c4p3[:, i]*dt
            integralc4p4 = integralc4p4 + c4p4[:, i]*dt

        c2m2_inf[:, i] = C0 * (c2m2[:, i] - factorl2*integralc2m2)
        c2m1_inf[:, i] = C0 * (c2m1[:, i] - factorl2*integralc2m1)
        c20_inf[:, i]  = C0 * (c20[:, i]  - factorl2*integralc20)
        c2p1_inf[:, i] = C0 * (c2p1[:, i] - factorl2*integralc2p1)
        c2p2_inf[:, i] = C0 * (c2p2[:, i] - factorl2*integralc2p2)

        c3m3_inf[:, i] = C0 * (c3m3[:, i] - factorl3*integralc3m3)
        c3m2_inf[:, i] = C0 * (c3m2[:, i] - factorl3*integralc3m2)
        c3m1_inf[:, i] = C0 * (c3m1[:, i] - factorl3*integralc3m1)
        c30_inf[:, i]  = C0 * (c30[:, i]  - factorl3*integralc30)
        c3p1_inf[:, i] = C0 * (c3p1[:, i] - factorl3*integralc3p1)
        c3p2_inf[:, i] = C0 * (c3p2[:, i] - factorl3*integralc3p2)
        c3p3_inf[:, i] = C0 * (c3p3[:, i] - factorl3*integralc3p3)

        c4m4_inf[:, i] = C0 * (c4m4[:, i] - factorl4*integralc4m4)
        c4m3_inf[:, i] = C0 * (c4m3[:, i] - factorl4*integralc4m3)
        c4m2_inf[:, i] = C0 * (c4m2[:, i] - factorl4*integralc4m2)
        c4m1_inf[:, i] = C0 * (c4m1[:, i] - factorl4*integralc4m1)
        c40_inf[:, i]  = C0 * (c40[:, i]  - factorl4*integralc40)
        c4p1_inf[:, i] = C0 * (c4p1[:, i] - factorl4*integralc4p1)
        c4p2_inf[:, i] = C0 * (c4p2[:, i] - factorl4*integralc4p2)
        c4p3_inf[:, i] = C0 * (c4p3[:, i] - factorl4*integralc4p3)
        c4p4_inf[:, i] = C0 * (c4p4[:, i] - factorl4*integralc4p4)

        f310.write(str(time_tort) + " " + str(c2m2_inf[0,i]) + " " + str(c2m2_inf[1,i]) + " " + str(time) + "\n")
        f311.write(str(time_tort) + " " + str(c2m1_inf[0,i]) + " " + str(c2m1_inf[1,i]) + " " + str(time) + "\n")
        f312.write(str(time_tort) + " " + str(c20_inf[0,i]) + " " + str(c20_inf[1,i]) + " " + str(time) + "\n")
        f313.write(str(time_tort) + " " + str(c2p1_inf[0,i]) + " " + str(c2p1_inf[1,i]) + " " + str(time) + "\n")
        f314.write(str(time_tort) + " " + str(c2p2_inf[0,i]) + " " + str(c2p2_inf[1,i]) + " " + str(time) + "\n")

        f320.write(str(time_tort) + " " + str(c3m3_inf[0,i]) + " " + str(c3m3_inf[1,i]) + " " + str(time) + "\n")
        f321.write(str(time_tort) + " " + str(c3m2_inf[0,i]) + " " + str(c3m2_inf[1,i]) + " " + str(time) + "\n")
        f322.write(str(time_tort) + " " + str(c3m1_inf[0,i]) + " " + str(c3m1_inf[1,i]) + " " + str(time) + "\n")
        f323.write(str(time_tort) + " " + str(c30_inf[0,i]) + " " + str(c30_inf[1,i]) + " " + str(time) + "\n")
        f324.write(str(time_tort) + " " + str(c3p1_inf[0,i]) + " " + str(c3p1_inf[1,i]) + " " + str(time) + "\n")
        f325.write(str(time_tort) + " " + str(c3p2_inf[0,i]) + " " + str(c3p2_inf[1,i]) + " " + str(time) + "\n")
        f326.write(str(time_tort) + " " + str(c3p3_inf[0,i]) + " " + str(c3p3_inf[1,i]) + " " + str(time) + "\n")

        f330.write(str(time_tort) + " " + str(c4m4_inf[0,i]) + " " + str(c4m4_inf[1,i]) + " " + str(time) + "\n")
        f331.write(str(time_tort) + " " + str(c4m3_inf[0,i]) + " " + str(c4m3_inf[1,i]) + " " + str(time) + "\n")
        f332.write(str(time_tort) + " " + str(c4m2_inf[0,i]) + " " + str(c4m2_inf[1,i]) + " " + str(time) + "\n")
        f333.write(str(time_tort) + " " + str(c4m1_inf[0,i]) + " " + str(c4m1_inf[1,i]) + " " + str(time) + "\n")
        f334.write(str(time_tort) + " " + str(c40_inf[0,i]) + " " + str(c40_inf[1,i]) + " " + str(time) + "\n")
        f335.write(str(time_tort) + " " + str(c4p1_inf[0,i]) + " " + str(c4p1_inf[1,i]) + " " + str(time) + "\n")
        f336.write(str(time_tort) + " " + str(c4p2_inf[0,i]) + " " + str(c4p2_inf[1,i]) + " " + str(time) + "\n")
        f337.write(str(time_tort) + " " + str(c4p3_inf[0,i]) + " " + str(c4p3_inf[1,i]) + " " + str(time) + "\n")
        f338.write(str(time_tort) + " " + str(c4p4_inf[0,i]) + " " + str(c4p4_inf[1,i]) + " " + str(time) + "\n")

        omega1 = 0.5*( phase[i+1]-phase[i-1] )/(2.0*dt)

        cR2p2   = c2p2[0, i]
        cI2p2   = c2p2[1, i]

        if i > 1 and i < len(times):
            dtcR2p2 = (c2p2[0, i+1] - c2p2[0, i-1]) / (2.0*dt)
            dtcI2p2 = (c2p2[1, i+1] - c2p2[1, i-1]) / (2.0*dt)
            temp1 = (dtcI2p2*cR2p2 - dtcR2p2*cI2p2)/(cR2p2**2 + cI2p2**2)   
            omega2 = abs((1.0/2.0)*temp1)
            f130.write(str(time_tort) + " " + str(omega2) + " " + str(omega1) + " " + str(time) + "\n")

    
        f131.write(str(time_tort) + " " + str(np.sqrt(c2p2[0,i]**2 + c2p2[1,i]**2 )) + " " + str(phase[i]) + " " + str(time) + "\n")


    # OK the above did a bunch of things wrt c22, but we have
    # the exptrapolated to infinity wavefor, so let us redo with that
    # as well... this file is getting big and uggly... but i saw beauty and the beast...

    cphase = 0

    for i in xrange(1, len(times)):
        time = i*dt
        time_tort = time - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        x = c2p2_inf[0, i]
        y = c2p2_inf[1, i]
        phase_inf = 0
        if (x > smallnumber and y >= 0.):
            phase_inf = math.atan(y/x) 
        elif ((x < -smallnumber) and (y  >= 0.0)) : 
            phase_inf = math.atan(y/x)+np.pi
        elif ((x < -smallnumber) and (y <= 0.0)) : 
            phase_inf = math.atan(y/x)+np.pi
        elif ((x  >  smallnumber) and (y <= 0.0)) : 
            phase_inf = math.atan(y/x)+2.*np.pi

        if(i > 2 and x > smallnumber) :
            if (c2p2_inf[1,i-1] > 0 and c2p2_inf[1, i] < 0.) :
                cphase=cphase-1

            elif(c2p2_inf[1,i-1] < 0 and c2p2_inf[1, i] > 0.) :
                cphase=cphase+1
        phase_inf = phase_inf + (cphase)*2.*np.pi

        f137.write(str(time_tort) + " " + str(np.sqrt(c2p2_inf[0,i]**2 + c2p2_inf[1,i]**2 )) + " " + str(phase_inf) + " " + str(time) + "\n")

    # compute the GW luminosity and total energy radiated in GW
    # first define some constant
    factor    = 1.0/(16.0*np.pi) 
    d2p2 = np.zeros((2))
    d2p1 = np.zeros((2))
    d20  = np.zeros((2))
    d2m1 = np.zeros((2))
    d2m2 = np.zeros((2))
    d2p2_inf = np.zeros((2))
    d2p1_inf = np.zeros((2))
    d20_inf  = np.zeros((2))
    d2m1_inf = np.zeros((2))
    d2m2_inf = np.zeros((2))
    print "ra=",ra,"factor", factor 

    for i in xrange(1, len(times)):
        time = time = i*dt
        time_tort = time - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        # with the raw data and the infinity extrapolation
        if (time > time_min):
            d2p2 = d2p2 + c2p2[:,i]*dt 
            d2p1 = d2p1 + c2p1[:,i]*dt 
            d20  = d20  + c20[:,i]*dt 
            d2m1 = d2m1 + c2m1[:,i]*dt 
            d2m2 = d2m2 + c2m2[:,i]*dt 

            d2p2_inf = d2p2_inf + c2p2_inf[:,i]*dt 
            d2p1_inf = d2p1_inf + c2p1_inf[:,i]*dt 
            d20_inf  = d20_inf  + c20_inf[:,i]*dt 
            d2m1_inf = d2m1_inf + c2m1_inf[:,i]*dt 
            d2m2_inf = d2m2_inf + c2m2_inf[:,i]*dt

        LGW = factor*( d2p2[0]**2 + d2p2[1]**2 + d2p1[0]**2 + d2p1[1]**2 + d20[0]**2 + d20[1]**2 + d2m1[0]**2 + d2m1[1]**2 + d2m2[0]**2 + d2m2[1]**2 )
    
        LGW_inf = factor * (d2p2_inf[0]**2 + d2p2_inf[1]**2 + d2p1_inf[0]**2 + d2p1_inf[1]**2 + d20_inf[0]**2 + d20_inf[1]**2 + d2m1_inf[0]**2 + d2m1_inf[1]**2 + d2m2_inf[0]**2 + d2m2_inf[1]**2 )

        f132.write(str(time_tort) + " " + str(LGW) + " " + str(LGW_inf) + " " + str(time) + "\n")



    # integrate twice in time to get the strain
    d2p2T = np.zeros((2, len(times)))
    d2p2T_inf = np.zeros((2, len(times)))

    # first time integral 
    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        # with the raw data and the infinity extrapolation
        if (localtime+2.*dt > time_min):
            d2p2T[:, i] = d2p2T[:,i-1] + 0.5*dt*(c2p2[:,i-1] + c2p2[:,i])
            d2p2T_inf[:,i] = d2p2T_inf[:,i-1] + 0.5*dt*(c2p2_inf[:,i-1] + c2p2_inf[:,i])
            f133.write(str(time_tort) + " " + str(d2p2T[0, i]) + " " + str(d2p2T[1, i]) + " " + str(localtime) + "\n")


    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!! the infinity part is picking a linear drift, 
    #! let us remove to see  if we can fix it somehow
    #! we will reuse variables defined for the h2p2 part
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # and we want to remove
    # using Steve's suggestion to do so
    
    h22_inf_ave = np.zeros((2))
    
    xyh22_inf_ave = np.zeros((2))
    count_ave = 0
    time_ave = 0
    xxtime_ave = 0
    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))

        # with the raw data and the infinity extrapolation
        if (localtime+2.*dt > time_min) :
            h22_inf_ave = h22_inf_ave + d2p2T_inf[:,i]
            xyh22_inf_ave = xyh22_inf_ave + d2p2T_inf[:,i]*localtime

            time_ave = time_ave + localtime
            xxtime_ave = xxtime_ave + localtime*localtime
            count_ave = count_ave + 1
        else:
            time_ave =  localtime
            xxtime_ave = localtime*localtime
            count_ave = 1

    slopeh22_inf =  (xyh22_inf_ave - h22_inf_ave*time_ave/count_ave) /(xxtime_ave - time_ave*time_ave/count_ave)
    interh22_inf = h22_inf_ave/count_ave - slopeh22_inf*time_ave/count_ave

    #remove drift
    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        print localtime
        if(localtime+2.*dt > time_min) :
            d2p2T_inf[:,i] = d2p2T_inf[:,i] - ( slopeh22_inf*localtime + interh22_inf)
            f134.write(str(time_tort) + " " + str(d2p2T_inf[0, i]) + " " + str(d2p2T_inf[1, i]) + " " + str(localtime) + "\n")



    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #!!! Now 2nd time integral, and : remove drift

    h2p2 = np.zeros((2, len(times)))
    h2p2_inf = np.zeros((2, len(times)))
    h22_ave = np.zeros((2))
    xyh22_ave = np.zeros((2))
    h22_inf_ave = np.zeros((2))
    xyh22_inf_ave = np.zeros((2))
    count_ave = 0

    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        # with the raw data and the infinity extrapolation
        if (localtime > time_min) :
            h2p2[:,i] = h2p2[:, i-1] + 0.5*dt*(d2p2T[:, i-1] + d2p2T[:,i])
            h2p2_inf[:,i] = h2p2_inf[:, i-1] + 0.5*dt*(d2p2T_inf[:, i-1] + d2p2T_inf[:,i])


            h22_ave = h22_ave + h2p2[:,i]
            h22_inf_ave = h22_inf_ave + h2p2_inf[:,i]

            xyh22_ave = xyh22_ave + h2p2[:,i]*localtime
            xyh22_inf_ave = xyh22_inf_ave + h2p2_inf[:,i]*localtime

            time_ave = time_ave + localtime
            xxtime_ave = xxtime_ave + localtime*localtime
            count_ave = count_ave + 1
        else:
            h22_ave =  h2p2[:,i]
            h22_inf_ave =  h2p2_inf[:,i]
            xyh22_ave =  h2p2[:,i]*localtime
            xyh22_inf_ave =  h2p2_inf[:,i]*localtime
            time_ave =  localtime
            xxtime_ave = localtime*localtime
            count_ave = 1

    slopeh22 =  (xyh22_ave - h22_ave*time_ave/count_ave) /(xxtime_ave - time_ave*time_ave/count_ave)

    interh22 = h22_ave/count_ave - slopeh22*time_ave/count_ave

    slopeh22_inf =  (xyh22_inf_ave - h22_inf_ave*time_ave/count_ave) /(xxtime_ave - time_ave*time_ave/count_ave)

    interh22_inf = h22_inf_ave/count_ave - slopeh22_inf*time_ave/count_ave


    #remove drift
    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        if (localtime > time_min) :

            h2p2[:,i] = h2p2[:,i] - (slopeh22*localtime + interh22)

            h2p2_inf[:,i] = h2p2_inf[:,i] - (slopeh22_inf*localtime + interh22_inf)

            f135.write(str(time_tort) + " " + str(h2p2[0, i]) + " " + str(h2p2[1, i]) + " " + str(localtime) + "\n")
            f136.write(str(time_tort) + " " + str(h2p2_inf[0, i]) + " " + str(h2p2_inf[1, i]) + " " + str(localtime) + "\n")


    #!!!!!! finally we note the phase and amplitudes above were obtained
    #!!! from c2p2, but that isnot the strain. so let us redo here with strains

    #!!! first from h2p2

    cphase = 0

    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        x = h2p2[0,i]
        y = h2p2[1,i]
        HHphase = 0
        if (localtime > time_min) :

            if (x  >  smallnumber and y >=0.) :
              HHphase = math.atan(y/x) 
            elif ((x  <  -smallnumber) and (y  >= 0.0)) : 
              HHphase = math.atan(y/x)+np.pi
            elif ((x  <  -smallnumber) and (y  <=  0.0)) : 
              HHphase = math.atan(y/x)+np.pi
            elif ((x  >  smallnumber) and (y  <=  0.0)) : 
              HHphase = math.atan(y/x)+2.*np.pi

            if(i > 2 and x > smallnumber) :
                if (h2p2[1,i-1] > 0 and h2p2[1,i] < 0.) :
                    cphase=cphase-1

                elif(h2p2[1,i-1] < 0 and h2p2[1,i] > 0.) :
                    cphase=cphase+1
            HHphase = HHphase + (cphase)*2.*np.pi

            f138.write(str(time_tort) + " " + str(np.sqrt( h2p2[0,i]**2 + h2p2[1,i]**2 )) + " " + str(HHphase) + " " + str(localtime) + "\n")


    #!!!! : with h2p2_inf
    cphase = 0

    for i in xrange(1, len(times)):
        localtime = i*dt
        time_tort = localtime - (rr + 2.0*massI*np.log(0.5*rr/massI -1.0))
        x = h2p2_inf[0,i]
        y = h2p2_inf[1,i]
        HHphase_inf = 0
        if (localtime > time_min) :

            if (x  >  smallnumber and y >=0.) :
              HHphase_inf = math.atan(y/x) 
            elif ((x  <  -smallnumber) and (y  >= 0.0)) : 
              HHphase_inf = math.atan(y/x)+np.pi
            elif ((x  <  -smallnumber) and (y  <=  0.0)) : 
              HHphase_inf = math.atan(y/x)+np.pi
            elif ((x  >  smallnumber) and (y  <=  0.0)) : 
              HHphase_inf = math.atan(y/x)+2.*np.pi

            if(i > 2 and x > smallnumber) :
                if (h2p2_inf[1,i-1] > 0 and h2p2_inf[1,i] < 0.) :
                    cphase=cphase-1
                elif(h2p2_inf[1,i-1] < 0 and h2p2_inf[1,i] > 0.) :
                    cphase=cphase+1

        HHphase_inf = HHphase_inf + (cphase)*2.*np.pi
        f139.write(str(time_tort) + " " + str(np.sqrt( h2p2_inf[0,i]**2 + h2p2_inf[1,i]**2 )) + " " + str(HHphase_inf) + " " + str(localtime) + "\n")


    f110.close()
    f111.close()
    f112.close()
    f113.close()
    f114.close()

    f120.close()
    f121.close()
    f122.close()

    f130.close()
    f131.close()
    f132.close()
    f133.close()
    f134.close()
    f135.close()
    f136.close()
    f137.close()

    f210.close()
    f211.close()
    f212.close()
    f213.close()
    f214.close()
    f215.close()
    f216.close()

    f220.close()
    f221.close()
    f222.close()
    f223.close()
    f224.close()
    f225.close()
    f226.close()
    f227.close()
    f228.close()

    f310.close()
    f311.close()
    f312.close()
    f313.close()
    f314.close()

    f320.close()
    f321.close()
    f322.close()
    f323.close()
    f324.close()
    f325.close()
    f326.close()

    f330.close()
    f331.close()
    f332.close()
    f333.close()
    f334.close()
    f335.close()
    f336.close()
    f337.close()
    f338.close()

if __name__ == "__main__":
    for (sphere, rext, hitwithr) in [('data_outputDir_sphere40', 40.0, 0)]:
        calculate(sphere, rext, hitwithr)
