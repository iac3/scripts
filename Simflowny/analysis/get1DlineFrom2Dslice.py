import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset

def get_line(folder, field, cycle, index, coord):
    if coord == 0:
        data = readData(folder + "/visit_dump." + str(c).zfill(5) + '/' + field + '_0.hdf5', field)[index, :]
        return ' '.join(map(str, data))
    if coord == 1:
        data = readData(folder + "/visit_dump." + str(c).zfill(5) + '/' + field + '_0.hdf5', field)[:, index]
        return ' '.join(map(str, data))

def main():
    indexOfCoordy0 = 
    indexOfCoordx0 = 
    f = open("output.txt", "a")
    for c in xrange(0, 251, 1):
        f.write(get_line(".", "psi4R", c, indexOfCoordy0, 1) + '\n')   # Line at axis x
        f.write(get_line(".", "psi4R", c, indexOfCoordx0, 0) + '\n')   # Line at axis y
    f.close()

if __name__ == "__main__":
    main()
