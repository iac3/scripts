import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

folders = {"..": [757760,778240,798720,819200,839680,860160,880640,901120]}
folders = {"..": [757760]}
level = 5
dx = 0.1
domain_min = -24
res = np.array([160, 80])
radius = 20
center = np.array([0, 0, 0])

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset

def saveData(folderout, field, level, data):
    #Writing the variable to disk
    f = h5py.File(folderout + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data.shape, dtype='float64')
    dset[:,:] = data[:,:]
    f.close()



def mapping_to_sphere(res, field, domain_min, dx):
    delta_phi = np.pi / res[1]
    delta_theta = 2 * np.pi / res[0]

    mapped_field = np.zeros((res[0], res[1]))
    for (i, j), v in np.ndenumerate(mapped_field):
        phi = j * delta_phi
        theta = i * delta_theta
        x = center[0] + radius * np.cos(theta) * np.sin(phi)
        y = center[1] + radius * np.sin(theta) * np.sin(phi)
        z = center[2] + radius * np.cos(phi)

        # Index of lower left point of interpolation cell:
        ii = int((x - domain_min) / dx);
        jj = int((y - domain_min) / dx);
        kk = int((z - domain_min) / dx);
             

        xfrac = ( x - ((ii*dx) + domain_min)) / dx;
        yfrac = ( y - ((jj*dx) + domain_min)) / dx;
        zfrac = ( z - ((kk*dx) + domain_min)) / dx;

        mapped_field[i,j] = ((1-xfrac) * (1-yfrac) * (1-zfrac) * field[ii, jj, kk]
                    + (1 - xfrac) *      yfrac  *      zfrac  * field[ii, jj + 1, kk + 1]
                    +        xfrac  * (1-yfrac) *      zfrac  * field[ii + 1, jj, kk + 1]
                    +        xfrac  *      yfrac  * (1-zfrac) * field[ii + 1, jj + 1, kk]
                    +        xfrac  * (1-yfrac) * (1-zfrac) * field[ii + 1, jj, kk]
                    + (1 - xfrac) *      yfrac  * (1-zfrac) * field[ii, jj + 1, kk]
                    + (1 - xfrac) * (1-yfrac) *      zfrac  * field[ii, jj, kk + 1]
                    +        xfrac  *      yfrac *      zfrac  * field[ii + 1, jj + 1, kk + 1])

    return mapped_field

def processFile(folderout, folder):
    bfu_x = readData(folder + "/Bfu_x_" + str(level) + ".hdf5", 'Bfu_x')
    bfu_y = readData(folder + "/Bfu_y_" + str(level) + ".hdf5", 'Bfu_y')
    bfu_z = readData(folder + "/Bfu_z_" + str(level) + ".hdf5", 'Bfu_z')

    # Calculate R component
    bfu_r = np.zeros(bfu_x.shape)
    for (i,j,k), v in np.ndenumerate(bfu_x):
        x = domain_min + i * dx
        y = domain_min + j * dx
        z = domain_min + k * dx
        r = np.sqrt(x*x + y*y + z*z) 
        bfu_r[i,j,k] = (x/r)*bfu_x[i,j,k] + (y/r)*bfu_y[i,j,k] + (z/r)*bfu_z[i,j,k]

    # Mapping to sphere
    bfu_r_map = mapping_to_sphere(res, bfu_r, domain_min, dx)

    saveData(folderout, 'Bfu_r', level, bfu_r_map)

    plt.figure()
    plt.imshow(np.moveaxis(bfu_r_map, 0, 1))
    plt.colorbar()
    plt.savefig(folderout + '/mapping.png')

def main():
    for f in folders:
        for i in folders[f]:
            folderout = f + "/visit_dump." + str(i).zfill(5) + '/borja_level' + str(level) + "/sphere"+ str(radius) + "/"
            if not os.path.exists(folderout):
                os.makedirs(folderout)
            processFile(folderout, f + "/visit_dump." + str(i).zfill(5) + '/borja_level' + str(level) + '/mask1e-7/box23')
    
if __name__ == "__main__":
    main()
