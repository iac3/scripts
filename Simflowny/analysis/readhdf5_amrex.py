import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

'''
    Write slice and sphere output to hdf5.
    Those outputs use only one box, even if simulation has run in parallel.
    Also, data is stored in C ordering.
'''
def writefieldsSliceSphere(folder, time, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes'][0]
    print("Box",boxes)

    size = [(boxes[2] - boxes[0]) + 2 ,(boxes[3] - boxes[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))
    print("Size",size)
    linear_size = size[0] * size[1]
    print("Linear size",linear_size)

    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            if value.decode("utf-8") == field:
                variable_offset = int(name[len('component_')])
                print("Variable offset ",variable_offset)
                tmp = level_data['data:datatype=0'][linear_size*variable_offset:linear_size*variable_offset+linear_size]
                tmp = np.reshape(tmp, (size[0],size[1]))
                data[:,:] = tmp[:,:]

                #Writing the variable to disk
                f = h5py.File(folder + '/' + field + '_' + str(level) + '_t' + str(time) + '.hdf5', "w")
                dset = f.create_dataset(field, data.shape, dtype='float64')
                dset[:,:] = data[:,:]
                f.close()
                f_sum.close()
                return


'''
    Write full output from a 2D simulation to hdf5.
    Several boxes can be generated internally, and it has to be gathered in the same numpy array.
    Also, data is stored in Fortran ordering.
'''
def writefieldsFull2D(folder, time, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes']
    print("Box",boxes)

    maximum = [0, 0]
    minimum = [999999, 999999]
    for box in boxes:
        print(box)
        maximum[0] = max(maximum[0], box[2])
        maximum[1] = max(maximum[1], box[3])
        minimum[0] = min(minimum[0], box[0])
        minimum[1] = min(minimum[1], box[1])
    size = [(maximum[0] - minimum[0]) + 2,(maximum[1] - minimum[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))
    print("Size",size)

    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            if value.decode("utf-8") == field:
                variable_offset = int(name[len('component_'):])
                print("Variable offset ",variable_offset)
                for index,box in np.ndenumerate(boxes):
                    print("Box", box, index[0])
                    box_offset = level_data['data:offsets=0'][index[0]]
                    rangeX = (box[0] - minimum[0], box[2]+2 - minimum[0])
                    rangeY = (box[1] - minimum[1], box[3]+2 - minimum[1])
                    sizeX = box[2] - box[0] + 2;
                    sizeY = box[3] - box[1] + 2;
                    linear_size = sizeX*sizeY
                    tmp = level_data['data:datatype=0'][box_offset + linear_size*variable_offset:box_offset + linear_size*variable_offset+linear_size]
                    tmp = np.reshape(tmp, (sizeX,sizeY), order='F')
                    print(rangeY)
                    data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]

                #Writing the variable to disk
                f = h5py.File(folder + '/' + field + '_' + str(level) + '_t' + str(time) + '.hdf5', "w")
                dset = f.create_dataset(field, data.shape, dtype='float64')
                dset[:,:] = data[:,:]
                f.close()
                f_sum.close()
                return

'''
    Write full output from a 3D simulation to hdf5.
    Several boxes can be generated internally, and it has to be gathered in the same numpy array.
    Also, data is stored in Fortran ordering.
'''
def writefieldsFull3D(folder, time, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes']
    print("Box",boxes)

    maximum = [0, 0, 0]
    minimum = [999999, 999999, 999999]
    for box in boxes:
        print(box)
        maximum[0] = max(maximum[0], box[3])
        maximum[1] = max(maximum[1], box[4])
        maximum[2] = max(maximum[1], box[5])
        minimum[0] = min(minimum[0], box[0])
        minimum[1] = min(minimum[1], box[1])
        minimum[2] = min(minimum[1], box[2])
    size = [(maximum[0] - minimum[0]) + 2,(maximum[1] - minimum[1]) + 2,(maximum[2] - minimum[2]) + 2]
    data = np.zeros(shape=(size[0], size[1], size[2]))
    print("Size",size)

    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            if value.decode("utf-8") == field:
                variable_offset = int(name[len('component_'):])
                print("Variable offset ",variable_offset)
                for index,box in np.ndenumerate(boxes):
                    print("Box", box, index[0])
                    box_offset = level_data['data:offsets=0'][index[0]]
                    print(box_offset)
                    rangeX = (box[0] - minimum[0], box[3]+2 - minimum[0])
                    rangeY = (box[1] - minimum[1], box[4]+2 - minimum[1])
                    rangeZ = (box[2] - minimum[2], box[5]+2 - minimum[2])
                    sizeX = box[3] - box[0] + 2;
                    sizeY = box[4] - box[1] + 2;
                    sizeZ = box[5] - box[2] + 2;
                    print(sizeY)
                    linear_size = sizeX*sizeY*sizeZ
                    tmp = level_data['data:datatype=0'][box_offset + linear_size*variable_offset:box_offset + linear_size*variable_offset+linear_size]
                    tmp = np.reshape(tmp, (sizeX,sizeY,sizeZ), order='F')
                    print(rangeY)
                    data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[:,:,:]

                #Writing the variable to disk
                f = h5py.File(folder + '/' + field + '_' + str(level) + '_t' + str(time) + '.hdf5', "w")
                dset = f.create_dataset(field, data.shape, dtype='float64')
                dset[:,:,:] = data[:,:,:]
                f.close()
                f_sum.close()
                return


def main():
    '''
    for t in range(0, 1, 1):
        writefieldsSliceSphere("./plt_slice", t, "Alpha", 0)

    for t in range(0, 1, 1):
        writefieldsFull2D("./plt_slice", t, "Alpha", 0)
    '''

    for t in range(0, 1, 1):
        writefieldsFull3D("./plt_full", t, "rhof", 1)

if __name__ == "__main__":
    main()
