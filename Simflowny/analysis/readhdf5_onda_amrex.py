import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

def writefields2D(folder, time, destFolder):
    #Read mesh information
    f_sum = h5py.File(folder + "/plt" + str(time).zfill(5) + ".h5", "r")
    dims = f_sum['/Chombo_global'].attrs.get("SpaceDim")[0]
    print("Dimensions",dims)
    level_data  = f_sum['/level_' + str(level)]
    boxes = level_data['boxes'][0]
    print("Box",boxes)

    size = [(boxes[2] - boxes[0]) + 2 ,(boxes[3] - boxes[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))
    print("Size",size)
    linear_size = size[0] * size[1]
    print("Linear size",linear_size)

    iVar = 0
    for (name, value) in f_sum.attrs.items():
        if 'component_' in name:
            var_name = value.decode("utf-8")
            data = np.zeros(shape=(size[0], size[1]))

            variable_offset = int(name[len('component_')])
            print("Variable offset ",variable_offset)
            tmp = level_data['data:datatype=0'][linear_size*variable_offset:linear_size*variable_offset+linear_size]
            tmp = np.reshape(tmp, (size[0],size[1]))
            data[:,:] = tmp[:,:]

            #Writing the variable to disk
            f = h5py.File(destFolder + "/" + var_name + '.hdf5', "w")
            dset = f.create_dataset(var_name, data.shape, dtype='f')
            dset[:,:] = data[:,:]
            f.close()

    f_sum.close()
def main():

    for s in [40, 50, 60]:
        for t in xrange(0, 442, 1):
            if not os.path.exists("data_outputDir_sphere" + str(s) + "/t" + str(t)):
                os.makedirs("data_outputDir_sphere" + str(s) + "/t" + str(t))
            writefields2D("outputDir_sphere" + str(s), t, "data_outputDir_sphere" + str(s) + "/t" + str(t))

if __name__ == "__main__":
    main()
