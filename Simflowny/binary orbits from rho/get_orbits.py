import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readfield(folder, field, level):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data

def get_orbit(rho, dom_min, dx):

    rho1 = 0
    rho2 = 0
    center1 = [0, 0]
    center2 = [0, 0]
    for (i,j), v in np.ndenumerate(rho):
        x_fd = dom_min[0] + i * dx[0]
        y_fd = dom_min[1] + j * dx[1]


        if x_fd < 0:
            if rho1 < v:
                rho1 = v
                center1 = [x_fd, y_fd]
        else:
            if rho2 < v:
                rho2 = v
                center2 = [x_fd, y_fd]
    
    return center1, center2


def main():

    level = 6
    dom_min = [-30.08, -30.08]
    dx = [10.24/(2**level), 10.24/(2**level)]
    print(dom_min)
    print(dx)
    outfile=open('orbits',"w+")
    outfile.write('#t cen1(x,y) cen2(x,y)\n')
    for t in range(0, 128001, 6400):
        rho = readfield("./datos_orbita/visit_dump." + ( "%05d" % t), 'rhof', level)
        center1, center2 =get_orbit(rho, dom_min, dx)
        print(t, center1[0], center1[1], center2[0], center2[1])
        outfile.write("%12.3e %12.3e %12.3e %12.3e %12.3e\n" % (t, center1[0], center1[1], center2[0], center2[1]))
    outfile.close()


if __name__ == "__main__":
    main()
