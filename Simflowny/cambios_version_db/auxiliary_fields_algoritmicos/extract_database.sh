DOCKER_CONTAINER=distracted_hamilton

docker cp DBDownloader-3.0.jar $DOCKER_CONTAINER:deploy/.
docker start $DOCKER_CONTAINER

LOG=`docker logs $DOCKER_CONTAINER | grep "Download completed!!"`
LOG=${LOG: : -1}
while [ "$LOG" != "Download completed!!" ]; do
    sleep 10
    LOG=`docker logs $DOCKER_CONTAINER | grep "Download completed!!"`
    LOG=${LOG: : -1}
    echo $LOGS
done

docker cp $DOCKER_CONTAINER:db_downloaded .
docker exec $DOCKER_CONTAINER rm -rf db_downloaded deploy/DBDownloader-3.0.jar