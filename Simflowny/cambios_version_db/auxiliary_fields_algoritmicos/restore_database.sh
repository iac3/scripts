DOCKER_CONTAINER=distracted_hamilton
DOCKER_VOLUME=storage_aux

mv db_downloaded db_uploader


docker stop $DOCKER_CONTAINER
docker exec $DOCKER_CONTAINER rm -rf db
docker run --rm -v $DOCKER_VOLUME:/db $DOCKER_CONTAINER rm -rf db
docker start $DOCKER_CONTAINER

docker cp db_uploader $DOCKER_CONTAINER:.
docker cp DBUploader-3.0.jar $DOCKER_CONTAINER:deploy/.

LOG=`docker logs $DOCKER_CONTAINER | grep "Updload completed!!"`
while [ ! -z "$LOG" ]; do
    sleep 10
    LOG=`docker logs $DOCKER_CONTAINER | grep "Updload completed!!"`
    echo $LOGS
done

docker exec $DOCKER_CONTAINER rm -rf db_downloaded deploy/DBUploader-3.0.jar
docker exec $DOCKER_CONTAINER rm -rf db_uploader

