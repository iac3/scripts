import h5py
import os
import shutil
from os import listdir
from os.path import isfile, join
import sys
import numpy as np

source_folder = "checkpoint.restart/restore.000400/nodes.0000004"
new_folder = "checkpoint.restart_new/restore.000400/nodes.0000004"

def appendNewField(f_tmp, field, default_value):

    print("Adding field "+field)
    patchHier = f_tmp['/PatchHierarchy']
    nlevels = f_tmp['/PatchHierarchy/d_number_levels'][0]
    #Iterate all levels
    for l in range(nlevels):
        patchHier = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4)]
        keys = list(patchHier.keys())
        for k in keys:
            #Find all level patches and blocks
            if k.startswith("level_"):
                #print(k)
                field_parent_group = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/' + k]
                namelist = field_parent_group['patch_data_namelist'][...].astype(str)
                original_dtype = field_parent_group['patch_data_namelist'][...].dtype
                print(namelist)
                #Get and clean field names
                fields = [w[:w.index("#")] for w in namelist]
                #print(fields)
                if field in fields:
                    sys.exit("appendNewField: Field " + field + " already in checkpoint")
                #Take first field as template
                field_template_name = namelist[0]
                field_template = field_parent_group[field_template_name]
                #Create new field
                new_field = field_parent_group.create_group(field + field_template_name[field_template_name.index("#"):])
                #Copy all template content
                for dset in field_template.keys():
                    field_template.copy(dset, new_field)
                #Empty new field data
                new_field['d_data/d_array'][...] = default_value
                #Set metadata
                namelist = np.append(namelist, field + field_template_name[field_template_name.index("#"):])
                attributes = field_parent_group['patch_data_namelist'].attrs
                del field_parent_group['patch_data_namelist']
                new_namelist = field_parent_group.create_dataset('patch_data_namelist', data = namelist.astype(original_dtype))
                for k in attributes.keys():
                    new_namelist.attrs[k] = attributes[k]
                field_parent_group['patch_data_namelist_count'][...] = len(namelist)

def removeField(f_tmp, field):

    print("Removing field "+field)
    patchHier = f_tmp['/PatchHierarchy']
    nlevels = f_tmp['/PatchHierarchy/d_number_levels'][0]
    #Iterate all levels
    for l in range(nlevels):
        patchHier = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4)]
        keys = list(patchHier.keys())
        for k in keys:
            #Find all level patches and blocks
            if k.startswith("level_"):
                #print(k)
                field_parent_group = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/' + k]
                namelist = field_parent_group['patch_data_namelist'][...].astype(str)
                original_dtype = field_parent_group['patch_data_namelist'][...].dtype
                print(namelist)
                #Get and clean field names
                fields = [w[:w.index("#")] for w in namelist]
                #print(fields)
                if field not in fields:
                    sys.exit("removeField: Field " + field + " does not exist in checkpoint")
                #Take first field as template
                field_template_name = namelist[0]
                field_template = field_parent_group[field_template_name]
                #Set metadata
                namelist = np.delete(namelist, np.where(namelist == field + field_template_name[field_template_name.index("#"):]))
                attributes = field_parent_group['patch_data_namelist'].attrs
                del field_parent_group['patch_data_namelist']
                new_namelist = field_parent_group.create_dataset('patch_data_namelist', data = namelist.astype(original_dtype))
                for k in attributes.keys():
                    new_namelist.attrs[k] = attributes[k]
                field_parent_group['patch_data_namelist_count'][...] = len(namelist)
                #Remove field
                del field_parent_group[field + field_template_name[field_template_name.index("#"):]]

def changeVariableName(f_tmp, oldField, newField):

    patchHier = f_tmp['/PatchHierarchy']
    nlevels = f_tmp['/PatchHierarchy/d_number_levels'][0]
    #Iterate all levels
    for l in range(nlevels):
        patchHier = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4)]
        keys = list(patchHier.keys())
        for k in keys:
            #Find all level patches and blocks
            if k.startswith("level_"):
                #print(k)
                field_parent_group = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/' + k]
                namelist = field_parent_group['patch_data_namelist'][...].astype(str)
                original_dtype = field_parent_group['patch_data_namelist'][...].dtype
                print(namelist)
                #Get and clean field names
                fields = [w[:w.index("#")] for w in namelist]
                #print(fields)
                if oldField not in fields:
                    sys.exit("changeVariableName: Field " + oldField + " does not exist in checkpoint")
                #Take first field as template
                field_template_name = namelist[0]
                field_template = field_parent_group[field_template_name]
                #Create new field
                new_field = field_parent_group.create_group(newField + field_template_name[field_template_name.index("#"):])
                #Copy all template content
                for dset in field_template.keys():
                    field_template.copy(dset, new_field)
                #Set metadata
                namelist = np.append(namelist, newField + field_template_name[field_template_name.index("#"):])
                namelist = np.delete(namelist, np.where(namelist == oldField + field_template_name[field_template_name.index("#"):]))
                attributes = field_parent_group['patch_data_namelist'].attrs
                del field_parent_group['patch_data_namelist']
                new_namelist = field_parent_group.create_dataset('patch_data_namelist', data = namelist.astype(original_dtype))
                for k in attributes.keys():
                    new_namelist.attrs[k] = attributes[k]
                #Remove field
                del field_parent_group[oldField + field_template_name[field_template_name.index("#"):]]

def setFieldToValue(f_tmp, field, value):

    patchHier = f_tmp['/PatchHierarchy']
    nlevels = f_tmp['/PatchHierarchy/d_number_levels'][0]
    #Iterate all levels
    for l in range(nlevels):
        patchHier = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4)]
        keys = list(patchHier.keys())
        for k in keys:
            #Find all level patches and blocks
            if k.startswith("level_"):
                #print(k)
                field_parent_group = f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/' + k]
                namelist = field_parent_group['patch_data_namelist'][...].astype(str)
                original_dtype = field_parent_group['patch_data_namelist'][...].dtype
                print(namelist)
                suffix = (namelist[0])[namelist[0].index("#"):]
                print(suffix)
                #Get and clean field names
                fields = [w[:w.index("#")] for w in namelist]
                #print(fields)
                if field not in fields:
                    sys.exit("setFieldToValue: Field " + field + " does no exist in checkpoint")
                #Empty new field data
                field_parent_group[field + suffix + '/d_data/d_array'][...] = value
                


def main():

    #Create new folder
    if not os.path.exists(new_folder):
        os.makedirs(new_folder)

    #Iterate all processor files
    for f in listdir(source_folder):
        shutil.copy(join(source_folder, f), join(new_folder, f))
        f_tmp = h5py.File(join(new_folder, f), "r+")
        print(join(new_folder, f))

        #appendNewField(f_tmp, "dpfdye", 0)
        #appendNewField(f_tmp, "t_optdeptha")
        #appendNewField(f_tmp, "t_optdepthx")
        #appendNewField(f_tmp, "t_optdepthe")
        #removeField(f_tmp, "psi")
        #removeField(f_tmp, "K")
        #removeField(f_tmp, "Qu_x")
        #removeField(f_tmp, "Qu_y")
        #removeField(f_tmp, "Qu_z")
        #removeField(f_tmp, "APH")
        #removeField(f_tmp, "Q")
        #changeVariableName(f_tmp, "d_i_gtd_zz_gtd_xy_gtd_xz_gtd_yz_Atd_xx_Atd_yy_Atd_zz_Atd_xy_Atd_xz_Atd_yz_Alpha_trK_phiR_phiI_pheR_pheI_piR_piI_peR_peI_chi_Gamh_x_Gamh_y_Gamh_z_theta_phi2_psi_K_Betau_x_Betau_y_Betau_z_Qu_x_Qu_y_Qu_z_APH_Q_gtd_xx_gtd_yy", "d_i_phi2_gtd_xx_gtd_xy_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Betau_x_Betau_y_Betau_z_Gamh_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI")
        #changeVariableName(f_tmp, "d_j_gtd_zz_gtd_xy_gtd_xz_gtd_yz_Atd_xx_Atd_yy_Atd_zz_Atd_xy_Atd_xz_Atd_yz_Alpha_trK_phiR_phiI_pheR_pheI_piR_piI_peR_peI_chi_Gamh_x_Gamh_y_Gamh_z_theta_phi2_psi_K_Betau_x_Betau_y_Betau_z_Qu_x_Qu_y_Qu_z_APH_Q_gtd_xx_gtd_yy", "d_j_phi2_gtd_xx_gtd_xy_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Betau_x_Betau_y_Betau_z_Gamh_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI")
        #changeVariableName(f_tmp, "d_k_gtd_zz_gtd_xy_gtd_xz_gtd_yz_Atd_xx_Atd_yy_Atd_zz_Atd_xy_Atd_xz_Atd_yz_Alpha_trK_phiR_phiI_pheR_pheI_piR_piI_peR_peI_chi_Gamh_x_Gamh_y_Gamh_z_theta_phi2_psi_K_Betau_x_Betau_y_Betau_z_Qu_x_Qu_y_Qu_z_APH_Q_gtd_xx_gtd_yy", "d_k_phi2_gtd_xx_gtd_xy_gtd_xz_gtd_yy_gtd_yz_gtd_zz_Atd_xx_Atd_xy_Atd_xz_Atd_yy_Atd_yz_Atd_zz_Gamh_x_Gamh_y_Betau_x_Betau_y_Betau_z_Gamh_z_Alpha_chi_trK_theta_phiR_pheR_phiI_pheI_piR_peR_piI_peI")

        setFieldToValue(f_tmp, "Phi", 1)

        f_tmp.close()

if __name__ == "__main__":
    main()

