import h5py
import os
import shutil
from os import listdir
from os.path import isfile, join

source_folder = "checkpoint.restart/restore.000001/"
old_nprocs = 1
new_nprocs = 8

def increaseNumberOfProcessors():
    if new_nprocs <= old_nprocs:
        raise Exception("Number of processors for the new checkpoint must be greater than the original one.")

    #Create new folder
    new_source_folder = source_folder + "nodes." + str(new_nprocs).zfill(7)
    if not os.path.exists(new_source_folder):
        os.makedirs(new_source_folder)

    #Copy old checkpoint files into new folder
    old_source_folder = source_folder + "nodes." + str(old_nprocs).zfill(7)
    for i in xrange(old_nprocs):
        shutil.copy(old_source_folder + '/proc.'+ str(i).zfill(7), new_source_folder + '/proc.'+ str(i).zfill(7))
        f = h5py.File(new_source_folder + '/proc.'+ str(i).zfill(7), "r+")
        levels = f['/PatchHierarchy/d_number_levels'][0]
        for l in xrange(levels):
            #Change number of procs
            f['/PatchHierarchy/level_' + str(l).zfill(4) + '/mapped_box_level/d_nproc'][0] = new_nprocs
        f.close()

    #Create new checkpoint files based in an original one
    for i in xrange(old_nprocs, new_nprocs):
        shutil.copy(old_source_folder + '/proc.'+ str(0).zfill(7), new_source_folder + '/tmp_proc.'+ str(i).zfill(7))
        f_tmp = h5py.File(new_source_folder + '/tmp_proc.'+ str(i).zfill(7), "r+")
        levels = f_tmp['/PatchHierarchy/d_number_levels'][0]
        for l in xrange(levels):
            #Change number of procs
            f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/mapped_box_level/d_nproc'][0] = new_nprocs
            #Change processor rank
            f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/mapped_box_level/d_rank'][0] = i
            #Unlink boxes
            f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/mapped_box_level/mapped_boxes/mapped_box_set_size'][0] = 0
            #Delete data
            for k in f_tmp['/PatchHierarchy/level_' + str(l).zfill(4)].keys():
                if k.startswith("level_"):
                    del f_tmp['/PatchHierarchy/level_' + str(l).zfill(4) + '/' + k]
        f_tmp.flush()

        #Move data from temporal to definitive file. Otherwise, the file keeps the original size (as if the datasets were not removed)
        f = h5py.File(new_source_folder + '/proc.'+ str(i).zfill(7), "w")
        for k in f_tmp['/'].keys():
            f_tmp.copy(k, f)
        f.close()
        f_tmp.close()
        os.remove(new_source_folder + '/tmp_proc.'+ str(i).zfill(7))

source_folder_ratio = "checkpoint.restart/restore.000001/nodes.0000004"
level = 2
ratio = 4
def changeRatio():
    for f in listdir(source_folder_ratio):
        f_tmp = h5py.File(join(source_folder_ratio, f), "r+")
        #Change ratio
        oldratio = f_tmp['/PatchHierarchy/ratio_to_coarser/level_' + str(level).zfill(4) + '_0'][0]
        for r in xrange(len(f_tmp['/PatchHierarchy/ratio_to_coarser/level_' + str(level).zfill(4) + '_0'])):
            f_tmp['/PatchHierarchy/ratio_to_coarser/level_' + str(level).zfill(4) + '_0'][r] = ratio
        #Change regrid interval
        f_tmp['/TimeRefinementIntegrator/regrid_interval'][level] = f_tmp['/TimeRefinementIntegrator/regrid_interval'][level - 1] * ratio
        #Change output intervals
        parentCurrentIteration = f_tmp['/MainRestartData/d_current_iteration'][level - 1]
        interval_timer = f_tmp['/MainRestartData/d_next_timer_output_iteration'][0] - parentCurrentIteration * oldratio
        if (f_tmp['/MainRestartData/d_next_timer_output_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_timer_output_iteration'][0]):
            f_tmp['/MainRestartData/d_next_timer_output_iteration'][0] = (f_tmp['/MainRestartData/d_next_timer_output_iteration'][0] - interval_timer)/oldratio * ratio + interval_timer
        interval_timer = f_tmp['/MainRestartData/d_next_console_output_iteration'] - parentCurrentIteration * oldratio
        if (f_tmp['/MainRestartData/d_next_console_output_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_console_output_iteration'][0]):
            f_tmp['/MainRestartData/d_next_console_output_iteration'][0] = (f_tmp['/MainRestartData/d_next_console_output_iteration'][0] - interval_timer)/oldratio * ratio + interval_timer
        if '/MainRestartData/d_next_analysis_dump_iteration' in f_tmp:
            interval_timer = f_tmp['/MainRestartData/d_next_analysis_dump_iteration'] - parentCurrentIteration * oldratio
            if (f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0]):
                f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0] = (f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0] - interval_timer)/oldratio * ratio + interval_timer
        if '/MainRestartData/d_next_slice_dump_iteration' in f_tmp:
            for s in xrange(len(f_tmp['/MainRestartData/d_next_slice_dump_iteration'])):
                interval_timer = f_tmp['/MainRestartData/d_next_slice_dump_iteration'][s] - parentCurrentIteration * oldratio
                if (f_tmp['/MainRestartData/d_next_slice_dump_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_slice_dump_iteration'][s]):
                    f_tmp['/MainRestartData/d_next_slice_dump_iteration'][s] = (f_tmp['/MainRestartData/d_next_slice_dump_iteration'][s] - interval_timer)/oldratio * ratio + interval_timer
        if '/MainRestartData/d_next_sphere_dump_iteration' in f_tmp:
            for s in xrange(len(f_tmp['/MainRestartData/d_next_sphere_dump_iteration'])):
                interval_timer = f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][s] - parentCurrentIteration * oldratio
                if (f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][s]):
                    f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][s] = (f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][s] - interval_timer)/oldratio * ratio + interval_timer
        if '/MainRestartData/d_next_sphere_dump_iteration' in f_tmp:
            for i in xrange(len(f_tmp['/MainRestartData/d_next_integration_dump_iteration'])):
                interval_timer = f_tmp['/MainRestartData/d_next_integration_dump_iteration'][i] - parentCurrentIteration * oldratio
                if (f_tmp['/MainRestartData/d_next_integration_dump_iteration'][0] > 0 and parentCurrentIteration * ratio > f_tmp['/MainRestartData/d_next_integration_dump_iteration'][i]):
                    f_tmp['/MainRestartData/d_next_integration_dump_iteration'][i] = (f_tmp['/MainRestartData/d_next_integration_dump_iteration'][i] - interval_timer)/oldratio * ratio + interval_timer

        f_tmp.close()

source_folder_dt = "checkpoint.restart/restore.000001/nodes.0000004"
newDt = 0.1
def changeDt():
    for f in listdir(source_folder_dt):
        f_tmp = h5py.File(join(source_folder_dt, f), "r+")
        levels = len(f_tmp['/TimeRefinementIntegrator/d_dt_actual_level'])
        accRatio = 1
        for l in xrange(levels):
            accRatio = accRatio * max(f_tmp['/PatchHierarchy/ratio_to_coarser/level_' + str(l).zfill(4) + '_0'])
            f_tmp['/TimeRefinementIntegrator/d_dt_actual_level'][l] = newDt / accRatio
            f_tmp['/TimeRefinementIntegrator/d_dt_max_level'][l] = newDt / accRatio


        f_tmp.close()        

source_folder_pnb = "checkpoint.restart/restore.000001/nodes.0000004"
properNestingBuffer = 6
def changeProperNestingBuffer():
      for f in listdir(source_folder_pnb):
        f_tmp = h5py.File(join(source_folder_pnb, f), "r+")
        buffers = len(f_tmp['/PatchHierarchy/proper_nesting_buffer'])
        for b in xrange(buffers):
            f_tmp['/PatchHierarchy/proper_nesting_buffer'][b] = properNestingBuffer

        f_tmp.close()  
        
def main():
    increaseNumberOfProcessors()
    changeRatio()
    changeDt()
    changeProperNestingBuffer()

if __name__ == "__main__":
    main()

