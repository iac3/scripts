import h5py
import os
import shutil
from os import listdir
from os.path import isfile, join

source_folder_ratio = "checkpoint.restart/restore.000020/nodes.0000001"
period_mesh = 1
period_analysis = 1
period_slices = 1
period_spheres = 1
period_integration = 1
period_point = 1
def restoreDump():
    for f in listdir(source_folder_ratio):
        f_tmp = h5py.File(join(source_folder_ratio, f), "r+")
        currentCycle = f_tmp['/MainRestartData/d_current_iteration'][0]
        if '/MainRestartData/d_next_mesh_dump_iteration' in f_tmp:
            if (f_tmp['/MainRestartData/d_next_mesh_dump_iteration'][0] > 0):
                f_tmp['/MainRestartData/d_next_mesh_dump_iteration'][0] = currentCycle + period_mesh
        if '/MainRestartData/d_next_analysis_dump_iteration' in f_tmp:
            if (f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0] > 0):
                f_tmp['/MainRestartData/d_next_analysis_dump_iteration'][0] = currentCycle + period_analysis
        '''
        if '/MainRestartData/d_next_slice_dump_iteration' in f_tmp:
            for s in xrange(len(f_tmp['/MainRestartData/d_next_slice_dump_iteration'])):
                if (f_tmp['/MainRestartData/d_next_slice_dump_iteration'][0] > 0):
                    f_tmp['/MainRestartData/d_next_slice_dump_iteration'][s] = currentCycle + period_slices
        if '/MainRestartData/d_next_sphere_dump_iteration' in f_tmp:
            for s in xrange(len(f_tmp['/MainRestartData/d_next_sphere_dump_iteration'])):
                if (f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][0] > 0):
                    f_tmp['/MainRestartData/d_next_sphere_dump_iteration'][s] = currentCycle + period_spheres
        if '/MainRestartData/d_next_sphere_dump_iteration' in f_tmp:
            for i in xrange(len(f_tmp['/MainRestartData/d_next_integration_dump_iteration'])):
                if (f_tmp['/MainRestartData/d_next_integration_dump_iteration'][0] > 0):
                    f_tmp['/MainRestartData/d_next_integration_dump_iteration'][i] = currentCycle + period_integration
        if '/MainRestartData/d_next_point_dump_iteration' in f_tmp:
            for i in xrange(len(f_tmp['/MainRestartData/d_next_point_dump_iteration'])):
                if (f_tmp['/MainRestartData/d_next_point_dump_iteration'][0] > 0):
                    f_tmp['/MainRestartData/d_next_point_dump_iteration'][i] = currentCycle + period_point
        '''
        f_tmp.close()
 
        
def main():
    restoreDump()

if __name__ == "__main__":
    main()

