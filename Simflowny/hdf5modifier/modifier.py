import sys, getopt
import numpy as np
import h5py
from scipy import interpolate
from os import listdir
from os.path import isfile, join


def modify_value(file):
    print file
    #Read mesh information
    f_sum = h5py.File(file, "r+")

    values = [10000, 2*10000, 4*10000, 8*10000, 16*10000, 32*10000, 64*10000, 128*10000]

    data = f_sum['/TimeRefinementIntegrator/regrid_interval']

    print data[...]

    data[...] = values

    print data[...]


    f_sum.close()


def main():

    for f in listdir("."):
        if isfile(join(".", f)):
            if "proc" in f:
                modify_value(f)

if __name__ == "__main__":
    main()
