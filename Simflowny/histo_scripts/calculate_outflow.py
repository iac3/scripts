import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py
from commons import calculate_histogram, readData, mask, saveData



def main():

    level = 1
    t = 1
    dx = 1.6
    folder = "../RePrimAnd/BSC/outputDir_slice0"

    gtd_xx = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_xx_' + str(level) + '.hdf5', 'gtd_xx')
    gtd_xy = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_xy_' + str(level) + '.hdf5', 'gtd_xy')
    gtd_xz = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_xz_' + str(level) + '.hdf5', 'gtd_xz')
    gtd_yy = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_yy_' + str(level) + '.hdf5', 'gtd_yy')
    gtd_yz = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_yz_' + str(level) + '.hdf5', 'gtd_yz')
    gtd_zz = readData(folder + "/visit_dump." + str(t).zfill(5) + '/gtd_zz_' + str(level) + '.hdf5', 'gtd_zz')

    detgtd = gtd_xx*gtd_yy*gtd_zz-gtd_xx*gtd_yz*gtd_yz-gtd_xy*gtd_xy*gtd_zz+2*gtd_xy*gtd_xz*gtd_yz-gtd_xz*gtd_xz*gtd_yy
    idetgtd = 1.0/detgtd
    gtu_xx = idetgtd*(gtd_yy*gtd_zz-gtd_yz*gtd_yz)
    gtu_xy = idetgtd*(-gtd_xy*gtd_zz+gtd_xz*gtd_yz)
    gtu_xz = idetgtd*(gtd_xy*gtd_yz-gtd_xz*gtd_yy)
    gtu_yy = idetgtd*(gtd_xx*gtd_zz-gtd_xz*gtd_xz)
    gtu_yz = idetgtd*(-gtd_xx*gtd_yz+gtd_xy*gtd_xz)
    gtu_zz = idetgtd*(gtd_xx*gtd_yy-gtd_xy*gtd_xy)


    v_x = readData(folder + "/visit_dump." + str(t).zfill(5) + '/vfd_x_' + str(level) + '.hdf5', 'vfd_x')
    v_y = readData(folder + "/visit_dump." + str(t).zfill(5) + '/vfd_y_' + str(level) + '.hdf5', 'vfd_y')
    v_z = readData(folder + "/visit_dump." + str(t).zfill(5) + '/vfd_z_' + str(level) + '.hdf5', 'vfd_z')

    chi = readData(folder + "/visit_dump." + str(t).zfill(5) + '/chi_' + str(level) + '.hdf5', 'chi')

    v2 = chi * (gtu_xx * v_x * v_x + 2 * gtu_xy * v_x * v_y + 2 * gtu_xz * v_x * v_z + gtu_yy * v_y * v_y + 2 * gtu_yz * v_y * v_z + gtu_zz * v_z * v_z)

    W2 = 1.0 / (1.0 - v2)

    alpha = readData(folder + "/visit_dump." + str(t).zfill(5) + '/Alpha_' + str(level) + '.hdf5', 'Alpha')
    alpha_W2 = alpha * W2
    alphaW2_unbound = mask(1.0, alpha_W2)
    saveData("alphaW_unbound.h5", "alphaW_unbound", alphaW2_unbound)
    alphaW2_bound = mask(1.0, -alpha_W2 + 2.0)
    saveData("alphaW_bound.h5", "alphaW_bound", alphaW2_bound)
    v = np.sqrt(v2)
    saveData("v.h5", "v", v)
    res = v * alphaW2_unbound
    D = readData(folder + "/visit_dump." + str(t).zfill(5) + '/Df_' + str(level) + '.hdf5', 'Df')
    calculate_histogram("histo", 12, 0.001, 0.61, D, res, dx)


if __name__ == "__main__":
    main()
