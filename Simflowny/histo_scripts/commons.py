import h5py
import numpy as np


def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset

def saveData(f, v, data):
    f = h5py.File(f, "w")
    dset = f.create_dataset(v, data=data)
    f.close()

def mask(value, field):
    return field >= value

def calculate_histogram(filename, numbins, hmin, hmax, reg, reg2, dx):
    dh = (hmax-hmin)/numbins
    histo = np.zeros((numbins))
    histobins = np.zeros((numbins + 1))
    for i in range(numbins):
        histobins[i] = hmin + i*dh
    histobins[numbins] = hmax

    numDims = len(reg.shape)     
    dV = 1.0
    for i in range(numDims):
        dV = dV * dx*0.5

    if numDims == 2:
        for j in range(reg.shape[1] - 1):
            for i in range(reg.shape[0] - 1):
                contrib = dV * (reg[i, j] + reg[i+1, j] + reg[i, j+1] + reg[i+1, j+1])
                binvalue = reg2[i, j]
                if (binvalue >= histobins[0] and  binvalue <= histobins[numbins - 1] ):
                    mybin = int((binvalue-histobins[0])/(histobins[1]-histobins[0]))
                    if mybin >= 1 and mybin <= numbins:
                        histo[mybin] = histo[mybin] + contrib
                    else:
                        print('grid_histo: big problem, mybin=',mybin)
    if numDims == 3:
        for k in range(reg.shape[2] - 1):
            for j in range(reg.shape[1] - 1):
                for i in range(reg.shape[0] - 1):
                    contrib = dV * (reg[i, j, k] + reg[i+1, j, k] + reg[i, j+1, k] + reg[i+1, j+1, k] + reg[i, j, k + 1] + reg[i+1, j, k + 1] + reg[i, j+1, k + 1] + reg[i+1, j+1, k + 1])
                    binvalue = reg2[i, j, k]
                    if (binvalue >= histobins[0] and  binvalue <= histobins[numbins - 1] ):
                        mybin = int((binvalue-histobins[0])/(histobins[1]-histobins[0]))
                        if mybin >= 1 and mybin <= numbins:
                            histo[mybin] = histo[mybin] + contrib
                        else:
                            print('grid_histo: big problem, mybin=',mybin)

    file  = open(filename, "w") 
    print('grid_histo: dV   = ',dV)
    file.write('grid_histo: dV   = ' + str(dV) + "\n")
    print('Histo: nbins: ',numbins)
    file.write('Histo: nbins: ' + str(numbins) + "\n")
    print('Histo: total mass: ', histo.sum())
    file.write('Histo: total mass: ' + str(histo.sum()) + "\n")
    for i in range(numbins):
        print('histo: Bin: ', i , histobins[i],histobins[i+1],histo[i])
        file.write('histo: Bin: ' + str(i) + str(histobins[i]) + str(histobins[i+1]) + str(histo[i]) + "\n")
    file.close()