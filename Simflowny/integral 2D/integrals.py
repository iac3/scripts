import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readfield(folder, field, level):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data

def get_integrals(field, time_list, dom_min, dx):

    integrals = np.zeros(time_list.shape)
    
    for (i_t,), t in np.ndenumerate(time_list):    
        rho = readfield("./datos_orbita/visit_dump.128000", field, level)
        cell_value = dx[0]*dx[1] * np.sum(v*v)
        integrals[i_t] = cell_value
    
    plt.figure()
    plt.title('Time integrals')
    plt.plot(time_list, integrals)
    plt.savefig('integrals_.png')

    outfile=open('integrals',"w+")
    outfile.write('#time integral\n')
    print("Time Integral")
    for (i_t,), t in np.ndenumerate(time_list):
        outfile.write("%12.3e %12.3e\n" % (t, integrals[i_t]))
        print(t, integrals[i_t])
    outfile.close()

def main():

    level = 6
    dom_min = [-30.08, -30.08]
    dx = [10.24/(2**level), 10.24/(2**level)]
    dt = 0.03
    time_list = np.arange(0, 30, 10) * dt
    field = 'rhof'
    print(dom_min)
    print(dx)
    get_integrals(field, time_list, dom_min, dx)
        
    


if __name__ == "__main__":
    main()
