import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readfield(folder, field, level):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data


def get_mass_center_indices(field, dom_min, dx):

    rho = 0
    x = 0
    y = 0
    M = 0
    for (i,j), v in np.ndenumerate(field):
        x_fd = dom_min[0] + i * dx[0]
        y_fd = dom_min[1] + j * dx[1]
        x = x + x_fd * v * dx[0]*dx[1] 
        y = y + y_fd * v * dx[0]*dx[1] 
        M = M + v * dx[0]*dx[1]

    x = x / M
    y = y / M

    i_x = (x - dom_min[0])/dx[0]
    j_y = (y - dom_min[1])/dx[1]
    
    center = [i_x, j_y]

    return center

def get_center_indices(field):

    rho = 0
    center = [-1, -1]
    for (i,j), v in np.ndenumerate(field):
        if rho < v:
            rho = v
            center = [i, j]
    
    return center


def get_integrals(time, field, radius_list, dom_min, dx, center_indices):

    integrals = np.zeros(radius_list.shape)
    center = dom_min + center_indices * dx
    for (i,j), v in np.ndenumerate(field):
        x_fd = dom_min[0] + i * dx[0]
        y_fd = dom_min[1] + j * dx[1]
        distance = np.sqrt((x_fd-center[0])**2 + (y_fd-center[1])**2)
        cell_value = dx[0]*dx[1] * v

        for (i_r,), r in np.ndenumerate(radius_list):
            if distance <= r:
                integrals[i_r] = integrals[i_r] + cell_value
    
    plt.figure()
    plt.title('Radius integrals')
    plt.plot(radius_list, integrals)
    plt.savefig('integrals_' + str(time) + '.png')

    outfile=open('integrals_' + str(time),"w+")
    outfile.write('#radius integral\n')
    print("Radius Integral")
    for (i_r,), r in np.ndenumerate(radius_list):
        outfile.write("%12.3e %12.3e\n" % (r, integrals[i_r]))
        print(r, integrals[i_r])
    outfile.close()

def main():

    level = 6
    dom_min = [-30.08, -30.08]
    dx = [10.24/(2**level), 10.24/(2**level)]
    radius_list = np.arange(1, 30, 0.2)
    print(dom_min)
    print(dx)
    rho = readfield("./datos_orbita/visit_dump.128000", 'rhof', level)
    #center = np.array(get_center_indices(rho))
    center = np.array(get_mass_center_indices(rho, dom_min, dx))
    print('Center', dom_min + center * dx)
    get_integrals('1000', rho, radius_list, dom_min, dx, center)
        
    


if __name__ == "__main__":
    main()
