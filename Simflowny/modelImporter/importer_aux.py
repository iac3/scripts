import lxml.etree as ET


def main():
    #transformers
    preprocess_transformer = "transformers/maple/toSimflowny_clean.xsl"
    model_transformer = "transformers/maple/toSimflowny_highOrder_create_aux.xsl"
    problem_transformer = "transformers/maple/toSimflowny_highOrder_createProblem.xsl"
    analysis_transformer = "transformers/maple/toSimflowny_highOrder_createAnalysis.xsl"

    #Input file
    model_file = "ccz4_sf_mhd.xml"
    analysis_file = "ccz4_sf_mhd_analysis.xml"

    ns = {'mms': 'urn:mathms'}

    dom_model = ET.parse(model_file)
    xslt_preprocess = ET.XSLT(ET.parse(preprocess_transformer))
    preprocessed = xslt_preprocess(dom_model)
    xslt_model = ET.XSLT(ET.parse(model_transformer))
    model = xslt_model(preprocessed)
    model.write("model.xml")
    xslt_problem = ET.XSLT(ET.parse(problem_transformer))
    problem = xslt_problem(preprocessed)
    if analysis_file != "":
        dom_analysis = ET.parse(analysis_file)
        preprocessed_analysis = xslt_preprocess(dom_analysis)
        xslt_analysis = ET.XSLT(ET.parse(analysis_transformer))
        analysis = xslt_analysis(preprocessed_analysis)
        #Analysis fields
        analysisFields = analysis.findall("//mms:analysisFields", ns)
        auxiliaryVariables = problem.findall("//mms:auxiliaryVariables", ns)
        if len(auxiliaryVariables) > 0:
            problem.getroot().insert(problem.getroot().index(problem.findall("//mms:auxiliaryVariables", ns)[0]) + 1, analysisFields[0])
        else:
            auxiliaryFields = problem.findall("//mms:auxiliaryFields", ns)
            if len(auxiliaryFields) > 0:
                problem.getroot().insert(problem.getroot().index(problem.findall("//mms:auxiliaryFields", ns)[0]) + 1, analysisFields[0])
            else:
                problem.getroot().insert(problem.getroot().index(problem.findall("//mms:fields", ns)[0]) + 1, analysisFields[0])
        #Auxiliary analysis fields
        auxiliaryAnalysisVariables = analysis.findall("//mms:auxiliaryAnalysisVariables", ns)
        if len(auxiliaryAnalysisVariables) > 0:
            problem.getroot().insert(problem.getroot().index(problem.findall("//mms:analysisFields", ns)[0]) + 1, auxiliaryAnalysisVariables[0])
        #analysisFieldEquations
        analysisFieldEquations = analysis.findall("//mms:analysisFieldEquations", ns)
        problem.getroot().insert(problem.getroot().index(problem.findall("//mms:boundaryConditions", ns)[0]), analysisFieldEquations[0])
        #analysisFieldEquations
        auxiliaryAnalysisVariableEquations = analysis.findall("//mms:auxiliaryAnalysisVariableEquations", ns)
        if len(auxiliaryAnalysisVariableEquations) > 0:
            problem.getroot().insert(problem.getroot().index(problem.findall("//mms:boundaryConditions", ns)[0]), auxiliaryAnalysisVariableEquations[0])
        #parameters
        newParams = analysis.findall("//mms:parameters/*", ns)
        for p in newParams:
            name = p.find('./mms:name', ns)
            existing = problem.xpath("//mms:parameter/mms:name[text()='" + name.text + "']", namespaces=ns)
            if len(existing) == 0:
                problem.getroot().findall("./mms:parameters", ns)[0].insert(0, p)



    problem.write("problem.xml")

    

if __name__ == "__main__":
    main()
