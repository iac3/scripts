<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mt="http://www.w3.org/1998/Math/MathML" version="1.0" xmlns:exsl="http://exslt.org/common"  extension-element-prefixes="exsl">

    <xsl:param name="spatial_dimensions" select="3"></xsl:param>

   <xsl:variable name="coords">
      <Item>t</Item>
      <Item>x</Item>
      <Item>y</Item>
      <Item>z</Item>
   </xsl:variable>

   <!-- Meta information -->
    <xsl:template match="//mt:math">
        <mt:math>
         <meta>
            <coord>x</coord>
            <coord>y</coord>
            <coord>z</coord>
         </meta>
         <xsl:apply-templates select="./*"/>
        </mt:math>
    </xsl:template>


   <!-- Replace signum -->
   <xsl:template match="//mt:apply[mt:csymbol = 'signum']">
      <mt:apply>
         <mt:minus/>
         <mt:apply>
            <mt:gt/>
            <xsl:apply-templates select="./*[2]"/>  
            <mt:cn>0</mt:cn>
         </mt:apply>
         <mt:apply>
            <mt:lt/>
            <xsl:apply-templates select="./*[2]"/>  
            <mt:cn>0</mt:cn>
         </mt:apply>
      </mt:apply>
   </xsl:template>

       <!-- Simplification of 1 * A -->
   <xsl:template match="//mt:apply[mt:times and mt:cn[text() = '1.0' or text() = '1']]">
      <xsl:choose>
         <xsl:when test="count(./*) > 3">
            <mt:apply>
               <mt:times/>
               <xsl:for-each select="./*[position() > 1]">
                  <xsl:if test="./self::*[not(text() = '1.0' or text() = '1')]">
                     <xsl:apply-templates select="."/>      
                  </xsl:if>
               </xsl:for-each>
            </mt:apply>
         </xsl:when>
         <xsl:otherwise>
            <xsl:for-each select="./*[position() > 1]">
               <xsl:if test="./self::*[not(text() = '1.0' or text() = '1')]">
                  <xsl:apply-templates select="."/>      
               </xsl:if>
            </xsl:for-each>
         </xsl:otherwise>
      </xsl:choose>
      
   </xsl:template>
       
    <!-- Develop power of derivatives -->
    <xsl:template match="//mt:apply[mt:power and mt:apply/mt:partialdiff]">
        <xsl:variable name="pow" select="floor(./mt:cn)"/>
        <xsl:variable name="curElement" select="."/>
        <mt:apply>
            <mt:times/>
           <xsl:call-template name="powerDerivatives">
              <xsl:with-param name="counter" select="$pow"/>
           </xsl:call-template>
        </mt:apply>
    </xsl:template>

   <xsl:template name="powerDerivatives">
      <xsl:param name="counter"/>
      <xsl:if test="$counter &gt; 0">
         <xsl:call-template name="powerDerivatives">
            <xsl:with-param name="counter" select="$counter - 1"/>
         </xsl:call-template>
         <xsl:apply-templates select="./mt:apply[mt:partialdiff]"/>
      </xsl:if>
   </xsl:template>

<xsl:template match="//mt:cn[ starts-with(text(), '-')]">
    <mt:apply>
        <mt:minus/>
        <mt:cn><xsl:value-of select=" substring(., 2)"></xsl:value-of></mt:cn>
    </mt:apply>
</xsl:template>


   <!-- minus preceding operator -->
   <xsl:template match="//mt:apply[mt:minus and mt:apply/*[position() = 1][local-name() = 'ci'] and count(*) = 2]">
      <xsl:variable name="opName" select="./mt:apply/mt:ci/text()"/>
      <xsl:choose>
         <xsl:when test="./mt:apply/mt:apply/*[1][local-name() = 'plus']">
            <mt:apply operator="{$opName}">
               <mt:plus/>
               <xsl:for-each select="./mt:apply/mt:apply/*[position() > 1]">
                  <mt:apply>
                     <mt:minus/>
                     <xsl:apply-templates select="."/>
                  </mt:apply>
               </xsl:for-each>
            </mt:apply>
         </xsl:when>
         <xsl:when test="./mt:apply[*[position()= 1 and local-name() = 'ci'] and *[position()> 1 and local-name() = 'apply']]">
            <mt:apply operator="{$opName}">
               <mt:minus/>
               <xsl:apply-templates select="./mt:apply/mt:apply"/>
            </mt:apply>
         </xsl:when>
         <xsl:otherwise>
            <mt:apply>
               <mt:minus/>
               <xsl:apply-templates select="./mt:apply"/>
            </mt:apply>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

    <!-- Terms in equations must be in a summation -->
    <xsl:template match="//mt:apply[mt:minus and preceding-sibling::mt:eq and count(*) > 2]">
        <mt:apply>
            <mt:plus/>
            <xsl:apply-templates select="./*[2]"/>
           <xsl:choose>
              <xsl:when test="./*[3][@operator]/*[1][local-name() = 'ci']">
                 <xsl:variable name="opName" select="/*[3]/@operator"/>
                 <xsl:choose>
                    <xsl:when test="./*[3]/mt:apply/*[1][local-name() = 'plus']">
                       <mt:apply operator="{$opName}">
                          <mt:plus/>
                          <xsl:for-each select="./*[3]/mt:apply/*[position() > 1]">
                             <mt:apply>
                                <mt:minus/>
                                <xsl:apply-templates select="."/>
                             </mt:apply>
                          </xsl:for-each>
                       </mt:apply>
                    </xsl:when>
                    <xsl:otherwise>
                       <mt:apply operator="{$opName}">
                          <mt:minus/>
                          <xsl:apply-templates select="./*[3]/mt:apply"/>
                       </mt:apply>
                    </xsl:otherwise>
                 </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                 <mt:apply>
                    <mt:minus/>
                    <xsl:apply-templates select="./*[3]"/>
                 </mt:apply>
              </xsl:otherwise>
           </xsl:choose>
     
        </mt:apply>
    </xsl:template>

    <!-- Fields -->
    <xsl:template match="//mt:apply[*[position()= 1 and local-name() = 'apply']]">
        <xsl:apply-templates select="./*[1]"/>
    </xsl:template>
    
    <xsl:template match="//mt:apply[*[position()= 1 and local-name() = 'ci']]">
        <xsl:choose>
            <xsl:when test="starts-with(./*[position()= 1 and local-name() = 'ci']/text(), 'tmp_')">
                <mt:ci type="variable"><xsl:value-of select="substring(./*[1], 5)"/></mt:ci>
            </xsl:when>
            <xsl:when test="starts-with(./*[position()= 1 and local-name() = 'ci']/text(), 'fluxes_')">
                <mt:ci type="flux"><xsl:value-of select="substring(./*[1], 8)"/></mt:ci>
            </xsl:when>
            <xsl:when test="starts-with(./*[position()= 1 and local-name() = 'ci']/text(), 'sources_')">
                <mt:ci type="source"><xsl:value-of select="substring(./*[1], 9)"/></mt:ci>
            </xsl:when>
           <xsl:when test="./*[position()> 1 and local-name() = 'apply']">
              <xsl:variable name="opName" select="./*[1]/text()"/>
              <mt:apply operator="{$opName}"><xsl:apply-templates select="./*[2]/*"/></mt:apply>
           </xsl:when>
            <xsl:otherwise>
                <mt:ci type="field"><xsl:value-of select="./*[1]/text()"/></mt:ci>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
   
   <!-- Terms diff/A must be 1/A * diff -->
   <xsl:template match="//mt:apply[mt:apply/mt:partialdiff and mt:divide]">
      <mt:apply>
         <mt:times/>
         <mt:apply>
            <mt:divide/>
             <mt:cn>1.0</mt:cn>
             <xsl:apply-templates select="./*[3]"/>
         </mt:apply>
         <xsl:apply-templates select="./*[2]"/>
      </mt:apply>
   </xsl:template>
    

    <xsl:template match="//mt:apply[*[position()= 1 and local-name() = 'selector']]">
        <xsl:choose>
           <xsl:when test="starts-with(./*[position()= 2 and local-name() = 'ci']/text(), 'delta')">
              <mt:ci>Δ<xsl:call-template name="replaceCoord"><xsl:with-param name="selector" select="./mt:cn/text()"></xsl:with-param></xsl:call-template></mt:ci>
           </xsl:when>
            <xsl:when test="starts-with(./*[position()= 2 and local-name() = 'ci']/text(), 'tmp_')">
                <mt:ci type="variable"><xsl:value-of select="substring(./*[2], 5)"/>_<xsl:for-each select="./*[position() > 2]"><xsl:call-template name="replaceCoord"><xsl:with-param name="selector" select="./text()"></xsl:with-param></xsl:call-template></xsl:for-each></mt:ci>
            </xsl:when>
            <xsl:when test="starts-with(./*[position()= 2 and local-name() = 'ci']/text(), 'fluxes_')">
                <mt:ci type="flux"><xsl:value-of select="substring(./*[2], 8)"/>_<xsl:for-each select="./*[position() > 2]"><xsl:call-template name="replaceCoord"><xsl:with-param name="selector" select="./text()"></xsl:with-param></xsl:call-template></xsl:for-each></mt:ci>
            </xsl:when>
            <xsl:when test="starts-with(./*[position()= 2 and local-name() = 'ci']/text(), 'sources_')">
                <mt:ci type="source"><xsl:value-of select="substring(./*[2], 9)"/>_<xsl:for-each select="./*[position() > 2]"><xsl:call-template name="replaceCoord"><xsl:with-param name="selector" select="./text()"></xsl:with-param></xsl:call-template></xsl:for-each></mt:ci>
            </xsl:when>
            <xsl:otherwise>
                <mt:ci type="field"><xsl:value-of select="./*[2]"/>_<xsl:for-each select="./*[position() > 2]"><xsl:call-template name="replaceCoord"><xsl:with-param name="selector" select="./text()"></xsl:with-param></xsl:call-template></xsl:for-each></mt:ci>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="mt:cn[ starts-with(text(), '.')]">
        <mt:cn>0<xsl:value-of select="."/></mt:cn>
    </xsl:template>

   <xsl:template name="replaceCoord">
      <xsl:param name="selector"></xsl:param>
      <xsl:value-of select="exsl:node-set($coords)/*[$selector + 1 mod 4]/text()"/>
   </xsl:template>

    <!-- Recursive copy -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- Namespace prefix -->
    <xsl:template match="*">
        <xsl:element name="mt:{name()}" namespace="http://www.w3.org/1998/Math/MathML">
            <xsl:copy-of select="namespace::*"/>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
