<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common"  extension-element-prefixes="exsl"
    xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms" xmlns:sml="urn:simml"
    version="1.0">
   
    <xsl:template match="mt:math">
        <mms:PDEModel>
            <mms:head>
                <mms:name/>
                <mms:id/>
                <mms:author/>
                <mms:version/>
                <mms:date>2020-01-01T00:00:00</mms:date>
            </mms:head>
            <mms:coordinates>
                <mms:spatialCoordinates>
                    <xsl:for-each select="meta/coord">
                        <mms:spatialCoordinate>
                            <xsl:value-of select="./text()"/>
                        </mms:spatialCoordinate>
                    </xsl:for-each>
                </mms:spatialCoordinates>
                <mms:timeCoordinate>t</mms:timeCoordinate>
            </mms:coordinates>
            <mms:fields>
                <xsl:for-each
                    select="//mt:apply[mt:eq]/*[position() = 2 and descendant-or-self::mt:partialdiff]/descendant-or-self::mt:ci[@type = 'field']">
                    <mms:field>
                        <xsl:apply-templates select="./text()"/>
                    </mms:field>
                </xsl:for-each>
            </mms:fields>
            <xsl:if
                test="count(//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'field' and not(starts-with(.,'fluxes_')) and not(starts-with(.,'sources_'))]) > 0">
                <mms:auxiliaryFields>
                    <xsl:for-each
                        select="//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'field' and not(starts-with(.,'fluxes_')) and not(starts-with(.,'sources_'))]">
                        <mms:auxiliaryField>
                            <xsl:apply-templates select="./text()"/>
                        </mms:auxiliaryField>
                    </xsl:for-each>
                </mms:auxiliaryFields>
            </xsl:if>
            <xsl:if
                test="count(//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']) > 0">
                <mms:auxiliaryVariables>
                    <xsl:for-each
                        select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']">
                        <mms:auxiliaryVariable>
                            <xsl:apply-templates select="./text()"/>
                        </mms:auxiliaryVariable>
                    </xsl:for-each>
                </mms:auxiliaryVariables>
            </xsl:if>
           <xsl:if test="count(//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]) > 0">
                <mms:parameters>
                   <xsl:for-each select="//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]">
                        <xsl:if
                           test="not(preceding::mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))][text() = current()/text()])">
                            <mms:parameter>
                                <mms:name>
                                    <xsl:apply-templates select="./text()"/>
                                </mms:name>
                                <mms:type>REAL</mms:type>
                            </mms:parameter>
                        </xsl:if>
                    </xsl:for-each>
                </mms:parameters>
            </xsl:if>
            <mms:evolutionEquations>
                <xsl:for-each
                    select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']]">
                    <mms:evolutionEquation>
                        <xsl:variable name="rhs" select="./*[position() = 3]"/>
                        <xsl:variable name="rhs_terms">
                            <xsl:call-template name="separateTerms">
                                <xsl:with-param name="rhs" select="$rhs"/>
                               <xsl:with-param name="operator" select="empty"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:variable name="flux_terms">
                            <xsl:call-template name="getFluxes">
                                <xsl:with-param name="rhs" select="$rhs"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:variable name="source_terms">
                            <xsl:call-template name="getSources">
                                <xsl:with-param name="rhs" select="$rhs"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <mms:name/>
                        <mms:field>
                            <xsl:value-of
                                select=".//mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']/mt:ci"
                            />
                        </mms:field>
                       <mms:referenceFrame>Eulerian</mms:referenceFrame>
                        <xsl:if test="$flux_terms">
                            <xsl:copy-of select="$flux_terms"/>    
                        </xsl:if>
                        <xsl:if test="$source_terms">
                            <xsl:copy-of select="$source_terms"/>    
                        </xsl:if>
                        <xsl:if test="exsl:node-set($rhs_terms)/*">
                           <!-- Default operator -->
                            <mms:operator>
                                <mms:name>Op</mms:name>
                                <xsl:for-each select="exsl:node-set($rhs_terms)/*">
                                    <mms:term>
                                        <xsl:call-template name="processTerm">
                                            <xsl:with-param name="term" select="."/>
                                        </xsl:call-template>
                                    </mms:term>
                                </xsl:for-each>
                            </mms:operator>
                        </xsl:if>
                       <xsl:for-each select="$rhs/descendant-or-self::mt:apply[not(preceding::mt:apply/@operator/text() = text())]/@operator">
                          <mms:operator>
                             <mms:name><xsl:value-of select="."/></mms:name>
                             <xsl:variable name="operatorName" select="."></xsl:variable>
                             <xsl:for-each select="$rhs/descendant-or-self::mt:apply[@operator = $operatorName]">
                                <xsl:variable name="rhs_terms_op">
                                   <xsl:call-template name="separateTerms">
                                      <xsl:with-param name="rhs" select="."/>
                                      <xsl:with-param name="operator" select="$operatorName"/>
                                   </xsl:call-template>
                                </xsl:variable>
                                <xsl:for-each select="exsl:node-set($rhs_terms_op)/*">
                                    <mms:term>
                                       <xsl:call-template name="processTerm">
                                          <xsl:with-param name="term" select="."/>
                                       </xsl:call-template>
                                    </mms:term>
                                </xsl:for-each>
                             </xsl:for-each>
                          </mms:operator>
                       </xsl:for-each>
                    </mms:evolutionEquation>
                </xsl:for-each>
            </mms:evolutionEquations>
            <xsl:if
                test="//mt:apply[*[position()= 1 and local-name() = 'eq'] and not(descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']) and *[position()= 2 and local-name() = 'ci' and @type = 'field']]">
                <mms:auxiliaryFieldEquations>
                    <xsl:for-each
                        select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and not(descendant::mt:apply[mt:partialdiff and mt:bvar/mt:ci/text() = 't']) and *[position()= 2 and local-name() = 'ci' and @type = 'field']]">
                        <mms:auxiliaryFieldEquation>
                            <mms:name/>
                            <mms:auxiliaryField>
                                <xsl:value-of select="./*[position() = 2]"/>
                            </mms:auxiliaryField>
                            <mms:definition>
                               <mms:algebraic>
                                  <xsl:variable name="rhs" select="./*[position() = 3]"/>
                                  <xsl:choose>
                                     <xsl:when test="$rhs/descendant-or-self::mt:apply/@operator">
                                        <xsl:for-each select="$rhs/descendant-or-self::mt:apply[not(preceding::mt:apply/@operator/text() = text())]/@operator">
                                           <mms:operator>
                                              <mms:name><xsl:value-of select="."/></mms:name>
                                              <xsl:variable name="operatorName" select="."></xsl:variable>
                                              <xsl:for-each select="$rhs/descendant-or-self::mt:apply[@operator = $operatorName]">
                                                 <xsl:variable name="rhs_terms_op">
                                                    <xsl:call-template name="separateTerms">
                                                       <xsl:with-param name="rhs" select="."/>
                                                       <xsl:with-param name="operator" select="$operatorName"/>
                                                    </xsl:call-template>
                                                 </xsl:variable>
                                                 <xsl:for-each select="exsl:node-set($rhs_terms_op)/*">
                                                    <mms:term>
                                                       <xsl:call-template name="processTerm">
                                                          <xsl:with-param name="term" select="."/>
                                                       </xsl:call-template>
                                                    </mms:term>
                                                 </xsl:for-each>
                                              </xsl:for-each>
                                           </mms:operator>
                                        </xsl:for-each>
                                     </xsl:when>
                                     <xsl:when test="$rhs/descendant-or-self::mt:apply[not(@operator) and mt:partialdiff]">
                                        <xsl:variable name="rhs_terms">
                                           <xsl:call-template name="separateTerms">
                                              <xsl:with-param name="rhs" select="$rhs"/>
                                              <xsl:with-param name="operator" select="empty"/>
                                           </xsl:call-template>
                                        </xsl:variable>
                                           <!-- Default operator -->
                                           <mms:operator>
                                              <mms:name>Op</mms:name>
                                              <xsl:for-each select="exsl:node-set($rhs_terms)/*">
                                                 <mms:term>
                                                    <xsl:call-template name="processTerm">
                                                       <xsl:with-param name="term" select="."/>
                                                    </xsl:call-template>
                                                 </mms:term>
                                              </xsl:for-each>
                                           </mms:operator>
                                     </xsl:when>
                                     <xsl:otherwise>
                                        <mt:math>
                                           <xsl:apply-templates select="./*[position() = 3]"/>
                                        </mt:math>
                                     </xsl:otherwise>
                                  </xsl:choose>
                               </mms:algebraic>
                            </mms:definition>
                        </mms:auxiliaryFieldEquation>
                    </xsl:for-each>
                </mms:auxiliaryFieldEquations>
            </xsl:if>
            <xsl:if
                test="//mt:apply[*[position()= 1 and local-name() = 'eq'] and *[position()= 2 and local-name() = 'ci' and @type = 'variable']]">
                <mms:auxiliaryVariableEquations>
                    <xsl:for-each
                        select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and *[position()= 2 and local-name() = 'ci' and @type = 'variable']]">
                        <mms:auxiliaryVariableEquation>
                            <mms:name/>
                            <mms:auxiliaryVariable>
                                <xsl:value-of select="./*[position() = 2]"/>
                            </mms:auxiliaryVariable>
                            <xsl:variable name="rhs" select="./*[position() = 3]"/>
                            <xsl:variable name="rhs_terms">
                                <xsl:call-template name="separateTerms">
                                    <xsl:with-param name="rhs" select="$rhs"/>
                                   <xsl:with-param name="operator" select="empty"/>
                                </xsl:call-template>
                            </xsl:variable>
                            <mms:definition>
                               <mms:algebraic>
                              <!-- Default operator -->
                              <xsl:if test="exsl:node-set($rhs_terms)/*">
                                 <mms:operator>
                                    <mms:name>Op</mms:name>
                                    <xsl:for-each select="exsl:node-set($rhs_terms)/*">
                                       <mms:term>
                                          <xsl:call-template name="processTerm">
                                             <xsl:with-param name="term" select="."/>
                                          </xsl:call-template>
                                       </mms:term>
                                    </xsl:for-each>
                                 </mms:operator>
                              </xsl:if>
                              <xsl:for-each select="$rhs/descendant-or-self::mt:apply[not(preceding::mt:apply/@operator/text() = text())]/@operator">
                                 <mms:operator>
                                    <mms:name><xsl:value-of select="."/></mms:name>
                                    <xsl:variable name="operatorName" select="."></xsl:variable>
                                    <xsl:for-each select="$rhs/descendant-or-self::mt:apply[@operator = $operatorName]">
                                       <xsl:variable name="rhs_terms_op">
                                          <xsl:call-template name="separateTerms">
                                             <xsl:with-param name="rhs" select="."/>
                                             <xsl:with-param name="operator" select="$operatorName"/>
                                          </xsl:call-template>
                                       </xsl:variable>
                                       <xsl:for-each select="exsl:node-set($rhs_terms_op)/*">
                                          <mms:term>
                                             <xsl:call-template name="processTerm">
                                                <xsl:with-param name="term" select="."/>
                                             </xsl:call-template>
                                          </mms:term>
                                       </xsl:for-each>
                                    </xsl:for-each>
                                 </mms:operator>
                              </xsl:for-each>
                           </mms:algebraic>
                           </mms:definition>
                        </mms:auxiliaryVariableEquation>
                    </xsl:for-each>
                </mms:auxiliaryVariableEquations>
            </xsl:if>
        </mms:PDEModel>
    </xsl:template>

    <xsl:template name="removeType">
        <xsl:param name="node"></xsl:param>
        <xsl:for-each select="$node">
           <xsl:choose>
              <xsl:when test="current()[ local-name() = 'ci']">
                   <mt:ci><xsl:value-of select="."></xsl:value-of></mt:ci>
               </xsl:when>
               <xsl:when test="count(./*) = 0">
                   <xsl:copy-of select="."></xsl:copy-of>
               </xsl:when>
               <xsl:otherwise>
                   <xsl:copy>
                       <xsl:for-each select="./*">
                           <xsl:call-template name="removeType">
                               <xsl:with-param name="node" select="."></xsl:with-param>
                           </xsl:call-template>
                       </xsl:for-each>
                   </xsl:copy>
               </xsl:otherwise>
           </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <!-- Recursive copy -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- Removes the partialdiff -->
    <xsl:template match="mt:apply[mt:partialdiff]">
        <xsl:apply-templates select="./*[3]"/>
    </xsl:template>

    <!-- get fluxes from RHS -->
    <xsl:template name="getFluxes">
        <xsl:param name="rhs"/>
        <!-- Only one term without derivatives -->
        <xsl:if test="count($rhs[descendant-or-self::mt:ci[@type='flux']]) > 0">
            <mms:fluxes>
               <xsl:for-each select="$rhs/descendant-or-self::mt:ci[@type='flux']">
            <xsl:variable name="flux" select="./text()"/>
                <mms:flux>
                    <mms:coordinate><xsl:value-of select="substring(., string-length(.))"/></mms:coordinate>
                    <mt:math>
                        <xsl:call-template name="removeType"><xsl:with-param name="node" select="//mt:apply[mt:eq and *[position() = 2][text() = $flux]]/*[position() = 3]"></xsl:with-param></xsl:call-template>
                    </mt:math>
                </mms:flux>
        </xsl:for-each>
        </mms:fluxes>
        </xsl:if>
    </xsl:template>

    <!-- get fluxes from RHS -->
    <xsl:template name="getSources">
        <xsl:param name="rhs"/>
        <!-- Only one term without derivatives -->
       <xsl:if test="$rhs/descendant-or-self::mt:ci[@type='source']">
          <xsl:variable name="source" select="$rhs/descendant-or-self::mt:ci[@type='source']/text()"/>
            <mms:source>
                <mt:math>
                    <xsl:call-template name="removeType"><xsl:with-param name="node" select="//mt:apply[mt:eq and *[position() = 2][text() = $source and @type='source']]/*[position() = 3]"></xsl:with-param></xsl:call-template>
                </mt:math>
            </mms:source>
            </xsl:if>
    </xsl:template>

    <!-- separate terms in a RHS -->
    <xsl:template name="separateTerms">
        <xsl:param name="rhs"/>
       <xsl:param name="operator"/>
        <xsl:choose>
            <!-- Only one term without derivatives -->
            <xsl:when test="$rhs[not(descendant::mt:partialdiff or descendant-or-self::mt:ci[@type='flux'] or descendant-or-self::mt:ci[@type='source'])]">
                <xsl:copy-of select="$rhs"/>
            </xsl:when>
           <!-- Only one term with derivatives -->
           <xsl:when test="$rhs[mt:partialdiff or count(*) = 2 or mt:times]">
              <xsl:copy-of select="$rhs"/>
           </xsl:when>
           <!-- only one operator -->
           <xsl:when test="$rhs[@operator and @operator = $operator and (mt:partialdiff or count(*) = 2 or mt:times)]">
              <xsl:copy-of select="$rhs"/>
           </xsl:when>
           <!-- only one operator but not mine -->
           <xsl:when test="$rhs[@operator and not(@operator = $operator)]">
           </xsl:when>
            <xsl:otherwise>
                 <xsl:for-each select="$rhs/*[position() > 1 and not(@type='flux') and not(@type='source') and (not(@operator) or (@operator = $operator))]">
                      <xsl:copy-of select="."/>
                  </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- process terms-->
    <xsl:template name="processTerm">
        <xsl:param name="term"/>
        <!-- Extract math expression if exists -->
        <xsl:if test="$term[not(mt:partialdiff)]">
            <mt:math>
                <xsl:call-template name="copyMathExpression">
                    <xsl:with-param name="term" select="$term"/>
                </xsl:call-template>
            </mt:math>
        </xsl:if>
        <!-- Process derivative if exists -->
        <xsl:choose>
            <!-- Multiplication of derivatives case -->
            <xsl:when
                test="$term/descendant-or-self::mt:apply[mt:times and mt:apply/mt:partialdiff]">
                <mms:partialDerivatives>
                    <!-- Process every derivative -->
                    <xsl:for-each
                        select="($term/descendant-or-self::mt:apply[mt:times and mt:apply/mt:partialdiff])[1]/mt:apply/mt:partialdiff">
                        <mms:partialDerivative>
                            <!-- get derivative coordinate -->
                            <mms:coordinate>
                                <xsl:value-of select="./parent::mt:apply/mt:bvar/mt:ci"/>
                            </mms:coordinate>
                            <!-- process term -->
                            <xsl:call-template name="processTerm">
                                <xsl:with-param name="term"
                                    select="./parent::mt:apply/*[position() = 3]"/>
                            </xsl:call-template>
                        </mms:partialDerivative>
                    </xsl:for-each>
                </mms:partialDerivatives>
            </xsl:when>
            <!-- Single derivative case -->
            <xsl:when test="$term[descendant-or-self::mt:partialdiff]">
                <mms:partialDerivatives>
                    <mms:partialDerivative>
                        <!-- get derivative coordinate -->
                        <mms:coordinate>
                            <xsl:value-of
                                select="($term/descendant-or-self::mt:apply[mt:partialdiff])[1]/mt:bvar/mt:ci"
                            />
                        </mms:coordinate>
                        <!-- process term -->
                        <xsl:call-template name="processTerm">
                            <xsl:with-param name="term"
                                select="($term/descendant-or-self::mt:apply[mt:partialdiff])[1]/*[position() = 3]"
                            />
                        </xsl:call-template>
                    </mms:partialDerivative>
                </mms:partialDerivatives>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- copy all expression elements -->
    <xsl:template name="copyMathExpression">
        <xsl:param name="term"/>
        <xsl:for-each select="$term">
            <xsl:choose>
                <!-- Partial diff multiplication -->
                <xsl:when test="current()[mt:times and mt:apply/mt:partialdiff]">
                    <xsl:choose>
                        <!-- More than one term multiplying, so keep times -->
                        <xsl:when test="count(./*) > 3">
                            <xsl:copy>
                                <xsl:for-each select="./*">
                                    <!-- Copy the elements ignoring the partialdiff apply -->
                                   <xsl:if test="current()[not(mt:partialdiff)]">
                                        <xsl:call-template name="removeType"><xsl:with-param name="node" select="."></xsl:with-param></xsl:call-template>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:copy>
                        </xsl:when>
                        <!-- Copy only the expression multiplying the partiadiff -->
                        <xsl:otherwise>
                            <xsl:call-template name="removeType"><xsl:with-param name="node" select="./*[position() > 1 and not(mt:partialdiff)]"></xsl:with-param></xsl:call-template>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
               <!-- Partial diff division -->
               <xsl:when test="current()[mt:divide and mt:apply/mt:partialdiff]">
                     <!-- Copy only the expression dividing the partiadiff -->
                     <mt:apply>
                        <mt:divide/>
                        <mt:cn>1</mt:cn>
                        <xsl:call-template name="removeType"><xsl:with-param name="node" select="./*[position() > 1 and not(mt:partialdiff)]"></xsl:with-param></xsl:call-template>
                     </mt:apply>
               </xsl:when>
                <!-- minus partial diff -->
               <xsl:when test="current()[mt:minus and mt:apply/mt:partialdiff]">
                    <mt:apply>
                        <mt:minus/>
                        <mt:cn>1</mt:cn>
                    </mt:apply>
                </xsl:when>
                <!-- Non partial diff multiplication -->
                <xsl:otherwise>
                    <xsl:copy>
                        <!-- Copy text if terminal node -->
                        <xsl:if test="count(./*) = 0">
                            <xsl:value-of select="."/>
                        </xsl:if>
                        <xsl:call-template name="copyMathExpression">
                            <xsl:with-param name="term" select="./*"/>
                        </xsl:call-template>
                    </xsl:copy>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
