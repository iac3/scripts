<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common"  extension-element-prefixes="exsl"
    xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms" xmlns:sml="urn:simml"
    version="1.0">
    <xsl:template match="mt:math">
        <mms:chunk>
            <mms:analysisFields>
                <xsl:for-each
                    select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'field']">
                    <mms:analysisField>
                        <xsl:apply-templates select="./text()"/>
                    </mms:analysisField>
                </xsl:for-each>
            </mms:analysisFields>
            <xsl:if
                test="count(//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']) > 0">
                <mms:auxiliaryAnalysisVariables>
                    <xsl:for-each
                        select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']">
                        <mms:auxiliaryAnalysisVariable>
                            <xsl:apply-templates select="./text()"/>
                        </mms:auxiliaryAnalysisVariable>
                    </xsl:for-each>
                </mms:auxiliaryAnalysisVariables>
            </xsl:if>
           <xsl:if test="count(//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]) > 0">
                <mms:parameters>
                   <xsl:for-each select="//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]">
                        <xsl:if
                           test="not(preceding::mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))][text() = current()/text()])">
                            <mms:parameter>
                                <mms:name>
                                    <xsl:apply-templates select="./text()"/>
                                </mms:name>
                                <mms:type>REAL</mms:type>
                            </mms:parameter>
                        </xsl:if>
                    </xsl:for-each>
                </mms:parameters>
            </xsl:if>
            <mms:analysisFieldEquations>
                <xsl:for-each
                    select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and *[position()= 2 and local-name() = 'ci' and @type = 'field']]">
                    <mms:analysisFieldEquation>
                        <xsl:variable name="rhs" select="./*[position() = 3]"/>
                        <xsl:variable name="rhs_terms">
                            <xsl:call-template name="separateTerms">
                                <xsl:with-param name="rhs" select="$rhs"/>
                               <xsl:with-param name="operator" select="empty"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <mms:name/>
                        <mms:analysisField>
                            <xsl:value-of select="./*[position() = 2]"/>
                        </mms:analysisField>
                        <mms:operator>
                            <mms:name>Op</mms:name>
                           <xsl:for-each select="exsl:node-set($rhs_terms)/*">
                                <mms:term>
                                    <xsl:call-template name="processTerm">
                                        <xsl:with-param name="term" select="."/>
                                    </xsl:call-template>
                                </mms:term>
                            </xsl:for-each>
                        </mms:operator>
                       <xsl:for-each select="$rhs/descendant-or-self::mt:apply[not(preceding::mt:apply/@operator/text() = text())]/@operator">
                          <mms:operator>
                             <mms:name><xsl:value-of select="."/></mms:name>
                             <xsl:variable name="operatorName" select="."></xsl:variable>
                             <xsl:for-each select="$rhs/descendant-or-self::mt:apply[@operator = $operatorName]">
                                <xsl:variable name="rhs_terms_op">
                                   <xsl:call-template name="separateTerms">
                                      <xsl:with-param name="rhs" select="."/>
                                      <xsl:with-param name="operator" select="$operatorName"/>
                                   </xsl:call-template>
                                </xsl:variable>
                                <xsl:for-each select="exsl:node-set($rhs_terms_op)/*">
                                   <mms:term>
                                      <xsl:call-template name="processTerm">
                                         <xsl:with-param name="term" select="."/>
                                      </xsl:call-template>
                                   </mms:term>
                                </xsl:for-each>
                             </xsl:for-each>
                          </mms:operator>
                       </xsl:for-each>
                    </mms:analysisFieldEquation>
                </xsl:for-each>
            </mms:analysisFieldEquations>
            <xsl:if
                test="//mt:apply[*[position()= 1 and local-name() = 'eq'] and *[position()= 2 and local-name() = 'ci' and @type = 'variable']]">
                <mms:auxiliaryAnalysisVariableEquations>
                    <xsl:for-each
                        select="//mt:apply[*[position()= 1 and local-name() = 'eq'] and *[position()= 2 and local-name() = 'ci' and @type = 'variable']]">
                        <mms:auxiliaryAnalysisVariableEquation>
                            <mms:name/>
                            <mms:auxiliaryAnalysisVariable>
                                <xsl:value-of select="./*[position() = 2]"/>
                            </mms:auxiliaryAnalysisVariable>
                            <xsl:variable name="rhs" select="./*[position() = 3]"/>
                            <xsl:variable name="rhs_terms">
                                <xsl:call-template name="separateTerms">
                                    <xsl:with-param name="rhs" select="$rhs"/>
                                   <xsl:with-param name="operator" select="empty"/>
                                </xsl:call-template>
                            </xsl:variable>
                            <mms:operator>
                                <mms:name>Op</mms:name>
                                <xsl:for-each select="exsl:node-set($rhs_terms)/*">
                                    <mms:term>
                                        <xsl:call-template name="processTerm">
                                            <xsl:with-param name="term" select="."/>
                                        </xsl:call-template>
                                    </mms:term>
                                </xsl:for-each>
                            </mms:operator>
                           <xsl:for-each select="$rhs/descendant-or-self::mt:apply[not(preceding::mt:apply/@operator/text() = text())]/@operator">
                              <mms:operator>
                                 <mms:name><xsl:value-of select="."/></mms:name>
                                 <xsl:variable name="operatorName" select="."></xsl:variable>
                                 <xsl:for-each select="$rhs/descendant-or-self::mt:apply[@operator = $operatorName]">
                                    <xsl:variable name="rhs_terms_op">
                                       <xsl:call-template name="separateTerms">
                                          <xsl:with-param name="rhs" select="."/>
                                          <xsl:with-param name="operator" select="$operatorName"/>
                                       </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:for-each select="exsl:node-set($rhs_terms_op)/*">
                                       <mms:term>
                                          <xsl:call-template name="processTerm">
                                             <xsl:with-param name="term" select="."/>
                                          </xsl:call-template>
                                       </mms:term>
                                    </xsl:for-each>
                                 </xsl:for-each>
                              </mms:operator>
                           </xsl:for-each>
                        </mms:auxiliaryAnalysisVariableEquation>
                    </xsl:for-each>
                </mms:auxiliaryAnalysisVariableEquations>
            </xsl:if>
           </mms:chunk>
    </xsl:template>

    <xsl:template name="removeType">
        <xsl:param name="node"></xsl:param>
        <xsl:choose>
            <xsl:when test="$node[@type]">
                <mt:ci><xsl:value-of select="$node"></xsl:value-of></mt:ci>
            </xsl:when>
            <xsl:when test="count($node/*) = 0">
                <xsl:copy-of select="$node"></xsl:copy-of>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:for-each select="$node/*">
                        <xsl:call-template name="removeType">
                            <xsl:with-param name="node" select="."></xsl:with-param>
                        </xsl:call-template>
                    </xsl:for-each>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Recursive copy -->
    <xsl:template match="node()">
        <xsl:copy>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- Removes the partialdiff -->
    <xsl:template match="mt:apply[mt:partialdiff]">
        <xsl:apply-templates select="./*[3]"/>
    </xsl:template>

    <!-- separate terms in a RHS -->
   <xsl:template name="separateTerms">
      <xsl:param name="rhs"/>
      <xsl:param name="operator"/>
      <xsl:choose>
         <!-- Only one term without derivatives -->
         <xsl:when test="$rhs[not(descendant::mt:partialdiff)]">
            <xsl:copy-of select="$rhs"/>
         </xsl:when>
         <!-- Only one term with derivatives -->
         <xsl:when test="$rhs[mt:partialdiff or count(*) = 2 or mt:times]">
            <xsl:copy-of select="$rhs"/>
         </xsl:when>
         <!-- only one operator -->
         <xsl:when test="$rhs[@operator and @operator = $operator and (mt:partialdiff or count(*) = 2 or mt:times)]">
            <xsl:copy-of select="$rhs"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:for-each select="$rhs/*[position() > 1 and (not(@operator) or (@operator = $operator))]">
               <xsl:call-template name="removeType"><xsl:with-param name="node" select="."></xsl:with-param></xsl:call-template>
            </xsl:for-each>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

    <!-- process terms-->
    <xsl:template name="processTerm">
        <xsl:param name="term"/>
        <!-- Extract math expression if exists -->
        <xsl:if test="$term[not(mt:partialdiff)]">
            <mt:math>
                <xsl:call-template name="copyMathExpression">
                    <xsl:with-param name="term" select="$term"/>
                </xsl:call-template>
            </mt:math>
        </xsl:if>
        <!-- Process derivative if exists -->
        <xsl:choose>
            <!-- Multiplication of derivatives case -->
            <xsl:when
                test="$term/descendant-or-self::mt:apply[mt:times and mt:apply/mt:partialdiff]">
                <mms:partialDerivatives>
                    <!-- Process every derivative -->
                    <xsl:for-each
                        select="($term/descendant-or-self::mt:apply[mt:times and mt:apply/mt:partialdiff])[1]/mt:apply/mt:partialdiff">
                        <mms:partialDerivative>
                            <!-- get derivative coordinate -->
                            <mms:coordinate>
                                <xsl:value-of select="./parent::mt:apply/mt:bvar/mt:ci"/>
                            </mms:coordinate>
                            <!-- process term -->
                            <xsl:call-template name="processTerm">
                                <xsl:with-param name="term"
                                    select="./parent::mt:apply/*[position() = 3]"/>
                            </xsl:call-template>
                        </mms:partialDerivative>
                    </xsl:for-each>
                </mms:partialDerivatives>
            </xsl:when>
            <!-- Single derivative case -->
            <xsl:when test="$term[descendant::mt:partialdiff]">
                <mms:partialDerivatives>
                    <mms:partialDerivative>
                        <!-- get derivative coordinate -->
                        <mms:coordinate>
                            <xsl:value-of
                                select="($term/descendant-or-self::mt:apply[mt:partialdiff])[1]/mt:bvar/mt:ci"
                            />
                        </mms:coordinate>
                        <!-- process term -->
                        <xsl:call-template name="processTerm">
                            <xsl:with-param name="term"
                                select="($term/descendant-or-self::mt:apply[mt:partialdiff])[1]/*[position() = 3]"
                            />
                        </xsl:call-template>
                    </mms:partialDerivative>
                </mms:partialDerivatives>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- copy all expression elements -->
    <xsl:template name="copyMathExpression">
        <xsl:param name="term"/>
        <xsl:for-each select="$term">
            <xsl:choose>
                <!-- Partial diff multiplication -->
                <xsl:when test="current()[mt:times and mt:apply/mt:partialdiff]">
                    <xsl:choose>
                        <!-- More than one term multiplying, so keep times -->
                        <xsl:when test="count(./*) > 3">
                            <xsl:copy>
                                <xsl:for-each select="./*">
                                    <!-- Copy the elements ignoring the partialdiff apply -->
                                   <xsl:if test="current()[not(mt:partialdiff)]">
                                        <xsl:copy-of select="."/>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:copy>
                        </xsl:when>
                        <!-- Copy only the expression multiplying the partiadiff -->
                        <xsl:otherwise>
                            <xsl:copy-of select="./*[position() > 1 and not(mt:partialdiff)]"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
               <!-- Partial diff division -->
               <xsl:when test="current()[mt:divide and mt:apply/mt:partialdiff]">
                  <!-- Copy only the expression dividing the partiadiff -->
                  <mt:apply>
                     <mt:divide/>
                     <mt:cn>1</mt:cn>
                     <xsl:call-template name="removeType"><xsl:with-param name="node" select="./*[position() > 1 and not(mt:partialdiff)]"></xsl:with-param></xsl:call-template>
                  </mt:apply>
               </xsl:when>
                <!-- minus partial diff -->
               <xsl:when test="current()[mt:minus and mt:apply/mt:partialdiff]">
                    <mt:apply>
                        <mt:minus/>
                        <mt:cn>1</mt:cn>
                    </mt:apply>
                </xsl:when>
                <!-- Non partial diff multiplication -->
                <xsl:otherwise>
                    <xsl:copy>
                        <!-- Copy text if terminal node -->
                        <xsl:if test="count(./*) = 0">
                            <xsl:value-of select="."/>
                        </xsl:if>
                        <xsl:call-template name="copyMathExpression">
                            <xsl:with-param name="term" select="./*"/>
                        </xsl:call-template>
                    </xsl:copy>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
