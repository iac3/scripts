<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common"  extension-element-prefixes="exsl"
    xmlns:mt="http://www.w3.org/1998/Math/MathML" xmlns:mms="urn:mathms" xmlns:sml="urn:simml"
    version="1.0">
    <xsl:template match="mt:math">
        <mms:PDEProblem>
            <mms:head>
                <mms:name/>
                <mms:id/>
                <mms:author/>
                <mms:version/>
               <mms:date>2020-01-01T00:00:00</mms:date>
            </mms:head>
            <mms:coordinates>
                <mms:spatialCoordinates>
                    <xsl:for-each select="meta/coord">
                        <mms:spatialCoordinate>
                            <xsl:value-of select="./text()"/>
                        </mms:spatialCoordinate>
                    </xsl:for-each>
                </mms:spatialCoordinates>
                <mms:timeCoordinate>t</mms:timeCoordinate>
            </mms:coordinates>
            <mms:fields>
                <xsl:for-each
                    select="//mt:apply[mt:eq]/*[position() = 2 and descendant-or-self::mt:partialdiff]/descendant-or-self::mt:ci[@type = 'field']">
                    <mms:field>
                        <xsl:apply-templates select="./text()"/>
                    </mms:field>
                </xsl:for-each>
            </mms:fields>
            <xsl:if
                test="count(//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'field']) > 0">
                <mms:auxiliaryFields>
                    <xsl:for-each
                        select="//mt:apply[mt:eq]/*[position() = 2 and not(descendant-or-self::mt:partialdiff)]/descendant-or-self::mt:ci[@type = 'field']">
                        <mms:auxiliaryField>
                            <xsl:apply-templates select="./text()"/>
                        </mms:auxiliaryField>
                    </xsl:for-each>
                </mms:auxiliaryFields>
            </xsl:if>
            <xsl:if
                test="count(//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']) > 0">
                <mms:auxiliaryVariables>
                    <xsl:for-each
                        select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'variable']">
                        <mms:auxiliaryVariable>
                            <xsl:apply-templates select="./text()"/>
                        </mms:auxiliaryVariable>
                    </xsl:for-each>
                </mms:auxiliaryVariables>
            </xsl:if>
           <xsl:if test="count(//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]) > 0">
                <mms:parameters>
                   <xsl:for-each select="//mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))]">
                        <xsl:if
                           test="not(preceding::mt:ci[not(@type = 'field' or @type = 'variable' or @type = 'flux' or @type = 'source' or parent::mt:bvar or text() = //meta/coord or starts-with(text(), 'Δ'))][text() = current()/text()])">
                            <mms:parameter>
                                <mms:name>
                                    <xsl:apply-templates select="./text()"/>
                                </mms:name>
                                <mms:type>REAL</mms:type>
                            </mms:parameter>
                        </xsl:if>
                    </xsl:for-each>
                </mms:parameters>
            </xsl:if>
            <mms:models>
                <mms:model>
                    <mms:name>ModelName</mms:name>
                    <mms:id>ea5015b1-67d9-3246-aa14-068b02433302</mms:id>
                </mms:model>
            </mms:models>
            <mms:region>
                <mms:name>Main</mms:name>
                <mms:interiorModels>
                    <mms:interiorModel>ModelName</mms:interiorModel>
                </mms:interiorModels>
                <mms:spatialDomain>
                    <xsl:for-each select="meta/coord">
                        <mms:coordinateLimits>
                            <mms:coordinate><xsl:apply-templates select="./text()"/></mms:coordinate>
                            <mms:coordinateMin>
                                <mt:math>
                                    <mt:apply>
                                        <mt:minus/>
                                        <mt:cn>1</mt:cn>
                                    </mt:apply>
                                </mt:math>
                            </mms:coordinateMin>
                            <mms:coordinateMax>
                                <mt:math>
                                    <mt:cn>1</mt:cn>
                                </mt:math>
                            </mms:coordinateMax>
                        </mms:coordinateLimits>
                    </xsl:for-each>
                </mms:spatialDomain>
                <mms:initialConditions>
                        <mms:initialCondition>
                            <mms:mathExpressions>
                                <xsl:for-each
                                    select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'field']">
                                    <mt:math>
                                        <mt:apply>
                                            <mt:eq/>
                                            <mt:ci><xsl:apply-templates select="./text()"/></mt:ci>
                                            <mt:ci>SetInitialCondition</mt:ci>
                                        </mt:apply>
                                    </mt:math>
                                </xsl:for-each>
                            </mms:mathExpressions>
                        </mms:initialCondition>
                </mms:initialConditions>
            </mms:region>
            <mms:boundaryConditions>
                <mms:boundaryPolicy>
                    <mms:boundaryRegions>
                        <mms:regionName>Main</mms:regionName>
                    </mms:boundaryRegions>
                    <mms:boundaryCondition>
                        <mms:type>
                            <mms:periodical/>
                        </mms:type>
                        <mms:axis>All</mms:axis>
                        <mms:side>All</mms:side>
                        <mms:fields>
                            <xsl:for-each
                                select="//mt:apply[mt:eq]/*[position() = 2]/descendant-or-self::mt:ci[@type = 'field']">
                                <mms:field>
                                    <xsl:apply-templates select="./text()"/>
                                </mms:field>
                            </xsl:for-each>
                        </mms:fields>
                    </mms:boundaryCondition>
                </mms:boundaryPolicy>
            </mms:boundaryConditions>
            <mms:boundaryPrecedence>
                <xsl:for-each select="meta/coord">
                    <mms:boundary><xsl:value-of select="./text()"/>-Lower</mms:boundary>
                    <mms:boundary><xsl:value-of select="./text()"/>-Upper</mms:boundary>
                </xsl:for-each>
            </mms:boundaryPrecedence>
            <mms:finalizationConditions>
                <mms:finalizationCondition>
                    <mms:mathExpressions>
                        <mt:math>
                            <mt:ci>SetMathExpression</mt:ci>
                        </mt:math>
                    </mms:mathExpressions>
                </mms:finalizationCondition>
            </mms:finalizationConditions>
        </mms:PDEProblem>
    </xsl:template>
</xsl:stylesheet>
