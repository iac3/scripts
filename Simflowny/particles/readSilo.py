import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate
import Silo
from os import listdir, makedirs
from os.path import isdir, join, exists, isfile


def listOfFiles(directory):
    return [ f for f in listdir(directory) if isfile(join(directory,f)) and f.startswith("processor_cluster.") ]


def readCoordinates (folder, speciesName):
    '''
    Reads a field from the file.

    Input:
        folder      data folder name
        field       field to read
    Returns:
                    the field of each particle
    '''
    data = []
    first = True
    for file in sorted(listOfFiles(folder)):
        db = Silo.Open(folder + "/" + file)
        db.SetDir("level_00000/patch_" + file[file.find('.') + 1:file.rfind('.')])
        ndims = db.GetVarInfo("PointMesh_"+speciesName)['ndims']
        for dim in range(ndims):
            if first:
               data.append(np.array(db.GetVar("PointMesh_" + speciesName + "_coord" + str(dim))))
            else:
                data[dim] = np.append(data[dim], np.array(db.GetVar("PointMesh_" + speciesName + "_coord" + str(dim))))
        first = False
        db.Close()

    for dim in range(ndims):
        f = h5py.File(folder + '/coord' + str(dim) + '.hdf5', "w")
        dset = f.create_dataset('coord' + str(dim), data[dim].shape, dtype='f')
        dset[:] = data[dim][:]
        f.close()

def readField (folder, field):
    '''
    Reads a field from the file.

    Input:
        folder      data folder name
        field       field to read
    Returns:
                    the field of each particle
    '''
    first = True
    for file in sorted(listOfFiles(folder)):
        db = Silo.Open(folder + "/" + file)
        db.SetDir("level_00000/patch_" + file[file.find('.') + 1:file.rfind('.')])
        if first:
                   data = np.array(db.GetVar(field + "__data"))
        else:
                   data = np.append(data, np.array(db.GetVar(field + "__data")))
        db.Close()
        first = False

    f = h5py.File(folder + '/' + field + '.hdf5', "w")
    dset = f.create_dataset('' + field, data.shape, dtype='f')
    dset[:] = data[:]
    f.close()

def main():

    for i in range(0, 522, 1):
        print("ciclo ",i)
        readField("outputDir_particles/visit_dump_" + str(i).zfill(5), "particle_procId_Prova")
        readField("outputDir_particles/visit_dump_" + str(i).zfill(5), "particle_id_Prova")
        readCoordinates("outputDir_particles/visit_dump_" + str(i).zfill(5), "Prova")

if __name__ == "__main__":
    main()
