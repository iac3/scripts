import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py
import math
import scipy.ndimage
from mpl_toolkits import mplot3d

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset


def trajectory(folder, particle_procId, particle_id):
    idp = readData(folder + '/particle_id_Prova.hdf5', 'particle_id_Prova')
    pidp = readData(folder + '/particle_procId_Prova.hdf5', 'particle_procId_Prova')
    x = readData(folder + '/coord0.hdf5', 'coord0')
    y = readData(folder + '/coord1.hdf5', 'coord1')
    z = readData(folder + '/coord2.hdf5', 'coord2')

    # Get particle id indices (multiple processors have same ids)
    p_index = np.where(idp == particle_id)[0]
    #Get index of particle_id from the given processor id
    pidp = pidp[p_index]
    pid_index = np.where(pidp == particle_procId)[0]
    p_pid = p_index[pid_index]

    return x[p_pid], y[p_pid], z[p_pid]

def main():

    proc_id = 1

    dt = 0.2

    ciclos = 2000

    particle = 45
    period = 4

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    
    radius = []
    particles = range(particle, particle+4, 1)

    for p in particles:

        x = np.ndarray((ciclos//period+1))
        y = np.ndarray((ciclos//period+1))
        z = np.ndarray((ciclos//period+1))
        for i in range(0, ciclos+1, period):
            x[i//period], y[i//period], z[i//period] = trajectory("./outputDir_particles/visit_dump_" + str(i).zfill(5), proc_id, p)

            print("radio particula ",p, np.sqrt(x[i]**2+y[i]**2+z[i]**2), i*dt)

        radius.append(np.sqrt(x**2+y**2+z**2))
        ax.plot3D(x, y, z, label=p)


    ax.set_xlim(-30, 30)
    ax.set_ylim(-30, 30)
    ax.set_zlim(-30, 30)
    ax.legend()

    plt.savefig('./trajectory.png')


    plt.figure()
    for i in range(len(radius)):
        plt.plot(radius[i], label=particles[i])
    plt.legend(loc=3)
    plt.savefig('./radius.png')


if __name__ == "__main__":
    main()
