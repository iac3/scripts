import numpy as np
import codecs
import h5py

np.set_printoptions(precision=16)

def save_field(field_index, output_file_name, coordx, field_data):
	values = np.zeros((coordx.shape[0]))
	for (i), v in np.ndenumerate(values):
		values[i] = field_data[np.where(((np.isclose(field_data[:, 0], xcoord[i]))))[0]][:,field_index]

	hdf = h5py.File(output_file_name, mode='w')
	dset = hdf.require_dataset('data', values.shape, dtype='d')
	dset[:] = values[:]

	dsetC1 = hdf.require_dataset('coord0', coordx.shape, dtype='d')
	dsetC1[:] = coordx[:]

	dset = hdf.require_dataset('nvars', (1,), dtype='d')
	dset[:] = 1

	hdf.close()


input_file_name = "phiRF4.0.0.000.dat"

with codecs.open(input_file_name, encoding='utf-8-sig') as f:
	X = [[float(x) for x in line.split()] for line in f]

data = np.array(X, dtype='d')

xcoord = np.unique(data[:, 0])

# Each field must be in a different file
# The following data block can be repeated for different fields
############DATA BLOCK############
field_index = 1
output_file_name = 'phir1D.h5'
save_field(field_index, output_file_name, xcoord, data)
##################################




