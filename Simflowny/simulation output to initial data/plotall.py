import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

from scipy.integrate import simps


def ReadAllfields(folder):
	#Read mesh information
	f_sum = h5py.File(folder + '/summary.samrai', "r")
	nProcessors  = f_sum['/BASIC_INFO/number_processors']
	nPatches     = f_sum['/BASIC_INFO/number_patches_at_level']
	patchExtents = f_sum['/extents/patch_extents']
	patchMap     = f_sum['/extents/patch_map']
	varNames = f_sum['/BASIC_INFO/var_names']

	maximum = [0, 0]
	minimum = [999999, 999999]
	coord_max = [0, 0]
	coord_min = [999999, 999999]
	for i in range(len(patchExtents)):
		maximum[0] = max(maximum[0], patchExtents[i][1][0])
		maximum[1] = max(maximum[1], patchExtents[i][1][1])
		minimum[0] = min(minimum[0], patchExtents[i][0][0])
		minimum[1] = min(minimum[1], patchExtents[i][0][1])

		coord_max[0] = max(coord_max[0], patchExtents[i][3][0])
		coord_max[1] = max(coord_max[1], patchExtents[i][3][1])
		coord_min[0] = min(coord_min[0], patchExtents[i][2][0])
		coord_min[1] = min(coord_min[1], patchExtents[i][2][1])

	size = [(maximum[0] - minimum[0]) + 1 ,(maximum[1] - minimum[1]) + 1]

	coords = {}
	coords[0] = np.linspace(coord_min[0],coord_max[0], size[0], endpoint=False)
	coords[1] = np.linspace(coord_min[1],coord_max[1], size[1], endpoint=False)

	data = {}
	for iVar in range(len(varNames)):
		data[varNames[iVar]] = np.zeros(shape=(size[0], size[1]))

	for iPatch in range(nPatches[0]):
		iProc     = patchMap[iPatch][0]
		iProcStr = str(iProc).zfill(5)
		iPatchStr = str(iPatch).zfill(5)
		rangeX = (patchExtents[iPatch][0][0], patchExtents[iPatch][1][0]+1)
		rangeY = (patchExtents[iPatch][0][1], patchExtents[iPatch][1][1]+1)
		sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 1;
		sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 1;
		f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
		for iVar in range(len(varNames)):
			tmp = f_data['/processor.' + iProcStr + '/level.00000/patch.' + iPatchStr + '/' + varNames[iVar]]
			tmp = np.reshape(tmp, (sizeX,sizeY), order="F")
			data[varNames[iVar]][rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]
		f_data.close()

	return data, coords


def auxfields(my, coords):
	segment = my["Segment"]
	ss = my["shearStress"]
 	rho = my["rho"]
 	rrho = my["rrho"]
	mz = my["mz"]

	dr = coords[1][1] - coords[1][0]
	nz = coords[0].shape[0]
	nr = coords[1].shape[0]

	# find the wall indices 
	wall_segment = 5
	wallindex = segment[:,0]*0.
	for i in range(nz):
		wallindex[i] = (segment[i,:] == wall_segment).argmax() 

	# mass flow
	mdot_theory= rho[:,0]*0.
	mdot= rho[:,0]*0.
	mdots= rho[:,0]*0.
	f,ax = plt.subplots(1,1,sharex=True,sharey="row",figsize=(10,8))
	for i in range(nz):
		mdot_theory[i] = np.mean(rho[i,0:wallindex[i]])*0.0920665*np.pi/2*(0.005988)**2
		mdot[i] = 2.*np.pi*np.dot( mz[i,0:wallindex[i]], coords[1][0:wallindex[i]])*dr 
		mdots[i] = 2.*np.pi*simps( mz[i,0:wallindex[i]]*coords[1][0:wallindex[i]] )*dr
	ax.plot(coords[0],mdot,"r")
	ax.plot(coords[0],mdots,"g",linewidth=2)
	ax.plot(coords[0],mdot_theory,"b")
	ax.set_ylabel("mdot")
	plt.show()



def plotfields(myfields, coords,title=""):
	numf = len(myfields)
	f, ax = plt.subplots(numf,2, sharex="col",figsize=(15,8))
	f.canvas.set_window_title(title)
	plt.subplots_adjust(left=0.05, bottom=0.05, right=0.99, top=0.99, wspace=0.2, hspace=0.05)
	rowplot = 0
	for field in sorted(myfields.iterkeys()):
		data = myfields[field]
		coordinates = coords[field]
		# we must transpose because it is ordered in the fortran way
		im = ax[rowplot,0].imshow(np.transpose(data), 
		                          extent=(coordinates[0].min(),coordinates[0].max(),coordinates[1].min(),coordinates[1].max()),
		                          aspect="auto",origin="lower")
		im.set_cmap('spectral')
		ax[rowplot,0].set_ylabel(field)
		ax[rowplot,0].locator_params(tight=True, nbins=5)
		for i in range(data.shape[1])[::5]:
			ax[rowplot,1].plot(coordinates[0],data[:,i])
		ax[rowplot,1].set_ylabel(field)
		ax[rowplot,1].locator_params(tight=True, nbins=5)
		rowplot += 1
	plt.show()


def main():

	#mynewtonian, coordsnewtonian = ReadAllfields("outputDir_Newtonian_res04x/visit_dump.750000")
	#mycy, coordscy = ReadAllfields("outputDir_NonNewtonian_CY_res04x/visit_dump.750000")
	mycasson4, coordscasson4 = ReadAllfields("outputDir_NonNewtonian_Casson_res04x/visit_dump.12000")
	#mycasson2, coordscasson2 = ReadAllfields("outputDir_NonNewtonian_Casson_res02x/visit_dump.375000")

	# plotfields(mynewtonian,"Newtonian 4x")
	# auxfields(mynewtonian)

	# plotfields(mycy,"CY 4x")
	# auxfields(mynewtonian)

	coords = {}
	for field in mycasson4:
		coords[field] = coordscasson4

	plotfields(mycasson4, coords,"Casson 4x")
	auxfields(mycasson4, coordscasson4)

	comparing = {}
	coords = {}
	mycy["mu"][0,0]=0.25
	comparing["mu_cy"] = mycy["mu"]
	coords["mu_cy"] = coordscy
	# cap the crazy maximum of casson
	mycasson4["mu"][mycasson4["mu"]>0.25] = 0.25
	comparing["mu_casson_4x"] = mycasson4["mu"]
	coords["mu_casson_4x"] = coordscasson4
	mycasson2["mu"][mycasson2["mu"]>0.25] = 0.25
	comparing["mu_casson_2x"]=mycasson2["mu"]
	coords["mu_casson_2x"] = coordscasson2
	plotfields(comparing, coords,"Comparing mu CY and Casson")

	comparing = {}
	coords = {}
	comparing["rrhoDL_cy"]=mycy["rrhoDL"]
	coords["rrhoDL_cy"] = coordscy
	comparing["rrhoDL_casson_4x"]=mycasson4["rrhoDL"]	
	coords["rrhoDL_casson_4x"] = coordscasson4
	comparing["rrhoDL_casson_2x"]=mycasson2["rrhoDL"]	
	coords["rrhoDL_casson_2x"] = coordscasson2
	plotfields(comparing, coords,"Comparing rrhoDL CY and Casson")

if __name__ == "__main__":
	main()
