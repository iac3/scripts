import sys, getopt
import os
import numpy as np
import h5py
import scipy.ndimage as ndimage
import matplotlib.pyplot as plt

def readfield(field):

	print field
	#Readig the variable to disk
	f= h5py.File(field + '.hdf5', "r")
	ds = f[field]
	data = np.empty(shape=ds.shape, dtype=ds.dtype)
	data[:] = ds[:]
	f.close()
	return data

def smooth(data, stencil=2):
	n, m= data.shape[0], data.shape[1]
	sdata = data[:,:]
	for j in range(m):
		i = stencil
		while (i<n-stencil):
			if (np.sum(np.abs(data[i-stencil:i+stencil+1,j]))<1e-10):
				pass
			else:
				sdata[i,j] = np.sum(data[i-stencil:i+stencil+1,j])/(2.*stencil+1)
			i = i+1

	plt.imshow(sdata, aspect="auto",origin="lower")
	plt.show()

	return sdata


def main():

	plt.figure(figsize=(14, 9))

	fieldlist = ["rho","P","mr","mz","rmr","rmz","rrho","rrhoDL","shearStress"]
	numf = len(fieldlist)

	numplot = 0
	for field in fieldlist:
		numplot += 1
		if (numplot==1) :
			ax1 = plt.subplot(numf,2,numplot)
		else:
			ax1 = plt.subplot(numf,2,numplot,sharex=ax1,sharey=ax1)
		ax1.locator_params(tight=True, nbins=4)

		data = readfield(field)
		data = smooth(data)


		#data = ndimage.uniform_filter(data, 10)
		plt.imshow(np.transpose(data), aspect="auto",origin="lower")
		plt.ylabel(field)
		numplot += 1
		ax3 = plt.subplot(numf,2,numplot)
		for i in range(40)[::2]:
			plt.plot(data[:,i])
		#plt.plot(data[:,20])
		ax3.locator_params(tight=True, nbins=4)

#	plt.tight_layout()
	plt.show()

if __name__ == "__main__":
	main()
