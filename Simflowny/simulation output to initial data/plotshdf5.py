import sys, getopt
import os
import numpy as np
import h5py
import scipy.ndimage as ndimage
import matplotlib.pyplot as plt
from scipy import interpolate

def readfield(field):
	print "Reading ",field
	#Reading the variable from disk
	f= h5py.File(field + '.hdf5', "r")
	ds = f[field]
	data = np.empty(shape=ds.shape, dtype=ds.dtype)
	data[:] = ds[:]
	f.close()
	return data

def readfields(fieldlist):
	dic = {}
	for field in fieldlist:
		dic[field] = readfield(field)
	return dic

def doubleres(data):
	x = np.linspace(0,1,data.shape[0])
	y = np.linspace(0,1,data.shape[1])
	f = interpolate.RectBivariateSpline(x, y, data,kx=1,ky=1) # linear spline
	x2 = np.linspace(0,1,data.shape[0]*2)
	y2 = np.linspace(0,1,data.shape[1]*2)
	return f(x2,y2)

def doubleresolution(myfields):
	my2dic = {}
	for field in myfields:
		data = myfields[field]
		my2dic[field] = doubleres(data)
	return my2dic


def plotfields(myfields):
	numf = len(myfields)
	f, ax = plt.subplots(numf,2,sharex='col', figsize=(20,10))
	f.canvas.set_window_title("Noozle res1x")
	plt.subplots_adjust(left=0.05, bottom=0.05, right=0.99, top=0.99, wspace=0.2, hspace=0.05)
	rowplot = 0
	for field in myfields:
		data = myfields[field][700:,:]
		ax[rowplot,0].imshow(np.transpose(data), aspect="auto",origin="lower")
		ax[rowplot,0].set_ylabel(field)
		ax[rowplot,0].locator_params(tight=True, nbins=5)
		for i in range(data.shape[1])[::10]:
			ax[rowplot,1].plot(data[:,i])
			ax[rowplot,1].set_ylabel(field)
		ax[rowplot,1].locator_params(tight=True, nbins=5)
		rowplot += 1
	plt.show()

def main():

	listfields = ["rho","P","mr","mz","rmr","rmz","rrho","rrhoDL","shearStress"]
	my = readfields(listfields)
	plotfields(my)
	#my2 = doubleresolution(my)
	#plotfields(my2)


if __name__ == "__main__":
	main()
