import sys, getopt
import os
import numpy as np
import h5py
from scipy import interpolate

def smooth(data, stencil=3):
	n, m= data.shape[0], data.shape[1]
	sdata = data[:,:]
	for j in range(m):
		i = stencil
		while (i<n-stencil):
			if (np.sum(np.abs(data[i-stencil:i+stencil+1,j]))<1e-10):
				pass
			else:
				sdata[i,j] = np.sum(data[i-stencil:i+stencil+1,j])/(2.*stencil+1)
			i = i+1
	return sdata

def mapToCorrectDimensions(dataset, dimensions, cells):
	if dimensions == 1:
		tmp = np.zeros(len(dataset),dtype='float')
		for i in range(0, len(dataset)):
			tmp[i] = dataset[i]
	elif dimensions == 2:
		tmp = np.zeros(shape=(cells[0], cells[1]),dtype='float')
		for i in range(0, len(dataset)):
			nj = i/cells[0]
			ni = i - nj*cells[0]
			tmp[ni, nj] = dataset[i]
	elif dimensions == 3:
		tmp = np.zeros(shape=(cells[0], cells[1], cells[2]),dtype='float')
		for i in range(0, len(dataset)):
			nk = i/(cells[0]*cells[1])
			nj = (i - nk * cells[0] * cells[1])/cells[0]
			ni = i - nj*cells[0] - nk * cells[0] * cells[1]
			tmp[ni, nj, nk] = dataset[i]
	return tmp


def doubleres(data):
	x = np.linspace(0,1,data.shape[0])
	y = np.linspace(0,1,data.shape[1])
	f = interpolate.RectBivariateSpline(x, y, data,kx=1,ky=1) # linear spline
	x2 = np.linspace(0,1,data.shape[0]*2)
	y2 = np.linspace(0,1,data.shape[1]*2)
	return f(x2,y2)

def writefields(folder):
	#Read mesh information
	f_sum = h5py.File(folder + '/summary.samrai', "r")
	nProcessors  = f_sum['/BASIC_INFO/number_processors']
	nPatches     = f_sum['/BASIC_INFO/number_patches_at_level']
	patchExtents = f_sum['/extents/patch_extents']
	patchMap     = f_sum['/extents/patch_map']
	varNames = f_sum['/BASIC_INFO/var_names']

	maximum = [0, 0]
	minimum = [999999, 999999]
	for i in range(len(patchExtents)):
		maximum[0] = max(maximum[0], patchExtents[i][1][0])
		maximum[1] = max(maximum[1], patchExtents[i][1][1])
		minimum[0] = min(minimum[0], patchExtents[i][0][0])
		minimum[1] = min(minimum[1], patchExtents[i][0][1])
	size = [(maximum[0] - minimum[0]) + 1 ,(maximum[1] - minimum[1]) + 1]

	data = {}
	for iVar in range(len(varNames)):
		data[iVar] = np.zeros(shape=(size[0], size[1]))

	for iPatch in range(nPatches[0]):
    		iProc     = patchMap[iPatch][0]
		iProcStr = str(iProc).zfill(5)
		iPatchStr = str(iPatch).zfill(5)
		rangeX = (patchExtents[iPatch][0][0], patchExtents[iPatch][1][0]+1)
		rangeY = (patchExtents[iPatch][0][1], patchExtents[iPatch][1][1]+1)
    		sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 1;
    		sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 1;
		f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
    		for iVar in range(len(varNames)):
			tmp = f_data['/processor.' + iProcStr + '/level.00000/patch.' + iPatchStr + '/' + varNames[iVar]]
			#tmp = mapToCorrectDimensions(tmp, 2, (sizeX,sizeY))
			tmp = np.reshape(tmp, (sizeX,sizeY), order="F")
        		data[iVar][rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]
		f_data.close()
	for iVar in range(len(varNames)):
		#data[iVar] = doubleres(data[iVar])
		#Writing the variable to disk
		f = h5py.File(varNames[iVar] + '.hdf5', "w")
		dset = f.create_dataset(varNames[iVar], data[iVar].shape, dtype='f')
		dset[:,:] = data[iVar][:,:]
		f.close()
	f_sum.close()



def main():
	writefields("visit_dump.1248000")

if __name__ == "__main__":
	main()
