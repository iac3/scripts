import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset


def main():

    for l in xrange(7):
	    plt.figure()
	    plt.imshow(readData("BSC/outputDir_slice0_gamma175/visit_dump.00000/rhof_" + str(l) + ".hdf5", "rhof"))
	    plt.colorbar()
	    plt.savefig('rhof_' + str(l) + '.png')


if __name__ == "__main__":
    main()
