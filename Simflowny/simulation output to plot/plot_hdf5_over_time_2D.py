import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt

prefix = ""

def read(folder, varName, x, y):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    nPatches     = f_sum['/BASIC_INFO/number_patches_at_level']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']

    
    maximum = [0, 0]
    minimum = [999999, 999999]
    npatch = -1
    for i in range(len(patchExtents)):
        low_x = patchExtents[i][2][0]
        low_y = patchExtents[i][2][1]
        up_x = patchExtents[i][3][0]
        up_y = patchExtents[i][3][1]
        if low_x <= x and x < up_x and low_y <= y and y < up_y:
            npatch = i

    size = [(patchExtents[npatch][1][0] - patchExtents[npatch][0][0]) + 1 ,(patchExtents[npatch][1][1] - patchExtents[npatch][0][1]) + 1 ]

    data = np.zeros(shape=(size[0], size[1]))

    iProc     = patchMap[npatch][0]
    iPatch     = patchMap[npatch][3]
    iLevel     = patchMap[npatch][2]
    iProcStr = str(iProc).zfill(5)
    iPatchStr = str(iPatch).zfill(5)
    iLevelStr = str(iLevel).zfill(5)
    f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
    tmp = f_data['/processor.' + iProcStr + '/level.' + iLevelStr + '/patch.' + iPatchStr + '/' + varName]

    #tmp = mapToCorrectDimensions(tmp, 2, (sizeX,sizeY))
    tmp = np.reshape(tmp, (size), order="F")
    data[:,:] = tmp[:,:]
    f_data.close()

    low_x = patchExtents[npatch][2][0]
    low_y = patchExtents[npatch][2][1]
    up_x = patchExtents[npatch][3][0]
    up_y = patchExtents[npatch][3][1]

    lowC_x = patchExtents[npatch][0][0]
    lowC_y = patchExtents[npatch][0][1]
    upC_x = patchExtents[npatch][1][0]
    upC_y = patchExtents[npatch][1][1]

    dx =  (up_x - low_x)/(upC_x - lowC_x)
    dy =  (up_y - low_y)/(upC_y - lowC_y)

    i = int((x - low_x) / dx)
    j = int((y - low_y) / dy)
    print i, j, data[i,j]

    return data[i,j]
    

def main():
    data = []
    for t in xrange(0,2001, 20):
        data.append(read("./outputDir_slice0/visit_dump." + ( "%05d" % t), "phiR", 0, 0))

    plt.plot(data)
    plt.show()

if __name__ == "__main__":
    main()
