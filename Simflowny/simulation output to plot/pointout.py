#!/shared/intel/intelpython2/bin/python
#!/usr/bin/python
import sys, getopt
import os
import numpy as np
import h5py
#from scipy import interpolate

def getpoint(folder, pointx, pointy, debug, field):
   if (debug): print "getpoint: field = ",field,"at point (",pointx,",",pointy,")"
   #Read mesh information
   f_sum        = h5py.File(folder + '/summary.samrai', "r")
   nProcessors  = f_sum['/BASIC_INFO/number_processors']
   time         = f_sum['/BASIC_INFO/time'][0]
   nPatches     = f_sum['/BASIC_INFO/number_patches_at_level']
   patchExtents = f_sum['/extents/patch_extents']
   patchMap     = f_sum['/extents/patch_map']
   #print '/processor.' + iProcStr + '/level.' + iLevelStr + '/patch.' + iPatchStr + '/' + field
   if (debug): print "getpoint: Finding appropriate patch containing point"
   npatch   = -1
   npatch_i = -1
   for i in range(len(patchExtents)):
       low_x = patchExtents[i][2][0]
       low_y = patchExtents[i][2][1]
       up_x  = patchExtents[i][3][0]
       up_y  = patchExtents[i][3][1]
       iLevel= patchMap[i][2]
       if (debug): print "               i = ",i," at level = ",iLevel,low_x,up_x,low_y,up_y
       if low_x <= pointx and pointx < up_x and low_y <= pointy and pointy < up_y and iLevel > npatch_i:
           npatch   = i
           npatch_i = iLevel
           if (debug): print "               This is currently the best patch"
   if (npatch<0): 
      print "No appropriate patch found."
      return  0.0, 0.0
   if (debug): print "Appropriate patch is npatch=",npatch," at level=",npatch_i
   if (debug): print "Extents: ", patchExtents[npatch][1][0],patchExtents[npatch][0][0],patchExtents[npatch][1][1],patchExtents[npatch][0][1]
   size       = [(patchExtents[npatch][1][0] - patchExtents[npatch][0][0]) + 2 ,(patchExtents[npatch][1][1] - patchExtents[npatch][0][1]) + 2 ]
   data       = np.zeros(shape=(size[0],size[1]))
   iProc      = patchMap[npatch][0]
   iLevel     = patchMap[npatch][2]
   iPatch     = patchMap[npatch][3]
   iProcStr   = str(iProc).zfill(5)
   iPatchStr  = str(iPatch).zfill(5)
   iLevelStr  = str(iLevel).zfill(5)
   f_data     = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
   tmp        = f_data['/processor.' + iProcStr + '/level.' + iLevelStr + '/patch.' + iPatchStr + '/' + field]
   tmp        = np.reshape(tmp, (size), order="F")
   data[:,:]  = tmp[:,:]
   f_data.close()
   #
   low_x  = patchExtents[npatch][2][0]
   low_y  = patchExtents[npatch][2][1]
   up_x   = patchExtents[npatch][3][0]
   up_y   = patchExtents[npatch][3][1]
   lowC_x = patchExtents[npatch][0][0]
   lowC_y = patchExtents[npatch][0][1]
   upC_x  = patchExtents[npatch][1][0]
   upC_y  = patchExtents[npatch][1][1]
   if (debug):
      print "getpoint:  low_x/y: ",low_x, low_y
      print "getpoint:   up_x/y: ",up_x,  up_y
      print "getpoint:   upC_x/y:",upC_x, upC_y
      print "getpoint:  lowC_x/y:",lowC_x,lowC_y
   #
   dxvec = f_sum['/BASIC_INFO/dx']
   dx    = dxvec[npatch_i][0]
   dy    = dxvec[npatch_i][1]
   #dz    = dxvec[npatch_i][2]
   #dx =  (up_x - low_x)/(upC_x - lowC_x)
   #dy =  (up_y - low_y)/(upC_y - lowC_y)
   i  = int((pointx - low_x) / dx)
   j  = int((pointy - low_y) / dy)
   if (debug):
      print "getpoint: time:  ",time
      print "getpoint: point: ",data[i,j]
      print "getpoint: dx/dy: ",dx,dy
      print "getpoint: i/j:   ",i,j
   return time, data[i,j]


def main():
        
    print sys.argv[0], len(sys.argv)
    basename = './outputDir_slice0'
    if (len(sys.argv)<=4):
       print "Usage:  ",sys.argv[0]," <directory> <field> <x-val> <y-val> [ <debug=1>]"
       print ""
       print "Output:   <time> <fieldvalue>"
       sys.exit()
    basename = sys.argv[1]
    field    = sys.argv[2]
    posx     = sys.argv[3]
    posy     = sys.argv[4]
    debug    = 0
    if (len(sys.argv)>5): debug = 1
    if (debug):
       print " directory: ",basename
       print " field:     ",field
       print " (x,y):      (",posx,",",posy,")"
    dir_list = os.walk(basename).next()[1]
    dir_list = sorted(dir_list, key=lambda x: (int(x.split(".", 2)[1]), x))
    for t in dir_list:
        path = basename+"/"+str(t)
        if (debug): print "Processing",path, " t = ",t
        mytime,fieldvalue = getpoint(path, float(posx), float(posy), debug, field)
        print mytime, fieldvalue

if __name__ == "__main__":
    main()
