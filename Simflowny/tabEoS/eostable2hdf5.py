import numpy as np
import codecs
import h5py

hdf = h5py.File('table.h5', mode='w')


eostable = h5py.File('eostable.h5', mode='r')

fields = ['Abar', 'dpderho', 'dedt', 'dpdrhoe']
coords = ['ye', 'logtemp', 'logrho']

nvars = 0
for coord in coords:
	data = np.asarray(eostable[coord])
	dset = hdf.require_dataset('coord' + str(nvars), data.shape, dtype='f')
	dset[:] = data[:]
	nvars = nvars + 1

nvars = 0
for field in fields:
	data = np.asarray(eostable[field])
	dset = hdf.require_dataset('var' + str(nvars), data.shape, dtype='f')
	dset[:] = data[:]
	nvars = nvars + 1

dset = hdf.require_dataset('nvars', (1,), dtype='f')
dset[:] = nvars
hdf.close()
eostable.close()
