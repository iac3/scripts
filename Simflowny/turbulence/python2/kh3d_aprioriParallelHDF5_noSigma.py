# THIS ROUTINE LOADS THE FILTERED FILES GENERATED WITH kh3d_FilteringParallel.py
# It compares the SFS quantities with different SGS models

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,AutoMinorLocator)
import math
import os
import sys
from scipy.optimize import fmin
from scipy import stats
from common import psd3Drad, readData3D, pdf, int3D, avrg3D, vorticity3D, strain3D, div3D, div3D_comp, grad3DAxis, avrg1D, saveData3D
import scipy.sparse

def main():
    '''
    runs = []
    maxcycle = []
    labels = []
    dt = []
    

    folder="/home/bminano/datos/bitbucket/bssn/tests/MHD/KH3D/python/"
    runs.append("o400")
    maxcycle.append(27000)
    labels.append("400^3")
    dt.append(8.e-4)
    Npoints = 401

    '''
    runs = []
    labels = []
    maxcycle = []
    dt =[]

    folder="/gpfs/projects/pr1ehy00/data/KH3D/non_helical/"
    runs.append("outputDir_2000")
    maxcycle.append(50000)
    labels.append("2000^3")
    dt.append(2.e-4)
    Npoints = 2001
    folderout = "/gpfs/projects/pr1ehy00/data/2000_SF2_borja_hdf5/"

# =========================================================================

# =========================================================================
# PARAMETERS OF THE PHYSICAL SIMULATION
# Size of the simulation box.
    L = 1.
# Ideal gas coefficient
    gamma = 4./3.

# =========================================================================
# Size of the filter (number of cells to be averaged out in each direction)
# Any integer > 1, which is a divisor of (Npoint-1)
    Size_filter = 2
# Number of cycles, excluding t=0 and considering the available outputs: i*(maxcycle / Ncycle), i=1,Ncycle
    Ncycle = 1
# Cycles in which histogram 2D is calculated
    Hist2dCycles = [50000]

# PARAMETERS FOR HISTOGRAM GRAPHICS (ONLY FOR PLOT_HISTO)
# number of bins in the plots
    Nbins = Npoints/2
# Width of the distributions in the histograms
    sigma_distr = 1

# =========================================================================
# === end of inputs =======================================================

    print "output folder for images: ", folderout
    print "Size filter: ", Size_filter
    dt = np.asarray(dt)
    maxcycle = np.asarray(maxcycle)
    nr = len(runs)

    if not os.path.exists(folderout):
        os.makedirs(folderout)

    analyze3D_filter(folder,folderout,runs,labels,maxcycle,dt,L,gamma,Npoints,Ncycle,Nbins,sigma_distr,Size_filter, Hist2dCycles)


def analyze3D_filter(folder,folderout,runs,labels,maxcycle,dt,L,gamma,Npoints,Ncycle,Nbins,sigma_distr,sf, Hist2dCycles):

    dx = L/Npoints

    nr = len(runs)
    dcycle = maxcycle/(Ncycle)
    time = np.zeros((Ncycle,nr))

    bestfit_coef = np.zeros((5,4,6,Ncycle))
    bestfit_pears = np.zeros((5,4,6,Ncycle))

    sigma_mom_bins = np.zeros((Nbins,Ncycle,nr))
    sigma_mom_pdf = np.zeros((Nbins,Ncycle,nr))
    sigma_ind_bins = np.zeros((Nbins,Ncycle,nr))
    sigma_ind_pdf = np.zeros((Nbins,Ncycle,nr))

    headerevo = "t, sigma_mom, sigma_ind\n"
    prename=folderout+"lingrad3D_"+str((Npoints-1))+"_filt"+str((Npoints-1)/sf)

    for r in xrange(0, nr, 1):

        run = folder+runs[r]
        print "======================================================"
        print "======================================================"
        print "RUN :",run
        folder_txt = folderout+"/PARALLEL_FILTERING_"+str((Npoints-1)/sf)
        if not os.path.exists(folder_txt):
            os.makedirs(folder_txt)
        fevo=open(folder_txt+"/transfer_evolution.out","w+")
        fevo.write(headerevo)
        for cc in xrange(0, Ncycle, 1):
            time[cc,r] = dcycle[r]*(cc+1)*dt[r]
            timename = "t"+str("%i" % (10*time[cc,r])).zfill(3)
            print "======================================================"
            print "TIME: ",time[cc,r]

            foldertime="/visit_dump."+str(dcycle[r]*(cc+1)).zfill(5)+"/"
            print run+foldertime

            calcHistogram = dcycle[r]*(cc+1) in Hist2dCycles
            print calcHistogram
            bestfit_coef[:,:,:,cc],bestfit_pears[:,:,:,cc] = fit3DCalculation(folderout+foldertime,folderout+"histo2d_SF"+str(sf)+"_"+timename+"_",fevo,time[cc,r],sf,L,gamma,Nbins,sigma_distr, (Npoints - 1)/sf, (Npoints - 1)/sf, (Npoints - 1)/sf, calcHistogram)

            savename = folderout+foldertime+"filtered_SF_parallel"+str(sf)

            saveData3D(savename + "/bestfit_coef.hdf5", "bestfit_coef", bestfit_coef[:,:,:,cc])
            saveData3D(savename + "/bestfit_pears.hdf5", "bestfit_pears", bestfit_pears[:,:,:,cc])
            print "End of fit3D"

            header = "sigma_mom (bin,pdf), sigma_ind (bin,pdf) \n "
            outname = folder_txt+"/pdf_"+timename+".out"
            f=open(outname,"w+")
            f.write(header)
            for kk in xrange(0,Nbins,1):
              f.write("%12.3e %12.3e %12.3e %12.3e \n" % (sigma_mom_bins[kk,cc,r],sigma_mom_pdf[kk,cc,r],sigma_ind_bins[kk,cc,r],sigma_ind_pdf[kk,cc,r]))

            f.close()
            print "Written PDFs at: ",outname

        fevo.close()
        print "Written evolution quantities at: ",folder_txt+"/transfer_evolution.out"

# Defining the bestfit coefficient and Pearson coefficients in two entire matrices, to contain all SGS models and quantities
# 1st dimension: SGS model. 0: grad. 1: dissipative generalized. 2: cross. 3: dynamo (induction only). 4: vorticity (induction only)
# 2nd dimension: quantity. Only the ones marked with asterisks are present for all models.
#                0: tau_kin (or tau_mom) comp.0:6. 1: tau_mag comp.0:6. 2: tau_ind* comp.3:6., tau_enth 0:3. 3: sigmas: sigma_pres (:,:,3,0), sigma_mom*. (:,:,3,1), sigma_ind* (:,:,3,2).
# 3rd dimension: components. Indices from 0:3 are the diagonal terms, 3:6 the off-diagonal. Scalars (sigma) only in 0.
# 4th dimension: time.

# PLOTTING THE EVOLUTION IN TIME

        if (Ncycle > 1):

          labels = ["grad","diss","cross"]
          bfc = np.zeros((3,Ncycle))
          bfp = np.zeros((3,Ncycle))
          for cc in xrange(0,Ncycle):
             for im in xrange(0,3):
           	   bfc[im,cc] = np.mean(bestfit_coef[im,0,:,cc])
           	   bfp[im,cc] = np.mean(bestfit_pears[im,0,:,cc])
          plot_bestfit_pearson(prename+"_tau_kin",time[:,r],bfc,bfp,labels)

          for cc in xrange(0,Ncycle):
             for im in xrange(0,3):
           	   bfc[im,cc] = np.mean(bestfit_coef[im,0,0:3,cc])
           	   bfp[im,cc] = np.mean(bestfit_pears[im,0,0:3,cc])
          plot_bestfit_pearson(prename+"_tau_kin_tra",time[:,r],bfc,bfp,labels)

          for cc in xrange(0,Ncycle):
             for im in xrange(0,3):
           	   bfc[im,cc] = np.mean(bestfit_coef[im,0,3:6,cc])
           	   bfp[im,cc] = np.mean(bestfit_pears[im,0,3:6,cc])
          plot_bestfit_pearson(prename+"_tau_kin_off",time[:,r],bfc,bfp,labels)

          labels = ["grad","diss"]
          bfc = np.zeros((2,Ncycle))
          bfp = np.zeros((2,Ncycle))
          for cc in xrange(0,Ncycle):
             for im in xrange(0,2):
           	   bfc[im,cc] = np.mean(bestfit_coef[im,1,:,cc])
           	   bfp[im,cc] = np.mean(bestfit_pears[im,1,:,cc])
          plot_bestfit_pearson(prename+"_tau_mag",time[:,r],bfc,bfp,labels)

          labels = ["grad","diss","cross","dyn","vort"]
          bfc = np.zeros((5,Ncycle))
          bfp = np.zeros((5,Ncycle))
          for cc in xrange(0,Ncycle):
             for im in xrange(0,5):
           	   bfc[im,cc] = np.mean(bestfit_coef[im,2,3:6,cc])
           	   bfp[im,cc] = np.mean(bestfit_pears[im,2,3:6,cc])
          plot_bestfit_pearson(prename+"_tau_ind",time[:,r],bfc,bfp,labels)

          '''
          labels = ["grad"]
          bfc = np.zeros((1,Ncycle))
          bfp = np.zeros((1,Ncycle))
          for cc in xrange(0,Ncycle):
           	   bfc[0,cc] = bestfit_coef[0,3,0,cc]
           	   bfp[0,cc] = bestfit_pears[0,3,0,cc]
          plot_bestfit_pearson(prename+"_sigma_mom",time[:,r],bfc,bfp,labels)
          '''
          labels = ["grad","diss","cross"]
          bfc = np.zeros((3,Ncycle))
          bfp = np.zeros((3,Ncycle))
          for cc in xrange(0,Ncycle):
             for im in xrange(0,3):
           	   bfc[im,cc] = bestfit_coef[im,3,1,cc]
           	   bfp[im,cc] = bestfit_pears[im,3,1,cc]
          plot_bestfit_pearson(prename+"_sigma_mom",time[:,r],bfc,bfp,labels)

          labels = ["grad","diss","cross","dyn","vort"]
          bfc = np.zeros((5,Ncycle))
          bfp = np.zeros((5,Ncycle))
          for cc in xrange(0,Ncycle):
             for im in xrange(0,5):
           	   bfc[im,cc] = bestfit_coef[im,3,2,cc]
           	   bfp[im,cc] = bestfit_pears[im,3,2,cc]               
          plot_bestfit_pearson(prename+"_sigma_ind",time[:,r],bfc,bfp,labels)



    return


# THIS ROUTINE LOADS OR CALLS THE SFS TENSORS AND PERFORMS THE PEARSON ANALYSIS
def fit3DCalculation(folder,histo_root,fevo,time,sf,L,gamma,Nbins,sigma_distr,Nxf,Nyf,Nzf, calcHistogram):
    tau_grad(folder,histo_root,fevo,time,sf,L,gamma,Nbins)
    dxf = L / Nxf

    return fit3D(folder,histo_root,fevo,time,sf,L,gamma, dxf,Nbins, sigma_distr,Nxf, Nyf, Nzf, calcHistogram)

def tau_grad(folder,histo_root,fevo,time,sf,L,gamma,Nbins):

    savename = folder+"filtered_SF_parallel"+str(sf)

    tau_grad_enthF(savename,L,gamma)
    #sigma_pres_sgs = sigma_grad_presF(rhof,pf,rhovxf,rhovyf,rhovzf,L,gamma)
    tau_grad_indF(savename,L)
    tau_grad_magF(savename,L)
    tau_grad_kinF(savename,L)  

    #saveData3D(savename + "/sigma_pres_sgs.hdf5", "sigma_pres_sgs", sigma_pres_sgs)
    
#@profile
def gradientModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, calcHistogram):

    #sigma_pres_sgs = readData3D(savename+"/sigma_pres_sgs.hdf5", "sigma_pres_sgs")
    #sigma_pres = readData3D(savename+"/sigma_pres.hdf5", "sigma_pres")

    bestfit_coef = np.zeros((4,6))
    bestfit_pears = np.zeros((4,6))

    print "============================================"
    print " GRADIENT MODELS FITS "
    print "============================================"

    bestfit_pears[3,1], bestfit_coef[3,1] = gradientModelFits_sigma_mom(savename,histo_root,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[0,:], bestfit_coef[0,:] = gradientModelFits_tau_kin(savename,histo_root,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[1,:], bestfit_coef[1,:] = gradientModelFits_tau_mag(savename,histo_root,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[2,3:6], bestfit_coef[2,3:6] = gradientModelFits_tau_ind(savename,histo_root,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[2,0:3], bestfit_coef[2,0:3] = gradientModelFits_tau_enth(savename,histo_root,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[3,2], bestfit_coef[3,2] = gradientModelFits_sigma_ind(savename,histo_root,Nbins,sigma_distr, calcHistogram)

    '''
    print "sigma_pres"
    file_histo = histo_root+"grad_sigma_pres"
    pearson,pvalue,coeff = pearson_scalar(sigma_pres,sigma_pres_sgs,file_histo,Nbins,sigma_distr)
    bestfit_pears[3,0] = pearson
    bestfit_coef[3,0] = coeff
    sigma_pres_sgs = None
    '''
    return bestfit_coef, bestfit_pears

#@profile
def gradientModelFits_sigma_mom(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    gradientModelFits_sigma_mom_sgs(savename) # Pico maximo
    print "sigma_mom"
    file_histo = histo_root+"grad_sigma_mom"
    sigma_mom = readData3D(savename+"/sigma_mom.hdf5", "sigma_mom")
    sigma_mom_sgs = readData3D(savename+"/sigma_mom_sgs.hdf5", "sigma_mom_sgs")
    pearson,pvalue,coeff = pearson_scalar(sigma_mom,sigma_mom_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

#@profile
def gradientModelFits_sigma_mom_sgs(savename):
    tau_mag_sgs = readData3D(savename+"/tau_mag_sgs.hdf5", "tau_mag_sgs")
    tau_kin_sgs = readData3D(savename+"/tau_kin_sgs.hdf5", "tau_kin_sgs")
    tau_mom_sgs = tau_mom_def(tau_kin_sgs,tau_mag_sgs)
    tau_mag_sgs = None
    tau_kin_sgs = None
    stf =  readData3D(savename+"/stf.hdf5", "stf")
    sigma_mom_sgs =(stf[:,:,:,0]*tau_mom_sgs[:,:,:,0] + stf[:,:,:,1]*tau_mom_sgs[:,:,:,1] + stf[:,:,:,2]*tau_mom_sgs[:,:,:,2] 
         + 2.*stf[:,:,:,3]*tau_mom_sgs[:,:,:,3] + 2.*stf[:,:,:,4]*tau_mom_sgs[:,:,:,4] + 2.*stf[:,:,:,5]*tau_mom_sgs[:,:,:,5])
    saveData3D(savename + "/sigma_mom_sgs.hdf5", "sigma_mom_sgs", sigma_mom_sgs)

#@profile
def gradientModelFits_tau_kin(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    print "tau_kin"
    file_histo = histo_root+"grad_tau_kin"
    tau_kin = readData3D(savename+"/tau_kin.hdf5", "tau_kin")
    tau_kin_sgs = readData3D(savename+"/tau_kin_sgs.hdf5", "tau_kin_sgs")
    pearson,pvalue,coeff = pearson_6(tau_kin,tau_kin_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    return pearson, coeff

#@profile
def gradientModelFits_tau_mag(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    print "tau_mag"
    file_histo = histo_root+"grad_tau_mag"
    tau_mag = readData3D(savename+"/tau_mag.hdf5", "tau_mag")
    tau_mag_sgs = readData3D(savename+"/tau_mag_sgs.hdf5", "tau_mag_sgs")
    pearson,pvalue,coeff = pearson_6(tau_mag,tau_mag_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

#@profile
def gradientModelFits_tau_ind(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    print "tau_ind"
    file_histo = histo_root+"grad_tau_ind"
    tau_ind = readData3D(savename+"/tau_ind.hdf5", "tau_ind")
    tau_ind_sgs = readData3D(savename+"/tau_ind_sgs.hdf5", "tau_ind_sgs")
    pearson,pvalue,coeff = pearson_3(tau_ind,tau_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    tau_ind = None
    jf = readData3D(savename+"/jf.hdf5", "jf")
    sigma_ind_sgs = 2.*tau_ind_sgs[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind_sgs[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind_sgs[:,:,:,2]*jf[:,:,:,2]
    saveData3D(savename + "/sigma_ind_sgs.hdf5", "sigma_ind_sgs", sigma_ind_sgs)

    return pearson, coeff

#@profile
def gradientModelFits_tau_enth(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    print "tau_enth"
    file_histo = histo_root+"grad_tau_enth"
    tau_enth_sgs = readData3D(savename+"/tau_enth_sgs.hdf5", "tau_enth_sgs")
    tau_enth = readData3D(savename+"/tau_enth.hdf5", "tau_enth")
    pearson,pvalue,coeff = pearson_3(tau_enth,tau_enth_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

#@profile
def gradientModelFits_sigma_ind(savename,histo_root, Nbins,sigma_distr, calcHistogram):
    print "sigma_ind"
    file_histo = histo_root+"grad_sigma_ind"
    sigma_ind = readData3D(savename+"/sigma_ind.hdf5", "sigma_ind")
    sigma_ind_sgs = readData3D(savename+"/sigma_ind_sgs.hdf5", "sigma_ind_sgs")
    pearson,pvalue,coeff = pearson_scalar(sigma_ind,sigma_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

def dissipativeModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):

    bestfit_coef = np.zeros((4,6))
    bestfit_pears = np.zeros((4,6))

    print "============================================"
    print "DISSIPATIVE MODELS"
    print "============================================"

    
    bestfit_pears[0,:], bestfit_coef[0,:], tau_kin_sgs  = dissipativeModelFits_tau_kin(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)

    bestfit_pears[1,:], bestfit_coef[1,:], tau_mag_sgs = dissipativeModelFits_tau_mag(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    
    tau_mom_sgs = tau_mom_def(tau_kin_sgs,tau_mag_sgs)
    tau_kin_sgs = None
    tau_mag_sgs = None
    stf =  readData3D(savename+"/stf.hdf5", "stf")
    sigma_mom_sgs = stf[:,:,:,0]*tau_mom_sgs[:,:,:,0] + stf[:,:,:,1]*tau_mom_sgs[:,:,:,1] + stf[:,:,:,2]*tau_mom_sgs[:,:,:,2] + 2.*stf[:,:,:,3]*tau_mom_sgs[:,:,:,3] + 2.*stf[:,:,:,4]*tau_mom_sgs[:,:,:,4] + 2.*stf[:,:,:,5]*tau_mom_sgs[:,:,:,5]
    tau_mom_sgs = None
    stf = None

    bestfit_pears[3,1], bestfit_coef[3,1] = dissipativeModelFits_sigma_mom(savename,histo_root, sigma_mom_sgs, Nbins,sigma_distr, calcHistogram)
    sigma_mom_sgs = None
    
    bestfit_pears[2,3:6], bestfit_coef[2,3:6], sigma_ind_sgs = dissipativeModelFits_tau_ind(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    bestfit_pears[3,2], bestfit_coef[3,2] = dissipativeModelFits_sigma_ind(savename,histo_root, sigma_ind_sgs, Nbins,sigma_distr, calcHistogram)
    
    return bestfit_coef, bestfit_pears



def dissipativeModelFits_tau_kin(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):
    print "tau_kin"
    stf =  readData3D(savename+"/stf.hdf5", "stf")
    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    mod = np.sqrt(stf[:,:,:,0]*stf[:,:,:,0] + stf[:,:,:,1]*stf[:,:,:,1] + stf[:,:,:,2]*stf[:,:,:,2] + 2.*(stf[:,:,:,3]*stf[:,:,:,3] + stf[:,:,:,4]*stf[:,:,:,4] + stf[:,:,:,5]*stf[:,:,:,5]) )
    tau_kin_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    for i in xrange(0,6):
        tau_kin_sgs[:,:,:,i] = (dxf)**2*rhof*mod*stf[:,:,:,i]
    mod = None
    stf = None
    file_histo = histo_root+"diss_tau_kin"
    tau_kin = readData3D(savename+"/tau_kin.hdf5", "tau_kin")
    pearson,pvalue,coeff = pearson_6(tau_kin,tau_kin_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff, tau_kin_sgs

def dissipativeModelFits_tau_mag(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):
    print "tau_mag"
    dissipativeModelFits_tau_mag_sgs(savename, dxf, Nxf,Nyf,Nzf) # Pico maximo
    file_histo = histo_root+"diss_tau_mag"
    tau_mag = readData3D(savename+"/tau_mag.hdf5", "tau_mag")
    tau_mag_sgs = readData3D(savename+"/tau_mag_sgs2.hdf5", "tau_mag_sgs")
    pearson,pvalue,coeff = pearson_6(tau_mag,tau_mag_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff, tau_mag_sgs

def dissipativeModelFits_tau_mag_sgs(savename, dxf, Nxf,Nyf,Nzf):
    mstf =  readData3D(savename+"/mstf.hdf5", "mstf")
    mod = np.sqrt(mstf[:,:,:,0]*mstf[:,:,:,0] + mstf[:,:,:,1]*mstf[:,:,:,1] + mstf[:,:,:,2]*mstf[:,:,:,2] + 2.*(mstf[:,:,:,3]*mstf[:,:,:,3] + mstf[:,:,:,4]*mstf[:,:,:,4] + mstf[:,:,:,5]*mstf[:,:,:,5]) )
    tau_mag_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    for i in xrange(0,6):
        tau_mag_sgs[:,:,:,i] = (dxf)**2*mod*mstf[:,:,:,i]
    mstf = None
    saveData3D(savename + "/tau_mag_sgs2.hdf5", "tau_mag_sgs", tau_mag_sgs)

def dissipativeModelFits_sigma_mom(savename,histo_root, sigma_mom_sgs, Nbins,sigma_distr, calcHistogram):
    print "sigma_mom"
    file_histo = histo_root+"diss_sigma_mom"
    sigma_mom = readData3D(savename+"/sigma_mom.hdf5", "sigma_mom")
    pearson,pvalue,coeff = pearson_scalar(sigma_mom,sigma_mom_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

def dissipativeModelFits_tau_ind(savename,histo_root, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):
    print "tau_ind"
    file_histo = histo_root+"diss_tau_ind"
    tau_ind_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    jf = readData3D(savename+"/jf.hdf5", "jf")
    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    for i in xrange(0,3):
        tau_ind_sgs[:,:,:,i] = (dxf)**2*abs(jf[:,:,:,i])*jf[:,:,:,i]/np.sqrt(rhof)
    rhof = None
    tau_ind = readData3D(savename+"/tau_ind.hdf5", "tau_ind")
    pearson,pvalue,coeff = pearson_3(tau_ind,tau_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = 2.*tau_ind_sgs[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind_sgs[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind_sgs[:,:,:,2]*jf[:,:,:,2]
    return pearson, coeff, sigma_ind_sgs

def dissipativeModelFits_sigma_ind(savename,histo_root, sigma_ind_sgs, Nbins,sigma_distr, calcHistogram):
    print "sigma_ind"
    file_histo = histo_root+"diss_sigma_ind"
    sigma_ind = readData3D(savename+"/sigma_ind.hdf5", "sigma_ind")
    pearson,pvalue,coeff = pearson_scalar(sigma_ind,sigma_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)

    return pearson, coeff

def crossHelicityModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):

    bestfit_coef = np.zeros((4,6))
    bestfit_pears = np.zeros((4,6))

    print "============================================"
    print "CROSS-HELICITY SGS MODEL"
    print "============================================"

    print "tau_mom"
    file_histo = histo_root+"cross_tau_mom"
    tau_mom_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    stf =  readData3D(savename+"/stf.hdf5", "stf")
    mstf =  readData3D(savename+"/mstf.hdf5", "mstf")
    mod = np.sqrt(rhof*abs(stf[:,:,:,0]*mstf[:,:,:,0] + stf[:,:,:,1]*mstf[:,:,:,1] + stf[:,:,:,2]*mstf[:,:,:,2] + 2.*(stf[:,:,:,3]*mstf[:,:,:,3] + stf[:,:,:,4]*mstf[:,:,:,4] + stf[:,:,:,5]*mstf[:,:,:,5]) ))
    mstf = None
    for i in xrange(0,6):
        tau_mom_sgs[:,:,:,i] = (dxf)**2*mod*stf[:,:,:,i]
    mod = None
    tau_mag = readData3D(savename+"/tau_mag.hdf5", "tau_mag")
    tau_kin = readData3D(savename+"/tau_kin.hdf5", "tau_kin")
    tau_mom = tau_mom_def(tau_kin,tau_mag)
    tau_mag = None
    tau_kin = None
    pearson,pvalue,coeff = pearson_6(tau_mom,tau_mom_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[0,:] = pearson
    bestfit_coef[0,:] = coeff
    sigma_mom_sgs = stf[:,:,:,0]*tau_mom_sgs[:,:,:,0] + stf[:,:,:,1]*tau_mom_sgs[:,:,:,1] + stf[:,:,:,2]*tau_mom_sgs[:,:,:,2] + 2.*stf[:,:,:,3]*tau_mom_sgs[:,:,:,3] + 2.*stf[:,:,:,4]*tau_mom_sgs[:,:,:,4] + 2.*stf[:,:,:,5]*tau_mom_sgs[:,:,:,5]
    stf = None
    tau_mom_sgs = None

    print "tau_ind"
    file_histo = histo_root+"cross_tau_ind"
    tau_ind_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    jf = readData3D(savename+"/jf.hdf5", "jf")
    vortf = readData3D(savename+"/vortf.hdf5", "vortf")
    mod = ( vortf[:,:,:,0]*jf[:,:,:,0] + vortf[:,:,:,1]*jf[:,:,:,1] + vortf[:,:,:,2]*jf[:,:,:,2] )/np.sqrt(rhof)
    vortf = None
    rhof = None
    for i in xrange(0,3):
        tau_ind_sgs[:,:,:,i] = (dxf)**2*np.sign(mod)*np.sqrt(abs(mod))*jf[:,:,:,i]
    mod = None
    tau_ind = readData3D(savename+"/tau_ind.hdf5", "tau_ind")
    pearson,pvalue,coeff = pearson_3(tau_ind,tau_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    bestfit_pears[2,3:6] = pearson
    bestfit_coef[2,3:6] = coeff
    tau_ind = None

    print "sigma_mom"
    file_histo = histo_root+"cross_sigma_mom"
    sigma_mom = readData3D(savename+"/sigma_mom.hdf5", "sigma_mom")
    pearson,pvalue,coeff = pearson_scalar(sigma_mom,sigma_mom_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_mom_sgs = None
    sigma_mom = None
    bestfit_pears[3,1] = pearson
    bestfit_coef[3,1] = coeff

    print "sigma_ind"
    file_histo = histo_root+"cross_sigma_ind"
    sigma_ind_sgs = 2.*tau_ind_sgs[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind_sgs[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind_sgs[:,:,:,2]*jf[:,:,:,2]
    jf = None
    sigma_ind = readData3D(savename+"/sigma_ind.hdf5", "sigma_ind")
    pearson,pvalue,coeff = pearson_scalar(sigma_ind,sigma_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = None
    sigma_ind = None
    bestfit_pears[3,2] = pearson
    bestfit_coef[3,2] = coeff

    return bestfit_coef, bestfit_pears

def dynamoModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):

    tau_ind = readData3D(savename+"/tau_ind.hdf5", "tau_ind")
    bmodf = readData3D(savename+"/bmodf.hdf5", "bmodf")
    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    jf = readData3D(savename+"/jf.hdf5", "jf")
    sigma_ind = readData3D(savename+"/sigma_ind.hdf5", "sigma_ind")

    bestfit_coef = np.zeros((4,6))
    bestfit_pears = np.zeros((4,6))

    print "============================================"
    print "DYNAMO SGS FIT"
    print "============================================"
    print "tau_ind"    
    file_histo = histo_root+"dyn_tau_ind"
    tau_ind_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    for i in xrange(0,3):
        tau_ind_sgs[:,:,:,i] = dxf*bmodf*jf[:,:,:,i]/np.sqrt(rhof)
    bmodf = None
    pearson,pvalue,coeff = pearson_3(tau_ind,tau_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = 2.*tau_ind_sgs[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind_sgs[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind_sgs[:,:,:,2]*jf[:,:,:,2]
    bestfit_pears[2,3:6] = pearson
    bestfit_coef[2,3:6] = coeff

    print "sigma_ind"
    file_histo = histo_root+"dyn_sigma_ind"
    pearson,pvalue,coeff = pearson_scalar(sigma_ind,sigma_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = None
    bestfit_pears[3,2] = pearson
    bestfit_coef[3,2] = coeff

    return bestfit_coef, bestfit_pears

def vorticityModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):

    tau_ind = readData3D(savename+"/tau_ind.hdf5", "tau_ind")
    vortf = readData3D(savename+"/vortf.hdf5", "vortf")
    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    jf = readData3D(savename+"/jf.hdf5", "jf")
    sigma_ind = readData3D(savename+"/sigma_ind.hdf5", "sigma_ind")

    bestfit_coef = np.zeros((4,6))
    bestfit_pears = np.zeros((4,6))

    print "============================================"
    print "VORTICITY SGS FIT"
    print "============================================"
    print "tau_ind"
    file_histo = histo_root+"vort_tau_ind"
    tau_ind_sgs = np.zeros((Nxf,Nyf,Nzf,6))
    for i in xrange(0,3):
        tau_ind_sgs[:,:,:,i] = (dxf)**2*np.sqrt(rhof)*abs(vortf[:,:,:,i])*vortf[:,:,:,i]
    vortf = None
    rhof = None
    pearson,pvalue,coeff = pearson_3(tau_ind,tau_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = 2.*tau_ind_sgs[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind_sgs[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind_sgs[:,:,:,2]*jf[:,:,:,2]
    jf = None
    tau_ind_sgs = None
    bestfit_pears[2,3:6] = pearson
    bestfit_coef[2,3:6] = coeff

    print "sigma_ind"
    file_histo = histo_root+"vort_sigma_ind"
    pearson,pvalue,coeff = pearson_scalar(sigma_ind,sigma_ind_sgs,file_histo,Nbins,sigma_distr, calcHistogram)
    sigma_ind_sgs = None
    bestfit_pears[3,2] = pearson
    bestfit_coef[3,2] = coeff

    return bestfit_coef, bestfit_pears

#@profile
def fit3D(folder,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram):

    savename = folder+"filtered_SF_parallel"+str(sf)

# Defining the bestfit coefficient and Pearson coefficients in two entire matrices, to contain all SGS models and quantities
# 1st dimension: SGS model. 0: grad. 1: dissipative generalized. 2: cross. 3: dynamo (induction only). 4: vorticity (induction only)
# 2nd dimension: quantity. Only the ones marked with asterisks are present for all models.
#                0: tau_kin* (or tau_mom) 0:6. 1: tau_mag 0:6. 2: tau_ind* 3:6., tau_enth 0:3. 3: 0: sigma_pres. 1: sigma_mom*. 2: sigma_ind*.
# 3rd dimension: components. Indices from 0:3 are the diagonal terms, 3:6 the off-diagonal. Scalars (sigma) only in 0.

    bestfit_coef = np.zeros((5,4,6))
    bestfit_pears = np.zeros((5,4,6))


# For a given SGS model i, the matrix 4x6 has components relative to:
#  tau_kin(xx) tau_kin(yy) tau_kin(zz) tau_kin(xy) tau_kin(yz) tau_kin(zx)
#  tau_mag(xx) tau_mag(yy) tau_mag(zz) tau_mag(xy) tau_mag(yz) tau_mag(zx)
#  tau_enth(x) tau_enth(y) tau_enth(z) tau_ind(xy) tau_ind(yz) tau_ind(zx)
#  sigma_pres  sigma_mom   sigma_ind   ----        -----       ----- 


# EVALUATION OF PEARSON COEFFICIENTS FOR THE DIFFERENT MODELS
# FOR THE GRADIENT MODEL, WE COMPARE ALL THE QUANITITES.
# FOR THE OTHERS, ONLY SOME OF THEM

    
    bestfit_coef[0, :, :], bestfit_pears[0, :, :] = gradientModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, calcHistogram)
    bestfit_coef[1, :, :], bestfit_pears[1, :, :] = dissipativeModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    bestfit_coef[2, :, :], bestfit_pears[2, :, :] = crossHelicityModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    bestfit_coef[3, :, :], bestfit_pears[3, :, :] = dynamoModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    bestfit_coef[4, :, :], bestfit_pears[4, :, :] = vorticityModelFits(savename,histo_root,fevo,time,sf,L,gamma, dxf, Nbins,sigma_distr, Nxf,Nyf,Nzf, calcHistogram)
    
    print ""
    print ""

    return bestfit_coef,bestfit_pears

#@profile
def tau_mom_def(tau_kin,tau_mag):

    tau_mom = tau_kin - tau_mag
    tau_mom [:,:,:,0] = tau_mom [:,:,:,0] + 0.5*( tau_mag[:,:,:,0] + tau_mag[:,:,:,1] + tau_mag[:,:,:,2] )
    tau_mom [:,:,:,1] = tau_mom [:,:,:,1] + 0.5*( tau_mag[:,:,:,0] + tau_mag[:,:,:,1] + tau_mag[:,:,:,2] )
    tau_mom [:,:,:,2] = tau_mom [:,:,:,2] + 0.5*( tau_mag[:,:,:,0] + tau_mag[:,:,:,1] + tau_mag[:,:,:,2] )

    return tau_mom

def sigma_grad_presF(rhof,pf,vxf,vyf,vzf,L,gamma):

    Nxf = rhof.shape[0]
    Nyf = rhof.shape[1]
    Nzf = rhof.shape[2]
    dx = L/Nxf

# See if there is a smartest way to do the following, or a python routine to define it directly instead of by components:
# The following terms have 5 dimensions: 3 of position, then index j and index k, as in the notes

    dxdp = grad3DAxis(pf,dx,0)
    dydp = grad3DAxis(pf,dx,1)
    dzdp = grad3DAxis(pf,dx,2)        

    dxdlnrho = grad3DAxis(rhof,dx,0)/rhof
    dydlnrho = grad3DAxis(rhof,dx,1)/rhof
    dzdlnrho = grad3DAxis(rhof,dx,2)/rhof

    dxdvx = grad3DAxis(vxf,dx,0)
    dydvx = grad3DAxis(vxf,dx,1)
    dzdvx = grad3DAxis(vxf,dx,2)
    dxdvy = grad3DAxis(vyf,dx,0)
    dydvy = grad3DAxis(vyf,dx,1)
    dzdvy = grad3DAxis(vyf,dx,2)
    dxdvz = grad3DAxis(vzf,dx,0)
    dydvz = grad3DAxis(vzf,dx,1)
    dzdvz = grad3DAxis(vzf,dx,2)

#    grad_comb_xx = grad3DAxis(dxdp,dx,0) - dxdlnrho*dxdp
#    grad_comb_xy = grad3DAxis(dydp,dx,0) - dxdlnrho*dydp
#    grad_comb_xz = grad3DAxis(dzdp,dx,0) - dxdlnrho*dzdp
#    grad_comb_yx = grad3DAxis(dxdp,dx,1) - dydlnrho*dxdp
#    grad_comb_yy = grad3DAxis(dydp,dx,1) - dydlnrho*dydp
#    grad_comb_yz = grad3DAxis(dzdp,dx,1) - dydlnrho*dzdp
#    grad_comb_zx = grad3DAxis(dxdp,dx,2) - dzdlnrho*dxdp
#    grad_comb_zy = grad3DAxis(dydp,dx,2) - dzdlnrho*dydp
#    grad_comb_zz = grad3DAxis(dzdp,dx,2) - dzdlnrho*dzdp

    grad_comb_xx = grad3DAxis(dxdp,dx,0)
    grad_comb_xy = grad3DAxis(dydp,dx,0)
    grad_comb_xz = grad3DAxis(dzdp,dx,0)
    grad_comb_yx = grad3DAxis(dxdp,dx,1)
    grad_comb_yy = grad3DAxis(dydp,dx,1)
    grad_comb_yz = grad3DAxis(dzdp,dx,1)
    grad_comb_zx = grad3DAxis(dxdp,dx,2)
    grad_comb_zy = grad3DAxis(dydp,dx,2)
    grad_comb_zz = grad3DAxis(dzdp,dx,2)


    sigma_grad_pres=np.zeros((Nxf,Nyf,Nzf))

    prefac = (dx)**2/12.0
    sigma_grad_pres = -prefac*( grad_comb_xx*dxdvx + grad_comb_xy*dydvx + grad_comb_xz*dzdvx
                              + grad_comb_yx*dxdvy + grad_comb_yy*dydvy + grad_comb_yz*dzdvy
                              + grad_comb_zx*dxdvz + grad_comb_zy*dydvz + grad_comb_zz*dzdvz )

    return sigma_grad_pres

def tau_grad_enthF(savename,L,gamma):

    rhof = readData3D(savename+"/rhof.hdf5", "rhof")
    
    Nxf = rhof.shape[0]
    Nyf = rhof.shape[1]
    Nzf = rhof.shape[2]
    dx = L/Nxf

    dxdlnrho = grad3DAxis(rhof,dx,0)/rhof
    dydlnrho = grad3DAxis(rhof,dx,1)/rhof
    dzdlnrho = grad3DAxis(rhof,dx,2)/rhof
    rhof = None

    vxf = readData3D(savename+"/rhovxf.hdf5", "rhovxf")
    dxdvx = grad3DAxis(vxf,dx,0)
    dydvx = grad3DAxis(vxf,dx,1)
    dzdvx = grad3DAxis(vxf,dx,2)
    vxf = None

    vyf = readData3D(savename+"/rhovyf.hdf5", "rhovyf")
    dxdvy = grad3DAxis(vyf,dx,0)
    dydvy = grad3DAxis(vyf,dx,1)
    dzdvy = grad3DAxis(vyf,dx,2)
    vyf = None

    vzf = readData3D(savename+"/rhovzf.hdf5", "rhovzf")
    dxdvz = grad3DAxis(vzf,dx,0)
    dydvz = grad3DAxis(vzf,dx,1)
    dzdvz = grad3DAxis(vzf,dx,2)
    vzf = None

    pf = readData3D(savename+"/pf.hdf5", "pf")

    dxdp = grad3DAxis(pf,dx,0)
    dydp = grad3DAxis(pf,dx,1)
    dzdp = grad3DAxis(pf,dx,2)

    tau_grad_enth = np.zeros((Nxf,Nyf,Nzf,3))

    prefac = ((dx)**2/12.)*(gamma/(gamma-1.0))
    tau_grad_enth[:,:,:,0] = - prefac*( (dxdp - pf*dxdlnrho)*dxdvx + (dydp - pf*dydlnrho)*dydvx + (dzdp - pf*dzdlnrho)*dzdvx ) 
    tau_grad_enth[:,:,:,1] = - prefac*( (dxdp - pf*dxdlnrho)*dxdvy + (dydp - pf*dydlnrho)*dydvy + (dzdp - pf*dzdlnrho)*dzdvy ) 
    tau_grad_enth[:,:,:,2] = - prefac*( (dxdp - pf*dxdlnrho)*dxdvz + (dydp - pf*dydlnrho)*dydvz + (dzdp - pf*dzdlnrho)*dzdvz ) 

    saveData3D(savename + "/tau_enth_sgs.hdf5", "tau_enth_sgs", tau_grad_enth)

def tau_grad_indF(savename,L):

    rhof = readData3D(savename+"/rhof.hdf5", "rhof")

    Nxf = rhof.shape[0]
    Nyf = rhof.shape[1]
    Nzf = rhof.shape[2]
    dx = L/Nxf

    dxdlnrho = grad3DAxis(rhof,dx,0)/rhof
    dydlnrho = grad3DAxis(rhof,dx,1)/rhof
    dzdlnrho = grad3DAxis(rhof,dx,2)/rhof
    rhof = None
    
    vxf = readData3D(savename+"/rhovxf.hdf5", "rhovxf")
    dxdvx = grad3DAxis(vxf,dx,0)
    dydvx = grad3DAxis(vxf,dx,1)
    dzdvx = grad3DAxis(vxf,dx,2)
    vxf = None

    vyf = readData3D(savename+"/rhovyf.hdf5", "rhovyf")
    dxdvy = grad3DAxis(vyf,dx,0)
    dydvy = grad3DAxis(vyf,dx,1)
    dzdvy = grad3DAxis(vyf,dx,2)
    vyf = None

    vzf = readData3D(savename+"/rhovzf.hdf5", "rhovzf")
    dxdvz = grad3DAxis(vzf,dx,0)
    dydvz = grad3DAxis(vzf,dx,1)
    dzdvz = grad3DAxis(vzf,dx,2)
    vzf = None

    Bxf = readData3D(savename+"/Bxf.hdf5", "Bxf")
    dxdBx = grad3DAxis(Bxf,dx,0)
    dydBx = grad3DAxis(Bxf,dx,1)
    dzdBx = grad3DAxis(Bxf,dx,2)
    Byf = readData3D(savename+"/Byf.hdf5", "Byf")
    dxdBy = grad3DAxis(Byf,dx,0)
    dydBy = grad3DAxis(Byf,dx,1)
    dzdBy = grad3DAxis(Byf,dx,2)
    Bzf = readData3D(savename+"/Bzf.hdf5", "Bzf")
    dxdBz = grad3DAxis(Bzf,dx,0)
    dydBz = grad3DAxis(Bzf,dx,1)
    dzdBz = grad3DAxis(Bzf,dx,2)

    prefac = (dx)**2/12.0
    tau_grad_ind = np.zeros((Nxf,Nyf,Nzf,3))

    tau_grad_ind[:,:,:,0] = - prefac*( dxdvx*(dxdBy - dxdlnrho*Byf) + dydvx*(dydBy - dydlnrho*Byf) + dzdvx*(dzdBy - dzdlnrho*Byf)
                                     - dxdvy*(dxdBx - dxdlnrho*Bxf) - dydvy*(dydBx - dydlnrho*Bxf) - dzdvy*(dzdBx - dzdlnrho*Bxf) )
    tau_grad_ind[:,:,:,1] = - prefac*( dxdvy*(dxdBz - dxdlnrho*Bzf) + dydvy*(dydBz - dydlnrho*Bzf) + dzdvy*(dzdBz - dzdlnrho*Bzf)
                                     - dxdvz*(dxdBy - dxdlnrho*Byf) - dydvz*(dydBy - dydlnrho*Byf) - dzdvz*(dzdBy - dzdlnrho*Byf) )
    tau_grad_ind[:,:,:,2] = - prefac*( dxdvz*(dxdBx - dxdlnrho*Bxf) + dydvz*(dydBx - dydlnrho*Bxf) + dzdvz*(dzdBx - dzdlnrho*Bxf)
                                     - dxdvx*(dxdBz - dxdlnrho*Bzf) - dydvx*(dydBz - dydlnrho*Bzf) - dzdvx*(dzdBz - dzdlnrho*Bzf) )

    saveData3D(savename + "/tau_ind_sgs.hdf5", "tau_ind_sgs", tau_grad_ind)

def tau_grad_magF(savename,L):

    Bxf = readData3D(savename+"/Bxf.hdf5", "Bxf")
    Byf = readData3D(savename+"/Byf.hdf5", "Byf")
    Bzf = readData3D(savename+"/Bzf.hdf5", "Bzf")

    Nxf = Bxf.shape[0]
    Nyf = Bxf.shape[1]
    Nzf = Bxf.shape[2]
    dx = L/Nxf

    dxdBx = grad3DAxis(Bxf,dx,0)
    dydBx = grad3DAxis(Bxf,dx,1)
    dzdBx = grad3DAxis(Bxf,dx,2)
    dxdBy = grad3DAxis(Byf,dx,0)
    dydBy = grad3DAxis(Byf,dx,1)
    dzdBy = grad3DAxis(Byf,dx,2)
    dxdBz = grad3DAxis(Bzf,dx,0)
    dydBz = grad3DAxis(Bzf,dx,1)
    dzdBz = grad3DAxis(Bzf,dx,2)

    prefac = (dx)**2/12.0
    tau_grad_mag = np.zeros((Nxf,Nyf,Nzf,6))
    tau_grad_mag[:,:,:,0] = - prefac*( dxdBx*dxdBx + dydBx*dydBx + dzdBx*dzdBx ) 
    tau_grad_mag[:,:,:,1] = - prefac*( dxdBy*dxdBy + dydBy*dydBy + dzdBy*dzdBy ) 
    tau_grad_mag[:,:,:,2] = - prefac*( dxdBz*dxdBz + dydBz*dydBz + dzdBz*dzdBz ) 
    tau_grad_mag[:,:,:,3] = - prefac*( dxdBx*dxdBy + dydBx*dydBy + dzdBx*dzdBy ) 
    tau_grad_mag[:,:,:,4] = - prefac*( dxdBy*dxdBz + dydBy*dydBz + dzdBy*dzdBz ) 
    tau_grad_mag[:,:,:,5] = - prefac*( dxdBz*dxdBx + dydBz*dydBx + dzdBz*dzdBx ) 

    saveData3D(savename + "/tau_mag_sgs.hdf5", "tau_mag_sgs", tau_grad_mag)

def tau_grad_kinF(savename,L):

    vxf = readData3D(savename+"/rhovxf.hdf5", "rhovxf")
    vyf = readData3D(savename+"/rhovyf.hdf5", "rhovyf")
    vzf = readData3D(savename+"/rhovzf.hdf5", "rhovzf")

    Nxf = vxf.shape[0]
    Nyf = vxf.shape[1]
    Nzf = vxf.shape[2]
    dx = L/Nxf

    dxdvx = grad3DAxis(vxf,dx,0)
    dydvx = grad3DAxis(vxf,dx,1)
    dzdvx = grad3DAxis(vxf,dx,2)
    dxdvy = grad3DAxis(vyf,dx,0)
    dydvy = grad3DAxis(vyf,dx,1)
    dzdvy = grad3DAxis(vyf,dx,2)
    dxdvz = grad3DAxis(vzf,dx,0)
    dydvz = grad3DAxis(vzf,dx,1)
    dzdvz = grad3DAxis(vzf,dx,2)

    rhof = readData3D(savename+"/rhof.hdf5", "rhof")

    prefac = (dx)**2/12.0
    tau_grad_kin = np.zeros((Nxf,Nyf,Nzf,6))
    tau_grad_kin[:,:,:,0] = - prefac*rhof*( dxdvx*dxdvx + dydvx*dydvx + dzdvx*dzdvx )
    tau_grad_kin[:,:,:,1] = - prefac*rhof*( dxdvy*dxdvy + dydvy*dydvy + dzdvy*dzdvy )
    tau_grad_kin[:,:,:,2] = - prefac*rhof*( dxdvz*dxdvz + dydvz*dydvz + dzdvz*dzdvz )
    tau_grad_kin[:,:,:,3] = - prefac*rhof*( dxdvx*dxdvy + dydvx*dydvy + dzdvx*dzdvy )
    tau_grad_kin[:,:,:,4] = - prefac*rhof*( dxdvy*dxdvz + dydvy*dydvz + dzdvy*dzdvz )
    tau_grad_kin[:,:,:,5] = - prefac*rhof*( dxdvz*dxdvx + dydvz*dydvx + dzdvz*dzdvx )

    saveData3D(savename + "/tau_kin_sgs.hdf5", "tau_kin_sgs", tau_grad_kin)
 
def pearson_scalar(tau_mom,tau_model,file_histo,Nbins,sigma_distr, calcHistogram):

    n = tau_mom.shape[0]**3
    a = np.reshape(tau_mom[:,:,:],n)
    b = np.reshape(tau_model[:,:,:],n)
    pearson, pvalue = stats.pearsonr(a,b)
    coeff = np.sum(tau_mom[:,:,:]*tau_model[:,:,:])/np.sum(tau_model[:,:,:]**2)
    if calcHistogram:
      plot_hist2d(file_histo,a,coeff*b,Nbins,sigma_distr)

    print "===== PEARSON FOR SCALAR FIELD ====="
    print "Coefficient",coeff
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "====================="
    return pearson,pvalue,coeff

#@profile
def pearson_6(tau_mom,tau_model,file_histo,Nbins,sigma_distr, calcHistogram):

    pearson = np.zeros(6)
    pvalue = np.zeros(6)
    coeff = np.sum(tau_mom[:,:,:,:]*tau_model[:,:,:,:], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:]**2, axis=(0, 1, 2))
    n = tau_mom.shape[0]**3
    for i in xrange(0,6):
        a = np.reshape(tau_mom[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        pearson[i], pvalue[i] = stats.pearsonr(a,b)
        if calcHistogram:
          plot_hist2d(file_histo+'_'+str(i),a,coeff[i]*b,Nbins,sigma_distr)

    print "===== SYMMETRIC TENSOR ====="
    print "===== Components ===="
    print "Coefficient",coeff
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "= TRACE DECOMPOSITION ="
    print "Coefficient",np.mean(coeff[0:3]),np.mean(coeff[3:6])
    print "Pearson coefficient",np.mean(pearson[0:3]),np.mean(pearson[3:6])
    print "====================="
    return pearson,pvalue,coeff

def pearson_3(tau_ind,tau_model,file_histo,Nbins,sigma_distr, calcHistogram):

    pearson = np.zeros(3)
    pvalue = np.zeros(3)
    coeff = np.sum(tau_ind[:,:,:,:3]*tau_model[:,:,:,:3], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:3]**2, axis=(0, 1, 2))
    n = tau_ind.shape[0]**3
    for i in xrange(0,3):
        a = np.reshape(tau_ind[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        pearson[i], pvalue[i] = stats.pearsonr(a,b)
        if calcHistogram:
          plot_hist2d(file_histo+'_'+str(i),a,coeff[i]*b,Nbins,sigma_distr)


    print "= 3 COMPONENTS TENSOR/VECTOR ==="
    print "Coefficient",coeff
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "====================="
    return pearson,pvalue,coeff


def plot_bestfit_pearson(folderout,time,bestfit_coef,bestfit_pears,labels):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    ms = 6.
    mysize = (6, 6)
    mydpi = 200
    li = ["-",":","--","-."]
    styles = ['o','^','s','v','<','x','h','8','1','3','>']
    Nmodels = bestfit_coef.shape[0]
    print Nmodels

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      plt.plot( time, bestfit_pears[i,:], marker=styles[i], label=labels[i], linewidth=lw)
    plt.ylim(-1.,1.)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Pearson coefficient', fontsize=fs)
    name = folderout+"_bestfit_pearson.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name


    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      pos = np.ma.masked_where(bestfit_coef[i,:] < 0, bestfit_coef[i,:])
      neg = np.ma.masked_where(bestfit_coef[i,:] > 0, abs(bestfit_coef[i,:]))
      plt.plot( time, pos, marker=styles[i], markersize=ms, label=labels[i], color='r', lw=0)
      plt.plot( time, neg, marker=styles[i], markersize=ms, color='b', lw=0)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Best-fit parameters', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_bestfit_coef.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

#    header = "Bestfit: time, pearson_cs, pearson_cm, pearsonv_grad, pearsonv_cross, pearsoni_domega, pearsoni_dj, pearsoni_grad, pearsoni_cross, pearsoni_dynamo, Cs, Cm, Cg, Cc, Domega, Dj, Dg, Dc, Dd \n"
    outname = folderout+"_best_fit_coef.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, best fit coefficient \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % bestfit_coef[im,cc])
      f.write("\n")
    print "Written bestfit ceofficient evolution at: ",outname

    outname = folderout+"_best_fit_pearson.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, best fit Pearson \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % bestfit_pears[im,cc])
      f.write("\n")

    f.close()
    print "Written bestfit Pearson evolution at: ",outname


#@profile
def plot_hist2d(file_histo,a,b,Nbins,sigma_distr):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200
    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    majorFormatter = FormatStrFormatter('%.1e')
    ax.xaxis.set_major_formatter(majorFormatter)
    ax.yaxis.set_major_formatter(majorFormatter)
    amean = np.mean(a)
    bmean = np.mean(b)
    astd = np.std(a)
    bstd = np.std(b)
    amin = amean - sigma_distr*astd
    amax = amean + sigma_distr*astd
    bmin = bmean - sigma_distr*bstd
    bmax = bmean + sigma_distr*bstd

    plt.locator_params(nbins=4)
    plt.xlabel('SFS', fontsize=fs)
    plt.ylabel('SGS model', fontsize=fs)
    name = file_histo+'.png'
    plt.hist2d(a, b, bins=Nbins, cmap='Greens', range=([amin,amax],[bmin,bmax]))

    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Pearson correlation figure at ",file_histo+'.png'

    return

def crazy_histogram2d(x, y, xmin, xmax, ymin, ymax, bins=10):
    try:
        nx, ny = bins
    except TypeError:
        nx = ny = bins
    xmin, xmax = x.min(), x.max()
    ymin, ymax = y.min(), y.max()
    dx = (xmax - xmin) / (nx - 1.0)
    dy = (ymax - ymin) / (ny - 1.0)

    weights = np.ones(x.size)

    # Basically, this is just doing what np.digitize does with one less copy
    xyi = np.vstack((x,y)).T
    xyi -= [xmin, ymin]
    xyi /= [dx, dy]
    xyi = np.floor(xyi, xyi).T

    # Now, we'll exploit a sparse coo_matrix to build the 2D histogram...
    grid = scipy.sparse.coo_matrix((weights, xyi), shape=(nx, ny)).toarray()

    return grid, np.linspace(xmin, xmax, nx), np.linspace(ymin, ymax, ny)


def plot_transfer(folderout,time,sigma,Nbins,quantity,labels,rmin):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200
    timename = str("%i" % (10*time)).zfill(3)

    nr = len(sigma_bins[0,:])

    ylow = 1.e-4

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in xrange(rmin, nr, 1):
        print sigma_pdf[:,r]
        np.histogram( sigma_pdf[:,r], bins=Nbins )
    plt.ylim([ylow,1.])
    plt.legend(loc = 1, fontsize=lgs)
    plt.xlabel(quantity, fontsize=fs)
    plt.ylabel('PDF', fontsize=fs)

    name = folderout+'_pdf_t'+timename+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

if __name__ == "__main__":
    main()

