import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.ticker as mtick
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy import ndimage
import h5py
import math
import os
import sys
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, LogLocator
from scipy import fftpack
from scipy.fftpack import ifftn
from mpi4py import MPI

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    npts = dset.shape[0]
    dset=dset[0:npts-1,0:npts-1]
    f.close()
    return dset

def readOriginalData3D(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    npts = dset.shape[0]
    dset=dset[0:npts-1,0:npts-1,0:npts-1]
    f.close()
    return dset

def readData3D(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset[...]

def saveData3D(f, v, data):
    f = h5py.File(f, "w")
    dset = f.create_dataset(v, data=data)
    f.close()

def createDataSet(f, v, shape, wSize, par=True):
    if par:
        f = h5py.File(f, "w", driver='mpio', comm=MPI.COMM_WORLD)
    else:
        f = h5py.File(f, "w")
    dset = f.create_dataset(v, shape)
    f.close()

def saveData3DWindow(f, v, data, w0, w1, w2, wSize, par=True):
    if par:
        f = h5py.File(f, "r+", driver='mpio', comm=MPI.COMM_WORLD)
    else:
        f = h5py.File(f, "r+")
    dset = f[v]
    dset[w0:w0 + wSize,w1:w1 + wSize,w2:w2 + wSize, ...] = data[:,:,:,...]
    f.close()

def readData3DWindow(f, v, w0, w1, w2, wSize):
    f = h5py.File(f, "r")
    dset = f[v]
    data=np.asarray(dset[w0:w0 + wSize,w1:w1 + wSize,w2:w2 + wSize])
    f.close()
    return data

def readData3DGhost(f, v, ghost):
    f = h5py.File(f, "r")
    dset = f[v]
    npts = dset.shape[0]
    dset = dset[0:npts-1,0:npts-1,0:npts-1]
    dset = np.pad(dset, ghost, 'wrap')

    f.close()
    return dset

def readData3DWindowGhost(f, v, w0, w1, w2, wSize, ghost):
    f = h5py.File(f, "r", driver='mpio', comm=MPI.COMM_WORLD)
    dset = f[v]
    dset = np.asarray(dset)

    sl0 = np.array(range(w0 - ghost,w0 + wSize + ghost)).reshape(-1,1, 1)%dset.shape[0]
    sl1 = np.array(range(w1 - ghost,w1 + wSize + ghost)).reshape(1,-1, 1)%dset.shape[1]
    sl2 = np.array(range(w2 - ghost,w2 + wSize + ghost)).reshape(1,1,-1)%dset.shape[2]
    result = dset[sl0,sl1,sl2]
    
    f.close()
    return result

def readData3DWindowGhostOriginal(f, v, w0, w1, w2, wSize, ghost):
    f = h5py.File(f, "r", driver='mpio', comm=MPI.COMM_WORLD)
    dset = f[v]
    npts = dset.shape[0]
    dset = dset[0:npts-1,0:npts-1,0:npts-1]

    sl0 = np.array(range(w0 - ghost,w0 + wSize + ghost)).reshape(-1,1, 1)%dset.shape[0]
    sl1 = np.array(range(w1 - ghost,w1 + wSize + ghost)).reshape(1,-1, 1)%dset.shape[1]
    sl2 = np.array(range(w2 - ghost,w2 + wSize + ghost)).reshape(1,1,-1)%dset.shape[2]

    f.close()
    return dset[sl0,sl1,sl2]

def der_tensor2D(fx,fy,L):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    dx = L/Nx
    dy = L/Ny
    strain = np.zeros((Nx,Ny,3))

    dxfx = (np.roll(fx,-1,axis=0) - np.roll(fx,1,axis=0) )/(2.*dx)
    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dy)
    dyfy = (np.roll(fy,-1,axis=1) - np.roll(fy,1,axis=1) )/(2.*dy)

    strain[:,:,0] = ( 2.*dxfx - dyfy )/3.
    strain[:,:,2] = ( 2.*dyfy - dxfx )/3.
    strain[:,:,1] = 0.5*( dxfy + dyfx )

    vorticity = 0.5*(dxfy - dyfx)

    return strain,vorticity

def der_tensor3D_vort(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    vorticity = np.zeros((Nx,Ny,Nz,3))

    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    dxfz = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    dyfz = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)

    dzfx = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    dzfy = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)

    vorticity[:,:,:,0] = 0.5*(dxfy - dyfx)
    vorticity[:,:,:,1] = 0.5*(dyfz - dzfy)
    vorticity[:,:,:,2] = 0.5*(dzfx - dxfz)

    return vorticity

def der_tensor3D_strain(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    strain = np.zeros((Nx,Ny,Nz,6))

    dxfx = (np.roll(fx,-1,axis=0) - np.roll(fx,1,axis=0) )/(2.*dx)
    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    dxfz = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    dyfy = (np.roll(fy,-1,axis=1) - np.roll(fy,1,axis=1) )/(2.*dx)
    dyfz = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)

    dzfx = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    dzfy = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)
    dzfz = (np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx)

    strain[:,:,:,0] = 0.5*(dxfx + dxfx)
    strain[:,:,:,1] = 0.5*(dyfy + dyfy)
    strain[:,:,:,2] = 0.5*(dzfz + dzfz)
    strain[:,:,:,3] = 0.5*(dxfy + dyfx)
    strain[:,:,:,4] = 0.5*(dyfz + dzfy)
    strain[:,:,:,5] = 0.5*(dzfx + dxfz)


    return strain


def grad3DAxis(S,dx, ax):

    return (np.roll(S,-1,axis=ax) - np.roll(S,1,axis=ax) )/(2.*dx)


def grad3D(S,dx):

    Nx = S.shape[0]
    Ny = S.shape[1]
    Nz = S.shape[2]
    grad = np.zeros((Nx,Ny,Nz,3))
    
    for i in range(0,3):
      grad[:,:,:,i] = (np.roll(S,-1,axis=i) - np.roll(S,1,axis=i) )/(2.*dx)

    return grad


def div3D(S,dx):
    
    div = ( np.roll(S[:,:,:,0],-1,axis=0) - np.roll(S[:,:,:,0],1,axis=0) + np.roll(S[:,:,:,1],-1,axis=1) - np.roll(S[:,:,:,1],1,axis=1) + np.roll(S[:,:,:,2],-1,axis=2) - np.roll(S[:,:,:,2],1,axis=2) )/(2.*dx)

    return div

def div3D_comp(vx,vy,vz,dx):
    
    div = ( np.roll(vx[:,:,:],-1,axis=0) - np.roll(vx[:,:,:],1,axis=0)  + np.roll(vy[:,:,:],-1,axis=1) - np.roll(vy[:,:,:],1,axis=1) + np.roll(vz[:,:,:],-1,axis=2) - np.roll(vz[:,:,:],1,axis=2) )/(2.*dx)

    return div


def strain3D(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    strain = np.zeros((Nx,Ny,Nz,6))

    df1 = (np.roll(fx,-1,axis=0) - np.roll(fx,1,axis=0) )/(2.*dx)
    df2 = (np.roll(fy,-1,axis=1) - np.roll(fy,1,axis=1) )/(2.*dx)

    strain[:,:,:,0] = ( 2.*df1 - df2 - (np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx) )/3.
    strain[:,:,:,1] = ( 2.*df2 - (np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx) - df1 )/3.
    strain[:,:,:,2] = ( 2.*(np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx) - df1 - df2 )/3.

    df2 = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    df1 = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    strain[:,:,:,3] = 0.5*( df2 + df1 )

    df2 = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)
    df1 = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)
    strain[:,:,:,4] = 0.5*( df2 + df1 )

    df1 = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    df2 = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)
    strain[:,:,:,5] = 0.5*( df1 + df2 )

    return strain

def vorticity3D(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    vorticity = np.zeros((Nx,Ny,Nz,3))

    df1 = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    df2 = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    vorticity[:,:,:,0] = 0.5*(df1 - df2)

    df1 = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)
    df2 = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)
    vorticity[:,:,:,1] = 0.5*(df1 - df2)

    df1 = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    df2 = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)
    vorticity[:,:,:,2] = 0.5*(df1 - df2)

    return vorticity


def writefields2D(folder, field, level):
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']

    maximum = [0, 0]
    minimum = [999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2]
    data = np.zeros(shape=(size[0], size[1]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:] = data2[:,:]
    f.close()
    f_sum.close()


def writefields3D(folder, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']

    maximum = [0, 0, 0]
    minimum = [999999, 999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            maximum[2] = max(maximum[2], patchExtents[i][1][2])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
            minimum[2] = min(minimum[2], patchExtents[i][0][2])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2, (maximum[2] - minimum[2]) + 2]
    data = np.zeros(shape=(size[0], size[1], size[2]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            rangeZ = (patchExtents[iPatch][0][2] - minimum[2], patchExtents[iPatch][1][2]+2 - minimum[2])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            sizeZ = patchExtents[iPatch][1][2] - patchExtents[iPatch][0][2] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY,sizeZ), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:,:] = data2[:,:,:]
    f.close()
    f_sum.close()


'''

    Averages using REBIN 
    https://scipython.com/blog/binning-a-2d-array-in-numpy/
    
'''

def avrg1D(field,sf):

    Nx = field.shape[0]
    return field.reshape([int(Nx/sf), sf]).mean(1)    

def avrg2D(field,sf):

    Nx = field.shape[0]
    Ny = field.shape[1]
    return field.reshape([int(Nx/sf), sf, int(Ny/sf), sf]).mean(3).mean(1)

def avrg3D(field,sf):

    Nx = field.shape[0]
    Ny = field.shape[1]
    Nz = field.shape[2]
    return field.reshape([int(Nx/sf), sf, int(Ny/sf), sf, int(Nz/sf), sf]).mean(5).mean(3).mean(1)


'''
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
Old code. Left for comparison purposes



def avrg2D(field,sf):
    Nx = field.shape[0]
    Ny = field.shape[1]
    field_filtered=np.zeros((Nx/sf,Ny/sf))

    for i in range(0,Nx,sf):
      for j in range(0,Ny,sf):
          field_filtered[i/sf,j/sf] = np.sum(field[i:(i+sf),j:(j+sf)])/(sf*sf)
    return field_filtered


def avrg1D(field,sf):
    Nx = field.shape[0]
    field_filtered=np.zeros((Nx/sf))

    for i in range(0,Nx,sf):
          field_filtered[i/sf] = np.sum(field[i:(i+sf)])/sf
    return field_filtered   


def avrg3D(field,sf):
    Nx = field.shape[0]
    Ny = field.shape[1]
    Nz = field.shape[2]
    field_filtered=np.zeros((Nx/sf,Ny/sf,Nz/sf))

    for i in range(0,Nx,sf):
      for j in range(0,Ny,sf):
        for k in range(0,Nz,sf):
          field_filtered[i/sf,j/sf,k/sf] = np.sum(field[i:(i+sf),j:(j+sf),k:(k+sf)])/(sf*sf*sf)
    return field_filtered
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
'''


def sim1d(f,dx):
    
    simpson1d = dx/3.*(f[0] + 4.*f[1] + f[2])
    return sim1d

def sim2d(f,dx):
    
    Nx = f.shape[0]
    Ny = f.shape[1]
    sim2d = np.zeros((Nx,Ny))

    sim2d[0,0] = 1.0
    sim2d[0,Ny-1] = 1.0
    sim2d[Nx-1,0] = 1.0
    sim2d[Nx-1,Ny-1] = 1.0

    for i in range(1,Nx-2,2):
        sim2d[i,0] = 4.0
        sim2d[i+1,0] = 2.0
        sim2d[i,Ny-1] = 4.0
        sim2d[i+1,Ny-1] = 2.0
        for j in range(1,Ny-2,2):
            sim2d[i,j] = 16.0
            sim2d[i+1,j] = 8.0
            sim2d[i,j+1] = 8.0
            sim2d[i+1,j+1] = 4.0
            if i==1:
                sim2d[0,j] = 4.0
                sim2d[0,j+1] = 2.0
                sim2d[Nx-1,j] = 4.0
                sim2d[Nx-1,j+1] = 2.0

    nwteo = 9*(Nx-2)*(Ny-2) + 12*(Nx-2) + 4
    w = sim2d*dx*dx/9.

    sim2d = np.sum(w*f)

    return sim2d


def int3D(f,dx):
    
    int3D = np.sum(f)*dx*dx*dx

    return int3D



def pdf(field,Nbins):

    xbins = np.zeros(Nbins)
    maxx=np.max([-np.min(field),np.max(field)])
    counts,binh=np.histogram(field, bins=Nbins, range=([-maxx,maxx]), normed=False, weights=None, density=None)
    pdf_pdfn = counts/float(np.sum(counts))
    pdf_x = binh[0:len(binh)-1]

    return pdf_x,pdf_pdfn


##@profile
def psd3Drad(field,kp,krad):

    N = kp.shape[0]
    FT = ifftn(field)

    power = FT.real[:N,:N,:N]*FT.real[:N,:N,:N] + FT.imag[:N,:N,:N]*FT.imag[:N,:N,:N]
    spe=np.zeros(N)
    sumu=np.zeros(N)
    nsum=np.zeros(N)
    dk=kp[1]-kp[0]

    for s in range(0,N):
        indices = np.where( (krad >= kp[s] - 0.5*dk) & (krad < kp[s] + 0.5*dk))
        sumu[s] = np.sum(power[indices])
        nsum[s] = power[indices].size
    
    psd3Drad = 2.*math.pi*kp*kp*sumu/nsum

    return psd3Drad




def tauf2D(folder,sf,L,iread,imag):

    rho  = readData(folder+"rho_0.hdf5","rho")
    vx   = readData(folder+"vx_0.hdf5","vx")
    vy   = readData(folder+"vy_0.hdf5","vy")
    Nx = vx.shape[0]
    Ny = vx.shape[1]

    Bx   = 0.*vx
    By   = Bx
    Jz   = Bx

    rhof  = avrg2D(rho,sf)
    vxf   = avrg2D(rho*vx,sf)/rhof
    vyf   = avrg2D(rho*vy,sf)/rhof
    vxvxf = avrg2D(rho*vx*vx,sf)/rhof
    vxvyf = avrg2D(rho*vx*vy,sf)/rhof
    vyvyf = avrg2D(rho*vy*vy,sf)/rhof

    Nxf = vxf.shape[0]
    Nyf = vxf.shape[1]
    dx = 2.*L/Nxf
    dy = 2.*L/Nyf
    htau = dx*dy


    u_xx = rhof*( vxvxf - vxf*vxf )
    u_xy = rhof*( vxvyf - vxf*vyf )
    u_yy = rhof*( vyvyf - vyf*vyf )

    tauf2D.esgs = 0.5*( rhof*(vxvxf - vxf*vxf + vyvyf - vyf*vyf) )

    b_xx = np.zeros((Nxf,Nyf))
    b_xy = np.zeros((Nxf,Nyf))
    b_yy = np.zeros((Nxf,Nyf))
    tauf2D.i = np.zeros((Nxf,Nyf))
    tauf2D.v = np.zeros((Nxf,Nyf,3))

    if imag==1:
      Bx   = readData(folder+"Bx_0.hdf5","Bx")
      By   = readData(folder+"By_0.hdf5","By")

      if iread == 1:
        Jz = readData3D(folder+"Jz_0.hdf5","Jz")

      else:
        _, Jz = der_tensor2D(Bx,By,L)

      Bxf = avrg2D(Bx,sf)
      Byf = avrg2D(By,sf)
      vxByf = avrg2D(rho*vx*By,sf)/rhof
      vyBxf = avrg2D(rho*vy*Bx,sf)/rhof
      BxBxf = avrg2D(Bx*Bx,sf)
      BxByf = avrg2D(Bx*By,sf)
      ByByf = avrg2D(By*By,sf)
      b2    = Bx*Bx + By*By
      b2f   = avrg2D(b2,sf)
      bmod  = np.sqrt(b2)
      bmodf = np.sqrt(b2f)
      Jzf   = avrg2D(Jz,sf)

      taudiag = 0.5*(b2f - (Bxf*Bxf + Byf*Byf ) )
      b_xx = BxBxf - Bxf*Bxf - taudiag
      b_xy = BxByf - Bxf*Byf
      b_yy = BxBxf - Byf*Byf - taudiag
      tauf2D.i = (vxByf - vxf*Byf) - (vyBxf - vyf*Bxf)

      tauf2D.esgs = tauf2D.esgs + 0.5*(BxBxf - Bxf*Bxf + ByByf - Byf*Byf)

      tauf2D.Bxf = Bxf
      tauf2D.Bx = Bx
  
# Hereafter, the convention for the tensors is as follows: the first two indices are the position, the third indicates the component of the tensor.
# For symmetric tensors (tauf2D.v, vst, mst): 0 = xx, 1 = xy, 2 = yy
# For antisymmetric tensors (vort, curr, tauf2D.i): only one component, the xy one.

    if iread == 1:
      St = np.zeros((Nx,Ny,3))
      St[:,:,0] = readData3D(folder+"Stxx_0.hdf5","Stxx")
      St[:,:,2] = readData3D(folder+"Styy_0.hdf5","Styy")
      St[:,:,1] = readData3D(folder+"Stxy_0.hdf5","Stxy")

# IF St files are not available, use the following subroutine and comment above
    else:
      St, _ = der_tensor2D(vx,vy,L)

    Stxxf = avrg2D(St[:,:,0],sf)
    Styyf = avrg2D(St[:,:,2],sf)
    Stxyf = avrg2D(St[:,:,1],sf)
    Jzf = np.zeros((Nxf,Nyf))


    tauf2D.v[:,:,0] = u_xx - b_xx
    tauf2D.v[:,:,1] = u_xy - b_xy
    tauf2D.v[:,:,2] = u_yy - b_yy
    tauf2D.tvv = Stxxf*tauf2D.v[:,:,0] + 2.*Stxyf*tauf2D.v[:,:,1] + Styyf*tauf2D.v[:,:,2]
    tauf2D.tbb = 2.*tauf2D.i*Jzf

    tauf2D.h = dx*dy
    tauf2D.rhof = rhof
    tauf2D.vxf = vxf
    tauf2D.vyf = vyf

    if imag == 1:
      tauf2D.Bxf = Bxf
      tauf2D.Byf = Byf
      tauf2D.bmodf = bmodf






def draw(field, fileimg, minf, maxf):
 
    field_r=ndimage.rotate(field, 90)
    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.imshow(field_r, clim=(minf,maxf))
    plt.colorbar(fraction=0.046, pad=0.04)
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()
    plt.savefig(fileimg, bbox_inches='tight')
    plt.close()
    print("Figure created: ", fileimg)

##profile
def draw_labels(x,y,field, fileout, minf, maxf, xlab, ylab, labelcol):
 
#    field_r=ndimage.rotate(field, 90)
    field_r=field
    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

#    Nx=len(xticks)
#    Ny=len(yticks)
#    xx=np.zeros((Nx,Ny))
#    yy=np.zeros((Nx,Ny))
#    for ii in range(0,Nx):
#     for jj in range(0,Ny):
#      xx[ii,jj] = xticks[ii]
#      yy[ii,jj] = yticks[jj]

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
#    fig = plt.figure(figsize=mysize, dpi=mydpi)
#    ax = fig.add_subplot(111, projection='3d')
    plt.contourf(x,y,field_r,100)#, clim=(minf,maxf))
#    ax.plot_surface(xx, yy, field, color='b')
    ax.tick_params(labelsize=lz)
    plt.locator_params(axis='x', nbins=4, format='sci')
    plt.locator_params(axis='y', nbins=4)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
    ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))

#    ax.ticklabel_format('sci')

    tickscol = ([minf,0.5*(minf+maxf),maxf])
#    tickscolr=arrayfun(@(x) sprintf('%.2f',x),tickscol,'un',0)
    plt.colorbar(fraction=0.046, pad=0.04, ticks=tickscol, label=labelcol)
    
#    ax.w_xaxis.set_major_locator(LinearLocator(5))
#    ax.w_yaxis.set_major_locator(LinearLocator(5))
    plt.xlabel(xlab, fontsize=fs)
    plt.ylabel(ylab, fontsize=fs)
#    plt.xticks(xticks)
#    plt.yticks(([0,len(yticks)-1]),([minf,maxf]))
#    ax.set_yticks([np.min(yticks),0.5*(np.min(yticks)+np.max(yticks)),np.max(yticks)])


    plt.tight_layout()
    plt.savefig(fileout, bbox_inches='tight')
#    plt.show()
    plt.close()
    print("Figure created: ", fileout)


          
def plot_evo(folderout,time,Etotevo,Eintevo,Ekinevo,Enstevo,Emagevo,labels,li,col,imag):

    nr = len(Ekinevo[0,:])

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        ltot=""
        lint=""
        lkin=""
        lmag=""
        if r == 0:
           ltot='Etot'
           lint='Eint'
           lkin='Ekin'
           lmag='Emag'
        plt.semilogy( time[:,r], Etotevo[:,r], linestyle="-", label=ltot, linewidth=lw)
        plt.semilogy( time[:,r], Ekinevo[:,r], linestyle=":", label=lkin, linewidth=lw)
        plt.semilogy( time[:,r], Eintevo[:,r], linestyle="--", label=lint, linewidth=lw)
        if imag==1:
            plt.semilogy( time[:,r], Emagevo[:,r], linestyle="-.", label=lmag, linewidth=lw)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Energy evolution', fontsize=fs)
    name = folderout+'evolution_energy.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        ltot=""
        lint=""
        lkin=""
        lmag=""
        if r == 0:
           ltot='Etot'
           lint='Eint'
           lkin='Ekin'
           lmag='Emag'
        plt.semilogy( time[:,r], Etotevo[:,r]/Etotevo[0,r],  label =ltot, linestyle="-", color=col[r], linewidth=lw ) 
        plt.semilogy( time[:,r], Eintevo[:,r]/Etotevo[0,r],  label =lint, linestyle="--", color=col[r], linewidth=lw ) 
        plt.semilogy( time[:,r], Ekinevo[:,r]/Etotevo[0,r],  label =lkin, linestyle=":", color=col[r], linewidth=lw ) 
        if imag==1:
            plt.semilogy( time[:,r], Emagevo[:,r]/Etotevo[0,r], linestyle="-.", label=lmag, color=col[r], linewidth=lw)
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Relative Energy', fontsize=fs)
    name = folderout+'evolution_relative.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        plt.semilogy( time[:,r], Enstevo[:,r]/Etotevo[0,r],  label =labels[r]+': Enst', linestyle=li[r], color=col[r], linewidth=lw ) 
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Enstrophy', fontsize=fs)
    name = folderout+'evolution_enstrophy.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_helicity(folderout,time,Hkinevo,Hcrossevo,labels,li,col,imag):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    nr = len(Hkinevo[0,:])

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        lkin=""
        lmag=""
        if r == 0:
           lkin='$|\int H_{kin}|$'
           lmag='$|\int H_{cross}|$'
        plt.semilogy( time[:,r], np.abs(Hkinevo[:,r]),  label =lkin, linestyle=":", color=col[r], linewidth=lw) 
        if imag==1:
            plt.semilogy( time[:,r], np.abs(Hcrossevo[:,r]), linestyle="--", label=lmag, color=col[r], linewidth=lw)
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Helicities', fontsize=fs)
    name = folderout+'evolution_helicity.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_spectra(folderout,time,kp,spekinevo,spemagevo,labels,li,col,imag):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    nr = len(spekinevo[0,:])
    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        plt.loglog( kp, spekinevo[:,r], label = labels[r], color=col[r],linestyle="-", linewidth=lw ) 
        if imag==1:
            plt.loglog( kp, spemagevo[:,r], color=col[r],linestyle="--", linewidth=lw)

    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('k', fontsize=fs)
    plt.ylabel('Spectra', fontsize=fs)

    name = folderout+'spectra_t'+str("%i" % (10*time)).zfill(3)+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



