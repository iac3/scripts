import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import math
import os
import sys
from scipy.optimize import fmin
from scipy import stats
from common import psd3Drad, readData3D, readData3DWindow, der_tensor3D_vort, der_tensor3D_strain, pdf, int3D, avrg3D, vorticity3D, strain3D, div3D, div3D_comp, grad3D, avrg1D, grad3DAxis, saveData3D, saveData3DWindow, createDataSet
from multiprocessing import Pool
import itertools
import shutil
import time


def main():

    runs = []
    maxcycle = []
    labels = []
    dt = []

    folder="/home/bminano/datos/bitbucket/bssn/tests/MHD/KH3D/python/"
    runs.append("o400")
    maxcycle.append(27000)
    labels.append("400^3")
    dt.append(8.e-4)
    Npoints = 401

    '''
    runs = []
    labels = []
    maxcycle = []
    dt =[]
    
    folder="/home/bminano/datos/bitbucket/bssn/tests/MHD/KH3D/python/"

    runs.append("outputDir_100")
    maxcycle.append(8000)
    labels.append("N=100")
    dt.append(2.5e-3)
    Npoints = 101
    '''
    folderout = "/home/bminano/datos/bitbucket/bssn/tests/MHD/KH3D/python/TESTS_IMAGES_PEARSON_400_sf5/"

# =========================================================================
# Size of the filter (number of cells to be averaged out in each direction)
# Any integer > 1, which is a divisor of (Npoint-1)
    Size_filter = 5
# Number of cycles, excluding t=0 and considering the available outputs: i*(maxcycle / Ncycle), i=1,Ncycle
    Ncycle = 3
# Size of parallel patches. It has to be a multiple of Size_filter, and a divisor of Npoints
    wSize = 200
# Number of processors for parallel sections
    nprocs = 4
# =========================================================================
# =========================================================================


# Size of the simulation box.
    L = 1.
# Ideal gas coefficient
    gamma = 4./3.
    li = ("-","--",":","-.","-","--",":","-.","-","--",":","-.","-","--",":","-.","-","--",":","-.","-")
    col = ('k','m','r','b','c','g','orange','y','chocolate','darkcyan','darksalmon')

    print("output folder for images: ", folderout)
    print("Size filter: ", Size_filter)
    dt = np.asarray(dt)
    maxcycle = np.asarray(maxcycle)
    nr = len(runs)

    if not os.path.exists(folderout):
        os.makedirs(folderout)

    analyze3D_filter(folder,folderout,runs,labels,li,col,maxcycle,dt,L,gamma,Npoints,Ncycle,Size_filter, wSize, nprocs)


def analyze3D_filter(folder,folderout,runs,labels,li,col,maxcycle,dt,L,gamma,Npoints,Ncycle,sf, wSize, nprocs):

    dx = L/Npoints

    Nx = Npoints - 1
    Nxf = Nx/sf
    print(Nx, Nxf)

# Number of bins of the probability distribution functions. Can be varied
    Npdf = 10

    nr = len(runs)
    dcycle = (maxcycle/(Ncycle)).astype(int)
    timeC = np.zeros((Ncycle,nr))

    headerevo = "t, sigma_mom, sigma_ind\n"
    prename=folderout+"lingrad3D_"+str((Npoints-1))+"_filt"+str((Npoints-1)/sf)


    # Creation of window patches for parallel sections
    if wSize > Npoints - 1:
      raise ValueError('wSize cannot be greater than Npoints - 1')
    window_idxs = [(i, j, k) for i, j, k in
                     itertools.product(range(0, Npoints-1, wSize),
                                       range(0, Npoints-1, wSize),
                                       range(0, Npoints-1, wSize))]


    p = Pool(nprocs)
    for r in range(0, nr, 1):

        run = folder+runs[r]
        print("======================================================")
        print("======================================================")
        print("RUN :",run)
        folder_txt = folderout+"/borja_FILTERING_"+str((Npoints-1)/sf)
        if not os.path.exists(folder_txt):
            os.makedirs(folder_txt)
        fevo=open(folder_txt+"/transfer_evolution.out","w+")
        fevo.write(headerevo)
        for cc in range(0, Ncycle, 1):
            timeC[cc,r] = dcycle[r]*(cc+1)*dt[r]
            timename = "t"+str("%i" % (10*timeC[cc,r])).zfill(3)
            print("======================================================")
            print("TIME: ",timeC[cc,r])

            foldertime="/visit_dump."+str(dcycle[r]*(cc+1)).zfill(5)+"/"
            print(run+foldertime)
            savename = folderout+foldertime+ "filtered_SF_parallel"+str(sf)
            if not os.path.exists(savename):
                os.makedirs(savename)
            createDataSet(savename + "/rhof.hdf5", "rhof", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/rhovxf.hdf5", "rhovxf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/rhovyf.hdf5", "rhovyf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/rhovzf.hdf5", "rhovzf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/Bxf.hdf5", "Bxf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/Byf.hdf5", "Byf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/Bzf.hdf5", "Bzf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/pf.hdf5", "pf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/bmodf.hdf5", "bmodf", (Nxf, Nxf, Nxf), int(wSize/sf))
            createDataSet(savename + "/tau_kin.hdf5", "tau_kin", (Nxf, Nxf, Nxf, 6), int(wSize/sf))
            createDataSet(savename + "/tau_ind.hdf5", "tau_ind", (Nxf, Nxf, Nxf, 3), int(wSize/sf))
            createDataSet(savename + "/tau_mag.hdf5", "tau_mag", (Nxf, Nxf, Nxf, 6), int(wSize/sf))
            createDataSet(savename + "/tau_enth.hdf5", "tau_enth", (Nxf, Nxf, Nxf, 3), int(wSize/sf))
            createDataSet(savename + "/tau_mom.hdf5", "tau_mom", (Nxf, Nxf, Nxf, 6), int(wSize/sf))
            createDataSet(savename + "/vtau_mom.hdf5", "vtau_mom", (Nxf, Nxf, Nxf, 3), int(wSize/sf))
            createDataSet(savename + "/btau_ind.hdf5", "btau_ind", (Nxf, Nxf, Nxf, 3), int(wSize/sf))
            # Parallel execution of decoupled fields
            print("Save Evol")            
            start_time = time.time()
            p.map(saveEvol, zip(itertools.repeat(run+foldertime), itertools.repeat(folderout+foldertime), itertools.repeat(sf), itertools.repeat(L), itertools.repeat(gamma), itertools.repeat(Npdf), window_idxs, itertools.repeat(wSize)))
            print("saveEvol: ", time.time() - start_time)
            # Divergence and PDF calculations (separated for memory performance)
            print("div1")
            start_time = time.time()
            divergences1a(folderout+foldertime, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            divergences1aa(folderout+foldertime, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            divergences1b(folderout+foldertime, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            print("div2")
            divergences2a(folderout+foldertime, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            divergences2b(folderout+foldertime, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            print("pdf")
            PDF(folderout+foldertime, fevo, timeC[cc,r], L/(Npoints-1) * sf, sf, gamma, Npdf, wSize, L)
            print("divs and pdf: ", time.time() - start_time)
            
        fevo.close()
        print("Written evolution quantities at: ",folder_txt+"/transfer_evolution.out")
  
# THIS ROUTINE LOADS OR CALLS THE SFS TENSORS AND PERFORMS THE PEARSON ANALYSIS
def saveEvol(args):

  folder, folderOut,sf,L,gamma,Npdf, window_idxs, wSize = args
  w0, w1, w2 = window_idxs

  tauf3D(folder, folderOut,Npdf,sf,L,gamma, w0, w1, w2, wSize)

  print("SAVED WINDOW: ",window_idxs)

   
def tauf3D(folder, folderOut,Npdf,sf,L,gamma, w0, w1, w2, wSize):

    savename = folderOut+ "filtered_SF_parallel"+str(sf)

    print("tauf3D init: ",w0, w1, w2)

    rho  = readData3DWindow(folder+"rho_0.hdf5","rho", w0, w1, w2, wSize)
    vx   = readData3DWindow(folder+"vx_0.hdf5","vx", w0, w1, w2, wSize)
    vy   = readData3DWindow(folder+"vy_0.hdf5","vy", w0, w1, w2, wSize)
    vz   = readData3DWindow(folder+"vz_0.hdf5","vz", w0, w1, w2, wSize)
    Nx = vx.shape[0]
    Ny = vx.shape[1]
    Nz = vx.shape[2]

    Nxf = int(Nx/sf)
    Nyf = int(Ny/sf)
    Nzf = int(Nz/sf)
# FILTERING

    Bx = readData3DWindow(folder+"Bx_0.hdf5","Bx", w0, w1, w2, wSize)
    By = readData3DWindow(folder+"By_0.hdf5","By", w0, w1, w2, wSize)
    Bz = readData3DWindow(folder+"Bz_0.hdf5","Bz", w0, w1, w2, wSize)
    p   = readData3DWindow(folder+"p_0.hdf5","p", w0, w1, w2, wSize)

    saveData3DWindow(savename + "/bmodf.hdf5", "bmodf", avrg3D(np.sqrt(Bx*Bx + By*By + Bz*Bz), sf), int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D bmodf")
    # quantities that are used all the time 
    rho_av = avrg3D(rho,sf)
    saveData3DWindow(savename + "/rhof.hdf5", "rhof", rho_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    rhovx_av = avrg3D(rho*vx,sf)/rho_av
    saveData3DWindow(savename + "/rhovxf.hdf5", "rhovxf", rhovx_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))

    rhovy_av = avrg3D(rho*vy,sf)/rho_av
    saveData3DWindow(savename + "/rhovyf.hdf5", "rhovyf", rhovy_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    rhovz_av = avrg3D(rho*vz,sf)/rho_av
    saveData3DWindow(savename + "/rhovzf.hdf5", "rhovzf", rhovz_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D rhov")
    Bx_av = avrg3D(Bx,sf)
    saveData3DWindow(savename + "/Bxf.hdf5", "Bxf", Bx_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    By_av = avrg3D(By,sf)
    saveData3DWindow(savename + "/Byf.hdf5", "Byf", By_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    Bz_av = avrg3D(Bz,sf)
    saveData3DWindow(savename + "/Bzf.hdf5", "Bzf", Bz_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    p_av  = avrg3D(p,sf)
    saveData3DWindow(savename + "/pf.hdf5", "pf", p_av, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))

    print("tauf3D B and p")

# Hereafter, the convention for the tensors is as follows: the first three indices are the 3D position,
# the fourth indicate the component of the tensor, labeled as follows:
# for symmetric tensors (tau_kin, tau_mag, stf, mstf): 0 = xx, 1 = yy, 2 = zz, 3 = xy, 4 = yz, 5 = zx
# for antisymmetric tensors (tau_ind, vortf, jf): 0 = xy, 1 = yz, 2 = zx
# for vectors (tau_enth, vtau_mom, batu_ind), 0 = x, 1 = y, 2 = z
# We are not defining rhof, vxf, etc. to save memory, event though it increases CPU time (avrg3D, grad3D are called over and over)

    tau_kin=np.zeros((Nxf,Nyf,Nzf,6))
    tau_kin[:,:,:,0] = rho_av*rhovx_av*rhovx_av - avrg3D(rho*vx*vx,sf)
    tau_kin[:,:,:,1] = rho_av*rhovy_av*rhovy_av - avrg3D(rho*vy*vy,sf)
    tau_kin[:,:,:,2] = rho_av*rhovz_av*rhovz_av - avrg3D(rho*vz*vz,sf)
    tau_kin[:,:,:,3] = rho_av*rhovx_av*rhovy_av - avrg3D(rho*vx*vy,sf)
    tau_kin[:,:,:,4] = rho_av*rhovy_av*rhovz_av - avrg3D(rho*vy*vz,sf)
    tau_kin[:,:,:,5] = rho_av*rhovz_av*rhovx_av - avrg3D(rho*vz*vx,sf)
    saveData3DWindow(savename + "/tau_kin.hdf5", "tau_kin", tau_kin, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D tau_kin")
    tau_ind=np.zeros((Nxf,Nyf,Nzf,3))
    tau_ind[:,:,:,0] = (rhovx_av*By_av - rhovy_av*Bx_av) - ( avrg3D(vx*By,sf) - avrg3D(vy*Bx,sf) )
    tau_ind[:,:,:,1] = (rhovy_av*Bz_av - rhovz_av*By_av) - ( avrg3D(vy*Bz,sf) - avrg3D(vz*By,sf) )
    tau_ind[:,:,:,2] = (rhovz_av*Bx_av - rhovx_av*Bz_av) - ( avrg3D(vz*Bx,sf) - avrg3D(vx*Bz,sf) )
    saveData3DWindow(savename + "/tau_ind.hdf5", "tau_ind", tau_ind, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D tau_ind")
    tau_mag=np.zeros((Nxf,Nyf,Nzf,6))
    tau_mag[:,:,:,0] = Bx_av*Bx_av - avrg3D(Bx*Bx,sf)  
    tau_mag[:,:,:,1] = By_av*By_av - avrg3D(By*By,sf)  
    tau_mag[:,:,:,2] = Bz_av*Bz_av - avrg3D(Bz*Bz,sf)  
    tau_mag[:,:,:,3] = Bx_av*By_av - avrg3D(Bx*By,sf)  
    tau_mag[:,:,:,4] = By_av*Bz_av - avrg3D(By*Bz,sf)  
    tau_mag[:,:,:,5] = Bz_av*Bx_av - avrg3D(Bz*Bx,sf)
    saveData3DWindow(savename + "/tau_mag.hdf5", "tau_mag", tau_mag, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D tau_mag")
    tau_mom = tau_mom_def(tau_kin,tau_mag)
    saveData3DWindow(savename + "/tau_mom.hdf5", "tau_mom", tau_mom, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D tau_mom")
    vtau_mom = np.zeros((Nxf,Nyf,Nzf,3))
    vtau_mom[:,:,:,0] = rhovx_av*tau_mom[:,:,:,0] + rhovy_av*tau_mom[:,:,:,3] + rhovz_av*tau_mom[:,:,:,5]
    vtau_mom[:,:,:,1] = rhovx_av*tau_mom[:,:,:,3] + rhovy_av*tau_mom[:,:,:,1] + rhovz_av*tau_mom[:,:,:,4]
    vtau_mom[:,:,:,2] = rhovx_av*tau_mom[:,:,:,5] + rhovy_av*tau_mom[:,:,:,4] + rhovz_av*tau_mom[:,:,:,2]
    saveData3DWindow(savename + "/vtau_mom.hdf5", "vtau_mom", vtau_mom, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D vtau_mom")
    btau_ind = np.zeros((Nxf,Nyf,Nzf,3))
    btau_ind[:,:,:,0] = By_av*tau_ind[:,:,:,0] - Bz_av*tau_ind[:,:,:,2]
    btau_ind[:,:,:,1] = Bz_av*tau_ind[:,:,:,1] - Bx_av*tau_ind[:,:,:,0]
    btau_ind[:,:,:,2] = Bx_av*tau_ind[:,:,:,2] - By_av*tau_ind[:,:,:,1]
    saveData3DWindow(savename + "/btau_ind.hdf5", "btau_ind", btau_ind, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D btau_ind")

# ENTHALPY RELATED TENSORS
# I have verified that, with the EoS, tau_advp = (gamma - 1)*tau_conv, at each single point.
# Since their sum is tau_enth, I just calculate tau_enth directly to save memory
# However, please put them both of them in case of non ideal EoS

    tau_enth = np.zeros((Nxf,Nyf,Nzf,3))
    fgamma = gamma/(gamma-1.0)
    tau_enth[:,:,:,0] = fgamma*( p_av*rhovx_av - avrg3D(p*vx,sf) )
    tau_enth[:,:,:,1] = fgamma*( p_av*rhovy_av - avrg3D(p*vy,sf) )
    tau_enth[:,:,:,2] = fgamma*( p_av*rhovz_av - avrg3D(p*vz,sf) )
    saveData3DWindow(savename + "/tau_enth.hdf5", "tau_enth", tau_enth, int(w0/sf), int(w1/sf), int(w2/sf), int(wSize/sf))
    print("tauf3D tau_enth")

#    tau_conv=np.zeros((Nxf,Nyf,Nzf,3))
#    tau_conv[:,:,:,0] = - avrg3D(rho*eint*vx,sf) + avrg3D(rho*vx,sf)*avrg3D(rho*eint,sf)/avrg3D(rho,sf)
#    tau_conv[:,:,:,1] = - avrg3D(rho*eint*vy,sf) + avrg3D(rho*vy,sf)*avrg3D(rho*eint,sf)/avrg3D(rho,sf)
#    tau_conv[:,:,:,2] = - avrg3D(rho*eint*vz,sf) + avrg3D(rho*vz,sf)*avrg3D(rho*eint,sf)/avrg3D(rho,sf)
#    tau_conv_div = div3D(tau_conv,dxf)

#    tau_advp=np.zeros((Nxf,Nyf,Nzf,3))
#    tau_advp[:,:,:,0] = - avrg3D(p*vx,sf) + avrg3D(rho*vx,sf)/avrg3D(rho,sf)*avrg3D(p,sf)
#    tau_advp[:,:,:,1] = - avrg3D(p*vy,sf) + avrg3D(rho*vy,sf)/avrg3D(rho,sf)*avrg3D(p,sf)
#    tau_advp[:,:,:,2] = - avrg3D(p*vz,sf) + avrg3D(rho*vz,sf)/avrg3D(rho,sf)*avrg3D(p,sf)
#    tau_advp_div = div3D(tau_advp,dxf)

# ========== END OF SFS TENSORS CALCULATIONS ===================




def divergences1a(folder, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    rhovxf = readData3D(savename + "/rhovxf.hdf5", "rhovxf")
    rhovyf = readData3D(savename + "/rhovyf.hdf5", "rhovyf")
    rhovzf = readData3D(savename + "/rhovzf.hdf5", "rhovzf")  

    stf = der_tensor3D_strain(rhovxf,rhovyf,rhovzf,L)

    saveData3D(savename + "/stf.hdf5", "stf", stf)

def divergences1aa(folder, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    stf = readData3D(savename + "/stf.hdf5", "stf")
    tau_mom = readData3D(savename + "/tau_mom.hdf5", "tau_mom")
    sigma_mom = stf[:,:,:,0]*tau_mom[:,:,:,0] + stf[:,:,:,1]*tau_mom[:,:,:,1] + stf[:,:,:,2]*tau_mom[:,:,:,2] + 2.*stf[:,:,:,3]*tau_mom[:,:,:,3] + 2.*stf[:,:,:,4]*tau_mom[:,:,:,4] + 2.*stf[:,:,:,5]*tau_mom[:,:,:,5]

    saveData3D(savename + "/sigma_mom.hdf5", "sigma_mom", sigma_mom)


def divergences1b(folder, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    rhovxf = readData3D(savename + "/rhovxf.hdf5", "rhovxf")
    rhovyf = readData3D(savename + "/rhovyf.hdf5", "rhovyf")
    rhovzf = readData3D(savename + "/rhovzf.hdf5", "rhovzf")   

    vortf = der_tensor3D_vort(rhovxf,rhovyf,rhovzf,L)
    saveData3D(savename + "/vortf.hdf5", "vortf", vortf)

def divergences2a(folder, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    tau_ind = readData3D(savename + "/tau_ind.hdf5", "tau_ind")


    Bxf = readData3D(savename + "/Bxf.hdf5", "Bxf")
    Byf = readData3D(savename + "/Byf.hdf5", "Byf")
    Bzf = readData3D(savename + "/Bzf.hdf5", "Bzf")

    jf = der_tensor3D_vort(Bxf,Byf,Bzf,L)

    sigma_ind = 2.*tau_ind[:,:,:,0]*jf[:,:,:,0] + 2.*tau_ind[:,:,:,1]*jf[:,:,:,1] + 2.*tau_ind[:,:,:,2]*jf[:,:,:,2]

    saveData3D(savename + "/jf.hdf5", "jf", jf)
    saveData3D(savename + "/sigma_ind.hdf5", "sigma_ind", sigma_ind)


def divergences2b(folder, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    Bxf = readData3D(savename + "/Bxf.hdf5", "Bxf")
    Byf = readData3D(savename + "/Byf.hdf5", "Byf")
    Bzf = readData3D(savename + "/Bzf.hdf5", "Bzf")

    mstf = der_tensor3D_strain(Bxf,Byf,Bzf,L)
    saveData3D(savename + "/mstf.hdf5", "mstf", mstf)

def PDF(folder, fevo, time, dxf, sf, gamma, Npdf, wSize, L):

    savename = folder + "filtered_SF_parallel" + str(sf)

    vtau_mom = readData3D(savename + "/vtau_mom.hdf5", "vtau_mom")
    btau_ind = readData3D(savename + "/btau_ind.hdf5", "btau_ind")
    tau_enth = readData3D(savename + "/tau_enth.hdf5", "tau_enth")
    sigma_mom = readData3D(savename + "/sigma_mom.hdf5", "sigma_mom")
    sigma_ind = readData3D(savename + "/sigma_ind.hdf5", "sigma_ind")

    vtau_mom_div = div3D(vtau_mom,dxf)
    btau_ind_div = div3D(btau_ind,dxf)
    tau_enth_div = div3D(tau_enth,dxf)


# evaluation of energy equation terms: probability distribution functions
    tau_enth_div_bins,tau_enth_div_pdf = pdf(tau_enth_div,Npdf)
    vtau_mom_div_bins,vtau_mom_div_pdf = pdf(vtau_mom_div,Npdf)
    btau_ind_div_bins,btau_ind_div_pdf = pdf(btau_ind_div,Npdf)

    sigma_mom_bins,sigma_mom_pdf = pdf(sigma_mom,Npdf)
    sigma_mom_evo = int3D(sigma_mom,dxf)
    sigma_ind_bins,sigma_ind_pdf = pdf(sigma_ind,Npdf)
    sigma_ind_evo = int3D(sigma_ind,dxf)




# Writing to the evolution file
    fevo.write("%10.4e %10.4e %10.4e \n" % (time,sigma_mom_evo,sigma_ind_evo))

    saveData3D(savename + "/tau_enth_div_bins.hdf5", "tau_enth_div_bins", tau_enth_div_bins)
    saveData3D(savename + "/tau_enth_div_pdf.hdf5", "tau_enth_div_pdf", tau_enth_div_pdf)
    saveData3D(savename + "/vtau_mom_div_bins.hdf5", "vtau_mom_div_bins", vtau_mom_div_bins)
    saveData3D(savename + "/vtau_mom_div_pdf.hdf5", "vtau_mom_div_pdf", vtau_mom_div_pdf)
    saveData3D(savename + "/btau_ind_div_bins.hdf5", "btau_ind_div_bins", btau_ind_div_bins)
    saveData3D(savename + "/btau_ind_div_pdf.hdf5", "btau_ind_div_pdf", btau_ind_div_pdf)
    saveData3D(savename + "/sigma_mom_bins.hdf5", "sigma_mom_bins", sigma_mom_bins)
    saveData3D(savename + "/sigma_mom_pdf.hdf5", "sigma_mom_pdf", sigma_mom_pdf)
    saveData3D(savename + "/sigma_ind_bins.hdf5", "sigma_ind_bins", sigma_ind_bins)
    saveData3D(savename + "/sigma_ind_pdf.hdf5", "sigma_ind_pdf", sigma_ind_pdf)


def tau_mom_def(tau_kin,tau_mag):

    tau_mom = tau_kin - tau_mag
    trace_tau_mag = tau_mag[:,:,:,0] + tau_mag[:,:,:,1] + tau_mag[:,:,:,2]
    tau_mom [:,:,:,0] = tau_mom [:,:,:,0] + 0.5*trace_tau_mag
    tau_mom [:,:,:,1] = tau_mom [:,:,:,1] + 0.5*trace_tau_mag
    tau_mom [:,:,:,2] = tau_mom [:,:,:,2] + 0.5*trace_tau_mag

    return tau_mom



if __name__ == "__main__":
    main()


