import sys, getopt
import os
import numpy as np
import h5py
from scipy import integrate
import math

"""
    This script calculates wave harmonics with data provided by extract_sphere.py.
"""

folderout = "../Harmonics/"         #Output folder
folders = {"../outputDir_mesh_c5": [544000, 672000, 928000, 1184000]}       #Input folders
level = 6                           #Data mesh level
res = np.array([160, 80])           #Resolution of the sphere data
radius = [28]                       #List of spheres to calculate harmonics
dt = 4/(2**9)                       #Simulation dt at finer level (the finer level could be different than level parameter)

folderin = "level8_multilevel6"     #Input folder (Complete path is folders + folderin)

def readfield(folder, field):
    """ Read data """
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data
    
def artifical_function():
    """ Artificial function for testing purposes. """
    d_phi = np.pi / res[1]
    d_theta = 2 * np.pi / res[0]
    field = np.zeros((res[0], res[1]))
    for (i,j),v in np.ndenumerate(field):
        theta = i * d_theta
        phi = j * d_phi
        field[i,j] = 0.5 * np.sqrt(3/np.pi) * np.cos(phi) + 0.25 * np.sqrt(105/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.cos(phi)*np.cos(-2*theta) + 0.046875 * np.sqrt(5005/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)*np.cos(6*theta)

    return field

def surface_integral2(samples, h_theta, h_phi):
    """
    Calculates surface integral of data

    Args:
        samples:    data
        h_theta:    theta spacing
        h_phi:      phi spacing
    Return:
        integral
    """

    integral = 0.0
  
    iphi = 0
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi]
     
    iphi = samples.shape[1] 
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta 
    integral = integral + np.sin(phi)*samples[itheta-1, iphi-1]

    iphi = 0
    itheta = samples.shape[0] 
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta-1, iphi]

    iphi = samples.shape[1] 
    itheta = 0
    phi = iphi*h_phi
    theta = itheta*h_theta
    integral = integral + np.sin(phi)*samples[itheta, iphi-1]
    
    for iphi in range(1, 80):
        phi = iphi*h_phi
        for itheta in range(1, 160):
            theta = itheta*h_theta 
            integral = integral + 4.0*np.sin(phi)*samples[itheta, iphi]


    iphi = 0
    phi = iphi*h_phi
    for itheta in range(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]

    iphi = samples.shape[1] 
    phi = iphi*h_phi
    for itheta in range(0, 160):
        theta = itheta*h_theta 
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi-1]

    itheta = 0
    theta = itheta*h_theta  
    for iphi in range(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta, iphi]       

    itheta = samples.shape[0] 
    theta = itheta*h_theta    
    for iphi in range(0, 80):
        phi = iphi*h_phi
        integral = integral + 2.0*np.sin(phi)*samples[itheta-1, iphi]           

    return 0.25*h_theta*h_phi*integral
      
def calculate(radius):
    """
    Calculates harmonics for a sphere

    Args:
        radius:    sphere
    """

    sphere = folderout + "/radius" + str(radius)

    if not os.path.exists(sphere):
        os.makedirs(sphere)

    f90 = open(sphere + '/s0_c00.dat', 'w')

    f100 = open(sphere + '/s0_c1m1.dat', 'w')
    f101 = open(sphere + '/s0_c10.dat', 'w')
    f102 = open(sphere + '/s0_c1p1.dat', 'w')

    f110 = open(sphere + '/s0_c2m2.dat', 'w')
    f111 = open(sphere + '/s0_c2m1.dat', 'w')
    f112 = open(sphere + '/s0_c20.dat', 'w')
    f113 = open(sphere + '/s0_c2p1.dat', 'w')
    f114 = open(sphere + '/s0_c2p2.dat', 'w')
 

    f120 = open(sphere + '/s0_c3m3.dat', 'w')
    f121 = open(sphere + '/s0_c3m2.dat', 'w')
    f122 = open(sphere + '/s0_c3m1.dat', 'w')
    f123 = open(sphere + '/s0_c30.dat', 'w')
    f124 = open(sphere + '/s0_c3p1.dat', 'w')
    f125 = open(sphere + '/s0_c3p2.dat', 'w')
    f126 = open(sphere + '/s0_c3p3.dat', 'w')

    f130 = open(sphere + '/s0_c4m4.dat', 'w')
    f131 = open(sphere + '/s0_c4m3.dat', 'w')
    f132 = open(sphere + '/s0_c4m2.dat', 'w')
    f133 = open(sphere + '/s0_c4m1.dat', 'w')
    f134 = open(sphere + '/s0_c40.dat', 'w')
    f135 = open(sphere + '/s0_c4p1.dat', 'w')
    f136 = open(sphere + '/s0_c4p2.dat', 'w')
    f137 = open(sphere + '/s0_c4p3.dat', 'w')
    f138 = open(sphere + '/s0_c4p4.dat', 'w')

    f140 = open(sphere + '/s0_c5m5.dat', 'w')
    f141 = open(sphere + '/s0_c5m4.dat', 'w')
    f142 = open(sphere + '/s0_c5m3.dat', 'w')
    f143 = open(sphere + '/s0_c5m2.dat', 'w')
    f144 = open(sphere + '/s0_c5m1.dat', 'w')
    f145 = open(sphere + '/s0_c50.dat', 'w')
    f146 = open(sphere + '/s0_c5p1.dat', 'w')
    f147 = open(sphere + '/s0_c5p2.dat', 'w')
    f148 = open(sphere + '/s0_c5p3.dat', 'w')
    f149 = open(sphere + '/s0_c5p4.dat', 'w')
    f150 = open(sphere + '/s0_c5p5.dat', 'w')

    f160 = open(sphere + '/s0_c6m6.dat', 'w')
    f161 = open(sphere + '/s0_c6m5.dat', 'w')
    f162 = open(sphere + '/s0_c6m4.dat', 'w')
    f163 = open(sphere + '/s0_c6m3.dat', 'w')
    f164 = open(sphere + '/s0_c6m2.dat', 'w')
    f165 = open(sphere + '/s0_c6m1.dat', 'w')
    f166 = open(sphere + '/s0_c60.dat', 'w')
    f167 = open(sphere + '/s0_c6p1.dat', 'w')
    f168 = open(sphere + '/s0_c6p2.dat', 'w')
    f169 = open(sphere + '/s0_c6p3.dat', 'w')
    f170 = open(sphere + '/s0_c6p4.dat', 'w')
    f171 = open(sphere + '/s0_c6p5.dat', 'w')
    f172 = open(sphere + '/s0_c6p6.dat', 'w')

    f180 = open(sphere + '/s0_c7m7.dat', 'w')
    f181 = open(sphere + '/s0_c7m6.dat', 'w')
    f182 = open(sphere + '/s0_c7m5.dat', 'w')
    f183 = open(sphere + '/s0_c7m4.dat', 'w')
    f184 = open(sphere + '/s0_c7m3.dat', 'w')
    f185 = open(sphere + '/s0_c7m2.dat', 'w')
    f186 = open(sphere + '/s0_c7m1.dat', 'w')
    f187 = open(sphere + '/s0_c70.dat', 'w')
    f188 = open(sphere + '/s0_c7p1.dat', 'w')
    f189 = open(sphere + '/s0_c7p2.dat', 'w')
    f190 = open(sphere + '/s0_c7p3.dat', 'w')
    f191 = open(sphere + '/s0_c7p4.dat', 'w')
    f192 = open(sphere + '/s0_c7p5.dat', 'w')
    f193 = open(sphere + '/s0_c7p6.dat', 'w')
    f194 = open(sphere + '/s0_c7p7.dat', 'w')

    f200 = open(sphere + '/s0_c8m8.dat', 'w')
    f201 = open(sphere + '/s0_c8m7.dat', 'w')
    f202 = open(sphere + '/s0_c8m6.dat', 'w')
    f203 = open(sphere + '/s0_c8m5.dat', 'w')
    f204 = open(sphere + '/s0_c8m4.dat', 'w')
    f205 = open(sphere + '/s0_c8m3.dat', 'w')
    f206 = open(sphere + '/s0_c8m2.dat', 'w')
    f207 = open(sphere + '/s0_c8m1.dat', 'w')
    f208 = open(sphere + '/s0_c80.dat', 'w')
    f209 = open(sphere + '/s0_c8p1.dat', 'w')
    f210 = open(sphere + '/s0_c8p2.dat', 'w')
    f211 = open(sphere + '/s0_c8p3.dat', 'w')
    f212 = open(sphere + '/s0_c8p4.dat', 'w')
    f213 = open(sphere + '/s0_c8p5.dat', 'w')
    f214 = open(sphere + '/s0_c8p6.dat', 'w')
    f215 = open(sphere + '/s0_c8p7.dat', 'w')
    f216 = open(sphere + '/s0_c8p8.dat', 'w')

    h_phi      = np.pi/res[1]
    h_theta      = 2*np.pi/res[0]

    #------computing the spin-weighted spherical harmonics Y'_l,m------------
    #--  Y_l,m = Y'_l,m * exp ( +i*m*phi) ---------------------------------
    #--  Y_l,m = Y'_l,m * [cos( +i*m*phi) + i*sin( +i*m*phi)] -------------
    #-- *Y_l,m = Y'_l,m * [cos( +i*m*phi) - i*sin( +i*m*phi)] -------------
    Y00 = np.zeros((2, res[0], res[1]))

    Y1m1 = np.zeros((2, res[0], res[1]))
    Y10 = np.zeros((2, res[0], res[1]))
    Y1p1 = np.zeros((2, res[0], res[1]))

    Y2m2 = np.zeros((2, res[0], res[1]))
    Y2m1 = np.zeros((2, res[0], res[1]))
    Y20 = np.zeros((2, res[0], res[1]))
    Y2p1 = np.zeros((2, res[0], res[1]))
    Y2p2 = np.zeros((2, res[0], res[1]))

    Y3m3 = np.zeros((2, res[0], res[1]))
    Y3m2 = np.zeros((2, res[0], res[1]))
    Y3m1 = np.zeros((2, res[0], res[1]))
    Y30 = np.zeros((2, res[0], res[1]))
    Y3p1 = np.zeros((2, res[0], res[1]))
    Y3p2 = np.zeros((2, res[0], res[1]))
    Y3p3 = np.zeros((2, res[0], res[1]))

    Y4m4 = np.zeros((2, res[0], res[1]))
    Y4m3 = np.zeros((2, res[0], res[1]))
    Y4m2 = np.zeros((2, res[0], res[1]))
    Y4m1 = np.zeros((2, res[0], res[1]))
    Y40 = np.zeros((2, res[0], res[1]))
    Y4p1 = np.zeros((2, res[0], res[1]))
    Y4p2 = np.zeros((2, res[0], res[1]))
    Y4p3 = np.zeros((2, res[0], res[1]))
    Y4p4 = np.zeros((2, res[0], res[1]))

    Y5m5 = np.zeros((2, res[0], res[1]))
    Y5m4 = np.zeros((2, res[0], res[1]))
    Y5m3 = np.zeros((2, res[0], res[1]))
    Y5m2 = np.zeros((2, res[0], res[1]))
    Y5m1 = np.zeros((2, res[0], res[1]))
    Y50 = np.zeros((2, res[0], res[1]))
    Y5p1 = np.zeros((2, res[0], res[1]))
    Y5p2 = np.zeros((2, res[0], res[1]))
    Y5p3 = np.zeros((2, res[0], res[1]))
    Y5p4 = np.zeros((2, res[0], res[1]))
    Y5p5 = np.zeros((2, res[0], res[1]))

    Y6m6 = np.zeros((2, res[0], res[1]))
    Y6m5 = np.zeros((2, res[0], res[1]))
    Y6m4 = np.zeros((2, res[0], res[1]))
    Y6m3 = np.zeros((2, res[0], res[1]))
    Y6m2 = np.zeros((2, res[0], res[1]))
    Y6m1 = np.zeros((2, res[0], res[1]))
    Y60 = np.zeros((2, res[0], res[1]))
    Y6p1 = np.zeros((2, res[0], res[1]))
    Y6p2 = np.zeros((2, res[0], res[1]))
    Y6p3 = np.zeros((2, res[0], res[1]))
    Y6p4 = np.zeros((2, res[0], res[1]))
    Y6p5 = np.zeros((2, res[0], res[1]))
    Y6p6 = np.zeros((2, res[0], res[1]))

    Y7m7 = np.zeros((2, res[0], res[1]))
    Y7m6 = np.zeros((2, res[0], res[1]))
    Y7m5 = np.zeros((2, res[0], res[1]))
    Y7m4 = np.zeros((2, res[0], res[1]))
    Y7m3 = np.zeros((2, res[0], res[1]))
    Y7m2 = np.zeros((2, res[0], res[1]))
    Y7m1 = np.zeros((2, res[0], res[1]))
    Y70 = np.zeros((2, res[0], res[1]))
    Y7p1 = np.zeros((2, res[0], res[1]))
    Y7p2 = np.zeros((2, res[0], res[1]))
    Y7p3 = np.zeros((2, res[0], res[1]))
    Y7p4 = np.zeros((2, res[0], res[1]))
    Y7p5 = np.zeros((2, res[0], res[1]))
    Y7p6 = np.zeros((2, res[0], res[1]))
    Y7p7 = np.zeros((2, res[0], res[1]))

    Y8m8 = np.zeros((2, res[0], res[1]))
    Y8m7 = np.zeros((2, res[0], res[1]))
    Y8m6 = np.zeros((2, res[0], res[1]))
    Y8m5 = np.zeros((2, res[0], res[1]))
    Y8m4 = np.zeros((2, res[0], res[1]))
    Y8m3 = np.zeros((2, res[0], res[1]))
    Y8m2 = np.zeros((2, res[0], res[1]))
    Y8m1 = np.zeros((2, res[0], res[1]))
    Y80 = np.zeros((2, res[0], res[1]))
    Y8p1 = np.zeros((2, res[0], res[1]))
    Y8p2 = np.zeros((2, res[0], res[1]))
    Y8p3 = np.zeros((2, res[0], res[1]))
    Y8p4 = np.zeros((2, res[0], res[1]))
    Y8p5 = np.zeros((2, res[0], res[1]))
    Y8p6 = np.zeros((2, res[0], res[1]))
    Y8p7 = np.zeros((2, res[0], res[1]))
    Y8p8 = np.zeros((2, res[0], res[1]))

    ty00 = 0.5 * np.sqrt(1/np.pi)


    for iphi in range(res[1]):
        phi = iphi*h_phi
        for itheta in range(res[0]):
            theta = itheta*h_theta 

            Y00[0, itheta, iphi] = ty00 * np.cos(0.0*theta)
            Y00[1, itheta, iphi] = -ty00 * np.sin(0.0*theta)

            ty1m1 = 0.5 * np.sqrt(3/(2*np.pi)) * np.sin(phi)
            ty10 = 0.5 * np.sqrt(3/np.pi) * np.cos(phi)
            ty1p1 = -ty1m1
            Y1m1[0, itheta, iphi] = ty1m1*np.cos(-1.0*theta)
            Y10[0, itheta, iphi]  = ty10*np.cos(0.0*theta)
            Y1p1[0, itheta, iphi] = ty1p1*np.cos(1.0*theta)
            Y1m1[1, itheta, iphi] =-ty1m1*np.sin(-1.0*theta)
            Y10[1, itheta, iphi]  =-ty10*np.sin(0.0*theta)
            Y1p1[1, itheta, iphi] =-ty1p1*np.sin(1.0*theta)
      
            ty2m2 = 0.25 * np.sqrt(15/(2*np.pi)) * np.sin(phi) * np.sin(phi)
            ty2m1 = 0.5 * np.sqrt(15/(2*np.pi)) * np.sin(phi) * np.cos(phi)
            ty20  = 0.25 * np.sqrt(5/np.pi) * (3 * np.cos(phi) * np.cos(phi) - 1)
            ty2p1 = -ty2m1
            ty2p2 = ty2m2
            Y2m2[0, itheta, iphi] = ty2m2 * np.cos(-2*theta)
            Y2m1[0, itheta, iphi] = ty2m1 * np.cos(-1*theta)
            Y20[0, itheta, iphi]  = ty20 * np.cos(0*theta)
            Y2p1[0, itheta, iphi] = ty2p1 * np.cos(1*theta)
            Y2p2[0, itheta, iphi] = ty2p2 * np.cos(2*theta)
            Y2m2[1, itheta, iphi] =-ty2m2 * np.sin(-2*theta)
            Y2m1[1, itheta, iphi] =-ty2m1 * np.sin(-1*theta)
            Y20[1, itheta, iphi]  =-ty20 * np.sin(0*theta)
            Y2p1[1, itheta, iphi] =-ty2p1 * np.sin(1*theta)
            Y2p2[1, itheta, iphi] =-ty2p2 * np.sin(2*theta)

            ty3m3 = 0.125 * np.sqrt(35/np.pi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty3m2 = 0.25 * np.sqrt(105/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty3m1 = 0.125 * np.sqrt(21/np.pi) * np.sin(phi) * (5*np.cos(phi) * np.cos(phi) - 1)
            ty30 = 0.25 * np.sqrt(7/np.pi) * (5*np.cos(phi) * np.cos(phi) * np.cos(phi) - 3 * np.cos(phi))
            ty3p1 = -ty3m1
            ty3p2 = ty3m2
            ty3p3 = -ty3m3
            Y3m3[0, itheta, iphi] = ty3m3 * np.cos(-3*theta)
            Y3m2[0, itheta, iphi] = ty3m2 * np.cos(-2*theta)
            Y3m1[0, itheta, iphi] = ty3m1 * np.cos(-1*theta)
            Y30[0, itheta, iphi]  = ty30 * np.cos(0*theta)
            Y3p1[0, itheta, iphi] = ty3p1 * np.cos(1*theta)
            Y3p2[0, itheta, iphi] = ty3p2 * np.cos(2*theta)
            Y3p3[0, itheta, iphi] = ty3p3 * np.cos(3*theta)
            Y3m3[1, itheta, iphi] =-ty3m3 * np.sin(-3*theta)
            Y3m2[1, itheta, iphi] =-ty3m2 * np.sin(-2*theta)
            Y3m1[1, itheta, iphi] =-ty3m1 * np.sin(-1*theta)
            Y30[1, itheta, iphi]  =-ty30 * np.sin(0*theta)
            Y3p1[1, itheta, iphi] =-ty3p1 * np.sin(1*theta)
            Y3p2[1, itheta, iphi] =-ty3p2 * np.sin(2*theta)
            Y3p3[1, itheta, iphi] =-ty3p3 * np.sin(3*theta)

            ty4m4 = 0.1875 * np.sqrt(35/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty4m3 = 0.375 * np.sqrt(35/np.pi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty4m2 = 0.375 * np.sqrt(5/(2*np.pi)) * np.sin(phi) * np.sin(phi) * (7*np.cos(phi) * np.cos(phi) - 1)
            ty4m1 = 0.375 * np.sqrt(5/np.pi) * np.sin(phi) * (7*np.cos(phi) * np.cos(phi) * np.cos(phi) - 3 * np.cos(phi))
            ty40 = 0.1875 * np.sqrt(1/np.pi) * (35 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 30 * np.cos(phi) * np.cos(phi) + 3)
            ty4p1 = -ty4m1
            ty4p2 = ty4m2
            ty4p3 = -ty4m3
            ty4p4 = ty4m4
            Y4m4[0, itheta, iphi] = ty4m4 * np.cos(-4*theta)
            Y4m3[0, itheta, iphi] = ty4m3 * np.cos(-3*theta)
            Y4m2[0, itheta, iphi] = ty4m2 * np.cos(-2*theta)
            Y4m1[0, itheta, iphi] = ty4m1 * np.cos(-1*theta)
            Y40[0, itheta, iphi]  = ty40 * np.cos(0*theta)
            Y4p1[0, itheta, iphi] = ty4p1 * np.cos(1*theta)
            Y4p2[0, itheta, iphi] = ty4p2 * np.cos(2*theta)
            Y4p3[0, itheta, iphi] = ty4p3 * np.cos(3*theta)
            Y4p4[0, itheta, iphi] = ty4p4 * np.cos(4*theta)
            Y4m4[1, itheta, iphi] =-ty4m4 * np.sin(-4*theta)
            Y4m3[1, itheta, iphi] =-ty4m3 * np.sin(-3*theta)
            Y4m2[1, itheta, iphi] =-ty4m2 * np.sin(-2*theta)
            Y4m1[1, itheta, iphi] =-ty4m1 * np.sin(-1*theta)
            Y40[1, itheta, iphi]  =-ty40 * np.sin(0*theta)
            Y4p1[1, itheta, iphi] =-ty4p1 * np.sin(1*theta)
            Y4p2[1, itheta, iphi] =-ty4p2 * np.sin(2*theta)
            Y4p3[1, itheta, iphi] =-ty4p3 * np.sin(3*theta)
            Y4p4[1, itheta, iphi] =-ty4p4 * np.sin(4*theta)

            ty5m5 = 0.09375 * np.sqrt(77/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty5m4 = 0.1875 * np.sqrt(385/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty5m3 = 0.03125 * np.sqrt(385/np.pi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (9 * np.cos(phi) * np.cos(phi) - 1)
            ty5m2 = 0.125 * np.sqrt(1155/(2*np.pi)) * np.sin(phi) * np.sin(phi) * (3*np.cos(phi) * np.cos(phi) * np.cos(phi) - np.cos(phi))
            ty5m1 = 0.0625 * np.sqrt(165/(2*np.pi)) * np.sin(phi) * (21*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 14 * np.cos(phi) * np.cos(phi) + 1)
            ty50 = 0.0625 * np.sqrt(11/np.pi) * (63 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 70 * np.cos(phi) * np.cos(phi) * np.cos(phi) + 15 * np.cos(phi))
            ty5p1 = -ty5m1
            ty5p2 = ty5m2
            ty5p3 = -ty5m3
            ty5p4 = ty5m4
            ty5p5 = -ty5m5
            Y5m5[0, itheta, iphi] = ty5m5 * np.cos(-5*theta)
            Y5m4[0, itheta, iphi] = ty5m4 * np.cos(-4*theta)
            Y5m3[0, itheta, iphi] = ty5m3 * np.cos(-3*theta)
            Y5m2[0, itheta, iphi] = ty5m2 * np.cos(-2*theta)
            Y5m1[0, itheta, iphi] = ty5m1 * np.cos(-1*theta)
            Y50[0, itheta, iphi]  = ty50 * np.cos(0*theta)
            Y5p1[0, itheta, iphi] = ty5p1 * np.cos(1*theta)
            Y5p2[0, itheta, iphi] = ty5p2 * np.cos(2*theta)
            Y5p3[0, itheta, iphi] = ty5p3 * np.cos(3*theta)
            Y5p4[0, itheta, iphi] = ty5p4 * np.cos(4*theta)
            Y5p5[0, itheta, iphi] = ty5p5 * np.cos(5*theta)
            Y5m5[1, itheta, iphi] =-ty5m5 * np.sin(-5*theta)
            Y5m4[1, itheta, iphi] =-ty5m4 * np.sin(-4*theta)
            Y5m3[1, itheta, iphi] =-ty5m3 * np.sin(-3*theta)
            Y5m2[1, itheta, iphi] =-ty5m2 * np.sin(-2*theta)
            Y5m1[1, itheta, iphi] =-ty5m1 * np.sin(-1*theta)
            Y50[1, itheta, iphi]  =-ty50 * np.sin(0*theta)
            Y5p1[1, itheta, iphi] =-ty5p1 * np.sin(1*theta)
            Y5p2[1, itheta, iphi] =-ty5p2 * np.sin(2*theta)
            Y5p3[1, itheta, iphi] =-ty5p3 * np.sin(3*theta)
            Y5p4[1, itheta, iphi] =-ty5p4 * np.sin(4*theta)
            Y5p5[1, itheta, iphi] =-ty5p5 * np.sin(5*theta)

            ty6m6 = 0.0156 * np.sqrt(3003/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty6m5 = 0.09375 * np.sqrt(1001/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty6m4 = 0.09375 * np.sqrt(91/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (11 * np.cos(phi) * np.cos(phi) - 1)
            ty6m3 = 0.03125 * np.sqrt(1365/np.pi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (11 * np.cos(phi) * np.cos(phi) * np.cos(phi) - 3 * np.cos(phi))
            ty6m2 = 0.015625 * np.sqrt(1365/(np.pi)) * np.sin(phi) * np.sin(phi) * (33*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 18 * np.cos(phi) * np.cos(phi) + 1)
            ty6m1 = 0.0625 * np.sqrt(273/(2*np.pi)) * np.sin(phi) * (33*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 30 * np.cos(phi) * np.cos(phi) * np.cos(phi) + 5 * np.cos(phi))
            ty60 = 0.03125 * np.sqrt(13/(2*np.pi)) * (231 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 315 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 105 * np.cos(phi) * np.cos(phi) - 5)
            ty6p1 = -ty6m1
            ty6p2 = ty6m2
            ty6p3 = -ty6m3
            ty6p4 = ty6m4
            ty6p5 = -ty6m5
            ty6p6 = ty6m6
            Y6m6[0, itheta, iphi] = ty6m6 * np.cos(-6*theta)
            Y6m5[0, itheta, iphi] = ty6m5 * np.cos(-5*theta)
            Y6m4[0, itheta, iphi] = ty6m4 * np.cos(-4*theta)
            Y6m3[0, itheta, iphi] = ty6m3 * np.cos(-3*theta)
            Y6m2[0, itheta, iphi] = ty6m2 * np.cos(-2*theta)
            Y6m1[0, itheta, iphi] = ty6m1 * np.cos(-1*theta)
            Y60[0, itheta, iphi]  = ty60 * np.cos(0*theta)
            Y6p1[0, itheta, iphi] = ty6p1 * np.cos(1*theta)
            Y6p2[0, itheta, iphi] = ty6p2 * np.cos(2*theta)
            Y6p3[0, itheta, iphi] = ty6p3 * np.cos(3*theta)
            Y6p4[0, itheta, iphi] = ty6p4 * np.cos(4*theta)
            Y6p5[0, itheta, iphi] = ty6p5 * np.cos(5*theta)
            Y6p6[0, itheta, iphi] = ty6p6 * np.cos(6*theta)
            Y6m6[1, itheta, iphi] =-ty6m6 * np.sin(-6*theta)
            Y6m5[1, itheta, iphi] =-ty6m5 * np.sin(-5*theta)
            Y6m4[1, itheta, iphi] =-ty6m4 * np.sin(-4*theta)
            Y6m3[1, itheta, iphi] =-ty6m3 * np.sin(-3*theta)
            Y6m2[1, itheta, iphi] =-ty6m2 * np.sin(-2*theta)
            Y6m1[1, itheta, iphi] =-ty6m1 * np.sin(-1*theta)
            Y60[1, itheta, iphi]  =-ty60 * np.sin(0*theta)
            Y6p1[1, itheta, iphi] =-ty6p1 * np.sin(1*theta)
            Y6p2[1, itheta, iphi] =-ty6p2 * np.sin(2*theta)
            Y6p3[1, itheta, iphi] =-ty6p3 * np.sin(3*theta)
            Y6p4[1, itheta, iphi] =-ty6p4 * np.sin(4*theta)
            Y6p5[1, itheta, iphi] =-ty6p5 * np.sin(5*theta)
            Y6p6[1, itheta, iphi] =-ty6p6 * np.sin(6*theta)

            ty7m7 = 0.046875 * np.sqrt(715/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty7m6 = 0.046875 * np.sqrt(5005/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty7m5 = 0.046875 * np.sqrt(385/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (13 * np.cos(phi) * np.cos(phi) - 1)
            ty7m4 = 0.09375 * np.sqrt(385/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (13 * np.cos(phi) * np.cos(phi) * np.cos(phi) - 3 * np.cos(phi))
            ty7m3 = 0.046875 * np.sqrt(35/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (143 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 66 * np.cos(phi) * np.cos(phi) + 3)
            ty7m2 = 0.046875 * np.sqrt(35/(np.pi)) * np.sin(phi) * np.sin(phi) * (143*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 110 * np.cos(phi) * np.cos(phi) * np.cos(phi) + 15 * np.cos(phi))
            ty7m1 = 0.015625 * np.sqrt(105/(2*np.pi)) * np.sin(phi) * (429*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 495 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 135 * np.cos(phi) * np.cos(phi) - 5)
            ty70 = 0.03125 * np.sqrt(15/(np.pi)) * (429 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 693 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 315 * np.cos(phi) * np.cos(phi) * np.cos(phi) - 35 * np.cos(phi))
            ty7p1 = -ty7m1
            ty7p2 = ty7m2
            ty7p3 = -ty7m3
            ty7p4 = ty7m4
            ty7p5 = -ty7m5
            ty7p6 = ty7m6
            ty7p7 = -ty7m7
            Y7m7[0, itheta, iphi] = ty7m7 * np.cos(-7*theta)
            Y7m6[0, itheta, iphi] = ty7m6 * np.cos(-6*theta)
            Y7m5[0, itheta, iphi] = ty7m5 * np.cos(-5*theta)
            Y7m4[0, itheta, iphi] = ty7m4 * np.cos(-4*theta)
            Y7m3[0, itheta, iphi] = ty7m3 * np.cos(-3*theta)
            Y7m2[0, itheta, iphi] = ty7m2 * np.cos(-2*theta)
            Y7m1[0, itheta, iphi] = ty7m1 * np.cos(-1*theta)
            Y70[0, itheta, iphi]  = ty70 * np.cos(0*theta)
            Y7p1[0, itheta, iphi] = ty7p1 * np.cos(1*theta)
            Y7p2[0, itheta, iphi] = ty7p2 * np.cos(2*theta)
            Y7p3[0, itheta, iphi] = ty7p3 * np.cos(3*theta)
            Y7p4[0, itheta, iphi] = ty7p4 * np.cos(4*theta)
            Y7p5[0, itheta, iphi] = ty7p5 * np.cos(5*theta)
            Y7p6[0, itheta, iphi] = ty7p6 * np.cos(6*theta)
            Y7p7[0, itheta, iphi] = ty7p7 * np.cos(7*theta)
            Y7m7[1, itheta, iphi] =-ty7m7 * np.sin(-7*theta)
            Y7m6[1, itheta, iphi] =-ty7m6 * np.sin(-6*theta)
            Y7m5[1, itheta, iphi] =-ty7m5 * np.sin(-5*theta)
            Y7m4[1, itheta, iphi] =-ty7m4 * np.sin(-4*theta)
            Y7m3[1, itheta, iphi] =-ty7m3 * np.sin(-3*theta)
            Y7m2[1, itheta, iphi] =-ty7m2 * np.sin(-2*theta)
            Y7m1[1, itheta, iphi] =-ty7m1 * np.sin(-1*theta)
            Y70[1, itheta, iphi]  =-ty70 * np.sin(0*theta)
            Y7p1[1, itheta, iphi] =-ty7p1 * np.sin(1*theta)
            Y7p2[1, itheta, iphi] =-ty7p2 * np.sin(2*theta)
            Y7p3[1, itheta, iphi] =-ty7p3 * np.sin(3*theta)
            Y7p4[1, itheta, iphi] =-ty7p4 * np.sin(4*theta)
            Y7p5[1, itheta, iphi] =-ty7p5 * np.sin(5*theta)
            Y7p6[1, itheta, iphi] =-ty7p6 * np.sin(6*theta)
            Y7p7[1, itheta, iphi] =-ty7p7 * np.sin(7*theta)

            ty8m8 = 0.01171875 * np.sqrt(12155/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi)
            ty8m7 = 0.046875 * np.sqrt(12155/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.cos(phi)
            ty8m6 = 0.0078125 * np.sqrt(7293/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (15 * np.cos(phi) * np.cos(phi) - 1)
            ty8m5 = 0.046875 * np.sqrt(17017/(np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (5 * np.cos(phi) * np.cos(phi) * np.cos(phi) - np.cos(phi))
            ty8m4 = 0.0234375 * np.sqrt(1309/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (65 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 26 * np.cos(phi) * np.cos(phi) + 1)
            ty8m3 = 0.015625 * np.sqrt(19635/(2*np.pi)) * np.sin(phi) * np.sin(phi) * np.sin(phi) * (39 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 26 * np.cos(phi) * np.cos(phi) * np.cos(phi) + 3 * np.cos(phi))
            ty8m2 = 0.0234375 * np.sqrt(595/(np.pi)) * np.sin(phi) * np.sin(phi) * (143*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 143 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 33 * np.cos(phi) * np.cos(phi) - 1)
            ty8m1 = 0.046875 * np.sqrt(17/(2*np.pi)) * np.sin(phi) * (715*np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 1001 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 385 * np.cos(phi) * np.cos(phi) * np.cos(phi) - 35 * np.cos(phi))
            ty80 = 0.00390625 * np.sqrt(17/(np.pi)) * (6435 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 12012 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) + 6930 * np.cos(phi) * np.cos(phi) * np.cos(phi) * np.cos(phi) - 1260 * np.cos(phi) * np.cos(phi) + 35)
            ty8p1 = -ty8m1
            ty8p2 = ty8m2
            ty8p3 = -ty8m3
            ty8p4 = ty8m4
            ty8p5 = -ty8m5
            ty8p6 = ty8m6
            ty8p7 = -ty8m7
            ty8p8 = ty8m8
            Y8m8[0, itheta, iphi] = ty8m8 * np.cos(-8*theta)
            Y8m7[0, itheta, iphi] = ty8m7 * np.cos(-7*theta)
            Y8m6[0, itheta, iphi] = ty8m6 * np.cos(-6*theta)
            Y8m5[0, itheta, iphi] = ty8m5 * np.cos(-5*theta)
            Y8m4[0, itheta, iphi] = ty8m4 * np.cos(-4*theta)
            Y8m3[0, itheta, iphi] = ty8m3 * np.cos(-3*theta)
            Y8m2[0, itheta, iphi] = ty8m2 * np.cos(-2*theta)
            Y8m1[0, itheta, iphi] = ty8m1 * np.cos(-1*theta)
            Y80[0, itheta, iphi]  = ty80 * np.cos(0*theta)
            Y8p1[0, itheta, iphi] = ty8p1 * np.cos(1*theta)
            Y8p2[0, itheta, iphi] = ty8p2 * np.cos(2*theta)
            Y8p3[0, itheta, iphi] = ty8p3 * np.cos(3*theta)
            Y8p4[0, itheta, iphi] = ty8p4 * np.cos(4*theta)
            Y8p5[0, itheta, iphi] = ty8p5 * np.cos(5*theta)
            Y8p6[0, itheta, iphi] = ty8p6 * np.cos(6*theta)
            Y8p7[0, itheta, iphi] = ty8p7 * np.cos(7*theta)
            Y8p8[0, itheta, iphi] = ty8p8 * np.cos(8*theta)
            Y8m8[1, itheta, iphi] =-ty8m8 * np.sin(-8*theta)
            Y8m7[1, itheta, iphi] =-ty8m7 * np.sin(-7*theta)
            Y8m6[1, itheta, iphi] =-ty8m6 * np.sin(-6*theta)
            Y8m5[1, itheta, iphi] =-ty8m5 * np.sin(-5*theta)
            Y8m4[1, itheta, iphi] =-ty8m4 * np.sin(-4*theta)
            Y8m3[1, itheta, iphi] =-ty8m3 * np.sin(-3*theta)
            Y8m2[1, itheta, iphi] =-ty8m2 * np.sin(-2*theta)
            Y8m1[1, itheta, iphi] =-ty8m1 * np.sin(-1*theta)
            Y80[1, itheta, iphi]  =-ty80 * np.sin(0*theta)
            Y8p1[1, itheta, iphi] =-ty8p1 * np.sin(1*theta)
            Y8p2[1, itheta, iphi] =-ty8p2 * np.sin(2*theta)
            Y8p3[1, itheta, iphi] =-ty8p3 * np.sin(3*theta)
            Y8p4[1, itheta, iphi] =-ty8p4 * np.sin(4*theta)
            Y8p5[1, itheta, iphi] =-ty8p5 * np.sin(5*theta)
            Y8p6[1, itheta, iphi] =-ty8p6 * np.sin(6*theta)
            Y8p7[1, itheta, iphi] =-ty8p7 * np.sin(7*theta)
            Y8p8[1, itheta, iphi] =-ty8p8 * np.sin(8*theta)

    for f in folders:
        for i in folders[f]:
            print(f, i, radius)
            time = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5) + "/" + folderin + "/sphere"+ str(radius)

            print("time=", time)
            field = readfield(foldertime, 'Bfu_r')
            #field = artifical_function()

            temp1 = field*Y00[0,:]
            temp2 = field*Y00[1,:]
            c00_r = surface_integral2(temp1, h_theta, h_phi)
            c00_i = surface_integral2(temp2, h_theta, h_phi)


            temp1 = field*Y1m1[0,:]
            temp2 = field*Y1m1[1,:]
            c1m1_r = surface_integral2(temp1, h_theta, h_phi)
            c1m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y10[0,:]
            temp2 = field*Y10[1,:]
            c10_r = surface_integral2(temp1, h_theta, h_phi)
            c10_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y1p1[0,:]
            temp2 = field*Y1p1[1,:]
            c1p1_r = surface_integral2(temp1, h_theta, h_phi)
            c1p1_i = surface_integral2(temp2, h_theta, h_phi)


            temp1 = field*Y2m2[0,:]
            temp2 = field*Y2m2[1,:]
            c2m2_r = surface_integral2(temp1, h_theta, h_phi)
            c2m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y2m1[0,:]
            temp2 = field*Y2m1[1,:]
            c2m1_r = surface_integral2(temp1, h_theta, h_phi)
            c2m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y20[0,:]
            temp2 = field*Y20[1,:]
            c20_r = surface_integral2(temp1, h_theta, h_phi)
            c20_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y2p1[0,:]
            temp2 = field*Y2p1[1,:]
            c2p1_r = surface_integral2(temp1, h_theta, h_phi)
            c2p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y2p2[0,:]
            temp2 = field*Y2p2[1,:]
            c2p2_r = surface_integral2(temp1, h_theta, h_phi)
            c2p2_i = surface_integral2(temp2, h_theta, h_phi)


            temp1 = field*Y3m3[0,:]
            temp2 = field*Y3m3[1,:]
            c3m3_r = surface_integral2(temp1, h_theta, h_phi)
            c3m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y3m2[0,:]
            temp2 = field*Y3m2[1,:]
            c3m2_r = surface_integral2(temp1, h_theta, h_phi)
            c3m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y3m1[0,:]
            temp2 = field*Y3m1[1,:]
            c3m1_r = surface_integral2(temp1, h_theta, h_phi)
            c3m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y30[0,:]
            temp2 = field*Y30[1,:]
            c30_r = surface_integral2(temp1, h_theta, h_phi)
            c30_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y3p1[0,:]
            temp2 = field*Y3p1[1,:]
            c3p1_r = surface_integral2(temp1, h_theta, h_phi)
            c3p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y3p2[0,:]
            temp2 = field*Y3p2[1,:]
            c3p2_r = surface_integral2(temp1, h_theta, h_phi)
            c3p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y3p3[0,:]
            temp2 = field*Y3p3[1,:]
            c3p3_r = surface_integral2(temp1, h_theta, h_phi)
            c3p3_i = surface_integral2(temp2, h_theta, h_phi)


            temp1 = field*Y4m4[0,:]
            temp2 = field*Y4m4[1,:]
            c4m4_r = surface_integral2(temp1, h_theta, h_phi)
            c4m4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4m3[0,:]
            temp2 = field*Y4m3[1,:]
            c4m3_r = surface_integral2(temp1, h_theta, h_phi)
            c4m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4m2[0,:]
            temp2 = field*Y4m2[1,:]
            c4m2_r = surface_integral2(temp1, h_theta, h_phi)
            c4m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4m1[0,:]
            temp2 = field*Y4m1[1,:]
            c4m1_r = surface_integral2(temp1, h_theta, h_phi)
            c4m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y40[0,:]
            temp2 = field*Y40[1,:]
            c40_r = surface_integral2(temp1, h_theta, h_phi)
            c40_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4p1[0,:]
            temp2 = field*Y4p1[1,:]
            c4p1_r = surface_integral2(temp1, h_theta, h_phi)
            c4p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4p2[0,:]
            temp2 = field*Y4p2[1,:]
            c4p2_r = surface_integral2(temp1, h_theta, h_phi)
            c4p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4p3[0,:]
            temp2 = field*Y4p3[1,:]
            c4p3_r = surface_integral2(temp1, h_theta, h_phi)
            c4p3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y4p4[0,:]
            temp2 = field*Y4p4[1,:]
            c4p4_r = surface_integral2(temp1, h_theta, h_phi)
            c4p4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5m5[0,:]
            temp2 = field*Y5m5[1,:]
            c5m5_r = surface_integral2(temp1, h_theta, h_phi)
            c5m5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5m4[0,:]
            temp2 = field*Y5m4[1,:]
            c5m4_r = surface_integral2(temp1, h_theta, h_phi)
            c5m4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5m3[0,:]
            temp2 = field*Y5m3[1,:]
            c5m3_r = surface_integral2(temp1, h_theta, h_phi)
            c5m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5m2[0,:]
            temp2 = field*Y5m2[1,:]
            c5m2_r = surface_integral2(temp1, h_theta, h_phi)
            c5m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5m1[0,:]
            temp2 = field*Y5m1[1,:]
            c5m1_r = surface_integral2(temp1, h_theta, h_phi)
            c5m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y50[0,:]
            temp2 = field*Y50[1,:]
            c50_r = surface_integral2(temp1, h_theta, h_phi)
            c50_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5p1[0,:]
            temp2 = field*Y5p1[1,:]
            c5p1_r = surface_integral2(temp1, h_theta, h_phi)
            c5p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5p2[0,:]
            temp2 = field*Y5p2[1,:]
            c5p2_r = surface_integral2(temp1, h_theta, h_phi)
            c5p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5p3[0,:]
            temp2 = field*Y5p3[1,:]
            c5p3_r = surface_integral2(temp1, h_theta, h_phi)
            c5p3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5p4[0,:]
            temp2 = field*Y5p4[1,:]
            c5p4_r = surface_integral2(temp1, h_theta, h_phi)
            c5p4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y5p5[0,:]
            temp2 = field*Y5p5[1,:]
            c5p5_r = surface_integral2(temp1, h_theta, h_phi)
            c5p5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m6[0,:]
            temp2 = field*Y6m6[1,:]
            c6m6_r = surface_integral2(temp1, h_theta, h_phi)
            c6m6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m5[0,:]
            temp2 = field*Y6m5[1,:]
            c6m5_r = surface_integral2(temp1, h_theta, h_phi)
            c6m5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m4[0,:]
            temp2 = field*Y6m4[1,:]
            c6m4_r = surface_integral2(temp1, h_theta, h_phi)
            c6m4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m3[0,:]
            temp2 = field*Y6m3[1,:]
            c6m3_r = surface_integral2(temp1, h_theta, h_phi)
            c6m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m2[0,:]
            temp2 = field*Y6m2[1,:]
            c6m2_r = surface_integral2(temp1, h_theta, h_phi)
            c6m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6m1[0,:]
            temp2 = field*Y6m1[1,:]
            c6m1_r = surface_integral2(temp1, h_theta, h_phi)
            c6m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y60[0,:]
            temp2 = field*Y60[1,:]
            c60_r = surface_integral2(temp1, h_theta, h_phi)
            c60_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p1[0,:]
            temp2 = field*Y6p1[1,:]
            c6p1_r = surface_integral2(temp1, h_theta, h_phi)
            c6p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p2[0,:]
            temp2 = field*Y6p2[1,:]
            c6p2_r = surface_integral2(temp1, h_theta, h_phi)
            c6p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p3[0,:]
            temp2 = field*Y6p3[1,:]
            c6p3_r = surface_integral2(temp1, h_theta, h_phi)
            c6p3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p4[0,:]
            temp2 = field*Y6p4[1,:]
            c6p4_r = surface_integral2(temp1, h_theta, h_phi)
            c6p4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p5[0,:]
            temp2 = field*Y6p5[1,:]
            c6p5_r = surface_integral2(temp1, h_theta, h_phi)
            c6p5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y6p6[0,:]
            temp2 = field*Y6p6[1,:]
            c6p6_r = surface_integral2(temp1, h_theta, h_phi)
            c6p6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m7[0,:]
            temp2 = field*Y7m7[1,:]
            c7m7_r = surface_integral2(temp1, h_theta, h_phi)
            c7m7_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m6[0,:]
            temp2 = field*Y7m6[1,:]
            c7m6_r = surface_integral2(temp1, h_theta, h_phi)
            c7m6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m5[0,:]
            temp2 = field*Y7m5[1,:]
            c7m5_r = surface_integral2(temp1, h_theta, h_phi)
            c7m5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m4[0,:]
            temp2 = field*Y7m4[1,:]
            c7m4_r = surface_integral2(temp1, h_theta, h_phi)
            c7m4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m3[0,:]
            temp2 = field*Y7m3[1,:]
            c7m3_r = surface_integral2(temp1, h_theta, h_phi)
            c7m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m2[0,:]
            temp2 = field*Y7m2[1,:]
            c7m2_r = surface_integral2(temp1, h_theta, h_phi)
            c7m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7m1[0,:]
            temp2 = field*Y7m1[1,:]
            c7m1_r = surface_integral2(temp1, h_theta, h_phi)
            c7m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y70[0,:]
            temp2 = field*Y70[1,:]
            c70_r = surface_integral2(temp1, h_theta, h_phi)
            c70_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p1[0,:]
            temp2 = field*Y7p1[1,:]
            c7p1_r = surface_integral2(temp1, h_theta, h_phi)
            c7p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p2[0,:]
            temp2 = field*Y7p2[1,:]
            c7p2_r = surface_integral2(temp1, h_theta, h_phi)
            c7p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p3[0,:]
            temp2 = field*Y7p3[1,:]
            c7p3_r = surface_integral2(temp1, h_theta, h_phi)
            c7p3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p4[0,:]
            temp2 = field*Y7p4[1,:]
            c7p4_r = surface_integral2(temp1, h_theta, h_phi)
            c7p4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p5[0,:]
            temp2 = field*Y7p5[1,:]
            c7p5_r = surface_integral2(temp1, h_theta, h_phi)
            c7p5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p6[0,:]
            temp2 = field*Y7p6[1,:]
            c7p6_r = surface_integral2(temp1, h_theta, h_phi)
            c7p6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y7p7[0,:]
            temp2 = field*Y7p7[1,:]
            c7p7_r = surface_integral2(temp1, h_theta, h_phi)
            c7p7_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m8[0,:]
            temp2 = field*Y8m8[1,:]
            c8m8_r = surface_integral2(temp1, h_theta, h_phi)
            c8m8_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m7[0,:]
            temp2 = field*Y8m7[1,:]
            c8m7_r = surface_integral2(temp1, h_theta, h_phi)
            c8m7_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m6[0,:]
            temp2 = field*Y8m6[1,:]
            c8m6_r = surface_integral2(temp1, h_theta, h_phi)
            c8m6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m5[0,:]
            temp2 = field*Y8m5[1,:]
            c8m5_r = surface_integral2(temp1, h_theta, h_phi)
            c8m5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m4[0,:]
            temp2 = field*Y8m4[1,:]
            c8m4_r = surface_integral2(temp1, h_theta, h_phi)
            c8m4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m3[0,:]
            temp2 = field*Y8m3[1,:]
            c8m3_r = surface_integral2(temp1, h_theta, h_phi)
            c8m3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m2[0,:]
            temp2 = field*Y8m2[1,:]
            c8m2_r = surface_integral2(temp1, h_theta, h_phi)
            c8m2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8m1[0,:]
            temp2 = field*Y8m1[1,:]
            c8m1_r = surface_integral2(temp1, h_theta, h_phi)
            c8m1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y80[0,:]
            temp2 = field*Y80[1,:]
            c80_r = surface_integral2(temp1, h_theta, h_phi)
            c80_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p1[0,:]
            temp2 = field*Y8p1[1,:]
            c8p1_r = surface_integral2(temp1, h_theta, h_phi)
            c8p1_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p2[0,:]
            temp2 = field*Y8p2[1,:]
            c8p2_r = surface_integral2(temp1, h_theta, h_phi)
            c8p2_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p3[0,:]
            temp2 = field*Y8p3[1,:]
            c8p3_r = surface_integral2(temp1, h_theta, h_phi)
            c8p3_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p4[0,:]
            temp2 = field*Y8p4[1,:]
            c8p4_r = surface_integral2(temp1, h_theta, h_phi)
            c8p4_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p5[0,:]
            temp2 = field*Y8p5[1,:]
            c8p5_r = surface_integral2(temp1, h_theta, h_phi)
            c8p5_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p6[0,:]
            temp2 = field*Y8p6[1,:]
            c8p6_r = surface_integral2(temp1, h_theta, h_phi)
            c8p6_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p7[0,:]
            temp2 = field*Y8p7[1,:]
            c8p7_r = surface_integral2(temp1, h_theta, h_phi)
            c8p7_i = surface_integral2(temp2, h_theta, h_phi)

            temp1 = field*Y8p8[0,:]
            temp2 = field*Y8p8[1,:]
            c8p8_r = surface_integral2(temp1, h_theta, h_phi)
            c8p8_i = surface_integral2(temp2, h_theta, h_phi)

            norm = (c00_r*c00_r + c00_i*c00_i
                + c1m1_r*c1m1_r + c1m1_i*c1m1_i
                + c10_r*c10_r + c10_i*c10_i
                + c1p1_r*c1p1_r + c1p1_i*c1p1_i
                + c2m2_r*c2m2_r + c2m2_i*c2m2_i
                + c2m1_r*c2m1_r + c2m1_i*c2m1_i
                + c20_r*c20_r + c20_i*c20_i
                + c2p1_r*c2p1_r + c2p1_i*c2p1_i
                + c2p2_r*c2p2_r + c2p2_i*c2p2_i
                + c3m3_r*c3m3_r + c3m3_i*c3m3_i
                + c3m2_r*c3m2_r + c3m2_i*c3m2_i
                + c3m1_r*c3m1_r + c3m1_i*c3m1_i
                + c30_r*c30_r + c30_i*c30_i
                + c3p1_r*c3p1_r + c3p1_i*c3p1_i
                + c3p2_r*c3p2_r + c3p2_i*c3p2_i
                + c3p3_r*c3p3_r + c3p3_i*c3p3_i
                + c4m4_r*c4m4_r + c4m4_i*c4m4_i
                + c4m3_r*c4m3_r + c4m3_i*c4m3_i
                + c4m2_r*c4m2_r + c4m2_i*c4m2_i
                + c4m1_r*c4m1_r + c4m1_i*c4m1_i
                + c40_r*c40_r + c40_i*c40_i
                + c4p1_r*c4p1_r + c4p1_i*c4p1_i
                + c4p2_r*c4p2_r + c4p2_i*c4p2_i
                + c4p3_r*c4p3_r + c4p3_i*c4p3_i
                + c4p4_r*c4p4_r + c4p4_i*c4p4_i
                + c5m5_r*c5m5_r + c5m5_i*c5m5_i
                + c5m4_r*c5m4_r + c5m4_i*c5m4_i
                + c5m3_r*c5m3_r + c5m3_i*c5m3_i
                + c5m2_r*c5m2_r + c5m2_i*c5m2_i
                + c5m1_r*c5m1_r + c5m1_i*c5m1_i
                + c50_r*c50_r + c50_i*c50_i
                + c5p1_r*c5p1_r + c5p1_i*c5p1_i
                + c5p2_r*c5p2_r + c5p2_i*c5p2_i
                + c5p3_r*c5p3_r + c5p3_i*c5p3_i
                + c5p4_r*c5p4_r + c5p4_i*c5p4_i
                + c5p5_r*c5p5_r + c5p5_i*c5p5_i
                + c6m6_r*c6m6_r + c6m6_i*c6m6_i
                + c6m5_r*c6m5_r + c6m5_i*c6m5_i
                + c6m4_r*c6m4_r + c6m4_i*c6m4_i
                + c6m3_r*c6m3_r + c6m3_i*c6m3_i
                + c6m2_r*c6m2_r + c6m2_i*c6m2_i
                + c6m1_r*c6m1_r + c6m1_i*c6m1_i
                + c60_r*c60_r + c60_i*c60_i
                + c6p1_r*c6p1_r + c6p1_i*c6p1_i
                + c6p2_r*c6p2_r + c6p2_i*c6p2_i
                + c6p3_r*c6p3_r + c6p3_i*c6p3_i
                + c6p4_r*c6p4_r + c6p4_i*c6p4_i
                + c6p5_r*c6p5_r + c6p5_i*c6p5_i
                + c6p6_r*c6p6_r + c6p6_i*c6p6_i
                + c7m7_r*c7m7_r + c7m7_i*c7m7_i
                + c7m6_r*c7m6_r + c7m6_i*c7m6_i
                + c7m5_r*c7m5_r + c7m5_i*c7m5_i
                + c7m4_r*c7m4_r + c7m4_i*c7m4_i
                + c7m3_r*c7m3_r + c7m3_i*c7m3_i
                + c7m2_r*c7m2_r + c7m2_i*c7m2_i
                + c7m1_r*c7m1_r + c7m1_i*c7m1_i
                + c70_r*c70_r + c70_i*c70_i
                + c7p1_r*c7p1_r + c7p1_i*c7p1_i
                + c7p2_r*c7p2_r + c7p2_i*c7p2_i
                + c7p3_r*c7p3_r + c7p3_i*c7p3_i
                + c7p4_r*c7p4_r + c7p4_i*c7p4_i
                + c7p5_r*c7p5_r + c7p5_i*c7p5_i
                + c7p6_r*c7p6_r + c7p6_i*c7p6_i
                + c7p7_r*c7p7_r + c7p7_i*c7p7_i
                + c8m8_r*c8m8_r + c8m8_i*c8m8_i
                + c8m7_r*c8m7_r + c8m7_i*c8m7_i
                + c8m6_r*c8m6_r + c8m6_i*c8m6_i
                + c8m5_r*c8m5_r + c8m5_i*c8m5_i
                + c8m4_r*c8m4_r + c8m4_i*c8m4_i
                + c8m3_r*c8m3_r + c8m3_i*c8m3_i
                + c8m2_r*c8m2_r + c8m2_i*c8m2_i
                + c8m1_r*c8m1_r + c8m1_i*c8m1_i
                + c80_r*c80_r + c80_i*c80_i
                + c8p1_r*c8p1_r + c8p1_i*c8p1_i
                + c8p2_r*c8p2_r + c8p2_i*c8p2_i
                + c8p3_r*c8p3_r + c8p3_i*c8p3_i
                + c8p4_r*c8p4_r + c8p4_i*c8p4_i
                + c8p5_r*c8p5_r + c8p5_i*c8p5_i
                + c8p6_r*c8p6_r + c8p6_i*c8p6_i
                + c8p7_r*c8p7_r + c8p7_i*c8p7_i
                + c8p8_r*c8p8_r + c8p8_i*c8p8_i)

            norm = np.sqrt(norm)
       
            # for testing
            norm = 1.0

            c00_r = c00_r / norm
            c00_i = c00_i / norm
            c1m1_r = c1m1_r / norm
            c1m1_i = c1m1_i / norm
            c10_r = c10_r / norm
            c10_i = c10_i / norm
            c1p1_r = c1p1_r / norm
            c1p1_i = c1p1_i / norm
            c2m2_r = c2m2_r / norm
            c2m2_i = c2m2_i / norm
            c2m1_r = c2m1_r / norm
            c2m1_i = c2m1_i / norm
            c20_r = c20_r / norm
            c20_i = c20_i / norm
            c2p1_r = c2p1_r / norm
            c2p1_i = c2p1_i / norm
            c2p2_r = c2p2_r / norm
            c2p2_i = c2p2_i / norm
            c3m3_r = c3m3_r / norm
            c3m3_i = c3m3_i / norm
            c3m2_r = c3m2_r / norm
            c3m2_i = c3m2_i / norm
            c3m1_r = c3m1_r / norm
            c3m1_i = c3m1_i / norm
            c30_r = c30_r / norm
            c30_i = c30_i / norm
            c3p1_r = c3p1_r /norm
            c3p1_i = c3p1_i / norm
            c3p2_r = c3p2_r / norm
            c3p2_i = c3p2_i / norm
            c3p3_r = c3p3_r / norm
            c3p3_i = c3p3_i / norm
            c4m4_r = c4m4_r / norm
            c4m4_i = c4m4_i / norm
            c4m3_r = c4m3_r / norm
            c4m3_i = c4m3_i / norm
            c4m2_r = c4m2_r /norm
            c4m2_i = c4m2_i / norm
            c4m1_r = c4m1_r / norm
            c4m1_i = c4m1_i / norm
            c40_r = c40_r / norm
            c40_i = c40_i / norm
            c4p1_r = c4p1_r / norm
            c4p1_i = c4p1_i / norm
            c4p2_r = c4p2_r / norm
            c4p2_i = c4p2_i / norm
            c4p3_r = c4p3_r / norm 
            c4p3_i = c4p3_i / norm
            c4p4_r = c4p4_r / norm
            c4p4_i = c4p4_i / norm
            c5m5_r = c5m5_r / norm
            c5m5_i = c5m5_i / norm
            c5m4_r = c5m4_r / norm
            c5m4_i = c5m4_i / norm
            c5m3_r = c5m3_r / norm
            c5m3_i = c5m3_i / norm
            c5m2_r = c5m2_r /norm
            c5m2_i = c5m2_i / norm
            c5m1_r = c5m1_r / norm
            c5m1_i = c5m1_i / norm
            c50_r = c50_r / norm
            c50_i = c50_i / norm
            c5p1_r = c5p1_r / norm
            c5p1_i = c5p1_i / norm
            c5p2_r = c5p2_r / norm
            c5p2_i = c5p2_i / norm
            c5p3_r = c5p3_r / norm 
            c5p3_i = c5p3_i / norm
            c5p4_r = c5p4_r / norm
            c5p4_i = c5p4_i / norm
            c5p5_r = c5p5_r / norm
            c5p5_i = c5p5_i / norm
            c6m6_r = c6m6_r / norm
            c6m6_i = c6m6_i / norm
            c6m5_r = c6m5_r / norm
            c6m5_i = c6m5_i / norm
            c6m4_r = c6m4_r / norm
            c6m4_i = c6m4_i / norm
            c6m3_r = c6m3_r / norm
            c6m3_i = c6m3_i / norm
            c6m2_r = c6m2_r /norm
            c6m2_i = c6m2_i / norm
            c6m1_r = c6m1_r / norm
            c6m1_i = c6m1_i / norm
            c60_r = c60_r / norm
            c60_i = c60_i / norm
            c6p1_r = c6p1_r / norm
            c6p1_i = c6p1_i / norm
            c6p2_r = c6p2_r / norm
            c6p2_i = c6p2_i / norm
            c6p3_r = c6p3_r / norm 
            c6p3_i = c6p3_i / norm
            c6p4_r = c6p4_r / norm
            c6p4_i = c6p4_i / norm
            c6p5_r = c6p5_r / norm
            c6p5_i = c6p5_i / norm
            c6p6_r = c6p6_r / norm
            c6p6_i = c6p6_i / norm
            c7m7_r = c7m7_r / norm
            c7m7_i = c7m7_i / norm
            c7m6_r = c7m6_r / norm
            c7m6_i = c7m6_i / norm
            c7m5_r = c7m5_r / norm
            c7m5_i = c7m5_i / norm
            c7m4_r = c7m4_r / norm
            c7m4_i = c7m4_i / norm
            c7m3_r = c7m3_r / norm
            c7m3_i = c7m3_i / norm
            c7m2_r = c7m2_r /norm
            c7m2_i = c7m2_i / norm
            c7m1_r = c7m1_r / norm
            c7m1_i = c7m1_i / norm
            c70_r = c70_r / norm
            c70_i = c70_i / norm
            c7p1_r = c7p1_r / norm
            c7p1_i = c7p1_i / norm
            c7p2_r = c7p2_r / norm
            c7p2_i = c7p2_i / norm
            c7p3_r = c7p3_r / norm 
            c7p3_i = c7p3_i / norm
            c7p4_r = c7p4_r / norm
            c7p4_i = c7p4_i / norm
            c7p5_r = c7p5_r / norm
            c7p5_i = c7p5_i / norm
            c7p6_r = c7p6_r / norm
            c7p6_i = c7p6_i / norm
            c7p7_r = c7p7_r / norm
            c7p7_i = c7p7_i / norm
            c8m8_r = c8m8_r / norm
            c8m8_i = c8m8_i / norm
            c8m7_r = c8m7_r / norm
            c8m7_i = c8m7_i / norm
            c8m6_r = c8m6_r / norm
            c8m6_i = c8m6_i / norm
            c8m5_r = c8m5_r / norm
            c8m5_i = c8m5_i / norm
            c8m4_r = c8m4_r / norm
            c8m4_i = c8m4_i / norm
            c8m3_r = c8m3_r / norm
            c8m3_i = c8m3_i / norm
            c8m2_r = c8m2_r /norm
            c8m2_i = c8m2_i / norm
            c8m1_r = c8m1_r / norm
            c8m1_i = c8m1_i / norm
            c80_r = c80_r / norm
            c80_i = c80_i / norm
            c8p1_r = c8p1_r / norm
            c8p1_i = c8p1_i / norm
            c8p2_r = c8p2_r / norm
            c8p2_i = c8p2_i / norm
            c8p3_r = c8p3_r / norm 
            c8p3_i = c8p3_i / norm
            c8p4_r = c8p4_r / norm
            c8p4_i = c8p4_i / norm
            c8p5_r = c8p5_r / norm
            c8p5_i = c8p5_i / norm
            c8p6_r = c8p6_r / norm
            c8p6_i = c8p6_i / norm
            c8p7_r = c8p7_r / norm
            c8p7_i = c8p7_i / norm
            c8p8_r = c8p8_r / norm
            c8p8_i = c8p8_i / norm

            print("-------------------------")
            print("-----L=0 MODES ----------")
            print("-------------------------")
            print("c00=",c00_r,  "c00=",c00_i)

            f90.write(str(time) + " " + str(c00_r) + " " + str(c00_i) + "\n")

            print("-------------------------")
            print("-----L=1 MODES ----------")
            print("-------------------------")
            print("c1m1=",c1m1_r,"c1m1=",c1m1_i)
            print("c10=",c10_r,  "c10=",c10_i)
            print("c1p1=",c1p1_r,"c1p1=",c1p1_i)

            f100.write(str(time) + " " + str(c1m1_r) + " " + str(c1m1_i) + "\n")
            f101.write(str(time) + " " + str(c10_r) + " " + str(c10_i) + "\n")
            f102.write(str(time) + " " + str(c1p1_r) + " " + str(c1p1_i) + "\n")

            print("-------------------------")   
            print("-----L=2 MODES ----------")
            print("-------------------------")
            print("c2m2=",c2m2_r,"c2m2=",c2m2_i)
            print("c2m1=",c2m1_r,"c2m1=",c2m1_i)
            print("c20=",c20_r,  "c20=",c20_i)
            print("c2p1=",c2p1_r,"c2p1=",c2p1_i)
            print("c2p2=",c2p2_r,"c2p2=",c2p2_i)

            f110.write(str(time) + " " + str(c2m2_r) + " " + str(c2m2_i) + "\n")
            f111.write(str(time) + " " + str(c2m1_r) + " " + str(c2m1_i) + "\n")
            f112.write(str(time) + " " + str(c20_r) + " " + str(c20_i) + "\n")
            f113.write(str(time) + " " + str(c2p1_r) + " " + str(c2p1_i) + "\n")
            f114.write(str(time) + " " + str(c2p2_r) + " " + str(c2p2_i) + "\n")

            print("-------------------------")
            print("-----L=3 MODES ----------")
            print("-------------------------")
            print("c3m3=",c3m3_r,"c3m3=",c3m3_i)
            print("c3m2=",c3m2_r,"c3m2=",c3m2_i)
            print("c3m1=",c3m1_r,"c3m1=",c3m1_i)
            print("c30=",c30_r,  "c30=",c30_i)
            print("c3p1=",c3p1_r,"c3p1=",c3p1_i)
            print("c3p2=",c3p2_r,"c3p2=",c3p2_i)
            print("c3p3=",c3p3_r,"c3p3=",c3p3_i)

            f120.write(str(time) + " " + str(c3m3_r) + " " + str(c3m3_i) + "\n")
            f121.write(str(time) + " " + str(c3m2_r) + " " + str(c3m2_i) + "\n")
            f122.write(str(time) + " " + str(c3m1_r) + " " + str(c3m1_i) + "\n")
            f123.write(str(time) + " " + str(c30_r) + " " + str(c30_i) + "\n")
            f124.write(str(time) + " " + str(c3p1_r) + " " + str(c3p1_i) + "\n")
            f125.write(str(time) + " " + str(c3p2_r) + " " + str(c3p2_i) + "\n")
            f126.write(str(time) + " " + str(c3p3_r) + " " + str(c3p3_i) + "\n")

            print("-------------------------")
            print("-----L=4 MODES ----------")
            print("-------------------------")
            print("c4m4=",c4m4_r,"c4m4=",c4m4_i)
            print("c4m3=",c4m3_r,"c4m3=",c4m3_i)
            print("c4m2=",c4m2_r,"c4m2=",c4m2_i)
            print("c4m1=",c4m1_r,"c4m1=",c4m1_i)
            print("c40=",c40_r,  "c40=",c40_i)
            print("c4p1=",c4p1_r,"c4p1=",c4p1_i)
            print("c4p2=",c4p2_r,"c4p2=",c4p2_i)
            print("c4p3=",c4p3_r,"c4p3=",c4p3_i)
            print("c4p4=",c4p4_r,"c4p4=",c4p4_i)

            f130.write(str(time) + " " + str(c4m4_r) + " " + str(c4m4_i) + "\n")
            f131.write(str(time) + " " + str(c4m3_r) + " " + str(c4m3_i) + "\n")
            f132.write(str(time) + " " + str(c4m2_r) + " " + str(c4m2_i) + "\n")
            f133.write(str(time) + " " + str(c4m1_r) + " " + str(c4m1_i) + "\n")
            f134.write(str(time) + " " + str(c40_r) + " " + str(c40_i) + "\n")
            f135.write(str(time) + " " + str(c4p1_r) + " " + str(c4p1_i) + "\n")
            f136.write(str(time) + " " + str(c4p2_r) + " " + str(c4p2_i) + "\n")
            f137.write(str(time) + " " + str(c4p3_r) + " " + str(c4p3_i) + "\n")
            f138.write(str(time) + " " + str(c4p4_r) + " " + str(c4p4_i) + "\n")

            print("-------------------------")
            print("-----L=5 MODES ----------")
            print("-------------------------")
            print("c5m5=",c5m5_r,"c5m5=",c5m5_i)
            print("c5m4=",c5m4_r,"c5m4=",c5m4_i)
            print("c5m3=",c5m3_r,"c5m3=",c5m3_i)
            print("c5m2=",c5m2_r,"c5m2=",c5m2_i)
            print("c5m1=",c5m1_r,"c5m1=",c5m1_i)
            print("c50=",c50_r,  "c50=",c50_i)
            print("c5p1=",c5p1_r,"c5p1=",c5p1_i)
            print("c5p2=",c5p2_r,"c5p2=",c5p2_i)
            print("c5p3=",c5p3_r,"c5p3=",c5p3_i)
            print("c5p4=",c5p4_r,"c5p4=",c5p4_i)
            print("c5p5=",c5p5_r,"c5p5=",c5p5_i)

            f140.write(str(time) + " " + str(c5m5_r) + " " + str(c5m5_i) + "\n")
            f141.write(str(time) + " " + str(c5m4_r) + " " + str(c5m4_i) + "\n")
            f142.write(str(time) + " " + str(c5m3_r) + " " + str(c5m3_i) + "\n")
            f143.write(str(time) + " " + str(c5m2_r) + " " + str(c5m2_i) + "\n")
            f144.write(str(time) + " " + str(c5m1_r) + " " + str(c5m1_i) + "\n")
            f145.write(str(time) + " " + str(c50_r) + " " + str(c50_i) + "\n")
            f146.write(str(time) + " " + str(c5p1_r) + " " + str(c5p1_i) + "\n")
            f147.write(str(time) + " " + str(c5p2_r) + " " + str(c5p2_i) + "\n")
            f148.write(str(time) + " " + str(c5p3_r) + " " + str(c5p3_i) + "\n")
            f149.write(str(time) + " " + str(c5p4_r) + " " + str(c5p4_i) + "\n")
            f150.write(str(time) + " " + str(c5p5_r) + " " + str(c5p5_i) + "\n")

            print("-------------------------")
            print("-----L=6 MODES ----------")
            print("-------------------------")
            print("c6m6=",c6m6_r,"c6m6=",c6m6_i)
            print("c6m5=",c6m5_r,"c6m5=",c6m5_i)
            print("c6m4=",c6m4_r,"c6m4=",c6m4_i)
            print("c6m3=",c6m3_r,"c6m3=",c6m3_i)
            print("c6m2=",c6m2_r,"c6m2=",c6m2_i)
            print("c6m1=",c6m1_r,"c6m1=",c6m1_i)
            print("c60=",c60_r,  "c60=",c60_i)
            print("c6p1=",c6p1_r,"c6p1=",c6p1_i)
            print("c6p2=",c6p2_r,"c6p2=",c6p2_i)
            print("c6p3=",c6p3_r,"c6p3=",c6p3_i)
            print("c6p4=",c6p4_r,"c6p4=",c6p4_i)
            print("c6p5=",c6p5_r,"c6p5=",c6p5_i)
            print("c6p6=",c6p6_r,"c6p6=",c6p6_i)

            f160.write(str(time) + " " + str(c6m6_r) + " " + str(c6m6_i) + "\n")
            f161.write(str(time) + " " + str(c6m5_r) + " " + str(c6m5_i) + "\n")
            f162.write(str(time) + " " + str(c6m4_r) + " " + str(c6m4_i) + "\n")
            f163.write(str(time) + " " + str(c6m3_r) + " " + str(c6m3_i) + "\n")
            f164.write(str(time) + " " + str(c6m2_r) + " " + str(c6m2_i) + "\n")
            f165.write(str(time) + " " + str(c6m1_r) + " " + str(c6m1_i) + "\n")
            f166.write(str(time) + " " + str(c60_r) + " " + str(c60_i) + "\n")
            f167.write(str(time) + " " + str(c6p1_r) + " " + str(c6p1_i) + "\n")
            f168.write(str(time) + " " + str(c6p2_r) + " " + str(c6p2_i) + "\n")
            f169.write(str(time) + " " + str(c6p3_r) + " " + str(c6p3_i) + "\n")
            f170.write(str(time) + " " + str(c6p4_r) + " " + str(c6p4_i) + "\n")
            f171.write(str(time) + " " + str(c6p5_r) + " " + str(c6p5_i) + "\n")
            f172.write(str(time) + " " + str(c6p6_r) + " " + str(c6p6_i) + "\n")

            print("-------------------------")
            print("-----L=7 MODES ----------")
            print("-------------------------")
            print("c7m7=",c7m7_r,"c7m7=",c7m7_i)
            print("c7m6=",c7m6_r,"c7m6=",c7m6_i)
            print("c7m5=",c7m5_r,"c7m5=",c7m5_i)
            print("c7m4=",c7m4_r,"c7m4=",c7m4_i)
            print("c7m3=",c7m3_r,"c7m3=",c7m3_i)
            print("c7m2=",c7m2_r,"c7m2=",c7m2_i)
            print("c7m1=",c7m1_r,"c7m1=",c7m1_i)
            print("c70=",c70_r,  "c70=",c70_i)
            print("c7p1=",c7p1_r,"c7p1=",c7p1_i)
            print("c7p2=",c7p2_r,"c7p2=",c7p2_i)
            print("c7p3=",c7p3_r,"c7p3=",c7p3_i)
            print("c7p4=",c7p4_r,"c7p4=",c7p4_i)
            print("c7p5=",c7p5_r,"c7p5=",c7p5_i)
            print("c7p6=",c7p6_r,"c7p6=",c7p6_i)
            print("c7p7=",c7p7_r,"c7p7=",c7p7_i)

            f180.write(str(time) + " " + str(c7m7_r) + " " + str(c7m7_i) + "\n")
            f181.write(str(time) + " " + str(c7m6_r) + " " + str(c7m6_i) + "\n")
            f182.write(str(time) + " " + str(c7m5_r) + " " + str(c7m5_i) + "\n")
            f183.write(str(time) + " " + str(c7m4_r) + " " + str(c7m4_i) + "\n")
            f184.write(str(time) + " " + str(c7m3_r) + " " + str(c7m3_i) + "\n")
            f185.write(str(time) + " " + str(c7m2_r) + " " + str(c7m2_i) + "\n")
            f186.write(str(time) + " " + str(c7m1_r) + " " + str(c7m1_i) + "\n")
            f187.write(str(time) + " " + str(c70_r) + " " + str(c70_i) + "\n")
            f188.write(str(time) + " " + str(c7p1_r) + " " + str(c7p1_i) + "\n")
            f189.write(str(time) + " " + str(c7p2_r) + " " + str(c7p2_i) + "\n")
            f190.write(str(time) + " " + str(c7p3_r) + " " + str(c7p3_i) + "\n")
            f191.write(str(time) + " " + str(c7p4_r) + " " + str(c7p4_i) + "\n")
            f192.write(str(time) + " " + str(c7p5_r) + " " + str(c7p5_i) + "\n")
            f193.write(str(time) + " " + str(c7p6_r) + " " + str(c7p6_i) + "\n")
            f194.write(str(time) + " " + str(c7p7_r) + " " + str(c7p7_i) + "\n")

            print("-------------------------")
            print("-----L=8 MODES ----------")
            print("-------------------------")
            print("c8m8=",c8m8_r,"c8m8=",c8m8_i)
            print("c8m7=",c8m7_r,"c8m7=",c8m7_i)
            print("c8m6=",c8m6_r,"c8m6=",c8m6_i)
            print("c8m5=",c8m5_r,"c8m5=",c8m5_i)
            print("c8m4=",c8m4_r,"c8m4=",c8m4_i)
            print("c8m3=",c8m3_r,"c8m3=",c8m3_i)
            print("c8m2=",c8m2_r,"c8m2=",c8m2_i)
            print("c8m1=",c8m1_r,"c8m1=",c8m1_i)
            print("c80=",c80_r,  "c80=",c80_i)
            print("c8p1=",c8p1_r,"c8p1=",c8p1_i)
            print("c8p2=",c8p2_r,"c8p2=",c8p2_i)
            print("c8p3=",c8p3_r,"c8p3=",c8p3_i)
            print("c8p4=",c8p4_r,"c8p4=",c8p4_i)
            print("c8p5=",c8p5_r,"c8p5=",c8p5_i)
            print("c8p6=",c8p6_r,"c8p6=",c8p6_i)
            print("c8p7=",c8p7_r,"c8p7=",c8p7_i)
            print("c8p8=",c8p8_r,"c8p8=",c8p8_i)

            f200.write(str(time) + " " + str(c8m8_r) + " " + str(c8m8_i) + "\n")
            f201.write(str(time) + " " + str(c8m7_r) + " " + str(c8m7_i) + "\n")
            f202.write(str(time) + " " + str(c8m6_r) + " " + str(c8m6_i) + "\n")
            f203.write(str(time) + " " + str(c8m5_r) + " " + str(c8m5_i) + "\n")
            f204.write(str(time) + " " + str(c8m4_r) + " " + str(c8m4_i) + "\n")
            f205.write(str(time) + " " + str(c8m3_r) + " " + str(c8m3_i) + "\n")
            f206.write(str(time) + " " + str(c8m2_r) + " " + str(c8m2_i) + "\n")
            f207.write(str(time) + " " + str(c8m1_r) + " " + str(c8m1_i) + "\n")
            f208.write(str(time) + " " + str(c80_r) + " " + str(c80_i) + "\n")
            f209.write(str(time) + " " + str(c8p1_r) + " " + str(c8p1_i) + "\n")
            f210.write(str(time) + " " + str(c8p2_r) + " " + str(c8p2_i) + "\n")
            f211.write(str(time) + " " + str(c8p3_r) + " " + str(c8p3_i) + "\n")
            f212.write(str(time) + " " + str(c8p4_r) + " " + str(c8p4_i) + "\n")
            f213.write(str(time) + " " + str(c8p5_r) + " " + str(c8p5_i) + "\n")
            f214.write(str(time) + " " + str(c8p6_r) + " " + str(c8p6_i) + "\n")
            f215.write(str(time) + " " + str(c8p7_r) + " " + str(c8p7_i) + "\n")
            f216.write(str(time) + " " + str(c8p8_r) + " " + str(c8p8_i) + "\n")

    f90.close()
    f100.close()
    f101.close()
    f102.close()

    f110.close()
    f111.close()
    f112.close()
    f113.close()
    f114.close()

    f120.close()
    f121.close()
    f122.close()
    f123.close()
    f124.close()
    f125.close()
    f126.close()

    f130.close()
    f131.close()
    f132.close()
    f133.close()
    f134.close()
    f135.close()
    f136.close()
    f137.close()
    f138.close()

    f140.close()
    f141.close()
    f142.close()
    f143.close()
    f144.close()
    f145.close()
    f146.close()
    f147.close()
    f148.close()
    f149.close()
    f150.close()

    f160.close()
    f161.close()
    f162.close()
    f163.close()
    f164.close()
    f165.close()
    f166.close()
    f167.close()
    f168.close()
    f169.close()
    f170.close()
    f171.close()
    f172.close()

    f180.close()
    f181.close()
    f182.close()
    f183.close()
    f184.close()
    f185.close()
    f186.close()
    f187.close()
    f188.close()
    f189.close()
    f190.close()
    f191.close()
    f192.close()
    f193.close()
    f194.close()

    f200.close()
    f201.close()
    f202.close()
    f203.close()
    f204.close()
    f205.close()
    f206.close()
    f207.close()
    f208.close()
    f209.close()
    f210.close()
    f211.close()
    f212.close()
    f213.close()
    f214.close()
    f215.close()
    f216.close()

if __name__ == "__main__":
    for r in radius:
        calculate(r)
