import numpy as np
import h5py
import os
import sys
from mpi4py import MPI


# Reads all the points in each direction
def readData2DAll(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset[...]

# Reads only the N points, avoiding the repeated ones
def readData3DInternal(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    npts = dset.shape[0]
    dset=dset[0:npts-1,0:npts-1,0:npts-1]
    f.close()
    return dset

# Adds before and after a Ghost-wide stencil in each direction, taken from periodical BC
def readData3DGhost(f, v, ghost):
    f = h5py.File(f, "r")
    dset = f[v]
    npts = dset.shape[0]
    dset = dset[0:npts-1,0:npts-1,0:npts-1]
    dset = np.pad(dset, ghost, 'wrap')

    f.close()
    return dset

# Considers Slabs cutting the first dimension
def readData3DSlab(f, v, rank, Np):
    f = h5py.File(f, "r")
    dset = f[v]
    data=np.asarray(dset[rank*Np:(rank+1)*Np, ...])
    data=data[:, :-1, :-1]
    f.close()
    return data

# Reads the entire set of Data by windows with side wSize
# By construction the last periodic points are not read
def readData3DWindow(f, v, w0, w1, w2, wSize):
    f = h5py.File(f, "r")
    dset = f[v]
    data=np.asarray(dset[w0:w0 + wSize,w1:w1 + wSize,w2:w2 + wSize])
    f.close()
    return data

# Reads the entire set of Data by windows with side wSize and add a stencil taken from the other windows
# By construction the last periodic points are not read
def readData3DWindowGhost(f, v, w0, w1, w2, wSize, ghost):
    f = h5py.File(f, "r")
#    f = h5py.File(f, "r", driver='mpio', comm=MPI.COMM_WORLD)
    dset = f[v]
    npts = dset.shape[0]
    dset = np.asarray(dset)[0:npts-1,0:npts-1,0:npts-1]

    sl0 = np.array(range(w0 - ghost,w0 + wSize + ghost)).reshape(-1,1, 1)%dset.shape[0]
    sl1 = np.array(range(w1 - ghost,w1 + wSize + ghost)).reshape(1,-1, 1)%dset.shape[1]
    sl2 = np.array(range(w2 - ghost,w2 + wSize + ghost)).reshape(1,1,-1)%dset.shape[2]
    result = dset[sl0,sl1,sl2]

    f.close()
    return result

# Reads the entire set of Data by windows with side wSize and add a stencil taken from the other windows
# Adapted to work without the MPI.COMM
# By construction the last periodic points are not read
def readData3DWindowGhostMono(f, v, w0, w1, w2, wSize, ghost):
    f = h5py.File(f, "r")
    dset = f[v]
    npts = dset.shape[0]
    dset = np.asarray(dset)[0:npts-1,0:npts-1,0:npts-1]

    sl0 = np.array(range(w0 - ghost,w0 + wSize + ghost)).reshape(-1,1, 1)%dset.shape[0]
    sl1 = np.array(range(w1 - ghost,w1 + wSize + ghost)).reshape(1,-1, 1)%dset.shape[1]
    sl2 = np.array(range(w2 - ghost,w2 + wSize + ghost)).reshape(1,1,-1)%dset.shape[2]
    result = dset[sl0,sl1,sl2]

    f.close()
    return result


# Save 3D data
def saveData3D(f, v, data):
    f = h5py.File(f, "w")
    dset = f.create_dataset(v, data=data)
    f.close()

# Save 3D data by windows
def saveData3DWindow(f, v, data, w0, w1, w2, wSize, par=True):
    if par:
        f = h5py.File(f, "r+")
#        f = h5py.File(f, "r+", driver='mpio', comm=MPI.COMM_WORLD)
    else:
        f = h5py.File(f, "r+")
    dset = f[v]
    dset[w0:w0 + wSize,w1:w1 + wSize,w2:w2 + wSize, ...] = data[:,:,:,...]
    f.close()

# create a Data Set for parallel saving
def createDataSet(f, v, shape, wSize, par=True):
    if par:
#        f = h5py.File(f, "w", driver='mpio', comm=MPI.COMM_WORLD)
        f = h5py.File(f, "w")
    else:
        f = h5py.File(f, "w")
    dset = f.create_dataset(v, shape)
    f.close()

# write fields
def writefields3D(folder, field, level):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']

    maximum = [0, 0, 0]
    minimum = [999999, 999999, 999999]
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum[0] = max(maximum[0], patchExtents[i][1][0])
            maximum[1] = max(maximum[1], patchExtents[i][1][1])
            maximum[2] = max(maximum[2], patchExtents[i][1][2])
            minimum[0] = min(minimum[0], patchExtents[i][0][0])
            minimum[1] = min(minimum[1], patchExtents[i][0][1])
            minimum[2] = min(minimum[2], patchExtents[i][0][2])
    size = [(maximum[0] - minimum[0]) + 2 ,(maximum[1] - minimum[1]) + 2, (maximum[2] - minimum[2]) + 2]
    data = np.zeros(shape=(size[0], size[1], size[2]))

    for iPatch in range(len(patchExtents)):
        if (patchMap[iPatch][2] == level):
            iProc = patchMap[iPatch][0]
            iProcStr = str(iProc).zfill(5)
            iPatchStr = str(patchMap[iPatch][3]).zfill(5)
            rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
            rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
            rangeZ = (patchExtents[iPatch][0][2] - minimum[2], patchExtents[iPatch][1][2]+2 - minimum[2])
            sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
            sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
            sizeZ = patchExtents[iPatch][1][2] - patchExtents[iPatch][0][2] + 2;
            f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
            tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
            tmp = np.reshape(tmp, (sizeX,sizeY,sizeZ), order="F")
            data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[:,:]
            f_data.close()

    data2 = data
    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data2.shape, dtype='float64')
    dset[:,:,:] = data2[:,:,:]
    f.close()
    f_sum.close()
