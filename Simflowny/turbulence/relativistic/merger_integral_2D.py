import numpy as np
import h5py
import os
import math
import sys
from mpi4py import MPI
from common_io_2D import readData2DAll
from common_op_2D import int2D, get_center_of_mass
"""
    This script calculates the integrals and their plots from 2D data generated by readhdf5_interpolador_2D.py
"""

def merger_integral(folders, dx,dt,folderin,folderout,level, mask_rho_lower_lim, mask_rho_upper_lim):
    """
    Does the integral calculations.

    Args:
        Check launcher
    """

    comm = MPI.COMM_WORLD
    num_processes = comm.Get_size()
    rank = comm.Get_rank()
   
    #Get number of cycles
    Ncycle = 0
    for f in folders:
        for i in folders[f]:
            Ncycle = Ncycle + 1

    #Get number of points in one dimension (data is squared) and mask
    key = list(folders.keys())[0]
    Npoints = readData2DAll(key + "/visit_dump."+str(folders[key][0]).zfill(5) + folderin +"/rhof_"+str(level)+".hdf5","rhof").shape[0]
    
    time = np.zeros((Ncycle))

    Nproc = (Npoints - 1) // num_processes

    folderout_r = folderout
    if not os.path.exists(folderout_r):
        os.makedirs(folderout_r)

    outname = folderout_r+"/integral.out"
    outname_com = folderout_r+"/center_of_mass.out"
    if rank == 0:
        f3=open(outname,"w+")
        f4=open(outname_com,"w+")
        f3.write("#     time    INT(Ekin)    INT(Erot)    INT(Eint)    INT(Emag)    BRMS     BRMSP     BRMST    BRMSZ    invbeta     INT(D)    MAX(rho) \n")
        f4.write("#     time          x            y   \n")

    cc = 0
    for f in folders:
        for i in folders[f]:
            #print i, dt
            time[cc] = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5)+ folderin
            print(f,i,time[cc])

            print("Calculate mask")
            mask = readData2DAll(key + "/visit_dump."+str(folders[key][0]).zfill(5) + folderin +"/rhof_"+str(level)+".hdf5","rhof")
            mask = np.logical_and(mask >= mask_rho_lower_lim, mask <= mask_rho_upper_lim)
            print("Puntos mascara",np.sum(mask))

            x = readData2DAll(foldertime+"x_"+str(level)+".hdf5","x")
            y = readData2DAll(foldertime+"y_"+str(level)+".hdf5","y")

            #Calculate center of mass in the finner level
            center_of_mass = get_center_of_mass(readData2DAll(foldertime +"/rhof_"+str(level)+".hdf5","rhof"), x, y)

            x = x - center_of_mass[0]
            y = y - center_of_mass[1]

            r = np.sqrt(x**2 + y **2)        
            r[r == 0] = 0.0000000000001

            #Integral calculation
            chi = readData2DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
            detg = np.power(chi, -3.0/2)
            print("int_ekin")
            int_ekin = readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")
            int_ekin = int_ekin * int_ekin
            int_ekin = int_ekin + readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")*readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")
            int_ekin = int_ekin + readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")*readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")
            int_ekin = int_ekin * readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
            int_ekin = detg * int_ekin * 0.5 * chi 
            int_ekin = int2D(int_ekin * mask, dx)

            print("int_emag")
            b2 = readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
            b2 = b2 * b2
            b2 = b2 + readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")*readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
            b2 = b2 + readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")*readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")
            b2 = b2 * chi * chi
            int_emag = detg * 0.5 * b2 
            int_emag = int2D(int_emag * mask, dx)

            BRMS = detg * np.sqrt(b2)
            BRMS = int2D(BRMS * mask, dx)

            print("int_eint")
            int_eint = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")*readData2DAll(foldertime+"epsf_"+str(level)+".hdf5","epsf")
            vi2 = readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")*readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")
            vi2 = vi2 + readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")*readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")
            vi2 = vi2 + readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")*readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")
            W = np.sqrt(np.maximum(1 - chi * vi2,0.1))
            int_eint = detg * int_eint * W
            int_eint = int2D(int_eint * mask, dx)
            
            print("int_erot")
            int_erot = -y/r * (readData2DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * chi * readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x") - readData2DAll(foldertime+"Betau_x_"+str(level)+".hdf5","Betau_x"))
            int_erot = int_erot + x/r * (readData2DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * chi * readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y") - readData2DAll(foldertime+"Betau_y_"+str(level)+".hdf5","Betau_y"))
            Ttphi = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof") * (1 + readData2DAll(foldertime+"epsf_"+str(level)+".hdf5","epsf")) + readData2DAll(foldertime+"pf_"+str(level)+".hdf5","pf")
            Ttphi = Ttphi * W / readData2DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha")
            Ttphi = Ttphi * W * (-y * readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x") + x * readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y"))
            int_erot = detg * 0.5 * int_erot * Ttphi
            int_erot = int2D(int_erot * mask, dx)

            volume = readData2DAll(foldertime+"mask_"+str(level)+".hdf5","mask")
            volume = detg * np.logical_not(volume)
            volume = int2D(volume * mask,dx)
           

            #Toroidal B
            print("B toroidal")
            int_bt = -y  * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
            int_bt = int_bt + x * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
            bt = int_bt * chi / r
            int_bt = detg * np.sqrt(bt*bt)
            int_bt = int2D(int_bt * mask, dx)
            BRMST = int_bt
            #Poloidal B
            print("B poloidal")
            int_bp = detg * np.sqrt(np.maximum(b2-bt*bt, 0))
            int_bp = int2D(int_bp * mask, dx)
            BRMSP = int_bp
            print("B z")
            int_bz = chi * readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z") 
            int_bz = detg * np.sqrt(int_bz*int_bz)
            int_bz = int2D(int_bz * mask, dx)
            BRMSZ = int_bz

            # invbeta = B^2/(2*P)
            int_invbeta = detg * 0.5* b2 / np.maximum(readData2DAll(foldertime+"pf_"+str(level)+".hdf5","pf"), 1e-20)
            int_invbeta = int2D(mask * int_invbeta, dx)
            invbeta = int_invbeta

            result = np.array(0.0)
            int_ekin = np.array(int_ekin)
            comm.Reduce([int_ekin, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_ekin = np.array(result)

            int_emag = np.array(int_emag)
            comm.Reduce([int_emag, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_emag = np.array(result)

            int_eint = np.array(int_eint)
            comm.Reduce([int_eint, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_eint = np.array(result)

            int_erot = np.array(int_erot)
            comm.Reduce([int_erot, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_erot = np.array(result)

            volume = np.array(volume)
            comm.Reduce([volume, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            volume = np.array(result)

            BRMS = np.array(BRMS)
            comm.Reduce([BRMS, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            BRMS = np.array(result/volume)

            BRMSP = np.array(BRMSP)
            comm.Reduce([BRMSP, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            BRMSP = np.array(result/volume)

            BRMST = np.array(BRMST)
            comm.Reduce([BRMST, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            BRMST = np.array(result/volume)

            BRMSZ = np.array(BRMSZ)
            comm.Reduce([BRMSZ, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            BRMSZ = np.array(result/volume)

            invbeta = np.array(invbeta)
            comm.Reduce([invbeta, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            invbeta = np.array(result/volume)

            #print int_ekin, int_emag
            #Integral D
            print("int_D")
            int_D = detg * readData2DAll(foldertime+"Df_"+str(level)+".hdf5","Df")
            int_D = int2D(int_D * mask, dx)
            int_D = np.array(int_D)
            comm.Reduce([int_D, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_D = np.array(result)
            #Max rho
            max_rho = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
            max_rho = np.max(max_rho)
            max_rho = np.array(max_rho)
            comm.Reduce([max_rho, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.MAX, root=0)
            max_rho = np.array(result)
            #Integral write
            if rank == 0:
                f3.write("%12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e\n" % (time[cc],int_ekin,int_erot,int_eint, int_emag, BRMS, BRMSP, BRMST, BRMSZ, invbeta, int_D, max_rho))
                f4.write("%12.3e %12.3e %12.3e\n" % (time[cc], center_of_mass[0], center_of_mass[1]))

    if rank == 0:
        f3.close()
        f4.close()
