import pyreprimand as pyr
import numpy as np
import os
import math
import sys
from common_io_3D import readData3DSlab, readData3DAll
from common_op_3D import fourier_space_3D, int3D, calculate_masks, get_center_of_mass

"""
    This script calculates the integrals and their plots from 3D data generated by readhdf5_levels_3D.py.
    This version uses RePrimAnd. And needs a launcher, similar to merger_integral_levels_launcher_3D.py, which is missing.
"""

def merger_integral(folders,dt,folderin,folderout,max_levels, box_init, mask_rho_lower_lim, mask_rho_upper_lim):
    """
    Does the integral calculations.

    Args:
        Check launcher
    """

    folderout_r = folderout
    if not os.path.exists(folderout_r):
        os.makedirs(folderout_r)


    outname = folderout_r+"/integral.out"
    outname_ave = folderout_r+"/averages.out"
    outname_com = folderout_r+"/center_of_mass.out"
    f3=open(outname,"w+")
    f4=open(outname_ave,"w+")
    f5=open(outname_com,"w+")
    f3.write("#     time      INT(Ekin)    INT(Erot)    INT(Eint)     INT(Emag)      INT(D)      MAX(rho)   Hmag    INT(P0)    INT(P1)    INT(P2)    INT(P3)    INT(P4)     INT(Eth)\n")
    f4.write("#     time         <B>        <Btor>       <Bpol>        <Bz>        <Bz/B>       <Btor/B>      <invbeta>         <Qz>        <Qphi>      <R>     <alphamag>      <l_MRI_z>      <l_MRI_phi>      <l_MRI_z_nodetg>      <l_MRI_phi_nodetg>    <bt>    <h>    <b2>    <omega>\n")
    f5.write("#     time          x            y            z \n")


    for f in folders:
        for i in folders[f]:
            print(f, i)
            time = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5) + folderin

            masks, levels = calculate_masks(foldertime, max_levels, box_init)
            int_D_total = 0
            max_rho_total = 0
            int_ekin_total = 0
            int_emag_total = 0
            int_eth_total = 0
            int_eint_total = 0
            int_erot_total = 0
            volume_total = 0
            volume_nodetg_total = 0
            int_bt_total = 0
            int_bp_total = 0
            int_bz_total = 0
            int_b_total = 0
            int_rBzB_total = 0
            int_rBtB_total = 0
            int_invbeta_total = 0
            int_hmag_total = 0
            int_p0_total = 0
            int_p1_total = 0
            int_p2_total = 0
            int_p3_total = 0
            int_p4_total = 0
            Qz_total = 0
            lambda_MRI_z_total = 0
            lambda_MRI_phi_total = 0
            lambda_MRI_z_nodetg_total = 0
            lambda_MRI_phi_nodetg_total = 0
            Qphi_total = 0
            R_total = 0
            alpha_mag_total = 0
            bt_total = 0
            h_total = 0
            b2_total = 0
            omega_total = 0
            center_of_mass = [0,0,0]
            for level in levels:
                l_i = levels.index(level)
                mask = masks[l_i]

                mask_rho = readData3DAll(foldertime + "/rhof_"+str(level)+".hdf5","rhof")
                mask_rho = np.logical_and(mask_rho >= mask_rho_lower_lim, mask_rho <= mask_rho_upper_lim)
                
                mask = mask * mask_rho

                dxs = np.load(foldertime + "/dx_" + str(level) + ".npy")
                dx = dxs[0]
                dy = dxs[1]
                dz = dxs[2]
                
                x = readData3DAll(foldertime+"x_"+str(level)+".hdf5","x")
                y = readData3DAll(foldertime+"y_"+str(level)+".hdf5","y")

                print("Nivel",level,"dx",dx,"puntos mascara",np.sum(mask))

                #Calculate center of mass in the finner level
                center_of_mass = get_center_of_mass(readData3DAll(foldertime +"/rhof_"+str(level)+".hdf5","rhof"), x, y, readData3DAll(foldertime+"z_"+str(level)+".hdf5","z"))

                x = x - center_of_mass[0]
                y = y - center_of_mass[1]

                rinv = np.sqrt(x**2 + y **2)        
                rinv[rinv == 0] = 0.0000000000001
                r2inv = 1/(rinv*rinv)
                rinv = 1/rinv


                #Integral calculation
                detg = readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                detg = np.power(detg, -3.0/2)
                
                h = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof") * (1 + readData3DAll(foldertime+"epsf_"+str(level)+".hdf5","epsf")) + readData3DAll(foldertime+"pf_"+str(level)+".hdf5","pf")
                h_total = h_total + int3D(mask * h, dx, dy, dz)

                int_ekin = readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")
                print("ekin",int_ekin.shape)
                int_ekin = int_ekin * int_ekin
                int_ekin = int_ekin + readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")*readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")
                int_ekin = int_ekin + readData3DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")*readData3DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")
                int_ekin = int_ekin * readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
                int_ekin = detg * int_ekin * 0.5 * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                int_ekin = int3D(mask * int_ekin, dx, dy, dz)
                int_ekin_total = int_ekin_total + int_ekin
                
                b2 = readData3DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
                b2 = b2 * b2
                b2 = b2 + readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")*readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
                b2 = b2 + readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")*readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")
                b2 = b2 * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                b2_total = b2_total + int3D(mask * b2, dx, dy, dz)
                int_emag = detg * 0.5* b2
                int_emag = int3D(mask * int_emag, dx, dy, dz)
                int_emag_total = int_emag_total + int_emag

                int_eint = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")*readData3DAll(foldertime+"epsf_"+str(level)+".hdf5","epsf")
                W = readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")*readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")
                W = W + readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")*readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")
                W = W + readData3DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")*readData3DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")
                W = 1.0 / np.sqrt(np.maximum(1 - readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * W,0.1))                
                int_eint = detg * int_eint * W
                int_eint = int3D(mask * int_eint, dx, dy, dz)
                int_eint_total = int_eint_total + int_eint
                
                eos = pyr.load_eos_thermal("../external.eos.h5", units=pyr.units.geom_solar)
                int_eth = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
                ye = np.ndarray(int_eth.shape)
                ye[...] = 0.5
                eps_min_at_rho_ye = np.vectorize(lambda rhoi, yei : eos.range_eps(rhoi, yei).min)
                eps_cold = eps_min_at_rho_ye(int_eth, ye)
                int_eth = detg * int_eth * W * (readData3DAll(foldertime+"epsf_"+str(level)+".hdf5","epsf") - eps_cold)
                int_eth = int3D(mask * int_eth, dx, dy, dz)
                int_eth_total = int_eth_total + int_eth

                omega = -y * r2inv * (readData3DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x") - readData3DAll(foldertime+"Betau_x_"+str(level)+".hdf5","Betau_x"))
                omega = omega + x * r2inv * (readData3DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y") - readData3DAll(foldertime+"Betau_y_"+str(level)+".hdf5","Betau_y"))
                omega_total = omega_total + int3D(mask * omega, dx, dy, dz)
                Ttphi = h * W / readData3DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha")
                Ttphi = Ttphi * W * (-y * readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x") + x * readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y"))
                int_erot = detg * 0.5 * omega * Ttphi
                int_erot = int3D(mask * int_erot, dx, dy, dz)
                int_erot_total = int_erot_total + int_erot

                volume = readData3DAll(foldertime+"mask_"+str(level)+".hdf5","mask")
                volume = detg * np.logical_not(volume)
                volume = int3D(mask * volume,dx, dy, dz)
                volume_total = volume_total + volume

                volume = readData3DAll(foldertime+"mask_"+str(level)+".hdf5","mask")
                volume = np.logical_not(volume)
                volume = int3D(mask * volume,dx, dy, dz)
                volume_nodetg_total = volume_nodetg_total + volume
                                
                #Integral Dbar, which already contains the detg factor Dbar=D*detg
                int_D = readData3DAll(foldertime+"Df_"+str(level)+".hdf5","Df")
                int_D = int3D(mask * int_D, dx, dy, dz)
                int_D_total = int_D_total + int_D
                
                # Pm
                phi = np.arctan2(y,x)
                common = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof") * W * detg
                # m=0
                int_p0 = common
                int_p0 = int3D(mask * int_p0, dx, dy, dz)
                int_p0_total = int_p0_total + int_p0
                # m=1
                int_p1_r = common * np.cos(phi)
                int_p1_r = int3D(mask * int_p1_r, dx, dy, dz)
                int_p1_i = common * np.sin(phi)
                int_p1_i = int3D(mask * int_p1_i, dx, dy, dz)
                int_p1 = np.sqrt(int_p1_r**2 + int_p1_i**2)
                int_p1_total = int_p1_total + int_p1
                # m=2
                int_p2_r = common * np.cos(2*phi)
                int_p2_r = int3D(mask * int_p2_r, dx, dy, dz)
                int_p2_i = common * np.sin(2*phi)
                int_p2_i = int3D(mask * int_p2_i, dx, dy, dz)
                int_p2 = np.sqrt(int_p2_r**2 + int_p2_i**2)
                int_p2_total = int_p2_total + int_p2
                # m=3
                int_p3_r = common * np.cos(3*phi)
                int_p3_r = int3D(mask * int_p3_r, dx, dy, dz)
                int_p3_i = common * np.sin(3*phi)
                int_p3_i = int3D(mask * int_p3_i, dx, dy, dz)
                int_p3 = np.sqrt(int_p3_r**2 + int_p3_i**2)
                int_p3_total = int_p3_total + int_p3
                # m=4
                int_p4_r = common * np.cos(4*phi)
                int_p4_r = int3D(mask * int_p4_r, dx, dy, dz)
                int_p4_i = common * np.sin(4*phi)
                int_p4_i = int3D(mask * int_p4_i, dx, dy, dz)
                int_p4 = np.sqrt(int_p4_r**2 + int_p4_i**2)
                int_p4_total = int_p4_total + int_p4


                #Max rho
                max_rho = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
                max_rho = np.max(mask * max_rho)
                max_rho_total = np.maximum(max_rho_total, max_rho)

                # components of the magnetic field
                #Toroidal B
                int_bt = -y * readData3DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
                int_bt = int_bt + x * readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
                bt = int_bt * rinv * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                bt_total = bt_total + int3D(mask * bt, dx, dy, dz)
                int_bt = detg * np.sqrt(bt*bt)
                int_bt = int3D(mask * int_bt, dx, dy, dz)
                int_bt_total = int_bt_total + int_bt
                #Poloidal B
                #int_bp = detg * np.sqrt(np.maximum(b2-bt*bt, 0))
                int_bp = detg * np.sqrt(b2-bt*bt)
                int_bp = int3D(mask * int_bp, dx, dy, dz)
                int_bp_total = int_bp_total + int_bp
                # z-component B (for helicoidal)
                bz = readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")*readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                int_bz = detg * np.sqrt(bz * bz) 
                int_bz = int3D(mask * int_bz, dx, dy, dz)
                int_bz_total = int_bz_total + int_bz
                # total
                int_b = detg * np.sqrt(b2)
                int_b = int3D(mask * int_b, dx, dy, dz)
                int_b_total = int_b_total + int_b
                # ratio Bz/B   como quitamos los puntos donde b2 diverge?             
                int_rBzB = detg * np.sqrt(bz*bz/np.maximum(b2,1.0e-8))
                int_rBzB = int3D(mask * int_rBzB, dx, dy, dz)
                int_rBzB_total = int_rBzB_total + int_rBzB
                # ratio Bt/B                
                int_rBtB = detg * np.sqrt(bt*bt/np.maximum(b2,1.0e-8))
                int_rBtB = int3D(mask * int_rBtB, dx, dy, dz)
                int_rBtB_total = int_rBtB_total + int_rBtB

                # invbeta = B^2/(2*P)
                int_invbeta = detg * 0.5* b2 / np.maximum(readData3DAll(foldertime+"pf_"+str(level)+".hdf5","pf"), 1e-20)
                int_invbeta = int3D(mask * int_invbeta, dx, dy, dz)
                int_invbeta_total = int_invbeta_total + int_invbeta

                Bx = readData3DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
                By = readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
                Bz = readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")
                JBx = (np.roll(Bz, 1, axis=1) - np.roll(Bz, -1, axis=1)) / dy - (np.roll(By, 1, axis=2) - np.roll(By, -1, axis=2)) / dz
                JBy = (np.roll(Bx, 1, axis=2) - np.roll(Bx, -1, axis=2)) / dz - (np.roll(Bz, 1, axis=0) - np.roll(Bz, -1, axis=0)) / dx
                JBz = (np.roll(By, 1, axis=0) - np.roll(By, -1, axis=0)) / dx - (np.roll(Bx, 1, axis=1) - np.roll(Bx, -1, axis=1)) / dy
                int_hmag = detg * (JBx * Bx + JBy * By + JBz * Bz)
                int_hmag = int3D(mask * int_hmag, dx, dy, dz)
                int_hmag_total = int_hmag_total + int_hmag
                
                #Qz
                lambda_MRI_z = bz * 2 * np.pi/(np.sqrt(h + b2)*omega)
                lambda_MRI_z = np.nan_to_num(lambda_MRI_z, nan=0, posinf=0, neginf=0)
                int_lambda_MRI_z = int3D(detg * mask * lambda_MRI_z, dx, dy, dz)
                lambda_MRI_z_total = lambda_MRI_z_total + int_lambda_MRI_z
                int_lambda_MRI_z = int3D(mask * lambda_MRI_z, dx, dy, dz)
                lambda_MRI_z_nodetg_total = lambda_MRI_z_nodetg_total + int_lambda_MRI_z

                int_Qz = lambda_MRI_z
                int_Qz = detg * int_Qz / dx
                int_Qz = int3D(mask * int_Qz, dx, dy, dz)
                Qz_total = Qz_total + int_Qz

                #Qphi
                lambda_MRI_phi = bt * 2 * np.pi/(np.sqrt(h + b2)*omega)
                lambda_MRI_phi = np.nan_to_num(lambda_MRI_phi, nan=0, posinf=0, neginf=0)
                int_lambda_MRI_phi = int3D(detg * mask * lambda_MRI_phi, dx, dy, dz)
                lambda_MRI_phi_total = lambda_MRI_phi_total + int_lambda_MRI_phi
                int_lambda_MRI_phi = int3D(mask * lambda_MRI_phi, dx, dy, dz)
                lambda_MRI_phi_nodetg_total = lambda_MRI_phi_nodetg_total + int_lambda_MRI_phi

                int_Qphi = lambda_MRI_phi
                int_Qphi = detg * int_Qphi / dx
                int_Qphi = int3D(mask * int_Qphi, dx, dy, dz)
                Qphi_total = Qphi_total + int_Qphi

                Br = readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * rinv * (x * readData3DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x") + y * readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y"))
                #R
                int_R = Br * Br
                int_R = detg * int_R / (bt * bt)
                int_R = np.nan_to_num(int_R, nan=0, posinf=0, neginf=0)
                int_R = int3D(mask * int_R, dx, dy, dz)
                R_total = R_total + int_R

                #alpha_mag
                int_alpha_mag = -Br * bt
                int_alpha_mag = detg * int_alpha_mag / b2
                int_alpha_mag = np.nan_to_num(int_alpha_mag, nan=0, posinf=0, neginf=0)
                int_alpha_mag = int3D(mask * int_alpha_mag, dx, dy, dz)
                alpha_mag_total = alpha_mag_total + int_alpha_mag

            #BRMS
            BRMS  = int_b_total / volume_total            
            BRMST = int_bt_total / volume_total
            BRMSP = int_bp_total / volume_total
            BRMSZ = int_bz_total / volume_total
            BZB = int_rBzB_total / volume_total
            BTB = int_rBtB_total / volume_total
            invbeta = int_invbeta_total / volume_total
            Qz = Qz_total / volume_total
            Qphi = Qphi_total / volume_total
            R = R_total / volume_total
            alpha_mag = alpha_mag_total / volume_total
            lambda_MRI_z = lambda_MRI_z_total / volume_total
            lambda_MRI_phi = lambda_MRI_phi_total / volume_total
            lambda_MRI_z_nodetg = lambda_MRI_z_nodetg_total / volume_nodetg_total
            lambda_MRI_phi_nodetg = lambda_MRI_phi_nodetg_total / volume_nodetg_total

            bt = bt_total / volume_total
            h = h_total / volume_total
            b2 = b2_total / volume_total
            omega = omega_total / volume_total
            #Integral write
            f3.write("%12.3e %12.3e %12.3e %12.3e  %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e\n" % (time, int_ekin_total, int_erot_total, int_eint_total, int_emag_total, int_D_total, max_rho_total, int_hmag_total, int_p0_total, int_p1_total, int_p2_total, int_p3_total, int_p4_total, int_eth_total))
            f4.write("%12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e\n" % (time, BRMS, BRMST, BRMSP, BRMSZ, BZB, BTB, invbeta, Qz, Qphi, R, alpha_mag, lambda_MRI_z, lambda_MRI_phi, lambda_MRI_z_nodetg, lambda_MRI_phi_nodetg, bt, h, b2, omega))
            f5.write("%12.3e %12.3e %12.3e %12.3e\n" % (time, center_of_mass[0], center_of_mass[1], center_of_mass[2]))


    if not os.path.exists(folderout):
        os.makedirs(folderout)


    f3.close()
    f4.close()
    f5.close()

