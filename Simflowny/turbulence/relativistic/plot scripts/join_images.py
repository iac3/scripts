import numpy as np
import os
import math
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.ndimage.filters import gaussian_filter


fig = plt.figure(figsize=(8, 4))
#fig, axs = plt.subplots(4,4, sharex=True, sharey=True)

image = plt.imread("rhoB_ot3250_legend.png")

ax1 = fig.add_subplot(2,4,1)

ax1.imshow(image)
ax1.axes.xaxis.set_visible(False)
ax1.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot3500.png")

ax2 = fig.add_subplot(2,4,2)

ax2.imshow(image)
ax2.axes.xaxis.set_visible(False)
ax2.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot3600.png")

ax4 = fig.add_subplot(2,4,3)

ax4.imshow(image)
ax4.axes.xaxis.set_visible(False)
ax4.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot3700.png")

ax6 = fig.add_subplot(2,4,4)

ax6.imshow(image)
ax6.axes.xaxis.set_visible(False)
ax6.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot3850.png")

ax7 = fig.add_subplot(2,4,5)

ax7.imshow(image)
ax7.axes.xaxis.set_visible(False)
ax7.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot4250.png")

ax8 = fig.add_subplot(2,4,6)

ax8.imshow(image)
ax8.axes.xaxis.set_visible(False)
ax8.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot5200.png")

ax10 = fig.add_subplot(2,4,7)

ax10.imshow(image)
ax10.axes.xaxis.set_visible(False)
ax10.axes.yaxis.set_visible(False)

image = plt.imread("rhoB_ot6000.png")

ax12 = fig.add_subplot(2,4,8)

im2 = ax12.imshow(image)
ax12.axes.xaxis.set_visible(False)
ax12.axes.yaxis.set_visible(False)


plt.subplots_adjust(wspace=0.05, hspace=0.02)
plt.savefig("rhoB.png", bbox_inches='tight',dpi= 360)
plt.close()