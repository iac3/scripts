#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, getopt
import os
import numpy as np
import h5py
import scipy.ndimage as ndimage
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.pyplot as pl
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from pylab import *
mpl.rcParams['text.usetex']=True
mpl.rcParams['text.latex.unicode']=True

def readfield(folder, field, level):
    #Readig the variable to disk
    f= h5py.File(folder + "/" + field + '_' + str(level) + '.hdf5', "r")
    ds = f[field]
    data = np.empty(shape=ds.shape, dtype=ds.dtype)
    data[:] = ds[:]
    f.close()
    return data

#NEW COLOR
viridis   = cm.get_cmap('magma_r', 500)
newcolors = viridis(np.linspace(0, 0.8, 400))
white     = np.array([0, 0, 0, 0])
newcolors[:1, :] = white
newcmp1 = ListedColormap(newcolors)

viridis   = cm.get_cmap('magma_r', 500)
newcolors = viridis(np.linspace(0, 0.8, 400))
white     = np.array([0, 0, 0, 0])
newcolors[191, :] = white #:193
newcmp = ListedColormap(newcolors)

# viridis   = cm.get_cmap('gist_ncar_r', 500)
# newcolors = viridis(np.linspace(0, 0.95, 475))
# white     = np.array([0, 0, 0, 0])
# newcolors[:1, :] = white
# newcmp = ListedColormap(newcolors)

############################################################################################
############################## BBS C0.03 - Anti-C0.22  ##########################################
############################################################################################


level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003AC022/outputDir_slice0/visit_dump.59648"
dataa1 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003AC022/outputDir_slice0/visit_dump.63232"
dataa2 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003AC022/outputDir_slice0/visit_dump.66944"
dataa3 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003AC022/outputDir_slice0/visit_dump.109440"
dataa4 = readfield(folder, 'N_Noetheri', level)



############################################################################################
############################## BBS C0.03 - C0.22  ##########################################
############################################################################################


level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003C022/BosonStar_003_022/outputDir_slice0/visit_dump.59648"
data1 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003C022/BosonStar_003_022/outputDir_slice0/visit_dump.63232"
data2 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003C022/BosonStar_003_022/outputDir_slice0/visit_dump.66944"
data3 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C003C022/BosonStar_003_022/outputDir_slice0/visit_dump.109440"
data4 = readfield(folder, 'N_Noetheri', level)

############################################################################################
############################## BBS C0.06 - C0.22  ##########################################
############################################################################################

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C006C022/BosonStar_006_022/outputDir_slice0/visit_dump.36096"
data5 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C006C022/BosonStar_006_022/outputDir_slice0/visit_dump.40832"
data6 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C006C022/BosonStar_006_022/outputDir_slice0/visit_dump.45568"
data7 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C006C022/BosonStar_006_022/outputDir_slice0/visit_dump.135936"
data8 = readfield(folder, 'N_Noetheri', level)

############################################################################################
############################## BBS C0.12 - C0.22  ##########################################
############################################################################################

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C022/omg1.37_final_kappas/outputDir_slice0/visit_dump.24704"
data9 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C022/omg1.37_final_kappas/outputDir_slice0/visit_dump.29568"
data10 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C022/omg1.37_final_kappas/outputDir_slice0/visit_dump.34432"
data11 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C022/omg1.37_final_kappas/outputDir_slice0/visit_dump.80768"
data12 = readfield(folder, 'N_Noetheri', level)


############################################################################################
############################## BBS C0.12 - C0.18  ##########################################
############################################################################################

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C018/omg1.28/outputDir_slice0_sim1/visit_dump.48384"
data13 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C018/omg1.28/outputDir_slice0_sim1/visit_dump.52992"
data14 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C018/omg1.28/outputDir_slice0_sim1/visit_dump.59776"
data15 = readfield(folder, 'N_Noetheri', level)

level = 5
folder = "/home/miguel/Programs/papers/unequalbs/unequalbs_my/cineca/BS/C012C018/omg1.28/outputDir_slice0_sim1/visit_dump.87936"
data16 = readfield(folder, 'N_Noetheri', level)

props = dict(boxstyle='round', facecolor='white', alpha=0.5) # for the box

f, axarr = pl.subplots(5,4, sharex=False, sharey=False)

plt.subplots_adjust(left = 0.12, bottom = 0.11, right = 0.90, top = 0.88, wspace = 0.02, hspace=0.02)

for ax in axarr.flat:
#    ax.set_axis_off()
    im = ax.imshow(0*np.transpose(data16), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)

cbar = f.colorbar(im, ax=axarr.ravel().tolist(), shrink=1)
#cbar.set_label(r'$N$')
cbar.ax.set_title(r'$N$',fontsize=25)
cbar.ax.tick_params(labelsize=14)
#cbar.set_ticks(np.arange(0, 1.1, 0.5))
#cbar.set_ticklabels(['low', 'medium', 'high'])


axarr[0,0].imshow(np.transpose(dataa1), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[0,0].text(0.05, 0.92, r"$\rm{C}003-\rm{C}022\rm{A}$", transform=axarr[0,0].transAxes, fontsize=18, verticalalignment='top', bbox=props)
axarr[0,0].text(0.73, 0.2, r"$t_{c} - 45$", transform=axarr[0,0].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)
#axarr[0,0].set_xlim(-7.5, 7.5)
#axarr[0,0].set_ylim(-7.5, 7.5)
axarr[0,0].tick_params(labelsize=14)


axarr[0,1].imshow(np.transpose(dataa2), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[0,1].get_yaxis().set_visible(False)
axarr[0,1].text(0.9, 0.2, r"$t_{c}$", transform=axarr[0,1].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[0,2].imshow(np.transpose(dataa3), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[0,2].get_yaxis().set_visible(False)
axarr[0,2].text(0.73, 0.2, r"$t_{c} + 45$", transform=axarr[0,2].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[0,3].imshow(np.transpose(dataa4), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[0,3].get_yaxis().set_visible(False)
axarr[0,3].text(0.7, 0.2, r"$t_{c} + 578$", transform=axarr[0,3].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[1,0].imshow(np.transpose(data1), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[1,0].text(0.05, 0.92, r"$\rm{C}003-\rm{C}022$", transform=axarr[1,0].transAxes, fontsize=18, verticalalignment='top', bbox=props)
axarr[1,0].text(0.73, 0.2, r"$t_{c} - 45$", transform=axarr[1,0].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)
axarr[1,0].tick_params(labelsize=14)

axarr[1,1].imshow(np.transpose(data2), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[1,1].get_yaxis().set_visible(False)
axarr[1,1].text(0.9, 0.2, r"$t_{c}$", transform=axarr[1,1].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[1,2].imshow(np.transpose(data3), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[1,2].get_yaxis().set_visible(False)
axarr[1,2].text(0.73, 0.2, r"$t_{c} + 45$", transform=axarr[1,2].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[1,3].imshow(np.transpose(data4), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
axarr[1,3].get_yaxis().set_visible(False)
axarr[1,3].text(0.7, 0.2, r"$t_{c} + 578$", transform=axarr[1,3].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[2,0].imshow(np.transpose(data5), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01208, vmax=0.01208) #extent=(x.min, x.max, y.max, y.min)
axarr[2,0].text(0.05, 0.92, r"$\rm{C}006-\rm{C}022$", transform=axarr[2,0].transAxes, fontsize=18, verticalalignment='top', bbox=props)
axarr[2,0].tick_params(labelsize=14)
axarr[2,0].text(0.73, 0.2, r"$t_{c} - 59$", transform=axarr[2,0].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[2,1].imshow(np.transpose(data6), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01208, vmax=0.01208) #extent=(x.min, x.max, y.max, y.min)
axarr[2,1].get_yaxis().set_visible(False)
axarr[2,1].text(0.9, 0.2, r"$t_{c}$", transform=axarr[2,1].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[2,2].imshow(np.transpose(data7), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01208, vmax=0.01208) #extent=(x.min, x.max, y.max, y.min)
axarr[2,2].get_yaxis().set_visible(False)
axarr[2,2].text(0.73, 0.2, r"$t_{c} + 59$", transform=axarr[2,2].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[2,3].imshow(np.transpose(data8), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.01208, vmax=0.01208) #extent=(x.min, x.max, y.max, y.min)
axarr[2,3].get_yaxis().set_visible(False)
axarr[2,3].text(0.66, 0.2, r"$t_{c} + 1188$", transform=axarr[2,3].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[3,0].imshow(np.transpose(data9), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.013, vmax=0.013) #extent=(x.min, x.max, y.max, y.min)
axarr[3,0].text(0.05, 0.92, r"$\rm{C}012-\rm{C}022$", transform=axarr[3,0].transAxes, fontsize=18, verticalalignment='top', bbox=props)
axarr[3,0].tick_params(labelsize=14)
axarr[3,0].text(0.73, 0.2, r"$t_{c} - 61$", transform=axarr[3,0].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[3,1].imshow(np.transpose(data10), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.013, vmax=0.013) #extent=(x.min, x.max, y.max, y.min)
axarr[3,1].get_yaxis().set_visible(False)
axarr[3,1].text(0.9, 0.2, r"$t_{c}$", transform=axarr[3,1].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[3,2].imshow(np.transpose(data11), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.013, vmax=0.013) #extent=(x.min, x.max, y.max, y.min)
axarr[3,2].get_yaxis().set_visible(False)
axarr[3,2].text(0.73, 0.2, r"$t_{c} + 61$", transform=axarr[3,2].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[3,3].imshow(np.transpose(data12), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.013, vmax=0.013) #extent=(x.min, x.max, y.max, y.min)
axarr[3,3].get_yaxis().set_visible(False)
axarr[3,3].text(0.7, 0.2, r"$t_{c} + 640$", transform=axarr[3,3].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[4,0].imshow(np.transpose(data13), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.0106, vmax=0.0106) #extent=(x.min, x.max, y.max, y.min)
axarr[4,0].text(0.05, 0.92, r"$\rm{C}012-\rm{C}018$", transform=axarr[4,0].transAxes, fontsize=18, verticalalignment='top', bbox=props)
axarr[4,0].tick_params(labelsize=14)
axarr[4,0].text(0.73, 0.2, r"$t_{c} - 58$", transform=axarr[4,0].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[4,1].imshow(np.transpose(data14), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.0106, vmax=0.0106) #extent=(x.min, x.max, y.max, y.min)
axarr[4,1].get_yaxis().set_visible(False)
axarr[4,1].tick_params(labelsize=14)
axarr[4,1].text(0.9, 0.2, r"$t_{c}$", transform=axarr[4,1].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)

axarr[4,2].imshow(np.transpose(data15), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.0106, vmax=0.0106) #extent=(x.min, x.max, y.max, y.min)
axarr[4,2].get_yaxis().set_visible(False)
axarr[4,2].tick_params(labelsize=14)
axarr[4,2].text(0.73, 0.2, r"$t_{c} + 85$", transform=axarr[4,2].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)


axarr[4,3].imshow(np.transpose(data16), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu,vmin=-0.0106, vmax=0.0106) #extent=(x.min, x.max, y.max, y.min)
axarr[4,3].get_yaxis().set_visible(False)
axarr[4,3].tick_params(labelsize=14)
axarr[4,3].text(0.7, 0.2, r"$t_{c} + 437$", transform=axarr[4,3].transAxes, fontsize=18, verticalalignment='top')#, bbox=props)


plt.show()

# plt.figure(1)
# plt.imshow(np.transpose(dataa1), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu, vmin=-0.01241, vmax=0.01241) #extent=(x.min, x.max, y.max, y.min)
# #plt.clim(0.0,15.0)
# cb = plt.colorbar() # grab the Colorbar instance
# for t in cb.ax.get_yticklabels():
#      t.set_fontsize(20)
# plt.ylabel(r'$N_Noetheri_5$')
# #plt.savefig('N_Noetheri_5_1' + '.png')
# plt.show()

# plt.figure(2)
# plt.imshow(np.transpose(data1), extent=(-12.5, 12.5, -12.5, 12.5), interpolation='bilinear',origin="lower", aspect="auto", cmap=cm.RdBu, vmin=-0.0106, vmax=0.0106) #extent=(x.min, x.max, y.max, y.min)
# #plt.clim(0.0,15.0)
# cb = plt.colorbar() # grab the Colorbar instance
# for t in cb.ax.get_yticklabels():
#      t.set_fontsize(20)
# plt.ylabel(r'$N_Noetheri_5$')
# #plt.savefig('N_Noetheri_5_1' + '.png')
# plt.show()
