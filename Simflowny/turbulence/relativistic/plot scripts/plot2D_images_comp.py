import numpy as np
import os
import math
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

b2_factor = 1e20
rho_factor = 6.14e17
omega_factor = 1.0/4.94e-6

res = [160, 160]
folderout_b0 = "2048_B0/2D_plots/"
folderout = "2048/2D_plots/"
folderout_les = "2048_LES/2D_plots/"
box_init = 55.0 * 1.5


'''
for t in times:
    plot_colorbar = t == 13000.0
    plot_axisy = t == 4250.0


    #rho = np.load(folderout + "/rho_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", )
    #b2 = np.load(folderout + "/b2_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    invbeta = np.load(folderout + "/invbeta_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    #omega = np.load(folderout + "/omega_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    #vz = np.load(folderout + "/vz_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    #jb = np.load(folderout + "/jb_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    #plot2DOrigin(folderout + "/rho_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(rho*rho_factor), -box_init, plot_colorbar, False, plot_axisy, cmap="rainbow", vmin=8, vmax=15)
    #plot2DOrigin(folderout + "/b2_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(b2*B_factor), -box_init, plot_colorbar, False, plot_axisy, cmap="YlOrRd", vmin=12, vmax=17)
    #plot2DOrigin(folderout + "/omega_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", omega*omega_factor, -box_init, plot_colorbar, False, plot_axisy, cmap="rainbow", vmin=0, vmax=10000)
    plot2DOrigin(folderout + "/invbeta_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(invbeta), -box_init, plot_colorbar, False, plot_axisy, cmap="rainbow", vmin=-5, vmax=0)
    #plot2DOrigin(folderout + "/vz_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", vz, -box_init, plot_colorbar, True, plot_axisy, cmap="seismic", vmin=-0.1, vmax=0.1)
    #plot2DOrigin(folderout + "/jb_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", jb, -box_init, plot_colorbar, True, plot_axisy, cmap="seismic", vmin=-1e-10, vmax=1e-10)
'''



cmap_rho = "rainbow"
vmin_rho = 8
vmax_rho = 15


fig, axes = plt.subplots(3,2,figsize=(4, 6))

rho = np.load(folderout_b0 + "/rho_4250.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

axes[0,0].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[0,0].axes.xaxis.set_visible(False)
axes[0,0].axes.yaxis.set_visible(True)
axes[0,0].set_yticks([-60,-30, 0, 30, 60])
axes[0,0].set_ylabel("z [km]")

rho = np.load(folderout_b0 + "/rho_13000.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

im1 = axes[0,1].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[0,1].axes.xaxis.set_visible(False)
axes[0,1].axes.yaxis.set_visible(False)

rho = np.load(folderout + "/rho_4250.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

axes[1,0].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[1,0].axes.xaxis.set_visible(False)
axes[1,0].axes.yaxis.set_visible(True)
axes[1,0].set_yticks([-60,-30, 0, 30, 60])
axes[1,0].set_ylabel("z [km]")

rho = np.load(folderout + "/rho_13000.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

im2 = axes[1,1].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[1,1].axes.xaxis.set_visible(False)
axes[1,1].axes.yaxis.set_visible(False)

rho = np.load(folderout_les + "/rho_4250.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

axes[2,0].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[2,0].axes.xaxis.set_visible(True)
axes[2,0].axes.yaxis.set_visible(True)
axes[2,0].set_xticks([-60,-30, 0, 30, 60])
axes[2,0].set_yticks([-60,-30, 0, 30, 60])
axes[2,0].set_ylabel("z [km]")
axes[2,0].set_xlabel("x [km]")

rho = np.load(folderout_les + "/rho_13000.0_" + str(res[0]) + "x" + str(res[1]) + ".npy")
rho_plot = np.log10(rho*rho_factor)

im3 = axes[2,1].imshow(np.flip(rho_plot.T, axis=0), extent=[-box_init,box_init,-box_init,box_init], cmap=cmap_rho, vmin=vmin_rho, vmax=vmax_rho)
axes[2,1].axes.xaxis.set_visible(True)
axes[2,1].axes.yaxis.set_visible(False)
axes[2,1].set_xticks([-60,-30, 0, 30, 60])
axes[2,1].set_xlabel("x [km]")

cax = plt.axes([0.95, 0.15, 0.05, 0.7])
cbar = fig.colorbar(im3, cax=cax)
cax.set_xlabel(r"$log(\rho)$",fontsize='medium', labelpad=-320)
#cbar.set_label(r'', rotation=90)

plt.subplots_adjust(wspace=0.05, hspace=0.05)
plt.savefig("./" +  str(res[0]) + "x" + str(res[1]) + ".png", bbox_inches='tight')
plt.close()