#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as ticker
import math
import os
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20


def readHistogram(file):
	f = open(file)
	bins = []
	values = []
	first_bin = True
	for line in f.readlines():
		if "histo: Bin" in line:
			data = line[12:].split()
			if first_bin:
				bins.append(float(data[1]))
				first_bin = False
			bins.append(float(data[2]))
			values.append(float(data[3]))
	bin_avg = ((bins + np.roll(bins, 1))/2)[1:]
	return bin_avg, values

output_folder = 'Histograms/L4L60/'


if not os.path.exists(output_folder):
	os.makedirs(output_folder)


bins_b2_7250_1024_a15, data_b2_7250_1024_a15 = readHistogram("1024/Histograms/L4L60/histo_rad_b2_15_r60_928000")

bins_b2_7250_1024_LES_a15, data_b2_7250_1024_LES_a15 = readHistogram("1024_LES/Histograms/L4L60/histo_rad_b2_15_r60_896000")

bins_b2_4250_2048_a15, data_b2_4250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_544000")
bins_b2_5250_2048_a15, data_b2_5250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_672000")
bins_b2_7250_2048_a15, data_b2_7250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_928000")
bins_b2_9250_2048_a15, data_b2_9250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_1184000")
bins_b2_11250_2048_a15, data_b2_11250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_1440000")
bins_b2_13000_2048_a15, data_b2_13000_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_15_r60_1664000")

bins_b2_4250_2048_a30, data_b2_4250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_544000")
bins_b2_5250_2048_a30, data_b2_5250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_672000")
bins_b2_7250_2048_a30, data_b2_7250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_928000")
bins_b2_9250_2048_a30, data_b2_9250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_1184000")
bins_b2_11250_2048_a30, data_b2_11250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_1440000")
bins_b2_13000_2048_a30, data_b2_13000_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_30_r60_1664000")

bins_b2_4250_2048_a45, data_b2_4250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_544000")
bins_b2_5250_2048_a45, data_b2_5250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_672000")
bins_b2_7250_2048_a45, data_b2_7250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_928000")
bins_b2_9250_2048_a45, data_b2_9250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_1184000")
bins_b2_11250_2048_a45, data_b2_11250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_1440000")
bins_b2_13000_2048_a45, data_b2_13000_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_b2_45_r60_1664000")


bins_b2_4250_2048_B0_a15, data_b2_4250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_544000")
bins_b2_5250_2048_B0_a15, data_b2_5250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_672000")
bins_b2_7250_2048_B0_a15, data_b2_7250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_928000")
bins_b2_9250_2048_B0_a15, data_b2_9250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_1184000")
bins_b2_11250_2048_B0_a15, data_b2_11250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_1440000")
bins_b2_13000_2048_B0_a15, data_b2_13000_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_15_r60_1664000")

bins_b2_4250_2048_B0_a30, data_b2_4250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_544000")
bins_b2_5250_2048_B0_a30, data_b2_5250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_672000")
bins_b2_7250_2048_B0_a30, data_b2_7250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_928000")
bins_b2_9250_2048_B0_a30, data_b2_9250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_1184000")
bins_b2_11250_2048_B0_a30, data_b2_11250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_1440000")
bins_b2_13000_2048_B0_a30, data_b2_13000_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_30_r60_1664000")

bins_b2_4250_2048_B0_a45, data_b2_4250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_544000")
bins_b2_5250_2048_B0_a45, data_b2_5250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_672000")
bins_b2_7250_2048_B0_a45, data_b2_7250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_928000")
bins_b2_9250_2048_B0_a45, data_b2_9250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_1184000")
bins_b2_11250_2048_B0_a45, data_b2_11250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_1440000")
bins_b2_13000_2048_B0_a45, data_b2_13000_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_b2_45_r60_1664000")

bins_b2_4250_2048_LES_a15, data_b2_4250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_544000")
bins_b2_5250_2048_LES_a15, data_b2_5250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_672000")
bins_b2_7250_2048_LES_a15, data_b2_7250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_928000")
bins_b2_9250_2048_LES_a15, data_b2_9250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_1184000")
bins_b2_11250_2048_LES_a15, data_b2_11250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_1440000")
bins_b2_13000_2048_LES_a15, data_b2_13000_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_15_r60_1664000")

bins_b2_4250_2048_LES_a30, data_b2_4250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_544000")
bins_b2_5250_2048_LES_a30, data_b2_5250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_672000")
bins_b2_7250_2048_LES_a30, data_b2_7250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_928000")
bins_b2_9250_2048_LES_a30, data_b2_9250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_1184000")
bins_b2_11250_2048_LES_a30, data_b2_11250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_1440000")
bins_b2_13000_2048_LES_a30, data_b2_13000_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_30_r60_1664000")

bins_b2_4250_2048_LES_a45, data_b2_4250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_544000")
bins_b2_5250_2048_LES_a45, data_b2_5250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_672000")
bins_b2_7250_2048_LES_a45, data_b2_7250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_928000")
bins_b2_9250_2048_LES_a45, data_b2_9250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_1184000")
bins_b2_11250_2048_LES_a45, data_b2_11250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_1440000")
bins_b2_13000_2048_LES_a45, data_b2_13000_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_b2_45_r60_1664000")

bins_b2_7250_4096_a15, data_b2_7250_4096_a15 = readHistogram("4096/Histograms/L4L60/histo_rad_b2_15_r60_928000")

bins_b2_7250_4096_LES_a15, data_b2_7250_4096_LES_a15 = readHistogram("4096_LES/Histograms/L4L60/histo_rad_b2_15_r60_928000")


bins_D_7250_1024_a15, data_D_7250_1024_a15 = readHistogram("1024/Histograms/L4L60/histo_rad_D_15_r60_928000")

bins_D_7250_1024_LES_a15, data_D_7250_1024_LES_a15 = readHistogram("1024_LES/Histograms/L4L60/histo_rad_D_15_r60_896000")


bins_D_4250_2048_a15, data_D_4250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_544000")
bins_D_5250_2048_a15, data_D_5250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_672000")
bins_D_7250_2048_a15, data_D_7250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_928000")
bins_D_9250_2048_a15, data_D_9250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_1184000")
bins_D_11250_2048_a15, data_D_11250_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_1440000")
bins_D_13000_2048_a15, data_D_13000_2048_a15 = readHistogram("2048/Histograms/L4L60/histo_rad_D_15_r60_1664000")

bins_D_4250_2048_a30, data_D_4250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_544000")
bins_D_5250_2048_a30, data_D_5250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_672000")
bins_D_7250_2048_a30, data_D_7250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_928000")
bins_D_9250_2048_a30, data_D_9250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_1184000")
bins_D_11250_2048_a30, data_D_11250_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_1440000")
bins_D_13000_2048_a30, data_D_13000_2048_a30 = readHistogram("2048/Histograms/L4L60/histo_rad_D_30_r60_1664000")

bins_D_4250_2048_a45, data_D_4250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_544000")
bins_D_5250_2048_a45, data_D_5250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_672000")
bins_D_7250_2048_a45, data_D_7250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_928000")
bins_D_9250_2048_a45, data_D_9250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_1184000")
bins_D_11250_2048_a45, data_D_11250_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_1440000")
bins_D_13000_2048_a45, data_D_13000_2048_a45 = readHistogram("2048/Histograms/L4L60/histo_rad_D_45_r60_1664000")

bins_D_4250_2048_B0_a15, data_D_4250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_544000")
bins_D_5250_2048_B0_a15, data_D_5250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_672000")
bins_D_7250_2048_B0_a15, data_D_7250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_928000")
bins_D_9250_2048_B0_a15, data_D_9250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_1184000")
bins_D_11250_2048_B0_a15, data_D_11250_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_1440000")
bins_D_13000_2048_B0_a15, data_D_13000_2048_B0_a15 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_15_r60_1664000")

bins_D_4250_2048_B0_a30, data_D_4250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_544000")
bins_D_5250_2048_B0_a30, data_D_5250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_672000")
bins_D_7250_2048_B0_a30, data_D_7250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_928000")
bins_D_9250_2048_B0_a30, data_D_9250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_1184000")
bins_D_11250_2048_B0_a30, data_D_11250_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_1440000")
bins_D_13000_2048_B0_a30, data_D_13000_2048_B0_a30 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_30_r60_1664000")

bins_D_4250_2048_B0_a45, data_D_4250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_544000")
bins_D_5250_2048_B0_a45, data_D_5250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_672000")
bins_D_7250_2048_B0_a45, data_D_7250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_928000")
bins_D_9250_2048_B0_a45, data_D_9250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_1184000")
bins_D_11250_2048_B0_a45, data_D_11250_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_1440000")
bins_D_13000_2048_B0_a45, data_D_13000_2048_B0_a45 = readHistogram("2048_B0/Histograms/L4L60/histo_rad_D_45_r60_1664000")


bins_D_4250_2048_LES_a15, data_D_4250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_544000")
bins_D_5250_2048_LES_a15, data_D_5250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_672000")
bins_D_7250_2048_LES_a15, data_D_7250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_928000")
bins_D_9250_2048_LES_a15, data_D_9250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_1184000")
bins_D_11250_2048_LES_a15, data_D_11250_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_1440000")
bins_D_13000_2048_LES_a15, data_D_13000_2048_LES_a15 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_15_r60_1664000")

bins_D_4250_2048_LES_a30, data_D_4250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_544000")
bins_D_5250_2048_LES_a30, data_D_5250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_672000")
bins_D_7250_2048_LES_a30, data_D_7250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_928000")
bins_D_9250_2048_LES_a30, data_D_9250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_1184000")
bins_D_11250_2048_LES_a30, data_D_11250_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_1440000")
bins_D_13000_2048_LES_a30, data_D_13000_2048_LES_a30 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_30_r60_1664000")

bins_D_4250_2048_LES_a45, data_D_4250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_544000")
bins_D_5250_2048_LES_a45, data_D_5250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_672000")
bins_D_7250_2048_LES_a45, data_D_7250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_928000")
bins_D_9250_2048_LES_a45, data_D_9250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_1184000")
bins_D_11250_2048_LES_a45, data_D_11250_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_1440000")
bins_D_13000_2048_LES_a45, data_D_13000_2048_LES_a45 = readHistogram("2048_LES/Histograms/L4L60/histo_rad_D_45_r60_1664000")

bins_D_7250_4096_a15, data_D_7250_4096_a15 = readHistogram("4096/Histograms/L4L60/histo_rad_D_15_r60_928000")

bins_D_7250_4096_LES_a15, data_D_7250_4096_LES_a15 = readHistogram("4096_LES/Histograms/L4L60/histo_rad_D_15_r60_928000")

pl.figure()
pl.plot(bins_b2_7250_1024_a15, data_b2_7250_1024_a15, label=r'$LR$', linestyle=':', color='r')
pl.plot(bins_b2_7250_1024_LES_a15, data_b2_7250_1024_LES_a15, label=r'$LR_{LES}$', color='r')
pl.plot(bins_b2_7250_2048_a15, data_b2_7250_2048_a15, label=r'$MR$', linestyle=':', color='b')
pl.plot(bins_b2_7250_2048_B0_a15, data_b2_7250_2048_B0_a15, label=r'$MR_{B0}$', linestyle='--', color='b')
pl.plot(bins_b2_7250_2048_LES_a15, data_b2_7250_2048_LES_a15, label=r'$MR_{LES}$', color='b')
pl.plot(bins_b2_7250_4096_a15, data_b2_7250_4096_a15, label=r'$HR$', linestyle=':', color='k')
pl.plot(bins_b2_7250_4096_LES_a15, data_b2_7250_4096_LES_a15, label=r'$HR_{LES}$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_t7250_a15.pdf",  bbox_inches="tight")


pl.figure()
pl.plot(bins_D_7250_1024_a15, data_D_7250_1024_a15, label=r'$LR$', linestyle=':', color='r')
pl.plot(bins_D_7250_1024_LES_a15, data_D_7250_1024_LES_a15, label=r'$LR_{LES}$', color='r')
pl.plot(bins_D_7250_2048_a15, data_D_7250_2048_a15, label=r'$MR$', linestyle=':', color='b')
pl.plot(bins_D_7250_2048_B0_a15, data_D_7250_2048_B0_a15, label=r'$MR_{B0}$', linestyle='--', color='b')
pl.plot(bins_D_7250_2048_LES_a15, data_D_7250_2048_LES_a15, label=r'$MR_{LES}$', color='b')
pl.plot(bins_D_7250_4096_a15, data_D_7250_4096_a15, label=r'$HR$', linestyle=':', color='k')
pl.plot(bins_D_7250_4096_LES_a15, data_D_7250_4096_LES_a15, label=r'$HR_{LES}$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_t7250_a15.pdf",  bbox_inches="tight")



pl.figure()
pl.plot(bins_b2_4250_2048_a15, data_b2_4250_2048_a15, label=r'$MR_{15} 4250$', linestyle=':', color='b')
pl.plot(bins_b2_13000_2048_a15, data_b2_13000_2048_a15, label=r'$MR_{15} 13000$', color='b')
pl.plot(bins_b2_4250_2048_B0_a15, data_b2_4250_2048_B0_a15, label=r'$MR_{15}^{B0} 4250$', linestyle=':', color='r')
pl.plot(bins_b2_13000_2048_B0_a15, data_b2_13000_2048_B0_a15, label=r'$MR_{15}^{B0} 13000$', color='r')
pl.plot(bins_b2_4250_2048_LES_a15, data_b2_4250_2048_LES_a15, label=r'$MR_{15}^{LES} 4250$', linestyle=':', color='k')
pl.plot(bins_b2_13000_2048_LES_a15, data_b2_13000_2048_LES_a15, label=r'$MR_{15}^{LES} 13000$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_a15.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_b2_4250_2048_a30, data_b2_4250_2048_a30, label=r'$MR_{30} 4250$', linestyle=':', color='b')
pl.plot(bins_b2_13000_2048_a30, data_b2_13000_2048_a30, label=r'$MR_{30} 13000$', color='b')
pl.plot(bins_b2_4250_2048_B0_a30, data_b2_4250_2048_B0_a30, label=r'$MR_{30}^{B0} 4250$', linestyle=':', color='r')
pl.plot(bins_b2_13000_2048_B0_a30, data_b2_13000_2048_B0_a30, label=r'$MR_{30}^{B0} 13000$', color='r')
pl.plot(bins_b2_4250_2048_LES_a30, data_b2_4250_2048_LES_a30, label=r'$MR_{30}^{LES} 4250$', linestyle=':', color='k')
pl.plot(bins_b2_13000_2048_LES_a30, data_b2_13000_2048_LES_a30, label=r'$MR_{30}^{LES} 13000$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_a30.pdf",  bbox_inches="tight")


pl.figure()
pl.plot(bins_b2_4250_2048_a45, data_b2_4250_2048_a45, label=r'$MR_{45} 4250$', linestyle=':', color='b')
pl.plot(bins_b2_13000_2048_a45, data_b2_13000_2048_a45, label=r'$MR_{45} 13000$', color='b')
pl.plot(bins_b2_4250_2048_B0_a45, data_b2_4250_2048_B0_a45, label=r'$MR_{45}^{B0} 4250$', linestyle=':', color='r')
pl.plot(bins_b2_13000_2048_B0_a45, data_b2_13000_2048_B0_a45, label=r'$MR_{45}^{B0} 13000$', color='r')
pl.plot(bins_b2_4250_2048_LES_a45, data_b2_4250_2048_LES_a45, label=r'$MR_{45}^{LES} 4250$', linestyle=':', color='k')
pl.plot(bins_b2_13000_2048_LES_a45, data_b2_13000_2048_LES_a45, label=r'$MR_{45}^{LES} 13000$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_a45.pdf",  bbox_inches="tight")


f, ax = pl.subplots()
ax.plot(bins_D_4250_2048_a15, data_D_4250_2048_a15, label=r'MR t=5ms', linestyle=':', color='b')
ax.plot(bins_D_4250_2048_B0_a15, data_D_4250_2048_B0_a15, label=r'MR B0 t=5ms', linestyle=':', color='r')
ax.plot(bins_D_4250_2048_LES_a15, data_D_4250_2048_LES_a15, label=r'MR LES t=5ms', linestyle=':', color='k')
ax.plot(bins_D_13000_2048_a15, data_D_13000_2048_a15, label=r'MR t=50ms', color='b')
ax.plot(bins_D_13000_2048_B0_a15, data_D_13000_2048_B0_a15, label=r'MR B0 t=50ms', color='r')
ax.plot(bins_D_13000_2048_LES_a15, data_D_13000_2048_LES_a15, label=r'MR LES t=50ms', color='k')
ax.legend(fontsize=14)
ax.tick_params(labelsize=14)
ax.set_yscale('log')
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_xlabel(r'$R[km]$',fontsize=20)
ax.set_ylabel(r'$\frac{dM}{dR}$',fontsize=20)
ax.set_xlim([0.5, 81/1.5])
ax.set_xticks([20.0/1.5, 40.0/1.5, 60.0/1.5, 80.0/1.5])
pl.savefig(output_folder + "/Hist_D_MR_a15.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_D_4250_2048_a30, data_D_4250_2048_a30, label=r'$MR_{30} 4250$', linestyle=':', color='b')
pl.plot(bins_D_13000_2048_a30, data_D_13000_2048_a30, label=r'$MR_{30} 13000$', color='b')
pl.plot(bins_D_4250_2048_B0_a30, data_D_4250_2048_B0_a30, label=r'$MR_{30}^{B0} 4250$', linestyle=':', color='r')
pl.plot(bins_D_13000_2048_B0_a30, data_D_13000_2048_B0_a30, label=r'$MR_{30}^{B0} 13000$', color='r')
pl.plot(bins_D_4250_2048_LES_a30, data_D_4250_2048_LES_a30, label=r'$MR_{30}^{LES} 4250$', linestyle=':', color='k')
pl.plot(bins_D_13000_2048_LES_a30, data_D_13000_2048_LES_a30, label=r'$MR_{30}^{LES} 13000$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_MR_a30.pdf",  bbox_inches="tight")


pl.figure()
pl.plot(bins_D_4250_2048_a45, data_D_4250_2048_a45, label=r'$MR_{45} 4250$', linestyle=':', color='b')
pl.plot(bins_D_13000_2048_a45, data_D_13000_2048_a45, label=r'$MR_{45} 13000$', color='b')
pl.plot(bins_D_4250_2048_B0_a45, data_D_4250_2048_B0_a45, label=r'$MR_{45}^{B0} 4250$', linestyle=':', color='r')
pl.plot(bins_D_13000_2048_B0_a45, data_D_13000_2048_B0_a45, label=r'$MR_{45}^{B0} 13000$', color='r')
pl.plot(bins_D_4250_2048_LES_a45, data_D_4250_2048_LES_a45, label=r'$MR_{45}^{LES} 4250$', linestyle=':', color='k')
pl.plot(bins_D_13000_2048_LES_a45, data_D_13000_2048_LES_a45, label=r'$MR_{45}^{LES} 13000$', color='k')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_MR_a45.pdf",  bbox_inches="tight")



pl.figure()
pl.plot(bins_b2_4250_2048_LES_a15, data_b2_4250_2048_LES_a15, label=r'$4250$')
pl.plot(bins_b2_5250_2048_LES_a15, data_b2_5250_2048_LES_a15, label=r'$5250$')
pl.plot(bins_b2_7250_2048_LES_a15, data_b2_7250_2048_LES_a15, label=r'$7250$')
pl.plot(bins_b2_9250_2048_LES_a15, data_b2_9250_2048_LES_a15, label=r'$9250$')
pl.plot(bins_b2_11250_2048_LES_a15, data_b2_11250_2048_LES_a15, label=r'$11250$')
pl.plot(bins_b2_13000_2048_LES_a15, data_b2_13000_2048_LES_a15, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_LES_a15_Times.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_b2_4250_2048_LES_a30, data_b2_4250_2048_LES_a30, label=r'$4250$')
pl.plot(bins_b2_5250_2048_LES_a30, data_b2_5250_2048_LES_a30, label=r'$5250$')
pl.plot(bins_b2_7250_2048_LES_a30, data_b2_7250_2048_LES_a30, label=r'$7250$')
pl.plot(bins_b2_9250_2048_LES_a30, data_b2_9250_2048_LES_a30, label=r'$9250$')
pl.plot(bins_b2_11250_2048_LES_a30, data_b2_11250_2048_LES_a30, label=r'$11250$')
pl.plot(bins_b2_13000_2048_LES_a30, data_b2_13000_2048_LES_a30, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_LES_a30_Times.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_b2_4250_2048_LES_a45, data_b2_4250_2048_LES_a45, label=r'$4250$')
pl.plot(bins_b2_5250_2048_LES_a45, data_b2_5250_2048_LES_a45, label=r'$5250$')
pl.plot(bins_b2_7250_2048_LES_a45, data_b2_7250_2048_LES_a45, label=r'$7250$')
pl.plot(bins_b2_9250_2048_LES_a45, data_b2_9250_2048_LES_a45, label=r'$9250$')
pl.plot(bins_b2_11250_2048_LES_a45, data_b2_11250_2048_LES_a45, label=r'$11250$')
pl.plot(bins_b2_13000_2048_LES_a45, data_b2_13000_2048_LES_a45, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$b^2$',fontsize=20)
pl.savefig(output_folder + "/Hist_b2_MR_LES_a45_Times.pdf",  bbox_inches="tight")



pl.figure()
pl.plot(bins_D_4250_2048_LES_a15, data_D_4250_2048_LES_a15, label=r'$4250$')
pl.plot(bins_D_5250_2048_LES_a15, data_D_5250_2048_LES_a15, label=r'$5250$')
pl.plot(bins_D_7250_2048_LES_a15, data_D_7250_2048_LES_a15, label=r'$7250$')
pl.plot(bins_D_9250_2048_LES_a15, data_D_9250_2048_LES_a15, label=r'$9250$')
pl.plot(bins_D_11250_2048_LES_a15, data_D_11250_2048_LES_a15, label=r'$11250$')
pl.plot(bins_D_13000_2048_LES_a15, data_D_13000_2048_LES_a15, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_MR_LES_a15_Times.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_D_4250_2048_LES_a30, data_D_4250_2048_LES_a30, label=r'$4250$')
pl.plot(bins_D_5250_2048_LES_a30, data_D_5250_2048_LES_a30, label=r'$5250$')
pl.plot(bins_D_7250_2048_LES_a30, data_D_7250_2048_LES_a30, label=r'$7250$')
pl.plot(bins_D_9250_2048_LES_a30, data_D_9250_2048_LES_a30, label=r'$9250$')
pl.plot(bins_D_11250_2048_LES_a30, data_D_11250_2048_LES_a30, label=r'$11250$')
pl.plot(bins_D_13000_2048_LES_a30, data_D_13000_2048_LES_a30, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_MR_LES_a30_Times.pdf",  bbox_inches="tight")

pl.figure()
pl.plot(bins_D_4250_2048_LES_a45, data_D_4250_2048_LES_a45, label=r'$4250$')
pl.plot(bins_D_5250_2048_LES_a45, data_D_5250_2048_LES_a45, label=r'$5250$')
pl.plot(bins_D_7250_2048_LES_a45, data_D_7250_2048_LES_a45, label=r'$7250$')
pl.plot(bins_D_9250_2048_LES_a45, data_D_9250_2048_LES_a45, label=r'$9250$')
pl.plot(bins_D_11250_2048_LES_a45, data_D_11250_2048_LES_a45, label=r'$11250$')
pl.plot(bins_D_13000_2048_LES_a45, data_D_13000_2048_LES_a45, label=r'$13000$')
pl.legend()
pl.yscale('log')
pl.xlabel(r'$r$',fontsize=20)
pl.ylabel(r'$D$',fontsize=20)
pl.savefig(output_folder + "/Hist_D_MR_LES_a45_Times.pdf",  bbox_inches="tight")
