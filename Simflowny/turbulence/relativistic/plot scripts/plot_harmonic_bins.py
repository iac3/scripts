#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20

time = "7250.0"


B_factor = 1e20
E_factor = 1.8e54

folder = "2048_LES/Harmonics/" 
bins = ["1..10..1", "11..20..1", "21..30..1", "31..40..1", "41..50..1"]



for b in bins:


	c00 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c00.dat")
	t_c00 = c00[:,0]
	v_c00 = c00[:,1]**2 + c00[:,2]**2
	c10 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c10.dat")
	t_c10 = c10[:,0]
	v_c10 = c10[:,1]**2 + c10[:,2]**2
	c20 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c20.dat")
	t_c20 = c20[:,0]
	v_c20 = c20[:,1]**2 + c20[:,2]**2
	c30 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c30.dat")
	t_c30 = c30[:,0]
	v_c30 = c30[:,1]**2 + c30[:,2]**2
	c40 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c40.dat")
	t_c40 = c40[:,0]
	v_c40 = c40[:,1]**2 + c40[:,2]**2
	c50 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c50.dat")
	t_c50 = c50[:,0]
	v_c50 = c50[:,1]**2 + c50[:,2]**2
	c60 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c60.dat")
	t_c60 = c60[:,0]
	v_c60 = c60[:,1]**2 + c60[:,2]**2
	c70 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c70.dat")
	t_c70 = c70[:,0]
	v_c70 = c70[:,1]**2 + c70[:,2]**2
	c80 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c80.dat")
	t_c80 = c80[:,0]
	v_c80 = c80[:,1]**2 + c80[:,2]**2

	c1p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c1p1.dat")
	t_c1p1 = c1p1[:,0]
	v_c1p1 = c1p1[:,1]**2 + c1p1[:,2]**2
	c2p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c2p1.dat")
	t_c2p1 = c2p1[:,0]
	v_c2p1 = c2p1[:,1]**2 + c2p1[:,2]**2
	c3p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3p1.dat")
	t_c3p1 = c3p1[:,0]
	v_c3p1 = c3p1[:,1]**2 + c3p1[:,2]**2
	c4p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4p1.dat")
	t_c4p1 = c4p1[:,0]
	v_c4p1 = c4p1[:,1]**2 + c4p1[:,2]**2
	c5p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5p1.dat")
	t_c5p1 = c5p1[:,0]
	v_c5p1 = c5p1[:,1]**2 + c5p1[:,2]**2
	c6p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p1.dat")
	t_c6p1 = c6p1[:,0]
	v_c6p1 = c6p1[:,1]**2 + c6p1[:,2]**2
	c7p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p1.dat")
	t_c7p1 = c7p1[:,0]
	v_c7p1 = c7p1[:,1]**2 + c7p1[:,2]**2
	c8p1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p1.dat")
	t_c8p1 = c8p1[:,0]
	v_c8p1 = c8p1[:,1]**2 + c8p1[:,2]**2

	c1m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c1m1.dat")
	t_c1m1 = c1m1[:,0]
	v_c1m1 = c1m1[:,1]**2 + c1m1[:,2]**2
	c2m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c2m1.dat")
	t_c2m1 = c2m1[:,0]
	v_c2m1 = c2m1[:,1]**2 + c2m1[:,2]**2
	c3m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3m1.dat")
	t_c3m1 = c3m1[:,0]
	v_c3m1 = c3m1[:,1]**2 + c3m1[:,2]**2
	c4m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4m1.dat")
	t_c4m1 = c4m1[:,0]
	v_c4m1 = c4m1[:,1]**2 + c4m1[:,2]**2
	c5m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5m1.dat")
	t_c5m1 = c5m1[:,0]
	v_c5m1 = c5m1[:,1]**2 + c5m1[:,2]**2
	c6m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m1.dat")
	t_c6m1 = c6m1[:,0]
	v_c6m1 = c6m1[:,1]**2 + c6m1[:,2]**2
	c7m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m1.dat")
	t_c7m1 = c7m1[:,0]
	v_c7m1 = c7m1[:,1]**2 + c7m1[:,2]**2
	c8m1 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m1.dat")
	t_c8m1 = c8m1[:,0]
	v_c8m1 = c8m1[:,1]**2 + c8m1[:,2]**2

	c2p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c2p2.dat")
	t_c2p2 = c2p2[:,0]
	v_c2p2 = c2p2[:,1]**2 + c2p2[:,2]**2
	c3p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3p2.dat")
	t_c3p2 = c3p2[:,0]
	v_c3p2 = c3p2[:,1]**2 + c3p2[:,2]**2
	c4p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4p2.dat")
	t_c4p2 = c4p2[:,0]
	v_c4p2 = c4p2[:,1]**2 + c4p2[:,2]**2
	c5p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5p2.dat")
	t_c5p2 = c5p2[:,0]
	v_c5p2 = c5p2[:,1]**2 + c5p2[:,2]**2
	c6p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p2.dat")
	t_c6p2 = c6p2[:,0]
	v_c6p2 = c6p2[:,1]**2 + c6p2[:,2]**2
	c7p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p2.dat")
	t_c7p2 = c7p2[:,0]
	v_c7p2 = c7p2[:,1]**2 + c7p2[:,2]**2
	c8p2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p2.dat")
	t_c8p2 = c8p2[:,0]
	v_c8p2 = c8p2[:,1]**2 + c8p2[:,2]**2

	c2m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c2m2.dat")
	t_c2m2 = c2m2[:,0]
	v_c2m2 = c2m2[:,1]**2 + c2m2[:,2]**2
	c3m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3m2.dat")
	t_c3m2 = c3m2[:,0]
	v_c3m2 = c3m2[:,1]**2 + c3m2[:,2]**2
	c4m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4m2.dat")
	t_c4m2 = c4m2[:,0]
	v_c4m2 = c4m2[:,1]**2 + c4m2[:,2]**2
	c5m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5m2.dat")
	t_c5m2 = c5m2[:,0]
	v_c5m2 = c5m2[:,1]**2 + c5m2[:,2]**2
	c6m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m2.dat")
	t_c6m2 = c6m2[:,0]
	v_c6m2 = c6m2[:,1]**2 + c6m2[:,2]**2
	c7m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m2.dat")
	t_c7m2 = c7m2[:,0]
	v_c7m2 = c7m2[:,1]**2 + c7m2[:,2]**2
	c8m2 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m2.dat")
	t_c8m2 = c8m2[:,0]
	v_c8m2 = c8m2[:,1]**2 + c8m2[:,2]**2

	c3p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3p3.dat")
	t_c3p3 = c3p3[:,0]
	v_c3p3 = c3p3[:,1]**2 + c3p3[:,2]**2
	c4p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4p3.dat")
	t_c4p3 = c4p3[:,0]
	v_c4p3 = c4p3[:,1]**2 + c4p3[:,2]**2
	c5p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5p3.dat")
	t_c5p3 = c5p3[:,0]
	v_c5p3 = c5p3[:,1]**2 + c5p3[:,2]**2
	c6p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p3.dat")
	t_c6p3 = c6p3[:,0]
	v_c6p3 = c6p3[:,1]**2 + c6p3[:,2]**2
	c7p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p3.dat")
	t_c7p3 = c7p3[:,0]
	v_c7p3 = c7p3[:,1]**2 + c7p3[:,2]**2
	c8p3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p3.dat")
	t_c8p3 = c8p3[:,0]
	v_c8p3 = c8p3[:,1]**2 + c8p3[:,2]**2

	c3m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c3m3.dat")
	t_c3m3 = c3m3[:,0]
	v_c3m3 = c3m3[:,1]**2 + c3m3[:,2]**2
	c4m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4m3.dat")
	t_c4m3 = c4m3[:,0]
	v_c4m3 = c4m3[:,1]**2 + c4m3[:,2]**2
	c5m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5m3.dat")
	t_c5m3 = c5m3[:,0]
	v_c5m3 = c5m3[:,1]**2 + c5m3[:,2]**2
	c6m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m3.dat")
	t_c6m3 = c6m3[:,0]
	v_c6m3 = c6m3[:,1]**2 + c6m3[:,2]**2
	c7m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m3.dat")
	t_c7m3 = c7m3[:,0]
	v_c7m3 = c7m3[:,1]**2 + c7m3[:,2]**2
	c8m3 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m3.dat")
	t_c8m3 = c8m3[:,0]
	v_c8m3 = c8m3[:,1]**2 + c8m3[:,2]**2

	c4p4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4p4.dat")
	t_c4p4 = c4p4[:,0]
	v_c4p4 = c4p4[:,1]**2 + c4p4[:,2]**2
	c5p4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5p4.dat")
	t_c5p4 = c5p4[:,0]
	v_c5p4 = c5p4[:,1]**2 + c5p4[:,2]**2
	c6p4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p4.dat")
	t_c6p4 = c6p4[:,0]
	v_c6p4 = c6p4[:,1]**2 + c6p4[:,2]**2
	c7p4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p4.dat")
	t_c7p4 = c7p4[:,0]
	v_c7p4 = c7p4[:,1]**2 + c7p4[:,2]**2
	c8p4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p4.dat")
	t_c8p4 = c8p4[:,0]
	v_c8p4 = c8p4[:,1]**2 + c8p4[:,2]**2

	c4m4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c4m4.dat")
	t_c4m4 = c4m4[:,0]
	v_c4m4 = c4m4[:,1]**2 + c4m4[:,2]**2
	c5m4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5m4.dat")
	t_c5m4 = c5m4[:,0]
	v_c5m4 = c5m4[:,1]**2 + c5m4[:,2]**2
	c6m4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m4.dat")
	t_c6m4 = c6m4[:,0]
	v_c6m4 = c6m4[:,1]**2 + c6m4[:,2]**2
	c7m4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m4.dat")
	t_c7m4 = c7m4[:,0]
	v_c7m4 = c7m4[:,1]**2 + c7m4[:,2]**2
	c8m4 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m4.dat")
	t_c8m4 = c8m4[:,0]
	v_c8m4 = c8m4[:,1]**2 + c8m4[:,2]**2

	c5p5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5p5.dat")
	t_c5p5 = c5p5[:,0]
	v_c5p5 = c5p5[:,1]**2 + c5p5[:,2]**2
	c6p5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p5.dat")
	t_c6p5 = c6p5[:,0]
	v_c6p5 = c6p5[:,1]**2 + c6p5[:,2]**2
	c7p5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p5.dat")
	t_c7p5 = c7p5[:,0]
	v_c7p5 = c7p5[:,1]**2 + c7p5[:,2]**2
	c8p5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p5.dat")
	t_c8p5 = c8p5[:,0]
	v_c8p5 = c8p5[:,1]**2 + c8p5[:,2]**2

	c5m5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c5m5.dat")
	t_c5m5 = c5m5[:,0]
	v_c5m5 = c5m5[:,1]**2 + c5m5[:,2]**2
	c6m5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m5.dat")
	t_c6m5 = c6m5[:,0]
	v_c6m5 = c6m5[:,1]**2 + c6m5[:,2]**2
	c7m5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m5.dat")
	t_c7m5 = c7m5[:,0]
	v_c7m5 = c7m5[:,1]**2 + c7m5[:,2]**2
	c8m5 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m5.dat")
	t_c8m5 = c8m5[:,0]
	v_c8m5 = c8m5[:,1]**2 + c8m5[:,2]**2

	c6p6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6p6.dat")
	t_c6p6 = c6p6[:,0]
	v_c6p6 = c6p6[:,1]**2 + c6p6[:,2]**2
	c7p6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p6.dat")
	t_c7p6 = c7p6[:,0]
	v_c7p6 = c7p6[:,1]**2 + c7p6[:,2]**2
	c8p6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p6.dat")
	t_c8p6 = c8p6[:,0]
	v_c8p6 = c8p6[:,1]**2 + c8p6[:,2]**2

	c6m6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c6m6.dat")
	t_c6m6 = c6m6[:,0]
	v_c6m6 = c6m6[:,1]**2 + c6m6[:,2]**2
	c7m6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m6.dat")
	t_c7m6 = c7m6[:,0]
	v_c7m6 = c7m6[:,1]**2 + c7m6[:,2]**2
	c8m6 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m6.dat")
	t_c8m6 = c8m6[:,0]
	v_c8m6 = c8m6[:,1]**2 + c8m6[:,2]**2

	c7p7 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7p7.dat")
	t_c7p7 = c7p7[:,0]
	v_c7p7 = c7p7[:,1]**2 + c7p7[:,2]**2
	c8p7 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p7.dat")
	t_c8p7 = c8p7[:,0]
	v_c8p7 = c8p7[:,1]**2 + c8p7[:,2]**2

	c7m7 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c7m7.dat")
	t_c7m7 = c7m7[:,0]
	v_c7m7 = c7m7[:,1]**2 + c7m7[:,2]**2
	c8m7 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m7.dat")
	t_c8m7 = c8m7[:,0]
	v_c8m7 = c8m7[:,1]**2 + c8m7[:,2]**2

	c8p8 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8p8.dat")
	t_c8p8 = c8p8[:,0]
	v_c8p8 = c8p8[:,1]**2 + c8p8[:,2]**2

	c8m8 = np.genfromtxt(fname= folder + "/radisu_avg_" + b + "/s0_c8m8.dat")
	t_c8m8 = c8m8[:,0]
	v_c8m8 = c8m8[:,1]**2 + c8m8[:,2]**2


	norm = (v_c00 + v_c1m1 + v_c10 + v_c1p1 + v_c2m2 + v_c2m1 + v_c20 + v_c2p1 + v_c2p2 + v_c3m3 + v_c3m2 + v_c3m1 + v_c30 + v_c3p1 + v_c3p2 + v_c3p3 + v_c4m4 + v_c4m3 + v_c4m2 + v_c4m1 + v_c40 + v_c4p1 + v_c4p2 + v_c4p3 + v_c4p4 + v_c5m5 + v_c5m4 + v_c5m3 + v_c5m2 + v_c5m1 + v_c50 + v_c5p1 + v_c5p2 + v_c5p3 + v_c5p4 + v_c5p5 + v_c6m6 + v_c6m5 + v_c6m4 + v_c6m3 + v_c6m2 + v_c6m1 + v_c60 + v_c6p1 + v_c6p2 + v_c6p3 + v_c6p4 + v_c6p5 + v_c6p6 + v_c7m7 + v_c7m6 + v_c7m5 + v_c7m4 + v_c7m3 + v_c7m2 + v_c7m1 + v_c70 + v_c7p1 + v_c7p2 + v_c7p3 + v_c7p4 + v_c7p5 + v_c7p6 + v_c7p7 + v_c8m8 + v_c8m7 + v_c8m6 + v_c8m5 + v_c8m4 + v_c8m3 + v_c8m2 + v_c8m1 + v_c80 + v_c8p1 + v_c8p2 + v_c8p3 + v_c8p4 + v_c8p5 + v_c8p6 + v_c8p7 + v_c8p8)


	v_m0 = v_c00 + v_c1m1 + v_c1p1 + v_c2m1 + v_c2p1 + v_c3m1 + v_c3p1 + v_c4m1 + v_c4p1 + v_c5m1 + v_c5p1 + v_c6m1 + v_c6p1 + v_c7m1 + v_c7p1 + v_c8m1 + v_c8p1
	v_m1 = v_c1m1 + v_c1p1 + v_c2m1 + v_c2p1 + v_c3m1 + v_c3p1 + v_c4m1 + v_c4p1 + v_c5m1 + v_c5p1 + v_c6m1 + v_c6p1 + v_c7m1 + v_c7p1 + v_c8m1 + v_c8p1
	v_m2 = v_c2m2 + v_c2p2 + v_c3m2 + v_c3p2 + v_c4m2 + v_c4p2 + v_c5m2 + v_c5p2 + v_c6m2 + v_c6p2 + v_c7m2 + v_c7p2 + v_c8m2 + v_c8p2
	v_m3 = v_c3m3 + v_c3p3 + v_c4m3 + v_c4p3 + v_c5m3 + v_c5p3 + v_c6m3 + v_c6p3 + v_c7m3 + v_c7p3 + v_c8m3 + v_c8p3
	v_m4 = v_c4m4 + v_c4p4 + v_c5m4 + v_c5p4 + v_c6m4 + v_c6p4 + v_c7m4 + v_c7p4 + v_c8m4 + v_c8p4
	v_m5 = v_c5m5 + v_c5p5 + v_c6m5 + v_c6p5 + v_c7m5 + v_c7p5 + v_c8m5 + v_c8p5
	v_m6 = v_c6m6 + v_c6p6 + v_c7m6 + v_c7p6 + v_c8m6 + v_c8p6
	v_m7 = v_c7m7 + v_c7p7 + v_c8m7 + v_c8p7
	v_m8 = v_c8m8 + v_c8p8


	v_m0 = v_m0 / norm
	v_m1 = v_m1 / norm
	v_m2 = v_m2 / norm
	v_m3 = v_m3 / norm
	v_m4 = v_m4 / norm
	v_m5 = v_m5 / norm
	v_m6 = v_m6 / norm
	v_m7 = v_m7 / norm
	v_m8 = v_m8 / norm

	plt.figure()
	plt.plot(t_c00, v_m0, label=r'$C_{m=0}$', color='hotpink', linestyle=':')
	plt.plot(t_c00, v_m1, label=r'$C_{m=1}$', color='b', linestyle=':')
	plt.plot(t_c00, v_m2, label=r'$C_{m=2}$', color='r', linestyle=':')
	plt.plot(t_c00, v_m3, label=r'$C_{m=3}$', color='g', linestyle=':')
	plt.plot(t_c00, v_m4, label=r'$C_{m=4}$', color='c', linestyle=':')
	plt.plot(t_c00, v_m5, label=r'$C_{m=5}$', color='m', linestyle=':')
	plt.plot(t_c00, v_m6, label=r'$C_{m=6}$', color='y', linestyle=':')
	plt.plot(t_c00, v_m7, label=r'$C_{m=7}$', color='orange', linestyle=':')
	plt.plot(t_c00, v_m8, label=r'$C_{m=8}$', color='lime', linestyle=':')

	plt.yscale("log")
	plt.legend()
	#plt.ylim([1e13, 2e16]) 
	plt.xlabel(r'$t$',fontsize=20)
	pl.savefig("Harmonics_bin_" + b + "_m.pdf",  bbox_inches="tight")


	v_l0 = v_c00
	v_l1 = v_c1m1 + v_c10 + v_c1p1
	v_l2 = v_c2m2 + v_c2m1 + v_c20 + v_c2p1 + v_c2p2
	v_l3 = v_c3m3 + v_c3m2 + v_c3m1 + v_c30 + v_c3p1 + v_c3p2 + v_c3p3
	v_l4 = v_c4m4 + v_c4m3 + v_c4m2 + v_c4m1 + v_c40 + v_c4p1 + v_c4p2 + v_c4p3 + v_c4p4
	v_l5 = v_c5m5 + v_c5m4 + v_c5m3 + v_c5m2 + v_c5m1 + v_c50 + v_c5p1 + v_c5p2 + v_c5p3 + v_c5p4 + v_c5p5
	v_l6 = v_c6m6 + v_c6m5 + v_c6m4 + v_c6m3 + v_c6m2 + v_c6m1 + v_c60 + v_c6p1 + v_c6p2 + v_c6p3 + v_c6p4 + v_c6p5 + v_c6p6
	v_l7 = v_c7m7 + v_c7m6 + v_c7m5 + v_c7m4 + v_c7m3 + v_c7m2 + v_c7m1 + v_c70 + v_c7p1 + v_c7p2 + v_c7p3 + v_c7p4 + v_c7p5 + v_c7p6 + v_c7p7
	v_l8 = v_c8m8 + v_c8m7 + v_c8m6 + v_c8m5 + v_c8m4 + v_c8m3 + v_c8m2 + v_c8m1 + v_c80 + v_c8p1 + v_c8p2 + v_c8p3 + v_c8p4 + v_c8p5 + v_c8p6 + v_c8p7 + v_c8p8


	v_l0 = v_l0 / norm
	v_l1 = v_l1 / norm
	v_l2 = v_l2 / norm
	v_l3 = v_l3 / norm
	v_l4 = v_l4 / norm
	v_l5 = v_l5 / norm
	v_l6 = v_l6 / norm
	v_l7 = v_l7 / norm
	v_l8 = v_l8 / norm

	plt.figure()
	plt.plot(t_c00, v_l0, label=r'$C_{l=0}$', color='hotpink', linestyle=':')
	plt.plot(t_c00, v_l1, label=r'$C_{l=1}$', color='b', linestyle=':')
	plt.plot(t_c00, v_l2, label=r'$C_{l=2}$', color='r', linestyle=':')
	plt.plot(t_c00, v_l3, label=r'$C_{l=3}$', color='g', linestyle=':')
	plt.plot(t_c00, v_l4, label=r'$C_{l=4}$', color='c', linestyle=':')
	plt.plot(t_c00, v_l5, label=r'$C_{l=5}$', color='m', linestyle=':')
	plt.plot(t_c00, v_l6, label=r'$C_{l=6}$', color='y', linestyle=':')
	plt.plot(t_c00, v_l7, label=r'$C_{l=7}$', color='orange', linestyle=':')
	plt.plot(t_c00, v_l8, label=r'$C_{l=8}$', color='lime', linestyle=':')

	plt.yscale("log")
	plt.legend()
	#plt.ylim([1e13, 2e16]) 
	plt.xlabel(r'$t$',fontsize=20)
	pl.savefig("Harmonics_bin_" + b + "_l.pdf",  bbox_inches="tight")

