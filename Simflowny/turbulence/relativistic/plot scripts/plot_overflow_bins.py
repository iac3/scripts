#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20


def readHistogram(file):
	f = open(file)
	bins = []
	values = []
	first_bin = True
	for line in f.readlines():
		if "histo: Bin" in line:
			data = line[12:].split()
			if first_bin:
				bins.append(float(data[1]))
				first_bin = False
			bins.append(float(data[2]))
			values.append(float(data[3]))
	bin_avg = ((bins + np.roll(bins, 1))/2)[1:]
	return bin_avg, values


bins_2048, data_2048 = readHistogram("2048/Histograms/histo_bound_672000")
bins_2048_B0, data_2048_B0 = readHistogram("2048_B0/Histograms/histo_bound_672000")
bins_2048_LES, data_2048_LES = readHistogram("2048_LES/Histograms/histo_bound_672000")
bins_4096, data_4096 = readHistogram("4096/Histograms/histo_bound_672000")
bins_4096_LES, data_4096_LES = readHistogram("4096_LES/Histograms/histo_bound_672000")

w = (abs(bins_2048_LES[1]) - abs(bins_2048_LES[0]))/5

pl.figure()
pl.bar(bins_2048_LES - 2*w, data_2048_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_2048-w, data_2048, label=r'$2048$', width=w)
pl.bar(bins_2048_B0, data_2048_B0, label=r'$2048_{B0}$', width=w)
pl.bar(bins_4096+w, data_4096, label=r'$4096$', width=w)
pl.bar(bins_4096_LES+2*w, data_4096_LES, label=r'$4096_{LES}$', width=w)
pl.legend()
pl.xlabel(r'$v$',fontsize=20)
pl.ylabel(r'$PDF_{mass}$',fontsize=20)
pl.savefig("Hist_v_bound_t5250.pdf",  bbox_inches="tight")

bins_2048, data_2048 = readHistogram("2048/Histograms/histo_unbound_672000")
bins_2048_B0, data_2048_B0 = readHistogram("2048_B0/Histograms/histo_unbound_672000")
bins_2048_LES, data_2048_LES = readHistogram("2048_LES/Histograms/histo_unbound_672000")
bins_4096, data_4096 = readHistogram("4096/Histograms/histo_unbound_672000")
bins_4096_LES, data_4096_LES = readHistogram("4096_LES/Histograms/histo_unbound_672000")


pl.figure()
pl.bar(bins_2048_LES - 2*w, data_2048_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_2048-w, data_2048, label=r'$2048$', width=w)
pl.bar(bins_2048_B0, data_2048_B0, label=r'$2048_{B0}$', width=w)
pl.bar(bins_4096+w, data_4096, label=r'$4096$', width=w)
pl.bar(bins_4096_LES+2*w, data_4096_LES, label=r'$4096_{LES}$', width=w)
pl.legend()
pl.yscale("log")
pl.xlabel(r'$v$',fontsize=20)
pl.ylabel(r'$PDF_{mass}$',fontsize=20)
pl.savefig("Hist_v_unbound_t5250.pdf",  bbox_inches="tight")


bins_b_4250_LES, data_b_4250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_544000")
bins_b_5250_LES, data_b_5250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_672000")
bins_b_7250_LES, data_b_7250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_928000")
bins_b_9250_LES, data_b_9250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_1184000")
bins_b_11250_LES, data_b_11250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_1440000")
bins_b_13000_LES, data_b_13000_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_b_1664000")

bins_b_4250, data_b_4250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_544000")
bins_b_5250, data_b_5250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_672000")
bins_b_7250, data_b_7250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_928000")
bins_b_9250, data_b_9250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_1184000")
bins_b_11250, data_b_11250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_1440000")
bins_b_13000, data_b_13000 = readHistogram("2048/Histograms/m1e13_m5e10/histo_b_1664000")

bins_b_4250_B0, data_b_4250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_544000")
bins_b_5250_B0, data_b_5250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_672000")
bins_b_7250_B0, data_b_7250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_928000")
bins_b_9250_B0, data_b_9250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_1184000")
bins_b_11250_B0, data_b_11250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_1440000")
bins_b_13000_B0, data_b_13000_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_b_1664000")

w = (abs(bins_b_4250[1]) - abs(bins_b_4250[0]))/3

pl.figure()
pl.bar(bins_b_4250_LES - w, data_b_4250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_4250, data_b_4250, label=r'$2048$', width=w)
pl.bar(bins_b_4250_B0 + w, data_b_4250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t4250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_b_5250_LES - w, data_b_5250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_5250, data_b_5250, label=r'$2048$', width=w)
pl.bar(bins_b_5250_B0 + w, data_b_5250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t5250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_b_7250_LES - w, data_b_7250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_7250, data_b_7250, label=r'$2048$', width=w)
pl.bar(bins_b_7250_B0 + w, data_b_7250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t7250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_b_9250_LES - w, data_b_9250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_9250, data_b_9250, label=r'$2048$', width=w)
pl.bar(bins_b_9250_B0 + w, data_b_9250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t9250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_b_11250_LES - w, data_b_11250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_11250, data_b_11250, label=r'$2048$', width=w)
pl.bar(bins_b_11250_B0 + w, data_b_11250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t11250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_b_13000_LES - w, data_b_13000_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_b_13000, data_b_13000, label=r'$2048$', width=w)
pl.bar(bins_b_13000_B0 + w, data_b_13000_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{b}$',fontsize=20)
pl.savefig("Hist_b_t13000.pdf",  bbox_inches="tight")

bins_D_4250_LES, data_D_4250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_544000")
bins_D_5250_LES, data_D_5250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_672000")
bins_D_7250_LES, data_D_7250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_928000")
bins_D_9250_LES, data_D_9250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_1184000")
bins_D_11250_LES, data_D_11250_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_1440000")
bins_D_13000_LES, data_D_13000_LES = readHistogram("2048_LES/Histograms/m1e13_m5e10/histo_D_1664000")

bins_D_4250, data_D_4250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_544000")
bins_D_5250, data_D_5250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_672000")
bins_D_7250, data_D_7250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_928000")
bins_D_9250, data_D_9250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_1184000")
bins_D_11250, data_D_11250 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_1440000")
bins_D_13000, data_D_13000 = readHistogram("2048/Histograms/m1e13_m5e10/histo_D_1664000")

bins_D_4250_B0, data_D_4250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_544000")
bins_D_5250_B0, data_D_5250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_672000")
bins_D_7250_B0, data_D_7250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_928000")
bins_D_9250_B0, data_D_9250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_1184000")
bins_D_11250_B0, data_D_11250_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_1440000")
bins_D_13000_B0, data_D_13000_B0 = readHistogram("2048_B0/Histograms/m1e13_m5e10/histo_D_1664000")

w = (abs(bins_D_4250[1]) - abs(bins_D_4250[0]))/3

pl.figure()
pl.bar(bins_D_4250_LES - w, data_D_4250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_4250, data_D_4250, label=r'$2048$', width=w)
pl.bar(bins_D_4250_B0 + w, data_D_4250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t4250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_D_5250_LES - w, data_D_5250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_5250, data_D_5250, label=r'$2048$', width=w)
pl.bar(bins_D_5250_B0 + w, data_D_5250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t5250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_D_7250_LES - w, data_D_7250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_7250, data_D_7250, label=r'$2048$', width=w)
pl.bar(bins_D_7250_B0 + w, data_D_7250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t7250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_D_9250_LES - w, data_D_9250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_9250, data_D_9250, label=r'$2048$', width=w)
pl.bar(bins_D_9250_B0 + w, data_D_9250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t9250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_D_11250_LES - w, data_D_11250_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_11250, data_D_11250, label=r'$2048$', width=w)
pl.bar(bins_D_11250_B0 + w, data_D_11250_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t11250.pdf",  bbox_inches="tight")

pl.figure()
pl.bar(bins_D_13000_LES - w, data_D_13000_LES, label=r'$2048_{LES}$', width=w)
pl.bar(bins_D_13000, data_D_13000, label=r'$2048$', width=w)
pl.bar(bins_D_13000_B0 + w, data_D_13000_B0, label=r'$2048_{B0}$', width=w)
pl.legend()
pl.xlabel(r'$\Theta$',fontsize=20)
pl.ylabel(r'$PDF_{D}$',fontsize=20)
pl.savefig("Hist_D_t13000.pdf",  bbox_inches="tight")


t = [4250, 5250, 7250, 9250, 11250, 13000]
t_D_2048 = np.zeros((6))
t_D_2048_LES = np.zeros((6))
t_D_2048_B0 = np.zeros((6))

t_D_2048[0] = data_D_4250[0] + data_D_4250[-1]
t_D_2048[1] = data_D_5250[0] + data_D_5250[-1]
t_D_2048[2] = data_D_7250[0] + data_D_7250[-1]
t_D_2048[3] = data_D_9250[0] + data_D_9250[-1]
t_D_2048[4] = data_D_11250[0] + data_D_11250[-1]
t_D_2048[5] = data_D_13000[0] + data_D_13000[-1]

t_D_2048_LES[0] = data_D_4250_LES[0] + data_D_4250_LES[-1]
t_D_2048_LES[1] = data_D_5250_LES[0] + data_D_5250_LES[-1]
t_D_2048_LES[2] = data_D_7250_LES[0] + data_D_7250_LES[-1]
t_D_2048_LES[3] = data_D_9250_LES[0] + data_D_9250_LES[-1]
t_D_2048_LES[4] = data_D_11250_LES[0] + data_D_11250_LES[-1]
t_D_2048_LES[5] = data_D_13000_LES[0] + data_D_13000_LES[-1]

t_D_2048_B0[0] = data_D_4250_B0[0] + data_D_4250_B0[-1]
t_D_2048_B0[1] = data_D_5250_B0[0] + data_D_5250_B0[-1]
t_D_2048_B0[2] = data_D_7250_B0[0] + data_D_7250_B0[-1]
t_D_2048_B0[3] = data_D_9250_B0[0] + data_D_9250_B0[-1]
t_D_2048_B0[4] = data_D_11250_B0[0] + data_D_11250_B0[-1]
t_D_2048_B0[5] = data_D_13000_B0[0] + data_D_13000_B0[-1]

plt.figure()
plt.plot(t, t_D_2048, label=r'$NO LES$', color='b')
plt.plot(t, t_D_2048_LES, label=r'$LES$', color='r')
plt.plot(t, t_D_2048_B0, label=r'$B0$', color='g')
plt.legend(loc='lower right')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$D_{DISK}$',fontsize=20)
pl.savefig("D_zAxis_m1e13_m5e10.pdf",  bbox_inches="tight")
pl.close()
