#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as ticker
import math
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20

time = "7250.0"


B_factor = 1e20
E_factor = 1.8e54
rho_factor = 6.14e17
omega_factor = 1.0/4.94e-6

# field_script_resolution_mask_file

########### Datos 2D ###########
'''
int2d_1024 = np.genfromtxt(fname="1024/Integrals_2D/level7/mask1e13/box60/integral.out")
t_int2d_1024_m1e13_int = int2d_1024[:,0]
BRMS_int2d_1024_m1e13_int = int2d_1024[:,5] * B_factor
BRMSP_int2d_1024_m1e13_int = int2d_1024[:,6] * B_factor
BRMST_int2d_1024_m1e13_int = int2d_1024[:,7] * B_factor
int2d_1024_m5e10 = np.genfromtxt(fname="1024/Integrals_2D/level7/mask5e10/box60/integral.out")
t_int2d_1024_m5e10_int = int2d_1024_m5e10[:,0]
BRMS_int2d_1024_m5e10_int = int2d_1024_m5e10[:,5] * B_factor
BRMSP_int2d_1024_m5e10_int = int2d_1024_m5e10[:,6] * B_factor
BRMST_int2d_1024_m5e10_int = int2d_1024_m5e10[:,7] * B_factor

int2d_1024_les = np.genfromtxt(fname="1024_LES/Integrals_2D/level7/mask1e13/box60/integral.out")
t_int2d_1024_les_m1e13_int = int2d_1024_les[:,0]
BRMS_int2d_1024_les_m1e13_int = int2d_1024_les[:,5] * B_factor
BRMSP_int2d_1024_les_m1e13_int = int2d_1024_les[:,6] * B_factor
BRMST_int2d_1024_les_m1e13_int = int2d_1024_les[:,7] * B_factor
int2d_1024_les_m5e10 = np.genfromtxt(fname="1024_LES/Integrals_2D/level7/mask5e10/box60/integral.out")
t_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,0]
BRMS_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,5] * B_factor
BRMSP_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,6] * B_factor
BRMST_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,7] * B_factor

int2d_2048 = np.genfromtxt(fname="2048/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_m1e13_int = int2d_2048[:,0]
BRMS_int2d_2048_m1e13_int = int2d_2048[:,5] * B_factor
BRMSP_int2d_2048_m1e13_int = int2d_2048[:,6] * B_factor
BRMST_int2d_2048_m1e13_int = int2d_2048[:,7] * B_factor
int2d_2048_m5e10 = np.genfromtxt(fname="2048/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_m5e10_int = int2d_2048_m5e10[:,0]
BRMS_int2d_2048_m5e10_int = int2d_2048_m5e10[:,5] * B_factor
BRMSP_int2d_2048_m5e10_int = int2d_2048_m5e10[:,6] * B_factor
BRMST_int2d_2048_m5e10_int = int2d_2048_m5e10[:,7] * B_factor

int2d_2048_les = np.genfromtxt(fname="2048_LES/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_les_m1e13_int = int2d_2048_les[:,0]
BRMS_int2d_2048_les_m1e13_int = int2d_2048_les[:,5] * B_factor
BRMSP_int2d_2048_les_m1e13_int = int2d_2048_les[:,6] * B_factor
BRMST_int2d_2048_les_m1e13_int = int2d_2048_les[:,7] * B_factor
int2d_2048_les_m5e10 = np.genfromtxt(fname="2048_LES/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,0]
BRMS_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,5] * B_factor
BRMSP_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,6] * B_factor
BRMST_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,7] * B_factor
'''
int2d_2048_les_t10 = np.genfromtxt(fname="2048_LES_t10/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,0]
BRMS_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,5] * B_factor
BRMSP_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,6] * B_factor
BRMST_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,7] * B_factor
int2d_2048_les_t10_m5e10 = np.genfromtxt(fname="2048_LES_t10/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,0]
BRMS_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,5] * B_factor
BRMSP_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,6] * B_factor
BRMST_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,7] * B_factor
'''
int2d_4096 = np.genfromtxt(fname="4096/Integrals_2D/level9/mask1e13/box60/integral.out")
t_int2d_4096_m1e13_int = int2d_4096[:,0]
BRMSP_int2d_4096_m1e13_int = int2d_4096[:,6] * B_factor
BRMST_int2d_4096_m1e13_int = int2d_4096[:,7] * B_factor
int2d_4096_m5e10 = np.genfromtxt(fname="4096/Integrals_2D/level9/mask5e10/box60/integral.out")
t_int2d_4096_m5e10_int = int2d_4096_m5e10[:,0]
BRMSP_int2d_4096_m5e10_int = int2d_4096_m5e10[:,6] * B_factor
BRMST_int2d_4096_m5e10_int = int2d_4096_m5e10[:,7] * B_factor

int2d_4096_les = np.genfromtxt(fname="4096_LES/Integrals_2D/level9/mask1e13/box60/integral.out")
t_int2d_4096_les_m1e13_int = int2d_4096_les[:,0]
BRMSP_int2d_4096_les_m1e13_int = int2d_4096_les[:,6] * B_factor
BRMST_int2d_4096_les_m1e13_int = int2d_4096_les[:,7] * B_factor
int2d_4096_les_m5e10 = np.genfromtxt(fname="4096_LES/Integrals_2D/level9/mask5e10/box60/integral.out")
t_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,0]
BRMSP_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,6] * B_factor
BRMST_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,7] * B_factor


########### Datos 3D ###########

int3d_1024_les_m1e13_m5e10_int = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,0]
P0_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,8]
P1_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,9]
P2_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,10]
P3_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,11]
P4_int3d_1024_les_m1e13_m5e10_int = int3d_1024_les_m1e13_m5e10_int[:,12]
int3d_1024_les_m1e13_avg = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask1e13/box60/averages.out")
t_int3d_1024_les_m1e13_avg = int3d_1024_les_m1e13_avg[:,0]
B_int3d_1024_les_m1e13_avg = int3d_1024_les_m1e13_avg[:,1] * B_factor

int3d_1024_les_m5e10_int = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask5e10/box60/integral.out")
t_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,0]
Erot_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,2] * E_factor
Eint_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,3] * E_factor
Emag_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,4] * E_factor
D_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,5] * E_factor
P0_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,8]
P1_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,9]
P2_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,10]
P3_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,11]
P4_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,12]
Eth_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,13] * E_factor
int3d_1024_les_m5e10_avg = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask5e10/box60/averages.out")
t_int3d_1024_les_m5e10_avg = int3d_1024_les_m5e10_avg[:,0]
B_int3d_1024_les_m5e10_avg = int3d_1024_les_m5e10_avg[:,1] * B_factor

int3d_1024_les_m1e13_m5e10_avg = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_1024_les_m1e13_m5e10_avg = int3d_1024_les_m1e13_m5e10_avg[:,0]
B_int3d_1024_les_m1e13_m5e10_avg = int3d_1024_les_m1e13_m5e10_avg[:,1] * B_factor

int3d_1024_m1e13_m5e10_int = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,0]
P0_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,8]
P1_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,9]
P2_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,10]
P3_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,11]
P4_int3d_1024_m1e13_m5e10_int = int3d_1024_m1e13_m5e10_int[:,12]

int3d_1024_m1e13_avg = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask1e13/box60/averages.out")
t_int3d_1024_m1e13_avg = int3d_1024_m1e13_avg[:,0]
B_int3d_1024_m1e13_avg = int3d_1024_m1e13_avg[:,1] * B_factor

int3d_1024_m1e13_m5e10_avg = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_1024_m1e13_m5e10_avg = int3d_1024_m1e13_m5e10_avg[:,0]
B_int3d_1024_m1e13_m5e10_avg = int3d_1024_m1e13_m5e10_avg[:,1] * B_factor


int3d_1024_m5e10_int = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask5e10/box60/integral.out")
t_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,0]
Erot_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,2] * E_factor
Eint_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,3] * E_factor
Emag_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,4] * E_factor
D_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,5] * E_factor
P0_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,8]
P1_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,9]
P2_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,10]
P3_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,11]
P4_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,12]
Eth_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,13] * E_factor
int3d_1024_m5e10_avg = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask5e10/box60/averages.out")
t_int3d_1024_m5e10_avg = int3d_1024_m5e10_avg[:,0]
B_int3d_1024_m5e10_avg = int3d_1024_m5e10_avg[:,1] * B_factor

int3d_2048_b0_m1e13_m5e10_int = np.genfromtxt(fname="2048_B0/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,0]
P0_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,8]
P1_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,9]
P2_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,10]
P3_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,11]
P4_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,12]

int3d_2048_b0_m5e10_int = np.genfromtxt(fname="2048_B0/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,0]
P0_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,8]
P1_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,9]
P2_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,10]
P3_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,11]
P4_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,12]

int3d_2048_les_m1e13_m5e10_int = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,0]
P0_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,8]
P1_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,9]
P2_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,10]
P3_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,11]
P4_int3d_2048_les_m1e13_m5e10_int = int3d_2048_les_m1e13_m5e10_int[:,12]
'''
int3d_2048_les_m1e13_m5e10_avg = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_2048_les_m1e13_m5e10_avg = int3d_2048_les_m1e13_m5e10_avg[:,0]
B_int3d_2048_les_m1e13_m5e10_avg = int3d_2048_les_m1e13_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_les_m1e13_m5e10_avg = int3d_2048_les_m1e13_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_les_m1e13_m5e10_avg = int3d_2048_les_m1e13_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_les_m1e13_m5e10_avg = int3d_2048_les_m1e13_m5e10_avg[:,7]

int3d_2048_les_m1e13_avg = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask1e13/box60/averages.out")
t_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,0]
B_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,1] * B_factor
Btor_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,2] * B_factor
Bpol_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,3] * B_factor
invbeta_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,7]
int3d_2048_les_m5e10_avg = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask5e10/box60/averages.out")
t_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,0]
B_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,7]


int3d_2048_les_full_m1e13_avg = np.genfromtxt(fname="2048_LES_full/Integrals_3D/level8_multilevel/mask1e13/box60/averages.out")
t_int3d_2048_les_full_m1e13_avg = int3d_2048_les_full_m1e13_avg[:,0]
B_int3d_2048_les_full_m1e13_avg = int3d_2048_les_full_m1e13_avg[:,1] * B_factor
Btor_int3d_2048_les_full_m1e13_avg = int3d_2048_les_full_m1e13_avg[:,2] * B_factor
Bpol_int3d_2048_les_full_m1e13_avg = int3d_2048_les_full_m1e13_avg[:,3] * B_factor
invbeta_int3d_2048_les_full_m1e13_avg = int3d_2048_les_full_m1e13_avg[:,7]

int3d_2048_les_full_m1e13_m5e10_avg = np.genfromtxt(fname="2048_LES_full/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_2048_les_full_m1e13_m5e10_avg = int3d_2048_les_full_m1e13_m5e10_avg[:,0]
B_int3d_2048_les_full_m1e13_m5e10_avg = int3d_2048_les_full_m1e13_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_les_full_m1e13_m5e10_avg = int3d_2048_les_full_m1e13_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_les_full_m1e13_m5e10_avg = int3d_2048_les_full_m1e13_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_les_full_m1e13_m5e10_avg = int3d_2048_les_full_m1e13_m5e10_avg[:,7]

int3d_2048_les_m5e10_int = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,0]
Erot_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,2] * E_factor
Eint_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,3] * E_factor
Emag_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,4] * E_factor
D_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,5] * E_factor
P0_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,8]
P1_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,9]
P2_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,10]
P3_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,11]
P4_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,12]
Eth_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,13] * E_factor

int3d_2048_les_full_m5e10_int = np.genfromtxt(fname="2048_LES_full/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,0]
Erot_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,2] * E_factor
Eint_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,3] * E_factor
Emag_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,4] * E_factor
D_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,5] * E_factor
P0_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,8]
P1_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,9]
P2_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,10]
P3_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,11]
P4_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,12]
Eth_int3d_2048_les_full_m5e10_int = int3d_2048_les_full_m5e10_int[:,13] * E_factor

'''
int3d_2048_les_m1em14_int = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask1e-14/box60/integral.out")
t_int3d_2048_les_m1em14_int = int3d_2048_les_m1em14_int[:,0]
Erot_int3d_2048_les_m1em14_int = int3d_2048_les_m1em14_int[:,2] * E_factor
Eint_int3d_2048_les_m1em14_int = int3d_2048_les_m1em14_int[:,3] * E_factor
Emag_int3d_2048_les_m1em14_int = int3d_2048_les_m1em14_int[:,4] * E_factor
D_int3d_2048_les_m1em14_int = int3d_2048_les_m1em14_int[:,5] * E_factor


int3d_2048_b0_m1e13_m5e10_int = np.genfromtxt(fname="2048_B0/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,0]
P0_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,8]
P1_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,9]
P2_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,10]
P3_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,11]
P4_int3d_2048_b0_m1e13_m5e10_int = int3d_2048_b0_m1e13_m5e10_int[:,12]

int3d_2048_b0_m5e10_int = np.genfromtxt(fname="2048_B0/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,0]
P0_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,8]
P1_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,9]
P2_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,10]
P3_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,11]
P4_int3d_2048_b0_m5e10_int = int3d_2048_b0_m5e10_int[:,12]

int3d_2048_m1e13_m5e10_int = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,0]
P0_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,8]
P1_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,9]
P2_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,10]
P3_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,11]
P4_int3d_2048_m1e13_m5e10_int = int3d_2048_m1e13_m5e10_int[:,12]

int3d_2048_m1e13_avg = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask1e13/box60/averages.out")
t_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,0]
B_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,1] * B_factor
Btor_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,2] * B_factor
Bpol_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,3] * B_factor
invbeta_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,7]

int3d_2048_m1e13_m5e10_avg = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_2048_m1e13_m5e10_avg = int3d_2048_m1e13_m5e10_avg[:,0]
B_int3d_2048_m1e13_m5e10_avg = int3d_2048_m1e13_m5e10_avg[:,1] * B_factor

int3d_2048_m5e10_avg = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask5e10/box60/averages.out")
t_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,0]
B_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,7]
'''
int3d_2048_m5e10_int = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,0]
Erot_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,2] * E_factor
Eint_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,3] * E_factor
Emag_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,4] * E_factor
D_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,5] * E_factor
P0_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,8]
P1_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,9]
P2_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,10]
P3_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,11]
P4_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,12]
Eth_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,13] * E_factor
'''
int3d_4096_m1e13_m5e10_int = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,0]
P0_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,8]
P1_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,9]
P2_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,10]
P3_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,11]
P4_int3d_4096_m1e13_m5e10_int = int3d_4096_m1e13_m5e10_int[:,12]

int3d_4096_m1e13_avg = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask1e13/box60/averages.out")
t_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,0]
B_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,1] * B_factor
Btor_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,2] * B_factor
Bpol_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,3] * B_factor
invbeta_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,7]
int3d_4096_m1e13_m5e10_avg = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_4096_m1e13_m5e10_avg = int3d_4096_m1e13_m5e10_avg[:,0]
B_int3d_4096_m1e13_m5e10_avg = int3d_4096_m1e13_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_m1e13_m5e10_avg = int3d_4096_m1e13_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_m1e13_m5e10_avg = int3d_4096_m1e13_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_m1e13_m5e10_avg = int3d_4096_m1e13_m5e10_avg[:,7]
int3d_4096_m5e10_avg = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask5e10/box60/averages.out")
t_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,0]
B_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,7]
int3d_4096_m5e10_int = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask5e10/box60/integral.out")
t_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,0]
Erot_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,2] * E_factor
Eint_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,3] * E_factor
Emag_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,4] * E_factor
D_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,5] * E_factor
P0_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,8]
P1_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,9]
P2_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,10]
P3_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,11]
P4_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,12]
Eth_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,13] * E_factor

int3d_4096_les_m1e13_m5e10_int = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask1e13_m5e10/box60/integral.out")
t_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,0]
P0_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,8]
P1_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,9]
P2_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,10]
P3_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,11]
P4_int3d_4096_les_m1e13_m5e10_int = int3d_4096_les_m1e13_m5e10_int[:,12]

int3d_4096_les_m1e13_avg = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask1e13/box60/averages.out")
t_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,0]
B_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,1] * B_factor
Btor_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,2] * B_factor
Bpol_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,3] * B_factor
invbeta_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,7]
int3d_4096_les_m1e13_m5e10_avg = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask1e13_m5e10/box60/averages.out")
t_int3d_4096_les_m1e13_m5e10_avg = int3d_4096_les_m1e13_m5e10_avg[:,0]
B_int3d_4096_les_m1e13_m5e10_avg = int3d_4096_les_m1e13_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_les_m1e13_m5e10_avg = int3d_4096_les_m1e13_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_les_m1e13_m5e10_avg = int3d_4096_les_m1e13_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_les_m1e13_m5e10_avg = int3d_4096_les_m1e13_m5e10_avg[:,7]
int3d_4096_les_m5e10_avg = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask5e10/box60/averages.out")
t_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,0]
B_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,7]
int3d_4096_les_m5e10_int = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask5e10/box60/integral.out")
t_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,0]
Erot_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,2] * E_factor
Eint_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,3] * E_factor
Emag_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,4] * E_factor
D_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,5] * E_factor
P0_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,8]
P1_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,9]
P2_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,10]
P3_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,11]
P4_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,12]
Eth_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,13] * E_factor

max_2048_les_m1em16 = np.genfromtxt(fname="2048_LES/Maximum_3D/level8_multilevel/mask1e-16/box60/maximum.out")
t_max_m1em16_2048_les = max_2048_les_m1em16[:,0]
invbeta_max_m1em16_2048_les = max_2048_les_m1em16[:,1]

########### Datos cilindro ############

cyl_flat_m5e10_1024_4250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
r_cyl_1024_m1e13_avg_4250 = cyl_flat_m5e10_1024_4250[:,0]
omega_cyl_flat_1024_m5e10_avg_4250 = cyl_flat_m5e10_1024_4250[:,9]

cyl_flat_m5e10_1024_5250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
omega_cyl_flat_1024_m5e10_avg_5250 = cyl_flat_m5e10_1024_5250[:,9]

cyl_flat_m5e10_1024_7250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
omega_cyl_flat_1024_m5e10_avg_7250 = cyl_flat_m5e10_1024_7250[:,9]

cyl_flat_m5e10_1024_9250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_9250.0.out")
omega_cyl_flat_1024_m5e10_avg_9250 = cyl_flat_m5e10_1024_9250[:,9]

cyl_flat_m5e10_1024_11250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_11250.0.out")
omega_cyl_flat_1024_m5e10_avg_11250 = cyl_flat_m5e10_1024_11250[:,9]

cyl_flat_m5e10_1024_les_4000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_4000.0.out")
r_cyl_1024_les_m1e13_avg_4000 = cyl_flat_m5e10_1024_les_4000[:,0]
omega_cyl_flat_1024_les_m5e10_avg_4000 = cyl_flat_m5e10_1024_les_4000[:,9]

cyl_flat_m5e10_1024_les_5000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_5000.0.out")
omega_cyl_flat_1024_les_m5e10_avg_5000 = cyl_flat_m5e10_1024_les_5000[:,9]

cyl_flat_m5e10_1024_les_7000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_7000.0.out")
omega_cyl_flat_1024_les_m5e10_avg_7000 = cyl_flat_m5e10_1024_les_7000[:,9]

cyl_flat_m5e10_1024_les_9000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask5e10/box60_r_flat/averages_cylinder_9000.0.out")
omega_cyl_flat_1024_les_m5e10_avg_9000 = cyl_flat_m5e10_1024_les_9000[:,9]


cyl_m1e13_1024_4250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_1024_m1e13_avg_4250 = cyl_m1e13_1024_4250[:,0]
omega_cyl_1024_m1e13_avg_4250 = cyl_m1e13_1024_4250[:,9]

cyl_m1e13_1024_5250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
omega_cyl_1024_m1e13_avg_5250 = cyl_m1e13_1024_5250[:,9]

cyl_m1e13_1024_7250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_1024_m1e13_avg_7250 = cyl_m1e13_1024_7250[:,9]

cyl_m1e13_1024_9250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
omega_cyl_1024_m1e13_avg_9250 = cyl_m1e13_1024_9250[:,9]

cyl_m1e13_1024_11250 = np.genfromtxt(fname="1024/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
omega_cyl_1024_m1e13_avg_11250 = cyl_m1e13_1024_11250[:,9]

cyl_m1e13_1024_les_4000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_4000.0.out")
r_cyl_1024_les_m1e13_avg_4000 = cyl_m1e13_1024_les_4000[:,0]
omega_cyl_1024_les_m1e13_avg_4000 = cyl_m1e13_1024_les_4000[:,9]

cyl_m1e13_1024_les_5000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_5000.0.out")
omega_cyl_1024_les_m1e13_avg_5000 = cyl_m1e13_1024_les_5000[:,9]

cyl_m1e13_1024_les_7000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_7000.0.out")
omega_cyl_1024_les_m1e13_avg_7000 = cyl_m1e13_1024_les_7000[:,9]

cyl_m1e13_1024_les_9000 = np.genfromtxt(fname="1024_LES/Cylinder/level7_multilevel/mask1e13/box60_r/averages_cylinder_9000.0.out")
omega_cyl_1024_les_m1e13_avg_9000 = cyl_m1e13_1024_les_9000[:,9]

'''

cyl_m1e13_2048_les_full_4250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,0]
Btor_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,7]
rho_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,8]
omega_cyl_2048_les_full_m1e13_avg_4250 = cyl_m1e13_2048_les_full_4250[:,9]
cyl_m1e13_2048_les_full_5250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,0]
Btor_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,7]
rho_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,8]
omega_cyl_2048_les_full_m1e13_avg_5250 = cyl_m1e13_2048_les_full_5250[:,9]
cyl_m1e13_2048_les_full_7250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,0]
Btor_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,7]
rho_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,8]
omega_cyl_2048_les_full_m1e13_avg_7250 = cyl_m1e13_2048_les_full_7250[:,9]
cyl_m1e13_2048_les_full_9250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,0]
Btor_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,7]
rho_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,8]
omega_cyl_2048_les_full_m1e13_avg_9250 = cyl_m1e13_2048_les_full_9250[:,9]
cyl_m1e13_2048_les_full_11250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,0]
Btor_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,7]
rho_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,8]
omega_cyl_2048_les_full_m1e13_avg_11250 = cyl_m1e13_2048_les_full_11250[:,9]
cyl_m1e13_2048_les_full_13000 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,0]
Btor_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,7]
rho_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,8]
omega_cyl_2048_les_full_m1e13_avg_13000 = cyl_m1e13_2048_les_full_13000[:,9]
cyl_m1e13_m5e10_2048_les_full_4250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_full_4250[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_full_4250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_full_4250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_full_4250[:,7]
cyl_m1e13_m5e10_2048_les_full_5250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_full_5250[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_full_5250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_full_5250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_full_5250[:,7]
cyl_m1e13_m5e10_2048_les_full_7250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_full_7250[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_full_7250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_full_7250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_full_7250[:,7]
cyl_m1e13_m5e10_2048_les_full_9250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_full_9250[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_full_9250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_full_9250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_full_9250[:,7]
cyl_m1e13_m5e10_2048_les_full_11250 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_full_11250[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_full_11250[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_full_11250[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_full_11250[:,7]
cyl_m1e13_m5e10_2048_les_full_13000 = np.genfromtxt(fname="2048_LES_full/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_full_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_full_13000[:,0]
Btor_cyl_2048_les_full_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_full_13000[:,2] * B_factor
Bpol_cyl_2048_les_full_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_full_13000[:,3] * B_factor
invbeta_cyl_2048_les_full_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_full_13000[:,7]

'''

cyl_m1e13_2048_les_4250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,0]
Btor_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,7]
rho_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,8]
omega_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,9]
cyl_m1e13_2048_les_5250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,0]
Btor_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,7]
rho_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,8]
omega_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,9]
cyl_m1e13_2048_les_7250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,0]
Btor_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,7]
rho_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,8]
omega_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,9]
cyl_m1e13_2048_les_9250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,0]
Btor_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,7]
rho_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,8]
omega_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,9]
cyl_m1e13_2048_les_11250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,0]
Btor_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,7]
rho_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,8]
omega_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,9]
cyl_m1e13_2048_les_13000 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,0]
Btor_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,7]
rho_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,8]
omega_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,9]
cyl_m1e13_m5e10_2048_les_4250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_4250[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_4250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_4250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_4250 = cyl_m1e13_m5e10_2048_les_4250[:,7]
cyl_m1e13_m5e10_2048_les_5250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_5250[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_5250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_5250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_5250 = cyl_m1e13_m5e10_2048_les_5250[:,7]
cyl_m1e13_m5e10_2048_les_7250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_7250[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_7250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_7250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_7250 = cyl_m1e13_m5e10_2048_les_7250[:,7]
cyl_m1e13_m5e10_2048_les_9250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_9250[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_9250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_9250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_9250 = cyl_m1e13_m5e10_2048_les_9250[:,7]
cyl_m1e13_m5e10_2048_les_11250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_11250[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_11250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_11250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_11250 = cyl_m1e13_m5e10_2048_les_11250[:,7]
cyl_m1e13_m5e10_2048_les_13000 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13_m5e10/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_13000[:,0]
Btor_cyl_2048_les_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_13000[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_13000[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_m5e10_avg_13000 = cyl_m1e13_m5e10_2048_les_13000[:,7]
cyl_m5e10_2048_les_4250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,0]
Btor_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,7]
rho_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,8]
omega_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,9]
cyl_m5e10_2048_les_5250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,0]
Btor_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,7]
rho_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,8]
omega_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,9]
cyl_m5e10_2048_les_7250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,0]
Btor_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,7]
rho_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,8]
omega_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,9]
cyl_m5e10_2048_les_9250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,0]
Btor_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,7]
rho_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,8]
omega_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,9]
cyl_m5e10_2048_les_11250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,0]
Btor_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,7]
rho_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,8]
omega_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,9]
cyl_m5e10_2048_les_13000 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,0]
Btor_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,7]
rho_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,8]
omega_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,9]


cyl_m1e13_2048_les_4250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
omega_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,9]
cyl_m1e13_2048_les_5250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,0]
rho_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,8]
omega_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,9]
cyl_m1e13_2048_les_7250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,9]
cyl_m1e13_2048_les_9250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,0]
rho_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,8]
omega_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,9]
cyl_m1e13_2048_les_11250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
omega_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,9]
cyl_m1e13_2048_les_13000 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
omega_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,9]


cyl_flat_m5e10_2048_les_4250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
r_cyl_flat_2048_les_m5e10_avg_4250 = cyl_flat_m5e10_2048_les_4250[:,0]
rho_cyl_flat_2048_les_m5e10_avg_4250 = cyl_flat_m5e10_2048_les_4250[:,8]
omega_cyl_flat_2048_les_m5e10_avg_4250 = cyl_flat_m5e10_2048_les_4250[:,9]

cyl_flat_m5e10_2048_les_5250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
r_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,0]
Btor_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,2] * B_factor
Bpol_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,3] * B_factor
invbeta_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,7]
rho_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,8]
omega_cyl_flat_2048_les_m5e10_avg_5250 = cyl_flat_m5e10_2048_les_5250[:,9]
cyl_flat_m5e10_2048_les_7250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
r_cyl_flat_2048_les_m5e10_avg_7250 = cyl_flat_m5e10_2048_les_7250[:,0]
Btor_cyl_flat_2048_les_m5e10_avg_7250 = cyl_flat_m5e10_2048_les_7250[:,2] * B_factor
Bpol_cyl_flat_2048_les_m5e10_avg_7250 = cyl_flat_m5e10_2048_les_7250[:,3] * B_factor
rho_cyl_flat_2048_les_m5e10_avg_7250 = cyl_flat_m5e10_2048_les_7250[:,8]
omega_cyl_flat_2048_les_m5e10_avg_7250 = cyl_flat_m5e10_2048_les_7250[:,9]
cyl_flat_m5e10_2048_les_9250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_9250.0.out")
r_cyl_flat_2048_les_m5e10_avg_9250 = cyl_flat_m5e10_2048_les_9250[:,0]
Btor_cyl_flat_2048_les_m5e10_avg_9250 = cyl_flat_m5e10_2048_les_9250[:,2] * B_factor
Bpol_cyl_flat_2048_les_m5e10_avg_9250 = cyl_flat_m5e10_2048_les_9250[:,3] * B_factor
rho_cyl_flat_2048_les_m5e10_avg_9250 = cyl_flat_m5e10_2048_les_9250[:,8]
omega_cyl_flat_2048_les_m5e10_avg_9250 = cyl_flat_m5e10_2048_les_9250[:,9]
cyl_flat_m5e10_2048_les_11250 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_11250.0.out")
r_cyl_flat_2048_les_m5e10_avg_11250 = cyl_flat_m5e10_2048_les_11250[:,0]
Btor_cyl_flat_2048_les_m5e10_avg_11250 = cyl_flat_m5e10_2048_les_11250[:,2] * B_factor
Bpol_cyl_flat_2048_les_m5e10_avg_11250 = cyl_flat_m5e10_2048_les_11250[:,3] * B_factor
rho_cyl_flat_2048_les_m5e10_avg_11250 = cyl_flat_m5e10_2048_les_11250[:,8]
omega_cyl_flat_2048_les_m5e10_avg_11250 = cyl_flat_m5e10_2048_les_11250[:,9]
cyl_flat_m5e10_2048_les_13000 = np.genfromtxt(fname="2048_LES/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_13000.0.out")
r_cyl_flat_2048_les_m5e10_avg_13000 = cyl_flat_m5e10_2048_les_13000[:,0]
Btor_cyl_flat_2048_les_m5e10_avg_13000 = cyl_flat_m5e10_2048_les_13000[:,2] * B_factor
Bpol_cyl_flat_2048_les_m5e10_avg_13000 = cyl_flat_m5e10_2048_les_13000[:,3] * B_factor
rho_cyl_flat_2048_les_m5e10_avg_13000 = cyl_flat_m5e10_2048_les_13000[:,8]
omega_cyl_flat_2048_les_m5e10_avg_13000 = cyl_flat_m5e10_2048_les_13000[:,9]

cyl_m1e13_2048_4250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_m1e13_avg_4250 = cyl_m1e13_2048_4250[:,0]
invbeta_cyl_2048_m1e13_avg_4250 = cyl_m1e13_2048_4250[:,7]
cyl_m5e10_2048_4250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,0]
invbeta_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,7]
rho_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,8]
omega_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,9]
cyl_m1e13_2048_5250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_m1e13_avg_5250 = cyl_m1e13_2048_5250[:,0]
invbeta_cyl_2048_m1e13_avg_5250 = cyl_m1e13_2048_5250[:,7]
cyl_m5e10_2048_5250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_m5e10_avg_5250 = cyl_m5e10_2048_5250[:,0]
invbeta_cyl_2048_m5e10_avg_5250 = cyl_m5e10_2048_5250[:,7]
rho_cyl_2048_m5e10_avg_5250 = cyl_m5e10_2048_5250[:,8]
omega_cyl_2048_m5e10_avg_5250 = cyl_m5e10_2048_5250[:,9]
cyl_m5e10_2048_7250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_m5e10_avg_7250 = cyl_m5e10_2048_7250[:,0]
rho_cyl_2048_m5e10_avg_7250 = cyl_m5e10_2048_7250[:,8]
omega_cyl_2048_m5e10_avg_7250 = cyl_m5e10_2048_7250[:,9]
cyl_m5e10_2048_9250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_m5e10_avg_9250 = cyl_m5e10_2048_9250[:,0]
rho_cyl_2048_m5e10_avg_9250 = cyl_m5e10_2048_9250[:,8]
omega_cyl_2048_m5e10_avg_9250 = cyl_m5e10_2048_9250[:,9]

cyl_flat_m5e10_2048_4250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
omega_cyl_flat_2048_m5e10_avg_4250 = cyl_flat_m5e10_2048_4250[:,9]
cyl_flat_m5e10_2048_5250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
r_cyl_flat_2048_m5e10_avg_5250 = cyl_flat_m5e10_2048_5250[:,0]
rho_cyl_flat_2048_m5e10_avg_5250 = cyl_flat_m5e10_2048_5250[:,8]
omega_cyl_flat_2048_m5e10_avg_5250 = cyl_flat_m5e10_2048_5250[:,9]
cyl_flat_m5e10_2048_7250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
omega_cyl_flat_2048_m5e10_avg_7250 = cyl_flat_m5e10_2048_7250[:,9]
cyl_flat_m5e10_2048_9250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_9250.0.out")
r_cyl_flat_2048_m5e10_avg_9250 = cyl_flat_m5e10_2048_9250[:,0]
rho_cyl_flat_2048_m5e10_avg_9250 = cyl_flat_m5e10_2048_9250[:,8]
omega_cyl_flat_2048_m5e10_avg_9250 = cyl_flat_m5e10_2048_9250[:,9]
cyl_flat_m5e10_2048_11250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_11250.0.out")
omega_cyl_flat_2048_m5e10_avg_11250 = cyl_flat_m5e10_2048_11250[:,9]
cyl_flat_m5e10_2048_13000 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_13000.0.out")
r_cyl_flat_2048_m5e10_avg_13000 = cyl_flat_m5e10_2048_13000[:,0]
rho_cyl_flat_2048_m5e10_avg_13000 = cyl_flat_m5e10_2048_13000[:,8]
omega_cyl_flat_2048_m5e10_avg_13000 = cyl_flat_m5e10_2048_13000[:,9]


cyl_m1e13_2048_4250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
omega_cyl_2048_m1e13_avg_4250 = cyl_m1e13_2048_4250[:,9]
cyl_m1e13_2048_5250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_m1e13_avg_5250 = cyl_m1e13_2048_5250[:,0]
rho_cyl_2048_m1e13_avg_5250 = cyl_m1e13_2048_5250[:,8]
omega_cyl_2048_m1e13_avg_5250 = cyl_m1e13_2048_5250[:,9]
cyl_m1e13_2048_7250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_2048_m1e13_avg_7250 = cyl_m1e13_2048_7250[:,9]
cyl_m1e13_2048_9250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_m1e13_avg_9250 = cyl_m1e13_2048_9250[:,0]
rho_cyl_2048_m1e13_avg_9250 = cyl_m1e13_2048_9250[:,8]
omega_cyl_2048_m1e13_avg_9250 = cyl_m1e13_2048_9250[:,9]
cyl_m1e13_2048_11250 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
omega_cyl_2048_m1e13_avg_11250 = cyl_m1e13_2048_11250[:,9]
cyl_m1e13_2048_13000 = np.genfromtxt(fname="2048/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
omega_cyl_2048_m1e13_avg_13000 = cyl_m1e13_2048_13000[:,9]


cyl_m5e10_2048_b0_4250 = np.genfromtxt(fname="2048_B0/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_b0_m5e10_avg_4250 = cyl_m5e10_2048_b0_4250[:,0]
rho_cyl_2048_b0_m5e10_avg_4250 = cyl_m5e10_2048_b0_4250[:,8]
omega_cyl_2048_b0_m5e10_avg_4250 = cyl_m5e10_2048_b0_4250[:,9]
cyl_m5e10_2048_b0_5250 = np.genfromtxt(fname="2048_B0/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_b0_m5e10_avg_5250 = cyl_m5e10_2048_b0_5250[:,0]
rho_cyl_2048_b0_m5e10_avg_5250 = cyl_m5e10_2048_b0_5250[:,8]
omega_cyl_2048_b0_m5e10_avg_5250 = cyl_m5e10_2048_b0_5250[:,9]
cyl_m5e10_2048_b0_7250 = np.genfromtxt(fname="2048_B0/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_b0_m5e10_avg_7250 = cyl_m5e10_2048_b0_7250[:,0]
rho_cyl_2048_b0_m5e10_avg_7250 = cyl_m5e10_2048_b0_7250[:,8]
omega_cyl_2048_b0_m5e10_avg_7250 = cyl_m5e10_2048_b0_7250[:,9]
cyl_m5e10_2048_b0_9250 = np.genfromtxt(fname="2048_B0/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_b0_m5e10_avg_9250 = cyl_m5e10_2048_b0_9250[:,0]
rho_cyl_2048_b0_m5e10_avg_9250 = cyl_m5e10_2048_b0_9250[:,8]
omega_cyl_2048_b0_m5e10_avg_9250 = cyl_m5e10_2048_b0_9250[:,9]

cyl_flat_m5e10_2048_b0_4250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
omega_cyl_flat_2048_b0_m5e10_avg_4250 = cyl_flat_m5e10_2048_b0_4250[:,9]
cyl_flat_m5e10_2048_b0_5250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
r_cyl_flat_2048_b0_m5e10_avg_5250 = cyl_flat_m5e10_2048_b0_5250[:,0]
rho_cyl_flat_2048_b0_m5e10_avg_5250 = cyl_flat_m5e10_2048_b0_5250[:,8]
omega_cyl_flat_2048_b0_m5e10_avg_5250 = cyl_flat_m5e10_2048_b0_5250[:,9]
cyl_flat_m5e10_2048_b0_7250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
omega_cyl_flat_2048_b0_m5e10_avg_7250 = cyl_flat_m5e10_2048_b0_7250[:,9]
cyl_flat_m5e10_2048_b0_9250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_9250.0.out")
r_cyl_flat_2048_b0_m5e10_avg_9250 = cyl_flat_m5e10_2048_b0_9250[:,0]
rho_cyl_flat_2048_b0_m5e10_avg_9250 = cyl_flat_m5e10_2048_b0_9250[:,8]
omega_cyl_flat_2048_b0_m5e10_avg_9250 = cyl_flat_m5e10_2048_b0_9250[:,9]
cyl_flat_m5e10_2048_b0_11250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_11250.0.out")
omega_cyl_flat_2048_b0_m5e10_avg_11250 = cyl_flat_m5e10_2048_b0_11250[:,9]
cyl_flat_m5e10_2048_b0_13000 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask5e10/box60_r_flat/averages_cylinder_13000.0.out")
r_cyl_flat_2048_b0_m5e10_avg_13000 = cyl_flat_m5e10_2048_b0_13000[:,0]
rho_cyl_flat_2048_b0_m5e10_avg_13000 = cyl_flat_m5e10_2048_b0_13000[:,8]
omega_cyl_flat_2048_b0_m5e10_avg_13000 = cyl_flat_m5e10_2048_b0_13000[:,9]


cyl_m1e13_2048_b0_4250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
omega_cyl_2048_b0_m1e13_avg_4250 = cyl_m1e13_2048_b0_4250[:,9]
cyl_m1e13_2048_b0_5250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_b0_m1e13_avg_5250 = cyl_m1e13_2048_b0_5250[:,0]
rho_cyl_2048_b0_m1e13_avg_5250 = cyl_m1e13_2048_b0_5250[:,8]
omega_cyl_2048_b0_m1e13_avg_5250 = cyl_m1e13_2048_b0_5250[:,9]
cyl_m1e13_2048_b0_7250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_2048_b0_m1e13_avg_7250 = cyl_m1e13_2048_b0_7250[:,9]
cyl_m1e13_2048_b0_9250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_b0_m1e13_avg_9250 = cyl_m1e13_2048_b0_9250[:,0]
rho_cyl_2048_b0_m1e13_avg_9250 = cyl_m1e13_2048_b0_9250[:,8]
omega_cyl_2048_b0_m1e13_avg_9250 = cyl_m1e13_2048_b0_9250[:,9]
cyl_m1e13_2048_b0_11250 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
omega_cyl_2048_b0_m1e13_avg_11250 = cyl_m1e13_2048_b0_11250[:,9]
cyl_m1e13_2048_b0_13000 = np.genfromtxt(fname="2048_B0/Cylinder/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
omega_cyl_2048_b0_m1e13_avg_13000 = cyl_m1e13_2048_b0_13000[:,9]


cyl_flat_m5e10_4096_4250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
r_cyl_flat_4096_m5e10_avg_4250 = cyl_flat_m5e10_4096_4250[:,0]
rho_cyl_flat_4096_m5e10_avg_4250 = cyl_flat_m5e10_4096_4250[:,8]
omega_cyl_flat_4096_m5e10_avg_4250 = cyl_flat_m5e10_4096_4250[:,9]

cyl_flat_m5e10_4096_5250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
r_cyl_flat_4096_m5e10_avg_5250 = cyl_flat_m5e10_4096_5250[:,0]
rho_cyl_flat_4096_m5e10_avg_5250 = cyl_flat_m5e10_4096_5250[:,8]
omega_cyl_flat_4096_m5e10_avg_5250 = cyl_flat_m5e10_4096_5250[:,9]

cyl_flat_m5e10_4096_7250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
r_cyl_flat_4096_m5e10_avg_7250 = cyl_flat_m5e10_4096_7250[:,0]
rho_cyl_flat_4096_m5e10_avg_7250 = cyl_flat_m5e10_4096_7250[:,8]
omega_cyl_flat_4096_m5e10_avg_7250 = cyl_flat_m5e10_4096_7250[:,9]

cyl_flat_m5e10_4096_les_4250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_4250.0.out")
r_cyl_flat_4096_les_m5e10_avg_4250 = cyl_flat_m5e10_4096_les_4250[:,0]
rho_cyl_flat_4096_les_m5e10_avg_4250 = cyl_flat_m5e10_4096_les_4250[:,8]
omega_cyl_flat_4096_les_m5e10_avg_4250 = cyl_flat_m5e10_4096_les_4250[:,9]

cyl_flat_m5e10_4096_les_5250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_5250.0.out")
r_cyl_flat_4096_les_m5e10_avg_5250 = cyl_flat_m5e10_4096_les_5250[:,0]
rho_cyl_flat_4096_les_m5e10_avg_5250 = cyl_flat_m5e10_4096_les_5250[:,8]
omega_cyl_flat_4096_les_m5e10_avg_5250 = cyl_flat_m5e10_4096_les_5250[:,9]

cyl_flat_m5e10_4096_les_7250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask5e10/box60_r_flat/averages_cylinder_7250.0.out")
r_cyl_flat_4096_les_m5e10_avg_7250 = cyl_flat_m5e10_4096_les_7250[:,0]
rho_cyl_flat_4096_les_m5e10_avg_7250 = cyl_flat_m5e10_4096_les_7250[:,8]
omega_cyl_flat_4096_les_m5e10_avg_7250 = cyl_flat_m5e10_4096_les_7250[:,9]



cyl_m1e13_4096_4250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_4096_m1e13_avg_4250 = cyl_m1e13_4096_4250[:,0]
omega_cyl_4096_m1e13_avg_4250 = cyl_m1e13_4096_4250[:,9]

cyl_m1e13_4096_5250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
omega_cyl_4096_m1e13_avg_5250 = cyl_m1e13_4096_5250[:,9]

cyl_m1e13_4096_7250 = np.genfromtxt(fname="4096/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_4096_m1e13_avg_7250 = cyl_m1e13_4096_7250[:,9]

cyl_m1e13_4096_les_4250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_4096_les_m1e13_avg_4250 = cyl_m1e13_4096_les_4250[:,0]
omega_cyl_4096_les_m1e13_avg_4250 = cyl_m1e13_4096_les_4250[:,9]

cyl_m1e13_4096_les_5250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
omega_cyl_4096_les_m1e13_avg_5250 = cyl_m1e13_4096_les_5250[:,9]

cyl_m1e13_4096_les_7250 = np.genfromtxt(fname="4096_LES/Cylinder/level9_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
omega_cyl_4096_les_m1e13_avg_7250 = cyl_m1e13_4096_les_7250[:,9]



############ Harmonics ######################

harm_avg_2048_les_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c00.dat")
t_harm_avg_2048_les_c00 = harm_avg_2048_les_c00[:,0]
v_harm_avg_2048_les_c00 = harm_avg_2048_les_c00[:,1]**2 + harm_avg_2048_les_c00[:,2]**2
harm_avg_2048_les_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c10.dat")
t_harm_avg_2048_les_c10 = harm_avg_2048_les_c10[:,0]
v_harm_avg_2048_les_c10 = harm_avg_2048_les_c10[:,1]**2 + harm_avg_2048_les_c10[:,2]**2
harm_avg_2048_les_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c20.dat")
t_harm_avg_2048_les_c20 = harm_avg_2048_les_c20[:,0]
v_harm_avg_2048_les_c20 = harm_avg_2048_les_c20[:,1]**2 + harm_avg_2048_les_c20[:,2]**2
harm_avg_2048_les_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c30.dat")
t_harm_avg_2048_les_c30 = harm_avg_2048_les_c30[:,0]
v_harm_avg_2048_les_c30 = harm_avg_2048_les_c30[:,1]**2 + harm_avg_2048_les_c30[:,2]**2
harm_avg_2048_les_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c40.dat")
t_harm_avg_2048_les_c40 = harm_avg_2048_les_c40[:,0]
v_harm_avg_2048_les_c40 = harm_avg_2048_les_c40[:,1]**2 + harm_avg_2048_les_c40[:,2]**2
harm_avg_2048_les_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c50.dat")
t_harm_avg_2048_les_c50 = harm_avg_2048_les_c50[:,0]
v_harm_avg_2048_les_c50 = harm_avg_2048_les_c50[:,1]**2 + harm_avg_2048_les_c50[:,2]**2
harm_avg_2048_les_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c60.dat")
t_harm_avg_2048_les_c60 = harm_avg_2048_les_c60[:,0]
v_harm_avg_2048_les_c60 = harm_avg_2048_les_c60[:,1]**2 + harm_avg_2048_les_c60[:,2]**2
harm_avg_2048_les_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c70.dat")
t_harm_avg_2048_les_c70 = harm_avg_2048_les_c70[:,0]
v_harm_avg_2048_les_c70 = harm_avg_2048_les_c70[:,1]**2 + harm_avg_2048_les_c70[:,2]**2
harm_avg_2048_les_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c80.dat")
t_harm_avg_2048_les_c80 = harm_avg_2048_les_c80[:,0]
v_harm_avg_2048_les_c80 = harm_avg_2048_les_c80[:,1]**2 + harm_avg_2048_les_c80[:,2]**2

harm_avg_2048_les_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c1p1.dat")
t_harm_avg_2048_les_c1p1 = harm_avg_2048_les_c1p1[:,0]
v_harm_avg_2048_les_c1p1 = harm_avg_2048_les_c1p1[:,1]**2 + harm_avg_2048_les_c1p1[:,2]**2
harm_avg_2048_les_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2p1.dat")
t_harm_avg_2048_les_c2p1 = harm_avg_2048_les_c2p1[:,0]
v_harm_avg_2048_les_c2p1 = harm_avg_2048_les_c2p1[:,1]**2 + harm_avg_2048_les_c2p1[:,2]**2
harm_avg_2048_les_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p1.dat")
t_harm_avg_2048_les_c3p1 = harm_avg_2048_les_c3p1[:,0]
v_harm_avg_2048_les_c3p1 = harm_avg_2048_les_c3p1[:,1]**2 + harm_avg_2048_les_c3p1[:,2]**2
harm_avg_2048_les_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p1.dat")
t_harm_avg_2048_les_c4p1 = harm_avg_2048_les_c4p1[:,0]
v_harm_avg_2048_les_c4p1 = harm_avg_2048_les_c4p1[:,1]**2 + harm_avg_2048_les_c4p1[:,2]**2
harm_avg_2048_les_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p1.dat")
t_harm_avg_2048_les_c5p1 = harm_avg_2048_les_c5p1[:,0]
v_harm_avg_2048_les_c5p1 = harm_avg_2048_les_c5p1[:,1]**2 + harm_avg_2048_les_c5p1[:,2]**2
harm_avg_2048_les_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p1.dat")
t_harm_avg_2048_les_c6p1 = harm_avg_2048_les_c6p1[:,0]
v_harm_avg_2048_les_c6p1 = harm_avg_2048_les_c6p1[:,1]**2 + harm_avg_2048_les_c6p1[:,2]**2
harm_avg_2048_les_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p1.dat")
t_harm_avg_2048_les_c7p1 = harm_avg_2048_les_c7p1[:,0]
v_harm_avg_2048_les_c7p1 = harm_avg_2048_les_c7p1[:,1]**2 + harm_avg_2048_les_c7p1[:,2]**2
harm_avg_2048_les_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p1.dat")
t_harm_avg_2048_les_c8p1 = harm_avg_2048_les_c8p1[:,0]
v_harm_avg_2048_les_c8p1 = harm_avg_2048_les_c8p1[:,1]**2 + harm_avg_2048_les_c8p1[:,2]**2

harm_avg_2048_les_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c1m1.dat")
t_harm_avg_2048_les_c1m1 = harm_avg_2048_les_c1m1[:,0]
v_harm_avg_2048_les_c1m1 = harm_avg_2048_les_c1m1[:,1]**2 + harm_avg_2048_les_c1m1[:,2]**2
harm_avg_2048_les_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2m1.dat")
t_harm_avg_2048_les_c2m1 = harm_avg_2048_les_c2m1[:,0]
v_harm_avg_2048_les_c2m1 = harm_avg_2048_les_c2m1[:,1]**2 + harm_avg_2048_les_c2m1[:,2]**2
harm_avg_2048_les_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m1.dat")
t_harm_avg_2048_les_c3m1 = harm_avg_2048_les_c3m1[:,0]
v_harm_avg_2048_les_c3m1 = harm_avg_2048_les_c3m1[:,1]**2 + harm_avg_2048_les_c3m1[:,2]**2
harm_avg_2048_les_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m1.dat")
t_harm_avg_2048_les_c4m1 = harm_avg_2048_les_c4m1[:,0]
v_harm_avg_2048_les_c4m1 = harm_avg_2048_les_c4m1[:,1]**2 + harm_avg_2048_les_c4m1[:,2]**2
harm_avg_2048_les_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m1.dat")
t_harm_avg_2048_les_c5m1 = harm_avg_2048_les_c5m1[:,0]
v_harm_avg_2048_les_c5m1 = harm_avg_2048_les_c5m1[:,1]**2 + harm_avg_2048_les_c5m1[:,2]**2
harm_avg_2048_les_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m1.dat")
t_harm_avg_2048_les_c6m1 = harm_avg_2048_les_c6m1[:,0]
v_harm_avg_2048_les_c6m1 = harm_avg_2048_les_c6m1[:,1]**2 + harm_avg_2048_les_c6m1[:,2]**2
harm_avg_2048_les_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m1.dat")
t_harm_avg_2048_les_c7m1 = harm_avg_2048_les_c7m1[:,0]
v_harm_avg_2048_les_c7m1 = harm_avg_2048_les_c7m1[:,1]**2 + harm_avg_2048_les_c7m1[:,2]**2
harm_avg_2048_les_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m1.dat")
t_harm_avg_2048_les_c8m1 = harm_avg_2048_les_c8m1[:,0]
v_harm_avg_2048_les_c8m1 = harm_avg_2048_les_c8m1[:,1]**2 + harm_avg_2048_les_c8m1[:,2]**2

harm_avg_2048_les_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2p2.dat")
t_harm_avg_2048_les_c2p2 = harm_avg_2048_les_c2p2[:,0]
v_harm_avg_2048_les_c2p2 = harm_avg_2048_les_c2p2[:,1]**2 + harm_avg_2048_les_c2p2[:,2]**2
harm_avg_2048_les_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p2.dat")
t_harm_avg_2048_les_c3p2 = harm_avg_2048_les_c3p2[:,0]
v_harm_avg_2048_les_c3p2 = harm_avg_2048_les_c3p2[:,1]**2 + harm_avg_2048_les_c3p2[:,2]**2
harm_avg_2048_les_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p2.dat")
t_harm_avg_2048_les_c4p2 = harm_avg_2048_les_c4p2[:,0]
v_harm_avg_2048_les_c4p2 = harm_avg_2048_les_c4p2[:,1]**2 + harm_avg_2048_les_c4p2[:,2]**2
harm_avg_2048_les_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p2.dat")
t_harm_avg_2048_les_c5p2 = harm_avg_2048_les_c5p2[:,0]
v_harm_avg_2048_les_c5p2 = harm_avg_2048_les_c5p2[:,1]**2 + harm_avg_2048_les_c5p2[:,2]**2
harm_avg_2048_les_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p2.dat")
t_harm_avg_2048_les_c6p2 = harm_avg_2048_les_c6p2[:,0]
v_harm_avg_2048_les_c6p2 = harm_avg_2048_les_c6p2[:,1]**2 + harm_avg_2048_les_c6p2[:,2]**2
harm_avg_2048_les_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p2.dat")
t_harm_avg_2048_les_c7p2 = harm_avg_2048_les_c7p2[:,0]
v_harm_avg_2048_les_c7p2 = harm_avg_2048_les_c7p2[:,1]**2 + harm_avg_2048_les_c7p2[:,2]**2
harm_avg_2048_les_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p2.dat")
t_harm_avg_2048_les_c8p2 = harm_avg_2048_les_c8p2[:,0]
v_harm_avg_2048_les_c8p2 = harm_avg_2048_les_c8p2[:,1]**2 + harm_avg_2048_les_c8p2[:,2]**2

harm_avg_2048_les_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2m2.dat")
t_harm_avg_2048_les_c2m2 = harm_avg_2048_les_c2m2[:,0]
v_harm_avg_2048_les_c2m2 = harm_avg_2048_les_c2m2[:,1]**2 + harm_avg_2048_les_c2m2[:,2]**2
harm_avg_2048_les_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m2.dat")
t_harm_avg_2048_les_c3m2 = harm_avg_2048_les_c3m2[:,0]
v_harm_avg_2048_les_c3m2 = harm_avg_2048_les_c3m2[:,1]**2 + harm_avg_2048_les_c3m2[:,2]**2
harm_avg_2048_les_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m2.dat")
t_harm_avg_2048_les_c4m2 = harm_avg_2048_les_c4m2[:,0]
v_harm_avg_2048_les_c4m2 = harm_avg_2048_les_c4m2[:,1]**2 + harm_avg_2048_les_c4m2[:,2]**2
harm_avg_2048_les_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m2.dat")
t_harm_avg_2048_les_c5m2 = harm_avg_2048_les_c5m2[:,0]
v_harm_avg_2048_les_c5m2 = harm_avg_2048_les_c5m2[:,1]**2 + harm_avg_2048_les_c5m2[:,2]**2
harm_avg_2048_les_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m2.dat")
t_harm_avg_2048_les_c6m2 = harm_avg_2048_les_c6m2[:,0]
v_harm_avg_2048_les_c6m2 = harm_avg_2048_les_c6m2[:,1]**2 + harm_avg_2048_les_c6m2[:,2]**2
harm_avg_2048_les_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m2.dat")
t_harm_avg_2048_les_c7m2 = harm_avg_2048_les_c7m2[:,0]
v_harm_avg_2048_les_c7m2 = harm_avg_2048_les_c7m2[:,1]**2 + harm_avg_2048_les_c7m2[:,2]**2
harm_avg_2048_les_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m2.dat")
t_harm_avg_2048_les_c8m2 = harm_avg_2048_les_c8m2[:,0]
v_harm_avg_2048_les_c8m2 = harm_avg_2048_les_c8m2[:,1]**2 + harm_avg_2048_les_c8m2[:,2]**2

harm_avg_2048_les_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p3.dat")
t_harm_avg_2048_les_c3p3 = harm_avg_2048_les_c3p3[:,0]
v_harm_avg_2048_les_c3p3 = harm_avg_2048_les_c3p3[:,1]**2 + harm_avg_2048_les_c3p3[:,2]**2
harm_avg_2048_les_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p3.dat")
t_harm_avg_2048_les_c4p3 = harm_avg_2048_les_c4p3[:,0]
v_harm_avg_2048_les_c4p3 = harm_avg_2048_les_c4p3[:,1]**2 + harm_avg_2048_les_c4p3[:,2]**2
harm_avg_2048_les_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p3.dat")
t_harm_avg_2048_les_c5p3 = harm_avg_2048_les_c5p3[:,0]
v_harm_avg_2048_les_c5p3 = harm_avg_2048_les_c5p3[:,1]**2 + harm_avg_2048_les_c5p3[:,2]**2
harm_avg_2048_les_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p3.dat")
t_harm_avg_2048_les_c6p3 = harm_avg_2048_les_c6p3[:,0]
v_harm_avg_2048_les_c6p3 = harm_avg_2048_les_c6p3[:,1]**2 + harm_avg_2048_les_c6p3[:,2]**2
harm_avg_2048_les_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p3.dat")
t_harm_avg_2048_les_c7p3 = harm_avg_2048_les_c7p3[:,0]
v_harm_avg_2048_les_c7p3 = harm_avg_2048_les_c7p3[:,1]**2 + harm_avg_2048_les_c7p3[:,2]**2
harm_avg_2048_les_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p3.dat")
t_harm_avg_2048_les_c8p3 = harm_avg_2048_les_c8p3[:,0]
v_harm_avg_2048_les_c8p3 = harm_avg_2048_les_c8p3[:,1]**2 + harm_avg_2048_les_c8p3[:,2]**2

harm_avg_2048_les_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m3.dat")
t_harm_avg_2048_les_c3m3 = harm_avg_2048_les_c3m3[:,0]
v_harm_avg_2048_les_c3m3 = harm_avg_2048_les_c3m3[:,1]**2 + harm_avg_2048_les_c3m3[:,2]**2
harm_avg_2048_les_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m3.dat")
t_harm_avg_2048_les_c4m3 = harm_avg_2048_les_c4m3[:,0]
v_harm_avg_2048_les_c4m3 = harm_avg_2048_les_c4m3[:,1]**2 + harm_avg_2048_les_c4m3[:,2]**2
harm_avg_2048_les_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m3.dat")
t_harm_avg_2048_les_c5m3 = harm_avg_2048_les_c5m3[:,0]
v_harm_avg_2048_les_c5m3 = harm_avg_2048_les_c5m3[:,1]**2 + harm_avg_2048_les_c5m3[:,2]**2
harm_avg_2048_les_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m3.dat")
t_harm_avg_2048_les_c6m3 = harm_avg_2048_les_c6m3[:,0]
v_harm_avg_2048_les_c6m3 = harm_avg_2048_les_c6m3[:,1]**2 + harm_avg_2048_les_c6m3[:,2]**2
harm_avg_2048_les_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m3.dat")
t_harm_avg_2048_les_c7m3 = harm_avg_2048_les_c7m3[:,0]
v_harm_avg_2048_les_c7m3 = harm_avg_2048_les_c7m3[:,1]**2 + harm_avg_2048_les_c7m3[:,2]**2
harm_avg_2048_les_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m3.dat")
t_harm_avg_2048_les_c8m3 = harm_avg_2048_les_c8m3[:,0]
v_harm_avg_2048_les_c8m3 = harm_avg_2048_les_c8m3[:,1]**2 + harm_avg_2048_les_c8m3[:,2]**2

harm_avg_2048_les_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p4.dat")
t_harm_avg_2048_les_c4p4 = harm_avg_2048_les_c4p4[:,0]
v_harm_avg_2048_les_c4p4 = harm_avg_2048_les_c4p4[:,1]**2 + harm_avg_2048_les_c4p4[:,2]**2
harm_avg_2048_les_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p4.dat")
t_harm_avg_2048_les_c5p4 = harm_avg_2048_les_c5p4[:,0]
v_harm_avg_2048_les_c5p4 = harm_avg_2048_les_c5p4[:,1]**2 + harm_avg_2048_les_c5p4[:,2]**2
harm_avg_2048_les_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p4.dat")
t_harm_avg_2048_les_c6p4 = harm_avg_2048_les_c6p4[:,0]
v_harm_avg_2048_les_c6p4 = harm_avg_2048_les_c6p4[:,1]**2 + harm_avg_2048_les_c6p4[:,2]**2
harm_avg_2048_les_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p4.dat")
t_harm_avg_2048_les_c7p4 = harm_avg_2048_les_c7p4[:,0]
v_harm_avg_2048_les_c7p4 = harm_avg_2048_les_c7p4[:,1]**2 + harm_avg_2048_les_c7p4[:,2]**2
harm_avg_2048_les_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p4.dat")
t_harm_avg_2048_les_c8p4 = harm_avg_2048_les_c8p4[:,0]
v_harm_avg_2048_les_c8p4 = harm_avg_2048_les_c8p4[:,1]**2 + harm_avg_2048_les_c8p4[:,2]**2

harm_avg_2048_les_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m4.dat")
t_harm_avg_2048_les_c4m4 = harm_avg_2048_les_c4m4[:,0]
v_harm_avg_2048_les_c4m4 = harm_avg_2048_les_c4m4[:,1]**2 + harm_avg_2048_les_c4m4[:,2]**2
harm_avg_2048_les_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m4.dat")
t_harm_avg_2048_les_c5m4 = harm_avg_2048_les_c5m4[:,0]
v_harm_avg_2048_les_c5m4 = harm_avg_2048_les_c5m4[:,1]**2 + harm_avg_2048_les_c5m4[:,2]**2
harm_avg_2048_les_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m4.dat")
t_harm_avg_2048_les_c6m4 = harm_avg_2048_les_c6m4[:,0]
v_harm_avg_2048_les_c6m4 = harm_avg_2048_les_c6m4[:,1]**2 + harm_avg_2048_les_c6m4[:,2]**2
harm_avg_2048_les_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m4.dat")
t_harm_avg_2048_les_c7m4 = harm_avg_2048_les_c7m4[:,0]
v_harm_avg_2048_les_c7m4 = harm_avg_2048_les_c7m4[:,1]**2 + harm_avg_2048_les_c7m4[:,2]**2
harm_avg_2048_les_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m4.dat")
t_harm_avg_2048_les_c8m4 = harm_avg_2048_les_c8m4[:,0]
v_harm_avg_2048_les_c8m4 = harm_avg_2048_les_c8m4[:,1]**2 + harm_avg_2048_les_c8m4[:,2]**2

harm_avg_2048_les_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p5.dat")
t_harm_avg_2048_les_c5p5 = harm_avg_2048_les_c5p5[:,0]
v_harm_avg_2048_les_c5p5 = harm_avg_2048_les_c5p5[:,1]**2 + harm_avg_2048_les_c5p5[:,2]**2
harm_avg_2048_les_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p5.dat")
t_harm_avg_2048_les_c6p5 = harm_avg_2048_les_c6p5[:,0]
v_harm_avg_2048_les_c6p5 = harm_avg_2048_les_c6p5[:,1]**2 + harm_avg_2048_les_c6p5[:,2]**2
harm_avg_2048_les_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p5.dat")
t_harm_avg_2048_les_c7p5 = harm_avg_2048_les_c7p5[:,0]
v_harm_avg_2048_les_c7p5 = harm_avg_2048_les_c7p5[:,1]**2 + harm_avg_2048_les_c7p5[:,2]**2
harm_avg_2048_les_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p5.dat")
t_harm_avg_2048_les_c8p5 = harm_avg_2048_les_c8p5[:,0]
v_harm_avg_2048_les_c8p5 = harm_avg_2048_les_c8p5[:,1]**2 + harm_avg_2048_les_c8p5[:,2]**2

harm_avg_2048_les_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m5.dat")
t_harm_avg_2048_les_c5m5 = harm_avg_2048_les_c5m5[:,0]
v_harm_avg_2048_les_c5m5 = harm_avg_2048_les_c5m5[:,1]**2 + harm_avg_2048_les_c5m5[:,2]**2
harm_avg_2048_les_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m5.dat")
t_harm_avg_2048_les_c6m5 = harm_avg_2048_les_c6m5[:,0]
v_harm_avg_2048_les_c6m5 = harm_avg_2048_les_c6m5[:,1]**2 + harm_avg_2048_les_c6m5[:,2]**2
harm_avg_2048_les_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m5.dat")
t_harm_avg_2048_les_c7m5 = harm_avg_2048_les_c7m5[:,0]
v_harm_avg_2048_les_c7m5 = harm_avg_2048_les_c7m5[:,1]**2 + harm_avg_2048_les_c7m5[:,2]**2
harm_avg_2048_les_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m5.dat")
t_harm_avg_2048_les_c8m5 = harm_avg_2048_les_c8m5[:,0]
v_harm_avg_2048_les_c8m5 = harm_avg_2048_les_c8m5[:,1]**2 + harm_avg_2048_les_c8m5[:,2]**2

harm_avg_2048_les_2L_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c00.dat")
t_harm_avg_2048_les_2L_c00 = harm_avg_2048_les_2L_c00[:,0]
v_harm_avg_2048_les_2L_c00 = harm_avg_2048_les_2L_c00[:,1]**2 + harm_avg_2048_les_2L_c00[:,2]**2
harm_avg_2048_les_2L_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c10.dat")
t_harm_avg_2048_les_2L_c10 = harm_avg_2048_les_2L_c10[:,0]
v_harm_avg_2048_les_2L_c10 = harm_avg_2048_les_2L_c10[:,1]**2 + harm_avg_2048_les_2L_c10[:,2]**2
harm_avg_2048_les_2L_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c20.dat")
t_harm_avg_2048_les_2L_c20 = harm_avg_2048_les_2L_c20[:,0]
v_harm_avg_2048_les_2L_c20 = harm_avg_2048_les_2L_c20[:,1]**2 + harm_avg_2048_les_2L_c20[:,2]**2
harm_avg_2048_les_2L_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c30.dat")
t_harm_avg_2048_les_2L_c30 = harm_avg_2048_les_2L_c30[:,0]
v_harm_avg_2048_les_2L_c30 = harm_avg_2048_les_2L_c30[:,1]**2 + harm_avg_2048_les_2L_c30[:,2]**2
harm_avg_2048_les_2L_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c40.dat")
t_harm_avg_2048_les_2L_c40 = harm_avg_2048_les_2L_c40[:,0]
v_harm_avg_2048_les_2L_c40 = harm_avg_2048_les_2L_c40[:,1]**2 + harm_avg_2048_les_2L_c40[:,2]**2
harm_avg_2048_les_2L_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c50.dat")
t_harm_avg_2048_les_2L_c50 = harm_avg_2048_les_2L_c50[:,0]
v_harm_avg_2048_les_2L_c50 = harm_avg_2048_les_2L_c50[:,1]**2 + harm_avg_2048_les_2L_c50[:,2]**2
harm_avg_2048_les_2L_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c60.dat")
t_harm_avg_2048_les_2L_c60 = harm_avg_2048_les_2L_c60[:,0]
v_harm_avg_2048_les_2L_c60 = harm_avg_2048_les_2L_c60[:,1]**2 + harm_avg_2048_les_2L_c60[:,2]**2
harm_avg_2048_les_2L_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c70.dat")
t_harm_avg_2048_les_2L_c70 = harm_avg_2048_les_2L_c70[:,0]
v_harm_avg_2048_les_2L_c70 = harm_avg_2048_les_2L_c70[:,1]**2 + harm_avg_2048_les_2L_c70[:,2]**2
harm_avg_2048_les_2L_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c80.dat")
t_harm_avg_2048_les_2L_c80 = harm_avg_2048_les_2L_c80[:,0]
v_harm_avg_2048_les_2L_c80 = harm_avg_2048_les_2L_c80[:,1]**2 + harm_avg_2048_les_2L_c80[:,2]**2

harm_avg_2048_les_2L_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c1p1.dat")
t_harm_avg_2048_les_2L_c1p1 = harm_avg_2048_les_2L_c1p1[:,0]
v_harm_avg_2048_les_2L_c1p1 = harm_avg_2048_les_2L_c1p1[:,1]**2 + harm_avg_2048_les_2L_c1p1[:,2]**2
harm_avg_2048_les_2L_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2p1.dat")
t_harm_avg_2048_les_2L_c2p1 = harm_avg_2048_les_2L_c2p1[:,0]
v_harm_avg_2048_les_2L_c2p1 = harm_avg_2048_les_2L_c2p1[:,1]**2 + harm_avg_2048_les_2L_c2p1[:,2]**2
harm_avg_2048_les_2L_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p1.dat")
t_harm_avg_2048_les_2L_c3p1 = harm_avg_2048_les_2L_c3p1[:,0]
v_harm_avg_2048_les_2L_c3p1 = harm_avg_2048_les_2L_c3p1[:,1]**2 + harm_avg_2048_les_2L_c3p1[:,2]**2
harm_avg_2048_les_2L_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p1.dat")
t_harm_avg_2048_les_2L_c4p1 = harm_avg_2048_les_2L_c4p1[:,0]
v_harm_avg_2048_les_2L_c4p1 = harm_avg_2048_les_2L_c4p1[:,1]**2 + harm_avg_2048_les_2L_c4p1[:,2]**2
harm_avg_2048_les_2L_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p1.dat")
t_harm_avg_2048_les_2L_c5p1 = harm_avg_2048_les_2L_c5p1[:,0]
v_harm_avg_2048_les_2L_c5p1 = harm_avg_2048_les_2L_c5p1[:,1]**2 + harm_avg_2048_les_2L_c5p1[:,2]**2
harm_avg_2048_les_2L_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p1.dat")
t_harm_avg_2048_les_2L_c6p1 = harm_avg_2048_les_2L_c6p1[:,0]
v_harm_avg_2048_les_2L_c6p1 = harm_avg_2048_les_2L_c6p1[:,1]**2 + harm_avg_2048_les_2L_c6p1[:,2]**2
harm_avg_2048_les_2L_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p1.dat")
t_harm_avg_2048_les_2L_c7p1 = harm_avg_2048_les_2L_c7p1[:,0]
v_harm_avg_2048_les_2L_c7p1 = harm_avg_2048_les_2L_c7p1[:,1]**2 + harm_avg_2048_les_2L_c7p1[:,2]**2
harm_avg_2048_les_2L_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p1.dat")
t_harm_avg_2048_les_2L_c8p1 = harm_avg_2048_les_2L_c8p1[:,0]
v_harm_avg_2048_les_2L_c8p1 = harm_avg_2048_les_2L_c8p1[:,1]**2 + harm_avg_2048_les_2L_c8p1[:,2]**2

harm_avg_2048_les_2L_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c1m1.dat")
t_harm_avg_2048_les_2L_c1m1 = harm_avg_2048_les_2L_c1m1[:,0]
v_harm_avg_2048_les_2L_c1m1 = harm_avg_2048_les_2L_c1m1[:,1]**2 + harm_avg_2048_les_2L_c1m1[:,2]**2
harm_avg_2048_les_2L_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2m1.dat")
t_harm_avg_2048_les_2L_c2m1 = harm_avg_2048_les_2L_c2m1[:,0]
v_harm_avg_2048_les_2L_c2m1 = harm_avg_2048_les_2L_c2m1[:,1]**2 + harm_avg_2048_les_2L_c2m1[:,2]**2
harm_avg_2048_les_2L_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m1.dat")
t_harm_avg_2048_les_2L_c3m1 = harm_avg_2048_les_2L_c3m1[:,0]
v_harm_avg_2048_les_2L_c3m1 = harm_avg_2048_les_2L_c3m1[:,1]**2 + harm_avg_2048_les_2L_c3m1[:,2]**2
harm_avg_2048_les_2L_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m1.dat")
t_harm_avg_2048_les_2L_c4m1 = harm_avg_2048_les_2L_c4m1[:,0]
v_harm_avg_2048_les_2L_c4m1 = harm_avg_2048_les_2L_c4m1[:,1]**2 + harm_avg_2048_les_2L_c4m1[:,2]**2
harm_avg_2048_les_2L_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m1.dat")
t_harm_avg_2048_les_2L_c5m1 = harm_avg_2048_les_2L_c5m1[:,0]
v_harm_avg_2048_les_2L_c5m1 = harm_avg_2048_les_2L_c5m1[:,1]**2 + harm_avg_2048_les_2L_c5m1[:,2]**2
harm_avg_2048_les_2L_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m1.dat")
t_harm_avg_2048_les_2L_c6m1 = harm_avg_2048_les_2L_c6m1[:,0]
v_harm_avg_2048_les_2L_c6m1 = harm_avg_2048_les_2L_c6m1[:,1]**2 + harm_avg_2048_les_2L_c6m1[:,2]**2
harm_avg_2048_les_2L_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m1.dat")
t_harm_avg_2048_les_2L_c7m1 = harm_avg_2048_les_2L_c7m1[:,0]
v_harm_avg_2048_les_2L_c7m1 = harm_avg_2048_les_2L_c7m1[:,1]**2 + harm_avg_2048_les_2L_c7m1[:,2]**2
harm_avg_2048_les_2L_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m1.dat")
t_harm_avg_2048_les_2L_c8m1 = harm_avg_2048_les_2L_c8m1[:,0]
v_harm_avg_2048_les_2L_c8m1 = harm_avg_2048_les_2L_c8m1[:,1]**2 + harm_avg_2048_les_2L_c8m1[:,2]**2

harm_avg_2048_les_2L_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2p2.dat")
t_harm_avg_2048_les_2L_c2p2 = harm_avg_2048_les_2L_c2p2[:,0]
v_harm_avg_2048_les_2L_c2p2 = harm_avg_2048_les_2L_c2p2[:,1]**2 + harm_avg_2048_les_2L_c2p2[:,2]**2
harm_avg_2048_les_2L_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p2.dat")
t_harm_avg_2048_les_2L_c3p2 = harm_avg_2048_les_2L_c3p2[:,0]
v_harm_avg_2048_les_2L_c3p2 = harm_avg_2048_les_2L_c3p2[:,1]**2 + harm_avg_2048_les_2L_c3p2[:,2]**2
harm_avg_2048_les_2L_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p2.dat")
t_harm_avg_2048_les_2L_c4p2 = harm_avg_2048_les_2L_c4p2[:,0]
v_harm_avg_2048_les_2L_c4p2 = harm_avg_2048_les_2L_c4p2[:,1]**2 + harm_avg_2048_les_2L_c4p2[:,2]**2
harm_avg_2048_les_2L_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p2.dat")
t_harm_avg_2048_les_2L_c5p2 = harm_avg_2048_les_2L_c5p2[:,0]
v_harm_avg_2048_les_2L_c5p2 = harm_avg_2048_les_2L_c5p2[:,1]**2 + harm_avg_2048_les_2L_c5p2[:,2]**2
harm_avg_2048_les_2L_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p2.dat")
t_harm_avg_2048_les_2L_c6p2 = harm_avg_2048_les_2L_c6p2[:,0]
v_harm_avg_2048_les_2L_c6p2 = harm_avg_2048_les_2L_c6p2[:,1]**2 + harm_avg_2048_les_2L_c6p2[:,2]**2
harm_avg_2048_les_2L_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p2.dat")
t_harm_avg_2048_les_2L_c7p2 = harm_avg_2048_les_2L_c7p2[:,0]
v_harm_avg_2048_les_2L_c7p2 = harm_avg_2048_les_2L_c7p2[:,1]**2 + harm_avg_2048_les_2L_c7p2[:,2]**2
harm_avg_2048_les_2L_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p2.dat")
t_harm_avg_2048_les_2L_c8p2 = harm_avg_2048_les_2L_c8p2[:,0]
v_harm_avg_2048_les_2L_c8p2 = harm_avg_2048_les_2L_c8p2[:,1]**2 + harm_avg_2048_les_2L_c8p2[:,2]**2

harm_avg_2048_les_2L_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2m2.dat")
t_harm_avg_2048_les_2L_c2m2 = harm_avg_2048_les_2L_c2m2[:,0]
v_harm_avg_2048_les_2L_c2m2 = harm_avg_2048_les_2L_c2m2[:,1]**2 + harm_avg_2048_les_2L_c2m2[:,2]**2
harm_avg_2048_les_2L_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m2.dat")
t_harm_avg_2048_les_2L_c3m2 = harm_avg_2048_les_2L_c3m2[:,0]
v_harm_avg_2048_les_2L_c3m2 = harm_avg_2048_les_2L_c3m2[:,1]**2 + harm_avg_2048_les_2L_c3m2[:,2]**2
harm_avg_2048_les_2L_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m2.dat")
t_harm_avg_2048_les_2L_c4m2 = harm_avg_2048_les_2L_c4m2[:,0]
v_harm_avg_2048_les_2L_c4m2 = harm_avg_2048_les_2L_c4m2[:,1]**2 + harm_avg_2048_les_2L_c4m2[:,2]**2
harm_avg_2048_les_2L_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m2.dat")
t_harm_avg_2048_les_2L_c5m2 = harm_avg_2048_les_2L_c5m2[:,0]
v_harm_avg_2048_les_2L_c5m2 = harm_avg_2048_les_2L_c5m2[:,1]**2 + harm_avg_2048_les_2L_c5m2[:,2]**2
harm_avg_2048_les_2L_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m2.dat")
t_harm_avg_2048_les_2L_c6m2 = harm_avg_2048_les_2L_c6m2[:,0]
v_harm_avg_2048_les_2L_c6m2 = harm_avg_2048_les_2L_c6m2[:,1]**2 + harm_avg_2048_les_2L_c6m2[:,2]**2
harm_avg_2048_les_2L_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m2.dat")
t_harm_avg_2048_les_2L_c7m2 = harm_avg_2048_les_2L_c7m2[:,0]
v_harm_avg_2048_les_2L_c7m2 = harm_avg_2048_les_2L_c7m2[:,1]**2 + harm_avg_2048_les_2L_c7m2[:,2]**2
harm_avg_2048_les_2L_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m2.dat")
t_harm_avg_2048_les_2L_c8m2 = harm_avg_2048_les_2L_c8m2[:,0]
v_harm_avg_2048_les_2L_c8m2 = harm_avg_2048_les_2L_c8m2[:,1]**2 + harm_avg_2048_les_2L_c8m2[:,2]**2

harm_avg_2048_les_2L_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p3.dat")
t_harm_avg_2048_les_2L_c3p3 = harm_avg_2048_les_2L_c3p3[:,0]
v_harm_avg_2048_les_2L_c3p3 = harm_avg_2048_les_2L_c3p3[:,1]**2 + harm_avg_2048_les_2L_c3p3[:,2]**2
harm_avg_2048_les_2L_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p3.dat")
t_harm_avg_2048_les_2L_c4p3 = harm_avg_2048_les_2L_c4p3[:,0]
v_harm_avg_2048_les_2L_c4p3 = harm_avg_2048_les_2L_c4p3[:,1]**2 + harm_avg_2048_les_2L_c4p3[:,2]**2
harm_avg_2048_les_2L_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p3.dat")
t_harm_avg_2048_les_2L_c5p3 = harm_avg_2048_les_2L_c5p3[:,0]
v_harm_avg_2048_les_2L_c5p3 = harm_avg_2048_les_2L_c5p3[:,1]**2 + harm_avg_2048_les_2L_c5p3[:,2]**2
harm_avg_2048_les_2L_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p3.dat")
t_harm_avg_2048_les_2L_c6p3 = harm_avg_2048_les_2L_c6p3[:,0]
v_harm_avg_2048_les_2L_c6p3 = harm_avg_2048_les_2L_c6p3[:,1]**2 + harm_avg_2048_les_2L_c6p3[:,2]**2
harm_avg_2048_les_2L_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p3.dat")
t_harm_avg_2048_les_2L_c7p3 = harm_avg_2048_les_2L_c7p3[:,0]
v_harm_avg_2048_les_2L_c7p3 = harm_avg_2048_les_2L_c7p3[:,1]**2 + harm_avg_2048_les_2L_c7p3[:,2]**2
harm_avg_2048_les_2L_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p3.dat")
t_harm_avg_2048_les_2L_c8p3 = harm_avg_2048_les_2L_c8p3[:,0]
v_harm_avg_2048_les_2L_c8p3 = harm_avg_2048_les_2L_c8p3[:,1]**2 + harm_avg_2048_les_2L_c8p3[:,2]**2

harm_avg_2048_les_2L_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m3.dat")
t_harm_avg_2048_les_2L_c3m3 = harm_avg_2048_les_2L_c3m3[:,0]
v_harm_avg_2048_les_2L_c3m3 = harm_avg_2048_les_2L_c3m3[:,1]**2 + harm_avg_2048_les_2L_c3m3[:,2]**2
harm_avg_2048_les_2L_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m3.dat")
t_harm_avg_2048_les_2L_c4m3 = harm_avg_2048_les_2L_c4m3[:,0]
v_harm_avg_2048_les_2L_c4m3 = harm_avg_2048_les_2L_c4m3[:,1]**2 + harm_avg_2048_les_2L_c4m3[:,2]**2
harm_avg_2048_les_2L_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m3.dat")
t_harm_avg_2048_les_2L_c5m3 = harm_avg_2048_les_2L_c5m3[:,0]
v_harm_avg_2048_les_2L_c5m3 = harm_avg_2048_les_2L_c5m3[:,1]**2 + harm_avg_2048_les_2L_c5m3[:,2]**2
harm_avg_2048_les_2L_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m3.dat")
t_harm_avg_2048_les_2L_c6m3 = harm_avg_2048_les_2L_c6m3[:,0]
v_harm_avg_2048_les_2L_c6m3 = harm_avg_2048_les_2L_c6m3[:,1]**2 + harm_avg_2048_les_2L_c6m3[:,2]**2
harm_avg_2048_les_2L_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m3.dat")
t_harm_avg_2048_les_2L_c7m3 = harm_avg_2048_les_2L_c7m3[:,0]
v_harm_avg_2048_les_2L_c7m3 = harm_avg_2048_les_2L_c7m3[:,1]**2 + harm_avg_2048_les_2L_c7m3[:,2]**2
harm_avg_2048_les_2L_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m3.dat")
t_harm_avg_2048_les_2L_c8m3 = harm_avg_2048_les_2L_c8m3[:,0]
v_harm_avg_2048_les_2L_c8m3 = harm_avg_2048_les_2L_c8m3[:,1]**2 + harm_avg_2048_les_2L_c8m3[:,2]**2

harm_avg_2048_les_2L_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p4.dat")
t_harm_avg_2048_les_2L_c4p4 = harm_avg_2048_les_2L_c4p4[:,0]
v_harm_avg_2048_les_2L_c4p4 = harm_avg_2048_les_2L_c4p4[:,1]**2 + harm_avg_2048_les_2L_c4p4[:,2]**2
harm_avg_2048_les_2L_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p4.dat")
t_harm_avg_2048_les_2L_c5p4 = harm_avg_2048_les_2L_c5p4[:,0]
v_harm_avg_2048_les_2L_c5p4 = harm_avg_2048_les_2L_c5p4[:,1]**2 + harm_avg_2048_les_2L_c5p4[:,2]**2
harm_avg_2048_les_2L_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p4.dat")
t_harm_avg_2048_les_2L_c6p4 = harm_avg_2048_les_2L_c6p4[:,0]
v_harm_avg_2048_les_2L_c6p4 = harm_avg_2048_les_2L_c6p4[:,1]**2 + harm_avg_2048_les_2L_c6p4[:,2]**2
harm_avg_2048_les_2L_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p4.dat")
t_harm_avg_2048_les_2L_c7p4 = harm_avg_2048_les_2L_c7p4[:,0]
v_harm_avg_2048_les_2L_c7p4 = harm_avg_2048_les_2L_c7p4[:,1]**2 + harm_avg_2048_les_2L_c7p4[:,2]**2
harm_avg_2048_les_2L_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p4.dat")
t_harm_avg_2048_les_2L_c8p4 = harm_avg_2048_les_2L_c8p4[:,0]
v_harm_avg_2048_les_2L_c8p4 = harm_avg_2048_les_2L_c8p4[:,1]**2 + harm_avg_2048_les_2L_c8p4[:,2]**2

harm_avg_2048_les_2L_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m4.dat")
t_harm_avg_2048_les_2L_c4m4 = harm_avg_2048_les_2L_c4m4[:,0]
v_harm_avg_2048_les_2L_c4m4 = harm_avg_2048_les_2L_c4m4[:,1]**2 + harm_avg_2048_les_2L_c4m4[:,2]**2
harm_avg_2048_les_2L_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m4.dat")
t_harm_avg_2048_les_2L_c5m4 = harm_avg_2048_les_2L_c5m4[:,0]
v_harm_avg_2048_les_2L_c5m4 = harm_avg_2048_les_2L_c5m4[:,1]**2 + harm_avg_2048_les_2L_c5m4[:,2]**2
harm_avg_2048_les_2L_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m4.dat")
t_harm_avg_2048_les_2L_c6m4 = harm_avg_2048_les_2L_c6m4[:,0]
v_harm_avg_2048_les_2L_c6m4 = harm_avg_2048_les_2L_c6m4[:,1]**2 + harm_avg_2048_les_2L_c6m4[:,2]**2
harm_avg_2048_les_2L_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m4.dat")
t_harm_avg_2048_les_2L_c7m4 = harm_avg_2048_les_2L_c7m4[:,0]
v_harm_avg_2048_les_2L_c7m4 = harm_avg_2048_les_2L_c7m4[:,1]**2 + harm_avg_2048_les_2L_c7m4[:,2]**2
harm_avg_2048_les_2L_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m4.dat")
t_harm_avg_2048_les_2L_c8m4 = harm_avg_2048_les_2L_c8m4[:,0]
v_harm_avg_2048_les_2L_c8m4 = harm_avg_2048_les_2L_c8m4[:,1]**2 + harm_avg_2048_les_2L_c8m4[:,2]**2

harm_avg_2048_les_2L_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p5.dat")
t_harm_avg_2048_les_2L_c5p5 = harm_avg_2048_les_2L_c5p5[:,0]
v_harm_avg_2048_les_2L_c5p5 = harm_avg_2048_les_2L_c5p5[:,1]**2 + harm_avg_2048_les_2L_c5p5[:,2]**2
harm_avg_2048_les_2L_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p5.dat")
t_harm_avg_2048_les_2L_c6p5 = harm_avg_2048_les_2L_c6p5[:,0]
v_harm_avg_2048_les_2L_c6p5 = harm_avg_2048_les_2L_c6p5[:,1]**2 + harm_avg_2048_les_2L_c6p5[:,2]**2
harm_avg_2048_les_2L_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p5.dat")
t_harm_avg_2048_les_2L_c7p5 = harm_avg_2048_les_2L_c7p5[:,0]
v_harm_avg_2048_les_2L_c7p5 = harm_avg_2048_les_2L_c7p5[:,1]**2 + harm_avg_2048_les_2L_c7p5[:,2]**2
harm_avg_2048_les_2L_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p5.dat")
t_harm_avg_2048_les_2L_c8p5 = harm_avg_2048_les_2L_c8p5[:,0]
v_harm_avg_2048_les_2L_c8p5 = harm_avg_2048_les_2L_c8p5[:,1]**2 + harm_avg_2048_les_2L_c8p5[:,2]**2

harm_avg_2048_les_2L_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m5.dat")
t_harm_avg_2048_les_2L_c5m5 = harm_avg_2048_les_2L_c5m5[:,0]
v_harm_avg_2048_les_2L_c5m5 = harm_avg_2048_les_2L_c5m5[:,1]**2 + harm_avg_2048_les_2L_c5m5[:,2]**2
harm_avg_2048_les_2L_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m5.dat")
t_harm_avg_2048_les_2L_c6m5 = harm_avg_2048_les_2L_c6m5[:,0]
v_harm_avg_2048_les_2L_c6m5 = harm_avg_2048_les_2L_c6m5[:,1]**2 + harm_avg_2048_les_2L_c6m5[:,2]**2
harm_avg_2048_les_2L_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m5.dat")
t_harm_avg_2048_les_2L_c7m5 = harm_avg_2048_les_2L_c7m5[:,0]
v_harm_avg_2048_les_2L_c7m5 = harm_avg_2048_les_2L_c7m5[:,1]**2 + harm_avg_2048_les_2L_c7m5[:,2]**2
harm_avg_2048_les_2L_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m5.dat")
t_harm_avg_2048_les_2L_c8m5 = harm_avg_2048_les_2L_c8m5[:,0]
v_harm_avg_2048_les_2L_c8m5 = harm_avg_2048_les_2L_c8m5[:,1]**2 + harm_avg_2048_les_2L_c8m5[:,2]**2

harm_avg_2048_les_2dr_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c00.dat")
t_harm_avg_2048_les_2dr_c00 = harm_avg_2048_les_2dr_c00[:,0]
v_harm_avg_2048_les_2dr_c00 = harm_avg_2048_les_2dr_c00[:,1]**2 + harm_avg_2048_les_2dr_c00[:,2]**2
harm_avg_2048_les_2dr_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c10.dat")
t_harm_avg_2048_les_2dr_c10 = harm_avg_2048_les_2dr_c10[:,0]
v_harm_avg_2048_les_2dr_c10 = harm_avg_2048_les_2dr_c10[:,1]**2 + harm_avg_2048_les_2dr_c10[:,2]**2
harm_avg_2048_les_2dr_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c20.dat")
t_harm_avg_2048_les_2dr_c20 = harm_avg_2048_les_2dr_c20[:,0]
v_harm_avg_2048_les_2dr_c20 = harm_avg_2048_les_2dr_c20[:,1]**2 + harm_avg_2048_les_2dr_c20[:,2]**2
harm_avg_2048_les_2dr_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c30.dat")
t_harm_avg_2048_les_2dr_c30 = harm_avg_2048_les_2dr_c30[:,0]
v_harm_avg_2048_les_2dr_c30 = harm_avg_2048_les_2dr_c30[:,1]**2 + harm_avg_2048_les_2dr_c30[:,2]**2
harm_avg_2048_les_2dr_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c40.dat")
t_harm_avg_2048_les_2dr_c40 = harm_avg_2048_les_2dr_c40[:,0]
v_harm_avg_2048_les_2dr_c40 = harm_avg_2048_les_2dr_c40[:,1]**2 + harm_avg_2048_les_2dr_c40[:,2]**2
harm_avg_2048_les_2dr_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c50.dat")
t_harm_avg_2048_les_2dr_c50 = harm_avg_2048_les_2dr_c50[:,0]
v_harm_avg_2048_les_2dr_c50 = harm_avg_2048_les_2dr_c50[:,1]**2 + harm_avg_2048_les_2dr_c50[:,2]**2
harm_avg_2048_les_2dr_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c60.dat")
t_harm_avg_2048_les_2dr_c60 = harm_avg_2048_les_2dr_c60[:,0]
v_harm_avg_2048_les_2dr_c60 = harm_avg_2048_les_2dr_c60[:,1]**2 + harm_avg_2048_les_2dr_c60[:,2]**2
harm_avg_2048_les_2dr_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c70.dat")
t_harm_avg_2048_les_2dr_c70 = harm_avg_2048_les_2dr_c70[:,0]
v_harm_avg_2048_les_2dr_c70 = harm_avg_2048_les_2dr_c70[:,1]**2 + harm_avg_2048_les_2dr_c70[:,2]**2
harm_avg_2048_les_2dr_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c80.dat")
t_harm_avg_2048_les_2dr_c80 = harm_avg_2048_les_2dr_c80[:,0]
v_harm_avg_2048_les_2dr_c80 = harm_avg_2048_les_2dr_c80[:,1]**2 + harm_avg_2048_les_2dr_c80[:,2]**2

harm_avg_2048_les_2dr_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_les_2dr_c1p1 = harm_avg_2048_les_2dr_c1p1[:,0]
v_harm_avg_2048_les_2dr_c1p1 = harm_avg_2048_les_2dr_c1p1[:,1]**2 + harm_avg_2048_les_2dr_c1p1[:,2]**2
harm_avg_2048_les_2dr_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_les_2dr_c2p1 = harm_avg_2048_les_2dr_c2p1[:,0]
v_harm_avg_2048_les_2dr_c2p1 = harm_avg_2048_les_2dr_c2p1[:,1]**2 + harm_avg_2048_les_2dr_c2p1[:,2]**2
harm_avg_2048_les_2dr_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_les_2dr_c3p1 = harm_avg_2048_les_2dr_c3p1[:,0]
v_harm_avg_2048_les_2dr_c3p1 = harm_avg_2048_les_2dr_c3p1[:,1]**2 + harm_avg_2048_les_2dr_c3p1[:,2]**2
harm_avg_2048_les_2dr_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_les_2dr_c4p1 = harm_avg_2048_les_2dr_c4p1[:,0]
v_harm_avg_2048_les_2dr_c4p1 = harm_avg_2048_les_2dr_c4p1[:,1]**2 + harm_avg_2048_les_2dr_c4p1[:,2]**2
harm_avg_2048_les_2dr_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_les_2dr_c5p1 = harm_avg_2048_les_2dr_c5p1[:,0]
v_harm_avg_2048_les_2dr_c5p1 = harm_avg_2048_les_2dr_c5p1[:,1]**2 + harm_avg_2048_les_2dr_c5p1[:,2]**2
harm_avg_2048_les_2dr_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_les_2dr_c6p1 = harm_avg_2048_les_2dr_c6p1[:,0]
v_harm_avg_2048_les_2dr_c6p1 = harm_avg_2048_les_2dr_c6p1[:,1]**2 + harm_avg_2048_les_2dr_c6p1[:,2]**2
harm_avg_2048_les_2dr_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_les_2dr_c7p1 = harm_avg_2048_les_2dr_c7p1[:,0]
v_harm_avg_2048_les_2dr_c7p1 = harm_avg_2048_les_2dr_c7p1[:,1]**2 + harm_avg_2048_les_2dr_c7p1[:,2]**2
harm_avg_2048_les_2dr_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_les_2dr_c8p1 = harm_avg_2048_les_2dr_c8p1[:,0]
v_harm_avg_2048_les_2dr_c8p1 = harm_avg_2048_les_2dr_c8p1[:,1]**2 + harm_avg_2048_les_2dr_c8p1[:,2]**2

harm_avg_2048_les_2dr_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_les_2dr_c1m1 = harm_avg_2048_les_2dr_c1m1[:,0]
v_harm_avg_2048_les_2dr_c1m1 = harm_avg_2048_les_2dr_c1m1[:,1]**2 + harm_avg_2048_les_2dr_c1m1[:,2]**2
harm_avg_2048_les_2dr_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_les_2dr_c2m1 = harm_avg_2048_les_2dr_c2m1[:,0]
v_harm_avg_2048_les_2dr_c2m1 = harm_avg_2048_les_2dr_c2m1[:,1]**2 + harm_avg_2048_les_2dr_c2m1[:,2]**2
harm_avg_2048_les_2dr_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_les_2dr_c3m1 = harm_avg_2048_les_2dr_c3m1[:,0]
v_harm_avg_2048_les_2dr_c3m1 = harm_avg_2048_les_2dr_c3m1[:,1]**2 + harm_avg_2048_les_2dr_c3m1[:,2]**2
harm_avg_2048_les_2dr_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_les_2dr_c4m1 = harm_avg_2048_les_2dr_c4m1[:,0]
v_harm_avg_2048_les_2dr_c4m1 = harm_avg_2048_les_2dr_c4m1[:,1]**2 + harm_avg_2048_les_2dr_c4m1[:,2]**2
harm_avg_2048_les_2dr_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_les_2dr_c5m1 = harm_avg_2048_les_2dr_c5m1[:,0]
v_harm_avg_2048_les_2dr_c5m1 = harm_avg_2048_les_2dr_c5m1[:,1]**2 + harm_avg_2048_les_2dr_c5m1[:,2]**2
harm_avg_2048_les_2dr_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_les_2dr_c6m1 = harm_avg_2048_les_2dr_c6m1[:,0]
v_harm_avg_2048_les_2dr_c6m1 = harm_avg_2048_les_2dr_c6m1[:,1]**2 + harm_avg_2048_les_2dr_c6m1[:,2]**2
harm_avg_2048_les_2dr_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_les_2dr_c7m1 = harm_avg_2048_les_2dr_c7m1[:,0]
v_harm_avg_2048_les_2dr_c7m1 = harm_avg_2048_les_2dr_c7m1[:,1]**2 + harm_avg_2048_les_2dr_c7m1[:,2]**2
harm_avg_2048_les_2dr_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_les_2dr_c8m1 = harm_avg_2048_les_2dr_c8m1[:,0]
v_harm_avg_2048_les_2dr_c8m1 = harm_avg_2048_les_2dr_c8m1[:,1]**2 + harm_avg_2048_les_2dr_c8m1[:,2]**2

harm_avg_2048_les_2dr_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_les_2dr_c2p2 = harm_avg_2048_les_2dr_c2p2[:,0]
v_harm_avg_2048_les_2dr_c2p2 = harm_avg_2048_les_2dr_c2p2[:,1]**2 + harm_avg_2048_les_2dr_c2p2[:,2]**2
harm_avg_2048_les_2dr_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_les_2dr_c3p2 = harm_avg_2048_les_2dr_c3p2[:,0]
v_harm_avg_2048_les_2dr_c3p2 = harm_avg_2048_les_2dr_c3p2[:,1]**2 + harm_avg_2048_les_2dr_c3p2[:,2]**2
harm_avg_2048_les_2dr_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_les_2dr_c4p2 = harm_avg_2048_les_2dr_c4p2[:,0]
v_harm_avg_2048_les_2dr_c4p2 = harm_avg_2048_les_2dr_c4p2[:,1]**2 + harm_avg_2048_les_2dr_c4p2[:,2]**2
harm_avg_2048_les_2dr_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_les_2dr_c5p2 = harm_avg_2048_les_2dr_c5p2[:,0]
v_harm_avg_2048_les_2dr_c5p2 = harm_avg_2048_les_2dr_c5p2[:,1]**2 + harm_avg_2048_les_2dr_c5p2[:,2]**2
harm_avg_2048_les_2dr_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_les_2dr_c6p2 = harm_avg_2048_les_2dr_c6p2[:,0]
v_harm_avg_2048_les_2dr_c6p2 = harm_avg_2048_les_2dr_c6p2[:,1]**2 + harm_avg_2048_les_2dr_c6p2[:,2]**2
harm_avg_2048_les_2dr_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_les_2dr_c7p2 = harm_avg_2048_les_2dr_c7p2[:,0]
v_harm_avg_2048_les_2dr_c7p2 = harm_avg_2048_les_2dr_c7p2[:,1]**2 + harm_avg_2048_les_2dr_c7p2[:,2]**2
harm_avg_2048_les_2dr_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_les_2dr_c8p2 = harm_avg_2048_les_2dr_c8p2[:,0]
v_harm_avg_2048_les_2dr_c8p2 = harm_avg_2048_les_2dr_c8p2[:,1]**2 + harm_avg_2048_les_2dr_c8p2[:,2]**2

harm_avg_2048_les_2dr_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_les_2dr_c2m2 = harm_avg_2048_les_2dr_c2m2[:,0]
v_harm_avg_2048_les_2dr_c2m2 = harm_avg_2048_les_2dr_c2m2[:,1]**2 + harm_avg_2048_les_2dr_c2m2[:,2]**2
harm_avg_2048_les_2dr_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_les_2dr_c3m2 = harm_avg_2048_les_2dr_c3m2[:,0]
v_harm_avg_2048_les_2dr_c3m2 = harm_avg_2048_les_2dr_c3m2[:,1]**2 + harm_avg_2048_les_2dr_c3m2[:,2]**2
harm_avg_2048_les_2dr_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_les_2dr_c4m2 = harm_avg_2048_les_2dr_c4m2[:,0]
v_harm_avg_2048_les_2dr_c4m2 = harm_avg_2048_les_2dr_c4m2[:,1]**2 + harm_avg_2048_les_2dr_c4m2[:,2]**2
harm_avg_2048_les_2dr_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_les_2dr_c5m2 = harm_avg_2048_les_2dr_c5m2[:,0]
v_harm_avg_2048_les_2dr_c5m2 = harm_avg_2048_les_2dr_c5m2[:,1]**2 + harm_avg_2048_les_2dr_c5m2[:,2]**2
harm_avg_2048_les_2dr_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_les_2dr_c6m2 = harm_avg_2048_les_2dr_c6m2[:,0]
v_harm_avg_2048_les_2dr_c6m2 = harm_avg_2048_les_2dr_c6m2[:,1]**2 + harm_avg_2048_les_2dr_c6m2[:,2]**2
harm_avg_2048_les_2dr_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_les_2dr_c7m2 = harm_avg_2048_les_2dr_c7m2[:,0]
v_harm_avg_2048_les_2dr_c7m2 = harm_avg_2048_les_2dr_c7m2[:,1]**2 + harm_avg_2048_les_2dr_c7m2[:,2]**2
harm_avg_2048_les_2dr_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_les_2dr_c8m2 = harm_avg_2048_les_2dr_c8m2[:,0]
v_harm_avg_2048_les_2dr_c8m2 = harm_avg_2048_les_2dr_c8m2[:,1]**2 + harm_avg_2048_les_2dr_c8m2[:,2]**2

harm_avg_2048_les_2dr_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_les_2dr_c3p3 = harm_avg_2048_les_2dr_c3p3[:,0]
v_harm_avg_2048_les_2dr_c3p3 = harm_avg_2048_les_2dr_c3p3[:,1]**2 + harm_avg_2048_les_2dr_c3p3[:,2]**2
harm_avg_2048_les_2dr_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_les_2dr_c4p3 = harm_avg_2048_les_2dr_c4p3[:,0]
v_harm_avg_2048_les_2dr_c4p3 = harm_avg_2048_les_2dr_c4p3[:,1]**2 + harm_avg_2048_les_2dr_c4p3[:,2]**2
harm_avg_2048_les_2dr_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_les_2dr_c5p3 = harm_avg_2048_les_2dr_c5p3[:,0]
v_harm_avg_2048_les_2dr_c5p3 = harm_avg_2048_les_2dr_c5p3[:,1]**2 + harm_avg_2048_les_2dr_c5p3[:,2]**2
harm_avg_2048_les_2dr_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_les_2dr_c6p3 = harm_avg_2048_les_2dr_c6p3[:,0]
v_harm_avg_2048_les_2dr_c6p3 = harm_avg_2048_les_2dr_c6p3[:,1]**2 + harm_avg_2048_les_2dr_c6p3[:,2]**2
harm_avg_2048_les_2dr_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_les_2dr_c7p3 = harm_avg_2048_les_2dr_c7p3[:,0]
v_harm_avg_2048_les_2dr_c7p3 = harm_avg_2048_les_2dr_c7p3[:,1]**2 + harm_avg_2048_les_2dr_c7p3[:,2]**2
harm_avg_2048_les_2dr_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_les_2dr_c8p3 = harm_avg_2048_les_2dr_c8p3[:,0]
v_harm_avg_2048_les_2dr_c8p3 = harm_avg_2048_les_2dr_c8p3[:,1]**2 + harm_avg_2048_les_2dr_c8p3[:,2]**2

harm_avg_2048_les_2dr_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_les_2dr_c3m3 = harm_avg_2048_les_2dr_c3m3[:,0]
v_harm_avg_2048_les_2dr_c3m3 = harm_avg_2048_les_2dr_c3m3[:,1]**2 + harm_avg_2048_les_2dr_c3m3[:,2]**2
harm_avg_2048_les_2dr_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_les_2dr_c4m3 = harm_avg_2048_les_2dr_c4m3[:,0]
v_harm_avg_2048_les_2dr_c4m3 = harm_avg_2048_les_2dr_c4m3[:,1]**2 + harm_avg_2048_les_2dr_c4m3[:,2]**2
harm_avg_2048_les_2dr_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_les_2dr_c5m3 = harm_avg_2048_les_2dr_c5m3[:,0]
v_harm_avg_2048_les_2dr_c5m3 = harm_avg_2048_les_2dr_c5m3[:,1]**2 + harm_avg_2048_les_2dr_c5m3[:,2]**2
harm_avg_2048_les_2dr_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_les_2dr_c6m3 = harm_avg_2048_les_2dr_c6m3[:,0]
v_harm_avg_2048_les_2dr_c6m3 = harm_avg_2048_les_2dr_c6m3[:,1]**2 + harm_avg_2048_les_2dr_c6m3[:,2]**2
harm_avg_2048_les_2dr_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_les_2dr_c7m3 = harm_avg_2048_les_2dr_c7m3[:,0]
v_harm_avg_2048_les_2dr_c7m3 = harm_avg_2048_les_2dr_c7m3[:,1]**2 + harm_avg_2048_les_2dr_c7m3[:,2]**2
harm_avg_2048_les_2dr_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_les_2dr_c8m3 = harm_avg_2048_les_2dr_c8m3[:,0]
v_harm_avg_2048_les_2dr_c8m3 = harm_avg_2048_les_2dr_c8m3[:,1]**2 + harm_avg_2048_les_2dr_c8m3[:,2]**2

harm_avg_2048_les_2dr_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_les_2dr_c4p4 = harm_avg_2048_les_2dr_c4p4[:,0]
v_harm_avg_2048_les_2dr_c4p4 = harm_avg_2048_les_2dr_c4p4[:,1]**2 + harm_avg_2048_les_2dr_c4p4[:,2]**2
harm_avg_2048_les_2dr_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_les_2dr_c5p4 = harm_avg_2048_les_2dr_c5p4[:,0]
v_harm_avg_2048_les_2dr_c5p4 = harm_avg_2048_les_2dr_c5p4[:,1]**2 + harm_avg_2048_les_2dr_c5p4[:,2]**2
harm_avg_2048_les_2dr_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_les_2dr_c6p4 = harm_avg_2048_les_2dr_c6p4[:,0]
v_harm_avg_2048_les_2dr_c6p4 = harm_avg_2048_les_2dr_c6p4[:,1]**2 + harm_avg_2048_les_2dr_c6p4[:,2]**2
harm_avg_2048_les_2dr_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_les_2dr_c7p4 = harm_avg_2048_les_2dr_c7p4[:,0]
v_harm_avg_2048_les_2dr_c7p4 = harm_avg_2048_les_2dr_c7p4[:,1]**2 + harm_avg_2048_les_2dr_c7p4[:,2]**2
harm_avg_2048_les_2dr_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_les_2dr_c8p4 = harm_avg_2048_les_2dr_c8p4[:,0]
v_harm_avg_2048_les_2dr_c8p4 = harm_avg_2048_les_2dr_c8p4[:,1]**2 + harm_avg_2048_les_2dr_c8p4[:,2]**2

harm_avg_2048_les_2dr_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_les_2dr_c4m4 = harm_avg_2048_les_2dr_c4m4[:,0]
v_harm_avg_2048_les_2dr_c4m4 = harm_avg_2048_les_2dr_c4m4[:,1]**2 + harm_avg_2048_les_2dr_c4m4[:,2]**2
harm_avg_2048_les_2dr_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_les_2dr_c5m4 = harm_avg_2048_les_2dr_c5m4[:,0]
v_harm_avg_2048_les_2dr_c5m4 = harm_avg_2048_les_2dr_c5m4[:,1]**2 + harm_avg_2048_les_2dr_c5m4[:,2]**2
harm_avg_2048_les_2dr_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_les_2dr_c6m4 = harm_avg_2048_les_2dr_c6m4[:,0]
v_harm_avg_2048_les_2dr_c6m4 = harm_avg_2048_les_2dr_c6m4[:,1]**2 + harm_avg_2048_les_2dr_c6m4[:,2]**2
harm_avg_2048_les_2dr_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_les_2dr_c7m4 = harm_avg_2048_les_2dr_c7m4[:,0]
v_harm_avg_2048_les_2dr_c7m4 = harm_avg_2048_les_2dr_c7m4[:,1]**2 + harm_avg_2048_les_2dr_c7m4[:,2]**2
harm_avg_2048_les_2dr_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_les_2dr_c8m4 = harm_avg_2048_les_2dr_c8m4[:,0]
v_harm_avg_2048_les_2dr_c8m4 = harm_avg_2048_les_2dr_c8m4[:,1]**2 + harm_avg_2048_les_2dr_c8m4[:,2]**2

harm_avg_2048_les_2dr_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_les_2dr_c5p5 = harm_avg_2048_les_2dr_c5p5[:,0]
v_harm_avg_2048_les_2dr_c5p5 = harm_avg_2048_les_2dr_c5p5[:,1]**2 + harm_avg_2048_les_2dr_c5p5[:,2]**2
harm_avg_2048_les_2dr_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_les_2dr_c6p5 = harm_avg_2048_les_2dr_c6p5[:,0]
v_harm_avg_2048_les_2dr_c6p5 = harm_avg_2048_les_2dr_c6p5[:,1]**2 + harm_avg_2048_les_2dr_c6p5[:,2]**2
harm_avg_2048_les_2dr_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_les_2dr_c7p5 = harm_avg_2048_les_2dr_c7p5[:,0]
v_harm_avg_2048_les_2dr_c7p5 = harm_avg_2048_les_2dr_c7p5[:,1]**2 + harm_avg_2048_les_2dr_c7p5[:,2]**2
harm_avg_2048_les_2dr_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_les_2dr_c8p5 = harm_avg_2048_les_2dr_c8p5[:,0]
v_harm_avg_2048_les_2dr_c8p5 = harm_avg_2048_les_2dr_c8p5[:,1]**2 + harm_avg_2048_les_2dr_c8p5[:,2]**2

harm_avg_2048_les_2dr_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_les_2dr_c5m5 = harm_avg_2048_les_2dr_c5m5[:,0]
v_harm_avg_2048_les_2dr_c5m5 = harm_avg_2048_les_2dr_c5m5[:,1]**2 + harm_avg_2048_les_2dr_c5m5[:,2]**2
harm_avg_2048_les_2dr_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_les_2dr_c6m5 = harm_avg_2048_les_2dr_c6m5[:,0]
v_harm_avg_2048_les_2dr_c6m5 = harm_avg_2048_les_2dr_c6m5[:,1]**2 + harm_avg_2048_les_2dr_c6m5[:,2]**2
harm_avg_2048_les_2dr_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_les_2dr_c7m5 = harm_avg_2048_les_2dr_c7m5[:,0]
v_harm_avg_2048_les_2dr_c7m5 = harm_avg_2048_les_2dr_c7m5[:,1]**2 + harm_avg_2048_les_2dr_c7m5[:,2]**2
harm_avg_2048_les_2dr_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_les_2dr_c8m5 = harm_avg_2048_les_2dr_c8m5[:,0]
v_harm_avg_2048_les_2dr_c8m5 = harm_avg_2048_les_2dr_c8m5[:,1]**2 + harm_avg_2048_les_2dr_c8m5[:,2]**2


harm_avg_2048_les_1_12_05_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c00.dat")
t_harm_avg_2048_les_1_12_05_c00 = harm_avg_2048_les_1_12_05_c00[:,0]
v_harm_avg_2048_les_1_12_05_c00 = harm_avg_2048_les_1_12_05_c00[:,1]**2 + harm_avg_2048_les_1_12_05_c00[:,2]**2
harm_avg_2048_les_1_12_05_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c10.dat")
t_harm_avg_2048_les_1_12_05_c10 = harm_avg_2048_les_1_12_05_c10[:,0]
v_harm_avg_2048_les_1_12_05_c10 = harm_avg_2048_les_1_12_05_c10[:,1]**2 + harm_avg_2048_les_1_12_05_c10[:,2]**2
harm_avg_2048_les_1_12_05_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c20.dat")
t_harm_avg_2048_les_1_12_05_c20 = harm_avg_2048_les_1_12_05_c20[:,0]
v_harm_avg_2048_les_1_12_05_c20 = harm_avg_2048_les_1_12_05_c20[:,1]**2 + harm_avg_2048_les_1_12_05_c20[:,2]**2
harm_avg_2048_les_1_12_05_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c30.dat")
t_harm_avg_2048_les_1_12_05_c30 = harm_avg_2048_les_1_12_05_c30[:,0]
v_harm_avg_2048_les_1_12_05_c30 = harm_avg_2048_les_1_12_05_c30[:,1]**2 + harm_avg_2048_les_1_12_05_c30[:,2]**2
harm_avg_2048_les_1_12_05_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c40.dat")
t_harm_avg_2048_les_1_12_05_c40 = harm_avg_2048_les_1_12_05_c40[:,0]
v_harm_avg_2048_les_1_12_05_c40 = harm_avg_2048_les_1_12_05_c40[:,1]**2 + harm_avg_2048_les_1_12_05_c40[:,2]**2
harm_avg_2048_les_1_12_05_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c50.dat")
t_harm_avg_2048_les_1_12_05_c50 = harm_avg_2048_les_1_12_05_c50[:,0]
v_harm_avg_2048_les_1_12_05_c50 = harm_avg_2048_les_1_12_05_c50[:,1]**2 + harm_avg_2048_les_1_12_05_c50[:,2]**2
harm_avg_2048_les_1_12_05_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c60.dat")
t_harm_avg_2048_les_1_12_05_c60 = harm_avg_2048_les_1_12_05_c60[:,0]
v_harm_avg_2048_les_1_12_05_c60 = harm_avg_2048_les_1_12_05_c60[:,1]**2 + harm_avg_2048_les_1_12_05_c60[:,2]**2
harm_avg_2048_les_1_12_05_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c70.dat")
t_harm_avg_2048_les_1_12_05_c70 = harm_avg_2048_les_1_12_05_c70[:,0]
v_harm_avg_2048_les_1_12_05_c70 = harm_avg_2048_les_1_12_05_c70[:,1]**2 + harm_avg_2048_les_1_12_05_c70[:,2]**2
harm_avg_2048_les_1_12_05_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c80.dat")
t_harm_avg_2048_les_1_12_05_c80 = harm_avg_2048_les_1_12_05_c80[:,0]
v_harm_avg_2048_les_1_12_05_c80 = harm_avg_2048_les_1_12_05_c80[:,1]**2 + harm_avg_2048_les_1_12_05_c80[:,2]**2

harm_avg_2048_les_1_12_05_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_les_1_12_05_c1p1 = harm_avg_2048_les_1_12_05_c1p1[:,0]
v_harm_avg_2048_les_1_12_05_c1p1 = harm_avg_2048_les_1_12_05_c1p1[:,1]**2 + harm_avg_2048_les_1_12_05_c1p1[:,2]**2
harm_avg_2048_les_1_12_05_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_les_1_12_05_c2p1 = harm_avg_2048_les_1_12_05_c2p1[:,0]
v_harm_avg_2048_les_1_12_05_c2p1 = harm_avg_2048_les_1_12_05_c2p1[:,1]**2 + harm_avg_2048_les_1_12_05_c2p1[:,2]**2
harm_avg_2048_les_1_12_05_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_les_1_12_05_c3p1 = harm_avg_2048_les_1_12_05_c3p1[:,0]
v_harm_avg_2048_les_1_12_05_c3p1 = harm_avg_2048_les_1_12_05_c3p1[:,1]**2 + harm_avg_2048_les_1_12_05_c3p1[:,2]**2
harm_avg_2048_les_1_12_05_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_les_1_12_05_c4p1 = harm_avg_2048_les_1_12_05_c4p1[:,0]
v_harm_avg_2048_les_1_12_05_c4p1 = harm_avg_2048_les_1_12_05_c4p1[:,1]**2 + harm_avg_2048_les_1_12_05_c4p1[:,2]**2
harm_avg_2048_les_1_12_05_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_les_1_12_05_c5p1 = harm_avg_2048_les_1_12_05_c5p1[:,0]
v_harm_avg_2048_les_1_12_05_c5p1 = harm_avg_2048_les_1_12_05_c5p1[:,1]**2 + harm_avg_2048_les_1_12_05_c5p1[:,2]**2
harm_avg_2048_les_1_12_05_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_les_1_12_05_c6p1 = harm_avg_2048_les_1_12_05_c6p1[:,0]
v_harm_avg_2048_les_1_12_05_c6p1 = harm_avg_2048_les_1_12_05_c6p1[:,1]**2 + harm_avg_2048_les_1_12_05_c6p1[:,2]**2
harm_avg_2048_les_1_12_05_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_les_1_12_05_c7p1 = harm_avg_2048_les_1_12_05_c7p1[:,0]
v_harm_avg_2048_les_1_12_05_c7p1 = harm_avg_2048_les_1_12_05_c7p1[:,1]**2 + harm_avg_2048_les_1_12_05_c7p1[:,2]**2
harm_avg_2048_les_1_12_05_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_les_1_12_05_c8p1 = harm_avg_2048_les_1_12_05_c8p1[:,0]
v_harm_avg_2048_les_1_12_05_c8p1 = harm_avg_2048_les_1_12_05_c8p1[:,1]**2 + harm_avg_2048_les_1_12_05_c8p1[:,2]**2

harm_avg_2048_les_1_12_05_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_les_1_12_05_c1m1 = harm_avg_2048_les_1_12_05_c1m1[:,0]
v_harm_avg_2048_les_1_12_05_c1m1 = harm_avg_2048_les_1_12_05_c1m1[:,1]**2 + harm_avg_2048_les_1_12_05_c1m1[:,2]**2
harm_avg_2048_les_1_12_05_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_les_1_12_05_c2m1 = harm_avg_2048_les_1_12_05_c2m1[:,0]
v_harm_avg_2048_les_1_12_05_c2m1 = harm_avg_2048_les_1_12_05_c2m1[:,1]**2 + harm_avg_2048_les_1_12_05_c2m1[:,2]**2
harm_avg_2048_les_1_12_05_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_les_1_12_05_c3m1 = harm_avg_2048_les_1_12_05_c3m1[:,0]
v_harm_avg_2048_les_1_12_05_c3m1 = harm_avg_2048_les_1_12_05_c3m1[:,1]**2 + harm_avg_2048_les_1_12_05_c3m1[:,2]**2
harm_avg_2048_les_1_12_05_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_les_1_12_05_c4m1 = harm_avg_2048_les_1_12_05_c4m1[:,0]
v_harm_avg_2048_les_1_12_05_c4m1 = harm_avg_2048_les_1_12_05_c4m1[:,1]**2 + harm_avg_2048_les_1_12_05_c4m1[:,2]**2
harm_avg_2048_les_1_12_05_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_les_1_12_05_c5m1 = harm_avg_2048_les_1_12_05_c5m1[:,0]
v_harm_avg_2048_les_1_12_05_c5m1 = harm_avg_2048_les_1_12_05_c5m1[:,1]**2 + harm_avg_2048_les_1_12_05_c5m1[:,2]**2
harm_avg_2048_les_1_12_05_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_les_1_12_05_c6m1 = harm_avg_2048_les_1_12_05_c6m1[:,0]
v_harm_avg_2048_les_1_12_05_c6m1 = harm_avg_2048_les_1_12_05_c6m1[:,1]**2 + harm_avg_2048_les_1_12_05_c6m1[:,2]**2
harm_avg_2048_les_1_12_05_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_les_1_12_05_c7m1 = harm_avg_2048_les_1_12_05_c7m1[:,0]
v_harm_avg_2048_les_1_12_05_c7m1 = harm_avg_2048_les_1_12_05_c7m1[:,1]**2 + harm_avg_2048_les_1_12_05_c7m1[:,2]**2
harm_avg_2048_les_1_12_05_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_les_1_12_05_c8m1 = harm_avg_2048_les_1_12_05_c8m1[:,0]
v_harm_avg_2048_les_1_12_05_c8m1 = harm_avg_2048_les_1_12_05_c8m1[:,1]**2 + harm_avg_2048_les_1_12_05_c8m1[:,2]**2

harm_avg_2048_les_1_12_05_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_les_1_12_05_c2p2 = harm_avg_2048_les_1_12_05_c2p2[:,0]
v_harm_avg_2048_les_1_12_05_c2p2 = harm_avg_2048_les_1_12_05_c2p2[:,1]**2 + harm_avg_2048_les_1_12_05_c2p2[:,2]**2
harm_avg_2048_les_1_12_05_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_les_1_12_05_c3p2 = harm_avg_2048_les_1_12_05_c3p2[:,0]
v_harm_avg_2048_les_1_12_05_c3p2 = harm_avg_2048_les_1_12_05_c3p2[:,1]**2 + harm_avg_2048_les_1_12_05_c3p2[:,2]**2
harm_avg_2048_les_1_12_05_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_les_1_12_05_c4p2 = harm_avg_2048_les_1_12_05_c4p2[:,0]
v_harm_avg_2048_les_1_12_05_c4p2 = harm_avg_2048_les_1_12_05_c4p2[:,1]**2 + harm_avg_2048_les_1_12_05_c4p2[:,2]**2
harm_avg_2048_les_1_12_05_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_les_1_12_05_c5p2 = harm_avg_2048_les_1_12_05_c5p2[:,0]
v_harm_avg_2048_les_1_12_05_c5p2 = harm_avg_2048_les_1_12_05_c5p2[:,1]**2 + harm_avg_2048_les_1_12_05_c5p2[:,2]**2
harm_avg_2048_les_1_12_05_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_les_1_12_05_c6p2 = harm_avg_2048_les_1_12_05_c6p2[:,0]
v_harm_avg_2048_les_1_12_05_c6p2 = harm_avg_2048_les_1_12_05_c6p2[:,1]**2 + harm_avg_2048_les_1_12_05_c6p2[:,2]**2
harm_avg_2048_les_1_12_05_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_les_1_12_05_c7p2 = harm_avg_2048_les_1_12_05_c7p2[:,0]
v_harm_avg_2048_les_1_12_05_c7p2 = harm_avg_2048_les_1_12_05_c7p2[:,1]**2 + harm_avg_2048_les_1_12_05_c7p2[:,2]**2
harm_avg_2048_les_1_12_05_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_les_1_12_05_c8p2 = harm_avg_2048_les_1_12_05_c8p2[:,0]
v_harm_avg_2048_les_1_12_05_c8p2 = harm_avg_2048_les_1_12_05_c8p2[:,1]**2 + harm_avg_2048_les_1_12_05_c8p2[:,2]**2

harm_avg_2048_les_1_12_05_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_les_1_12_05_c2m2 = harm_avg_2048_les_1_12_05_c2m2[:,0]
v_harm_avg_2048_les_1_12_05_c2m2 = harm_avg_2048_les_1_12_05_c2m2[:,1]**2 + harm_avg_2048_les_1_12_05_c2m2[:,2]**2
harm_avg_2048_les_1_12_05_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_les_1_12_05_c3m2 = harm_avg_2048_les_1_12_05_c3m2[:,0]
v_harm_avg_2048_les_1_12_05_c3m2 = harm_avg_2048_les_1_12_05_c3m2[:,1]**2 + harm_avg_2048_les_1_12_05_c3m2[:,2]**2
harm_avg_2048_les_1_12_05_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_les_1_12_05_c4m2 = harm_avg_2048_les_1_12_05_c4m2[:,0]
v_harm_avg_2048_les_1_12_05_c4m2 = harm_avg_2048_les_1_12_05_c4m2[:,1]**2 + harm_avg_2048_les_1_12_05_c4m2[:,2]**2
harm_avg_2048_les_1_12_05_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_les_1_12_05_c5m2 = harm_avg_2048_les_1_12_05_c5m2[:,0]
v_harm_avg_2048_les_1_12_05_c5m2 = harm_avg_2048_les_1_12_05_c5m2[:,1]**2 + harm_avg_2048_les_1_12_05_c5m2[:,2]**2
harm_avg_2048_les_1_12_05_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_les_1_12_05_c6m2 = harm_avg_2048_les_1_12_05_c6m2[:,0]
v_harm_avg_2048_les_1_12_05_c6m2 = harm_avg_2048_les_1_12_05_c6m2[:,1]**2 + harm_avg_2048_les_1_12_05_c6m2[:,2]**2
harm_avg_2048_les_1_12_05_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_les_1_12_05_c7m2 = harm_avg_2048_les_1_12_05_c7m2[:,0]
v_harm_avg_2048_les_1_12_05_c7m2 = harm_avg_2048_les_1_12_05_c7m2[:,1]**2 + harm_avg_2048_les_1_12_05_c7m2[:,2]**2
harm_avg_2048_les_1_12_05_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_les_1_12_05_c8m2 = harm_avg_2048_les_1_12_05_c8m2[:,0]
v_harm_avg_2048_les_1_12_05_c8m2 = harm_avg_2048_les_1_12_05_c8m2[:,1]**2 + harm_avg_2048_les_1_12_05_c8m2[:,2]**2

harm_avg_2048_les_1_12_05_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_les_1_12_05_c3p3 = harm_avg_2048_les_1_12_05_c3p3[:,0]
v_harm_avg_2048_les_1_12_05_c3p3 = harm_avg_2048_les_1_12_05_c3p3[:,1]**2 + harm_avg_2048_les_1_12_05_c3p3[:,2]**2
harm_avg_2048_les_1_12_05_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_les_1_12_05_c4p3 = harm_avg_2048_les_1_12_05_c4p3[:,0]
v_harm_avg_2048_les_1_12_05_c4p3 = harm_avg_2048_les_1_12_05_c4p3[:,1]**2 + harm_avg_2048_les_1_12_05_c4p3[:,2]**2
harm_avg_2048_les_1_12_05_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_les_1_12_05_c5p3 = harm_avg_2048_les_1_12_05_c5p3[:,0]
v_harm_avg_2048_les_1_12_05_c5p3 = harm_avg_2048_les_1_12_05_c5p3[:,1]**2 + harm_avg_2048_les_1_12_05_c5p3[:,2]**2
harm_avg_2048_les_1_12_05_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_les_1_12_05_c6p3 = harm_avg_2048_les_1_12_05_c6p3[:,0]
v_harm_avg_2048_les_1_12_05_c6p3 = harm_avg_2048_les_1_12_05_c6p3[:,1]**2 + harm_avg_2048_les_1_12_05_c6p3[:,2]**2
harm_avg_2048_les_1_12_05_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_les_1_12_05_c7p3 = harm_avg_2048_les_1_12_05_c7p3[:,0]
v_harm_avg_2048_les_1_12_05_c7p3 = harm_avg_2048_les_1_12_05_c7p3[:,1]**2 + harm_avg_2048_les_1_12_05_c7p3[:,2]**2
harm_avg_2048_les_1_12_05_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_les_1_12_05_c8p3 = harm_avg_2048_les_1_12_05_c8p3[:,0]
v_harm_avg_2048_les_1_12_05_c8p3 = harm_avg_2048_les_1_12_05_c8p3[:,1]**2 + harm_avg_2048_les_1_12_05_c8p3[:,2]**2

harm_avg_2048_les_1_12_05_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_les_1_12_05_c3m3 = harm_avg_2048_les_1_12_05_c3m3[:,0]
v_harm_avg_2048_les_1_12_05_c3m3 = harm_avg_2048_les_1_12_05_c3m3[:,1]**2 + harm_avg_2048_les_1_12_05_c3m3[:,2]**2
harm_avg_2048_les_1_12_05_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_les_1_12_05_c4m3 = harm_avg_2048_les_1_12_05_c4m3[:,0]
v_harm_avg_2048_les_1_12_05_c4m3 = harm_avg_2048_les_1_12_05_c4m3[:,1]**2 + harm_avg_2048_les_1_12_05_c4m3[:,2]**2
harm_avg_2048_les_1_12_05_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_les_1_12_05_c5m3 = harm_avg_2048_les_1_12_05_c5m3[:,0]
v_harm_avg_2048_les_1_12_05_c5m3 = harm_avg_2048_les_1_12_05_c5m3[:,1]**2 + harm_avg_2048_les_1_12_05_c5m3[:,2]**2
harm_avg_2048_les_1_12_05_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_les_1_12_05_c6m3 = harm_avg_2048_les_1_12_05_c6m3[:,0]
v_harm_avg_2048_les_1_12_05_c6m3 = harm_avg_2048_les_1_12_05_c6m3[:,1]**2 + harm_avg_2048_les_1_12_05_c6m3[:,2]**2
harm_avg_2048_les_1_12_05_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_les_1_12_05_c7m3 = harm_avg_2048_les_1_12_05_c7m3[:,0]
v_harm_avg_2048_les_1_12_05_c7m3 = harm_avg_2048_les_1_12_05_c7m3[:,1]**2 + harm_avg_2048_les_1_12_05_c7m3[:,2]**2
harm_avg_2048_les_1_12_05_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_les_1_12_05_c8m3 = harm_avg_2048_les_1_12_05_c8m3[:,0]
v_harm_avg_2048_les_1_12_05_c8m3 = harm_avg_2048_les_1_12_05_c8m3[:,1]**2 + harm_avg_2048_les_1_12_05_c8m3[:,2]**2

harm_avg_2048_les_1_12_05_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_les_1_12_05_c4p4 = harm_avg_2048_les_1_12_05_c4p4[:,0]
v_harm_avg_2048_les_1_12_05_c4p4 = harm_avg_2048_les_1_12_05_c4p4[:,1]**2 + harm_avg_2048_les_1_12_05_c4p4[:,2]**2
harm_avg_2048_les_1_12_05_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_les_1_12_05_c5p4 = harm_avg_2048_les_1_12_05_c5p4[:,0]
v_harm_avg_2048_les_1_12_05_c5p4 = harm_avg_2048_les_1_12_05_c5p4[:,1]**2 + harm_avg_2048_les_1_12_05_c5p4[:,2]**2
harm_avg_2048_les_1_12_05_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_les_1_12_05_c6p4 = harm_avg_2048_les_1_12_05_c6p4[:,0]
v_harm_avg_2048_les_1_12_05_c6p4 = harm_avg_2048_les_1_12_05_c6p4[:,1]**2 + harm_avg_2048_les_1_12_05_c6p4[:,2]**2
harm_avg_2048_les_1_12_05_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_les_1_12_05_c7p4 = harm_avg_2048_les_1_12_05_c7p4[:,0]
v_harm_avg_2048_les_1_12_05_c7p4 = harm_avg_2048_les_1_12_05_c7p4[:,1]**2 + harm_avg_2048_les_1_12_05_c7p4[:,2]**2
harm_avg_2048_les_1_12_05_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_les_1_12_05_c8p4 = harm_avg_2048_les_1_12_05_c8p4[:,0]
v_harm_avg_2048_les_1_12_05_c8p4 = harm_avg_2048_les_1_12_05_c8p4[:,1]**2 + harm_avg_2048_les_1_12_05_c8p4[:,2]**2

harm_avg_2048_les_1_12_05_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_les_1_12_05_c4m4 = harm_avg_2048_les_1_12_05_c4m4[:,0]
v_harm_avg_2048_les_1_12_05_c4m4 = harm_avg_2048_les_1_12_05_c4m4[:,1]**2 + harm_avg_2048_les_1_12_05_c4m4[:,2]**2
harm_avg_2048_les_1_12_05_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_les_1_12_05_c5m4 = harm_avg_2048_les_1_12_05_c5m4[:,0]
v_harm_avg_2048_les_1_12_05_c5m4 = harm_avg_2048_les_1_12_05_c5m4[:,1]**2 + harm_avg_2048_les_1_12_05_c5m4[:,2]**2
harm_avg_2048_les_1_12_05_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_les_1_12_05_c6m4 = harm_avg_2048_les_1_12_05_c6m4[:,0]
v_harm_avg_2048_les_1_12_05_c6m4 = harm_avg_2048_les_1_12_05_c6m4[:,1]**2 + harm_avg_2048_les_1_12_05_c6m4[:,2]**2
harm_avg_2048_les_1_12_05_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_les_1_12_05_c7m4 = harm_avg_2048_les_1_12_05_c7m4[:,0]
v_harm_avg_2048_les_1_12_05_c7m4 = harm_avg_2048_les_1_12_05_c7m4[:,1]**2 + harm_avg_2048_les_1_12_05_c7m4[:,2]**2
harm_avg_2048_les_1_12_05_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_les_1_12_05_c8m4 = harm_avg_2048_les_1_12_05_c8m4[:,0]
v_harm_avg_2048_les_1_12_05_c8m4 = harm_avg_2048_les_1_12_05_c8m4[:,1]**2 + harm_avg_2048_les_1_12_05_c8m4[:,2]**2

harm_avg_2048_les_1_12_05_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_les_1_12_05_c5p5 = harm_avg_2048_les_1_12_05_c5p5[:,0]
v_harm_avg_2048_les_1_12_05_c5p5 = harm_avg_2048_les_1_12_05_c5p5[:,1]**2 + harm_avg_2048_les_1_12_05_c5p5[:,2]**2
harm_avg_2048_les_1_12_05_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_les_1_12_05_c6p5 = harm_avg_2048_les_1_12_05_c6p5[:,0]
v_harm_avg_2048_les_1_12_05_c6p5 = harm_avg_2048_les_1_12_05_c6p5[:,1]**2 + harm_avg_2048_les_1_12_05_c6p5[:,2]**2
harm_avg_2048_les_1_12_05_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_les_1_12_05_c7p5 = harm_avg_2048_les_1_12_05_c7p5[:,0]
v_harm_avg_2048_les_1_12_05_c7p5 = harm_avg_2048_les_1_12_05_c7p5[:,1]**2 + harm_avg_2048_les_1_12_05_c7p5[:,2]**2
harm_avg_2048_les_1_12_05_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_les_1_12_05_c8p5 = harm_avg_2048_les_1_12_05_c8p5[:,0]
v_harm_avg_2048_les_1_12_05_c8p5 = harm_avg_2048_les_1_12_05_c8p5[:,1]**2 + harm_avg_2048_les_1_12_05_c8p5[:,2]**2

harm_avg_2048_les_1_12_05_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_les_1_12_05_c5m5 = harm_avg_2048_les_1_12_05_c5m5[:,0]
v_harm_avg_2048_les_1_12_05_c5m5 = harm_avg_2048_les_1_12_05_c5m5[:,1]**2 + harm_avg_2048_les_1_12_05_c5m5[:,2]**2
harm_avg_2048_les_1_12_05_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_les_1_12_05_c6m5 = harm_avg_2048_les_1_12_05_c6m5[:,0]
v_harm_avg_2048_les_1_12_05_c6m5 = harm_avg_2048_les_1_12_05_c6m5[:,1]**2 + harm_avg_2048_les_1_12_05_c6m5[:,2]**2
harm_avg_2048_les_1_12_05_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_les_1_12_05_c7m5 = harm_avg_2048_les_1_12_05_c7m5[:,0]
v_harm_avg_2048_les_1_12_05_c7m5 = harm_avg_2048_les_1_12_05_c7m5[:,1]**2 + harm_avg_2048_les_1_12_05_c7m5[:,2]**2
harm_avg_2048_les_1_12_05_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_les_1_12_05_c8m5 = harm_avg_2048_les_1_12_05_c8m5[:,0]
v_harm_avg_2048_les_1_12_05_c8m5 = harm_avg_2048_les_1_12_05_c8m5[:,1]**2 + harm_avg_2048_les_1_12_05_c8m5[:,2]**2


harm_avg_2048_1_12_05_c00 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c00.dat")
t_harm_avg_2048_1_12_05_c00 = harm_avg_2048_1_12_05_c00[:,0]
v_harm_avg_2048_1_12_05_c00 = harm_avg_2048_1_12_05_c00[:,1]**2 + harm_avg_2048_1_12_05_c00[:,2]**2
harm_avg_2048_1_12_05_c10 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c10.dat")
t_harm_avg_2048_1_12_05_c10 = harm_avg_2048_1_12_05_c10[:,0]
v_harm_avg_2048_1_12_05_c10 = harm_avg_2048_1_12_05_c10[:,1]**2 + harm_avg_2048_1_12_05_c10[:,2]**2
harm_avg_2048_1_12_05_c20 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c20.dat")
t_harm_avg_2048_1_12_05_c20 = harm_avg_2048_1_12_05_c20[:,0]
v_harm_avg_2048_1_12_05_c20 = harm_avg_2048_1_12_05_c20[:,1]**2 + harm_avg_2048_1_12_05_c20[:,2]**2
harm_avg_2048_1_12_05_c30 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c30.dat")
t_harm_avg_2048_1_12_05_c30 = harm_avg_2048_1_12_05_c30[:,0]
v_harm_avg_2048_1_12_05_c30 = harm_avg_2048_1_12_05_c30[:,1]**2 + harm_avg_2048_1_12_05_c30[:,2]**2
harm_avg_2048_1_12_05_c40 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c40.dat")
t_harm_avg_2048_1_12_05_c40 = harm_avg_2048_1_12_05_c40[:,0]
v_harm_avg_2048_1_12_05_c40 = harm_avg_2048_1_12_05_c40[:,1]**2 + harm_avg_2048_1_12_05_c40[:,2]**2
harm_avg_2048_1_12_05_c50 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c50.dat")
t_harm_avg_2048_1_12_05_c50 = harm_avg_2048_1_12_05_c50[:,0]
v_harm_avg_2048_1_12_05_c50 = harm_avg_2048_1_12_05_c50[:,1]**2 + harm_avg_2048_1_12_05_c50[:,2]**2
harm_avg_2048_1_12_05_c60 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c60.dat")
t_harm_avg_2048_1_12_05_c60 = harm_avg_2048_1_12_05_c60[:,0]
v_harm_avg_2048_1_12_05_c60 = harm_avg_2048_1_12_05_c60[:,1]**2 + harm_avg_2048_1_12_05_c60[:,2]**2
harm_avg_2048_1_12_05_c70 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c70.dat")
t_harm_avg_2048_1_12_05_c70 = harm_avg_2048_1_12_05_c70[:,0]
v_harm_avg_2048_1_12_05_c70 = harm_avg_2048_1_12_05_c70[:,1]**2 + harm_avg_2048_1_12_05_c70[:,2]**2
harm_avg_2048_1_12_05_c80 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c80.dat")
t_harm_avg_2048_1_12_05_c80 = harm_avg_2048_1_12_05_c80[:,0]
v_harm_avg_2048_1_12_05_c80 = harm_avg_2048_1_12_05_c80[:,1]**2 + harm_avg_2048_1_12_05_c80[:,2]**2

harm_avg_2048_1_12_05_c1p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_1_12_05_c1p1 = harm_avg_2048_1_12_05_c1p1[:,0]
v_harm_avg_2048_1_12_05_c1p1 = harm_avg_2048_1_12_05_c1p1[:,1]**2 + harm_avg_2048_1_12_05_c1p1[:,2]**2
harm_avg_2048_1_12_05_c2p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_1_12_05_c2p1 = harm_avg_2048_1_12_05_c2p1[:,0]
v_harm_avg_2048_1_12_05_c2p1 = harm_avg_2048_1_12_05_c2p1[:,1]**2 + harm_avg_2048_1_12_05_c2p1[:,2]**2
harm_avg_2048_1_12_05_c3p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_1_12_05_c3p1 = harm_avg_2048_1_12_05_c3p1[:,0]
v_harm_avg_2048_1_12_05_c3p1 = harm_avg_2048_1_12_05_c3p1[:,1]**2 + harm_avg_2048_1_12_05_c3p1[:,2]**2
harm_avg_2048_1_12_05_c4p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_1_12_05_c4p1 = harm_avg_2048_1_12_05_c4p1[:,0]
v_harm_avg_2048_1_12_05_c4p1 = harm_avg_2048_1_12_05_c4p1[:,1]**2 + harm_avg_2048_1_12_05_c4p1[:,2]**2
harm_avg_2048_1_12_05_c5p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_1_12_05_c5p1 = harm_avg_2048_1_12_05_c5p1[:,0]
v_harm_avg_2048_1_12_05_c5p1 = harm_avg_2048_1_12_05_c5p1[:,1]**2 + harm_avg_2048_1_12_05_c5p1[:,2]**2
harm_avg_2048_1_12_05_c6p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_1_12_05_c6p1 = harm_avg_2048_1_12_05_c6p1[:,0]
v_harm_avg_2048_1_12_05_c6p1 = harm_avg_2048_1_12_05_c6p1[:,1]**2 + harm_avg_2048_1_12_05_c6p1[:,2]**2
harm_avg_2048_1_12_05_c7p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_1_12_05_c7p1 = harm_avg_2048_1_12_05_c7p1[:,0]
v_harm_avg_2048_1_12_05_c7p1 = harm_avg_2048_1_12_05_c7p1[:,1]**2 + harm_avg_2048_1_12_05_c7p1[:,2]**2
harm_avg_2048_1_12_05_c8p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_1_12_05_c8p1 = harm_avg_2048_1_12_05_c8p1[:,0]
v_harm_avg_2048_1_12_05_c8p1 = harm_avg_2048_1_12_05_c8p1[:,1]**2 + harm_avg_2048_1_12_05_c8p1[:,2]**2

harm_avg_2048_1_12_05_c1m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_1_12_05_c1m1 = harm_avg_2048_1_12_05_c1m1[:,0]
v_harm_avg_2048_1_12_05_c1m1 = harm_avg_2048_1_12_05_c1m1[:,1]**2 + harm_avg_2048_1_12_05_c1m1[:,2]**2
harm_avg_2048_1_12_05_c2m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_1_12_05_c2m1 = harm_avg_2048_1_12_05_c2m1[:,0]
v_harm_avg_2048_1_12_05_c2m1 = harm_avg_2048_1_12_05_c2m1[:,1]**2 + harm_avg_2048_1_12_05_c2m1[:,2]**2
harm_avg_2048_1_12_05_c3m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_1_12_05_c3m1 = harm_avg_2048_1_12_05_c3m1[:,0]
v_harm_avg_2048_1_12_05_c3m1 = harm_avg_2048_1_12_05_c3m1[:,1]**2 + harm_avg_2048_1_12_05_c3m1[:,2]**2
harm_avg_2048_1_12_05_c4m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_1_12_05_c4m1 = harm_avg_2048_1_12_05_c4m1[:,0]
v_harm_avg_2048_1_12_05_c4m1 = harm_avg_2048_1_12_05_c4m1[:,1]**2 + harm_avg_2048_1_12_05_c4m1[:,2]**2
harm_avg_2048_1_12_05_c5m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_1_12_05_c5m1 = harm_avg_2048_1_12_05_c5m1[:,0]
v_harm_avg_2048_1_12_05_c5m1 = harm_avg_2048_1_12_05_c5m1[:,1]**2 + harm_avg_2048_1_12_05_c5m1[:,2]**2
harm_avg_2048_1_12_05_c6m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_1_12_05_c6m1 = harm_avg_2048_1_12_05_c6m1[:,0]
v_harm_avg_2048_1_12_05_c6m1 = harm_avg_2048_1_12_05_c6m1[:,1]**2 + harm_avg_2048_1_12_05_c6m1[:,2]**2
harm_avg_2048_1_12_05_c7m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_1_12_05_c7m1 = harm_avg_2048_1_12_05_c7m1[:,0]
v_harm_avg_2048_1_12_05_c7m1 = harm_avg_2048_1_12_05_c7m1[:,1]**2 + harm_avg_2048_1_12_05_c7m1[:,2]**2
harm_avg_2048_1_12_05_c8m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_1_12_05_c8m1 = harm_avg_2048_1_12_05_c8m1[:,0]
v_harm_avg_2048_1_12_05_c8m1 = harm_avg_2048_1_12_05_c8m1[:,1]**2 + harm_avg_2048_1_12_05_c8m1[:,2]**2

harm_avg_2048_1_12_05_c2p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_1_12_05_c2p2 = harm_avg_2048_1_12_05_c2p2[:,0]
v_harm_avg_2048_1_12_05_c2p2 = harm_avg_2048_1_12_05_c2p2[:,1]**2 + harm_avg_2048_1_12_05_c2p2[:,2]**2
harm_avg_2048_1_12_05_c3p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_1_12_05_c3p2 = harm_avg_2048_1_12_05_c3p2[:,0]
v_harm_avg_2048_1_12_05_c3p2 = harm_avg_2048_1_12_05_c3p2[:,1]**2 + harm_avg_2048_1_12_05_c3p2[:,2]**2
harm_avg_2048_1_12_05_c4p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_1_12_05_c4p2 = harm_avg_2048_1_12_05_c4p2[:,0]
v_harm_avg_2048_1_12_05_c4p2 = harm_avg_2048_1_12_05_c4p2[:,1]**2 + harm_avg_2048_1_12_05_c4p2[:,2]**2
harm_avg_2048_1_12_05_c5p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_1_12_05_c5p2 = harm_avg_2048_1_12_05_c5p2[:,0]
v_harm_avg_2048_1_12_05_c5p2 = harm_avg_2048_1_12_05_c5p2[:,1]**2 + harm_avg_2048_1_12_05_c5p2[:,2]**2
harm_avg_2048_1_12_05_c6p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_1_12_05_c6p2 = harm_avg_2048_1_12_05_c6p2[:,0]
v_harm_avg_2048_1_12_05_c6p2 = harm_avg_2048_1_12_05_c6p2[:,1]**2 + harm_avg_2048_1_12_05_c6p2[:,2]**2
harm_avg_2048_1_12_05_c7p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_1_12_05_c7p2 = harm_avg_2048_1_12_05_c7p2[:,0]
v_harm_avg_2048_1_12_05_c7p2 = harm_avg_2048_1_12_05_c7p2[:,1]**2 + harm_avg_2048_1_12_05_c7p2[:,2]**2
harm_avg_2048_1_12_05_c8p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_1_12_05_c8p2 = harm_avg_2048_1_12_05_c8p2[:,0]
v_harm_avg_2048_1_12_05_c8p2 = harm_avg_2048_1_12_05_c8p2[:,1]**2 + harm_avg_2048_1_12_05_c8p2[:,2]**2

harm_avg_2048_1_12_05_c2m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_1_12_05_c2m2 = harm_avg_2048_1_12_05_c2m2[:,0]
v_harm_avg_2048_1_12_05_c2m2 = harm_avg_2048_1_12_05_c2m2[:,1]**2 + harm_avg_2048_1_12_05_c2m2[:,2]**2
harm_avg_2048_1_12_05_c3m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_1_12_05_c3m2 = harm_avg_2048_1_12_05_c3m2[:,0]
v_harm_avg_2048_1_12_05_c3m2 = harm_avg_2048_1_12_05_c3m2[:,1]**2 + harm_avg_2048_1_12_05_c3m2[:,2]**2
harm_avg_2048_1_12_05_c4m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_1_12_05_c4m2 = harm_avg_2048_1_12_05_c4m2[:,0]
v_harm_avg_2048_1_12_05_c4m2 = harm_avg_2048_1_12_05_c4m2[:,1]**2 + harm_avg_2048_1_12_05_c4m2[:,2]**2
harm_avg_2048_1_12_05_c5m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_1_12_05_c5m2 = harm_avg_2048_1_12_05_c5m2[:,0]
v_harm_avg_2048_1_12_05_c5m2 = harm_avg_2048_1_12_05_c5m2[:,1]**2 + harm_avg_2048_1_12_05_c5m2[:,2]**2
harm_avg_2048_1_12_05_c6m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_1_12_05_c6m2 = harm_avg_2048_1_12_05_c6m2[:,0]
v_harm_avg_2048_1_12_05_c6m2 = harm_avg_2048_1_12_05_c6m2[:,1]**2 + harm_avg_2048_1_12_05_c6m2[:,2]**2
harm_avg_2048_1_12_05_c7m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_1_12_05_c7m2 = harm_avg_2048_1_12_05_c7m2[:,0]
v_harm_avg_2048_1_12_05_c7m2 = harm_avg_2048_1_12_05_c7m2[:,1]**2 + harm_avg_2048_1_12_05_c7m2[:,2]**2
harm_avg_2048_1_12_05_c8m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_1_12_05_c8m2 = harm_avg_2048_1_12_05_c8m2[:,0]
v_harm_avg_2048_1_12_05_c8m2 = harm_avg_2048_1_12_05_c8m2[:,1]**2 + harm_avg_2048_1_12_05_c8m2[:,2]**2

harm_avg_2048_1_12_05_c3p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_1_12_05_c3p3 = harm_avg_2048_1_12_05_c3p3[:,0]
v_harm_avg_2048_1_12_05_c3p3 = harm_avg_2048_1_12_05_c3p3[:,1]**2 + harm_avg_2048_1_12_05_c3p3[:,2]**2
harm_avg_2048_1_12_05_c4p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_1_12_05_c4p3 = harm_avg_2048_1_12_05_c4p3[:,0]
v_harm_avg_2048_1_12_05_c4p3 = harm_avg_2048_1_12_05_c4p3[:,1]**2 + harm_avg_2048_1_12_05_c4p3[:,2]**2
harm_avg_2048_1_12_05_c5p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_1_12_05_c5p3 = harm_avg_2048_1_12_05_c5p3[:,0]
v_harm_avg_2048_1_12_05_c5p3 = harm_avg_2048_1_12_05_c5p3[:,1]**2 + harm_avg_2048_1_12_05_c5p3[:,2]**2
harm_avg_2048_1_12_05_c6p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_1_12_05_c6p3 = harm_avg_2048_1_12_05_c6p3[:,0]
v_harm_avg_2048_1_12_05_c6p3 = harm_avg_2048_1_12_05_c6p3[:,1]**2 + harm_avg_2048_1_12_05_c6p3[:,2]**2
harm_avg_2048_1_12_05_c7p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_1_12_05_c7p3 = harm_avg_2048_1_12_05_c7p3[:,0]
v_harm_avg_2048_1_12_05_c7p3 = harm_avg_2048_1_12_05_c7p3[:,1]**2 + harm_avg_2048_1_12_05_c7p3[:,2]**2
harm_avg_2048_1_12_05_c8p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_1_12_05_c8p3 = harm_avg_2048_1_12_05_c8p3[:,0]
v_harm_avg_2048_1_12_05_c8p3 = harm_avg_2048_1_12_05_c8p3[:,1]**2 + harm_avg_2048_1_12_05_c8p3[:,2]**2

harm_avg_2048_1_12_05_c3m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_1_12_05_c3m3 = harm_avg_2048_1_12_05_c3m3[:,0]
v_harm_avg_2048_1_12_05_c3m3 = harm_avg_2048_1_12_05_c3m3[:,1]**2 + harm_avg_2048_1_12_05_c3m3[:,2]**2
harm_avg_2048_1_12_05_c4m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_1_12_05_c4m3 = harm_avg_2048_1_12_05_c4m3[:,0]
v_harm_avg_2048_1_12_05_c4m3 = harm_avg_2048_1_12_05_c4m3[:,1]**2 + harm_avg_2048_1_12_05_c4m3[:,2]**2
harm_avg_2048_1_12_05_c5m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_1_12_05_c5m3 = harm_avg_2048_1_12_05_c5m3[:,0]
v_harm_avg_2048_1_12_05_c5m3 = harm_avg_2048_1_12_05_c5m3[:,1]**2 + harm_avg_2048_1_12_05_c5m3[:,2]**2
harm_avg_2048_1_12_05_c6m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_1_12_05_c6m3 = harm_avg_2048_1_12_05_c6m3[:,0]
v_harm_avg_2048_1_12_05_c6m3 = harm_avg_2048_1_12_05_c6m3[:,1]**2 + harm_avg_2048_1_12_05_c6m3[:,2]**2
harm_avg_2048_1_12_05_c7m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_1_12_05_c7m3 = harm_avg_2048_1_12_05_c7m3[:,0]
v_harm_avg_2048_1_12_05_c7m3 = harm_avg_2048_1_12_05_c7m3[:,1]**2 + harm_avg_2048_1_12_05_c7m3[:,2]**2
harm_avg_2048_1_12_05_c8m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_1_12_05_c8m3 = harm_avg_2048_1_12_05_c8m3[:,0]
v_harm_avg_2048_1_12_05_c8m3 = harm_avg_2048_1_12_05_c8m3[:,1]**2 + harm_avg_2048_1_12_05_c8m3[:,2]**2

harm_avg_2048_1_12_05_c4p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_1_12_05_c4p4 = harm_avg_2048_1_12_05_c4p4[:,0]
v_harm_avg_2048_1_12_05_c4p4 = harm_avg_2048_1_12_05_c4p4[:,1]**2 + harm_avg_2048_1_12_05_c4p4[:,2]**2
harm_avg_2048_1_12_05_c5p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_1_12_05_c5p4 = harm_avg_2048_1_12_05_c5p4[:,0]
v_harm_avg_2048_1_12_05_c5p4 = harm_avg_2048_1_12_05_c5p4[:,1]**2 + harm_avg_2048_1_12_05_c5p4[:,2]**2
harm_avg_2048_1_12_05_c6p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_1_12_05_c6p4 = harm_avg_2048_1_12_05_c6p4[:,0]
v_harm_avg_2048_1_12_05_c6p4 = harm_avg_2048_1_12_05_c6p4[:,1]**2 + harm_avg_2048_1_12_05_c6p4[:,2]**2
harm_avg_2048_1_12_05_c7p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_1_12_05_c7p4 = harm_avg_2048_1_12_05_c7p4[:,0]
v_harm_avg_2048_1_12_05_c7p4 = harm_avg_2048_1_12_05_c7p4[:,1]**2 + harm_avg_2048_1_12_05_c7p4[:,2]**2
harm_avg_2048_1_12_05_c8p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_1_12_05_c8p4 = harm_avg_2048_1_12_05_c8p4[:,0]
v_harm_avg_2048_1_12_05_c8p4 = harm_avg_2048_1_12_05_c8p4[:,1]**2 + harm_avg_2048_1_12_05_c8p4[:,2]**2

harm_avg_2048_1_12_05_c4m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_1_12_05_c4m4 = harm_avg_2048_1_12_05_c4m4[:,0]
v_harm_avg_2048_1_12_05_c4m4 = harm_avg_2048_1_12_05_c4m4[:,1]**2 + harm_avg_2048_1_12_05_c4m4[:,2]**2
harm_avg_2048_1_12_05_c5m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_1_12_05_c5m4 = harm_avg_2048_1_12_05_c5m4[:,0]
v_harm_avg_2048_1_12_05_c5m4 = harm_avg_2048_1_12_05_c5m4[:,1]**2 + harm_avg_2048_1_12_05_c5m4[:,2]**2
harm_avg_2048_1_12_05_c6m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_1_12_05_c6m4 = harm_avg_2048_1_12_05_c6m4[:,0]
v_harm_avg_2048_1_12_05_c6m4 = harm_avg_2048_1_12_05_c6m4[:,1]**2 + harm_avg_2048_1_12_05_c6m4[:,2]**2
harm_avg_2048_1_12_05_c7m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_1_12_05_c7m4 = harm_avg_2048_1_12_05_c7m4[:,0]
v_harm_avg_2048_1_12_05_c7m4 = harm_avg_2048_1_12_05_c7m4[:,1]**2 + harm_avg_2048_1_12_05_c7m4[:,2]**2
harm_avg_2048_1_12_05_c8m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_1_12_05_c8m4 = harm_avg_2048_1_12_05_c8m4[:,0]
v_harm_avg_2048_1_12_05_c8m4 = harm_avg_2048_1_12_05_c8m4[:,1]**2 + harm_avg_2048_1_12_05_c8m4[:,2]**2

harm_avg_2048_1_12_05_c5p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_1_12_05_c5p5 = harm_avg_2048_1_12_05_c5p5[:,0]
v_harm_avg_2048_1_12_05_c5p5 = harm_avg_2048_1_12_05_c5p5[:,1]**2 + harm_avg_2048_1_12_05_c5p5[:,2]**2
harm_avg_2048_1_12_05_c6p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_1_12_05_c6p5 = harm_avg_2048_1_12_05_c6p5[:,0]
v_harm_avg_2048_1_12_05_c6p5 = harm_avg_2048_1_12_05_c6p5[:,1]**2 + harm_avg_2048_1_12_05_c6p5[:,2]**2
harm_avg_2048_1_12_05_c7p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_1_12_05_c7p5 = harm_avg_2048_1_12_05_c7p5[:,0]
v_harm_avg_2048_1_12_05_c7p5 = harm_avg_2048_1_12_05_c7p5[:,1]**2 + harm_avg_2048_1_12_05_c7p5[:,2]**2
harm_avg_2048_1_12_05_c8p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_1_12_05_c8p5 = harm_avg_2048_1_12_05_c8p5[:,0]
v_harm_avg_2048_1_12_05_c8p5 = harm_avg_2048_1_12_05_c8p5[:,1]**2 + harm_avg_2048_1_12_05_c8p5[:,2]**2

harm_avg_2048_1_12_05_c5m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_1_12_05_c5m5 = harm_avg_2048_1_12_05_c5m5[:,0]
v_harm_avg_2048_1_12_05_c5m5 = harm_avg_2048_1_12_05_c5m5[:,1]**2 + harm_avg_2048_1_12_05_c5m5[:,2]**2
harm_avg_2048_1_12_05_c6m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_1_12_05_c6m5 = harm_avg_2048_1_12_05_c6m5[:,0]
v_harm_avg_2048_1_12_05_c6m5 = harm_avg_2048_1_12_05_c6m5[:,1]**2 + harm_avg_2048_1_12_05_c6m5[:,2]**2
harm_avg_2048_1_12_05_c7m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_1_12_05_c7m5 = harm_avg_2048_1_12_05_c7m5[:,0]
v_harm_avg_2048_1_12_05_c7m5 = harm_avg_2048_1_12_05_c7m5[:,1]**2 + harm_avg_2048_1_12_05_c7m5[:,2]**2
harm_avg_2048_1_12_05_c8m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_1_12_05_c8m5 = harm_avg_2048_1_12_05_c8m5[:,0]
v_harm_avg_2048_1_12_05_c8m5 = harm_avg_2048_1_12_05_c8m5[:,1]**2 + harm_avg_2048_1_12_05_c8m5[:,2]**2

############ ONDA ################

wave_c2p2_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_r200 = wave_c2p2_2048_r200[:,0]
v2_wave_c2p2_2048_r200 = wave_c2p2_2048_r200[:,1]**2 + wave_c2p2_2048_r200[:,2]**2
'''
wave_c2p2_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_b0_r200 = wave_c2p2_2048_b0_r200[:,0]
v2_wave_c2p2_2048_b0_r200 = wave_c2p2_2048_b0_r200[:,1]**2 + wave_c2p2_2048_b0_r200[:,2]**2

wave_c2p2_2048_les_r100 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R100/c2p2.dat")
t_wave_c2p2_2048_les_r100 = wave_c2p2_2048_les_r100[:,0]
v_wave_c2p2_2048_les_r100 = wave_c2p2_2048_les_r100[:,1]
v2_wave_c2p2_2048_les_r100 = wave_c2p2_2048_les_r100[:,1]**2 + wave_c2p2_2048_les_r100[:,2]**2

wave_c2p2_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_les_r200 = wave_c2p2_2048_les_r200[:,0]
v_wave_c2p2_2048_les_r200 = wave_c2p2_2048_les_r200[:,1]
v2_wave_c2p2_2048_les_r200 = wave_c2p2_2048_les_r200[:,1]**2 + wave_c2p2_2048_les_r200[:,2]**2

wave_c2p2_2048_les_r300 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R300/c2p2.dat")
t_wave_c2p2_2048_les_r300 = wave_c2p2_2048_les_r300[:,0]
v_wave_c2p2_2048_les_r300 = wave_c2p2_2048_les_r300[:,1]
v2_wave_c2p2_2048_les_r300 = wave_c2p2_2048_les_r300[:,1]**2 + wave_c2p2_2048_les_r300[:,2]**2


wave_c2p2_2048_les_full_r100 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R100/c2p2.dat")
t_wave_c2p2_2048_les_full_r100 = wave_c2p2_2048_les_full_r100[:,0]
v_wave_c2p2_2048_les_full_r100 = wave_c2p2_2048_les_full_r100[:,1]
v2_wave_c2p2_2048_les_full_r100 = wave_c2p2_2048_les_full_r100[:,1]**2 + wave_c2p2_2048_les_full_r100[:,2]**2

wave_c2p2_2048_les_full_r200 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_les_full_r200 = wave_c2p2_2048_les_full_r200[:,0]
v_wave_c2p2_2048_les_full_r200 = wave_c2p2_2048_les_full_r200[:,1]
v2_wave_c2p2_2048_les_full_r200 = wave_c2p2_2048_les_full_r200[:,1]**2 + wave_c2p2_2048_les_full_r200[:,2]**2

wave_c2p2_2048_les_full_r300 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R300/c2p2.dat")
t_wave_c2p2_2048_les_full_r300 = wave_c2p2_2048_les_full_r300[:,0]
v_wave_c2p2_2048_les_full_r300 = wave_c2p2_2048_les_full_r300[:,1]
v2_wave_c2p2_2048_les_full_r300 = wave_c2p2_2048_les_full_r300[:,1]**2 + wave_c2p2_2048_les_full_r300[:,2]**2


'''

wave_c2p1_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_r200 = wave_c2p1_2048_r200[:,0]
v2_wave_c2p1_2048_r200 = wave_c2p1_2048_r200[:,1]**2 + wave_c2p1_2048_r200[:,2]**2
'''
wave_c2p1_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_b0_r200 = wave_c2p1_2048_b0_r200[:,0]
v2_wave_c2p1_2048_b0_r200 = wave_c2p1_2048_b0_r200[:,1]**2 + wave_c2p1_2048_b0_r200[:,2]**2

wave_c2p1_2048_les_r100 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R100/c2p1.dat")
t_wave_c2p1_2048_les_r100 = wave_c2p1_2048_les_r100[:,0]
v2_wave_c2p1_2048_les_r100 = wave_c2p1_2048_les_r100[:,1]**2 + wave_c2p1_2048_les_r100[:,2]**2

wave_c2p1_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_les_r200 = wave_c2p1_2048_les_r200[:,0]
v2_wave_c2p1_2048_les_r200 = wave_c2p1_2048_les_r200[:,1]**2 + wave_c2p1_2048_les_r200[:,2]**2

wave_c2p1_2048_les_r300 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R300/c2p1.dat")
t_wave_c2p1_2048_les_r300 = wave_c2p1_2048_les_r300[:,0]
v2_wave_c2p1_2048_les_r300 = wave_c2p1_2048_les_r300[:,1]**2 + wave_c2p1_2048_les_r300[:,2]**2


wave_c2p1_2048_les_full_r200 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_les_full_r200 = wave_c2p1_2048_les_full_r200[:,0]
v2_wave_c2p1_2048_les_full_r200 = wave_c2p1_2048_les_full_r200[:,1]**2 + wave_c2p1_2048_les_full_r200[:,2]**2

'''


wave_c20_2048_les_r100 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R100/c20.dat")
t_wave_c20_2048_les_r100 = wave_c20_2048_les_r100[:,0]
v2_wave_c20_2048_les_r100 = wave_c20_2048_les_r100[:,1]**2 + wave_c20_2048_les_r100[:,2]**2

wave_c20_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/c20.dat")
t_wave_c20_2048_les_r200 = wave_c20_2048_les_r200[:,0]
v2_wave_c20_2048_les_r200 = wave_c20_2048_les_r200[:,1]**2 + wave_c20_2048_les_r200[:,2]**2

wave_c20_2048_les_r300 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R300/c20.dat")
t_wave_c20_2048_les_r300 = wave_c20_2048_les_r300[:,0]
v2_wave_c20_2048_les_r300 = wave_c20_2048_les_r300[:,1]**2 + wave_c20_2048_les_r300[:,2]**2

fmass_2048_les_r100 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R100/flux_mass.dat")
t_fmass_2048_les_r100 = fmass_2048_les_r100[:,0]
v_fmass_2048_les_r100 = fmass_2048_les_r100[:,1]

fmass_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/flux_mass.dat")
t_fmass_2048_les_r200 = fmass_2048_les_r200[:,0]
v_fmass_2048_les_r200 = fmass_2048_les_r200[:,1]

fmass_2048_les_r300 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R300/flux_mass.dat")
t_fmass_2048_les_r300 = fmass_2048_les_r300[:,0]
v_fmass_2048_les_r300 = fmass_2048_les_r300[:,1]

'''


fmass_2048_les_full_r100 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R100/flux_mass.dat")
t_fmass_2048_les_full_r100 = fmass_2048_les_full_r100[:,0]
v_fmass_2048_les_full_r100 = fmass_2048_les_full_r100[:,1]

fmass_2048_les_full_r200 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R200/flux_mass.dat")
t_fmass_2048_les_full_r200 = fmass_2048_les_full_r200[:,0]
v_fmass_2048_les_full_r200 = fmass_2048_les_full_r200[:,1]

fmass_2048_les_full_r300 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R300/flux_mass.dat")
t_fmass_2048_les_full_r300 = fmass_2048_les_full_r300[:,0]
v_fmass_2048_les_full_r300 = fmass_2048_les_full_r300[:,1]


'''

fmassAcc_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/flux_mass_acc.dat")
t_fmassAcc_2048_les_r200 = fmassAcc_2048_les_r200[:,0]
v_fmassAcc_2048_les_r200 = fmassAcc_2048_les_r200[:,1]

fmassAcc_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/flux_mass_acc.dat")
t_fmassAcc_2048_r200 = fmassAcc_2048_r200[:,0]
v_fmassAcc_2048_r200 = fmassAcc_2048_r200[:,1]

fmassAcc_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/flux_mass_acc.dat")
t_fmassAcc_2048_b0_r200 = fmassAcc_2048_b0_r200[:,0]
v_fmassAcc_2048_b0_r200 = fmassAcc_2048_b0_r200[:,1]

fmassAcc_4096_les_r200 = np.genfromtxt(fname="4096_LES/data_outputDir_sphere_R200/flux_mass_acc.dat")
t_fmassAcc_4096_les_r200 = fmassAcc_4096_les_r200[:,0]
v_fmassAcc_4096_les_r200 = fmassAcc_4096_les_r200[:,1]

fmassAcc_4096_r200 = np.genfromtxt(fname="4096/data_outputDir_sphere_R200/flux_mass_acc.dat")
t_fmassAcc_4096_r200 = fmassAcc_4096_r200[:,0]
v_fmassAcc_4096_r200 = fmassAcc_4096_r200[:,1]
wave_c2p2_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_r200 = wave_c2p2_2048_r200[:,0]
v2_wave_c2p2_2048_r200 = wave_c2p2_2048_r200[:,1]**2 + wave_c2p2_2048_r200[:,2]**2

wave_c2p1_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_r200 = wave_c2p1_2048_r200[:,0]
v2_wave_c2p1_2048_r200 = wave_c2p1_2048_r200[:,1]**2 + wave_c2p1_2048_r200[:,2]**2

wave_c20_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/c20.dat")
t_wave_c20_2048_r200 = wave_c20_2048_r200[:,0]
v2_wave_c20_2048_r200 = wave_c20_2048_r200[:,1]**2 + wave_c20_2048_r200[:,2]**2


wave_c2p2_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/c2p2.dat")
t_wave_c2p2_2048_b0_r200 = wave_c2p2_2048_b0_r200[:,0]
v2_wave_c2p2_2048_b0_r200 = wave_c2p2_2048_b0_r200[:,1]**2 + wave_c2p2_2048_b0_r200[:,2]**2

wave_c2p1_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/c2p1.dat")
t_wave_c2p1_2048_b0_r200 = wave_c2p1_2048_b0_r200[:,0]
v2_wave_c2p1_2048_b0_r200 = wave_c2p1_2048_b0_r200[:,1]**2 + wave_c2p1_2048_b0_r200[:,2]**2

wave_c20_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/c20.dat")
t_wave_c20_2048_b0_r200 = wave_c20_2048_b0_r200[:,0]
v2_wave_c20_2048_b0_r200 = wave_c20_2048_b0_r200[:,1]**2 + wave_c20_2048_b0_r200[:,2]**2


wave_omega_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_2048_r200 = wave_omega_2048_r200[:,0]
v_wave_omega_2048_r200 = wave_omega_2048_r200[:,1]
v2_wave_omega_2048_r200 = wave_omega_2048_r200[:,1]**2 + wave_omega_2048_r200[:,2]**2

'''

wave_omega_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_2048_b0_r200 = wave_omega_2048_b0_r200[:,0]
v_wave_omega_2048_b0_r200 = wave_omega_2048_b0_r200[:,1]
v2_wave_omega_2048_b0_r200 = wave_omega_2048_b0_r200[:,1]**2 + wave_omega_2048_b0_r200[:,2]**2

wave_omega_2048_les_full_r200 = np.genfromtxt(fname="2048_LES_full/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_2048_les_full_r200 = wave_omega_2048_les_full_r200[:,0]
v_wave_omega_2048_les_full_r200 = wave_omega_2048_les_full_r200[:,1]
v2_wave_omega_2048_les_full_r200 = wave_omega_2048_les_full_r200[:,1]**2 + wave_omega_2048_les_full_r200[:,2]**2


wave_omega_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_2048_les_r200 = wave_omega_2048_les_r200[:,0]
v_wave_omega_2048_les_r200 = wave_omega_2048_les_r200[:,1]
v2_wave_omega_2048_les_r200 = wave_omega_2048_les_r200[:,1]**2 + wave_omega_2048_les_r200[:,2]**2

'''

wave_omega_4096_r200 = np.genfromtxt(fname="4096/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_4096_r200 = wave_omega_4096_r200[:,0]
v_wave_omega_4096_r200 = wave_omega_4096_r200[:,1]
v2_wave_omega_4096_r200 = wave_omega_4096_r200[:,1]**2 + wave_omega_4096_r200[:,2]**2

wave_omega_4096_les_r200 = np.genfromtxt(fname="4096_LES/data_outputDir_sphere_R200/omega.dat")
t_wave_omega_4096_les_r200 = wave_omega_4096_les_r200[:,0]
v_wave_omega_4096_les_r200 = wave_omega_4096_les_r200[:,1]
v2_wave_omega_4096_les_r200 = wave_omega_4096_les_r200[:,1]**2 + wave_omega_4096_les_r200[:,2]**2


wave_fft_2048_les_r200 = np.genfromtxt(fname="2048_LES/data_outputDir_sphere_R200/FFT_200.dat")
f_wave_fft_2048_les_r200 = wave_fft_2048_les_r200[:,0]
v_wave_fft_2048_les_r200 = wave_fft_2048_les_r200[:,1]

wave_fft_2048_b0_r200 = np.genfromtxt(fname="2048_B0/data_outputDir_sphere_R200/FFT_200.dat")
f_wave_fft_2048_b0_r200 = wave_fft_2048_b0_r200[:,0]
v_wave_fft_2048_b0_r200 = wave_fft_2048_b0_r200[:,1]

wave_fft_2048_r200 = np.genfromtxt(fname="2048/data_outputDir_sphere_R200/FFT_200.dat")
f_wave_fft_2048_r200 = wave_fft_2048_r200[:,0]
v_wave_fft_2048_r200 = wave_fft_2048_r200[:,1]

wave_fft_4096_les_r200 = np.genfromtxt(fname="4096_LES/data_outputDir_sphere_R200/FFT_200.dat")
f_wave_fft_4096_les_r200 = wave_fft_4096_les_r200[:,0]
v_wave_fft_4096_les_r200 = wave_fft_4096_les_r200[:,1]

wave_fft_4096_r200 = np.genfromtxt(fname="4096/data_outputDir_sphere_R200/FFT_200.dat")
f_wave_fft_4096_r200 = wave_fft_4096_r200[:,0]
v_wave_fft_4096_r200 = wave_fft_4096_r200[:,1]

############ PLOTS ###############



index = -3./2. 
rref = r_cyl_flat_2048_les_m5e10_avg_5250[40:75] # Inici i final en x
s1 = 230000
r_k = s1*(rref)**(index)


# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_5250, rho_cyl_flat_2048_les_m5e10_avg_5250*rho_factor, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_13000, rho_cyl_flat_2048_les_m5e10_avg_13000*rho_factor, label=r'$50 ms$', color='b')
axarr[0].plot(r_cyl_flat_2048_m5e10_avg_5250, rho_cyl_flat_2048_m5e10_avg_5250*rho_factor, color='r', linestyle='--')
axarr[0].plot(r_cyl_flat_2048_m5e10_avg_13000, rho_cyl_flat_2048_m5e10_avg_13000*rho_factor, color='b', linestyle='--')
axarr[0].plot(r_cyl_flat_2048_b0_m5e10_avg_5250, rho_cyl_flat_2048_b0_m5e10_avg_5250*rho_factor, color='r', linestyle=':')
axarr[0].plot(r_cyl_flat_2048_b0_m5e10_avg_13000, rho_cyl_flat_2048_b0_m5e10_avg_13000*rho_factor, color='b', linestyle=':')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':16},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([1, 31/1.5])
axarr[0].set_ylim([7e9, 1.1e15])
axarr[0].set_yscale('log')
axarr[0].set_ylabel(r'$<\rho>[g/cm^3]$',fontsize=20)
axarr[0].tick_params(labelsize=16)
axarr[0].set_yticks([1e10, 1e11, 1e12, 1e13, 1e14, 1e15])
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_5250, omega_cyl_flat_2048_les_m5e10_avg_5250*omega_factor, label=r'MR LES', color='r')
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_13000, omega_cyl_flat_2048_les_m5e10_avg_13000*omega_factor, color='b')
axarr[1].plot(r_cyl_flat_2048_m5e10_avg_5250, omega_cyl_flat_2048_m5e10_avg_5250*omega_factor, label=r'MR', color='r', linestyle='--')
axarr[1].plot(r_cyl_flat_2048_m5e10_avg_13000, omega_cyl_flat_2048_m5e10_avg_13000*omega_factor, color='b', linestyle='--')
axarr[1].plot(r_cyl_flat_2048_b0_m5e10_avg_5250, omega_cyl_flat_2048_b0_m5e10_avg_5250*omega_factor, label=r'MR B0', color='r', linestyle=':')
axarr[1].plot(r_cyl_flat_2048_b0_m5e10_avg_13000, omega_cyl_flat_2048_b0_m5e10_avg_13000*omega_factor, color='b', linestyle=':')
axarr[1].plot(rref, r_k, color='g', linestyle='-.')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':16},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([1, 31/1.5])
#axarr[1].set_ylim([0.005, 0.022])
axarr[1].set_xlabel(r'$R[km]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
axarr[0].xaxis.set_major_formatter(ticks_x)
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].tick_params(labelsize=16)
axarr[1].set_ylabel(r'$<\Omega>[rad/s]$',fontsize=20)
axarr[1].set_xticks([10.0/1.5, 20.0/1.5, 30.0/1.5])

pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Flat_Rho_Omega_mask5e10_2048_Comp.pdf",  bbox_inches="tight")
pl.close()




times = [4250, 5250, 7250, 9250, 11250, 13000]
times_1024_les = [4000, 5000, 7000, 9000]

max_1024_m1e13 = np.array([np.max(omega_cyl_1024_m1e13_avg_4250), np.max(omega_cyl_1024_m1e13_avg_5250), np.max(omega_cyl_1024_m1e13_avg_7250), np.max(omega_cyl_1024_m1e13_avg_9250), np.max(omega_cyl_1024_m1e13_avg_11250)])

max_1024_les_m1e13 = np.array([np.max(omega_cyl_1024_les_m1e13_avg_4000), np.max(omega_cyl_1024_les_m1e13_avg_5000), np.max(omega_cyl_1024_les_m1e13_avg_7000), np.max(omega_cyl_1024_les_m1e13_avg_9000)])

max_2048_m1e13 = np.array([np.max(omega_cyl_2048_m1e13_avg_4250), np.max(omega_cyl_2048_m1e13_avg_5250), np.max(omega_cyl_2048_m1e13_avg_7250), np.max(omega_cyl_2048_m1e13_avg_9250), np.max(omega_cyl_2048_m1e13_avg_11250), np.max(omega_cyl_2048_m1e13_avg_13000)])

max_2048_b0_m1e13 = np.array([np.max(omega_cyl_2048_b0_m1e13_avg_4250), np.max(omega_cyl_2048_b0_m1e13_avg_5250), np.max(omega_cyl_2048_b0_m1e13_avg_7250), np.max(omega_cyl_2048_b0_m1e13_avg_9250), np.max(omega_cyl_2048_b0_m1e13_avg_11250), np.max(omega_cyl_2048_b0_m1e13_avg_13000)])

max_2048_les_m1e13 = np.array([np.max(omega_cyl_2048_les_m1e13_avg_4250), np.max(omega_cyl_2048_les_m1e13_avg_5250), np.max(omega_cyl_2048_les_m1e13_avg_7250), np.max(omega_cyl_2048_les_m1e13_avg_9250), np.max(omega_cyl_2048_les_m1e13_avg_11250), np.max(omega_cyl_2048_les_m1e13_avg_13000)])

max_4096_m1e13 = np.array([np.max(omega_cyl_4096_m1e13_avg_4250), np.max(omega_cyl_4096_m1e13_avg_5250), np.max(omega_cyl_4096_m1e13_avg_7250)])

max_4096_les_m1e13 = np.array([np.max(omega_cyl_4096_les_m1e13_avg_4250), np.max(omega_cyl_4096_les_m1e13_avg_5250), np.max(omega_cyl_4096_les_m1e13_avg_7250)])


fig, ax = pl.subplots()
ax.plot(times[:5], max_1024_m1e13, label=r'$LR$', color='r', linestyle=':')
ax.plot(times_1024_les, max_1024_les_m1e13, label=r'$LR LES$', color='r')
ax.plot(times, max_2048_m1e13, label=r'$MR$', color='b', linestyle=':')
ax.plot(times, max_2048_b0_m1e13, label=r'$MR B0$', color='b', linestyle='--')
ax.plot(times, max_2048_les_m1e13, label=r'$MR LES$', color='b')
ax.plot(times[:3], max_4096_m1e13, label=r'$HR$', color='k', linestyle=':')
ax.plot(times[:3], max_4096_les_m1e13, label=r'$HR LES$', color='k')
ax.set_ylabel(r'$\Delta \Omega$')
ax.legend(loc='lower right')
ax.set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_yscale("log")
pl.savefig("Max_omega_m1e13.pdf",  bbox_inches="tight")
pl.close()



max_min_1024_m5e10 = np.array([np.max(omega_cyl_flat_1024_m5e10_avg_4250) - np.min(omega_cyl_flat_1024_m5e10_avg_4250), np.max(omega_cyl_flat_1024_m5e10_avg_5250) - np.min(omega_cyl_flat_1024_m5e10_avg_5250), np.max(omega_cyl_flat_1024_m5e10_avg_7250) - np.min(omega_cyl_flat_1024_m5e10_avg_7250), np.max(omega_cyl_flat_1024_m5e10_avg_9250) - np.min(omega_cyl_flat_1024_m5e10_avg_9250), np.max(omega_cyl_flat_1024_m5e10_avg_11250) - np.min(omega_cyl_flat_1024_m5e10_avg_11250)])


max_min_1024_les_m5e10 = np.array([np.max(omega_cyl_flat_1024_les_m5e10_avg_4000) - np.min(omega_cyl_flat_1024_les_m5e10_avg_4000), np.max(omega_cyl_flat_1024_les_m5e10_avg_5000) - np.min(omega_cyl_flat_1024_les_m5e10_avg_5000), np.max(omega_cyl_flat_1024_les_m5e10_avg_7000) - np.min(omega_cyl_flat_1024_les_m5e10_avg_7000), np.max(omega_cyl_flat_1024_les_m5e10_avg_9000) - np.min(omega_cyl_flat_1024_les_m5e10_avg_9000)])

max_min_2048_m5e10 = np.array([np.max(omega_cyl_flat_2048_m5e10_avg_4250) - omega_cyl_flat_2048_m5e10_avg_4250[0], np.max(omega_cyl_flat_2048_m5e10_avg_5250) - omega_cyl_flat_2048_m5e10_avg_5250[0], np.max(omega_cyl_flat_2048_m5e10_avg_7250) - omega_cyl_flat_2048_m5e10_avg_7250[0], np.max(omega_cyl_flat_2048_m5e10_avg_9250) - omega_cyl_flat_2048_m5e10_avg_9250[0], np.max(omega_cyl_flat_2048_m5e10_avg_11250) - omega_cyl_flat_2048_m5e10_avg_11250[0], np.max(omega_cyl_flat_2048_m5e10_avg_13000) - omega_cyl_flat_2048_m5e10_avg_13000[0]])

max_min_2048_b0_m5e10 = np.array([np.max(omega_cyl_flat_2048_b0_m5e10_avg_4250) - omega_cyl_flat_2048_b0_m5e10_avg_4250[0], np.max(omega_cyl_flat_2048_b0_m5e10_avg_5250) - omega_cyl_flat_2048_b0_m5e10_avg_5250[0], np.max(omega_cyl_flat_2048_b0_m5e10_avg_7250) - omega_cyl_flat_2048_b0_m5e10_avg_7250[0], np.max(omega_cyl_flat_2048_b0_m5e10_avg_9250) - omega_cyl_flat_2048_b0_m5e10_avg_9250[0], np.max(omega_cyl_flat_2048_b0_m5e10_avg_11250) - omega_cyl_flat_2048_b0_m5e10_avg_11250[0], np.max(omega_cyl_flat_2048_b0_m5e10_avg_13000) - omega_cyl_flat_2048_b0_m5e10_avg_13000[0]])

max_min_2048_les_m5e10 = np.array([np.max(omega_cyl_flat_2048_les_m5e10_avg_4250) - omega_cyl_flat_2048_les_m5e10_avg_4250[0], np.max(omega_cyl_flat_2048_les_m5e10_avg_5250) - omega_cyl_flat_2048_les_m5e10_avg_5250[0], np.max(omega_cyl_flat_2048_les_m5e10_avg_7250) - omega_cyl_flat_2048_les_m5e10_avg_7250[0], np.max(omega_cyl_flat_2048_les_m5e10_avg_9250) - omega_cyl_flat_2048_les_m5e10_avg_9250[0], np.max(omega_cyl_flat_2048_les_m5e10_avg_11250) - omega_cyl_flat_2048_les_m5e10_avg_11250[0], np.max(omega_cyl_flat_2048_les_m5e10_avg_13000) - omega_cyl_flat_2048_les_m5e10_avg_13000[0]])


max_min_4096_m5e10 = np.array([np.max(omega_cyl_flat_4096_m5e10_avg_4250) - np.min(omega_cyl_flat_4096_m5e10_avg_4250), np.max(omega_cyl_flat_4096_m5e10_avg_5250) - np.min(omega_cyl_flat_4096_m5e10_avg_5250), np.max(omega_cyl_flat_4096_m5e10_avg_7250) - np.min(omega_cyl_flat_4096_m5e10_avg_7250)])

max_min_4096_les_m5e10 = np.array([np.max(omega_cyl_flat_4096_les_m5e10_avg_4250) - np.min(omega_cyl_flat_4096_les_m5e10_avg_4250), np.max(omega_cyl_flat_4096_les_m5e10_avg_5250) - np.min(omega_cyl_flat_4096_les_m5e10_avg_5250), np.max(omega_cyl_flat_4096_les_m5e10_avg_7250) - np.min(omega_cyl_flat_4096_les_m5e10_avg_7250)])



print(max_min_2048_m5e10)
print(max_min_2048_b0_m5e10)
print(max_min_2048_les_m5e10)

fig, ax = pl.subplots()
ax.plot(times, max_min_2048_les_m5e10*omega_factor, label=r'MR LES', color='r')
ax.plot(times, max_min_2048_m5e10*omega_factor, label=r'MR', color='b')
ax.plot(times, max_min_2048_b0_m5e10*omega_factor, label=r'MR B0', color='g')
ax.set_ylabel(r'$\Delta \Omega[rad/s]$',fontsize=20)
ax.legend(loc='upper right',fontsize=16)
ax.set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax.xaxis.set_major_formatter(ticks_x)
#ax.set_ylim([0.03, 0.04])
ax.set_xlim([8.0/0.00494+3200, 50.0/0.00494+3200])
ax.tick_params(labelsize=14)
ax.set_xticks([10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.savefig("Max_min_omega.pdf",  bbox_inches="tight")
pl.close()

max_min_1024_m5e10 = np.array([np.max(omega_cyl_flat_1024_m5e10_avg_4250), np.max(omega_cyl_flat_1024_m5e10_avg_5250), np.max(omega_cyl_flat_1024_m5e10_avg_7250), np.max(omega_cyl_flat_1024_m5e10_avg_9250), np.max(omega_cyl_flat_1024_m5e10_avg_11250)])

max_min_1024_les_m5e10 = np.array([np.max(omega_cyl_flat_1024_les_m5e10_avg_4000), np.max(omega_cyl_flat_1024_les_m5e10_avg_5000), np.max(omega_cyl_flat_1024_les_m5e10_avg_7000), np.max(omega_cyl_flat_1024_les_m5e10_avg_9000)])

max_min_2048_m5e10 = np.array([np.max(omega_cyl_flat_2048_m5e10_avg_4250), np.max(omega_cyl_flat_2048_m5e10_avg_5250), np.max(omega_cyl_flat_2048_m5e10_avg_7250), np.max(omega_cyl_flat_2048_m5e10_avg_9250), np.max(omega_cyl_flat_2048_m5e10_avg_11250), np.max(omega_cyl_flat_2048_m5e10_avg_13000)])

max_min_2048_b0_m5e10 = np.array([np.max(omega_cyl_flat_2048_b0_m5e10_avg_4250), np.max(omega_cyl_flat_2048_b0_m5e10_avg_5250), np.max(omega_cyl_flat_2048_b0_m5e10_avg_7250), np.max(omega_cyl_flat_2048_b0_m5e10_avg_9250), np.max(omega_cyl_flat_2048_b0_m5e10_avg_11250), np.max(omega_cyl_flat_2048_b0_m5e10_avg_13000)])

max_min_2048_les_m5e10 = np.array([np.max(omega_cyl_flat_2048_les_m5e10_avg_4250), np.max(omega_cyl_flat_2048_les_m5e10_avg_5250), np.max(omega_cyl_flat_2048_les_m5e10_avg_7250), np.max(omega_cyl_flat_2048_les_m5e10_avg_9250), np.max(omega_cyl_flat_2048_les_m5e10_avg_11250), np.max(omega_cyl_flat_2048_les_m5e10_avg_13000)])

max_min_4096_m5e10 = np.array([np.max(omega_cyl_flat_4096_m5e10_avg_4250), np.max(omega_cyl_flat_4096_m5e10_avg_5250), np.max(omega_cyl_flat_4096_m5e10_avg_7250)])

max_min_4096_les_m5e10 = np.array([np.max(omega_cyl_flat_4096_les_m5e10_avg_4250), np.max(omega_cyl_flat_4096_les_m5e10_avg_5250), np.max(omega_cyl_flat_4096_les_m5e10_avg_7250)])

print(max_min_1024_m5e10, max_min_2048_m5e10)

fig, ax = pl.subplots()
ax.plot(times, max_min_2048_m5e10, label=r'$MR$', color='r')
ax.plot(times, max_min_2048_b0_m5e10, label=r'$MR B0$', color='b')
ax.plot(times, max_min_2048_les_m5e10, label=r'$MR LES$', color='g')
ax.set_ylabel(r'$\Delta \Omega$')
ax.legend(loc='lower right')
ax.set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_yscale("log")
pl.savefig("Max_omega.pdf",  bbox_inches="tight")
pl.close()

'''
long = 8000

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)

axarr[0].plot(t_fmass_2048_les_full_r100[:long], v_fmass_2048_les_full_r100[:long], label=r'$r_{ext}=150 km$', color='b', linestyle='-', linewidth=1)
axarr[0].plot(t_fmass_2048_les_full_r200[:long] - 100, v_fmass_2048_les_full_r200[:long], label=r'$r_{ext}=300 km$', color='r', linestyle='-', linewidth=1)
axarr[0].plot(t_fmass_2048_les_full_r300[:long] - 200, v_fmass_2048_les_full_r300[:long], label=r'$r_{ext}=450 km$', color='g', linestyle='-', linewidth=1)
axarr[0].legend(loc="upper right", fontsize=12)
axarr[0].set_ylabel(r'$\dot{M}_{ej}[M_{\odot}/ms]$',fontsize=20)
axarr[0].set_yscale("log")
axarr[0].set_ylim([1e-10*4.94e-3, 2e-2*4.94e-3])
axarr[0].tick_params(labelsize=14)

ticks_y = ticker.FuncFormatter(lambda y, pos: '{:.0e}'.format(y/4.94e-3))
axarr[0].yaxis.set_major_formatter(ticks_y)
#axarr[0].ticklabel_format(axis="y", style="sci", scilimits=(-10,-1))
axarr[0].set_yticks([1e-9*4.94e-3, 1e-7*4.94e-3, 1e-5*4.94e-3, 1e-3*4.94e-3])
axarr[1].plot(t_wave_c2p2_2048_les_full_r100[:long], v_wave_c2p2_2048_les_full_r100[:long], label=r'$r_{ext}=150 km$', color='b', linestyle='-', linewidth=1)
axarr[1].plot(t_wave_c2p2_2048_les_full_r200[:long] - 100, v_wave_c2p2_2048_les_full_r200[:long], label=r'$r_{ext}=300 km$', color='r', linestyle='-', linewidth=1)
axarr[1].plot(t_wave_c2p2_2048_les_full_r300[:long] - 200, v_wave_c2p2_2048_les_full_r300[:long], label=r'$r_{ext}=450 km$', color='g', linestyle='-', linewidth=1)
axarr[1].set_ylabel(r'$C_{2,2}$',fontsize=20)
axarr[1].set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
axarr[1].tick_params(labelsize=14)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([-10.0/0.00494+3200, 3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
axarr[1].set_xlim([-15.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_Wave_fluxMass_2048_LES.pdf", bbox_inches="tight")
pl.close()

'''
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int3d_2048_les_m5e10_int, (P1_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$\textrm{MR LES}~m=1$', color='r')
axarr[0].plot(t_int3d_2048_les_m5e10_int, (P2_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$\textrm{MR LES}~m=2$', color='r', linestyle='--')
axarr[0].plot(t_int3d_2048_m5e10_int, (P1_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$\textrm{MR}~m=1$', color='b')
axarr[0].plot(t_int3d_2048_m5e10_int, (P2_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$\textrm{MR}~m=2$', color='b', linestyle='--')
axarr[0].plot(t_int3d_2048_b0_m5e10_int, (P1_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$\textrm{MR B0}~m=1$', color='g')
axarr[0].plot(t_int3d_2048_b0_m5e10_int, (P2_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$\textrm{MR B0}~m=2$', color='g', linestyle='--')
axarr[0].legend(loc='lower right', ncol=3,fontsize=10)
axarr[0].set_ylabel(r'$<\rho_m>_{13}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[0].tick_params(labelsize=14)
axarr[0].set_xlim([3100, 9250])
axarr[1].plot(t_int3d_2048_les_m1e13_m5e10_int, (P1_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m5e10_int[:-1], color='r')
axarr[1].plot(t_int3d_2048_les_m1e13_m5e10_int, (P2_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m5e10_int[:-1], color='r', linestyle='--')
axarr[1].plot(t_int3d_2048_m1e13_m5e10_int, (P1_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_les_m5e10_int, color='b')
axarr[1].plot(t_int3d_2048_m1e13_m5e10_int, (P2_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_les_m5e10_int, color='b', linestyle='--')
axarr[1].plot(t_int3d_2048_b0_m1e13_m5e10_int[:-1], (P1_int3d_2048_b0_m1e13_m5e10_int[:-1])/P0_int3d_2048_les_m5e10_int, color='g')
axarr[1].plot(t_int3d_2048_b0_m1e13_m5e10_int[:-1], (P2_int3d_2048_b0_m1e13_m5e10_int[:-1])/P0_int3d_2048_les_m5e10_int, color='g', linestyle='--')
axarr[1].set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
axarr[1].set_ylabel(r'$<\rho_m>_{10}^{13}$',fontsize=20)
axarr[1].set_yscale("log")
axarr[1].tick_params(labelsize=14)
axarr[1].set_xlim([3100, 13000])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("Pm_2048_Comp.pdf", bbox_inches="tight")
pl.close()





f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int3d_2048_les_m5e10_int, (P1_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$\textrm{MR LES}~m=1$', color='r')
axarr[0].plot(t_int3d_2048_les_m5e10_int, (P2_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$\textrm{MR LES}~m=2$', color='r', linestyle='--')
axarr[0].plot(t_int3d_2048_m5e10_int, (P1_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$\textrm{MR}~m=1$', color='b')
axarr[0].plot(t_int3d_2048_m5e10_int, (P2_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$\textrm{MR}~m=2$', color='b', linestyle='--')
axarr[0].plot(t_int3d_2048_b0_m5e10_int, (P1_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$\textrm{MR B0}~m=1$', color='g')
axarr[0].plot(t_int3d_2048_b0_m5e10_int, (P2_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$\textrm{MR B0}~m=2$', color='g', linestyle='--')
axarr[0].legend(loc='lower right', ncol=3,fontsize=10)
axarr[0].set_ylabel(r'$<\rho_m>_{13}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[0].tick_params(labelsize=14)
axarr[0].set_xlim([3100, 9250])
from scipy.signal import savgol_filter
v_wave_omega_smooth_2048_les_r200 = np.concatenate([v_wave_omega_2048_les_r200[:2000], savgol_filter(v_wave_omega_2048_les_r200[2000:2500], 201, 5), savgol_filter(v_wave_omega_2048_les_r200[2500:], 501, 3)])
v_wave_omega_smooth_2048_r200 = np.concatenate([v_wave_omega_2048_r200[:2000], savgol_filter(v_wave_omega_2048_r200[2000:2500], 201, 5), savgol_filter(v_wave_omega_2048_r200[2500:], 501, 3)])
v_wave_omega_smooth_2048_b0_r200 = np.concatenate([v_wave_omega_2048_b0_r200[:2000], savgol_filter(v_wave_omega_2048_b0_r200[2000:], 1001, 1)])
axarr[1].plot(t_wave_omega_2048_les_r200, v_wave_omega_smooth_2048_les_r200/0.00494/(np.pi), label=r'MR LES', color='r')
axarr[1].plot(t_wave_omega_2048_r200, v_wave_omega_smooth_2048_r200/0.00494/(np.pi), label=r'MR', color='b')
axarr[1].plot(t_wave_omega_2048_b0_r200, v_wave_omega_smooth_2048_b0_r200/0.00494/(np.pi), label=r'MR B0', color='g')
axarr[1].plot(t_wave_omega_4096_les_r200, v_wave_omega_4096_les_r200/0.00494/(np.pi), label=r'HR LES', color='m')
axarr[1].plot(t_wave_omega_4096_r200, v_wave_omega_4096_r200/0.00494/(np.pi), label=r'HR', color='c')
axarr[1].legend(loc='upper right', ncol=2)
axarr[1].set_ylabel(r'$f_{2,2}[kHz]$',fontsize=20)
axarr[1].set_yticks([1,2,3, 4, 5])
axarr[1].set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
axarr[1].tick_params(labelsize=14)
axarr[1].set_xlim([3100, 13000])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("Pm_2048_Onda.pdf", bbox_inches="tight")
pl.close()



pl.figure()
pl.plot(t_max_m1em16_2048_les, invbeta_max_m1em16_2048_les, label=r'$2048_{LES}$', color='r')
pl.ylabel(r'$max(\frac{1}{\beta})$')
pl.xlabel(r'$t$',fontsize=20)
pl.yscale("log")
pl.savefig("Max_invbeta_2048_LES.pdf",  bbox_inches="tight")
pl.close()

'''

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(t_wave_c2p2_2048_les_r200[:5001], v2_wave_c2p2_2048_les_r200[:5001], label=r'$\textrm{MR LES}~m=2$', color='r')
axarr[0].plot(t_wave_c2p1_2048_les_r200[:5001], v2_wave_c2p1_2048_les_r200[:5001], label=r'$\textrm{MR LES}~m=1$', color='r', linestyle='--')
axarr[0].plot(t_wave_c2p2_2048_les_full_r200[:8001] + 250, v2_wave_c2p2_2048_les_full_r200[:8001], label=r'$\textrm{MR LES FULL}~m=2$', color='b')
axarr[0].plot(t_wave_c2p1_2048_les_full_r200[:8001] + 250, v2_wave_c2p1_2048_les_full_r200[:8001], label=r'$\textrm{MR LES FULL}~m=1$', color='b', linestyle='--')
axarr[0].plot(t_wave_c2p2_2048_b0_r200[:5001], v2_wave_c2p2_2048_b0_r200[:5001], label=r'$\textrm{MR B0}~m=2$', color='g')
axarr[0].plot(t_wave_c2p1_2048_b0_r200[:5001], v2_wave_c2p1_2048_b0_r200[:5001], label=r'$\textrm{MR B0}~m=1$', color='g', linestyle='--')
axarr[0].legend(loc='lower right', ncol=3)
axarr[0].set_yscale('log')
axarr[0].set_ylabel(r'$|C_{2,m}|$',fontsize=22)
axarr[0].set_ylim([1.1e-13, 1e-4])
axarr[0].set_xlim([3100, 50.0/0.00494+3200])

from scipy.signal import savgol_filter
v_wave_omega_smooth_2048_les_r200 = np.concatenate([v_wave_omega_2048_les_r200[:2000], savgol_filter(v_wave_omega_2048_les_r200[2000:2500], 201, 5), savgol_filter(v_wave_omega_2048_les_r200[2500:], 501, 3)])
v_wave_omega_smooth_2048_les_full_r200 = np.concatenate([v_wave_omega_2048_les_full_r200[:3000], savgol_filter(v_wave_omega_2048_les_full_r200[3000:3500], 201, 5), savgol_filter(v_wave_omega_2048_les_full_r200[3500:], 501, 3)])
v_wave_omega_smooth_2048_b0_r200 = np.concatenate([v_wave_omega_2048_b0_r200[:2000], savgol_filter(v_wave_omega_2048_b0_r200[2000:], 1001, 1)])
axarr[1].plot(t_wave_omega_2048_les_r200, v_wave_omega_smooth_2048_les_r200/0.00494/(np.pi), label=r'MR LES', color='r')
axarr[1].plot(t_wave_omega_2048_les_full_r200 + 250, v_wave_omega_smooth_2048_les_full_r200/0.00494/(np.pi), label=r'MR LES FULL', color='b')
axarr[1].plot(t_wave_omega_2048_b0_r200, v_wave_omega_smooth_2048_b0_r200/0.00494/(np.pi), label=r'MR B0', color='g')
axarr[1].legend(loc='upper right')
axarr[1].set_ylabel(r'$f_{2,2}[kHz]$',fontsize=20)
axarr[1].set_yticks([1,2,3, 4, 5])
axarr[1].set_xlabel(r'$t-t_{merger}[ms]$',fontsize=20)
axarr[1].tick_params(labelsize=14)
axarr[1].set_ylim([0.8, 5.4])
axarr[1].set_xlim([3100, 13000])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])

pl.tight_layout(pad=0.1)
pl.savefig("full_L2_M210_2048_Comp.pdf",  bbox_inches="tight")
pl.close()


# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_4250, Btor_cyl_2048_les_full_m1e13_avg_4250, label=r'$5 ms$', color='b')
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_5250, Btor_cyl_2048_les_full_m1e13_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_7250, Btor_cyl_2048_les_full_m1e13_avg_7250, label=r'$20 ms$', color='g')
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_9250, Btor_cyl_2048_les_full_m1e13_avg_9250, label=r'$30 ms$', color='y')
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_11250, Btor_cyl_2048_les_full_m1e13_avg_11250, label=r'$40 ms$', color='m')
axarr[0].plot(r_cyl_2048_les_full_m1e13_avg_13000, Btor_cyl_2048_les_full_m1e13_avg_13000, label=r'$50 ms$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0.5, 16/1.5])
#axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{13} [G]$',fontsize=20)
axarr[0].tick_params(labelsize=14)
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_4250, Bpol_cyl_2048_les_full_m1e13_avg_4250, label=r'$5 ms$', color='b')
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_5250, Bpol_cyl_2048_les_full_m1e13_avg_5250, label=r'$10 ms$', color='r')
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_7250, Bpol_cyl_2048_les_full_m1e13_avg_7250, label=r'$20 ms$', color='g')
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_9250, Bpol_cyl_2048_les_full_m1e13_avg_9250, label=r'$30 ms$', color='y')
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_11250, Bpol_cyl_2048_les_full_m1e13_avg_11250, label=r'$40 ms$', color='m')
axarr[1].plot(r_cyl_2048_les_full_m1e13_avg_13000, Bpol_cyl_2048_les_full_m1e13_avg_13000, label=r'$50 ms$', color='c')
#axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0.5, 16/1.5])
axarr[1].set_xlabel(r'$R[km]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
axarr[0].xaxis.set_major_formatter(ticks_x)
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_ylabel(r'$<B_{pol}>_{13} [G]$',fontsize=20)
axarr[1].tick_params(labelsize=14)
axarr[1].set_xticks([5.0/1.5, 10.0/1.5, 15.0/1.5])
pl.tight_layout(pad=0.1)
pl.savefig("full_Cylinder_Btor_Bpol_mask1e13_2048_LES.pdf",  bbox_inches="tight")
pl.close()

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_4250, Btor_cyl_2048_les_full_m1e13_m5e10_avg_4250, label=r'$5 ms$', color='b')
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_5250, Btor_cyl_2048_les_full_m1e13_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_7250, Btor_cyl_2048_les_full_m1e13_m5e10_avg_7250, label=r'$20 ms$', color='g')
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_9250, Btor_cyl_2048_les_full_m1e13_m5e10_avg_9250, label=r'$30 ms$', color='y')
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_11250, Btor_cyl_2048_les_full_m1e13_m5e10_avg_11250, label=r'$40 ms$', color='m')
axarr[0].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_13000, Btor_cyl_2048_les_full_m1e13_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0.5, 31/1.5])
#axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].tick_params(labelsize=14)
axarr[0].set_ylabel(r'$<B_{tor}>_{10}^{13} [G]$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_4250, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_4250, label=r'$5 ms$', color='b')
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_5250, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_7250, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_7250, label=r'$20 ms$', color='g')
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_9250, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_9250, label=r'$30 ms$', color='y')
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_11250, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_11250, label=r'$40 ms$', color='m')
axarr[1].plot(r_cyl_2048_les_full_m1e13_m5e10_avg_13000, Bpol_cyl_2048_les_full_m1e13_m5e10_avg_13000, label=r'$50 ms$', color='c')
#axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0.5, 31/1.5])
axarr[1].set_xlabel(r'$R[km]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
axarr[0].xaxis.set_major_formatter(ticks_x)
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_ylabel(r'$<B_{pol}>_{10}^{13} [G]$',fontsize=20)
axarr[1].tick_params(labelsize=14)
axarr[1].set_xticks([10.0/1.5, 20.0/1.5, 30.0/1.5])
pl.tight_layout(pad=0.1)
pl.savefig("full_Cylinder_Btor_Bpol_mask1e13_mask5e10_2048_LES.pdf",  bbox_inches="tight")
pl.close()

f, ax = plt.subplots(2, sharex=True)
ax[0].plot(t_int3d_2048_les_full_m1e13_m5e10_avg, B_int3d_2048_les_full_m1e13_m5e10_avg, label=r'$<B>_{10}^{13}$', color='b', linestyle='--')
ax[0].plot(t_int3d_2048_les_full_m1e13_m5e10_avg, Bpol_int3d_2048_les_full_m1e13_m5e10_avg, label=r'$<B_{pol}>_{10}^{13}$', color='g', linestyle='--')
ax[0].plot(t_int3d_2048_les_full_m1e13_m5e10_avg, Btor_int3d_2048_les_full_m1e13_m5e10_avg, label=r'$<B_{tor}>_{10}^{13}$', color='r', linestyle='--')
ax[0].plot(t_int3d_2048_les_full_m1e13_avg, B_int3d_2048_les_full_m1e13_avg, label=r'$<B>_{13}$', color='b')
ax[0].plot(t_int3d_2048_les_full_m1e13_avg, Bpol_int3d_2048_les_full_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g')
ax[0].plot(t_int3d_2048_les_full_m1e13_avg, Btor_int3d_2048_les_full_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r')
ax[0].tick_params(labelsize=14)
ax[0].set_ylim([6e11, 2e16]) 
ax[0].set_ylabel(r'$<B>_{full} [G]$',fontsize=20) 
ax[0].set_xlim([3100, 13200])
ax[0].set_yscale("log")
ax[0].set_yticks([1e12, 1e13, 1e14, 1e15, 1e16])
ax[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, B_int3d_2048_les_m1e13_m5e10_avg, label=r'$<B>_{10}^{13}$', color='b', linestyle='--')
ax[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, Bpol_int3d_2048_les_m1e13_m5e10_avg, label=r'$<B_{pol}>_{10}^{13}$', color='g', linestyle='--')
ax[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, Btor_int3d_2048_les_m1e13_m5e10_avg, label=r'$<B_{tor}>_{10}^{13}$', color='r', linestyle='--')
ax[1].plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}$', color='b')
ax[1].plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g')
ax[1].plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r')
ax[1].set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax[1].xaxis.set_major_formatter(ticks_x)
ax[1].set_yscale("log")
plt.legend(loc='lower right',fontsize=12, ncol=3)
ax[1].tick_params(labelsize=14)
ax[1].set_yticks([1e12, 1e13, 1e14, 1e15, 1e16])
ax[1].set_ylim([6e11, 2e16]) 
ax[1].set_ylabel(r'$<B> [G]$',fontsize=20) 
ax[1].set_xlim([3100, 13200])
ax[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_3D_masks_2048_LES.pdf",  bbox_inches="tight")
pl.close()



f, ax = plt.subplots()
ax.plot(t_int3d_2048_les_full_m1e13_m5e10_avg + 250, B_int3d_2048_les_full_m1e13_m5e10_avg, label=r'$<B>_{10}^{13} FULL$', color='k')
ax.plot(t_int3d_2048_les_m1e13_m5e10_avg, B_int3d_2048_les_m1e13_m5e10_avg, label=r'$<B>_{10}^{13}$', color='r', linestyle='--')
ax.plot(t_int3d_2048_les_full_m1e13_avg + 250, B_int3d_2048_les_full_m1e13_avg, label=r'$<B>_{13} FULL$', color='k')
ax.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}$', color='r', linestyle='--')
ax.set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_yscale("log")
plt.legend(loc='lower right',fontsize=12, ncol=2)
ax.tick_params(labelsize=14)
ax.set_yticks([1e12, 1e13, 1e14, 1e15, 1e16])
ax.set_ylim([2e13, 2e16]) 
ax.set_ylabel(r'$<B> [G]$',fontsize=20) 
ax.set_xlim([3100, 13200])
ax.set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_3D_masks_2048_LES_comp.pdf",  bbox_inches="tight")
pl.close()

'''
f, axarr = pl.subplots(2, sharex=True, figsize=(6, 6))
axarr[0].plot(t_int3d_4096_m1e13_avg, Bpol_int3d_4096_m1e13_avg, label=r'$<B_{pol}> \textrm{HR}$', color='b', linestyle=':')
axarr[0].plot(t_int3d_4096_m1e13_avg, Btor_int3d_4096_m1e13_avg, label=r'$<B_{tor}> \textrm{HR}$', color='r', linestyle=':')
axarr[0].plot(t_int3d_4096_les_m1e13_avg, Bpol_int3d_4096_les_m1e13_avg, label=r'$<B_{pol}> \textrm{HR LES}$', color='b', linestyle='--')
axarr[0].plot(t_int3d_4096_les_m1e13_avg, Btor_int3d_4096_les_m1e13_avg, label=r'$<B_{tor}> \textrm{HR LES}$', color='r', linestyle='--')
axarr[0].plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}> \textrm{MR LES}$', color='b')
axarr[0].plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}> \textrm{MR LES}$', color='r')
axarr[0].set_yscale("log")
axarr[0].legend(loc='lower right',fontsize=12)
axarr[0].set_ylabel(r'$<B>_{13}[G]$',fontsize=20)
axarr[0].set_ylim([1.1e11, 2e16])
axarr[0].set_xlim([3100, 9250])
axarr[0].tick_params(axis='y', labelsize=14)
axarr[1].plot(t_int3d_4096_m1e13_m5e10_avg, Bpol_int3d_4096_m1e13_m5e10_avg, color='b', linestyle=':')
axarr[1].plot(t_int3d_4096_m1e13_m5e10_avg, Btor_int3d_4096_m1e13_m5e10_avg, color='r', linestyle=':')
axarr[1].plot(t_int3d_4096_les_m1e13_m5e10_avg, Bpol_int3d_4096_les_m1e13_m5e10_avg, color='b', linestyle='--')
axarr[1].plot(t_int3d_4096_les_m1e13_m5e10_avg, Btor_int3d_4096_les_m1e13_m5e10_avg, color='r', linestyle='--')
axarr[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, Bpol_int3d_2048_les_m1e13_m5e10_avg, color='b')
axarr[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, Btor_int3d_2048_les_m1e13_m5e10_avg, color='r')
axarr[1].set_yscale("log")
axarr[1].set_ylabel(r'$<B>_{10}^{13}[G]$',fontsize=20)
axarr[1].set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
axarr[1].tick_params(axis='x', labelsize=14)
axarr[1].tick_params(axis='y', labelsize=14)
axarr[1].set_xlim([3100, 9250])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_3D_LES_4096.pdf",  bbox_inches="tight")
pl.close()



f, axarr = pl.subplots(2, sharex=True, figsize=(6, 6))
axarr[0].plot(t_int3d_1024_m1e13_avg, B_int3d_1024_m1e13_avg, label=r'LR', color='b', linestyle='--')
axarr[0].plot(t_int3d_1024_les_m1e13_avg, B_int3d_1024_les_m1e13_avg, label=r'LR LES', color='b')
axarr[0].plot(t_int3d_2048_m1e13_avg, B_int3d_2048_m1e13_avg, label=r'MR', color='r', linestyle='--')
axarr[0].plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'MR LES', color='r')
axarr[0].plot(t_int3d_4096_m1e13_avg, B_int3d_4096_m1e13_avg, label=r'HR', color='k', linestyle='--')
axarr[0].plot(t_int3d_4096_les_m1e13_avg, B_int3d_4096_les_m1e13_avg, label=r'HR LES', color='k')
axarr[0].set_yscale("log")
axarr[0].legend(loc='lower right',fontsize=10)
axarr[0].set_ylim([2e11, 3e16]) 
axarr[0].set_ylabel(r'$<B>_{13} [G]$',fontsize=20)
axarr[0].tick_params(axis='y', labelsize=14)
axarr[1].plot(t_int3d_1024_m1e13_m5e10_avg, B_int3d_1024_m1e13_m5e10_avg, label=r'$LR$', color='b', linestyle='--')
axarr[1].plot(t_int3d_1024_les_m1e13_m5e10_avg, B_int3d_1024_les_m1e13_m5e10_avg, label=r'$LR_{LES}$', color='b')
axarr[1].plot(t_int3d_2048_m1e13_m5e10_avg, B_int3d_2048_m1e13_m5e10_avg, label=r'$MR$', color='r', linestyle='--')
axarr[1].plot(t_int3d_2048_les_m1e13_m5e10_avg, B_int3d_2048_les_m1e13_m5e10_avg, label=r'$MR_{LES}$', color='r')
axarr[1].plot(t_int3d_4096_m1e13_m5e10_avg, B_int3d_4096_m1e13_m5e10_avg, label=r'$HR$', color='k', linestyle='--')
axarr[1].plot(t_int3d_4096_les_m1e13_m5e10_avg, B_int3d_4096_les_m1e13_m5e10_avg, label=r'$HR_{LES}$', color='k')
axarr[1].set_yscale("log")
axarr[1].set_ylim([1.2e12, 1.2e15]) 
axarr[1].set_ylabel(r'$<B>_{10}^{13} [G]$',fontsize=20)
axarr[1].set_xlim([3100, 9250]) 
axarr[1].set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
axarr[1].tick_params(axis='x', labelsize=14)
axarr[1].tick_params(axis='y', labelsize=14)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[1].xaxis.set_major_formatter(ticks_x)
axarr[1].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_B10_13_INT_3D_m5e10.pdf",  bbox_inches="tight")
pl.close()

fig, ax = pl.subplots(2, sharex=False)
from scipy.signal import savgol_filter
v_wave_omega_smooth_2048_les_r200 = np.concatenate([v_wave_omega_2048_les_r200[:2000], savgol_filter(v_wave_omega_2048_les_r200[2000:2500], 201, 5), savgol_filter(v_wave_omega_2048_les_r200[2500:], 501, 3)])
v_wave_omega_smooth_2048_r200 = np.concatenate([v_wave_omega_2048_r200[:2000], savgol_filter(v_wave_omega_2048_r200[2000:2500], 201, 5), savgol_filter(v_wave_omega_2048_r200[2500:], 501, 3)])
v_wave_omega_smooth_2048_b0_r200 = np.concatenate([v_wave_omega_2048_b0_r200[:2000], savgol_filter(v_wave_omega_2048_b0_r200[2000:], 1001, 1)])
ax[0].plot(t_wave_omega_2048_les_r200, v_wave_omega_smooth_2048_les_r200/0.00494/(np.pi), label=r'MR LES', color='r')
ax[0].plot(t_wave_omega_2048_r200, v_wave_omega_smooth_2048_r200/0.00494/(np.pi), label=r'MR', color='b')
ax[0].plot(t_wave_omega_2048_b0_r200, v_wave_omega_smooth_2048_b0_r200/0.00494/(np.pi), label=r'MR B0', color='g')
ax[0].plot(t_wave_omega_4096_les_r200, v_wave_omega_4096_les_r200/0.00494/(np.pi), label=r'HR LES', color='m')
ax[0].plot(t_wave_omega_4096_r200, v_wave_omega_4096_r200/0.00494/(np.pi), label=r'HR', color='c')
ax[0].legend(loc='upper right', ncol=2)
ax[0].set_ylabel(r'$f[kHz]$',fontsize=20)
ax[0].set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
#ax[0].set_ylim([0.015, 0.088])
ax[0].set_xlim([3100, 50.0/0.00494+3200])
ax[0].tick_params(labelsize=14)
ax[0].set_xticks([10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
ax[0].set_yticks([1,2,3, 4, 5])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax[0].xaxis.set_major_formatter(ticks_x)
ax[1].plot(f_wave_fft_2048_les_r200, v_wave_fft_2048_les_r200, label=r'$MR LES$', color='r')
ax[1].plot(f_wave_fft_2048_r200, v_wave_fft_2048_r200, label=r'$MR$', color='b')
ax[1].plot(f_wave_fft_2048_b0_r200, v_wave_fft_2048_b0_r200, label=r'$MR B0$', color='g')
ax[1].plot(f_wave_fft_4096_r200, v_wave_fft_4096_r200, label=r'$HR$', color='c')
ax[1].plot(f_wave_fft_4096_les_r200, v_wave_fft_4096_les_r200, label=r'$HR LES$', color='m')
ax[1].set_yscale('log')
ax[1].set_ylabel(r'$\mathcal{F}\{C_{2,2}\}$',fontsize=20)
ax[1].set_xlabel(r'$f[kHz]$',fontsize=20)
ax[1].set_xlim([1.4*0.00494, 3.9*0.00494])
ax[1].set_ylim([1e-9, 1e-3])
ax[1].tick_params(labelsize=14)
ax[1].set_xticks([1.5*0.00494, 2*0.00494 , 2.5*0.00494, 3*0.00494, 3.5*0.00494])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x/0.00494))
ax[1].xaxis.set_major_formatter(ticks_x)
pl.tight_layout(pad=0.1)
pl.savefig("Onda_omega_r200.pdf",  bbox_inches="tight")
pl.close()

pl.figure()
pl.plot(t_fmassAcc_2048_les_r200[:3100], v_fmassAcc_2048_les_r200[:3100], label=r'$2048_{LES}$', color='r')
pl.plot(t_fmassAcc_2048_r200[:3100], v_fmassAcc_2048_r200[:3100], label=r'$2048$', color='b')
pl.plot(t_fmassAcc_2048_b0_r200[:3100], v_fmassAcc_2048_b0_r200[:3100], label=r'$2048_{B0}$', color='g')
pl.plot(t_fmassAcc_4096_r200[:3100], v_fmassAcc_4096_r200[:3100], label=r'$4096$', color='c')
pl.plot(t_fmassAcc_4096_les_r200[:3100], v_fmassAcc_4096_les_r200[:3100], label=r'$4096_{LES}$', color='m')
pl.legend(loc='lower right')
pl.xlabel(r'$t$',fontsize=20)
pl.savefig("Mejected.pdf",  bbox_inches="tight")
pl.close()
'''

# Two subplots sharing x axes
f, axarr = pl.subplots(3, sharex=True, figsize=(6, 6))
axarr[0].plot(t_int3d_2048_m5e10_int, Erot_int3d_2048_m5e10_int, label=r'$MR_{10}$', color='r')
axarr[0].plot(t_int3d_2048_les_m5e10_int, Erot_int3d_2048_les_m5e10_int, label=r'$MR_{10} LES$', color='b')
axarr[0].plot(t_int3d_2048_les_full_m5e10_int + 250, Erot_int3d_2048_les_full_m5e10_int, label=r'$MR_{10} LES Full$', color='k')
axarr[0].tick_params(axis='y', labelsize=14)
axarr[0].set_ylabel(r'$E_{rot} [erg]$',fontsize=20)
axarr[0].set_yticks(np.arange(1e53, 4.5e53, 1e53))
t = axarr[0].yaxis.get_offset_text()
t.set_x(-0.05)
axarr[1].plot(t_int3d_2048_m5e10_int, Eth_int3d_2048_m5e10_int, label=r'$2048_{10}$', color='r')
axarr[1].plot(t_int3d_2048_les_m5e10_int, Eth_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='b')
axarr[1].plot(t_int3d_2048_les_full_m5e10_int + 250, Eth_int3d_2048_les_full_m5e10_int, label=r'$2048_{10} LES Full$', color='k')
axarr[1].tick_params(axis='y', labelsize=14)
axarr[1].set_ylabel(r'$E_{th} [erg]$',fontsize=20)
axarr[1].set_yticks(np.arange(0.5e53, 5.5e53, 1.5e53))
t = axarr[1].yaxis.get_offset_text()
t.set_x(-0.05)
axarr[2].plot(t_int3d_2048_m5e10_int, Emag_int3d_2048_m5e10_int, label=r'MR', color='r')
axarr[2].plot(t_int3d_2048_les_m5e10_int, Emag_int3d_2048_les_m5e10_int, label=r'MR LES', color='b')
axarr[2].plot(t_int3d_2048_les_full_m5e10_int + 250, Emag_int3d_2048_les_full_m5e10_int, label=r'MR LES Full', color='k')
axarr[2].legend(loc="upper right", prop={'size':10},ncol=3, shadow=True)
axarr[2].tick_params(axis='x', labelsize=14)
axarr[2].tick_params(axis='y', labelsize=14)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
axarr[2].xaxis.set_major_formatter(ticks_x)
t = axarr[2].yaxis.get_offset_text()
t.set_x(-0.05)
axarr[2].set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
axarr[2].set_xlim([3100, 13250])
axarr[2].set_ylabel(r'$E_{mag} [erg]$',fontsize=20)
axarr[2].set_yticks(np.arange(0, max(Emag_int3d_2048_les_full_m5e10_int), 1e50))
axarr[2].set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
pl.tight_layout(pad=0.1)
pl.savefig("full_Energies_INT3D_mask5e10.pdf",  bbox_inches="tight")
pl.close()

'''
fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_1024_les_m1e13_m5e10_int, np.abs(P1_int3d_1024_les_m1e13_m5e10_int)/P0_int3d_1024_les_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_1024_les_m1e13_m5e10_int, np.abs(P2_int3d_1024_les_m1e13_m5e10_int)/P0_int3d_1024_les_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_1024_les_m1e13_m5e10_int, np.abs(P3_int3d_1024_les_m1e13_m5e10_int)/P0_int3d_1024_les_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_1024_les_m1e13_m5e10_int, np.abs(P4_int3d_1024_les_m1e13_m5e10_int)/P0_int3d_1024_les_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_1024_les_m5e10_int, np.abs(P1_int3d_1024_les_m5e10_int)/P0_int3d_1024_les_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_1024_les_m5e10_int, np.abs(P2_int3d_1024_les_m5e10_int)/P0_int3d_1024_les_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_1024_les_m5e10_int, np.abs(P3_int3d_1024_les_m5e10_int)/P0_int3d_1024_les_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_1024_les_m5e10_int, np.abs(P4_int3d_1024_les_m5e10_int)/P0_int3d_1024_les_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_1024_LES.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_1024_m1e13_m5e10_int, np.abs(P1_int3d_1024_m1e13_m5e10_int)/P0_int3d_1024_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_1024_m1e13_m5e10_int, np.abs(P2_int3d_1024_m1e13_m5e10_int)/P0_int3d_1024_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_1024_m1e13_m5e10_int, np.abs(P3_int3d_1024_m1e13_m5e10_int)/P0_int3d_1024_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_1024_m1e13_m5e10_int, np.abs(P4_int3d_1024_m1e13_m5e10_int)/P0_int3d_1024_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_1024_m5e10_int, np.abs(P1_int3d_1024_m5e10_int)/P0_int3d_1024_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_1024_m5e10_int, np.abs(P2_int3d_1024_m5e10_int)/P0_int3d_1024_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_1024_m5e10_int, np.abs(P3_int3d_1024_m5e10_int)/P0_int3d_1024_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_1024_m5e10_int, np.abs(P4_int3d_1024_m5e10_int)/P0_int3d_1024_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_1024.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_2048_les_m1e13_m5e10_int, np.abs(P1_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_2048_les_m1e13_m5e10_int, np.abs(P2_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_2048_les_m1e13_m5e10_int, np.abs(P3_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_2048_les_m1e13_m5e10_int, np.abs(P4_int3d_2048_les_m1e13_m5e10_int)/P0_int3d_2048_les_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_2048_les_m5e10_int, np.abs(P1_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_2048_les_m5e10_int, np.abs(P2_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_2048_les_m5e10_int, np.abs(P3_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_2048_les_m5e10_int, np.abs(P4_int3d_2048_les_m5e10_int)/P0_int3d_2048_les_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_2048_LES.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_2048_m1e13_m5e10_int, np.abs(P1_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_2048_m1e13_m5e10_int, np.abs(P2_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_2048_m1e13_m5e10_int, np.abs(P3_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_2048_m1e13_m5e10_int, np.abs(P4_int3d_2048_m1e13_m5e10_int)/P0_int3d_2048_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_2048_m5e10_int, np.abs(P1_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_2048_m5e10_int, np.abs(P2_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_2048_m5e10_int, np.abs(P3_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_2048_m5e10_int, np.abs(P4_int3d_2048_m5e10_int)/P0_int3d_2048_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_2048.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_2048_b0_m1e13_m5e10_int, np.abs(P1_int3d_2048_b0_m1e13_m5e10_int)/P0_int3d_2048_b0_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_2048_b0_m1e13_m5e10_int, np.abs(P2_int3d_2048_b0_m1e13_m5e10_int)/P0_int3d_2048_b0_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_2048_b0_m1e13_m5e10_int, np.abs(P3_int3d_2048_b0_m1e13_m5e10_int)/P0_int3d_2048_b0_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_2048_b0_m1e13_m5e10_int, np.abs(P4_int3d_2048_b0_m1e13_m5e10_int)/P0_int3d_2048_b0_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_2048_b0_m5e10_int, np.abs(P1_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_2048_b0_m5e10_int, np.abs(P2_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_2048_b0_m5e10_int, np.abs(P3_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_2048_b0_m5e10_int, np.abs(P4_int3d_2048_b0_m5e10_int)/P0_int3d_2048_b0_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_2048_B0.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_4096_les_m1e13_m5e10_int, np.abs(P1_int3d_4096_les_m1e13_m5e10_int)/P0_int3d_4096_les_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_4096_les_m1e13_m5e10_int, np.abs(P2_int3d_4096_les_m1e13_m5e10_int)/P0_int3d_4096_les_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_4096_les_m1e13_m5e10_int, np.abs(P3_int3d_4096_les_m1e13_m5e10_int)/P0_int3d_4096_les_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_4096_les_m1e13_m5e10_int, np.abs(P4_int3d_4096_les_m1e13_m5e10_int)/P0_int3d_4096_les_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_4096_les_m5e10_int, np.abs(P1_int3d_4096_les_m5e10_int)/P0_int3d_4096_les_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_4096_les_m5e10_int, np.abs(P2_int3d_4096_les_m5e10_int)/P0_int3d_4096_les_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_4096_les_m5e10_int, np.abs(P3_int3d_4096_les_m5e10_int)/P0_int3d_4096_les_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_4096_les_m5e10_int, np.abs(P4_int3d_4096_les_m5e10_int)/P0_int3d_4096_les_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_4096_LES.pdf", bbox_inches="tight")
pl.close()

fig, (ax1, ax2) = pl.subplots(1, 2, sharey=True)
ax1.plot(t_int3d_4096_m1e13_m5e10_int, np.abs(P1_int3d_4096_m1e13_m5e10_int)/P0_int3d_4096_m1e13_m5e10_int, label=r'$m=1$', color='r')
ax1.plot(t_int3d_4096_m1e13_m5e10_int, np.abs(P2_int3d_4096_m1e13_m5e10_int)/P0_int3d_4096_m1e13_m5e10_int, label=r'$m=2$', color='g')
ax1.plot(t_int3d_4096_m1e13_m5e10_int, np.abs(P3_int3d_4096_m1e13_m5e10_int)/P0_int3d_4096_m1e13_m5e10_int, label=r'$m=3$', color='b')
ax1.plot(t_int3d_4096_m1e13_m5e10_int, np.abs(P4_int3d_4096_m1e13_m5e10_int)/P0_int3d_4096_m1e13_m5e10_int, label=r'$m=4$', color='c')
ax1.legend()
ax1.set_xlabel(r'$t$',fontsize=20)
ax1.set_ylabel(r'$|P_m|/P_0$',fontsize=20)
ax1.set_yscale("log")
ax2.plot(t_int3d_4096_m5e10_int, np.abs(P1_int3d_4096_m5e10_int)/P0_int3d_4096_m5e10_int, label=r'$m=1$', color='r')
ax2.plot(t_int3d_4096_m5e10_int, np.abs(P2_int3d_4096_m5e10_int)/P0_int3d_4096_m5e10_int, label=r'$m=2$', color='g')
ax2.plot(t_int3d_4096_m5e10_int, np.abs(P3_int3d_4096_m5e10_int)/P0_int3d_4096_m5e10_int, label=r'$m=3$', color='b')
ax2.plot(t_int3d_4096_m5e10_int, np.abs(P4_int3d_4096_m5e10_int)/P0_int3d_4096_m5e10_int, label=r'$m=4$', color='c')
ax2.set_xlabel(r'$t$',fontsize=20)
ax2.set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("Pm_4096.pdf", bbox_inches="tight")
pl.close()



pl.figure()
pl.plot(t_wave_c2p2_2048_les_r200[:3100], v2_wave_c2p2_2048_les_r200[:3100], label=r'$|C_{2,2}|_{LES}$', color='r')
pl.plot(t_wave_c2p1_2048_les_r200[:3100], v2_wave_c2p1_2048_les_r200[:3100], label=r'$|C_{2,1}|_{LES}$', color='r', linestyle='--')
pl.plot(t_wave_c2p2_2048_r200[:3100], v2_wave_c2p2_2048_r200[:3100], label=r'$|C_{2,2}|$', color='b')
pl.plot(t_wave_c2p1_2048_r200[:3100], v2_wave_c2p1_2048_r200[:3100], label=r'$|C_{2,1}|$', color='b', linestyle='--')
pl.plot(t_wave_c2p2_2048_b0_r200[:3100], v2_wave_c2p2_2048_b0_r200[:3100], label=r'$|C_{2,2}|_{B0}$', color='g')
pl.plot(t_wave_c2p1_2048_b0_r200[:3100], v2_wave_c2p1_2048_b0_r200[:3100], label=r'$|C_{2,1}|_{B0}$', color='g', linestyle='--')
pl.legend(loc='upper right')
pl.yscale('log')
pl.xlabel(r'$t$',fontsize=20)
pl.savefig("L2_M210_2048_Comp_3100.pdf",  bbox_inches="tight")
pl.close()



plt.figure()
plt.plot(t_harm_avg_2048_1_12_05_c00, (v_harm_avg_2048_1_12_05_c00 + v_harm_avg_2048_1_12_05_c10 + v_harm_avg_2048_1_12_05_c20 + v_harm_avg_2048_1_12_05_c30 + v_harm_avg_2048_1_12_05_c40 + v_harm_avg_2048_1_12_05_c50 + v_harm_avg_2048_1_12_05_c60 + v_harm_avg_2048_1_12_05_c70 + v_harm_avg_2048_1_12_05_c80)/9, label=r'$C_{m=0}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c1m1, (v_harm_avg_2048_1_12_05_c1p1 + v_harm_avg_2048_1_12_05_c2p1 + v_harm_avg_2048_1_12_05_c3p1 + v_harm_avg_2048_1_12_05_c4p1 + v_harm_avg_2048_1_12_05_c5p1 + v_harm_avg_2048_1_12_05_c6p1 + v_harm_avg_2048_1_12_05_c7p1 + v_harm_avg_2048_1_12_05_c8p1 + v_harm_avg_2048_1_12_05_c1m1 + v_harm_avg_2048_1_12_05_c2m1 + v_harm_avg_2048_1_12_05_c3m1 + v_harm_avg_2048_1_12_05_c4m1 + v_harm_avg_2048_1_12_05_c5m1 + v_harm_avg_2048_1_12_05_c6m1 + v_harm_avg_2048_1_12_05_c7m1 + v_harm_avg_2048_1_12_05_c8m1)/16, label=r'$C_{m=1}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c2m2, (v_harm_avg_2048_1_12_05_c2p2 + v_harm_avg_2048_1_12_05_c3p2 + v_harm_avg_2048_1_12_05_c4p2 + v_harm_avg_2048_1_12_05_c5p2 + v_harm_avg_2048_1_12_05_c6p2 + v_harm_avg_2048_1_12_05_c7p2 + v_harm_avg_2048_1_12_05_c8p2 + v_harm_avg_2048_1_12_05_c2m2 + v_harm_avg_2048_1_12_05_c3m2 + v_harm_avg_2048_1_12_05_c4m2 + v_harm_avg_2048_1_12_05_c5m2 + v_harm_avg_2048_1_12_05_c6m2 + v_harm_avg_2048_1_12_05_c7m2 + v_harm_avg_2048_1_12_05_c8m2)/14, label=r'$C_{m=2}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c3m3, (v_harm_avg_2048_1_12_05_c3p3 + v_harm_avg_2048_1_12_05_c4p3 + v_harm_avg_2048_1_12_05_c5p3 + v_harm_avg_2048_1_12_05_c6p3 + v_harm_avg_2048_1_12_05_c7p3 + v_harm_avg_2048_1_12_05_c8p3 + v_harm_avg_2048_1_12_05_c3m3 + v_harm_avg_2048_1_12_05_c4m3 + v_harm_avg_2048_1_12_05_c5m3 + v_harm_avg_2048_1_12_05_c6m3 + v_harm_avg_2048_1_12_05_c7m3 + v_harm_avg_2048_1_12_05_c8m3)/12, label=r'$C_{m=3}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c4m4, (v_harm_avg_2048_1_12_05_c4p4 + v_harm_avg_2048_1_12_05_c5p4 + v_harm_avg_2048_1_12_05_c6p4 + v_harm_avg_2048_1_12_05_c7p4 + v_harm_avg_2048_1_12_05_c8p4 + v_harm_avg_2048_1_12_05_c4m4 + v_harm_avg_2048_1_12_05_c5m4 + v_harm_avg_2048_1_12_05_c6m4 + v_harm_avg_2048_1_12_05_c7m4 + v_harm_avg_2048_1_12_05_c8m4)/10, label=r'$C_{m=4}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c5m5, (v_harm_avg_2048_1_12_05_c5p5 + v_harm_avg_2048_1_12_05_c6p5 + v_harm_avg_2048_1_12_05_c7p5 + v_harm_avg_2048_1_12_05_c8p5 + v_harm_avg_2048_1_12_05_c5m5 + v_harm_avg_2048_1_12_05_c6m5 + v_harm_avg_2048_1_12_05_c7m5 + v_harm_avg_2048_1_12_05_c8m5)/8, label=r'$C_{m=5}$', color='y', linestyle=':')

plt.plot(t_harm_avg_2048_les_1_12_05_c00, (v_harm_avg_2048_les_1_12_05_c00 + v_harm_avg_2048_les_1_12_05_c10 + v_harm_avg_2048_les_1_12_05_c20 + v_harm_avg_2048_les_1_12_05_c30 + v_harm_avg_2048_les_1_12_05_c40 + v_harm_avg_2048_les_1_12_05_c50 + v_harm_avg_2048_les_1_12_05_c60 + v_harm_avg_2048_les_1_12_05_c70 + v_harm_avg_2048_les_1_12_05_c80)/9, label=r'$C_{m=0}^{LES}$', color='b')
plt.plot(t_harm_avg_2048_les_1_12_05_c1m1, (v_harm_avg_2048_les_1_12_05_c1p1 + v_harm_avg_2048_les_1_12_05_c2p1 + v_harm_avg_2048_les_1_12_05_c3p1 + v_harm_avg_2048_les_1_12_05_c4p1 + v_harm_avg_2048_les_1_12_05_c5p1 + v_harm_avg_2048_les_1_12_05_c6p1 + v_harm_avg_2048_les_1_12_05_c7p1 + v_harm_avg_2048_les_1_12_05_c8p1 + v_harm_avg_2048_les_1_12_05_c1m1 + v_harm_avg_2048_les_1_12_05_c2m1 + v_harm_avg_2048_les_1_12_05_c3m1 + v_harm_avg_2048_les_1_12_05_c4m1 + v_harm_avg_2048_les_1_12_05_c5m1 + v_harm_avg_2048_les_1_12_05_c6m1 + v_harm_avg_2048_les_1_12_05_c7m1 + v_harm_avg_2048_les_1_12_05_c8m1)/16, label=r'$C_{m=1}^{LES}$', color='r')
plt.plot(t_harm_avg_2048_les_1_12_05_c2m2, (v_harm_avg_2048_les_1_12_05_c2p2 + v_harm_avg_2048_les_1_12_05_c3p2 + v_harm_avg_2048_les_1_12_05_c4p2 + v_harm_avg_2048_les_1_12_05_c5p2 + v_harm_avg_2048_les_1_12_05_c6p2 + v_harm_avg_2048_les_1_12_05_c7p2 + v_harm_avg_2048_les_1_12_05_c8p2 + v_harm_avg_2048_les_1_12_05_c2m2 + v_harm_avg_2048_les_1_12_05_c3m2 + v_harm_avg_2048_les_1_12_05_c4m2 + v_harm_avg_2048_les_1_12_05_c5m2 + v_harm_avg_2048_les_1_12_05_c6m2 + v_harm_avg_2048_les_1_12_05_c7m2 + v_harm_avg_2048_les_1_12_05_c8m2)/14, label=r'$C_{m=2}^{LES}$', color='g')
plt.plot(t_harm_avg_2048_les_1_12_05_c3m3, (v_harm_avg_2048_les_1_12_05_c3p3 + v_harm_avg_2048_les_1_12_05_c4p3 + v_harm_avg_2048_les_1_12_05_c5p3 + v_harm_avg_2048_les_1_12_05_c6p3 + v_harm_avg_2048_les_1_12_05_c7p3 + v_harm_avg_2048_les_1_12_05_c8p3 + v_harm_avg_2048_les_1_12_05_c3m3 + v_harm_avg_2048_les_1_12_05_c4m3 + v_harm_avg_2048_les_1_12_05_c5m3 + v_harm_avg_2048_les_1_12_05_c6m3 + v_harm_avg_2048_les_1_12_05_c7m3 + v_harm_avg_2048_les_1_12_05_c8m3)/12, label=r'$C_{m=3}^{LES}$', color='c')
plt.plot(t_harm_avg_2048_les_1_12_05_c4m4, (v_harm_avg_2048_les_1_12_05_c4p4 + v_harm_avg_2048_les_1_12_05_c5p4 + v_harm_avg_2048_les_1_12_05_c6p4 + v_harm_avg_2048_les_1_12_05_c7p4 + v_harm_avg_2048_les_1_12_05_c8p4 + v_harm_avg_2048_les_1_12_05_c4m4 + v_harm_avg_2048_les_1_12_05_c5m4 + v_harm_avg_2048_les_1_12_05_c6m4 + v_harm_avg_2048_les_1_12_05_c7m4 + v_harm_avg_2048_les_1_12_05_c8m4)/10, label=r'$C_{m=4}^{LES}$', color='m')
plt.plot(t_harm_avg_2048_les_1_12_05_c5m5, (v_harm_avg_2048_les_1_12_05_c5p5 + v_harm_avg_2048_les_1_12_05_c6p5 + v_harm_avg_2048_les_1_12_05_c7p5 + v_harm_avg_2048_les_1_12_05_c8p5 + v_harm_avg_2048_les_1_12_05_c5m5 + v_harm_avg_2048_les_1_12_05_c6m5 + v_harm_avg_2048_les_1_12_05_c7m5 + v_harm_avg_2048_les_1_12_05_c8m5)/8, label=r'$C_{m=5}^{LES}$', color='y')

plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2024_LES_noLES.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_harm_avg_2048_les_c00, (v_harm_avg_2048_les_c00 + v_harm_avg_2048_les_c10 + v_harm_avg_2048_les_c20 + v_harm_avg_2048_les_c30 + v_harm_avg_2048_les_c40 + v_harm_avg_2048_les_c50 + v_harm_avg_2048_les_c60 + v_harm_avg_2048_les_c70 + v_harm_avg_2048_les_c80)/9, label=r'$C_{m=0}$', color='b')
plt.plot(t_harm_avg_2048_les_c1m1, (v_harm_avg_2048_les_c1p1 + v_harm_avg_2048_les_c2p1 + v_harm_avg_2048_les_c3p1 + v_harm_avg_2048_les_c4p1 + v_harm_avg_2048_les_c5p1 + v_harm_avg_2048_les_c6p1 + v_harm_avg_2048_les_c7p1 + v_harm_avg_2048_les_c8p1 + v_harm_avg_2048_les_c1m1 + v_harm_avg_2048_les_c2m1 + v_harm_avg_2048_les_c3m1 + v_harm_avg_2048_les_c4m1 + v_harm_avg_2048_les_c5m1 + v_harm_avg_2048_les_c6m1 + v_harm_avg_2048_les_c7m1 + v_harm_avg_2048_les_c8m1)/16, label=r'$C_{m=1}$', color='r')
plt.plot(t_harm_avg_2048_les_c2m2, (v_harm_avg_2048_les_c2p2 + v_harm_avg_2048_les_c3p2 + v_harm_avg_2048_les_c4p2 + v_harm_avg_2048_les_c5p2 + v_harm_avg_2048_les_c6p2 + v_harm_avg_2048_les_c7p2 + v_harm_avg_2048_les_c8p2 + v_harm_avg_2048_les_c2m2 + v_harm_avg_2048_les_c3m2 + v_harm_avg_2048_les_c4m2 + v_harm_avg_2048_les_c5m2 + v_harm_avg_2048_les_c6m2 + v_harm_avg_2048_les_c7m2 + v_harm_avg_2048_les_c8m2)/14, label=r'$C_{m=2}$', color='g')
plt.plot(t_harm_avg_2048_les_c3m3, (v_harm_avg_2048_les_c3p3 + v_harm_avg_2048_les_c4p3 + v_harm_avg_2048_les_c5p3 + v_harm_avg_2048_les_c6p3 + v_harm_avg_2048_les_c7p3 + v_harm_avg_2048_les_c8p3 + v_harm_avg_2048_les_c3m3 + v_harm_avg_2048_les_c4m3 + v_harm_avg_2048_les_c5m3 + v_harm_avg_2048_les_c6m3 + v_harm_avg_2048_les_c7m3 + v_harm_avg_2048_les_c8m3)/12, label=r'$C_{m=3}$', color='c')
plt.plot(t_harm_avg_2048_les_c4m4, (v_harm_avg_2048_les_c4p4 + v_harm_avg_2048_les_c5p4 + v_harm_avg_2048_les_c6p4 + v_harm_avg_2048_les_c7p4 + v_harm_avg_2048_les_c8p4 + v_harm_avg_2048_les_c4m4 + v_harm_avg_2048_les_c5m4 + v_harm_avg_2048_les_c6m4 + v_harm_avg_2048_les_c7m4 + v_harm_avg_2048_les_c8m4)/10, label=r'$C_{m=4}$', color='m')
plt.plot(t_harm_avg_2048_les_c5m5, (v_harm_avg_2048_les_c5p5 + v_harm_avg_2048_les_c6p5 + v_harm_avg_2048_les_c7p5 + v_harm_avg_2048_les_c8p5 + v_harm_avg_2048_les_c5m5 + v_harm_avg_2048_les_c6m5 + v_harm_avg_2048_les_c7m5 + v_harm_avg_2048_les_c8m5)/8, label=r'$C_{m=5}$', color='y')

plt.plot(t_harm_avg_2048_les_2L_c00, (v_harm_avg_2048_les_2L_c00 + v_harm_avg_2048_les_2L_c10 + v_harm_avg_2048_les_2L_c20 + v_harm_avg_2048_les_2L_c30 + v_harm_avg_2048_les_2L_c40 + v_harm_avg_2048_les_2L_c50 + v_harm_avg_2048_les_2L_c60 + v_harm_avg_2048_les_2L_c70 + v_harm_avg_2048_les_2L_c80)/9, label=r'$C_{m=0}^{2L}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c1m1, (v_harm_avg_2048_les_2L_c1p1 + v_harm_avg_2048_les_2L_c2p1 + v_harm_avg_2048_les_2L_c3p1 + v_harm_avg_2048_les_2L_c4p1 + v_harm_avg_2048_les_2L_c5p1 + v_harm_avg_2048_les_2L_c6p1 + v_harm_avg_2048_les_2L_c7p1 + v_harm_avg_2048_les_2L_c8p1 + v_harm_avg_2048_les_2L_c1m1 + v_harm_avg_2048_les_2L_c2m1 + v_harm_avg_2048_les_2L_c3m1 + v_harm_avg_2048_les_2L_c4m1 + v_harm_avg_2048_les_2L_c5m1 + v_harm_avg_2048_les_2L_c6m1 + v_harm_avg_2048_les_2L_c7m1 + v_harm_avg_2048_les_2L_c8m1)/16, label=r'$C_{m=1}^{2L}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c2m2, (v_harm_avg_2048_les_2L_c2p2 + v_harm_avg_2048_les_2L_c3p2 + v_harm_avg_2048_les_2L_c4p2 + v_harm_avg_2048_les_2L_c5p2 + v_harm_avg_2048_les_2L_c6p2 + v_harm_avg_2048_les_2L_c7p2 + v_harm_avg_2048_les_2L_c8p2 + v_harm_avg_2048_les_2L_c2m2 + v_harm_avg_2048_les_2L_c3m2 + v_harm_avg_2048_les_2L_c4m2 + v_harm_avg_2048_les_2L_c5m2 + v_harm_avg_2048_les_2L_c6m2 + v_harm_avg_2048_les_2L_c7m2 + v_harm_avg_2048_les_2L_c8m2)/14, label=r'$C_{m=2}^{2L}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c3m3, (v_harm_avg_2048_les_2L_c3p3 + v_harm_avg_2048_les_2L_c4p3 + v_harm_avg_2048_les_2L_c5p3 + v_harm_avg_2048_les_2L_c6p3 + v_harm_avg_2048_les_2L_c7p3 + v_harm_avg_2048_les_2L_c8p3 + v_harm_avg_2048_les_2L_c3m3 + v_harm_avg_2048_les_2L_c4m3 + v_harm_avg_2048_les_2L_c5m3 + v_harm_avg_2048_les_2L_c6m3 + v_harm_avg_2048_les_2L_c7m3 + v_harm_avg_2048_les_2L_c8m3)/12, label=r'$C_{m=3}^{2L}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c4m4, (v_harm_avg_2048_les_2L_c4p4 + v_harm_avg_2048_les_2L_c5p4 + v_harm_avg_2048_les_2L_c6p4 + v_harm_avg_2048_les_2L_c7p4 + v_harm_avg_2048_les_2L_c8p4 + v_harm_avg_2048_les_2L_c4m4 + v_harm_avg_2048_les_2L_c5m4 + v_harm_avg_2048_les_2L_c6m4 + v_harm_avg_2048_les_2L_c7m4 + v_harm_avg_2048_les_2L_c8m4)/10, label=r'$C_{m=4}^{2L}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c5m5, (v_harm_avg_2048_les_2L_c5p5 + v_harm_avg_2048_les_2L_c6p5 + v_harm_avg_2048_les_2L_c7p5 + v_harm_avg_2048_les_2L_c8p5 + v_harm_avg_2048_les_2L_c5m5 + v_harm_avg_2048_les_2L_c6m5 + v_harm_avg_2048_les_2L_c7m5 + v_harm_avg_2048_les_2L_c8m5)/8, label=r'$C_{m=5}^{2L}$', color='y', linestyle=':')

plt.plot(t_harm_avg_2048_les_2dr_c00, (v_harm_avg_2048_les_2dr_c00 + v_harm_avg_2048_les_2dr_c10 + v_harm_avg_2048_les_2dr_c20 + v_harm_avg_2048_les_2dr_c30 + v_harm_avg_2048_les_2dr_c40 + v_harm_avg_2048_les_2dr_c50 + v_harm_avg_2048_les_2dr_c60 + v_harm_avg_2048_les_2dr_c70 + v_harm_avg_2048_les_2dr_c80)/9, label=r'$C_{m=0}^{2 \Delta r}$', color='b', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c1m1, (v_harm_avg_2048_les_2dr_c1p1 + v_harm_avg_2048_les_2dr_c2p1 + v_harm_avg_2048_les_2dr_c3p1 + v_harm_avg_2048_les_2dr_c4p1 + v_harm_avg_2048_les_2dr_c5p1 + v_harm_avg_2048_les_2dr_c6p1 + v_harm_avg_2048_les_2dr_c7p1 + v_harm_avg_2048_les_2dr_c8p1 + v_harm_avg_2048_les_2dr_c1m1 + v_harm_avg_2048_les_2dr_c2m1 + v_harm_avg_2048_les_2dr_c3m1 + v_harm_avg_2048_les_2dr_c4m1 + v_harm_avg_2048_les_2dr_c5m1 + v_harm_avg_2048_les_2dr_c6m1 + v_harm_avg_2048_les_2dr_c7m1 + v_harm_avg_2048_les_2dr_c8m1)/16, label=r'$C_{m=1}^{2 \Delta r}$', color='r', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c2m2, (v_harm_avg_2048_les_2dr_c2p2 + v_harm_avg_2048_les_2dr_c3p2 + v_harm_avg_2048_les_2dr_c4p2 + v_harm_avg_2048_les_2dr_c5p2 + v_harm_avg_2048_les_2dr_c6p2 + v_harm_avg_2048_les_2dr_c7p2 + v_harm_avg_2048_les_2dr_c8p2 + v_harm_avg_2048_les_2dr_c2m2 + v_harm_avg_2048_les_2dr_c3m2 + v_harm_avg_2048_les_2dr_c4m2 + v_harm_avg_2048_les_2dr_c5m2 + v_harm_avg_2048_les_2dr_c6m2 + v_harm_avg_2048_les_2dr_c7m2 + v_harm_avg_2048_les_2dr_c8m2)/14, label=r'$C_{m=2}^{2 \Delta r}$', color='g', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c3m3, (v_harm_avg_2048_les_2dr_c3p3 + v_harm_avg_2048_les_2dr_c4p3 + v_harm_avg_2048_les_2dr_c5p3 + v_harm_avg_2048_les_2dr_c6p3 + v_harm_avg_2048_les_2dr_c7p3 + v_harm_avg_2048_les_2dr_c8p3 + v_harm_avg_2048_les_2dr_c3m3 + v_harm_avg_2048_les_2dr_c4m3 + v_harm_avg_2048_les_2dr_c5m3 + v_harm_avg_2048_les_2dr_c6m3 + v_harm_avg_2048_les_2dr_c7m3 + v_harm_avg_2048_les_2dr_c8m3)/12, label=r'$C_{m=3}^{2 \Delta r}$', color='c', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c4m4, (v_harm_avg_2048_les_2dr_c4p4 + v_harm_avg_2048_les_2dr_c5p4 + v_harm_avg_2048_les_2dr_c6p4 + v_harm_avg_2048_les_2dr_c7p4 + v_harm_avg_2048_les_2dr_c8p4 + v_harm_avg_2048_les_2dr_c4m4 + v_harm_avg_2048_les_2dr_c5m4 + v_harm_avg_2048_les_2dr_c6m4 + v_harm_avg_2048_les_2dr_c7m4 + v_harm_avg_2048_les_2dr_c8m4)/10, label=r'$C_{m=4}^{2 \Delta r}$', color='m')
plt.plot(t_harm_avg_2048_les_2dr_c5m5, (v_harm_avg_2048_les_2dr_c5p5 + v_harm_avg_2048_les_2dr_c6p5 + v_harm_avg_2048_les_2dr_c7p5 + v_harm_avg_2048_les_2dr_c8p5 + v_harm_avg_2048_les_2dr_c5m5 + v_harm_avg_2048_les_2dr_c6m5 + v_harm_avg_2048_les_2dr_c7m5 + v_harm_avg_2048_les_2dr_c8m5)/8, label=r'$C_{m=5}^{2 \Delta r}$', color='y', linestyle='-.')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_all.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_harm_avg_2048_les_c00, (v_harm_avg_2048_les_c00 + v_harm_avg_2048_les_c10 + v_harm_avg_2048_les_c20 + v_harm_avg_2048_les_c30 + v_harm_avg_2048_les_c40 + v_harm_avg_2048_les_c50 + v_harm_avg_2048_les_c60 + v_harm_avg_2048_les_c70 + v_harm_avg_2048_les_c80)/9, label=r'$C_{m=0}$', color='b')
plt.plot(t_harm_avg_2048_les_c1m1, (v_harm_avg_2048_les_c1p1 + v_harm_avg_2048_les_c2p1 + v_harm_avg_2048_les_c3p1 + v_harm_avg_2048_les_c4p1 + v_harm_avg_2048_les_c5p1 + v_harm_avg_2048_les_c6p1 + v_harm_avg_2048_les_c7p1 + v_harm_avg_2048_les_c8p1 + v_harm_avg_2048_les_c1m1 + v_harm_avg_2048_les_c2m1 + v_harm_avg_2048_les_c3m1 + v_harm_avg_2048_les_c4m1 + v_harm_avg_2048_les_c5m1 + v_harm_avg_2048_les_c6m1 + v_harm_avg_2048_les_c7m1 + v_harm_avg_2048_les_c8m1)/16, label=r'$C_{m=1}$', color='r')
plt.plot(t_harm_avg_2048_les_c2m2, (v_harm_avg_2048_les_c2p2 + v_harm_avg_2048_les_c3p2 + v_harm_avg_2048_les_c4p2 + v_harm_avg_2048_les_c5p2 + v_harm_avg_2048_les_c6p2 + v_harm_avg_2048_les_c7p2 + v_harm_avg_2048_les_c8p2 + v_harm_avg_2048_les_c2m2 + v_harm_avg_2048_les_c3m2 + v_harm_avg_2048_les_c4m2 + v_harm_avg_2048_les_c5m2 + v_harm_avg_2048_les_c6m2 + v_harm_avg_2048_les_c7m2 + v_harm_avg_2048_les_c8m2)/14, label=r'$C_{m=2}$', color='g')
plt.plot(t_harm_avg_2048_les_c3m3, (v_harm_avg_2048_les_c3p3 + v_harm_avg_2048_les_c4p3 + v_harm_avg_2048_les_c5p3 + v_harm_avg_2048_les_c6p3 + v_harm_avg_2048_les_c7p3 + v_harm_avg_2048_les_c8p3 + v_harm_avg_2048_les_c3m3 + v_harm_avg_2048_les_c4m3 + v_harm_avg_2048_les_c5m3 + v_harm_avg_2048_les_c6m3 + v_harm_avg_2048_les_c7m3 + v_harm_avg_2048_les_c8m3)/12, label=r'$C_{m=3}$', color='c')
plt.plot(t_harm_avg_2048_les_c4m4, (v_harm_avg_2048_les_c4p4 + v_harm_avg_2048_les_c5p4 + v_harm_avg_2048_les_c6p4 + v_harm_avg_2048_les_c7p4 + v_harm_avg_2048_les_c8p4 + v_harm_avg_2048_les_c4m4 + v_harm_avg_2048_les_c5m4 + v_harm_avg_2048_les_c6m4 + v_harm_avg_2048_les_c7m4 + v_harm_avg_2048_les_c8m4)/10, label=r'$C_{m=4}$', color='m')
plt.plot(t_harm_avg_2048_les_c5m5, (v_harm_avg_2048_les_c5p5 + v_harm_avg_2048_les_c6p5 + v_harm_avg_2048_les_c7p5 + v_harm_avg_2048_les_c8p5 + v_harm_avg_2048_les_c5m5 + v_harm_avg_2048_les_c6m5 + v_harm_avg_2048_les_c7m5 + v_harm_avg_2048_les_c8m5)/8, label=r'$C_{m=5}$', color='y')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_harm_avg_2048_les_2L_c00, (v_harm_avg_2048_les_2L_c00 + v_harm_avg_2048_les_2L_c10 + v_harm_avg_2048_les_2L_c20 + v_harm_avg_2048_les_2L_c30 + v_harm_avg_2048_les_2L_c40 + v_harm_avg_2048_les_2L_c50 + v_harm_avg_2048_les_2L_c60 + v_harm_avg_2048_les_2L_c70 + v_harm_avg_2048_les_2L_c80)/9, label=r'$C_{m=0}^{2L}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c1m1, (v_harm_avg_2048_les_2L_c1p1 + v_harm_avg_2048_les_2L_c2p1 + v_harm_avg_2048_les_2L_c3p1 + v_harm_avg_2048_les_2L_c4p1 + v_harm_avg_2048_les_2L_c5p1 + v_harm_avg_2048_les_2L_c6p1 + v_harm_avg_2048_les_2L_c7p1 + v_harm_avg_2048_les_2L_c8p1 + v_harm_avg_2048_les_2L_c1m1 + v_harm_avg_2048_les_2L_c2m1 + v_harm_avg_2048_les_2L_c3m1 + v_harm_avg_2048_les_2L_c4m1 + v_harm_avg_2048_les_2L_c5m1 + v_harm_avg_2048_les_2L_c6m1 + v_harm_avg_2048_les_2L_c7m1 + v_harm_avg_2048_les_2L_c8m1)/16, label=r'$C_{m=1}^{2L}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c2m2, (v_harm_avg_2048_les_2L_c2p2 + v_harm_avg_2048_les_2L_c3p2 + v_harm_avg_2048_les_2L_c4p2 + v_harm_avg_2048_les_2L_c5p2 + v_harm_avg_2048_les_2L_c6p2 + v_harm_avg_2048_les_2L_c7p2 + v_harm_avg_2048_les_2L_c8p2 + v_harm_avg_2048_les_2L_c2m2 + v_harm_avg_2048_les_2L_c3m2 + v_harm_avg_2048_les_2L_c4m2 + v_harm_avg_2048_les_2L_c5m2 + v_harm_avg_2048_les_2L_c6m2 + v_harm_avg_2048_les_2L_c7m2 + v_harm_avg_2048_les_2L_c8m2)/14, label=r'$C_{m=2}^{2L}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c3m3, (v_harm_avg_2048_les_2L_c3p3 + v_harm_avg_2048_les_2L_c4p3 + v_harm_avg_2048_les_2L_c5p3 + v_harm_avg_2048_les_2L_c6p3 + v_harm_avg_2048_les_2L_c7p3 + v_harm_avg_2048_les_2L_c8p3 + v_harm_avg_2048_les_2L_c3m3 + v_harm_avg_2048_les_2L_c4m3 + v_harm_avg_2048_les_2L_c5m3 + v_harm_avg_2048_les_2L_c6m3 + v_harm_avg_2048_les_2L_c7m3 + v_harm_avg_2048_les_2L_c8m3)/12, label=r'$C_{m=3}^{2L}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c4m4, (v_harm_avg_2048_les_2L_c4p4 + v_harm_avg_2048_les_2L_c5p4 + v_harm_avg_2048_les_2L_c6p4 + v_harm_avg_2048_les_2L_c7p4 + v_harm_avg_2048_les_2L_c8p4 + v_harm_avg_2048_les_2L_c4m4 + v_harm_avg_2048_les_2L_c5m4 + v_harm_avg_2048_les_2L_c6m4 + v_harm_avg_2048_les_2L_c7m4 + v_harm_avg_2048_les_2L_c8m4)/10, label=r'$C_{m=4}^{2L}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c5m5, (v_harm_avg_2048_les_2L_c5p5 + v_harm_avg_2048_les_2L_c6p5 + v_harm_avg_2048_les_2L_c7p5 + v_harm_avg_2048_les_2L_c8p5 + v_harm_avg_2048_les_2L_c5m5 + v_harm_avg_2048_les_2L_c6m5 + v_harm_avg_2048_les_2L_c7m5 + v_harm_avg_2048_les_2L_c8m5)/8, label=r'$C_{m=5}^{2L}$', color='y', linestyle=':')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2L.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_harm_avg_2048_les_2dr_c00, (v_harm_avg_2048_les_2dr_c00 + v_harm_avg_2048_les_2dr_c10 + v_harm_avg_2048_les_2dr_c20 + v_harm_avg_2048_les_2dr_c30 + v_harm_avg_2048_les_2dr_c40 + v_harm_avg_2048_les_2dr_c50 + v_harm_avg_2048_les_2dr_c60 + v_harm_avg_2048_les_2dr_c70 + v_harm_avg_2048_les_2dr_c80)/9, label=r'$C_{m=0}^{2 \Delta r}$', color='b', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c1m1, (v_harm_avg_2048_les_2dr_c1p1 + v_harm_avg_2048_les_2dr_c2p1 + v_harm_avg_2048_les_2dr_c3p1 + v_harm_avg_2048_les_2dr_c4p1 + v_harm_avg_2048_les_2dr_c5p1 + v_harm_avg_2048_les_2dr_c6p1 + v_harm_avg_2048_les_2dr_c7p1 + v_harm_avg_2048_les_2dr_c8p1 + v_harm_avg_2048_les_2dr_c1m1 + v_harm_avg_2048_les_2dr_c2m1 + v_harm_avg_2048_les_2dr_c3m1 + v_harm_avg_2048_les_2dr_c4m1 + v_harm_avg_2048_les_2dr_c5m1 + v_harm_avg_2048_les_2dr_c6m1 + v_harm_avg_2048_les_2dr_c7m1 + v_harm_avg_2048_les_2dr_c8m1)/16, label=r'$C_{m=1}^{2 \Delta r}$', color='r', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c2m2, (v_harm_avg_2048_les_2dr_c2p2 + v_harm_avg_2048_les_2dr_c3p2 + v_harm_avg_2048_les_2dr_c4p2 + v_harm_avg_2048_les_2dr_c5p2 + v_harm_avg_2048_les_2dr_c6p2 + v_harm_avg_2048_les_2dr_c7p2 + v_harm_avg_2048_les_2dr_c8p2 + v_harm_avg_2048_les_2dr_c2m2 + v_harm_avg_2048_les_2dr_c3m2 + v_harm_avg_2048_les_2dr_c4m2 + v_harm_avg_2048_les_2dr_c5m2 + v_harm_avg_2048_les_2dr_c6m2 + v_harm_avg_2048_les_2dr_c7m2 + v_harm_avg_2048_les_2dr_c8m2)/14, label=r'$C_{m=2}^{2 \Delta r}$', color='g', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c3m3, (v_harm_avg_2048_les_2dr_c3p3 + v_harm_avg_2048_les_2dr_c4p3 + v_harm_avg_2048_les_2dr_c5p3 + v_harm_avg_2048_les_2dr_c6p3 + v_harm_avg_2048_les_2dr_c7p3 + v_harm_avg_2048_les_2dr_c8p3 + v_harm_avg_2048_les_2dr_c3m3 + v_harm_avg_2048_les_2dr_c4m3 + v_harm_avg_2048_les_2dr_c5m3 + v_harm_avg_2048_les_2dr_c6m3 + v_harm_avg_2048_les_2dr_c7m3 + v_harm_avg_2048_les_2dr_c8m3)/12, label=r'$C_{m=3}^{2 \Delta r}$', color='c', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c4m4, (v_harm_avg_2048_les_2dr_c4p4 + v_harm_avg_2048_les_2dr_c5p4 + v_harm_avg_2048_les_2dr_c6p4 + v_harm_avg_2048_les_2dr_c7p4 + v_harm_avg_2048_les_2dr_c8p4 + v_harm_avg_2048_les_2dr_c4m4 + v_harm_avg_2048_les_2dr_c5m4 + v_harm_avg_2048_les_2dr_c6m4 + v_harm_avg_2048_les_2dr_c7m4 + v_harm_avg_2048_les_2dr_c8m4)/10, label=r'$C_{m=4}^{2 \Delta r}$', color='m', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c5m5, (v_harm_avg_2048_les_2dr_c5p5 + v_harm_avg_2048_les_2dr_c6p5 + v_harm_avg_2048_les_2dr_c7p5 + v_harm_avg_2048_les_2dr_c8p5 + v_harm_avg_2048_les_2dr_c5m5 + v_harm_avg_2048_les_2dr_c6m5 + v_harm_avg_2048_les_2dr_c7m5 + v_harm_avg_2048_les_2dr_c8m5)/8, label=r'$C_{m=5}^{2 \Delta r}$', color='y', linestyle='-.')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2dr.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_int2d_2048_les_m1e13_int, BRMS_int2d_2048_les_m1e13_int, label=r'$<B>_{13}^{2D}$', color='b', linestyle=':')
plt.plot(t_int2d_2048_les_m1e13_int, BRMSP_int2d_2048_les_m1e13_int, label=r'$<B_{pol}>_{13}^{2D}$', color='g', linestyle=':')
plt.plot(t_int2d_2048_les_m1e13_int, BRMST_int2d_2048_les_m1e13_int, label=r'$<B_{tor}>_{13}^{2D}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}^{3D}$', color='b')
plt.plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}^{3D}$', color='g')
plt.plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}^{3D}$', color='r')
plt.yscale("log")
plt.legend()
plt.ylim([1e11, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("2Dvs3D_2048_LES.pdf",  bbox_inches="tight")
pl.close()




plt.figure()
plt.plot(t_int3d_2048_m1e13_avg, B_int3d_2048_m1e13_avg, label=r'$<B>_{13}$', color='b', linestyle=':')
plt.plot(t_int3d_2048_m1e13_avg, Bpol_int3d_2048_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g', linestyle=':')
plt.plot(t_int3d_2048_m1e13_avg, Btor_int3d_2048_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}^{LES}$', color='b')
plt.plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}^{LES}$', color='g')
plt.plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.ylim([1e11, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_2048_m1e13.pdf",  bbox_inches="tight")
pl.close()

plt.figure()
plt.plot(t_int3d_2048_m5e10_avg, B_int3d_2048_m5e10_avg, label=r'$<B>_{10}$', color='b', linestyle=':')
plt.plot(t_int3d_2048_m5e10_avg, Bpol_int3d_2048_m5e10_avg, label=r'$<B_{pol}>_{10}$', color='g', linestyle=':')
plt.plot(t_int3d_2048_m5e10_avg, Btor_int3d_2048_m5e10_avg, label=r'$<B_{tor}>_{10}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m5e10_avg, B_int3d_2048_les_m5e10_avg, label=r'$<B>_{10}^{LES}$', color='b')
plt.plot(t_int3d_2048_les_m5e10_avg, Bpol_int3d_2048_les_m5e10_avg, label=r'$<B_{pol}>_{10}^{LES}$', color='g')
plt.plot(t_int3d_2048_les_m5e10_avg, Btor_int3d_2048_les_m5e10_avg, label=r'$<B_{tor}>_{10}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.ylim([1e11, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_2048_m5e10.pdf",  bbox_inches="tight")
pl.close()
'''


f, ax = pl.subplots()
ax.plot(t_int3d_2048_les_m1e13_avg, invbeta_int3d_2048_les_m1e13_avg, label=r'$\textrm{MR LES}_{13}$', color='b', linestyle='--')
ax.plot(t_int3d_2048_les_m1e13_m5e10_avg, invbeta_int3d_2048_les_m1e13_m5e10_avg, label=r'$\textrm{MR LES}_{10}^{13}$', color='r', linestyle='--')
ax.plot(t_int3d_2048_les_full_m1e13_avg + 250, invbeta_int3d_2048_les_full_m1e13_avg, label=r'$\textrm{MR LES FULL}_{13}$', color='b')
ax.plot(t_int3d_2048_les_full_m1e13_m5e10_avg + 250, invbeta_int3d_2048_les_full_m1e13_m5e10_avg, label=r'$\textrm{MR LES FULL}_{10}^{13}$', color='r')
ax.set_yscale("log")
ax.legend(loc='lower right', fontsize=16)
ax.tick_params(labelsize=16)
ax.set_xlim([3100, 13000])
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format((x-3200)*0.00494))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_xticks([3200, 10.0/0.00494+3200, 20.0/0.00494+3200, 30.0/0.00494+3200, 40.0/0.00494+3200, 50.0/0.00494+3200])
ax.set_xlabel(r'$t - t_{merger}[ms]$',fontsize=20)
ax.set_ylabel(r'$<\beta^{-1}>$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("full_3D_invbeta_2048_4096_LES.pdf",  bbox_inches="tight")
pl.close()

'''
# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_4250, Btor_cyl_2048_les_m5e10_avg_4250, label=r'$5 ms$', color='b')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, Btor_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_7250, Btor_cyl_2048_les_m5e10_avg_7250, label=r'$20 ms$', color='g')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, Btor_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='y')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_11250, Btor_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='m')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_13000, Btor_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 16])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{10}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_4250, Bpol_cyl_2048_les_m5e10_avg_4250, label=r'$5 ms$', color='b')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, Bpol_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, Bpol_cyl_2048_les_m5e10_avg_7250, label=r'$20 ms$', color='g')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_9250, Bpol_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='y')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_11250, Bpol_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='m')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_13000, Bpol_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 16])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Btor_Bpol_mask5e10_2048_LES.pdf",  bbox_inches="tight")
pl.close()
'''
f, ax = pl.subplots()
ax.plot(r_cyl_2048_les_full_m1e13_avg_4250, invbeta_cyl_2048_les_full_m1e13_avg_4250, label=r'$5 ms$', color='b')
ax.plot(r_cyl_2048_les_full_m1e13_avg_5250, invbeta_cyl_2048_les_full_m1e13_avg_5250, label=r'$10 ms$', color='r')
ax.plot(r_cyl_2048_les_full_m1e13_avg_7250, invbeta_cyl_2048_les_full_m1e13_avg_7250, label=r'$20 ms$', color='g')
ax.plot(r_cyl_2048_les_full_m1e13_avg_9250, invbeta_cyl_2048_les_full_m1e13_avg_9250, label=r'$30 ms$', color='y')
ax.plot(r_cyl_2048_les_full_m1e13_avg_11250, invbeta_cyl_2048_les_full_m1e13_avg_11250, label=r'$40 ms$', color='m')
ax.plot(r_cyl_2048_les_full_m1e13_avg_13000, invbeta_cyl_2048_les_full_m1e13_avg_13000, label=r'$50 ms$', color='c')
ax.legend(loc='upper left',fontsize=16)
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_xlabel(r'$R[km]$',fontsize=20)
ax.set_ylabel(r'$<\beta^{-1}>_{13}$',fontsize=20)
ax.set_xlim([0.5, 16/1.5])
ax.tick_params(labelsize=14)
ax.set_xticks([5.0/1.5, 10.0/1.5, 15.0/1.5])
pl.tight_layout(pad=0.1)
pl.savefig("full_Cylinder_invbeta_2048_LES_m1e13.pdf",  bbox_inches="tight")
pl.close()

'''
plt.figure()
plt.plot(r_cyl_2048_les_m5e10_avg_4250, invbeta_cyl_2048_les_m5e10_avg_4250, label=r'$4250$', color='b')
plt.plot(r_cyl_2048_les_m5e10_avg_5250, invbeta_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
plt.plot(r_cyl_2048_les_m5e10_avg_7250, invbeta_cyl_2048_les_m5e10_avg_7250, label=r'$20 ms$', color='g')
plt.plot(r_cyl_2048_les_m5e10_avg_9250, invbeta_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='y')
plt.plot(r_cyl_2048_les_m5e10_avg_11250, invbeta_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='m')
plt.plot(r_cyl_2048_les_m5e10_avg_13000, invbeta_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
plt.legend(loc='upper right')
plt.xlabel(r'$r$',fontsize=20)
plt.ylabel(r'$<\frac{1}{\beta}>_{10}$',fontsize=20)
pl.savefig("Cylinder_invbeta_2048_LES.pdf",  bbox_inches="tight")
pl.close()
'''
f, ax = pl.subplots()
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_4250, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_4250, label=r'$5 ms$', color='b')
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_5250, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_5250, label=r'$10 ms$', color='r')
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_7250, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_7250, label=r'$20 ms$', color='g')
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_9250, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_9250, label=r'$30 ms$', color='y')
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_11250, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_11250, label=r'$40 ms$', color='m')
ax.plot(r_cyl_2048_les_full_m1e13_m5e10_avg_13000, invbeta_cyl_2048_les_full_m1e13_m5e10_avg_13000, label=r'$50 ms$', color='c')
ticks_x = ticker.FuncFormatter(lambda x, pos: '{0:g}'.format(x*1.5))
ax.xaxis.set_major_formatter(ticks_x)
ax.set_xlabel(r'$R[km]$',fontsize=20)
ax.set_ylabel(r'$<\beta^{-1}>_{10}^{13}$',fontsize=20)
ax.set_xlim([0.5, 31/1.5])
ax.tick_params(labelsize=14)
ax.tick_params(labelsize=14)
ax.set_xticks([10.0/1.5, 20.0/1.5, 30.0/1.5])
pl.savefig("full_Cylinder_invbeta_2048_LES_m1e13_m5e10.pdf",  bbox_inches="tight")
pl.close()
'''
plt.figure()
plt.plot(r_cyl_2048_m1e13_avg_5250, invbeta_cyl_2048_m1e13_avg_5250, label=r'$13$', color='b', linestyle=':')
plt.plot(r_cyl_2048_m5e10_avg_5250, invbeta_cyl_2048_m5e10_avg_5250, label=r'$10$', color='r', linestyle=':')
plt.plot(r_cyl_2048_les_m1e13_avg_5250, invbeta_cyl_2048_les_m1e13_avg_5250, label=r'$13 LES$', color='b')
plt.plot(r_cyl_2048_les_m5e10_avg_5250, invbeta_cyl_2048_les_m5e10_avg_5250, label=r'$10 LES$', color='r')
plt.legend(loc='lower right')
plt.xlabel(r'$r$',fontsize=20)
plt.ylabel(r'$<\frac{1}{\beta}>$',fontsize=20)
pl.savefig("Cylinder_invbeta_2048_LES_5250.pdf",  bbox_inches="tight")
pl.close()



# Two subplots sharing x axes
f, axarr = pl.subplots(4, sharex=True)
axarr[0].plot(t_int3d_2048_les_m5e10_int, Erot_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[0].plot(t_int3d_2048_les_m1em14_int, Erot_int3d_2048_les_m1em14_int, label=r'$2048_{-14} LES$', color='b')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':10},ncol=2, shadow=True)
axarr[0].tick_params(axis='y', labelsize=14)
axarr[0].set_xlim([3000, 9250])
axarr[0].set_ylabel(r'$E_{rot}$',fontsize=20)
axarr[1].plot(t_int3d_2048_les_m5e10_int, Eint_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[1].plot(t_int3d_2048_les_m1em14_int, Eint_int3d_2048_les_m1em14_int, label=r'$2048_{-14} LES$', color='b')
axarr[1].tick_params(axis='y', labelsize=14)
axarr[1].set_xlim([3000, 9250])
axarr[1].set_ylabel(r'$E_{int}$',fontsize=20)
axarr[2].plot(t_int3d_2048_les_m5e10_int, Emag_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[2].plot(t_int3d_2048_les_m1em14_int, Emag_int3d_2048_les_m1em14_int, label=r'$2048_{-14} LES$', color='b')
axarr[2].tick_params(axis='y', labelsize=14)
axarr[2].set_xlim([3000, 9250])
axarr[2].set_ylabel(r'$E_{mag}$',fontsize=20)
axarr[3].plot(t_int3d_2048_les_m5e10_int, D_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[3].plot(t_int3d_2048_les_m1em14_int, D_int3d_2048_les_m1em14_int, label=r'$2048_{-14} LES$', color='b')
axarr[3].tick_params(axis='x', labelsize=14)
axarr[3].tick_params(axis='y', labelsize=14)
axarr[3].set_xlim([3000, 9250])
axarr[3].set_xlabel(r'$t$',fontsize=20)
axarr[3].set_ylabel(r'$E_{bar}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Energies_INT3D_masks_2048_LES.pdf",  bbox_inches="tight")
pl.close()



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int2d_1024_m1e13_int, BRMST_int2d_1024_m1e13_int, label=r'$1024$', color='g', linestyle=':')
axarr[0].plot(t_int2d_1024_les_m1e13_int, BRMST_int2d_1024_les_m1e13_int, label=r'$1024 LES$', color='g')
axarr[0].plot(t_int2d_2048_m1e13_int, BRMST_int2d_2048_m1e13_int, label=r'$2048$', color='b', linestyle=':')
axarr[0].plot(t_int2d_2048_les_m1e13_int, BRMST_int2d_2048_les_m1e13_int, label=r'$2048 LES$', color='b')
#axarr[0].plot(t_int2d_2048_les_t10_m1e13_int, BRMST_int2d_2048_les_t10_m1e13_int, label=r'$2048 LES_{t10}$', color='g')
axarr[0].plot(t_int2d_4096_m1e13_int, BRMST_int2d_4096_m1e13_int, label=r'$4096$', color='r', linestyle=':')
axarr[0].plot(t_int2d_4096_les_m1e13_int, BRMST_int2d_4096_les_m1e13_int, label=r'$4096 LES$', color='r')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlabel(r'$t$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{13}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[1].plot(t_int2d_1024_m1e13_int, BRMSP_int2d_1024_m1e13_int, label=r'$1024$', color='g', linestyle=':')
axarr[1].plot(t_int2d_1024_les_m1e13_int, BRMSP_int2d_1024_les_m1e13_int, label=r'$1024 LES$', color='g')
axarr[1].plot(t_int2d_2048_m1e13_int, BRMSP_int2d_2048_m1e13_int, label=r'$2048$', color='b', linestyle=':')
axarr[1].plot(t_int2d_2048_les_m1e13_int, BRMSP_int2d_2048_les_m1e13_int, label=r'$2048 LES$', color='b')
#axarr[1].plot(t_int2d_2048_les_t10_m1e13_int, BRMSP_int2d_2048_les_t10_m1e13_int, label=r'$2048 LES_{t10}$', color='g')
axarr[1].plot(t_int2d_4096_m1e13_int, BRMSP_int2d_4096_m1e13_int, label=r'$4096$', color='r', linestyle=':')
axarr[1].plot(t_int2d_4096_les_m1e13_int, BRMSP_int2d_4096_les_m1e13_int, label=r'$4096 LES$', color='r')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlabel(r'$t$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{13}$',fontsize=20)
axarr[1].set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("INT2D_Btor_Bpol_mask1e13.pdf",  bbox_inches="tight")
pl.close()

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int2d_1024_m5e10_int, BRMST_int2d_1024_m5e10_int, label=r'$1024$', color='g', linestyle=':')
axarr[0].plot(t_int2d_1024_les_m5e10_int, BRMST_int2d_1024_les_m5e10_int, label=r'$1024 LES$', color='g')
axarr[0].plot(t_int2d_2048_m5e10_int, BRMST_int2d_2048_m5e10_int, label=r'$2048$', color='b', linestyle=':')
axarr[0].plot(t_int2d_2048_les_m5e10_int, BRMST_int2d_2048_les_m5e10_int, label=r'$2048 LES$', color='b')
#axarr[0].plot(t_int2d_2048_les_t10_m5e10_int, BRMST_int2d_2048_les_t10_m5e10_int, label=r'$2048 LES_{t10}$', color='g')
axarr[0].plot(t_int2d_4096_m5e10_int, BRMST_int2d_4096_m5e10_int, label=r'$4096$', color='r', linestyle=':')
axarr[0].plot(t_int2d_4096_les_m5e10_int, BRMST_int2d_4096_les_m5e10_int, label=r'$4096 LES$', color='r')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlabel(r'$t$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{10}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[1].plot(t_int2d_1024_m5e10_int, BRMSP_int2d_1024_m5e10_int, label=r'$1024$', color='g', linestyle=':')
axarr[1].plot(t_int2d_1024_les_m5e10_int, BRMSP_int2d_1024_les_m5e10_int, label=r'$1024 LES$', color='g')
axarr[1].plot(t_int2d_2048_m5e10_int, BRMSP_int2d_2048_m5e10_int, label=r'$2048$', color='b', linestyle=':')
axarr[1].plot(t_int2d_2048_les_m5e10_int, BRMSP_int2d_2048_les_m5e10_int, label=r'$2048 LES$', color='b')
#axarr[1].plot(t_int2d_2048_les_t10_m5e10_int, BRMSP_int2d_2048_les_t10_m5e10_int, label=r'$2048 LES_{t10}$', color='g')
axarr[1].plot(t_int2d_4096_m5e10_int, BRMSP_int2d_4096_m5e10_int, label=r'$4096$', color='r', linestyle=':')
axarr[1].plot(t_int2d_4096_les_m5e10_int, BRMSP_int2d_4096_les_m5e10_int, label=r'$4096 LES$', color='r')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlabel(r'$t$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{10}$',fontsize=20)
axarr[1].set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("INT2D_Btor_Bpol_mask5e10.pdf",  bbox_inches="tight")
pl.close()


# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, rho_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, rho_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='g')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_11250, rho_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='b')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_13000, rho_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_5250, rho_cyl_flat_2048_les_m5e10_avg_5250*r_cyl_flat_2048_les_m5e10_avg_5250*r_cyl_flat_2048_les_m5e10_avg_5250, color='r', linestyle=':')
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_9250, rho_cyl_flat_2048_les_m5e10_avg_9250*r_cyl_flat_2048_les_m5e10_avg_9250*r_cyl_flat_2048_les_m5e10_avg_9250, color='g', linestyle=':')
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_11250, rho_cyl_flat_2048_les_m5e10_avg_11250*r_cyl_flat_2048_les_m5e10_avg_11250*r_cyl_flat_2048_les_m5e10_avg_11250, color='b', linestyle=':')
axarr[0].plot(r_cyl_flat_2048_les_m5e10_avg_13000, rho_cyl_flat_2048_les_m5e10_avg_13000*r_cyl_flat_2048_les_m5e10_avg_13000*r_cyl_flat_2048_les_m5e10_avg_13000, color='c', linestyle=':')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 25])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<r^2 \rho>_{10}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, omega_cyl_2048_les_m5e10_avg_5250, label=r'$Cylinder$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, omega_cyl_2048_les_m5e10_avg_9250, color='g')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_11250, omega_cyl_2048_les_m5e10_avg_11250, color='b')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_13000, omega_cyl_2048_les_m5e10_avg_13000, color='c')
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_5250, omega_cyl_flat_2048_les_m5e10_avg_5250, label=r'$z-plane$', color='r', linestyle=':')
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_7250, omega_cyl_flat_2048_les_m5e10_avg_9250, color='g', linestyle=':')
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_11250, omega_cyl_flat_2048_les_m5e10_avg_11250, color='b', linestyle=':')
axarr[1].plot(r_cyl_flat_2048_les_m5e10_avg_13000, omega_cyl_flat_2048_les_m5e10_avg_13000, color='c', linestyle=':')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 25])
axarr[1].set_ylim([0.005, 0.025])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<\Omega>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_vs_Plane_Rho_Omega_mask5e10_2048_LES.pdf",  bbox_inches="tight")
pl.close()

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, rho_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, rho_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='g')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_11250, rho_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='b')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_13000, rho_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 25])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<r^2 \rho>_{10}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, omega_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, omega_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='g')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_11250, omega_cyl_2048_les_m5e10_avg_11250, label=r'$40 ms$', color='b')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_13000, omega_cyl_2048_les_m5e10_avg_13000, label=r'$50 ms$', color='c')
axarr[1].set_xlim([0, 25])
axarr[1].set_ylim([0.006, 0.021])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<\Omega>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Rho_Omega_mask5e10_2048_LES.pdf",  bbox_inches="tight")
pl.close()

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, rho_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250, label=r'$10 ms$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, rho_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250, label=r'$30 ms$', color='b')
axarr[0].plot(r_cyl_2048_m5e10_avg_5250, rho_cyl_2048_m5e10_avg_5250*r_cyl_2048_m5e10_avg_5250*r_cyl_2048_m5e10_avg_5250, color='r', linestyle='--')
axarr[0].plot(r_cyl_2048_m5e10_avg_9250, rho_cyl_2048_m5e10_avg_9250*r_cyl_2048_m5e10_avg_9250*r_cyl_2048_m5e10_avg_9250, color='b', linestyle='--')
axarr[0].plot(r_cyl_2048_b0_m5e10_avg_5250, rho_cyl_2048_b0_m5e10_avg_5250*r_cyl_2048_b0_m5e10_avg_5250*r_cyl_2048_b0_m5e10_avg_5250, color='r', linestyle=':')
axarr[0].plot(r_cyl_2048_b0_m5e10_avg_9250, rho_cyl_2048_b0_m5e10_avg_9250*r_cyl_2048_b0_m5e10_avg_9250*r_cyl_2048_b0_m5e10_avg_9250, color='b', linestyle=':')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 14])
axarr[0].set_ylabel(r'$<r^2 \rho>_{10}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, omega_cyl_2048_les_m5e10_avg_5250, label=r'$2048_{LES}$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_9250, omega_cyl_2048_les_m5e10_avg_9250, color='b')
axarr[1].plot(r_cyl_2048_m5e10_avg_5250, omega_cyl_2048_m5e10_avg_5250, label=r'$2048$', color='r', linestyle='--')
axarr[1].plot(r_cyl_2048_m5e10_avg_9250, omega_cyl_2048_m5e10_avg_9250, color='b', linestyle='--')
axarr[1].plot(r_cyl_2048_b0_m5e10_avg_5250, omega_cyl_2048_b0_m5e10_avg_5250, label=r'$2048_{B0}$', color='r', linestyle=':')
axarr[1].plot(r_cyl_2048_b0_m5e10_avg_9250, omega_cyl_2048_b0_m5e10_avg_9250, color='b', linestyle=':')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 14])
axarr[1].set_ylim([0.01, 0.021])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<\Omega>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Rho_Omega_mask5e10_2048_Comp.pdf",  bbox_inches="tight")
pl.close()


'''