#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20

kpfactor = 1.48e5
spectra_factor = (24**3) * 1.8e54 * 1.48e5

li = "-"
col = ['k', 'g',  'r']


folderout = './'


lw=3.
fs = 22
lgs = 22
lz = 20
mysize = (6*5, 6*2)
mydpi = 200

folderin = './2048_LES/Spectra/L12/m1e7/'


ff, axs = plt.subplots(2, 5, sharex=True, sharey=True, figsize=mysize, dpi=mydpi) 


spectra = np.genfromtxt(fname=folderin + "/spectrum_5250.0.out1")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor


#Kolmogorov (kinetic)
index = -5./3. # Pendent de Kolmogorov
kref = ([kp[11],kp[35]]) # Inici i final en x
s1 = 2.e-8*spectra_factor # Where begins in y
k1 = kref[0]
k_m53 = ([s1,s1*(kref[1]/k1)**(index)])

#Kazantsev (magnetic)
indexm = 3./2. # Pendent de Kazantsev
krefm = ([kp[2],kp[9]]) # Inici i final en x
s1m = 1.e-13*spectra_factor # Where begins in y
k1m = krefm[0]
k_32 = ([s1m,s1m*(krefm[1]/k1m)**(indexm)])

x = np.array([5.499e-01, 9.115e+00])/kpfactor
y = np.array([2.374e-06, 1.2e-10])*spectra_factor

axs[0, 0].tick_params(labelsize=lz)
axs[0, 0].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 0].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 0].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 0].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 0].loglog( kref, k_m53, color='b',linestyle="-", linewidth=lw-1)
axs[0, 0].loglog( krefm, k_32, color='b',linestyle="--", linewidth=lw-1)
axs[0, 0].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[0, 0].set_ylim([5e48, 5e58]) 
axs[0, 0].axes.xaxis.set_ticklabels([])
axs[0, 0].grid(axis='both')
axs[0, 0].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 0].legend(loc = 1, fontsize=lgs)
axs[0, 0].set_ylabel(r'$\mathcal{E}(k) [erg~cm]$', fontsize=fs)

spectra = np.genfromtxt(fname=folderin + "/spectrum_7250.0.out2")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([5.077e-01, 6.473e+00])/kpfactor
y = np.array([2.461e-06, 1.1e-10])*spectra_factor

axs[0, 1].tick_params(labelsize=lz)
axs[0, 1].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 1].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 1].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 1].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 1].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[0, 1].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[0, 1].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[0, 1].axes.xaxis.set_ticklabels([])
axs[0, 1].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 1].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_9250.0.out3")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([5.058e-01, 4.720e+00])/kpfactor
y = np.array([2.4e-06, 1.6e-10])*spectra_factor

axs[0, 2].tick_params(labelsize=lz)
axs[0, 2].text(1.8e-5, 5e57, r'MR LES', fontsize=28)
axs[0, 2].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 2].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 2].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 2].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 2].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[0, 2].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[0, 2].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[0, 2].axes.xaxis.set_ticklabels([])
axs[0, 2].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 2].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_11250.0.out4")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([5.039e-01, 4.114e+00])/kpfactor
y = np.array([2.160e-06, 2.9e-10])*spectra_factor

axs[0, 3].tick_params(labelsize=lz)
axs[0, 3].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 3].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 3].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 3].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 3].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[0, 3].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[0, 3].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[0, 3].axes.xaxis.set_ticklabels([])
axs[0, 3].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 3].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_13000.0.out5")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([5.202e-01, 3.875e+00])/kpfactor
y = np.array([2.2e-06, 3.8e-10])*spectra_factor

axs[0, 4].tick_params(labelsize=lz)
axs[0, 4].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 4].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 4].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 4].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 4].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[0, 4].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[0, 4].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[0, 4].axes.xaxis.set_ticklabels([])
axs[0, 4].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 4].grid(axis='both')

folderin = './2048/Spectra/L12/m1e7/'

spectra = np.genfromtxt(fname=folderin + "/spectrum_5250.0.out1")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

#Kolmogorov (kinetic)
index = -5./3. # Pendent de Kolmogorov
kref = ([kp[11],kp[35]]) # Inici i final en x
s1 = 2.e-8*spectra_factor # Where begins in y
k1 = kref[0]
k_m53 = ([s1,s1*(kref[1]/k1)**(index)])

#Kazantsev (magnetic)
indexm = 3./2. # Pendent de Kazantsev
krefm = ([kp[2],kp[9]]) # Inici i final en x
s1m = 1.5e-14*spectra_factor # Where begins in y
k1m = krefm[0]
k_32 = ([s1m,s1m*(krefm[1]/k1m)**(indexm)])

x = np.array([5.214e-01, 9.671e+00])/kpfactor
y = np.array([2.374e-06, 1.7e-11])*spectra_factor

axs[1, 0].tick_params(labelsize=lz)
axs[1, 0].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 0].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 0].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 0].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 0].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[1, 0].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[1, 0].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[1, 0].set_ylim([1e48, 5e58])
axs[1, 0].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 0].legend(loc = 1, fontsize=lgs)
axs[1, 0].grid(axis='both')
axs[1, 0].set_ylabel(r'$\mathcal{E}(k) [erg~cm]$', fontsize=fs)
axs[1, 0].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)

spectra = np.genfromtxt(fname=folderin + "/spectrum_7250.0.out2")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([5.093e-01, 7.602e+00])/kpfactor
y = np.array([2.174e-06, 2.7e-11])*spectra_factor

axs[1, 1].tick_params(labelsize=lz)
axs[1, 1].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 1].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 1].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 1].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 1].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[1, 1].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[1, 1].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[1, 1].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[1, 1].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 1].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_9250.0.out3")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([4.992e-01, 6.208e+00])/kpfactor
y = np.array([2.174e-06, 2.8e-11])*spectra_factor

axs[1, 2].tick_params(labelsize=lz)
axs[1, 2].text(2.5e-5, 5e57, r'HR LES', fontsize=28)
axs[1, 2].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 2].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 2].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 2].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 2].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[1, 2].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[1, 2].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[1, 2].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[1, 2].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 2].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_11250.0.out4")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([4.992e-01, 6.208e+00])/kpfactor
y = np.array([2.174e-06, 2.8e-11])*spectra_factor

axs[1, 3].tick_params(labelsize=lz)
axs[1, 3].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 3].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 3].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 3].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 3].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[1, 3].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[1, 3].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[1, 3].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[1, 3].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 3].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_13000.0.out5")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

x = np.array([4.992e-01, 6.208e+00])/kpfactor
y = np.array([2.174e-06, 2.8e-11])*spectra_factor

axs[1, 4].tick_params(labelsize=lz)
axs[1, 4].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 4].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 4].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 4].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 4].loglog( kref, k_m53, label = r'$k^{-5/3}$', color='b',linestyle="-", linewidth=lw-1)
axs[1, 4].loglog( krefm, k_32, label = r'$k^{3/2}$', color='b',linestyle="--", linewidth=lw-1)
axs[1, 4].scatter(x, y, s=200, c=['darkorange', 'indigo'])
axs[1, 4].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[1, 4].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 4].grid(axis='both')


name = folderout+'spectra_poltor.png'
plt.tight_layout()
plt.savefig(name)
plt.close()
print("Figure created: ", name)

