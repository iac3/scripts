#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
#mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20

kpfactor = 1.48e5
spectra_factor = (24**3) * 1.8e54 * 1.48e5

li = "-"
col = ['k', 'g',  'r']


folderout = './'


lw=3.
fs = 22
lgs = 22
lz = 20
mysize = (6*3, 6*3)
mydpi = 200

folderin = './2048_LES/Spectra/L12/m1e7/'


ff, axs = plt.subplots(3, 3, sharex=True, sharey=True, figsize=mysize, dpi=mydpi) 

spectra = np.genfromtxt(fname=folderin + "/spectrum_4250.0.out0")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[0, 0].tick_params(labelsize=lz)
axs[0, 0].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 0].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 0].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 0].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 0].set_ylim([1e48, 5e58]) 
axs[0, 0].axes.xaxis.set_ticklabels([])
axs[0, 0].grid(axis='both')
axs[0, 0].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 0].legend(loc = 1, fontsize=lgs)
axs[0, 0].set_ylabel(r'$\mathcal{E}(k) [erg~cm]$', fontsize=fs)

spectra = np.genfromtxt(fname=folderin + "/spectrum_5250.0.out1")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[0, 1].tick_params(labelsize=lz)
axs[0, 1].text(1.8e-5, 5e57, r'MR LES', fontsize=28)
axs[0, 1].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 1].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 1].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 1].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 1].axes.xaxis.set_ticklabels([])
axs[0, 1].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 1].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_7250.0.out2")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[0, 2].tick_params(labelsize=lz)
axs[0, 2].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[0, 2].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[0, 2].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[0, 2].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[0, 2].axes.xaxis.set_ticklabels([])
axs[0, 2].set_xlim([1.1e-6, 1.1e-3]) 
axs[0, 2].grid(axis='both')


folderin = './4096/Spectra/L12/m1e7/'

spectra = np.genfromtxt(fname=folderin + "/spectrum_4250.0.out0")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor
axs[1, 0].tick_params(labelsize=lz)
axs[1, 0].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 0].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 0].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 0].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 0].set_ylim([1e48, 5e58]) 
axs[1, 0].axes.xaxis.set_ticklabels([])
axs[1, 0].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 0].grid(axis='both')
axs[1, 0].legend(loc = 1, fontsize=lgs)
axs[1, 0].set_ylabel(r'$\mathcal{E}(k) [erg~cm]$', fontsize=fs)

spectra = np.genfromtxt(fname=folderin + "/spectrum_5250.0.out1")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[1, 1].tick_params(labelsize=lz)
axs[1, 1].text(4e-5, 5e57, r'HR', fontsize=28)
axs[1, 1].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 1].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 1].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 1].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 1].axes.xaxis.set_ticklabels([])
axs[1, 1].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 1].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_7250.0.out2")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[1, 2].tick_params(labelsize=lz)
axs[1, 2].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[1, 2].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[1, 2].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[1, 2].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[1, 2].axes.xaxis.set_ticklabels([])
axs[1, 2].set_xlim([1.1e-6, 1.1e-3]) 
axs[1, 2].grid(axis='both')


folderin = './4096_LES/Spectra/L12/m1e7/'

spectra = np.genfromtxt(fname=folderin + "/spectrum_4250.0.out0")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor
axs[2, 0].tick_params(labelsize=lz)
axs[2, 0].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[2, 0].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[2, 0].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[2, 0].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[2, 0].set_ylim([1e48, 5e58])
axs[2, 0].set_xlim([1.1e-6, 1.1e-3]) 
axs[2, 0].legend(loc = 1, fontsize=lgs)
axs[2, 0].grid(axis='both')
axs[2, 0].set_ylabel(r'$\mathcal{E}(k) [erg~cm]$', fontsize=fs)
axs[2, 0].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)

spectra = np.genfromtxt(fname=folderin + "/spectrum_5250.0.out1")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[2, 1].tick_params(labelsize=lz)
axs[2, 1].text(2.5e-5, 5e57, r'HR LES', fontsize=28)
axs[2, 1].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[2, 1].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[2, 1].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[2, 1].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[2, 1].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[2, 1].set_xlim([1.1e-6, 1.1e-3]) 
axs[2, 1].grid(axis='both')

spectra = np.genfromtxt(fname=folderin + "/spectrum_7250.0.out2")
kp = spectra[:,0]/kpfactor
spekinevo = spectra[:,1]*spectra_factor
spemagevo = spectra[:,2]*spectra_factor
spemagevo_pol = spectra[:,3]*spectra_factor
spemagevo_tor = spectra[:,4]*spectra_factor

axs[2, 2].tick_params(labelsize=lz)
axs[2, 2].loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw )
axs[2, 2].loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
axs[2, 2].loglog( kp, spemagevo_pol, label = 'Poloidal', color=col[1],linestyle="--", linewidth=lw)
axs[2, 2].loglog( kp, spemagevo_tor, label = 'Toroidal', color=col[2],linestyle="--", linewidth=lw)
axs[2, 2].set_xlabel(r'$k [cm^{-1}]$', fontsize=fs)
axs[2, 2].set_xlim([1.1e-6, 1.1e-3]) 
axs[2, 2].grid(axis='both')

name = folderout+'spectra_poltor.png'
plt.tight_layout()
plt.savefig(name)
plt.close()
print("Figure created: ", name)

