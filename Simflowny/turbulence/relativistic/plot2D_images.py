import numpy as np
import os
import math
import sys
from common_plots import plot2DOrigin

"""
    2nd Step
    This script plots the calculations done by plot2D_slice_mapper.py.

    Both scripts are separated to allow an easy edition of plots without having
    to run calculations again.
"""

#Common factors
B_factor = 1e20
rho_factor = 6.14e17
omega_factor = 1.0/4.94e-6

res = [160, 160]            #Resolution
folderout = "./2D_plots/"   #Output folder (also input folder)
box_init = -59.0            #Box init for the plot
times = [4250.0, 5250.0, 7250.0, 9250.0, 11250.0, 13000.0]  #Times to process

for t in times:

    rho = np.load(folderout + "/rho_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", )
    invbeta = np.load(folderout + "/invbeta_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    omega = np.load(folderout + "/omega_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    b2 = np.load(folderout + "/b2_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy")
    plot2DOrigin(folderout + "/rho_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(rho*rho_factor), -box_init, cmap="rainbow", vmin=7, vmax=15)
    plot2DOrigin(folderout + "/invbeta_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(invbeta), -box_init, cmap="rainbow", vmin=-5, vmax=-1)
    plot2DOrigin(folderout + "/omega_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", omega*omega_factor, -box_init, cmap="rainbow", vmin=2000, vmax=100000)
    plot2DOrigin(folderout + "/b2_" + str(t) + "_" + str(res[0]) + "x" + str(res[1]) + ".png", np.log10(b2*B_factor), -box_init, cmap="YlOrRd", vmin=13, vmax=16)

    
    print(omega*omega_factor)