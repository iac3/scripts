import numpy as np
import h5py
import os
import math
import sys
from common_io_3D import readData3DAll
from common_op_3D import get_center_of_mass, get_existing_levels
from common_op_2D import int2D2

"""
    This script creates a 2D slice from 3D data from readhdf5_levels_3D.py.
    
    Derived data must be set in this script.
"""

#Common factors
B_factor = 1e20
rho_factor = 6.14e17
omega_factor = 1.0/4.94e-6

def mapping_to_plane(field, res, x_plane, box_init, center, level_init, dxs):
    """
    Maps 3D data into a plane.

    Args:
        field:          field to read
        res:            plane esolution
        x_plane:        Cut plane
        box_init:       Domain box, only lower values
        level_init:     Low limits of level domain
        dxs:            Cell spacing

    Return:
        mapped field
    """

    delta_y = 2*-box_init / res[0]
    delta_z = 2*-box_init / res[1]

    mapped_field = np.zeros((res[0], res[1]))
    for (j, k), v in np.ndenumerate(mapped_field):

        x = center[0] - x_plane
        y = center[1] + box_init + j * delta_y
        z = center[2] + box_init + k * delta_z

        # Index of lower left point of interpolation cell:
        ii = int((x - (level_init[0])) / dxs[0]);
        jj = int((y - (level_init[1])) / dxs[1]);
        kk = int((z - (level_init[2])) / dxs[2]);

        field_shape = field.shape

        #Check if the plane index is in the original data box
        if ii >= 0 and ii + 1 < field_shape[0] and jj >= 0 and jj + 1 < field_shape[1] and kk >= 0 and kk + 1 < field_shape[2]:
            xfrac = ( x - ((ii*dxs[0]) + level_init[0])) / dxs[0];
            yfrac = ( y - ((jj*dxs[1]) + level_init[1])) / dxs[1];
            zfrac = ( z - ((kk*dxs[2]) + level_init[2])) / dxs[2];

            mapped_field[j,k] = ((1-xfrac) * (1-yfrac) * (1-zfrac) * field[ii, jj, kk]
                        + (1 - xfrac) *      yfrac  *      zfrac  * field[ii, jj + 1, kk + 1]
                        +        xfrac  * (1-yfrac) *      zfrac  * field[ii + 1, jj, kk + 1]
                        +        xfrac  *      yfrac  * (1-zfrac) * field[ii + 1, jj + 1, kk]
                        +        xfrac  * (1-yfrac) * (1-zfrac) * field[ii + 1, jj, kk]
                        + (1 - xfrac) *      yfrac  * (1-zfrac) * field[ii, jj + 1, kk]
                        + (1 - xfrac) * (1-yfrac) *      zfrac  * field[ii, jj, kk + 1]
                        +        xfrac  *      yfrac *      zfrac  * field[ii + 1, jj + 1, kk + 1])


    return mapped_field



def slice_mapping(folders,dt,folderin,folderout,max_levels, box_init, res, x_plane):
    """
    Do the calculations in 3D and map into the plane.

    The process is done plane by plane and then accumulated.

    Args:
        folders:        Input base folders
        dt:             Simulation dt at finer level
        folderin:       Input path 
        folderout:      Ouput folder
        levels:         Levels where data is taken from
        box_init:       Domain box, only lower values
        res:            plane esolution
        x_plane:        Cut plane

    """

    #Get number of cycles
    Ncycle = 0
    for f in folders:
        for i in folders[f]:
            Ncycle = Ncycle + 1

    folderout_r = folderout
    if not os.path.exists(folderout_r):
        os.makedirs(folderout_r)

    cc = 0
    for f in folders:
        for i in folders[f]:
            print(f, i, dt)
            time = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5) + folderin

            #Get existing levels
            levels = get_existing_levels(foldertime, max_levels)

            rho = np.zeros((res[0], res[1]))
            b2 = np.zeros((res[0], res[1]))
            omega = np.zeros((res[0], res[1]))
            invbeta = np.zeros((res[0], res[1]))

            center_of_mass = [0,0,0]
            #Process each level separately
            for level in levels:
                print("level", level)
                l_i = levels.index(level)

                dxs = np.load(foldertime + "/dx_" + str(level) + ".npy")
                level_init = np.load(foldertime + "/init_" + str(level) + ".npy")
                dx = dxs[0]
                dy = dxs[1]
                dz = dxs[2]
                
                x = readData3DAll(foldertime+"x_"+str(level)+".hdf5","x")
                y = readData3DAll(foldertime+"y_"+str(level)+".hdf5","y")


                #Calculate center of mass in the finner level
                center_of_mass = get_center_of_mass(readData3DAll(foldertime + "/rhof_"+str(level)+".hdf5","rhof"), x, y, readData3DAll(foldertime+"z_"+str(level)+".hdf5","z"))

                x = x - center_of_mass[0]
                y = y - center_of_mass[1]
                
                rinv = np.sqrt(x**2 + y **2)        
                rinv[rinv == 0] = 0.0000000000001
                rinv = 1/rinv

                #Derived calculations
                int_rho = readData3DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
                int_rho = mapping_to_plane(int_rho, res, x_plane, box_init, center_of_mass, level_init, dxs)
                #Get only data from defined areas
                rho[np.where(int_rho > 0)] = int_rho[np.where(int_rho > 0)]
                
                int_b2 = readData3DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
                int_b2 = int_b2 * int_b2
                int_b2 = int_b2 + readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")*readData3DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
                int_b2 = int_b2 + readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")*readData3DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")
                int_b2 = int_b2 * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi")
                int_b2p = mapping_to_plane(np.sqrt(int_b2), res, x_plane, box_init, center_of_mass, level_init, dxs)  
                b2[np.where(int_b2p > 0)] = int_b2p[np.where(int_b2p > 0)]
                             
                int_omega = -y * rinv * (readData3DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x") - readData3DAll(foldertime+"Betau_x_"+str(level)+".hdf5","Betau_x"))
                int_omega = int_omega + x * rinv * (readData3DAll(foldertime+"Alpha_"+str(level)+".hdf5","Alpha") * readData3DAll(foldertime+"chi_"+str(level)+".hdf5","chi") * readData3DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y") - readData3DAll(foldertime+"Betau_y_"+str(level)+".hdf5","Betau_y"))
                int_omega = mapping_to_plane(int_omega, res, x_plane, box_init, center_of_mass, level_init, dxs)
                omega[np.where(int_omega > 0)] = int_omega[np.where(int_omega > 0)]

                # invbeta = B^2/(2*P)
                int_invbeta = 0.5* int_b2 / np.maximum(readData3DAll(foldertime+"pf_"+str(level)+".hdf5","pf"), 1e-20)
                int_invbeta = mapping_to_plane(int_invbeta, res, x_plane, box_init, center_of_mass, level_init, dxs)
                invbeta[np.where(int_invbeta > 0)] = int_invbeta[np.where(int_invbeta > 0)]
                
            print (omega[80,:])
            print (invbeta[80,:])
            np.save(folderout_r + "/rho_" + str(time) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", rho)
            np.save(folderout_r + "/invbeta_" + str(time) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", invbeta)
            np.save(folderout_r + "/omega_" + str(time) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", omega)
            np.save(folderout_r + "/b2_" + str(time) + "_" + str(res[0]) + "x" + str(res[1]) + ".npy", b2)