from plot2D_slice_mapper import slice_mapping
import numpy as np

"""
    1st Step
    This script creates a 2D slice from 3D data from readhdf5_levels_3D.py.
    2D data can be derived calculations, set up in plot2D_slice_mapper.py
    
    Once this script is run, plot2D_images.py will create the plot images.
"""

def main():

    levels = [4,5,6,7,8]        #Levels where data is taken from (See readhdf5_levels_3D.py output)
    folders = {"../outputDir_mesh_c5": [544000, 672000, 928000, 1184000, 1440000,1664000]}  #Input folders
    dt = 4.0/(2**9)             #Simulation dt at finer level
    box_init = -59.0            #Box init for the plot
    
    #Slice
    res = [160, 160]            #Resolution
    x_plane = 0                 #Cut plane, in domain units

    folderout = "./2D_plots/"                           #Output folder for slice data
    folderin = "/level8_multilevel/mask1e-10/box60/"    #Folder for input data (Complete path is folders + folderin)

    slice_mapping(folders, dt, folderin, folderout, levels, box_init, res, x_plane)


if __name__ == "__main__":
    main()

