#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl
import math
from pylab import *
mpl.rcParams['text.usetex']=True
mpl.rcParams['text.latex.unicode']=True
mpl.rcParams['axes.titlesize'] = 20

time = "7250.0"


B_factor = 1e20
E_factor = 1.8e54

# field_script_resolution_mask_file

########### Datos 2D ###########

int2d_1024 = np.genfromtxt(fname="1024/Integrals_2D/level7/mask1e13/box60/integral.out")
t_int2d_1024_m1e13_int = int2d_1024[:,0]
BRMS_int2d_1024_m1e13_int = int2d_1024[:,5] * B_factor
BRMSP_int2d_1024_m1e13_int = int2d_1024[:,6] * B_factor
BRMST_int2d_1024_m1e13_int = int2d_1024[:,7] * B_factor
int2d_1024_m5e10 = np.genfromtxt(fname="1024/Integrals_2D/level7/mask5e10/box60/integral.out")
t_int2d_1024_m5e10_int = int2d_1024_m5e10[:,0]
BRMS_int2d_1024_m5e10_int = int2d_1024_m5e10[:,5] * B_factor
BRMSP_int2d_1024_m5e10_int = int2d_1024_m5e10[:,6] * B_factor
BRMST_int2d_1024_m5e10_int = int2d_1024_m5e10[:,7] * B_factor

int2d_1024_les = np.genfromtxt(fname="1024_LES/Integrals_2D/level7/mask1e13/box60/integral.out")
t_int2d_1024_les_m1e13_int = int2d_1024_les[:,0]
BRMS_int2d_1024_les_m1e13_int = int2d_1024_les[:,5] * B_factor
BRMSP_int2d_1024_les_m1e13_int = int2d_1024_les[:,6] * B_factor
BRMST_int2d_1024_les_m1e13_int = int2d_1024_les[:,7] * B_factor
int2d_1024_les_m5e10 = np.genfromtxt(fname="1024_LES/Integrals_2D/level7/mask5e10/box60/integral.out")
t_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,0]
BRMS_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,5] * B_factor
BRMSP_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,6] * B_factor
BRMST_int2d_1024_les_m5e10_int = int2d_1024_les_m5e10[:,7] * B_factor

int2d_2048 = np.genfromtxt(fname="2048/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_m1e13_int = int2d_2048[:,0]
BRMS_int2d_2048_m1e13_int = int2d_2048[:,5] * B_factor
BRMSP_int2d_2048_m1e13_int = int2d_2048[:,6] * B_factor
BRMST_int2d_2048_m1e13_int = int2d_2048[:,7] * B_factor
int2d_2048_m5e10 = np.genfromtxt(fname="2048/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_m5e10_int = int2d_2048_m5e10[:,0]
BRMS_int2d_2048_m5e10_int = int2d_2048_m5e10[:,5] * B_factor
BRMSP_int2d_2048_m5e10_int = int2d_2048_m5e10[:,6] * B_factor
BRMST_int2d_2048_m5e10_int = int2d_2048_m5e10[:,7] * B_factor

int2d_2048_les = np.genfromtxt(fname="2048_LES/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_les_m1e13_int = int2d_2048_les[:,0]
BRMS_int2d_2048_les_m1e13_int = int2d_2048_les[:,5] * B_factor
BRMSP_int2d_2048_les_m1e13_int = int2d_2048_les[:,6] * B_factor
BRMST_int2d_2048_les_m1e13_int = int2d_2048_les[:,7] * B_factor
int2d_2048_les_m5e10 = np.genfromtxt(fname="2048_LES/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,0]
BRMS_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,5] * B_factor
BRMSP_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,6] * B_factor
BRMST_int2d_2048_les_m5e10_int = int2d_2048_les_m5e10[:,7] * B_factor

int2d_2048_les_t10 = np.genfromtxt(fname="2048_LES_t10/Integrals_2D/level8/mask1e13/box60/integral.out")
t_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,0]
BRMS_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,5] * B_factor
BRMSP_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,6] * B_factor
BRMST_int2d_2048_les_t10_m1e13_int = int2d_2048_les_t10[:,7] * B_factor
int2d_2048_les_t10_m5e10 = np.genfromtxt(fname="2048_LES_t10/Integrals_2D/level8/mask5e10/box60/integral.out")
t_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,0]
BRMS_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,5] * B_factor
BRMSP_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,6] * B_factor
BRMST_int2d_2048_les_t10_m5e10_int = int2d_2048_les_t10_m5e10[:,7] * B_factor

int2d_4096 = np.genfromtxt(fname="4096/Integrals_2D/level9/mask1e13/box60/integral.out")
t_int2d_4096_m1e13_int = int2d_4096[:,0]
BRMSP_int2d_4096_m1e13_int = int2d_4096[:,6] * B_factor
BRMST_int2d_4096_m1e13_int = int2d_4096[:,7] * B_factor
int2d_4096_m5e10 = np.genfromtxt(fname="4096/Integrals_2D/level9/mask5e10/box60/integral.out")
t_int2d_4096_m5e10_int = int2d_4096_m5e10[:,0]
BRMSP_int2d_4096_m5e10_int = int2d_4096_m5e10[:,6] * B_factor
BRMST_int2d_4096_m5e10_int = int2d_4096_m5e10[:,7] * B_factor

int2d_4096_les = np.genfromtxt(fname="4096_LES/Integrals_2D/level9/mask1e13/box60/integral.out")
t_int2d_4096_les_m1e13_int = int2d_4096_les[:,0]
BRMSP_int2d_4096_les_m1e13_int = int2d_4096_les[:,6] * B_factor
BRMST_int2d_4096_les_m1e13_int = int2d_4096_les[:,7] * B_factor
int2d_4096_les_m5e10 = np.genfromtxt(fname="4096_LES/Integrals_2D/level9/mask5e10/box60/integral.out")
t_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,0]
BRMSP_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,6] * B_factor
BRMST_int2d_4096_les_m5e10_int = int2d_4096_les_m5e10[:,7] * B_factor


########### Datos 3D ###########

int3d_1024_les_m5e10_int = np.genfromtxt(fname="1024_LES/Integrals_3D/level7_multilevel/mask5e10/box60/integral.out")
t_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,0]
Ekin_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,1] * E_factor
Eint_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,3] * E_factor
Emag_int3d_1024_les_m5e10_int = int3d_1024_les_m5e10_int[:,4] * E_factor

int3d_1024_m5e10_int = np.genfromtxt(fname="1024/Integrals_3D/level7_multilevel/mask5e10/box60/integral.out")
t_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,0]
Ekin_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,1] * E_factor
Eint_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,3] * E_factor
Emag_int3d_1024_m5e10_int = int3d_1024_m5e10_int[:,4] * E_factor

int3d_2048_les_m1e13_avg = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask1e13/box60/averages.out")
t_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,0]
B_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,1] * B_factor
Btor_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,2] * B_factor
Bpol_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,3] * B_factor
invbeta_int3d_2048_les_m1e13_avg = int3d_2048_les_m1e13_avg[:,7]
int3d_2048_les_m5e10_avg = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask5e10/box60/averages.out")
t_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,0]
B_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_les_m5e10_avg = int3d_2048_les_m5e10_avg[:,7]
int3d_2048_les_m5e10_int = np.genfromtxt(fname="2048_LES/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,0]
Ekin_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,1] * E_factor
Eint_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,3] * E_factor
Emag_int3d_2048_les_m5e10_int = int3d_2048_les_m5e10_int[:,4] * E_factor

int3d_2048_m1e13_avg = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask1e13/box60/averages.out")
t_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,0]
B_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,1] * B_factor
Btor_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,2] * B_factor
Bpol_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,3] * B_factor
invbeta_int3d_2048_m1e13_avg = int3d_2048_m1e13_avg[:,7]
int3d_2048_m5e10_avg = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask5e10/box60/averages.out")
t_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,0]
B_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,1] * B_factor
Btor_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,2] * B_factor
Bpol_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,3] * B_factor
invbeta_int3d_2048_m5e10_avg = int3d_2048_m5e10_avg[:,7]
int3d_2048_m5e10_int = np.genfromtxt(fname="2048/Integrals_3D/level8_multilevel/mask5e10/box60/integral.out")
t_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,0]
Ekin_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,1] * E_factor
Eint_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,3] * E_factor
Emag_int3d_2048_m5e10_int = int3d_2048_m5e10_int[:,4] * E_factor

int3d_4096_m1e13_avg = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask1e13/box60/averages.out")
t_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,0]
B_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,1] * B_factor
Btor_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,2] * B_factor
Bpol_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,3] * B_factor
invbeta_int3d_4096_m1e13_avg = int3d_4096_m1e13_avg[:,7]
int3d_4096_m5e10_avg = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask5e10/box60/averages.out")
t_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,0]
B_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_m5e10_avg = int3d_4096_m5e10_avg[:,7]
int3d_4096_m5e10_int = np.genfromtxt(fname="4096/Integrals_3D/level9_multilevel/mask5e10/box60/integral.out")
t_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,0]
Ekin_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,1] * E_factor
Eint_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,3] * E_factor
Emag_int3d_4096_m5e10_int = int3d_4096_m5e10_int[:,4] * E_factor

int3d_4096_les_m1e13_avg = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask1e13/box60/averages.out")
t_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,0]
B_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,1] * B_factor
Btor_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,2] * B_factor
Bpol_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,3] * B_factor
invbeta_int3d_4096_les_m1e13_avg = int3d_4096_les_m1e13_avg[:,7]
int3d_4096_les_m5e10_avg = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask5e10/box60/averages.out")
t_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,0]
B_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,1] * B_factor
Btor_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,2] * B_factor
Bpol_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,3] * B_factor
invbeta_int3d_4096_les_m5e10_avg = int3d_4096_les_m5e10_avg[:,7]
int3d_4096_les_m5e10_int = np.genfromtxt(fname="4096_LES/Integrals_3D/level9_multilevel/mask5e10/box60/integral.out")
t_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,0]
Ekin_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,1] * E_factor
Eint_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,3] * E_factor
Emag_int3d_4096_les_m5e10_int = int3d_4096_les_m5e10_int[:,4] * E_factor


########### Datos cilindro ############

cyl_m1e13_2048_les_4250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,0]
Btor_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,3] * B_factor
invbeta_cyl_2048_les_m1e13_avg_4250 = cyl_m1e13_2048_les_4250[:,7]
cyl_m1e13_2048_les_5250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,0]
Btor_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_5250 = cyl_m1e13_2048_les_5250[:,3] * B_factor
cyl_m1e13_2048_les_7250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,0]
Btor_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_7250 = cyl_m1e13_2048_les_7250[:,3] * B_factor
cyl_m1e13_2048_les_9250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,0]
Btor_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_9250 = cyl_m1e13_2048_les_9250[:,3] * B_factor
cyl_m1e13_2048_les_11250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,0]
Btor_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_11250 = cyl_m1e13_2048_les_11250[:,3] * B_factor
cyl_m1e13_2048_les_13000 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,0]
Btor_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,2] * B_factor
Bpol_cyl_2048_les_m1e13_avg_13000 = cyl_m1e13_2048_les_13000[:,3] * B_factor
cyl_m5e10_2048_les_4250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,0]
Btor_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,3] * B_factor
invbeta_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,7]
rho_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,8]
omega_cyl_2048_les_m5e10_avg_4250 = cyl_m5e10_2048_les_4250[:,9]
cyl_m5e10_2048_les_5250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_5250.0.out")
r_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,0]
Btor_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,3] * B_factor
rho_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,8]
omega_cyl_2048_les_m5e10_avg_5250 = cyl_m5e10_2048_les_5250[:,9]
cyl_m5e10_2048_les_7250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_7250.0.out")
r_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,0]
Btor_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,3] * B_factor
rho_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,8]
omega_cyl_2048_les_m5e10_avg_7250 = cyl_m5e10_2048_les_7250[:,9]
cyl_m5e10_2048_les_9250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_9250.0.out")
r_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,0]
Btor_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,3] * B_factor
rho_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,8]
omega_cyl_2048_les_m5e10_avg_9250 = cyl_m5e10_2048_les_9250[:,9]
cyl_m5e10_2048_les_11250 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_11250.0.out")
r_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,0]
Btor_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,3] * B_factor
rho_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,8]
omega_cyl_2048_les_m5e10_avg_11250 = cyl_m5e10_2048_les_11250[:,9]
cyl_m5e10_2048_les_13000 = np.genfromtxt(fname="2048_LES/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_13000.0.out")
r_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,0]
Btor_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,2] * B_factor
Bpol_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,3] * B_factor
rho_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,8]
omega_cyl_2048_les_m5e10_avg_13000 = cyl_m5e10_2048_les_13000[:,9]

cyl_m1e13_2048_4250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask1e13/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_m1e13_avg_4250 = cyl_m1e13_2048_4250[:,0]
invbeta_cyl_2048_m1e13_avg_4250 = cyl_m1e13_2048_4250[:,7]
cyl_m5e10_2048_4250 = np.genfromtxt(fname="2048/Spectra_levels_com/level8_multilevel/mask5e10/box60_r/averages_cylinder_4250.0.out")
r_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,0]
invbeta_cyl_2048_m5e10_avg_4250 = cyl_m5e10_2048_4250[:,7]


############ Harmonics ######################

harm_avg_2048_les_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c00.dat")
t_harm_avg_2048_les_c00 = harm_avg_2048_les_c00[:,0]
v_harm_avg_2048_les_c00 = harm_avg_2048_les_c00[:,1]**2 + harm_avg_2048_les_c00[:,2]**2
harm_avg_2048_les_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c10.dat")
t_harm_avg_2048_les_c10 = harm_avg_2048_les_c10[:,0]
v_harm_avg_2048_les_c10 = harm_avg_2048_les_c10[:,1]**2 + harm_avg_2048_les_c10[:,2]**2
harm_avg_2048_les_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c20.dat")
t_harm_avg_2048_les_c20 = harm_avg_2048_les_c20[:,0]
v_harm_avg_2048_les_c20 = harm_avg_2048_les_c20[:,1]**2 + harm_avg_2048_les_c20[:,2]**2
harm_avg_2048_les_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c30.dat")
t_harm_avg_2048_les_c30 = harm_avg_2048_les_c30[:,0]
v_harm_avg_2048_les_c30 = harm_avg_2048_les_c30[:,1]**2 + harm_avg_2048_les_c30[:,2]**2
harm_avg_2048_les_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c40.dat")
t_harm_avg_2048_les_c40 = harm_avg_2048_les_c40[:,0]
v_harm_avg_2048_les_c40 = harm_avg_2048_les_c40[:,1]**2 + harm_avg_2048_les_c40[:,2]**2
harm_avg_2048_les_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c50.dat")
t_harm_avg_2048_les_c50 = harm_avg_2048_les_c50[:,0]
v_harm_avg_2048_les_c50 = harm_avg_2048_les_c50[:,1]**2 + harm_avg_2048_les_c50[:,2]**2
harm_avg_2048_les_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c60.dat")
t_harm_avg_2048_les_c60 = harm_avg_2048_les_c60[:,0]
v_harm_avg_2048_les_c60 = harm_avg_2048_les_c60[:,1]**2 + harm_avg_2048_les_c60[:,2]**2
harm_avg_2048_les_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c70.dat")
t_harm_avg_2048_les_c70 = harm_avg_2048_les_c70[:,0]
v_harm_avg_2048_les_c70 = harm_avg_2048_les_c70[:,1]**2 + harm_avg_2048_les_c70[:,2]**2
harm_avg_2048_les_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c80.dat")
t_harm_avg_2048_les_c80 = harm_avg_2048_les_c80[:,0]
v_harm_avg_2048_les_c80 = harm_avg_2048_les_c80[:,1]**2 + harm_avg_2048_les_c80[:,2]**2

harm_avg_2048_les_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c1p1.dat")
t_harm_avg_2048_les_c1p1 = harm_avg_2048_les_c1p1[:,0]
v_harm_avg_2048_les_c1p1 = harm_avg_2048_les_c1p1[:,1]**2 + harm_avg_2048_les_c1p1[:,2]**2
harm_avg_2048_les_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2p1.dat")
t_harm_avg_2048_les_c2p1 = harm_avg_2048_les_c2p1[:,0]
v_harm_avg_2048_les_c2p1 = harm_avg_2048_les_c2p1[:,1]**2 + harm_avg_2048_les_c2p1[:,2]**2
harm_avg_2048_les_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p1.dat")
t_harm_avg_2048_les_c3p1 = harm_avg_2048_les_c3p1[:,0]
v_harm_avg_2048_les_c3p1 = harm_avg_2048_les_c3p1[:,1]**2 + harm_avg_2048_les_c3p1[:,2]**2
harm_avg_2048_les_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p1.dat")
t_harm_avg_2048_les_c4p1 = harm_avg_2048_les_c4p1[:,0]
v_harm_avg_2048_les_c4p1 = harm_avg_2048_les_c4p1[:,1]**2 + harm_avg_2048_les_c4p1[:,2]**2
harm_avg_2048_les_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p1.dat")
t_harm_avg_2048_les_c5p1 = harm_avg_2048_les_c5p1[:,0]
v_harm_avg_2048_les_c5p1 = harm_avg_2048_les_c5p1[:,1]**2 + harm_avg_2048_les_c5p1[:,2]**2
harm_avg_2048_les_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p1.dat")
t_harm_avg_2048_les_c6p1 = harm_avg_2048_les_c6p1[:,0]
v_harm_avg_2048_les_c6p1 = harm_avg_2048_les_c6p1[:,1]**2 + harm_avg_2048_les_c6p1[:,2]**2
harm_avg_2048_les_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p1.dat")
t_harm_avg_2048_les_c7p1 = harm_avg_2048_les_c7p1[:,0]
v_harm_avg_2048_les_c7p1 = harm_avg_2048_les_c7p1[:,1]**2 + harm_avg_2048_les_c7p1[:,2]**2
harm_avg_2048_les_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p1.dat")
t_harm_avg_2048_les_c8p1 = harm_avg_2048_les_c8p1[:,0]
v_harm_avg_2048_les_c8p1 = harm_avg_2048_les_c8p1[:,1]**2 + harm_avg_2048_les_c8p1[:,2]**2

harm_avg_2048_les_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c1m1.dat")
t_harm_avg_2048_les_c1m1 = harm_avg_2048_les_c1m1[:,0]
v_harm_avg_2048_les_c1m1 = harm_avg_2048_les_c1m1[:,1]**2 + harm_avg_2048_les_c1m1[:,2]**2
harm_avg_2048_les_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2m1.dat")
t_harm_avg_2048_les_c2m1 = harm_avg_2048_les_c2m1[:,0]
v_harm_avg_2048_les_c2m1 = harm_avg_2048_les_c2m1[:,1]**2 + harm_avg_2048_les_c2m1[:,2]**2
harm_avg_2048_les_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m1.dat")
t_harm_avg_2048_les_c3m1 = harm_avg_2048_les_c3m1[:,0]
v_harm_avg_2048_les_c3m1 = harm_avg_2048_les_c3m1[:,1]**2 + harm_avg_2048_les_c3m1[:,2]**2
harm_avg_2048_les_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m1.dat")
t_harm_avg_2048_les_c4m1 = harm_avg_2048_les_c4m1[:,0]
v_harm_avg_2048_les_c4m1 = harm_avg_2048_les_c4m1[:,1]**2 + harm_avg_2048_les_c4m1[:,2]**2
harm_avg_2048_les_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m1.dat")
t_harm_avg_2048_les_c5m1 = harm_avg_2048_les_c5m1[:,0]
v_harm_avg_2048_les_c5m1 = harm_avg_2048_les_c5m1[:,1]**2 + harm_avg_2048_les_c5m1[:,2]**2
harm_avg_2048_les_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m1.dat")
t_harm_avg_2048_les_c6m1 = harm_avg_2048_les_c6m1[:,0]
v_harm_avg_2048_les_c6m1 = harm_avg_2048_les_c6m1[:,1]**2 + harm_avg_2048_les_c6m1[:,2]**2
harm_avg_2048_les_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m1.dat")
t_harm_avg_2048_les_c7m1 = harm_avg_2048_les_c7m1[:,0]
v_harm_avg_2048_les_c7m1 = harm_avg_2048_les_c7m1[:,1]**2 + harm_avg_2048_les_c7m1[:,2]**2
harm_avg_2048_les_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m1.dat")
t_harm_avg_2048_les_c8m1 = harm_avg_2048_les_c8m1[:,0]
v_harm_avg_2048_les_c8m1 = harm_avg_2048_les_c8m1[:,1]**2 + harm_avg_2048_les_c8m1[:,2]**2

harm_avg_2048_les_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2p2.dat")
t_harm_avg_2048_les_c2p2 = harm_avg_2048_les_c2p2[:,0]
v_harm_avg_2048_les_c2p2 = harm_avg_2048_les_c2p2[:,1]**2 + harm_avg_2048_les_c2p2[:,2]**2
harm_avg_2048_les_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p2.dat")
t_harm_avg_2048_les_c3p2 = harm_avg_2048_les_c3p2[:,0]
v_harm_avg_2048_les_c3p2 = harm_avg_2048_les_c3p2[:,1]**2 + harm_avg_2048_les_c3p2[:,2]**2
harm_avg_2048_les_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p2.dat")
t_harm_avg_2048_les_c4p2 = harm_avg_2048_les_c4p2[:,0]
v_harm_avg_2048_les_c4p2 = harm_avg_2048_les_c4p2[:,1]**2 + harm_avg_2048_les_c4p2[:,2]**2
harm_avg_2048_les_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p2.dat")
t_harm_avg_2048_les_c5p2 = harm_avg_2048_les_c5p2[:,0]
v_harm_avg_2048_les_c5p2 = harm_avg_2048_les_c5p2[:,1]**2 + harm_avg_2048_les_c5p2[:,2]**2
harm_avg_2048_les_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p2.dat")
t_harm_avg_2048_les_c6p2 = harm_avg_2048_les_c6p2[:,0]
v_harm_avg_2048_les_c6p2 = harm_avg_2048_les_c6p2[:,1]**2 + harm_avg_2048_les_c6p2[:,2]**2
harm_avg_2048_les_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p2.dat")
t_harm_avg_2048_les_c7p2 = harm_avg_2048_les_c7p2[:,0]
v_harm_avg_2048_les_c7p2 = harm_avg_2048_les_c7p2[:,1]**2 + harm_avg_2048_les_c7p2[:,2]**2
harm_avg_2048_les_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p2.dat")
t_harm_avg_2048_les_c8p2 = harm_avg_2048_les_c8p2[:,0]
v_harm_avg_2048_les_c8p2 = harm_avg_2048_les_c8p2[:,1]**2 + harm_avg_2048_les_c8p2[:,2]**2

harm_avg_2048_les_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c2m2.dat")
t_harm_avg_2048_les_c2m2 = harm_avg_2048_les_c2m2[:,0]
v_harm_avg_2048_les_c2m2 = harm_avg_2048_les_c2m2[:,1]**2 + harm_avg_2048_les_c2m2[:,2]**2
harm_avg_2048_les_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m2.dat")
t_harm_avg_2048_les_c3m2 = harm_avg_2048_les_c3m2[:,0]
v_harm_avg_2048_les_c3m2 = harm_avg_2048_les_c3m2[:,1]**2 + harm_avg_2048_les_c3m2[:,2]**2
harm_avg_2048_les_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m2.dat")
t_harm_avg_2048_les_c4m2 = harm_avg_2048_les_c4m2[:,0]
v_harm_avg_2048_les_c4m2 = harm_avg_2048_les_c4m2[:,1]**2 + harm_avg_2048_les_c4m2[:,2]**2
harm_avg_2048_les_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m2.dat")
t_harm_avg_2048_les_c5m2 = harm_avg_2048_les_c5m2[:,0]
v_harm_avg_2048_les_c5m2 = harm_avg_2048_les_c5m2[:,1]**2 + harm_avg_2048_les_c5m2[:,2]**2
harm_avg_2048_les_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m2.dat")
t_harm_avg_2048_les_c6m2 = harm_avg_2048_les_c6m2[:,0]
v_harm_avg_2048_les_c6m2 = harm_avg_2048_les_c6m2[:,1]**2 + harm_avg_2048_les_c6m2[:,2]**2
harm_avg_2048_les_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m2.dat")
t_harm_avg_2048_les_c7m2 = harm_avg_2048_les_c7m2[:,0]
v_harm_avg_2048_les_c7m2 = harm_avg_2048_les_c7m2[:,1]**2 + harm_avg_2048_les_c7m2[:,2]**2
harm_avg_2048_les_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m2.dat")
t_harm_avg_2048_les_c8m2 = harm_avg_2048_les_c8m2[:,0]
v_harm_avg_2048_les_c8m2 = harm_avg_2048_les_c8m2[:,1]**2 + harm_avg_2048_les_c8m2[:,2]**2

harm_avg_2048_les_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3p3.dat")
t_harm_avg_2048_les_c3p3 = harm_avg_2048_les_c3p3[:,0]
v_harm_avg_2048_les_c3p3 = harm_avg_2048_les_c3p3[:,1]**2 + harm_avg_2048_les_c3p3[:,2]**2
harm_avg_2048_les_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p3.dat")
t_harm_avg_2048_les_c4p3 = harm_avg_2048_les_c4p3[:,0]
v_harm_avg_2048_les_c4p3 = harm_avg_2048_les_c4p3[:,1]**2 + harm_avg_2048_les_c4p3[:,2]**2
harm_avg_2048_les_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p3.dat")
t_harm_avg_2048_les_c5p3 = harm_avg_2048_les_c5p3[:,0]
v_harm_avg_2048_les_c5p3 = harm_avg_2048_les_c5p3[:,1]**2 + harm_avg_2048_les_c5p3[:,2]**2
harm_avg_2048_les_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p3.dat")
t_harm_avg_2048_les_c6p3 = harm_avg_2048_les_c6p3[:,0]
v_harm_avg_2048_les_c6p3 = harm_avg_2048_les_c6p3[:,1]**2 + harm_avg_2048_les_c6p3[:,2]**2
harm_avg_2048_les_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p3.dat")
t_harm_avg_2048_les_c7p3 = harm_avg_2048_les_c7p3[:,0]
v_harm_avg_2048_les_c7p3 = harm_avg_2048_les_c7p3[:,1]**2 + harm_avg_2048_les_c7p3[:,2]**2
harm_avg_2048_les_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p3.dat")
t_harm_avg_2048_les_c8p3 = harm_avg_2048_les_c8p3[:,0]
v_harm_avg_2048_les_c8p3 = harm_avg_2048_les_c8p3[:,1]**2 + harm_avg_2048_les_c8p3[:,2]**2

harm_avg_2048_les_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c3m3.dat")
t_harm_avg_2048_les_c3m3 = harm_avg_2048_les_c3m3[:,0]
v_harm_avg_2048_les_c3m3 = harm_avg_2048_les_c3m3[:,1]**2 + harm_avg_2048_les_c3m3[:,2]**2
harm_avg_2048_les_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m3.dat")
t_harm_avg_2048_les_c4m3 = harm_avg_2048_les_c4m3[:,0]
v_harm_avg_2048_les_c4m3 = harm_avg_2048_les_c4m3[:,1]**2 + harm_avg_2048_les_c4m3[:,2]**2
harm_avg_2048_les_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m3.dat")
t_harm_avg_2048_les_c5m3 = harm_avg_2048_les_c5m3[:,0]
v_harm_avg_2048_les_c5m3 = harm_avg_2048_les_c5m3[:,1]**2 + harm_avg_2048_les_c5m3[:,2]**2
harm_avg_2048_les_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m3.dat")
t_harm_avg_2048_les_c6m3 = harm_avg_2048_les_c6m3[:,0]
v_harm_avg_2048_les_c6m3 = harm_avg_2048_les_c6m3[:,1]**2 + harm_avg_2048_les_c6m3[:,2]**2
harm_avg_2048_les_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m3.dat")
t_harm_avg_2048_les_c7m3 = harm_avg_2048_les_c7m3[:,0]
v_harm_avg_2048_les_c7m3 = harm_avg_2048_les_c7m3[:,1]**2 + harm_avg_2048_les_c7m3[:,2]**2
harm_avg_2048_les_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m3.dat")
t_harm_avg_2048_les_c8m3 = harm_avg_2048_les_c8m3[:,0]
v_harm_avg_2048_les_c8m3 = harm_avg_2048_les_c8m3[:,1]**2 + harm_avg_2048_les_c8m3[:,2]**2

harm_avg_2048_les_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4p4.dat")
t_harm_avg_2048_les_c4p4 = harm_avg_2048_les_c4p4[:,0]
v_harm_avg_2048_les_c4p4 = harm_avg_2048_les_c4p4[:,1]**2 + harm_avg_2048_les_c4p4[:,2]**2
harm_avg_2048_les_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p4.dat")
t_harm_avg_2048_les_c5p4 = harm_avg_2048_les_c5p4[:,0]
v_harm_avg_2048_les_c5p4 = harm_avg_2048_les_c5p4[:,1]**2 + harm_avg_2048_les_c5p4[:,2]**2
harm_avg_2048_les_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p4.dat")
t_harm_avg_2048_les_c6p4 = harm_avg_2048_les_c6p4[:,0]
v_harm_avg_2048_les_c6p4 = harm_avg_2048_les_c6p4[:,1]**2 + harm_avg_2048_les_c6p4[:,2]**2
harm_avg_2048_les_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p4.dat")
t_harm_avg_2048_les_c7p4 = harm_avg_2048_les_c7p4[:,0]
v_harm_avg_2048_les_c7p4 = harm_avg_2048_les_c7p4[:,1]**2 + harm_avg_2048_les_c7p4[:,2]**2
harm_avg_2048_les_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p4.dat")
t_harm_avg_2048_les_c8p4 = harm_avg_2048_les_c8p4[:,0]
v_harm_avg_2048_les_c8p4 = harm_avg_2048_les_c8p4[:,1]**2 + harm_avg_2048_les_c8p4[:,2]**2

harm_avg_2048_les_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c4m4.dat")
t_harm_avg_2048_les_c4m4 = harm_avg_2048_les_c4m4[:,0]
v_harm_avg_2048_les_c4m4 = harm_avg_2048_les_c4m4[:,1]**2 + harm_avg_2048_les_c4m4[:,2]**2
harm_avg_2048_les_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m4.dat")
t_harm_avg_2048_les_c5m4 = harm_avg_2048_les_c5m4[:,0]
v_harm_avg_2048_les_c5m4 = harm_avg_2048_les_c5m4[:,1]**2 + harm_avg_2048_les_c5m4[:,2]**2
harm_avg_2048_les_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m4.dat")
t_harm_avg_2048_les_c6m4 = harm_avg_2048_les_c6m4[:,0]
v_harm_avg_2048_les_c6m4 = harm_avg_2048_les_c6m4[:,1]**2 + harm_avg_2048_les_c6m4[:,2]**2
harm_avg_2048_les_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m4.dat")
t_harm_avg_2048_les_c7m4 = harm_avg_2048_les_c7m4[:,0]
v_harm_avg_2048_les_c7m4 = harm_avg_2048_les_c7m4[:,1]**2 + harm_avg_2048_les_c7m4[:,2]**2
harm_avg_2048_les_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m4.dat")
t_harm_avg_2048_les_c8m4 = harm_avg_2048_les_c8m4[:,0]
v_harm_avg_2048_les_c8m4 = harm_avg_2048_les_c8m4[:,1]**2 + harm_avg_2048_les_c8m4[:,2]**2

harm_avg_2048_les_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5p5.dat")
t_harm_avg_2048_les_c5p5 = harm_avg_2048_les_c5p5[:,0]
v_harm_avg_2048_les_c5p5 = harm_avg_2048_les_c5p5[:,1]**2 + harm_avg_2048_les_c5p5[:,2]**2
harm_avg_2048_les_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6p5.dat")
t_harm_avg_2048_les_c6p5 = harm_avg_2048_les_c6p5[:,0]
v_harm_avg_2048_les_c6p5 = harm_avg_2048_les_c6p5[:,1]**2 + harm_avg_2048_les_c6p5[:,2]**2
harm_avg_2048_les_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7p5.dat")
t_harm_avg_2048_les_c7p5 = harm_avg_2048_les_c7p5[:,0]
v_harm_avg_2048_les_c7p5 = harm_avg_2048_les_c7p5[:,1]**2 + harm_avg_2048_les_c7p5[:,2]**2
harm_avg_2048_les_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8p5.dat")
t_harm_avg_2048_les_c8p5 = harm_avg_2048_les_c8p5[:,0]
v_harm_avg_2048_les_c8p5 = harm_avg_2048_les_c8p5[:,1]**2 + harm_avg_2048_les_c8p5[:,2]**2

harm_avg_2048_les_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c5m5.dat")
t_harm_avg_2048_les_c5m5 = harm_avg_2048_les_c5m5[:,0]
v_harm_avg_2048_les_c5m5 = harm_avg_2048_les_c5m5[:,1]**2 + harm_avg_2048_les_c5m5[:,2]**2
harm_avg_2048_les_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c6m5.dat")
t_harm_avg_2048_les_c6m5 = harm_avg_2048_les_c6m5[:,0]
v_harm_avg_2048_les_c6m5 = harm_avg_2048_les_c6m5[:,1]**2 + harm_avg_2048_les_c6m5[:,2]**2
harm_avg_2048_les_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c7m5.dat")
t_harm_avg_2048_les_c7m5 = harm_avg_2048_les_c7m5[:,0]
v_harm_avg_2048_les_c7m5 = harm_avg_2048_les_c7m5[:,1]**2 + harm_avg_2048_les_c7m5[:,2]**2
harm_avg_2048_les_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg[ 4.  5.  6.  7.  8.  9. 10. 11. 12.]/s0_c8m5.dat")
t_harm_avg_2048_les_c8m5 = harm_avg_2048_les_c8m5[:,0]
v_harm_avg_2048_les_c8m5 = harm_avg_2048_les_c8m5[:,1]**2 + harm_avg_2048_les_c8m5[:,2]**2

harm_avg_2048_les_2L_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c00.dat")
t_harm_avg_2048_les_2L_c00 = harm_avg_2048_les_2L_c00[:,0]
v_harm_avg_2048_les_2L_c00 = harm_avg_2048_les_2L_c00[:,1]**2 + harm_avg_2048_les_2L_c00[:,2]**2
harm_avg_2048_les_2L_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c10.dat")
t_harm_avg_2048_les_2L_c10 = harm_avg_2048_les_2L_c10[:,0]
v_harm_avg_2048_les_2L_c10 = harm_avg_2048_les_2L_c10[:,1]**2 + harm_avg_2048_les_2L_c10[:,2]**2
harm_avg_2048_les_2L_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c20.dat")
t_harm_avg_2048_les_2L_c20 = harm_avg_2048_les_2L_c20[:,0]
v_harm_avg_2048_les_2L_c20 = harm_avg_2048_les_2L_c20[:,1]**2 + harm_avg_2048_les_2L_c20[:,2]**2
harm_avg_2048_les_2L_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c30.dat")
t_harm_avg_2048_les_2L_c30 = harm_avg_2048_les_2L_c30[:,0]
v_harm_avg_2048_les_2L_c30 = harm_avg_2048_les_2L_c30[:,1]**2 + harm_avg_2048_les_2L_c30[:,2]**2
harm_avg_2048_les_2L_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c40.dat")
t_harm_avg_2048_les_2L_c40 = harm_avg_2048_les_2L_c40[:,0]
v_harm_avg_2048_les_2L_c40 = harm_avg_2048_les_2L_c40[:,1]**2 + harm_avg_2048_les_2L_c40[:,2]**2
harm_avg_2048_les_2L_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c50.dat")
t_harm_avg_2048_les_2L_c50 = harm_avg_2048_les_2L_c50[:,0]
v_harm_avg_2048_les_2L_c50 = harm_avg_2048_les_2L_c50[:,1]**2 + harm_avg_2048_les_2L_c50[:,2]**2
harm_avg_2048_les_2L_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c60.dat")
t_harm_avg_2048_les_2L_c60 = harm_avg_2048_les_2L_c60[:,0]
v_harm_avg_2048_les_2L_c60 = harm_avg_2048_les_2L_c60[:,1]**2 + harm_avg_2048_les_2L_c60[:,2]**2
harm_avg_2048_les_2L_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c70.dat")
t_harm_avg_2048_les_2L_c70 = harm_avg_2048_les_2L_c70[:,0]
v_harm_avg_2048_les_2L_c70 = harm_avg_2048_les_2L_c70[:,1]**2 + harm_avg_2048_les_2L_c70[:,2]**2
harm_avg_2048_les_2L_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c80.dat")
t_harm_avg_2048_les_2L_c80 = harm_avg_2048_les_2L_c80[:,0]
v_harm_avg_2048_les_2L_c80 = harm_avg_2048_les_2L_c80[:,1]**2 + harm_avg_2048_les_2L_c80[:,2]**2

harm_avg_2048_les_2L_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c1p1.dat")
t_harm_avg_2048_les_2L_c1p1 = harm_avg_2048_les_2L_c1p1[:,0]
v_harm_avg_2048_les_2L_c1p1 = harm_avg_2048_les_2L_c1p1[:,1]**2 + harm_avg_2048_les_2L_c1p1[:,2]**2
harm_avg_2048_les_2L_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2p1.dat")
t_harm_avg_2048_les_2L_c2p1 = harm_avg_2048_les_2L_c2p1[:,0]
v_harm_avg_2048_les_2L_c2p1 = harm_avg_2048_les_2L_c2p1[:,1]**2 + harm_avg_2048_les_2L_c2p1[:,2]**2
harm_avg_2048_les_2L_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p1.dat")
t_harm_avg_2048_les_2L_c3p1 = harm_avg_2048_les_2L_c3p1[:,0]
v_harm_avg_2048_les_2L_c3p1 = harm_avg_2048_les_2L_c3p1[:,1]**2 + harm_avg_2048_les_2L_c3p1[:,2]**2
harm_avg_2048_les_2L_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p1.dat")
t_harm_avg_2048_les_2L_c4p1 = harm_avg_2048_les_2L_c4p1[:,0]
v_harm_avg_2048_les_2L_c4p1 = harm_avg_2048_les_2L_c4p1[:,1]**2 + harm_avg_2048_les_2L_c4p1[:,2]**2
harm_avg_2048_les_2L_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p1.dat")
t_harm_avg_2048_les_2L_c5p1 = harm_avg_2048_les_2L_c5p1[:,0]
v_harm_avg_2048_les_2L_c5p1 = harm_avg_2048_les_2L_c5p1[:,1]**2 + harm_avg_2048_les_2L_c5p1[:,2]**2
harm_avg_2048_les_2L_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p1.dat")
t_harm_avg_2048_les_2L_c6p1 = harm_avg_2048_les_2L_c6p1[:,0]
v_harm_avg_2048_les_2L_c6p1 = harm_avg_2048_les_2L_c6p1[:,1]**2 + harm_avg_2048_les_2L_c6p1[:,2]**2
harm_avg_2048_les_2L_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p1.dat")
t_harm_avg_2048_les_2L_c7p1 = harm_avg_2048_les_2L_c7p1[:,0]
v_harm_avg_2048_les_2L_c7p1 = harm_avg_2048_les_2L_c7p1[:,1]**2 + harm_avg_2048_les_2L_c7p1[:,2]**2
harm_avg_2048_les_2L_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p1.dat")
t_harm_avg_2048_les_2L_c8p1 = harm_avg_2048_les_2L_c8p1[:,0]
v_harm_avg_2048_les_2L_c8p1 = harm_avg_2048_les_2L_c8p1[:,1]**2 + harm_avg_2048_les_2L_c8p1[:,2]**2

harm_avg_2048_les_2L_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c1m1.dat")
t_harm_avg_2048_les_2L_c1m1 = harm_avg_2048_les_2L_c1m1[:,0]
v_harm_avg_2048_les_2L_c1m1 = harm_avg_2048_les_2L_c1m1[:,1]**2 + harm_avg_2048_les_2L_c1m1[:,2]**2
harm_avg_2048_les_2L_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2m1.dat")
t_harm_avg_2048_les_2L_c2m1 = harm_avg_2048_les_2L_c2m1[:,0]
v_harm_avg_2048_les_2L_c2m1 = harm_avg_2048_les_2L_c2m1[:,1]**2 + harm_avg_2048_les_2L_c2m1[:,2]**2
harm_avg_2048_les_2L_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m1.dat")
t_harm_avg_2048_les_2L_c3m1 = harm_avg_2048_les_2L_c3m1[:,0]
v_harm_avg_2048_les_2L_c3m1 = harm_avg_2048_les_2L_c3m1[:,1]**2 + harm_avg_2048_les_2L_c3m1[:,2]**2
harm_avg_2048_les_2L_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m1.dat")
t_harm_avg_2048_les_2L_c4m1 = harm_avg_2048_les_2L_c4m1[:,0]
v_harm_avg_2048_les_2L_c4m1 = harm_avg_2048_les_2L_c4m1[:,1]**2 + harm_avg_2048_les_2L_c4m1[:,2]**2
harm_avg_2048_les_2L_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m1.dat")
t_harm_avg_2048_les_2L_c5m1 = harm_avg_2048_les_2L_c5m1[:,0]
v_harm_avg_2048_les_2L_c5m1 = harm_avg_2048_les_2L_c5m1[:,1]**2 + harm_avg_2048_les_2L_c5m1[:,2]**2
harm_avg_2048_les_2L_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m1.dat")
t_harm_avg_2048_les_2L_c6m1 = harm_avg_2048_les_2L_c6m1[:,0]
v_harm_avg_2048_les_2L_c6m1 = harm_avg_2048_les_2L_c6m1[:,1]**2 + harm_avg_2048_les_2L_c6m1[:,2]**2
harm_avg_2048_les_2L_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m1.dat")
t_harm_avg_2048_les_2L_c7m1 = harm_avg_2048_les_2L_c7m1[:,0]
v_harm_avg_2048_les_2L_c7m1 = harm_avg_2048_les_2L_c7m1[:,1]**2 + harm_avg_2048_les_2L_c7m1[:,2]**2
harm_avg_2048_les_2L_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m1.dat")
t_harm_avg_2048_les_2L_c8m1 = harm_avg_2048_les_2L_c8m1[:,0]
v_harm_avg_2048_les_2L_c8m1 = harm_avg_2048_les_2L_c8m1[:,1]**2 + harm_avg_2048_les_2L_c8m1[:,2]**2

harm_avg_2048_les_2L_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2p2.dat")
t_harm_avg_2048_les_2L_c2p2 = harm_avg_2048_les_2L_c2p2[:,0]
v_harm_avg_2048_les_2L_c2p2 = harm_avg_2048_les_2L_c2p2[:,1]**2 + harm_avg_2048_les_2L_c2p2[:,2]**2
harm_avg_2048_les_2L_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p2.dat")
t_harm_avg_2048_les_2L_c3p2 = harm_avg_2048_les_2L_c3p2[:,0]
v_harm_avg_2048_les_2L_c3p2 = harm_avg_2048_les_2L_c3p2[:,1]**2 + harm_avg_2048_les_2L_c3p2[:,2]**2
harm_avg_2048_les_2L_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p2.dat")
t_harm_avg_2048_les_2L_c4p2 = harm_avg_2048_les_2L_c4p2[:,0]
v_harm_avg_2048_les_2L_c4p2 = harm_avg_2048_les_2L_c4p2[:,1]**2 + harm_avg_2048_les_2L_c4p2[:,2]**2
harm_avg_2048_les_2L_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p2.dat")
t_harm_avg_2048_les_2L_c5p2 = harm_avg_2048_les_2L_c5p2[:,0]
v_harm_avg_2048_les_2L_c5p2 = harm_avg_2048_les_2L_c5p2[:,1]**2 + harm_avg_2048_les_2L_c5p2[:,2]**2
harm_avg_2048_les_2L_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p2.dat")
t_harm_avg_2048_les_2L_c6p2 = harm_avg_2048_les_2L_c6p2[:,0]
v_harm_avg_2048_les_2L_c6p2 = harm_avg_2048_les_2L_c6p2[:,1]**2 + harm_avg_2048_les_2L_c6p2[:,2]**2
harm_avg_2048_les_2L_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p2.dat")
t_harm_avg_2048_les_2L_c7p2 = harm_avg_2048_les_2L_c7p2[:,0]
v_harm_avg_2048_les_2L_c7p2 = harm_avg_2048_les_2L_c7p2[:,1]**2 + harm_avg_2048_les_2L_c7p2[:,2]**2
harm_avg_2048_les_2L_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p2.dat")
t_harm_avg_2048_les_2L_c8p2 = harm_avg_2048_les_2L_c8p2[:,0]
v_harm_avg_2048_les_2L_c8p2 = harm_avg_2048_les_2L_c8p2[:,1]**2 + harm_avg_2048_les_2L_c8p2[:,2]**2

harm_avg_2048_les_2L_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c2m2.dat")
t_harm_avg_2048_les_2L_c2m2 = harm_avg_2048_les_2L_c2m2[:,0]
v_harm_avg_2048_les_2L_c2m2 = harm_avg_2048_les_2L_c2m2[:,1]**2 + harm_avg_2048_les_2L_c2m2[:,2]**2
harm_avg_2048_les_2L_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m2.dat")
t_harm_avg_2048_les_2L_c3m2 = harm_avg_2048_les_2L_c3m2[:,0]
v_harm_avg_2048_les_2L_c3m2 = harm_avg_2048_les_2L_c3m2[:,1]**2 + harm_avg_2048_les_2L_c3m2[:,2]**2
harm_avg_2048_les_2L_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m2.dat")
t_harm_avg_2048_les_2L_c4m2 = harm_avg_2048_les_2L_c4m2[:,0]
v_harm_avg_2048_les_2L_c4m2 = harm_avg_2048_les_2L_c4m2[:,1]**2 + harm_avg_2048_les_2L_c4m2[:,2]**2
harm_avg_2048_les_2L_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m2.dat")
t_harm_avg_2048_les_2L_c5m2 = harm_avg_2048_les_2L_c5m2[:,0]
v_harm_avg_2048_les_2L_c5m2 = harm_avg_2048_les_2L_c5m2[:,1]**2 + harm_avg_2048_les_2L_c5m2[:,2]**2
harm_avg_2048_les_2L_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m2.dat")
t_harm_avg_2048_les_2L_c6m2 = harm_avg_2048_les_2L_c6m2[:,0]
v_harm_avg_2048_les_2L_c6m2 = harm_avg_2048_les_2L_c6m2[:,1]**2 + harm_avg_2048_les_2L_c6m2[:,2]**2
harm_avg_2048_les_2L_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m2.dat")
t_harm_avg_2048_les_2L_c7m2 = harm_avg_2048_les_2L_c7m2[:,0]
v_harm_avg_2048_les_2L_c7m2 = harm_avg_2048_les_2L_c7m2[:,1]**2 + harm_avg_2048_les_2L_c7m2[:,2]**2
harm_avg_2048_les_2L_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m2.dat")
t_harm_avg_2048_les_2L_c8m2 = harm_avg_2048_les_2L_c8m2[:,0]
v_harm_avg_2048_les_2L_c8m2 = harm_avg_2048_les_2L_c8m2[:,1]**2 + harm_avg_2048_les_2L_c8m2[:,2]**2

harm_avg_2048_les_2L_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3p3.dat")
t_harm_avg_2048_les_2L_c3p3 = harm_avg_2048_les_2L_c3p3[:,0]
v_harm_avg_2048_les_2L_c3p3 = harm_avg_2048_les_2L_c3p3[:,1]**2 + harm_avg_2048_les_2L_c3p3[:,2]**2
harm_avg_2048_les_2L_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p3.dat")
t_harm_avg_2048_les_2L_c4p3 = harm_avg_2048_les_2L_c4p3[:,0]
v_harm_avg_2048_les_2L_c4p3 = harm_avg_2048_les_2L_c4p3[:,1]**2 + harm_avg_2048_les_2L_c4p3[:,2]**2
harm_avg_2048_les_2L_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p3.dat")
t_harm_avg_2048_les_2L_c5p3 = harm_avg_2048_les_2L_c5p3[:,0]
v_harm_avg_2048_les_2L_c5p3 = harm_avg_2048_les_2L_c5p3[:,1]**2 + harm_avg_2048_les_2L_c5p3[:,2]**2
harm_avg_2048_les_2L_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p3.dat")
t_harm_avg_2048_les_2L_c6p3 = harm_avg_2048_les_2L_c6p3[:,0]
v_harm_avg_2048_les_2L_c6p3 = harm_avg_2048_les_2L_c6p3[:,1]**2 + harm_avg_2048_les_2L_c6p3[:,2]**2
harm_avg_2048_les_2L_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p3.dat")
t_harm_avg_2048_les_2L_c7p3 = harm_avg_2048_les_2L_c7p3[:,0]
v_harm_avg_2048_les_2L_c7p3 = harm_avg_2048_les_2L_c7p3[:,1]**2 + harm_avg_2048_les_2L_c7p3[:,2]**2
harm_avg_2048_les_2L_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p3.dat")
t_harm_avg_2048_les_2L_c8p3 = harm_avg_2048_les_2L_c8p3[:,0]
v_harm_avg_2048_les_2L_c8p3 = harm_avg_2048_les_2L_c8p3[:,1]**2 + harm_avg_2048_les_2L_c8p3[:,2]**2

harm_avg_2048_les_2L_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c3m3.dat")
t_harm_avg_2048_les_2L_c3m3 = harm_avg_2048_les_2L_c3m3[:,0]
v_harm_avg_2048_les_2L_c3m3 = harm_avg_2048_les_2L_c3m3[:,1]**2 + harm_avg_2048_les_2L_c3m3[:,2]**2
harm_avg_2048_les_2L_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m3.dat")
t_harm_avg_2048_les_2L_c4m3 = harm_avg_2048_les_2L_c4m3[:,0]
v_harm_avg_2048_les_2L_c4m3 = harm_avg_2048_les_2L_c4m3[:,1]**2 + harm_avg_2048_les_2L_c4m3[:,2]**2
harm_avg_2048_les_2L_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m3.dat")
t_harm_avg_2048_les_2L_c5m3 = harm_avg_2048_les_2L_c5m3[:,0]
v_harm_avg_2048_les_2L_c5m3 = harm_avg_2048_les_2L_c5m3[:,1]**2 + harm_avg_2048_les_2L_c5m3[:,2]**2
harm_avg_2048_les_2L_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m3.dat")
t_harm_avg_2048_les_2L_c6m3 = harm_avg_2048_les_2L_c6m3[:,0]
v_harm_avg_2048_les_2L_c6m3 = harm_avg_2048_les_2L_c6m3[:,1]**2 + harm_avg_2048_les_2L_c6m3[:,2]**2
harm_avg_2048_les_2L_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m3.dat")
t_harm_avg_2048_les_2L_c7m3 = harm_avg_2048_les_2L_c7m3[:,0]
v_harm_avg_2048_les_2L_c7m3 = harm_avg_2048_les_2L_c7m3[:,1]**2 + harm_avg_2048_les_2L_c7m3[:,2]**2
harm_avg_2048_les_2L_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m3.dat")
t_harm_avg_2048_les_2L_c8m3 = harm_avg_2048_les_2L_c8m3[:,0]
v_harm_avg_2048_les_2L_c8m3 = harm_avg_2048_les_2L_c8m3[:,1]**2 + harm_avg_2048_les_2L_c8m3[:,2]**2

harm_avg_2048_les_2L_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4p4.dat")
t_harm_avg_2048_les_2L_c4p4 = harm_avg_2048_les_2L_c4p4[:,0]
v_harm_avg_2048_les_2L_c4p4 = harm_avg_2048_les_2L_c4p4[:,1]**2 + harm_avg_2048_les_2L_c4p4[:,2]**2
harm_avg_2048_les_2L_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p4.dat")
t_harm_avg_2048_les_2L_c5p4 = harm_avg_2048_les_2L_c5p4[:,0]
v_harm_avg_2048_les_2L_c5p4 = harm_avg_2048_les_2L_c5p4[:,1]**2 + harm_avg_2048_les_2L_c5p4[:,2]**2
harm_avg_2048_les_2L_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p4.dat")
t_harm_avg_2048_les_2L_c6p4 = harm_avg_2048_les_2L_c6p4[:,0]
v_harm_avg_2048_les_2L_c6p4 = harm_avg_2048_les_2L_c6p4[:,1]**2 + harm_avg_2048_les_2L_c6p4[:,2]**2
harm_avg_2048_les_2L_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p4.dat")
t_harm_avg_2048_les_2L_c7p4 = harm_avg_2048_les_2L_c7p4[:,0]
v_harm_avg_2048_les_2L_c7p4 = harm_avg_2048_les_2L_c7p4[:,1]**2 + harm_avg_2048_les_2L_c7p4[:,2]**2
harm_avg_2048_les_2L_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p4.dat")
t_harm_avg_2048_les_2L_c8p4 = harm_avg_2048_les_2L_c8p4[:,0]
v_harm_avg_2048_les_2L_c8p4 = harm_avg_2048_les_2L_c8p4[:,1]**2 + harm_avg_2048_les_2L_c8p4[:,2]**2

harm_avg_2048_les_2L_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c4m4.dat")
t_harm_avg_2048_les_2L_c4m4 = harm_avg_2048_les_2L_c4m4[:,0]
v_harm_avg_2048_les_2L_c4m4 = harm_avg_2048_les_2L_c4m4[:,1]**2 + harm_avg_2048_les_2L_c4m4[:,2]**2
harm_avg_2048_les_2L_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m4.dat")
t_harm_avg_2048_les_2L_c5m4 = harm_avg_2048_les_2L_c5m4[:,0]
v_harm_avg_2048_les_2L_c5m4 = harm_avg_2048_les_2L_c5m4[:,1]**2 + harm_avg_2048_les_2L_c5m4[:,2]**2
harm_avg_2048_les_2L_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m4.dat")
t_harm_avg_2048_les_2L_c6m4 = harm_avg_2048_les_2L_c6m4[:,0]
v_harm_avg_2048_les_2L_c6m4 = harm_avg_2048_les_2L_c6m4[:,1]**2 + harm_avg_2048_les_2L_c6m4[:,2]**2
harm_avg_2048_les_2L_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m4.dat")
t_harm_avg_2048_les_2L_c7m4 = harm_avg_2048_les_2L_c7m4[:,0]
v_harm_avg_2048_les_2L_c7m4 = harm_avg_2048_les_2L_c7m4[:,1]**2 + harm_avg_2048_les_2L_c7m4[:,2]**2
harm_avg_2048_les_2L_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m4.dat")
t_harm_avg_2048_les_2L_c8m4 = harm_avg_2048_les_2L_c8m4[:,0]
v_harm_avg_2048_les_2L_c8m4 = harm_avg_2048_les_2L_c8m4[:,1]**2 + harm_avg_2048_les_2L_c8m4[:,2]**2

harm_avg_2048_les_2L_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5p5.dat")
t_harm_avg_2048_les_2L_c5p5 = harm_avg_2048_les_2L_c5p5[:,0]
v_harm_avg_2048_les_2L_c5p5 = harm_avg_2048_les_2L_c5p5[:,1]**2 + harm_avg_2048_les_2L_c5p5[:,2]**2
harm_avg_2048_les_2L_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6p5.dat")
t_harm_avg_2048_les_2L_c6p5 = harm_avg_2048_les_2L_c6p5[:,0]
v_harm_avg_2048_les_2L_c6p5 = harm_avg_2048_les_2L_c6p5[:,1]**2 + harm_avg_2048_les_2L_c6p5[:,2]**2
harm_avg_2048_les_2L_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7p5.dat")
t_harm_avg_2048_les_2L_c7p5 = harm_avg_2048_les_2L_c7p5[:,0]
v_harm_avg_2048_les_2L_c7p5 = harm_avg_2048_les_2L_c7p5[:,1]**2 + harm_avg_2048_les_2L_c7p5[:,2]**2
harm_avg_2048_les_2L_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8p5.dat")
t_harm_avg_2048_les_2L_c8p5 = harm_avg_2048_les_2L_c8p5[:,0]
v_harm_avg_2048_les_2L_c8p5 = harm_avg_2048_les_2L_c8p5[:,1]**2 + harm_avg_2048_les_2L_c8p5[:,2]**2

harm_avg_2048_les_2L_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c5m5.dat")
t_harm_avg_2048_les_2L_c5m5 = harm_avg_2048_les_2L_c5m5[:,0]
v_harm_avg_2048_les_2L_c5m5 = harm_avg_2048_les_2L_c5m5[:,1]**2 + harm_avg_2048_les_2L_c5m5[:,2]**2
harm_avg_2048_les_2L_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c6m5.dat")
t_harm_avg_2048_les_2L_c6m5 = harm_avg_2048_les_2L_c6m5[:,0]
v_harm_avg_2048_les_2L_c6m5 = harm_avg_2048_les_2L_c6m5[:,1]**2 + harm_avg_2048_les_2L_c6m5[:,2]**2
harm_avg_2048_les_2L_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c7m5.dat")
t_harm_avg_2048_les_2L_c7m5 = harm_avg_2048_les_2L_c7m5[:,0]
v_harm_avg_2048_les_2L_c7m5 = harm_avg_2048_les_2L_c7m5[:,1]**2 + harm_avg_2048_les_2L_c7m5[:,2]**2
harm_avg_2048_les_2L_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_2..24..1/s0_c8m5.dat")
t_harm_avg_2048_les_2L_c8m5 = harm_avg_2048_les_2L_c8m5[:,0]
v_harm_avg_2048_les_2L_c8m5 = harm_avg_2048_les_2L_c8m5[:,1]**2 + harm_avg_2048_les_2L_c8m5[:,2]**2

harm_avg_2048_les_2dr_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c00.dat")
t_harm_avg_2048_les_2dr_c00 = harm_avg_2048_les_2dr_c00[:,0]
v_harm_avg_2048_les_2dr_c00 = harm_avg_2048_les_2dr_c00[:,1]**2 + harm_avg_2048_les_2dr_c00[:,2]**2
harm_avg_2048_les_2dr_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c10.dat")
t_harm_avg_2048_les_2dr_c10 = harm_avg_2048_les_2dr_c10[:,0]
v_harm_avg_2048_les_2dr_c10 = harm_avg_2048_les_2dr_c10[:,1]**2 + harm_avg_2048_les_2dr_c10[:,2]**2
harm_avg_2048_les_2dr_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c20.dat")
t_harm_avg_2048_les_2dr_c20 = harm_avg_2048_les_2dr_c20[:,0]
v_harm_avg_2048_les_2dr_c20 = harm_avg_2048_les_2dr_c20[:,1]**2 + harm_avg_2048_les_2dr_c20[:,2]**2
harm_avg_2048_les_2dr_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c30.dat")
t_harm_avg_2048_les_2dr_c30 = harm_avg_2048_les_2dr_c30[:,0]
v_harm_avg_2048_les_2dr_c30 = harm_avg_2048_les_2dr_c30[:,1]**2 + harm_avg_2048_les_2dr_c30[:,2]**2
harm_avg_2048_les_2dr_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c40.dat")
t_harm_avg_2048_les_2dr_c40 = harm_avg_2048_les_2dr_c40[:,0]
v_harm_avg_2048_les_2dr_c40 = harm_avg_2048_les_2dr_c40[:,1]**2 + harm_avg_2048_les_2dr_c40[:,2]**2
harm_avg_2048_les_2dr_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c50.dat")
t_harm_avg_2048_les_2dr_c50 = harm_avg_2048_les_2dr_c50[:,0]
v_harm_avg_2048_les_2dr_c50 = harm_avg_2048_les_2dr_c50[:,1]**2 + harm_avg_2048_les_2dr_c50[:,2]**2
harm_avg_2048_les_2dr_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c60.dat")
t_harm_avg_2048_les_2dr_c60 = harm_avg_2048_les_2dr_c60[:,0]
v_harm_avg_2048_les_2dr_c60 = harm_avg_2048_les_2dr_c60[:,1]**2 + harm_avg_2048_les_2dr_c60[:,2]**2
harm_avg_2048_les_2dr_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c70.dat")
t_harm_avg_2048_les_2dr_c70 = harm_avg_2048_les_2dr_c70[:,0]
v_harm_avg_2048_les_2dr_c70 = harm_avg_2048_les_2dr_c70[:,1]**2 + harm_avg_2048_les_2dr_c70[:,2]**2
harm_avg_2048_les_2dr_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c80.dat")
t_harm_avg_2048_les_2dr_c80 = harm_avg_2048_les_2dr_c80[:,0]
v_harm_avg_2048_les_2dr_c80 = harm_avg_2048_les_2dr_c80[:,1]**2 + harm_avg_2048_les_2dr_c80[:,2]**2

harm_avg_2048_les_2dr_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_les_2dr_c1p1 = harm_avg_2048_les_2dr_c1p1[:,0]
v_harm_avg_2048_les_2dr_c1p1 = harm_avg_2048_les_2dr_c1p1[:,1]**2 + harm_avg_2048_les_2dr_c1p1[:,2]**2
harm_avg_2048_les_2dr_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_les_2dr_c2p1 = harm_avg_2048_les_2dr_c2p1[:,0]
v_harm_avg_2048_les_2dr_c2p1 = harm_avg_2048_les_2dr_c2p1[:,1]**2 + harm_avg_2048_les_2dr_c2p1[:,2]**2
harm_avg_2048_les_2dr_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_les_2dr_c3p1 = harm_avg_2048_les_2dr_c3p1[:,0]
v_harm_avg_2048_les_2dr_c3p1 = harm_avg_2048_les_2dr_c3p1[:,1]**2 + harm_avg_2048_les_2dr_c3p1[:,2]**2
harm_avg_2048_les_2dr_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_les_2dr_c4p1 = harm_avg_2048_les_2dr_c4p1[:,0]
v_harm_avg_2048_les_2dr_c4p1 = harm_avg_2048_les_2dr_c4p1[:,1]**2 + harm_avg_2048_les_2dr_c4p1[:,2]**2
harm_avg_2048_les_2dr_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_les_2dr_c5p1 = harm_avg_2048_les_2dr_c5p1[:,0]
v_harm_avg_2048_les_2dr_c5p1 = harm_avg_2048_les_2dr_c5p1[:,1]**2 + harm_avg_2048_les_2dr_c5p1[:,2]**2
harm_avg_2048_les_2dr_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_les_2dr_c6p1 = harm_avg_2048_les_2dr_c6p1[:,0]
v_harm_avg_2048_les_2dr_c6p1 = harm_avg_2048_les_2dr_c6p1[:,1]**2 + harm_avg_2048_les_2dr_c6p1[:,2]**2
harm_avg_2048_les_2dr_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_les_2dr_c7p1 = harm_avg_2048_les_2dr_c7p1[:,0]
v_harm_avg_2048_les_2dr_c7p1 = harm_avg_2048_les_2dr_c7p1[:,1]**2 + harm_avg_2048_les_2dr_c7p1[:,2]**2
harm_avg_2048_les_2dr_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_les_2dr_c8p1 = harm_avg_2048_les_2dr_c8p1[:,0]
v_harm_avg_2048_les_2dr_c8p1 = harm_avg_2048_les_2dr_c8p1[:,1]**2 + harm_avg_2048_les_2dr_c8p1[:,2]**2

harm_avg_2048_les_2dr_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_les_2dr_c1m1 = harm_avg_2048_les_2dr_c1m1[:,0]
v_harm_avg_2048_les_2dr_c1m1 = harm_avg_2048_les_2dr_c1m1[:,1]**2 + harm_avg_2048_les_2dr_c1m1[:,2]**2
harm_avg_2048_les_2dr_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_les_2dr_c2m1 = harm_avg_2048_les_2dr_c2m1[:,0]
v_harm_avg_2048_les_2dr_c2m1 = harm_avg_2048_les_2dr_c2m1[:,1]**2 + harm_avg_2048_les_2dr_c2m1[:,2]**2
harm_avg_2048_les_2dr_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_les_2dr_c3m1 = harm_avg_2048_les_2dr_c3m1[:,0]
v_harm_avg_2048_les_2dr_c3m1 = harm_avg_2048_les_2dr_c3m1[:,1]**2 + harm_avg_2048_les_2dr_c3m1[:,2]**2
harm_avg_2048_les_2dr_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_les_2dr_c4m1 = harm_avg_2048_les_2dr_c4m1[:,0]
v_harm_avg_2048_les_2dr_c4m1 = harm_avg_2048_les_2dr_c4m1[:,1]**2 + harm_avg_2048_les_2dr_c4m1[:,2]**2
harm_avg_2048_les_2dr_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_les_2dr_c5m1 = harm_avg_2048_les_2dr_c5m1[:,0]
v_harm_avg_2048_les_2dr_c5m1 = harm_avg_2048_les_2dr_c5m1[:,1]**2 + harm_avg_2048_les_2dr_c5m1[:,2]**2
harm_avg_2048_les_2dr_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_les_2dr_c6m1 = harm_avg_2048_les_2dr_c6m1[:,0]
v_harm_avg_2048_les_2dr_c6m1 = harm_avg_2048_les_2dr_c6m1[:,1]**2 + harm_avg_2048_les_2dr_c6m1[:,2]**2
harm_avg_2048_les_2dr_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_les_2dr_c7m1 = harm_avg_2048_les_2dr_c7m1[:,0]
v_harm_avg_2048_les_2dr_c7m1 = harm_avg_2048_les_2dr_c7m1[:,1]**2 + harm_avg_2048_les_2dr_c7m1[:,2]**2
harm_avg_2048_les_2dr_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_les_2dr_c8m1 = harm_avg_2048_les_2dr_c8m1[:,0]
v_harm_avg_2048_les_2dr_c8m1 = harm_avg_2048_les_2dr_c8m1[:,1]**2 + harm_avg_2048_les_2dr_c8m1[:,2]**2

harm_avg_2048_les_2dr_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_les_2dr_c2p2 = harm_avg_2048_les_2dr_c2p2[:,0]
v_harm_avg_2048_les_2dr_c2p2 = harm_avg_2048_les_2dr_c2p2[:,1]**2 + harm_avg_2048_les_2dr_c2p2[:,2]**2
harm_avg_2048_les_2dr_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_les_2dr_c3p2 = harm_avg_2048_les_2dr_c3p2[:,0]
v_harm_avg_2048_les_2dr_c3p2 = harm_avg_2048_les_2dr_c3p2[:,1]**2 + harm_avg_2048_les_2dr_c3p2[:,2]**2
harm_avg_2048_les_2dr_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_les_2dr_c4p2 = harm_avg_2048_les_2dr_c4p2[:,0]
v_harm_avg_2048_les_2dr_c4p2 = harm_avg_2048_les_2dr_c4p2[:,1]**2 + harm_avg_2048_les_2dr_c4p2[:,2]**2
harm_avg_2048_les_2dr_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_les_2dr_c5p2 = harm_avg_2048_les_2dr_c5p2[:,0]
v_harm_avg_2048_les_2dr_c5p2 = harm_avg_2048_les_2dr_c5p2[:,1]**2 + harm_avg_2048_les_2dr_c5p2[:,2]**2
harm_avg_2048_les_2dr_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_les_2dr_c6p2 = harm_avg_2048_les_2dr_c6p2[:,0]
v_harm_avg_2048_les_2dr_c6p2 = harm_avg_2048_les_2dr_c6p2[:,1]**2 + harm_avg_2048_les_2dr_c6p2[:,2]**2
harm_avg_2048_les_2dr_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_les_2dr_c7p2 = harm_avg_2048_les_2dr_c7p2[:,0]
v_harm_avg_2048_les_2dr_c7p2 = harm_avg_2048_les_2dr_c7p2[:,1]**2 + harm_avg_2048_les_2dr_c7p2[:,2]**2
harm_avg_2048_les_2dr_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_les_2dr_c8p2 = harm_avg_2048_les_2dr_c8p2[:,0]
v_harm_avg_2048_les_2dr_c8p2 = harm_avg_2048_les_2dr_c8p2[:,1]**2 + harm_avg_2048_les_2dr_c8p2[:,2]**2

harm_avg_2048_les_2dr_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_les_2dr_c2m2 = harm_avg_2048_les_2dr_c2m2[:,0]
v_harm_avg_2048_les_2dr_c2m2 = harm_avg_2048_les_2dr_c2m2[:,1]**2 + harm_avg_2048_les_2dr_c2m2[:,2]**2
harm_avg_2048_les_2dr_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_les_2dr_c3m2 = harm_avg_2048_les_2dr_c3m2[:,0]
v_harm_avg_2048_les_2dr_c3m2 = harm_avg_2048_les_2dr_c3m2[:,1]**2 + harm_avg_2048_les_2dr_c3m2[:,2]**2
harm_avg_2048_les_2dr_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_les_2dr_c4m2 = harm_avg_2048_les_2dr_c4m2[:,0]
v_harm_avg_2048_les_2dr_c4m2 = harm_avg_2048_les_2dr_c4m2[:,1]**2 + harm_avg_2048_les_2dr_c4m2[:,2]**2
harm_avg_2048_les_2dr_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_les_2dr_c5m2 = harm_avg_2048_les_2dr_c5m2[:,0]
v_harm_avg_2048_les_2dr_c5m2 = harm_avg_2048_les_2dr_c5m2[:,1]**2 + harm_avg_2048_les_2dr_c5m2[:,2]**2
harm_avg_2048_les_2dr_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_les_2dr_c6m2 = harm_avg_2048_les_2dr_c6m2[:,0]
v_harm_avg_2048_les_2dr_c6m2 = harm_avg_2048_les_2dr_c6m2[:,1]**2 + harm_avg_2048_les_2dr_c6m2[:,2]**2
harm_avg_2048_les_2dr_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_les_2dr_c7m2 = harm_avg_2048_les_2dr_c7m2[:,0]
v_harm_avg_2048_les_2dr_c7m2 = harm_avg_2048_les_2dr_c7m2[:,1]**2 + harm_avg_2048_les_2dr_c7m2[:,2]**2
harm_avg_2048_les_2dr_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_les_2dr_c8m2 = harm_avg_2048_les_2dr_c8m2[:,0]
v_harm_avg_2048_les_2dr_c8m2 = harm_avg_2048_les_2dr_c8m2[:,1]**2 + harm_avg_2048_les_2dr_c8m2[:,2]**2

harm_avg_2048_les_2dr_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_les_2dr_c3p3 = harm_avg_2048_les_2dr_c3p3[:,0]
v_harm_avg_2048_les_2dr_c3p3 = harm_avg_2048_les_2dr_c3p3[:,1]**2 + harm_avg_2048_les_2dr_c3p3[:,2]**2
harm_avg_2048_les_2dr_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_les_2dr_c4p3 = harm_avg_2048_les_2dr_c4p3[:,0]
v_harm_avg_2048_les_2dr_c4p3 = harm_avg_2048_les_2dr_c4p3[:,1]**2 + harm_avg_2048_les_2dr_c4p3[:,2]**2
harm_avg_2048_les_2dr_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_les_2dr_c5p3 = harm_avg_2048_les_2dr_c5p3[:,0]
v_harm_avg_2048_les_2dr_c5p3 = harm_avg_2048_les_2dr_c5p3[:,1]**2 + harm_avg_2048_les_2dr_c5p3[:,2]**2
harm_avg_2048_les_2dr_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_les_2dr_c6p3 = harm_avg_2048_les_2dr_c6p3[:,0]
v_harm_avg_2048_les_2dr_c6p3 = harm_avg_2048_les_2dr_c6p3[:,1]**2 + harm_avg_2048_les_2dr_c6p3[:,2]**2
harm_avg_2048_les_2dr_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_les_2dr_c7p3 = harm_avg_2048_les_2dr_c7p3[:,0]
v_harm_avg_2048_les_2dr_c7p3 = harm_avg_2048_les_2dr_c7p3[:,1]**2 + harm_avg_2048_les_2dr_c7p3[:,2]**2
harm_avg_2048_les_2dr_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_les_2dr_c8p3 = harm_avg_2048_les_2dr_c8p3[:,0]
v_harm_avg_2048_les_2dr_c8p3 = harm_avg_2048_les_2dr_c8p3[:,1]**2 + harm_avg_2048_les_2dr_c8p3[:,2]**2

harm_avg_2048_les_2dr_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_les_2dr_c3m3 = harm_avg_2048_les_2dr_c3m3[:,0]
v_harm_avg_2048_les_2dr_c3m3 = harm_avg_2048_les_2dr_c3m3[:,1]**2 + harm_avg_2048_les_2dr_c3m3[:,2]**2
harm_avg_2048_les_2dr_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_les_2dr_c4m3 = harm_avg_2048_les_2dr_c4m3[:,0]
v_harm_avg_2048_les_2dr_c4m3 = harm_avg_2048_les_2dr_c4m3[:,1]**2 + harm_avg_2048_les_2dr_c4m3[:,2]**2
harm_avg_2048_les_2dr_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_les_2dr_c5m3 = harm_avg_2048_les_2dr_c5m3[:,0]
v_harm_avg_2048_les_2dr_c5m3 = harm_avg_2048_les_2dr_c5m3[:,1]**2 + harm_avg_2048_les_2dr_c5m3[:,2]**2
harm_avg_2048_les_2dr_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_les_2dr_c6m3 = harm_avg_2048_les_2dr_c6m3[:,0]
v_harm_avg_2048_les_2dr_c6m3 = harm_avg_2048_les_2dr_c6m3[:,1]**2 + harm_avg_2048_les_2dr_c6m3[:,2]**2
harm_avg_2048_les_2dr_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_les_2dr_c7m3 = harm_avg_2048_les_2dr_c7m3[:,0]
v_harm_avg_2048_les_2dr_c7m3 = harm_avg_2048_les_2dr_c7m3[:,1]**2 + harm_avg_2048_les_2dr_c7m3[:,2]**2
harm_avg_2048_les_2dr_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_les_2dr_c8m3 = harm_avg_2048_les_2dr_c8m3[:,0]
v_harm_avg_2048_les_2dr_c8m3 = harm_avg_2048_les_2dr_c8m3[:,1]**2 + harm_avg_2048_les_2dr_c8m3[:,2]**2

harm_avg_2048_les_2dr_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_les_2dr_c4p4 = harm_avg_2048_les_2dr_c4p4[:,0]
v_harm_avg_2048_les_2dr_c4p4 = harm_avg_2048_les_2dr_c4p4[:,1]**2 + harm_avg_2048_les_2dr_c4p4[:,2]**2
harm_avg_2048_les_2dr_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_les_2dr_c5p4 = harm_avg_2048_les_2dr_c5p4[:,0]
v_harm_avg_2048_les_2dr_c5p4 = harm_avg_2048_les_2dr_c5p4[:,1]**2 + harm_avg_2048_les_2dr_c5p4[:,2]**2
harm_avg_2048_les_2dr_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_les_2dr_c6p4 = harm_avg_2048_les_2dr_c6p4[:,0]
v_harm_avg_2048_les_2dr_c6p4 = harm_avg_2048_les_2dr_c6p4[:,1]**2 + harm_avg_2048_les_2dr_c6p4[:,2]**2
harm_avg_2048_les_2dr_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_les_2dr_c7p4 = harm_avg_2048_les_2dr_c7p4[:,0]
v_harm_avg_2048_les_2dr_c7p4 = harm_avg_2048_les_2dr_c7p4[:,1]**2 + harm_avg_2048_les_2dr_c7p4[:,2]**2
harm_avg_2048_les_2dr_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_les_2dr_c8p4 = harm_avg_2048_les_2dr_c8p4[:,0]
v_harm_avg_2048_les_2dr_c8p4 = harm_avg_2048_les_2dr_c8p4[:,1]**2 + harm_avg_2048_les_2dr_c8p4[:,2]**2

harm_avg_2048_les_2dr_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_les_2dr_c4m4 = harm_avg_2048_les_2dr_c4m4[:,0]
v_harm_avg_2048_les_2dr_c4m4 = harm_avg_2048_les_2dr_c4m4[:,1]**2 + harm_avg_2048_les_2dr_c4m4[:,2]**2
harm_avg_2048_les_2dr_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_les_2dr_c5m4 = harm_avg_2048_les_2dr_c5m4[:,0]
v_harm_avg_2048_les_2dr_c5m4 = harm_avg_2048_les_2dr_c5m4[:,1]**2 + harm_avg_2048_les_2dr_c5m4[:,2]**2
harm_avg_2048_les_2dr_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_les_2dr_c6m4 = harm_avg_2048_les_2dr_c6m4[:,0]
v_harm_avg_2048_les_2dr_c6m4 = harm_avg_2048_les_2dr_c6m4[:,1]**2 + harm_avg_2048_les_2dr_c6m4[:,2]**2
harm_avg_2048_les_2dr_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_les_2dr_c7m4 = harm_avg_2048_les_2dr_c7m4[:,0]
v_harm_avg_2048_les_2dr_c7m4 = harm_avg_2048_les_2dr_c7m4[:,1]**2 + harm_avg_2048_les_2dr_c7m4[:,2]**2
harm_avg_2048_les_2dr_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_les_2dr_c8m4 = harm_avg_2048_les_2dr_c8m4[:,0]
v_harm_avg_2048_les_2dr_c8m4 = harm_avg_2048_les_2dr_c8m4[:,1]**2 + harm_avg_2048_les_2dr_c8m4[:,2]**2

harm_avg_2048_les_2dr_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_les_2dr_c5p5 = harm_avg_2048_les_2dr_c5p5[:,0]
v_harm_avg_2048_les_2dr_c5p5 = harm_avg_2048_les_2dr_c5p5[:,1]**2 + harm_avg_2048_les_2dr_c5p5[:,2]**2
harm_avg_2048_les_2dr_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_les_2dr_c6p5 = harm_avg_2048_les_2dr_c6p5[:,0]
v_harm_avg_2048_les_2dr_c6p5 = harm_avg_2048_les_2dr_c6p5[:,1]**2 + harm_avg_2048_les_2dr_c6p5[:,2]**2
harm_avg_2048_les_2dr_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_les_2dr_c7p5 = harm_avg_2048_les_2dr_c7p5[:,0]
v_harm_avg_2048_les_2dr_c7p5 = harm_avg_2048_les_2dr_c7p5[:,1]**2 + harm_avg_2048_les_2dr_c7p5[:,2]**2
harm_avg_2048_les_2dr_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_les_2dr_c8p5 = harm_avg_2048_les_2dr_c8p5[:,0]
v_harm_avg_2048_les_2dr_c8p5 = harm_avg_2048_les_2dr_c8p5[:,1]**2 + harm_avg_2048_les_2dr_c8p5[:,2]**2

harm_avg_2048_les_2dr_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_les_2dr_c5m5 = harm_avg_2048_les_2dr_c5m5[:,0]
v_harm_avg_2048_les_2dr_c5m5 = harm_avg_2048_les_2dr_c5m5[:,1]**2 + harm_avg_2048_les_2dr_c5m5[:,2]**2
harm_avg_2048_les_2dr_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_les_2dr_c6m5 = harm_avg_2048_les_2dr_c6m5[:,0]
v_harm_avg_2048_les_2dr_c6m5 = harm_avg_2048_les_2dr_c6m5[:,1]**2 + harm_avg_2048_les_2dr_c6m5[:,2]**2
harm_avg_2048_les_2dr_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_les_2dr_c7m5 = harm_avg_2048_les_2dr_c7m5[:,0]
v_harm_avg_2048_les_2dr_c7m5 = harm_avg_2048_les_2dr_c7m5[:,1]**2 + harm_avg_2048_les_2dr_c7m5[:,2]**2
harm_avg_2048_les_2dr_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radius_avg_4..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_les_2dr_c8m5 = harm_avg_2048_les_2dr_c8m5[:,0]
v_harm_avg_2048_les_2dr_c8m5 = harm_avg_2048_les_2dr_c8m5[:,1]**2 + harm_avg_2048_les_2dr_c8m5[:,2]**2


harm_avg_2048_les_1_12_05_c00 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c00.dat")
t_harm_avg_2048_les_1_12_05_c00 = harm_avg_2048_les_1_12_05_c00[:,0]
v_harm_avg_2048_les_1_12_05_c00 = harm_avg_2048_les_1_12_05_c00[:,1]**2 + harm_avg_2048_les_1_12_05_c00[:,2]**2
harm_avg_2048_les_1_12_05_c10 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c10.dat")
t_harm_avg_2048_les_1_12_05_c10 = harm_avg_2048_les_1_12_05_c10[:,0]
v_harm_avg_2048_les_1_12_05_c10 = harm_avg_2048_les_1_12_05_c10[:,1]**2 + harm_avg_2048_les_1_12_05_c10[:,2]**2
harm_avg_2048_les_1_12_05_c20 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c20.dat")
t_harm_avg_2048_les_1_12_05_c20 = harm_avg_2048_les_1_12_05_c20[:,0]
v_harm_avg_2048_les_1_12_05_c20 = harm_avg_2048_les_1_12_05_c20[:,1]**2 + harm_avg_2048_les_1_12_05_c20[:,2]**2
harm_avg_2048_les_1_12_05_c30 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c30.dat")
t_harm_avg_2048_les_1_12_05_c30 = harm_avg_2048_les_1_12_05_c30[:,0]
v_harm_avg_2048_les_1_12_05_c30 = harm_avg_2048_les_1_12_05_c30[:,1]**2 + harm_avg_2048_les_1_12_05_c30[:,2]**2
harm_avg_2048_les_1_12_05_c40 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c40.dat")
t_harm_avg_2048_les_1_12_05_c40 = harm_avg_2048_les_1_12_05_c40[:,0]
v_harm_avg_2048_les_1_12_05_c40 = harm_avg_2048_les_1_12_05_c40[:,1]**2 + harm_avg_2048_les_1_12_05_c40[:,2]**2
harm_avg_2048_les_1_12_05_c50 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c50.dat")
t_harm_avg_2048_les_1_12_05_c50 = harm_avg_2048_les_1_12_05_c50[:,0]
v_harm_avg_2048_les_1_12_05_c50 = harm_avg_2048_les_1_12_05_c50[:,1]**2 + harm_avg_2048_les_1_12_05_c50[:,2]**2
harm_avg_2048_les_1_12_05_c60 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c60.dat")
t_harm_avg_2048_les_1_12_05_c60 = harm_avg_2048_les_1_12_05_c60[:,0]
v_harm_avg_2048_les_1_12_05_c60 = harm_avg_2048_les_1_12_05_c60[:,1]**2 + harm_avg_2048_les_1_12_05_c60[:,2]**2
harm_avg_2048_les_1_12_05_c70 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c70.dat")
t_harm_avg_2048_les_1_12_05_c70 = harm_avg_2048_les_1_12_05_c70[:,0]
v_harm_avg_2048_les_1_12_05_c70 = harm_avg_2048_les_1_12_05_c70[:,1]**2 + harm_avg_2048_les_1_12_05_c70[:,2]**2
harm_avg_2048_les_1_12_05_c80 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c80.dat")
t_harm_avg_2048_les_1_12_05_c80 = harm_avg_2048_les_1_12_05_c80[:,0]
v_harm_avg_2048_les_1_12_05_c80 = harm_avg_2048_les_1_12_05_c80[:,1]**2 + harm_avg_2048_les_1_12_05_c80[:,2]**2

harm_avg_2048_les_1_12_05_c1p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_les_1_12_05_c1p1 = harm_avg_2048_les_1_12_05_c1p1[:,0]
v_harm_avg_2048_les_1_12_05_c1p1 = harm_avg_2048_les_1_12_05_c1p1[:,1]**2 + harm_avg_2048_les_1_12_05_c1p1[:,2]**2
harm_avg_2048_les_1_12_05_c2p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_les_1_12_05_c2p1 = harm_avg_2048_les_1_12_05_c2p1[:,0]
v_harm_avg_2048_les_1_12_05_c2p1 = harm_avg_2048_les_1_12_05_c2p1[:,1]**2 + harm_avg_2048_les_1_12_05_c2p1[:,2]**2
harm_avg_2048_les_1_12_05_c3p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_les_1_12_05_c3p1 = harm_avg_2048_les_1_12_05_c3p1[:,0]
v_harm_avg_2048_les_1_12_05_c3p1 = harm_avg_2048_les_1_12_05_c3p1[:,1]**2 + harm_avg_2048_les_1_12_05_c3p1[:,2]**2
harm_avg_2048_les_1_12_05_c4p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_les_1_12_05_c4p1 = harm_avg_2048_les_1_12_05_c4p1[:,0]
v_harm_avg_2048_les_1_12_05_c4p1 = harm_avg_2048_les_1_12_05_c4p1[:,1]**2 + harm_avg_2048_les_1_12_05_c4p1[:,2]**2
harm_avg_2048_les_1_12_05_c5p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_les_1_12_05_c5p1 = harm_avg_2048_les_1_12_05_c5p1[:,0]
v_harm_avg_2048_les_1_12_05_c5p1 = harm_avg_2048_les_1_12_05_c5p1[:,1]**2 + harm_avg_2048_les_1_12_05_c5p1[:,2]**2
harm_avg_2048_les_1_12_05_c6p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_les_1_12_05_c6p1 = harm_avg_2048_les_1_12_05_c6p1[:,0]
v_harm_avg_2048_les_1_12_05_c6p1 = harm_avg_2048_les_1_12_05_c6p1[:,1]**2 + harm_avg_2048_les_1_12_05_c6p1[:,2]**2
harm_avg_2048_les_1_12_05_c7p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_les_1_12_05_c7p1 = harm_avg_2048_les_1_12_05_c7p1[:,0]
v_harm_avg_2048_les_1_12_05_c7p1 = harm_avg_2048_les_1_12_05_c7p1[:,1]**2 + harm_avg_2048_les_1_12_05_c7p1[:,2]**2
harm_avg_2048_les_1_12_05_c8p1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_les_1_12_05_c8p1 = harm_avg_2048_les_1_12_05_c8p1[:,0]
v_harm_avg_2048_les_1_12_05_c8p1 = harm_avg_2048_les_1_12_05_c8p1[:,1]**2 + harm_avg_2048_les_1_12_05_c8p1[:,2]**2

harm_avg_2048_les_1_12_05_c1m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_les_1_12_05_c1m1 = harm_avg_2048_les_1_12_05_c1m1[:,0]
v_harm_avg_2048_les_1_12_05_c1m1 = harm_avg_2048_les_1_12_05_c1m1[:,1]**2 + harm_avg_2048_les_1_12_05_c1m1[:,2]**2
harm_avg_2048_les_1_12_05_c2m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_les_1_12_05_c2m1 = harm_avg_2048_les_1_12_05_c2m1[:,0]
v_harm_avg_2048_les_1_12_05_c2m1 = harm_avg_2048_les_1_12_05_c2m1[:,1]**2 + harm_avg_2048_les_1_12_05_c2m1[:,2]**2
harm_avg_2048_les_1_12_05_c3m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_les_1_12_05_c3m1 = harm_avg_2048_les_1_12_05_c3m1[:,0]
v_harm_avg_2048_les_1_12_05_c3m1 = harm_avg_2048_les_1_12_05_c3m1[:,1]**2 + harm_avg_2048_les_1_12_05_c3m1[:,2]**2
harm_avg_2048_les_1_12_05_c4m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_les_1_12_05_c4m1 = harm_avg_2048_les_1_12_05_c4m1[:,0]
v_harm_avg_2048_les_1_12_05_c4m1 = harm_avg_2048_les_1_12_05_c4m1[:,1]**2 + harm_avg_2048_les_1_12_05_c4m1[:,2]**2
harm_avg_2048_les_1_12_05_c5m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_les_1_12_05_c5m1 = harm_avg_2048_les_1_12_05_c5m1[:,0]
v_harm_avg_2048_les_1_12_05_c5m1 = harm_avg_2048_les_1_12_05_c5m1[:,1]**2 + harm_avg_2048_les_1_12_05_c5m1[:,2]**2
harm_avg_2048_les_1_12_05_c6m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_les_1_12_05_c6m1 = harm_avg_2048_les_1_12_05_c6m1[:,0]
v_harm_avg_2048_les_1_12_05_c6m1 = harm_avg_2048_les_1_12_05_c6m1[:,1]**2 + harm_avg_2048_les_1_12_05_c6m1[:,2]**2
harm_avg_2048_les_1_12_05_c7m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_les_1_12_05_c7m1 = harm_avg_2048_les_1_12_05_c7m1[:,0]
v_harm_avg_2048_les_1_12_05_c7m1 = harm_avg_2048_les_1_12_05_c7m1[:,1]**2 + harm_avg_2048_les_1_12_05_c7m1[:,2]**2
harm_avg_2048_les_1_12_05_c8m1 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_les_1_12_05_c8m1 = harm_avg_2048_les_1_12_05_c8m1[:,0]
v_harm_avg_2048_les_1_12_05_c8m1 = harm_avg_2048_les_1_12_05_c8m1[:,1]**2 + harm_avg_2048_les_1_12_05_c8m1[:,2]**2

harm_avg_2048_les_1_12_05_c2p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_les_1_12_05_c2p2 = harm_avg_2048_les_1_12_05_c2p2[:,0]
v_harm_avg_2048_les_1_12_05_c2p2 = harm_avg_2048_les_1_12_05_c2p2[:,1]**2 + harm_avg_2048_les_1_12_05_c2p2[:,2]**2
harm_avg_2048_les_1_12_05_c3p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_les_1_12_05_c3p2 = harm_avg_2048_les_1_12_05_c3p2[:,0]
v_harm_avg_2048_les_1_12_05_c3p2 = harm_avg_2048_les_1_12_05_c3p2[:,1]**2 + harm_avg_2048_les_1_12_05_c3p2[:,2]**2
harm_avg_2048_les_1_12_05_c4p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_les_1_12_05_c4p2 = harm_avg_2048_les_1_12_05_c4p2[:,0]
v_harm_avg_2048_les_1_12_05_c4p2 = harm_avg_2048_les_1_12_05_c4p2[:,1]**2 + harm_avg_2048_les_1_12_05_c4p2[:,2]**2
harm_avg_2048_les_1_12_05_c5p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_les_1_12_05_c5p2 = harm_avg_2048_les_1_12_05_c5p2[:,0]
v_harm_avg_2048_les_1_12_05_c5p2 = harm_avg_2048_les_1_12_05_c5p2[:,1]**2 + harm_avg_2048_les_1_12_05_c5p2[:,2]**2
harm_avg_2048_les_1_12_05_c6p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_les_1_12_05_c6p2 = harm_avg_2048_les_1_12_05_c6p2[:,0]
v_harm_avg_2048_les_1_12_05_c6p2 = harm_avg_2048_les_1_12_05_c6p2[:,1]**2 + harm_avg_2048_les_1_12_05_c6p2[:,2]**2
harm_avg_2048_les_1_12_05_c7p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_les_1_12_05_c7p2 = harm_avg_2048_les_1_12_05_c7p2[:,0]
v_harm_avg_2048_les_1_12_05_c7p2 = harm_avg_2048_les_1_12_05_c7p2[:,1]**2 + harm_avg_2048_les_1_12_05_c7p2[:,2]**2
harm_avg_2048_les_1_12_05_c8p2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_les_1_12_05_c8p2 = harm_avg_2048_les_1_12_05_c8p2[:,0]
v_harm_avg_2048_les_1_12_05_c8p2 = harm_avg_2048_les_1_12_05_c8p2[:,1]**2 + harm_avg_2048_les_1_12_05_c8p2[:,2]**2

harm_avg_2048_les_1_12_05_c2m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_les_1_12_05_c2m2 = harm_avg_2048_les_1_12_05_c2m2[:,0]
v_harm_avg_2048_les_1_12_05_c2m2 = harm_avg_2048_les_1_12_05_c2m2[:,1]**2 + harm_avg_2048_les_1_12_05_c2m2[:,2]**2
harm_avg_2048_les_1_12_05_c3m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_les_1_12_05_c3m2 = harm_avg_2048_les_1_12_05_c3m2[:,0]
v_harm_avg_2048_les_1_12_05_c3m2 = harm_avg_2048_les_1_12_05_c3m2[:,1]**2 + harm_avg_2048_les_1_12_05_c3m2[:,2]**2
harm_avg_2048_les_1_12_05_c4m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_les_1_12_05_c4m2 = harm_avg_2048_les_1_12_05_c4m2[:,0]
v_harm_avg_2048_les_1_12_05_c4m2 = harm_avg_2048_les_1_12_05_c4m2[:,1]**2 + harm_avg_2048_les_1_12_05_c4m2[:,2]**2
harm_avg_2048_les_1_12_05_c5m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_les_1_12_05_c5m2 = harm_avg_2048_les_1_12_05_c5m2[:,0]
v_harm_avg_2048_les_1_12_05_c5m2 = harm_avg_2048_les_1_12_05_c5m2[:,1]**2 + harm_avg_2048_les_1_12_05_c5m2[:,2]**2
harm_avg_2048_les_1_12_05_c6m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_les_1_12_05_c6m2 = harm_avg_2048_les_1_12_05_c6m2[:,0]
v_harm_avg_2048_les_1_12_05_c6m2 = harm_avg_2048_les_1_12_05_c6m2[:,1]**2 + harm_avg_2048_les_1_12_05_c6m2[:,2]**2
harm_avg_2048_les_1_12_05_c7m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_les_1_12_05_c7m2 = harm_avg_2048_les_1_12_05_c7m2[:,0]
v_harm_avg_2048_les_1_12_05_c7m2 = harm_avg_2048_les_1_12_05_c7m2[:,1]**2 + harm_avg_2048_les_1_12_05_c7m2[:,2]**2
harm_avg_2048_les_1_12_05_c8m2 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_les_1_12_05_c8m2 = harm_avg_2048_les_1_12_05_c8m2[:,0]
v_harm_avg_2048_les_1_12_05_c8m2 = harm_avg_2048_les_1_12_05_c8m2[:,1]**2 + harm_avg_2048_les_1_12_05_c8m2[:,2]**2

harm_avg_2048_les_1_12_05_c3p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_les_1_12_05_c3p3 = harm_avg_2048_les_1_12_05_c3p3[:,0]
v_harm_avg_2048_les_1_12_05_c3p3 = harm_avg_2048_les_1_12_05_c3p3[:,1]**2 + harm_avg_2048_les_1_12_05_c3p3[:,2]**2
harm_avg_2048_les_1_12_05_c4p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_les_1_12_05_c4p3 = harm_avg_2048_les_1_12_05_c4p3[:,0]
v_harm_avg_2048_les_1_12_05_c4p3 = harm_avg_2048_les_1_12_05_c4p3[:,1]**2 + harm_avg_2048_les_1_12_05_c4p3[:,2]**2
harm_avg_2048_les_1_12_05_c5p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_les_1_12_05_c5p3 = harm_avg_2048_les_1_12_05_c5p3[:,0]
v_harm_avg_2048_les_1_12_05_c5p3 = harm_avg_2048_les_1_12_05_c5p3[:,1]**2 + harm_avg_2048_les_1_12_05_c5p3[:,2]**2
harm_avg_2048_les_1_12_05_c6p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_les_1_12_05_c6p3 = harm_avg_2048_les_1_12_05_c6p3[:,0]
v_harm_avg_2048_les_1_12_05_c6p3 = harm_avg_2048_les_1_12_05_c6p3[:,1]**2 + harm_avg_2048_les_1_12_05_c6p3[:,2]**2
harm_avg_2048_les_1_12_05_c7p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_les_1_12_05_c7p3 = harm_avg_2048_les_1_12_05_c7p3[:,0]
v_harm_avg_2048_les_1_12_05_c7p3 = harm_avg_2048_les_1_12_05_c7p3[:,1]**2 + harm_avg_2048_les_1_12_05_c7p3[:,2]**2
harm_avg_2048_les_1_12_05_c8p3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_les_1_12_05_c8p3 = harm_avg_2048_les_1_12_05_c8p3[:,0]
v_harm_avg_2048_les_1_12_05_c8p3 = harm_avg_2048_les_1_12_05_c8p3[:,1]**2 + harm_avg_2048_les_1_12_05_c8p3[:,2]**2

harm_avg_2048_les_1_12_05_c3m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_les_1_12_05_c3m3 = harm_avg_2048_les_1_12_05_c3m3[:,0]
v_harm_avg_2048_les_1_12_05_c3m3 = harm_avg_2048_les_1_12_05_c3m3[:,1]**2 + harm_avg_2048_les_1_12_05_c3m3[:,2]**2
harm_avg_2048_les_1_12_05_c4m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_les_1_12_05_c4m3 = harm_avg_2048_les_1_12_05_c4m3[:,0]
v_harm_avg_2048_les_1_12_05_c4m3 = harm_avg_2048_les_1_12_05_c4m3[:,1]**2 + harm_avg_2048_les_1_12_05_c4m3[:,2]**2
harm_avg_2048_les_1_12_05_c5m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_les_1_12_05_c5m3 = harm_avg_2048_les_1_12_05_c5m3[:,0]
v_harm_avg_2048_les_1_12_05_c5m3 = harm_avg_2048_les_1_12_05_c5m3[:,1]**2 + harm_avg_2048_les_1_12_05_c5m3[:,2]**2
harm_avg_2048_les_1_12_05_c6m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_les_1_12_05_c6m3 = harm_avg_2048_les_1_12_05_c6m3[:,0]
v_harm_avg_2048_les_1_12_05_c6m3 = harm_avg_2048_les_1_12_05_c6m3[:,1]**2 + harm_avg_2048_les_1_12_05_c6m3[:,2]**2
harm_avg_2048_les_1_12_05_c7m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_les_1_12_05_c7m3 = harm_avg_2048_les_1_12_05_c7m3[:,0]
v_harm_avg_2048_les_1_12_05_c7m3 = harm_avg_2048_les_1_12_05_c7m3[:,1]**2 + harm_avg_2048_les_1_12_05_c7m3[:,2]**2
harm_avg_2048_les_1_12_05_c8m3 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_les_1_12_05_c8m3 = harm_avg_2048_les_1_12_05_c8m3[:,0]
v_harm_avg_2048_les_1_12_05_c8m3 = harm_avg_2048_les_1_12_05_c8m3[:,1]**2 + harm_avg_2048_les_1_12_05_c8m3[:,2]**2

harm_avg_2048_les_1_12_05_c4p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_les_1_12_05_c4p4 = harm_avg_2048_les_1_12_05_c4p4[:,0]
v_harm_avg_2048_les_1_12_05_c4p4 = harm_avg_2048_les_1_12_05_c4p4[:,1]**2 + harm_avg_2048_les_1_12_05_c4p4[:,2]**2
harm_avg_2048_les_1_12_05_c5p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_les_1_12_05_c5p4 = harm_avg_2048_les_1_12_05_c5p4[:,0]
v_harm_avg_2048_les_1_12_05_c5p4 = harm_avg_2048_les_1_12_05_c5p4[:,1]**2 + harm_avg_2048_les_1_12_05_c5p4[:,2]**2
harm_avg_2048_les_1_12_05_c6p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_les_1_12_05_c6p4 = harm_avg_2048_les_1_12_05_c6p4[:,0]
v_harm_avg_2048_les_1_12_05_c6p4 = harm_avg_2048_les_1_12_05_c6p4[:,1]**2 + harm_avg_2048_les_1_12_05_c6p4[:,2]**2
harm_avg_2048_les_1_12_05_c7p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_les_1_12_05_c7p4 = harm_avg_2048_les_1_12_05_c7p4[:,0]
v_harm_avg_2048_les_1_12_05_c7p4 = harm_avg_2048_les_1_12_05_c7p4[:,1]**2 + harm_avg_2048_les_1_12_05_c7p4[:,2]**2
harm_avg_2048_les_1_12_05_c8p4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_les_1_12_05_c8p4 = harm_avg_2048_les_1_12_05_c8p4[:,0]
v_harm_avg_2048_les_1_12_05_c8p4 = harm_avg_2048_les_1_12_05_c8p4[:,1]**2 + harm_avg_2048_les_1_12_05_c8p4[:,2]**2

harm_avg_2048_les_1_12_05_c4m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_les_1_12_05_c4m4 = harm_avg_2048_les_1_12_05_c4m4[:,0]
v_harm_avg_2048_les_1_12_05_c4m4 = harm_avg_2048_les_1_12_05_c4m4[:,1]**2 + harm_avg_2048_les_1_12_05_c4m4[:,2]**2
harm_avg_2048_les_1_12_05_c5m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_les_1_12_05_c5m4 = harm_avg_2048_les_1_12_05_c5m4[:,0]
v_harm_avg_2048_les_1_12_05_c5m4 = harm_avg_2048_les_1_12_05_c5m4[:,1]**2 + harm_avg_2048_les_1_12_05_c5m4[:,2]**2
harm_avg_2048_les_1_12_05_c6m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_les_1_12_05_c6m4 = harm_avg_2048_les_1_12_05_c6m4[:,0]
v_harm_avg_2048_les_1_12_05_c6m4 = harm_avg_2048_les_1_12_05_c6m4[:,1]**2 + harm_avg_2048_les_1_12_05_c6m4[:,2]**2
harm_avg_2048_les_1_12_05_c7m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_les_1_12_05_c7m4 = harm_avg_2048_les_1_12_05_c7m4[:,0]
v_harm_avg_2048_les_1_12_05_c7m4 = harm_avg_2048_les_1_12_05_c7m4[:,1]**2 + harm_avg_2048_les_1_12_05_c7m4[:,2]**2
harm_avg_2048_les_1_12_05_c8m4 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_les_1_12_05_c8m4 = harm_avg_2048_les_1_12_05_c8m4[:,0]
v_harm_avg_2048_les_1_12_05_c8m4 = harm_avg_2048_les_1_12_05_c8m4[:,1]**2 + harm_avg_2048_les_1_12_05_c8m4[:,2]**2

harm_avg_2048_les_1_12_05_c5p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_les_1_12_05_c5p5 = harm_avg_2048_les_1_12_05_c5p5[:,0]
v_harm_avg_2048_les_1_12_05_c5p5 = harm_avg_2048_les_1_12_05_c5p5[:,1]**2 + harm_avg_2048_les_1_12_05_c5p5[:,2]**2
harm_avg_2048_les_1_12_05_c6p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_les_1_12_05_c6p5 = harm_avg_2048_les_1_12_05_c6p5[:,0]
v_harm_avg_2048_les_1_12_05_c6p5 = harm_avg_2048_les_1_12_05_c6p5[:,1]**2 + harm_avg_2048_les_1_12_05_c6p5[:,2]**2
harm_avg_2048_les_1_12_05_c7p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_les_1_12_05_c7p5 = harm_avg_2048_les_1_12_05_c7p5[:,0]
v_harm_avg_2048_les_1_12_05_c7p5 = harm_avg_2048_les_1_12_05_c7p5[:,1]**2 + harm_avg_2048_les_1_12_05_c7p5[:,2]**2
harm_avg_2048_les_1_12_05_c8p5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_les_1_12_05_c8p5 = harm_avg_2048_les_1_12_05_c8p5[:,0]
v_harm_avg_2048_les_1_12_05_c8p5 = harm_avg_2048_les_1_12_05_c8p5[:,1]**2 + harm_avg_2048_les_1_12_05_c8p5[:,2]**2

harm_avg_2048_les_1_12_05_c5m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_les_1_12_05_c5m5 = harm_avg_2048_les_1_12_05_c5m5[:,0]
v_harm_avg_2048_les_1_12_05_c5m5 = harm_avg_2048_les_1_12_05_c5m5[:,1]**2 + harm_avg_2048_les_1_12_05_c5m5[:,2]**2
harm_avg_2048_les_1_12_05_c6m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_les_1_12_05_c6m5 = harm_avg_2048_les_1_12_05_c6m5[:,0]
v_harm_avg_2048_les_1_12_05_c6m5 = harm_avg_2048_les_1_12_05_c6m5[:,1]**2 + harm_avg_2048_les_1_12_05_c6m5[:,2]**2
harm_avg_2048_les_1_12_05_c7m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_les_1_12_05_c7m5 = harm_avg_2048_les_1_12_05_c7m5[:,0]
v_harm_avg_2048_les_1_12_05_c7m5 = harm_avg_2048_les_1_12_05_c7m5[:,1]**2 + harm_avg_2048_les_1_12_05_c7m5[:,2]**2
harm_avg_2048_les_1_12_05_c8m5 = np.genfromtxt(fname="2048_LES/Harmonics/radisu_avg_1..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_les_1_12_05_c8m5 = harm_avg_2048_les_1_12_05_c8m5[:,0]
v_harm_avg_2048_les_1_12_05_c8m5 = harm_avg_2048_les_1_12_05_c8m5[:,1]**2 + harm_avg_2048_les_1_12_05_c8m5[:,2]**2


harm_avg_2048_1_12_05_c00 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c00.dat")
t_harm_avg_2048_1_12_05_c00 = harm_avg_2048_1_12_05_c00[:,0]
v_harm_avg_2048_1_12_05_c00 = harm_avg_2048_1_12_05_c00[:,1]**2 + harm_avg_2048_1_12_05_c00[:,2]**2
harm_avg_2048_1_12_05_c10 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c10.dat")
t_harm_avg_2048_1_12_05_c10 = harm_avg_2048_1_12_05_c10[:,0]
v_harm_avg_2048_1_12_05_c10 = harm_avg_2048_1_12_05_c10[:,1]**2 + harm_avg_2048_1_12_05_c10[:,2]**2
harm_avg_2048_1_12_05_c20 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c20.dat")
t_harm_avg_2048_1_12_05_c20 = harm_avg_2048_1_12_05_c20[:,0]
v_harm_avg_2048_1_12_05_c20 = harm_avg_2048_1_12_05_c20[:,1]**2 + harm_avg_2048_1_12_05_c20[:,2]**2
harm_avg_2048_1_12_05_c30 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c30.dat")
t_harm_avg_2048_1_12_05_c30 = harm_avg_2048_1_12_05_c30[:,0]
v_harm_avg_2048_1_12_05_c30 = harm_avg_2048_1_12_05_c30[:,1]**2 + harm_avg_2048_1_12_05_c30[:,2]**2
harm_avg_2048_1_12_05_c40 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c40.dat")
t_harm_avg_2048_1_12_05_c40 = harm_avg_2048_1_12_05_c40[:,0]
v_harm_avg_2048_1_12_05_c40 = harm_avg_2048_1_12_05_c40[:,1]**2 + harm_avg_2048_1_12_05_c40[:,2]**2
harm_avg_2048_1_12_05_c50 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c50.dat")
t_harm_avg_2048_1_12_05_c50 = harm_avg_2048_1_12_05_c50[:,0]
v_harm_avg_2048_1_12_05_c50 = harm_avg_2048_1_12_05_c50[:,1]**2 + harm_avg_2048_1_12_05_c50[:,2]**2
harm_avg_2048_1_12_05_c60 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c60.dat")
t_harm_avg_2048_1_12_05_c60 = harm_avg_2048_1_12_05_c60[:,0]
v_harm_avg_2048_1_12_05_c60 = harm_avg_2048_1_12_05_c60[:,1]**2 + harm_avg_2048_1_12_05_c60[:,2]**2
harm_avg_2048_1_12_05_c70 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c70.dat")
t_harm_avg_2048_1_12_05_c70 = harm_avg_2048_1_12_05_c70[:,0]
v_harm_avg_2048_1_12_05_c70 = harm_avg_2048_1_12_05_c70[:,1]**2 + harm_avg_2048_1_12_05_c70[:,2]**2
harm_avg_2048_1_12_05_c80 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c80.dat")
t_harm_avg_2048_1_12_05_c80 = harm_avg_2048_1_12_05_c80[:,0]
v_harm_avg_2048_1_12_05_c80 = harm_avg_2048_1_12_05_c80[:,1]**2 + harm_avg_2048_1_12_05_c80[:,2]**2

harm_avg_2048_1_12_05_c1p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c1p1.dat")
t_harm_avg_2048_1_12_05_c1p1 = harm_avg_2048_1_12_05_c1p1[:,0]
v_harm_avg_2048_1_12_05_c1p1 = harm_avg_2048_1_12_05_c1p1[:,1]**2 + harm_avg_2048_1_12_05_c1p1[:,2]**2
harm_avg_2048_1_12_05_c2p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2p1.dat")
t_harm_avg_2048_1_12_05_c2p1 = harm_avg_2048_1_12_05_c2p1[:,0]
v_harm_avg_2048_1_12_05_c2p1 = harm_avg_2048_1_12_05_c2p1[:,1]**2 + harm_avg_2048_1_12_05_c2p1[:,2]**2
harm_avg_2048_1_12_05_c3p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p1.dat")
t_harm_avg_2048_1_12_05_c3p1 = harm_avg_2048_1_12_05_c3p1[:,0]
v_harm_avg_2048_1_12_05_c3p1 = harm_avg_2048_1_12_05_c3p1[:,1]**2 + harm_avg_2048_1_12_05_c3p1[:,2]**2
harm_avg_2048_1_12_05_c4p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p1.dat")
t_harm_avg_2048_1_12_05_c4p1 = harm_avg_2048_1_12_05_c4p1[:,0]
v_harm_avg_2048_1_12_05_c4p1 = harm_avg_2048_1_12_05_c4p1[:,1]**2 + harm_avg_2048_1_12_05_c4p1[:,2]**2
harm_avg_2048_1_12_05_c5p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p1.dat")
t_harm_avg_2048_1_12_05_c5p1 = harm_avg_2048_1_12_05_c5p1[:,0]
v_harm_avg_2048_1_12_05_c5p1 = harm_avg_2048_1_12_05_c5p1[:,1]**2 + harm_avg_2048_1_12_05_c5p1[:,2]**2
harm_avg_2048_1_12_05_c6p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p1.dat")
t_harm_avg_2048_1_12_05_c6p1 = harm_avg_2048_1_12_05_c6p1[:,0]
v_harm_avg_2048_1_12_05_c6p1 = harm_avg_2048_1_12_05_c6p1[:,1]**2 + harm_avg_2048_1_12_05_c6p1[:,2]**2
harm_avg_2048_1_12_05_c7p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p1.dat")
t_harm_avg_2048_1_12_05_c7p1 = harm_avg_2048_1_12_05_c7p1[:,0]
v_harm_avg_2048_1_12_05_c7p1 = harm_avg_2048_1_12_05_c7p1[:,1]**2 + harm_avg_2048_1_12_05_c7p1[:,2]**2
harm_avg_2048_1_12_05_c8p1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p1.dat")
t_harm_avg_2048_1_12_05_c8p1 = harm_avg_2048_1_12_05_c8p1[:,0]
v_harm_avg_2048_1_12_05_c8p1 = harm_avg_2048_1_12_05_c8p1[:,1]**2 + harm_avg_2048_1_12_05_c8p1[:,2]**2

harm_avg_2048_1_12_05_c1m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c1m1.dat")
t_harm_avg_2048_1_12_05_c1m1 = harm_avg_2048_1_12_05_c1m1[:,0]
v_harm_avg_2048_1_12_05_c1m1 = harm_avg_2048_1_12_05_c1m1[:,1]**2 + harm_avg_2048_1_12_05_c1m1[:,2]**2
harm_avg_2048_1_12_05_c2m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2m1.dat")
t_harm_avg_2048_1_12_05_c2m1 = harm_avg_2048_1_12_05_c2m1[:,0]
v_harm_avg_2048_1_12_05_c2m1 = harm_avg_2048_1_12_05_c2m1[:,1]**2 + harm_avg_2048_1_12_05_c2m1[:,2]**2
harm_avg_2048_1_12_05_c3m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m1.dat")
t_harm_avg_2048_1_12_05_c3m1 = harm_avg_2048_1_12_05_c3m1[:,0]
v_harm_avg_2048_1_12_05_c3m1 = harm_avg_2048_1_12_05_c3m1[:,1]**2 + harm_avg_2048_1_12_05_c3m1[:,2]**2
harm_avg_2048_1_12_05_c4m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m1.dat")
t_harm_avg_2048_1_12_05_c4m1 = harm_avg_2048_1_12_05_c4m1[:,0]
v_harm_avg_2048_1_12_05_c4m1 = harm_avg_2048_1_12_05_c4m1[:,1]**2 + harm_avg_2048_1_12_05_c4m1[:,2]**2
harm_avg_2048_1_12_05_c5m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m1.dat")
t_harm_avg_2048_1_12_05_c5m1 = harm_avg_2048_1_12_05_c5m1[:,0]
v_harm_avg_2048_1_12_05_c5m1 = harm_avg_2048_1_12_05_c5m1[:,1]**2 + harm_avg_2048_1_12_05_c5m1[:,2]**2
harm_avg_2048_1_12_05_c6m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m1.dat")
t_harm_avg_2048_1_12_05_c6m1 = harm_avg_2048_1_12_05_c6m1[:,0]
v_harm_avg_2048_1_12_05_c6m1 = harm_avg_2048_1_12_05_c6m1[:,1]**2 + harm_avg_2048_1_12_05_c6m1[:,2]**2
harm_avg_2048_1_12_05_c7m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m1.dat")
t_harm_avg_2048_1_12_05_c7m1 = harm_avg_2048_1_12_05_c7m1[:,0]
v_harm_avg_2048_1_12_05_c7m1 = harm_avg_2048_1_12_05_c7m1[:,1]**2 + harm_avg_2048_1_12_05_c7m1[:,2]**2
harm_avg_2048_1_12_05_c8m1 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m1.dat")
t_harm_avg_2048_1_12_05_c8m1 = harm_avg_2048_1_12_05_c8m1[:,0]
v_harm_avg_2048_1_12_05_c8m1 = harm_avg_2048_1_12_05_c8m1[:,1]**2 + harm_avg_2048_1_12_05_c8m1[:,2]**2

harm_avg_2048_1_12_05_c2p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2p2.dat")
t_harm_avg_2048_1_12_05_c2p2 = harm_avg_2048_1_12_05_c2p2[:,0]
v_harm_avg_2048_1_12_05_c2p2 = harm_avg_2048_1_12_05_c2p2[:,1]**2 + harm_avg_2048_1_12_05_c2p2[:,2]**2
harm_avg_2048_1_12_05_c3p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p2.dat")
t_harm_avg_2048_1_12_05_c3p2 = harm_avg_2048_1_12_05_c3p2[:,0]
v_harm_avg_2048_1_12_05_c3p2 = harm_avg_2048_1_12_05_c3p2[:,1]**2 + harm_avg_2048_1_12_05_c3p2[:,2]**2
harm_avg_2048_1_12_05_c4p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p2.dat")
t_harm_avg_2048_1_12_05_c4p2 = harm_avg_2048_1_12_05_c4p2[:,0]
v_harm_avg_2048_1_12_05_c4p2 = harm_avg_2048_1_12_05_c4p2[:,1]**2 + harm_avg_2048_1_12_05_c4p2[:,2]**2
harm_avg_2048_1_12_05_c5p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p2.dat")
t_harm_avg_2048_1_12_05_c5p2 = harm_avg_2048_1_12_05_c5p2[:,0]
v_harm_avg_2048_1_12_05_c5p2 = harm_avg_2048_1_12_05_c5p2[:,1]**2 + harm_avg_2048_1_12_05_c5p2[:,2]**2
harm_avg_2048_1_12_05_c6p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p2.dat")
t_harm_avg_2048_1_12_05_c6p2 = harm_avg_2048_1_12_05_c6p2[:,0]
v_harm_avg_2048_1_12_05_c6p2 = harm_avg_2048_1_12_05_c6p2[:,1]**2 + harm_avg_2048_1_12_05_c6p2[:,2]**2
harm_avg_2048_1_12_05_c7p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p2.dat")
t_harm_avg_2048_1_12_05_c7p2 = harm_avg_2048_1_12_05_c7p2[:,0]
v_harm_avg_2048_1_12_05_c7p2 = harm_avg_2048_1_12_05_c7p2[:,1]**2 + harm_avg_2048_1_12_05_c7p2[:,2]**2
harm_avg_2048_1_12_05_c8p2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p2.dat")
t_harm_avg_2048_1_12_05_c8p2 = harm_avg_2048_1_12_05_c8p2[:,0]
v_harm_avg_2048_1_12_05_c8p2 = harm_avg_2048_1_12_05_c8p2[:,1]**2 + harm_avg_2048_1_12_05_c8p2[:,2]**2

harm_avg_2048_1_12_05_c2m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c2m2.dat")
t_harm_avg_2048_1_12_05_c2m2 = harm_avg_2048_1_12_05_c2m2[:,0]
v_harm_avg_2048_1_12_05_c2m2 = harm_avg_2048_1_12_05_c2m2[:,1]**2 + harm_avg_2048_1_12_05_c2m2[:,2]**2
harm_avg_2048_1_12_05_c3m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m2.dat")
t_harm_avg_2048_1_12_05_c3m2 = harm_avg_2048_1_12_05_c3m2[:,0]
v_harm_avg_2048_1_12_05_c3m2 = harm_avg_2048_1_12_05_c3m2[:,1]**2 + harm_avg_2048_1_12_05_c3m2[:,2]**2
harm_avg_2048_1_12_05_c4m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m2.dat")
t_harm_avg_2048_1_12_05_c4m2 = harm_avg_2048_1_12_05_c4m2[:,0]
v_harm_avg_2048_1_12_05_c4m2 = harm_avg_2048_1_12_05_c4m2[:,1]**2 + harm_avg_2048_1_12_05_c4m2[:,2]**2
harm_avg_2048_1_12_05_c5m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m2.dat")
t_harm_avg_2048_1_12_05_c5m2 = harm_avg_2048_1_12_05_c5m2[:,0]
v_harm_avg_2048_1_12_05_c5m2 = harm_avg_2048_1_12_05_c5m2[:,1]**2 + harm_avg_2048_1_12_05_c5m2[:,2]**2
harm_avg_2048_1_12_05_c6m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m2.dat")
t_harm_avg_2048_1_12_05_c6m2 = harm_avg_2048_1_12_05_c6m2[:,0]
v_harm_avg_2048_1_12_05_c6m2 = harm_avg_2048_1_12_05_c6m2[:,1]**2 + harm_avg_2048_1_12_05_c6m2[:,2]**2
harm_avg_2048_1_12_05_c7m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m2.dat")
t_harm_avg_2048_1_12_05_c7m2 = harm_avg_2048_1_12_05_c7m2[:,0]
v_harm_avg_2048_1_12_05_c7m2 = harm_avg_2048_1_12_05_c7m2[:,1]**2 + harm_avg_2048_1_12_05_c7m2[:,2]**2
harm_avg_2048_1_12_05_c8m2 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m2.dat")
t_harm_avg_2048_1_12_05_c8m2 = harm_avg_2048_1_12_05_c8m2[:,0]
v_harm_avg_2048_1_12_05_c8m2 = harm_avg_2048_1_12_05_c8m2[:,1]**2 + harm_avg_2048_1_12_05_c8m2[:,2]**2

harm_avg_2048_1_12_05_c3p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3p3.dat")
t_harm_avg_2048_1_12_05_c3p3 = harm_avg_2048_1_12_05_c3p3[:,0]
v_harm_avg_2048_1_12_05_c3p3 = harm_avg_2048_1_12_05_c3p3[:,1]**2 + harm_avg_2048_1_12_05_c3p3[:,2]**2
harm_avg_2048_1_12_05_c4p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p3.dat")
t_harm_avg_2048_1_12_05_c4p3 = harm_avg_2048_1_12_05_c4p3[:,0]
v_harm_avg_2048_1_12_05_c4p3 = harm_avg_2048_1_12_05_c4p3[:,1]**2 + harm_avg_2048_1_12_05_c4p3[:,2]**2
harm_avg_2048_1_12_05_c5p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p3.dat")
t_harm_avg_2048_1_12_05_c5p3 = harm_avg_2048_1_12_05_c5p3[:,0]
v_harm_avg_2048_1_12_05_c5p3 = harm_avg_2048_1_12_05_c5p3[:,1]**2 + harm_avg_2048_1_12_05_c5p3[:,2]**2
harm_avg_2048_1_12_05_c6p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p3.dat")
t_harm_avg_2048_1_12_05_c6p3 = harm_avg_2048_1_12_05_c6p3[:,0]
v_harm_avg_2048_1_12_05_c6p3 = harm_avg_2048_1_12_05_c6p3[:,1]**2 + harm_avg_2048_1_12_05_c6p3[:,2]**2
harm_avg_2048_1_12_05_c7p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p3.dat")
t_harm_avg_2048_1_12_05_c7p3 = harm_avg_2048_1_12_05_c7p3[:,0]
v_harm_avg_2048_1_12_05_c7p3 = harm_avg_2048_1_12_05_c7p3[:,1]**2 + harm_avg_2048_1_12_05_c7p3[:,2]**2
harm_avg_2048_1_12_05_c8p3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p3.dat")
t_harm_avg_2048_1_12_05_c8p3 = harm_avg_2048_1_12_05_c8p3[:,0]
v_harm_avg_2048_1_12_05_c8p3 = harm_avg_2048_1_12_05_c8p3[:,1]**2 + harm_avg_2048_1_12_05_c8p3[:,2]**2

harm_avg_2048_1_12_05_c3m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c3m3.dat")
t_harm_avg_2048_1_12_05_c3m3 = harm_avg_2048_1_12_05_c3m3[:,0]
v_harm_avg_2048_1_12_05_c3m3 = harm_avg_2048_1_12_05_c3m3[:,1]**2 + harm_avg_2048_1_12_05_c3m3[:,2]**2
harm_avg_2048_1_12_05_c4m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m3.dat")
t_harm_avg_2048_1_12_05_c4m3 = harm_avg_2048_1_12_05_c4m3[:,0]
v_harm_avg_2048_1_12_05_c4m3 = harm_avg_2048_1_12_05_c4m3[:,1]**2 + harm_avg_2048_1_12_05_c4m3[:,2]**2
harm_avg_2048_1_12_05_c5m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m3.dat")
t_harm_avg_2048_1_12_05_c5m3 = harm_avg_2048_1_12_05_c5m3[:,0]
v_harm_avg_2048_1_12_05_c5m3 = harm_avg_2048_1_12_05_c5m3[:,1]**2 + harm_avg_2048_1_12_05_c5m3[:,2]**2
harm_avg_2048_1_12_05_c6m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m3.dat")
t_harm_avg_2048_1_12_05_c6m3 = harm_avg_2048_1_12_05_c6m3[:,0]
v_harm_avg_2048_1_12_05_c6m3 = harm_avg_2048_1_12_05_c6m3[:,1]**2 + harm_avg_2048_1_12_05_c6m3[:,2]**2
harm_avg_2048_1_12_05_c7m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m3.dat")
t_harm_avg_2048_1_12_05_c7m3 = harm_avg_2048_1_12_05_c7m3[:,0]
v_harm_avg_2048_1_12_05_c7m3 = harm_avg_2048_1_12_05_c7m3[:,1]**2 + harm_avg_2048_1_12_05_c7m3[:,2]**2
harm_avg_2048_1_12_05_c8m3 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m3.dat")
t_harm_avg_2048_1_12_05_c8m3 = harm_avg_2048_1_12_05_c8m3[:,0]
v_harm_avg_2048_1_12_05_c8m3 = harm_avg_2048_1_12_05_c8m3[:,1]**2 + harm_avg_2048_1_12_05_c8m3[:,2]**2

harm_avg_2048_1_12_05_c4p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4p4.dat")
t_harm_avg_2048_1_12_05_c4p4 = harm_avg_2048_1_12_05_c4p4[:,0]
v_harm_avg_2048_1_12_05_c4p4 = harm_avg_2048_1_12_05_c4p4[:,1]**2 + harm_avg_2048_1_12_05_c4p4[:,2]**2
harm_avg_2048_1_12_05_c5p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p4.dat")
t_harm_avg_2048_1_12_05_c5p4 = harm_avg_2048_1_12_05_c5p4[:,0]
v_harm_avg_2048_1_12_05_c5p4 = harm_avg_2048_1_12_05_c5p4[:,1]**2 + harm_avg_2048_1_12_05_c5p4[:,2]**2
harm_avg_2048_1_12_05_c6p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p4.dat")
t_harm_avg_2048_1_12_05_c6p4 = harm_avg_2048_1_12_05_c6p4[:,0]
v_harm_avg_2048_1_12_05_c6p4 = harm_avg_2048_1_12_05_c6p4[:,1]**2 + harm_avg_2048_1_12_05_c6p4[:,2]**2
harm_avg_2048_1_12_05_c7p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p4.dat")
t_harm_avg_2048_1_12_05_c7p4 = harm_avg_2048_1_12_05_c7p4[:,0]
v_harm_avg_2048_1_12_05_c7p4 = harm_avg_2048_1_12_05_c7p4[:,1]**2 + harm_avg_2048_1_12_05_c7p4[:,2]**2
harm_avg_2048_1_12_05_c8p4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p4.dat")
t_harm_avg_2048_1_12_05_c8p4 = harm_avg_2048_1_12_05_c8p4[:,0]
v_harm_avg_2048_1_12_05_c8p4 = harm_avg_2048_1_12_05_c8p4[:,1]**2 + harm_avg_2048_1_12_05_c8p4[:,2]**2

harm_avg_2048_1_12_05_c4m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c4m4.dat")
t_harm_avg_2048_1_12_05_c4m4 = harm_avg_2048_1_12_05_c4m4[:,0]
v_harm_avg_2048_1_12_05_c4m4 = harm_avg_2048_1_12_05_c4m4[:,1]**2 + harm_avg_2048_1_12_05_c4m4[:,2]**2
harm_avg_2048_1_12_05_c5m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m4.dat")
t_harm_avg_2048_1_12_05_c5m4 = harm_avg_2048_1_12_05_c5m4[:,0]
v_harm_avg_2048_1_12_05_c5m4 = harm_avg_2048_1_12_05_c5m4[:,1]**2 + harm_avg_2048_1_12_05_c5m4[:,2]**2
harm_avg_2048_1_12_05_c6m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m4.dat")
t_harm_avg_2048_1_12_05_c6m4 = harm_avg_2048_1_12_05_c6m4[:,0]
v_harm_avg_2048_1_12_05_c6m4 = harm_avg_2048_1_12_05_c6m4[:,1]**2 + harm_avg_2048_1_12_05_c6m4[:,2]**2
harm_avg_2048_1_12_05_c7m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m4.dat")
t_harm_avg_2048_1_12_05_c7m4 = harm_avg_2048_1_12_05_c7m4[:,0]
v_harm_avg_2048_1_12_05_c7m4 = harm_avg_2048_1_12_05_c7m4[:,1]**2 + harm_avg_2048_1_12_05_c7m4[:,2]**2
harm_avg_2048_1_12_05_c8m4 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m4.dat")
t_harm_avg_2048_1_12_05_c8m4 = harm_avg_2048_1_12_05_c8m4[:,0]
v_harm_avg_2048_1_12_05_c8m4 = harm_avg_2048_1_12_05_c8m4[:,1]**2 + harm_avg_2048_1_12_05_c8m4[:,2]**2

harm_avg_2048_1_12_05_c5p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5p5.dat")
t_harm_avg_2048_1_12_05_c5p5 = harm_avg_2048_1_12_05_c5p5[:,0]
v_harm_avg_2048_1_12_05_c5p5 = harm_avg_2048_1_12_05_c5p5[:,1]**2 + harm_avg_2048_1_12_05_c5p5[:,2]**2
harm_avg_2048_1_12_05_c6p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6p5.dat")
t_harm_avg_2048_1_12_05_c6p5 = harm_avg_2048_1_12_05_c6p5[:,0]
v_harm_avg_2048_1_12_05_c6p5 = harm_avg_2048_1_12_05_c6p5[:,1]**2 + harm_avg_2048_1_12_05_c6p5[:,2]**2
harm_avg_2048_1_12_05_c7p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7p5.dat")
t_harm_avg_2048_1_12_05_c7p5 = harm_avg_2048_1_12_05_c7p5[:,0]
v_harm_avg_2048_1_12_05_c7p5 = harm_avg_2048_1_12_05_c7p5[:,1]**2 + harm_avg_2048_1_12_05_c7p5[:,2]**2
harm_avg_2048_1_12_05_c8p5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8p5.dat")
t_harm_avg_2048_1_12_05_c8p5 = harm_avg_2048_1_12_05_c8p5[:,0]
v_harm_avg_2048_1_12_05_c8p5 = harm_avg_2048_1_12_05_c8p5[:,1]**2 + harm_avg_2048_1_12_05_c8p5[:,2]**2

harm_avg_2048_1_12_05_c5m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c5m5.dat")
t_harm_avg_2048_1_12_05_c5m5 = harm_avg_2048_1_12_05_c5m5[:,0]
v_harm_avg_2048_1_12_05_c5m5 = harm_avg_2048_1_12_05_c5m5[:,1]**2 + harm_avg_2048_1_12_05_c5m5[:,2]**2
harm_avg_2048_1_12_05_c6m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c6m5.dat")
t_harm_avg_2048_1_12_05_c6m5 = harm_avg_2048_1_12_05_c6m5[:,0]
v_harm_avg_2048_1_12_05_c6m5 = harm_avg_2048_1_12_05_c6m5[:,1]**2 + harm_avg_2048_1_12_05_c6m5[:,2]**2
harm_avg_2048_1_12_05_c7m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c7m5.dat")
t_harm_avg_2048_1_12_05_c7m5 = harm_avg_2048_1_12_05_c7m5[:,0]
v_harm_avg_2048_1_12_05_c7m5 = harm_avg_2048_1_12_05_c7m5[:,1]**2 + harm_avg_2048_1_12_05_c7m5[:,2]**2
harm_avg_2048_1_12_05_c8m5 = np.genfromtxt(fname="2048/Harmonics/radisu_avg_1..12..0.5/s0_c8m5.dat")
t_harm_avg_2048_1_12_05_c8m5 = harm_avg_2048_1_12_05_c8m5[:,0]
v_harm_avg_2048_1_12_05_c8m5 = harm_avg_2048_1_12_05_c8m5[:,1]**2 + harm_avg_2048_1_12_05_c8m5[:,2]**2

############ PLOTS ###############

plt.figure()
plt.plot(t_harm_avg_2048_1_12_05_c00, (v_harm_avg_2048_1_12_05_c00 + v_harm_avg_2048_1_12_05_c10 + v_harm_avg_2048_1_12_05_c20 + v_harm_avg_2048_1_12_05_c30 + v_harm_avg_2048_1_12_05_c40 + v_harm_avg_2048_1_12_05_c50 + v_harm_avg_2048_1_12_05_c60 + v_harm_avg_2048_1_12_05_c70 + v_harm_avg_2048_1_12_05_c80)/9, label=r'$C_{m=0}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c1m1, (v_harm_avg_2048_1_12_05_c1p1 + v_harm_avg_2048_1_12_05_c2p1 + v_harm_avg_2048_1_12_05_c3p1 + v_harm_avg_2048_1_12_05_c4p1 + v_harm_avg_2048_1_12_05_c5p1 + v_harm_avg_2048_1_12_05_c6p1 + v_harm_avg_2048_1_12_05_c7p1 + v_harm_avg_2048_1_12_05_c8p1 + v_harm_avg_2048_1_12_05_c1m1 + v_harm_avg_2048_1_12_05_c2m1 + v_harm_avg_2048_1_12_05_c3m1 + v_harm_avg_2048_1_12_05_c4m1 + v_harm_avg_2048_1_12_05_c5m1 + v_harm_avg_2048_1_12_05_c6m1 + v_harm_avg_2048_1_12_05_c7m1 + v_harm_avg_2048_1_12_05_c8m1)/16, label=r'$C_{m=1}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c2m2, (v_harm_avg_2048_1_12_05_c2p2 + v_harm_avg_2048_1_12_05_c3p2 + v_harm_avg_2048_1_12_05_c4p2 + v_harm_avg_2048_1_12_05_c5p2 + v_harm_avg_2048_1_12_05_c6p2 + v_harm_avg_2048_1_12_05_c7p2 + v_harm_avg_2048_1_12_05_c8p2 + v_harm_avg_2048_1_12_05_c2m2 + v_harm_avg_2048_1_12_05_c3m2 + v_harm_avg_2048_1_12_05_c4m2 + v_harm_avg_2048_1_12_05_c5m2 + v_harm_avg_2048_1_12_05_c6m2 + v_harm_avg_2048_1_12_05_c7m2 + v_harm_avg_2048_1_12_05_c8m2)/14, label=r'$C_{m=2}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c3m3, (v_harm_avg_2048_1_12_05_c3p3 + v_harm_avg_2048_1_12_05_c4p3 + v_harm_avg_2048_1_12_05_c5p3 + v_harm_avg_2048_1_12_05_c6p3 + v_harm_avg_2048_1_12_05_c7p3 + v_harm_avg_2048_1_12_05_c8p3 + v_harm_avg_2048_1_12_05_c3m3 + v_harm_avg_2048_1_12_05_c4m3 + v_harm_avg_2048_1_12_05_c5m3 + v_harm_avg_2048_1_12_05_c6m3 + v_harm_avg_2048_1_12_05_c7m3 + v_harm_avg_2048_1_12_05_c8m3)/12, label=r'$C_{m=3}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c4m4, (v_harm_avg_2048_1_12_05_c4p4 + v_harm_avg_2048_1_12_05_c5p4 + v_harm_avg_2048_1_12_05_c6p4 + v_harm_avg_2048_1_12_05_c7p4 + v_harm_avg_2048_1_12_05_c8p4 + v_harm_avg_2048_1_12_05_c4m4 + v_harm_avg_2048_1_12_05_c5m4 + v_harm_avg_2048_1_12_05_c6m4 + v_harm_avg_2048_1_12_05_c7m4 + v_harm_avg_2048_1_12_05_c8m4)/10, label=r'$C_{m=4}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_1_12_05_c5m5, (v_harm_avg_2048_1_12_05_c5p5 + v_harm_avg_2048_1_12_05_c6p5 + v_harm_avg_2048_1_12_05_c7p5 + v_harm_avg_2048_1_12_05_c8p5 + v_harm_avg_2048_1_12_05_c5m5 + v_harm_avg_2048_1_12_05_c6m5 + v_harm_avg_2048_1_12_05_c7m5 + v_harm_avg_2048_1_12_05_c8m5)/8, label=r'$C_{m=5}$', color='y', linestyle=':')

plt.plot(t_harm_avg_2048_les_1_12_05_c00, (v_harm_avg_2048_les_1_12_05_c00 + v_harm_avg_2048_les_1_12_05_c10 + v_harm_avg_2048_les_1_12_05_c20 + v_harm_avg_2048_les_1_12_05_c30 + v_harm_avg_2048_les_1_12_05_c40 + v_harm_avg_2048_les_1_12_05_c50 + v_harm_avg_2048_les_1_12_05_c60 + v_harm_avg_2048_les_1_12_05_c70 + v_harm_avg_2048_les_1_12_05_c80)/9, label=r'$C_{m=0}^{LES}$', color='b')
plt.plot(t_harm_avg_2048_les_1_12_05_c1m1, (v_harm_avg_2048_les_1_12_05_c1p1 + v_harm_avg_2048_les_1_12_05_c2p1 + v_harm_avg_2048_les_1_12_05_c3p1 + v_harm_avg_2048_les_1_12_05_c4p1 + v_harm_avg_2048_les_1_12_05_c5p1 + v_harm_avg_2048_les_1_12_05_c6p1 + v_harm_avg_2048_les_1_12_05_c7p1 + v_harm_avg_2048_les_1_12_05_c8p1 + v_harm_avg_2048_les_1_12_05_c1m1 + v_harm_avg_2048_les_1_12_05_c2m1 + v_harm_avg_2048_les_1_12_05_c3m1 + v_harm_avg_2048_les_1_12_05_c4m1 + v_harm_avg_2048_les_1_12_05_c5m1 + v_harm_avg_2048_les_1_12_05_c6m1 + v_harm_avg_2048_les_1_12_05_c7m1 + v_harm_avg_2048_les_1_12_05_c8m1)/16, label=r'$C_{m=1}^{LES}$', color='r')
plt.plot(t_harm_avg_2048_les_1_12_05_c2m2, (v_harm_avg_2048_les_1_12_05_c2p2 + v_harm_avg_2048_les_1_12_05_c3p2 + v_harm_avg_2048_les_1_12_05_c4p2 + v_harm_avg_2048_les_1_12_05_c5p2 + v_harm_avg_2048_les_1_12_05_c6p2 + v_harm_avg_2048_les_1_12_05_c7p2 + v_harm_avg_2048_les_1_12_05_c8p2 + v_harm_avg_2048_les_1_12_05_c2m2 + v_harm_avg_2048_les_1_12_05_c3m2 + v_harm_avg_2048_les_1_12_05_c4m2 + v_harm_avg_2048_les_1_12_05_c5m2 + v_harm_avg_2048_les_1_12_05_c6m2 + v_harm_avg_2048_les_1_12_05_c7m2 + v_harm_avg_2048_les_1_12_05_c8m2)/14, label=r'$C_{m=2}^{LES}$', color='g')
plt.plot(t_harm_avg_2048_les_1_12_05_c3m3, (v_harm_avg_2048_les_1_12_05_c3p3 + v_harm_avg_2048_les_1_12_05_c4p3 + v_harm_avg_2048_les_1_12_05_c5p3 + v_harm_avg_2048_les_1_12_05_c6p3 + v_harm_avg_2048_les_1_12_05_c7p3 + v_harm_avg_2048_les_1_12_05_c8p3 + v_harm_avg_2048_les_1_12_05_c3m3 + v_harm_avg_2048_les_1_12_05_c4m3 + v_harm_avg_2048_les_1_12_05_c5m3 + v_harm_avg_2048_les_1_12_05_c6m3 + v_harm_avg_2048_les_1_12_05_c7m3 + v_harm_avg_2048_les_1_12_05_c8m3)/12, label=r'$C_{m=3}^{LES}$', color='c')
plt.plot(t_harm_avg_2048_les_1_12_05_c4m4, (v_harm_avg_2048_les_1_12_05_c4p4 + v_harm_avg_2048_les_1_12_05_c5p4 + v_harm_avg_2048_les_1_12_05_c6p4 + v_harm_avg_2048_les_1_12_05_c7p4 + v_harm_avg_2048_les_1_12_05_c8p4 + v_harm_avg_2048_les_1_12_05_c4m4 + v_harm_avg_2048_les_1_12_05_c5m4 + v_harm_avg_2048_les_1_12_05_c6m4 + v_harm_avg_2048_les_1_12_05_c7m4 + v_harm_avg_2048_les_1_12_05_c8m4)/10, label=r'$C_{m=4}^{LES}$', color='m')
plt.plot(t_harm_avg_2048_les_1_12_05_c5m5, (v_harm_avg_2048_les_1_12_05_c5p5 + v_harm_avg_2048_les_1_12_05_c6p5 + v_harm_avg_2048_les_1_12_05_c7p5 + v_harm_avg_2048_les_1_12_05_c8p5 + v_harm_avg_2048_les_1_12_05_c5m5 + v_harm_avg_2048_les_1_12_05_c6m5 + v_harm_avg_2048_les_1_12_05_c7m5 + v_harm_avg_2048_les_1_12_05_c8m5)/8, label=r'$C_{m=5}^{LES}$', color='y')

plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2024_LES_noLES.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_harm_avg_2048_les_c00, (v_harm_avg_2048_les_c00 + v_harm_avg_2048_les_c10 + v_harm_avg_2048_les_c20 + v_harm_avg_2048_les_c30 + v_harm_avg_2048_les_c40 + v_harm_avg_2048_les_c50 + v_harm_avg_2048_les_c60 + v_harm_avg_2048_les_c70 + v_harm_avg_2048_les_c80)/9, label=r'$C_{m=0}$', color='b')
plt.plot(t_harm_avg_2048_les_c1m1, (v_harm_avg_2048_les_c1p1 + v_harm_avg_2048_les_c2p1 + v_harm_avg_2048_les_c3p1 + v_harm_avg_2048_les_c4p1 + v_harm_avg_2048_les_c5p1 + v_harm_avg_2048_les_c6p1 + v_harm_avg_2048_les_c7p1 + v_harm_avg_2048_les_c8p1 + v_harm_avg_2048_les_c1m1 + v_harm_avg_2048_les_c2m1 + v_harm_avg_2048_les_c3m1 + v_harm_avg_2048_les_c4m1 + v_harm_avg_2048_les_c5m1 + v_harm_avg_2048_les_c6m1 + v_harm_avg_2048_les_c7m1 + v_harm_avg_2048_les_c8m1)/16, label=r'$C_{m=1}$', color='r')
plt.plot(t_harm_avg_2048_les_c2m2, (v_harm_avg_2048_les_c2p2 + v_harm_avg_2048_les_c3p2 + v_harm_avg_2048_les_c4p2 + v_harm_avg_2048_les_c5p2 + v_harm_avg_2048_les_c6p2 + v_harm_avg_2048_les_c7p2 + v_harm_avg_2048_les_c8p2 + v_harm_avg_2048_les_c2m2 + v_harm_avg_2048_les_c3m2 + v_harm_avg_2048_les_c4m2 + v_harm_avg_2048_les_c5m2 + v_harm_avg_2048_les_c6m2 + v_harm_avg_2048_les_c7m2 + v_harm_avg_2048_les_c8m2)/14, label=r'$C_{m=2}$', color='g')
plt.plot(t_harm_avg_2048_les_c3m3, (v_harm_avg_2048_les_c3p3 + v_harm_avg_2048_les_c4p3 + v_harm_avg_2048_les_c5p3 + v_harm_avg_2048_les_c6p3 + v_harm_avg_2048_les_c7p3 + v_harm_avg_2048_les_c8p3 + v_harm_avg_2048_les_c3m3 + v_harm_avg_2048_les_c4m3 + v_harm_avg_2048_les_c5m3 + v_harm_avg_2048_les_c6m3 + v_harm_avg_2048_les_c7m3 + v_harm_avg_2048_les_c8m3)/12, label=r'$C_{m=3}$', color='c')
plt.plot(t_harm_avg_2048_les_c4m4, (v_harm_avg_2048_les_c4p4 + v_harm_avg_2048_les_c5p4 + v_harm_avg_2048_les_c6p4 + v_harm_avg_2048_les_c7p4 + v_harm_avg_2048_les_c8p4 + v_harm_avg_2048_les_c4m4 + v_harm_avg_2048_les_c5m4 + v_harm_avg_2048_les_c6m4 + v_harm_avg_2048_les_c7m4 + v_harm_avg_2048_les_c8m4)/10, label=r'$C_{m=4}$', color='m')
plt.plot(t_harm_avg_2048_les_c5m5, (v_harm_avg_2048_les_c5p5 + v_harm_avg_2048_les_c6p5 + v_harm_avg_2048_les_c7p5 + v_harm_avg_2048_les_c8p5 + v_harm_avg_2048_les_c5m5 + v_harm_avg_2048_les_c6m5 + v_harm_avg_2048_les_c7m5 + v_harm_avg_2048_les_c8m5)/8, label=r'$C_{m=5}$', color='y')

plt.plot(t_harm_avg_2048_les_2L_c00, (v_harm_avg_2048_les_2L_c00 + v_harm_avg_2048_les_2L_c10 + v_harm_avg_2048_les_2L_c20 + v_harm_avg_2048_les_2L_c30 + v_harm_avg_2048_les_2L_c40 + v_harm_avg_2048_les_2L_c50 + v_harm_avg_2048_les_2L_c60 + v_harm_avg_2048_les_2L_c70 + v_harm_avg_2048_les_2L_c80)/9, label=r'$C_{m=0}^{2L}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c1m1, (v_harm_avg_2048_les_2L_c1p1 + v_harm_avg_2048_les_2L_c2p1 + v_harm_avg_2048_les_2L_c3p1 + v_harm_avg_2048_les_2L_c4p1 + v_harm_avg_2048_les_2L_c5p1 + v_harm_avg_2048_les_2L_c6p1 + v_harm_avg_2048_les_2L_c7p1 + v_harm_avg_2048_les_2L_c8p1 + v_harm_avg_2048_les_2L_c1m1 + v_harm_avg_2048_les_2L_c2m1 + v_harm_avg_2048_les_2L_c3m1 + v_harm_avg_2048_les_2L_c4m1 + v_harm_avg_2048_les_2L_c5m1 + v_harm_avg_2048_les_2L_c6m1 + v_harm_avg_2048_les_2L_c7m1 + v_harm_avg_2048_les_2L_c8m1)/16, label=r'$C_{m=1}^{2L}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c2m2, (v_harm_avg_2048_les_2L_c2p2 + v_harm_avg_2048_les_2L_c3p2 + v_harm_avg_2048_les_2L_c4p2 + v_harm_avg_2048_les_2L_c5p2 + v_harm_avg_2048_les_2L_c6p2 + v_harm_avg_2048_les_2L_c7p2 + v_harm_avg_2048_les_2L_c8p2 + v_harm_avg_2048_les_2L_c2m2 + v_harm_avg_2048_les_2L_c3m2 + v_harm_avg_2048_les_2L_c4m2 + v_harm_avg_2048_les_2L_c5m2 + v_harm_avg_2048_les_2L_c6m2 + v_harm_avg_2048_les_2L_c7m2 + v_harm_avg_2048_les_2L_c8m2)/14, label=r'$C_{m=2}^{2L}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c3m3, (v_harm_avg_2048_les_2L_c3p3 + v_harm_avg_2048_les_2L_c4p3 + v_harm_avg_2048_les_2L_c5p3 + v_harm_avg_2048_les_2L_c6p3 + v_harm_avg_2048_les_2L_c7p3 + v_harm_avg_2048_les_2L_c8p3 + v_harm_avg_2048_les_2L_c3m3 + v_harm_avg_2048_les_2L_c4m3 + v_harm_avg_2048_les_2L_c5m3 + v_harm_avg_2048_les_2L_c6m3 + v_harm_avg_2048_les_2L_c7m3 + v_harm_avg_2048_les_2L_c8m3)/12, label=r'$C_{m=3}^{2L}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c4m4, (v_harm_avg_2048_les_2L_c4p4 + v_harm_avg_2048_les_2L_c5p4 + v_harm_avg_2048_les_2L_c6p4 + v_harm_avg_2048_les_2L_c7p4 + v_harm_avg_2048_les_2L_c8p4 + v_harm_avg_2048_les_2L_c4m4 + v_harm_avg_2048_les_2L_c5m4 + v_harm_avg_2048_les_2L_c6m4 + v_harm_avg_2048_les_2L_c7m4 + v_harm_avg_2048_les_2L_c8m4)/10, label=r'$C_{m=4}^{2L}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c5m5, (v_harm_avg_2048_les_2L_c5p5 + v_harm_avg_2048_les_2L_c6p5 + v_harm_avg_2048_les_2L_c7p5 + v_harm_avg_2048_les_2L_c8p5 + v_harm_avg_2048_les_2L_c5m5 + v_harm_avg_2048_les_2L_c6m5 + v_harm_avg_2048_les_2L_c7m5 + v_harm_avg_2048_les_2L_c8m5)/8, label=r'$C_{m=5}^{2L}$', color='y', linestyle=':')

plt.plot(t_harm_avg_2048_les_2dr_c00, (v_harm_avg_2048_les_2dr_c00 + v_harm_avg_2048_les_2dr_c10 + v_harm_avg_2048_les_2dr_c20 + v_harm_avg_2048_les_2dr_c30 + v_harm_avg_2048_les_2dr_c40 + v_harm_avg_2048_les_2dr_c50 + v_harm_avg_2048_les_2dr_c60 + v_harm_avg_2048_les_2dr_c70 + v_harm_avg_2048_les_2dr_c80)/9, label=r'$C_{m=0}^{2 \Delta r}$', color='b', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c1m1, (v_harm_avg_2048_les_2dr_c1p1 + v_harm_avg_2048_les_2dr_c2p1 + v_harm_avg_2048_les_2dr_c3p1 + v_harm_avg_2048_les_2dr_c4p1 + v_harm_avg_2048_les_2dr_c5p1 + v_harm_avg_2048_les_2dr_c6p1 + v_harm_avg_2048_les_2dr_c7p1 + v_harm_avg_2048_les_2dr_c8p1 + v_harm_avg_2048_les_2dr_c1m1 + v_harm_avg_2048_les_2dr_c2m1 + v_harm_avg_2048_les_2dr_c3m1 + v_harm_avg_2048_les_2dr_c4m1 + v_harm_avg_2048_les_2dr_c5m1 + v_harm_avg_2048_les_2dr_c6m1 + v_harm_avg_2048_les_2dr_c7m1 + v_harm_avg_2048_les_2dr_c8m1)/16, label=r'$C_{m=1}^{2 \Delta r}$', color='r', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c2m2, (v_harm_avg_2048_les_2dr_c2p2 + v_harm_avg_2048_les_2dr_c3p2 + v_harm_avg_2048_les_2dr_c4p2 + v_harm_avg_2048_les_2dr_c5p2 + v_harm_avg_2048_les_2dr_c6p2 + v_harm_avg_2048_les_2dr_c7p2 + v_harm_avg_2048_les_2dr_c8p2 + v_harm_avg_2048_les_2dr_c2m2 + v_harm_avg_2048_les_2dr_c3m2 + v_harm_avg_2048_les_2dr_c4m2 + v_harm_avg_2048_les_2dr_c5m2 + v_harm_avg_2048_les_2dr_c6m2 + v_harm_avg_2048_les_2dr_c7m2 + v_harm_avg_2048_les_2dr_c8m2)/14, label=r'$C_{m=2}^{2 \Delta r}$', color='g', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c3m3, (v_harm_avg_2048_les_2dr_c3p3 + v_harm_avg_2048_les_2dr_c4p3 + v_harm_avg_2048_les_2dr_c5p3 + v_harm_avg_2048_les_2dr_c6p3 + v_harm_avg_2048_les_2dr_c7p3 + v_harm_avg_2048_les_2dr_c8p3 + v_harm_avg_2048_les_2dr_c3m3 + v_harm_avg_2048_les_2dr_c4m3 + v_harm_avg_2048_les_2dr_c5m3 + v_harm_avg_2048_les_2dr_c6m3 + v_harm_avg_2048_les_2dr_c7m3 + v_harm_avg_2048_les_2dr_c8m3)/12, label=r'$C_{m=3}^{2 \Delta r}$', color='c', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c4m4, (v_harm_avg_2048_les_2dr_c4p4 + v_harm_avg_2048_les_2dr_c5p4 + v_harm_avg_2048_les_2dr_c6p4 + v_harm_avg_2048_les_2dr_c7p4 + v_harm_avg_2048_les_2dr_c8p4 + v_harm_avg_2048_les_2dr_c4m4 + v_harm_avg_2048_les_2dr_c5m4 + v_harm_avg_2048_les_2dr_c6m4 + v_harm_avg_2048_les_2dr_c7m4 + v_harm_avg_2048_les_2dr_c8m4)/10, label=r'$C_{m=4}^{2 \Delta r}$', color='m')
plt.plot(t_harm_avg_2048_les_2dr_c5m5, (v_harm_avg_2048_les_2dr_c5p5 + v_harm_avg_2048_les_2dr_c6p5 + v_harm_avg_2048_les_2dr_c7p5 + v_harm_avg_2048_les_2dr_c8p5 + v_harm_avg_2048_les_2dr_c5m5 + v_harm_avg_2048_les_2dr_c6m5 + v_harm_avg_2048_les_2dr_c7m5 + v_harm_avg_2048_les_2dr_c8m5)/8, label=r'$C_{m=5}^{2 \Delta r}$', color='y', linestyle='-.')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_all.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_harm_avg_2048_les_c00, (v_harm_avg_2048_les_c00 + v_harm_avg_2048_les_c10 + v_harm_avg_2048_les_c20 + v_harm_avg_2048_les_c30 + v_harm_avg_2048_les_c40 + v_harm_avg_2048_les_c50 + v_harm_avg_2048_les_c60 + v_harm_avg_2048_les_c70 + v_harm_avg_2048_les_c80)/9, label=r'$C_{m=0}$', color='b')
plt.plot(t_harm_avg_2048_les_c1m1, (v_harm_avg_2048_les_c1p1 + v_harm_avg_2048_les_c2p1 + v_harm_avg_2048_les_c3p1 + v_harm_avg_2048_les_c4p1 + v_harm_avg_2048_les_c5p1 + v_harm_avg_2048_les_c6p1 + v_harm_avg_2048_les_c7p1 + v_harm_avg_2048_les_c8p1 + v_harm_avg_2048_les_c1m1 + v_harm_avg_2048_les_c2m1 + v_harm_avg_2048_les_c3m1 + v_harm_avg_2048_les_c4m1 + v_harm_avg_2048_les_c5m1 + v_harm_avg_2048_les_c6m1 + v_harm_avg_2048_les_c7m1 + v_harm_avg_2048_les_c8m1)/16, label=r'$C_{m=1}$', color='r')
plt.plot(t_harm_avg_2048_les_c2m2, (v_harm_avg_2048_les_c2p2 + v_harm_avg_2048_les_c3p2 + v_harm_avg_2048_les_c4p2 + v_harm_avg_2048_les_c5p2 + v_harm_avg_2048_les_c6p2 + v_harm_avg_2048_les_c7p2 + v_harm_avg_2048_les_c8p2 + v_harm_avg_2048_les_c2m2 + v_harm_avg_2048_les_c3m2 + v_harm_avg_2048_les_c4m2 + v_harm_avg_2048_les_c5m2 + v_harm_avg_2048_les_c6m2 + v_harm_avg_2048_les_c7m2 + v_harm_avg_2048_les_c8m2)/14, label=r'$C_{m=2}$', color='g')
plt.plot(t_harm_avg_2048_les_c3m3, (v_harm_avg_2048_les_c3p3 + v_harm_avg_2048_les_c4p3 + v_harm_avg_2048_les_c5p3 + v_harm_avg_2048_les_c6p3 + v_harm_avg_2048_les_c7p3 + v_harm_avg_2048_les_c8p3 + v_harm_avg_2048_les_c3m3 + v_harm_avg_2048_les_c4m3 + v_harm_avg_2048_les_c5m3 + v_harm_avg_2048_les_c6m3 + v_harm_avg_2048_les_c7m3 + v_harm_avg_2048_les_c8m3)/12, label=r'$C_{m=3}$', color='c')
plt.plot(t_harm_avg_2048_les_c4m4, (v_harm_avg_2048_les_c4p4 + v_harm_avg_2048_les_c5p4 + v_harm_avg_2048_les_c6p4 + v_harm_avg_2048_les_c7p4 + v_harm_avg_2048_les_c8p4 + v_harm_avg_2048_les_c4m4 + v_harm_avg_2048_les_c5m4 + v_harm_avg_2048_les_c6m4 + v_harm_avg_2048_les_c7m4 + v_harm_avg_2048_les_c8m4)/10, label=r'$C_{m=4}$', color='m')
plt.plot(t_harm_avg_2048_les_c5m5, (v_harm_avg_2048_les_c5p5 + v_harm_avg_2048_les_c6p5 + v_harm_avg_2048_les_c7p5 + v_harm_avg_2048_les_c8p5 + v_harm_avg_2048_les_c5m5 + v_harm_avg_2048_les_c6m5 + v_harm_avg_2048_les_c7m5 + v_harm_avg_2048_les_c8m5)/8, label=r'$C_{m=5}$', color='y')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_harm_avg_2048_les_2L_c00, (v_harm_avg_2048_les_2L_c00 + v_harm_avg_2048_les_2L_c10 + v_harm_avg_2048_les_2L_c20 + v_harm_avg_2048_les_2L_c30 + v_harm_avg_2048_les_2L_c40 + v_harm_avg_2048_les_2L_c50 + v_harm_avg_2048_les_2L_c60 + v_harm_avg_2048_les_2L_c70 + v_harm_avg_2048_les_2L_c80)/9, label=r'$C_{m=0}^{2L}$', color='b', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c1m1, (v_harm_avg_2048_les_2L_c1p1 + v_harm_avg_2048_les_2L_c2p1 + v_harm_avg_2048_les_2L_c3p1 + v_harm_avg_2048_les_2L_c4p1 + v_harm_avg_2048_les_2L_c5p1 + v_harm_avg_2048_les_2L_c6p1 + v_harm_avg_2048_les_2L_c7p1 + v_harm_avg_2048_les_2L_c8p1 + v_harm_avg_2048_les_2L_c1m1 + v_harm_avg_2048_les_2L_c2m1 + v_harm_avg_2048_les_2L_c3m1 + v_harm_avg_2048_les_2L_c4m1 + v_harm_avg_2048_les_2L_c5m1 + v_harm_avg_2048_les_2L_c6m1 + v_harm_avg_2048_les_2L_c7m1 + v_harm_avg_2048_les_2L_c8m1)/16, label=r'$C_{m=1}^{2L}$', color='r', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c2m2, (v_harm_avg_2048_les_2L_c2p2 + v_harm_avg_2048_les_2L_c3p2 + v_harm_avg_2048_les_2L_c4p2 + v_harm_avg_2048_les_2L_c5p2 + v_harm_avg_2048_les_2L_c6p2 + v_harm_avg_2048_les_2L_c7p2 + v_harm_avg_2048_les_2L_c8p2 + v_harm_avg_2048_les_2L_c2m2 + v_harm_avg_2048_les_2L_c3m2 + v_harm_avg_2048_les_2L_c4m2 + v_harm_avg_2048_les_2L_c5m2 + v_harm_avg_2048_les_2L_c6m2 + v_harm_avg_2048_les_2L_c7m2 + v_harm_avg_2048_les_2L_c8m2)/14, label=r'$C_{m=2}^{2L}$', color='g', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c3m3, (v_harm_avg_2048_les_2L_c3p3 + v_harm_avg_2048_les_2L_c4p3 + v_harm_avg_2048_les_2L_c5p3 + v_harm_avg_2048_les_2L_c6p3 + v_harm_avg_2048_les_2L_c7p3 + v_harm_avg_2048_les_2L_c8p3 + v_harm_avg_2048_les_2L_c3m3 + v_harm_avg_2048_les_2L_c4m3 + v_harm_avg_2048_les_2L_c5m3 + v_harm_avg_2048_les_2L_c6m3 + v_harm_avg_2048_les_2L_c7m3 + v_harm_avg_2048_les_2L_c8m3)/12, label=r'$C_{m=3}^{2L}$', color='c', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c4m4, (v_harm_avg_2048_les_2L_c4p4 + v_harm_avg_2048_les_2L_c5p4 + v_harm_avg_2048_les_2L_c6p4 + v_harm_avg_2048_les_2L_c7p4 + v_harm_avg_2048_les_2L_c8p4 + v_harm_avg_2048_les_2L_c4m4 + v_harm_avg_2048_les_2L_c5m4 + v_harm_avg_2048_les_2L_c6m4 + v_harm_avg_2048_les_2L_c7m4 + v_harm_avg_2048_les_2L_c8m4)/10, label=r'$C_{m=4}^{2L}$', color='m', linestyle=':')
plt.plot(t_harm_avg_2048_les_2L_c5m5, (v_harm_avg_2048_les_2L_c5p5 + v_harm_avg_2048_les_2L_c6p5 + v_harm_avg_2048_les_2L_c7p5 + v_harm_avg_2048_les_2L_c8p5 + v_harm_avg_2048_les_2L_c5m5 + v_harm_avg_2048_les_2L_c6m5 + v_harm_avg_2048_les_2L_c7m5 + v_harm_avg_2048_les_2L_c8m5)/8, label=r'$C_{m=5}^{2L}$', color='y', linestyle=':')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2L.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_harm_avg_2048_les_2dr_c00, (v_harm_avg_2048_les_2dr_c00 + v_harm_avg_2048_les_2dr_c10 + v_harm_avg_2048_les_2dr_c20 + v_harm_avg_2048_les_2dr_c30 + v_harm_avg_2048_les_2dr_c40 + v_harm_avg_2048_les_2dr_c50 + v_harm_avg_2048_les_2dr_c60 + v_harm_avg_2048_les_2dr_c70 + v_harm_avg_2048_les_2dr_c80)/9, label=r'$C_{m=0}^{2 \Delta r}$', color='b', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c1m1, (v_harm_avg_2048_les_2dr_c1p1 + v_harm_avg_2048_les_2dr_c2p1 + v_harm_avg_2048_les_2dr_c3p1 + v_harm_avg_2048_les_2dr_c4p1 + v_harm_avg_2048_les_2dr_c5p1 + v_harm_avg_2048_les_2dr_c6p1 + v_harm_avg_2048_les_2dr_c7p1 + v_harm_avg_2048_les_2dr_c8p1 + v_harm_avg_2048_les_2dr_c1m1 + v_harm_avg_2048_les_2dr_c2m1 + v_harm_avg_2048_les_2dr_c3m1 + v_harm_avg_2048_les_2dr_c4m1 + v_harm_avg_2048_les_2dr_c5m1 + v_harm_avg_2048_les_2dr_c6m1 + v_harm_avg_2048_les_2dr_c7m1 + v_harm_avg_2048_les_2dr_c8m1)/16, label=r'$C_{m=1}^{2 \Delta r}$', color='r', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c2m2, (v_harm_avg_2048_les_2dr_c2p2 + v_harm_avg_2048_les_2dr_c3p2 + v_harm_avg_2048_les_2dr_c4p2 + v_harm_avg_2048_les_2dr_c5p2 + v_harm_avg_2048_les_2dr_c6p2 + v_harm_avg_2048_les_2dr_c7p2 + v_harm_avg_2048_les_2dr_c8p2 + v_harm_avg_2048_les_2dr_c2m2 + v_harm_avg_2048_les_2dr_c3m2 + v_harm_avg_2048_les_2dr_c4m2 + v_harm_avg_2048_les_2dr_c5m2 + v_harm_avg_2048_les_2dr_c6m2 + v_harm_avg_2048_les_2dr_c7m2 + v_harm_avg_2048_les_2dr_c8m2)/14, label=r'$C_{m=2}^{2 \Delta r}$', color='g', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c3m3, (v_harm_avg_2048_les_2dr_c3p3 + v_harm_avg_2048_les_2dr_c4p3 + v_harm_avg_2048_les_2dr_c5p3 + v_harm_avg_2048_les_2dr_c6p3 + v_harm_avg_2048_les_2dr_c7p3 + v_harm_avg_2048_les_2dr_c8p3 + v_harm_avg_2048_les_2dr_c3m3 + v_harm_avg_2048_les_2dr_c4m3 + v_harm_avg_2048_les_2dr_c5m3 + v_harm_avg_2048_les_2dr_c6m3 + v_harm_avg_2048_les_2dr_c7m3 + v_harm_avg_2048_les_2dr_c8m3)/12, label=r'$C_{m=3}^{2 \Delta r}$', color='c', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c4m4, (v_harm_avg_2048_les_2dr_c4p4 + v_harm_avg_2048_les_2dr_c5p4 + v_harm_avg_2048_les_2dr_c6p4 + v_harm_avg_2048_les_2dr_c7p4 + v_harm_avg_2048_les_2dr_c8p4 + v_harm_avg_2048_les_2dr_c4m4 + v_harm_avg_2048_les_2dr_c5m4 + v_harm_avg_2048_les_2dr_c6m4 + v_harm_avg_2048_les_2dr_c7m4 + v_harm_avg_2048_les_2dr_c8m4)/10, label=r'$C_{m=4}^{2 \Delta r}$', color='m', linestyle='-.')
plt.plot(t_harm_avg_2048_les_2dr_c5m5, (v_harm_avg_2048_les_2dr_c5p5 + v_harm_avg_2048_les_2dr_c6p5 + v_harm_avg_2048_les_2dr_c7p5 + v_harm_avg_2048_les_2dr_c8p5 + v_harm_avg_2048_les_2dr_c5m5 + v_harm_avg_2048_les_2dr_c6m5 + v_harm_avg_2048_les_2dr_c7m5 + v_harm_avg_2048_les_2dr_c8m5)/8, label=r'$C_{m=5}^{2 \Delta r}$', color='y', linestyle='-.')
plt.yscale("log")
plt.legend()
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("Harmonics_2dr.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_int2d_2048_les_m1e13_int, BRMS_int2d_2048_les_m1e13_int, label=r'$<B>_{13}^{2D}$', color='b', linestyle=':')
plt.plot(t_int2d_2048_les_m1e13_int, BRMSP_int2d_2048_les_m1e13_int, label=r'$<B_{pol}>_{13}^{2D}$', color='g', linestyle=':')
plt.plot(t_int2d_2048_les_m1e13_int, BRMST_int2d_2048_les_m1e13_int, label=r'$<B_{tor}>_{13}^{2D}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}^{3D}$', color='b')
plt.plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}^{3D}$', color='g')
plt.plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}^{3D}$', color='r')
plt.yscale("log")
plt.legend()
plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("2Dvs3D_2048_LES.pdf",  bbox_inches="tight")




plt.figure()
plt.plot(t_int3d_2048_les_m5e10_avg, B_int3d_2048_les_m5e10_avg, label=r'$<B>_{10}$', color='b', linestyle=':')
plt.plot(t_int3d_2048_les_m5e10_avg, Bpol_int3d_2048_les_m5e10_avg, label=r'$<B_{pol}>_{10}$', color='g', linestyle=':')
plt.plot(t_int3d_2048_les_m5e10_avg, Btor_int3d_2048_les_m5e10_avg, label=r'$<B_{tor}>_{10}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}$', color='b')
plt.plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g')
plt.plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_masks_2048_LES.pdf",  bbox_inches="tight")




plt.figure()
plt.plot(t_int3d_2048_m1e13_avg, B_int3d_2048_m1e13_avg, label=r'$<B>_{13}$', color='b', linestyle=':')
plt.plot(t_int3d_2048_m1e13_avg, Bpol_int3d_2048_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g', linestyle=':')
plt.plot(t_int3d_2048_m1e13_avg, Btor_int3d_2048_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, B_int3d_2048_les_m1e13_avg, label=r'$<B>_{13}^{LES}$', color='b')
plt.plot(t_int3d_2048_les_m1e13_avg, Bpol_int3d_2048_les_m1e13_avg, label=r'$<B_{pol}>_{13}^{LES}$', color='g')
plt.plot(t_int3d_2048_les_m1e13_avg, Btor_int3d_2048_les_m1e13_avg, label=r'$<B_{tor}>_{13}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_2048_m1e13.pdf",  bbox_inches="tight")


plt.figure()
plt.plot(t_int3d_2048_m5e10_avg, B_int3d_2048_m5e10_avg, label=r'$<B>_{10}$', color='b', linestyle=':')
plt.plot(t_int3d_2048_m5e10_avg, Bpol_int3d_2048_m5e10_avg, label=r'$<B_{pol}>_{10}$', color='g', linestyle=':')
plt.plot(t_int3d_2048_m5e10_avg, Btor_int3d_2048_m5e10_avg, label=r'$<B_{tor}>_{10}$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m5e10_avg, B_int3d_2048_les_m5e10_avg, label=r'$<B>_{10}^{LES}$', color='b')
plt.plot(t_int3d_2048_les_m5e10_avg, Bpol_int3d_2048_les_m5e10_avg, label=r'$<B_{pol}>_{10}^{LES}$', color='g')
plt.plot(t_int3d_2048_les_m5e10_avg, Btor_int3d_2048_les_m5e10_avg, label=r'$<B_{tor}>_{10}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_2048_m5e10.pdf",  bbox_inches="tight")

plt.figure()
plt.plot(t_int3d_4096_m1e13_avg, B_int3d_4096_m1e13_avg, label=r'$<B>_{13}$', color='b', linestyle=':')
plt.plot(t_int3d_4096_m1e13_avg, Bpol_int3d_4096_m1e13_avg, label=r'$<B_{pol}>_{13}$', color='g', linestyle=':')
plt.plot(t_int3d_4096_m1e13_avg, Btor_int3d_4096_m1e13_avg, label=r'$<B_{tor}>_{13}$', color='r', linestyle=':')
plt.plot(t_int3d_4096_les_m1e13_avg, B_int3d_4096_les_m1e13_avg, label=r'$<B>_{13}^{LES}$', color='b')
plt.plot(t_int3d_4096_les_m1e13_avg, Bpol_int3d_4096_les_m1e13_avg, label=r'$<B_{pol}>_{13}^{LES}$', color='g')
plt.plot(t_int3d_4096_les_m1e13_avg, Btor_int3d_4096_les_m1e13_avg, label=r'$<B_{tor}>_{13}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_4096_m1e13.pdf",  bbox_inches="tight")


plt.figure()
plt.plot(t_int3d_4096_m5e10_avg, B_int3d_4096_m5e10_avg, label=r'$<B>_{10}$', color='b', linestyle=':')
plt.plot(t_int3d_4096_m5e10_avg, Bpol_int3d_4096_m5e10_avg, label=r'$<B_{pol}>_{10}$', color='g', linestyle=':')
plt.plot(t_int3d_4096_m5e10_avg, Btor_int3d_4096_m5e10_avg, label=r'$<B_{tor}>_{10}$', color='r', linestyle=':')
plt.plot(t_int3d_4096_les_m5e10_avg, B_int3d_4096_les_m5e10_avg, label=r'$<B>_{10}^{LES}$', color='b')
plt.plot(t_int3d_4096_les_m5e10_avg, Bpol_int3d_4096_les_m5e10_avg, label=r'$<B_{pol}>_{10}^{LES}$', color='g')
plt.plot(t_int3d_4096_les_m5e10_avg, Btor_int3d_4096_les_m5e10_avg, label=r'$<B_{tor}>_{10}^{LES}$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
#plt.ylim([1e13, 2e16]) 
plt.xlabel(r'$t$',fontsize=20)
pl.savefig("3D_LES_4096_m5e10.pdf",  bbox_inches="tight")




plt.figure()
plt.plot(t_int3d_2048_m1e13_avg, invbeta_int3d_2048_m1e13_avg, label=r'$13$', color='b', linestyle=':')
plt.plot(t_int3d_2048_m5e10_avg, invbeta_int3d_2048_m5e10_avg, label=r'$10$', color='r', linestyle=':')
plt.plot(t_int3d_2048_les_m1e13_avg, invbeta_int3d_2048_les_m1e13_avg, label=r'$13 LES$', color='b')
plt.plot(t_int3d_2048_les_m5e10_avg, invbeta_int3d_2048_les_m5e10_avg, label=r'$10 LES$', color='r')
plt.yscale("log")
plt.legend(loc='lower right')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$<\frac{1}{\beta}>$',fontsize=20)
pl.savefig("3D_invbeta_2048_LES.pdf",  bbox_inches="tight")



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m1e13_avg_4250, Btor_cyl_2048_les_m1e13_avg_4250, label=r'$4050$', color='b')
axarr[0].plot(r_cyl_2048_les_m1e13_avg_5250, Btor_cyl_2048_les_m1e13_avg_5250, label=r'$5250$', color='r')
axarr[0].plot(r_cyl_2048_les_m1e13_avg_7250, Btor_cyl_2048_les_m1e13_avg_7250, label=r'$7250$', color='g')
axarr[0].plot(r_cyl_2048_les_m1e13_avg_9250, Btor_cyl_2048_les_m1e13_avg_9250, label=r'$9250$', color='y')
axarr[0].plot(r_cyl_2048_les_m1e13_avg_11250, Btor_cyl_2048_les_m1e13_avg_11250, label=r'$11250$', color='m')
axarr[0].plot(r_cyl_2048_les_m1e13_avg_13000, Btor_cyl_2048_les_m1e13_avg_13000, label=r'$13000$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 16])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{13}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m1e13_avg_4250, Bpol_cyl_2048_les_m1e13_avg_4250, label=r'$4050$', color='b')
axarr[1].plot(r_cyl_2048_les_m1e13_avg_5250, Bpol_cyl_2048_les_m1e13_avg_5250, label=r'$5250$', color='r')
axarr[1].plot(r_cyl_2048_les_m1e13_avg_7250, Bpol_cyl_2048_les_m1e13_avg_7250, label=r'$7250$', color='g')
axarr[1].plot(r_cyl_2048_les_m1e13_avg_9250, Bpol_cyl_2048_les_m1e13_avg_9250, label=r'$9250$', color='y')
axarr[1].plot(r_cyl_2048_les_m1e13_avg_11250, Bpol_cyl_2048_les_m1e13_avg_11250, label=r'$11250$', color='m')
axarr[1].plot(r_cyl_2048_les_m1e13_avg_13000, Bpol_cyl_2048_les_m1e13_avg_13000, label=r'$13000$', color='c')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 16])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{13}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Btor_Bpol_mask1e13_2048_LES.pdf",  bbox_inches="tight")



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_4250, Btor_cyl_2048_les_m5e10_avg_4250, label=r'$4050$', color='b')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, Btor_cyl_2048_les_m5e10_avg_5250, label=r'$5250$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_7250, Btor_cyl_2048_les_m5e10_avg_7250, label=r'$7250$', color='g')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, Btor_cyl_2048_les_m5e10_avg_9250, label=r'$9250$', color='y')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_11250, Btor_cyl_2048_les_m5e10_avg_11250, label=r'$11250$', color='m')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_13000, Btor_cyl_2048_les_m5e10_avg_13000, label=r'$13000$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 16])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{10}$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_4250, Bpol_cyl_2048_les_m5e10_avg_4250, label=r'$4050$', color='b')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, Bpol_cyl_2048_les_m5e10_avg_5250, label=r'$5250$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, Bpol_cyl_2048_les_m5e10_avg_7250, label=r'$7250$', color='g')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_9250, Bpol_cyl_2048_les_m5e10_avg_9250, label=r'$9250$', color='y')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_11250, Bpol_cyl_2048_les_m5e10_avg_11250, label=r'$11250$', color='m')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_13000, Bpol_cyl_2048_les_m5e10_avg_13000, label=r'$13000$', color='c')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 16])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Btor_Bpol_mask5e10_2048_LES.pdf",  bbox_inches="tight")



plt.figure()
plt.plot(r_cyl_2048_m1e13_avg_4250, invbeta_cyl_2048_m1e13_avg_4250, label=r'$13$', color='b', linestyle=':')
plt.plot(r_cyl_2048_m5e10_avg_4250, invbeta_cyl_2048_m5e10_avg_4250, label=r'$10$', color='r', linestyle=':')
plt.plot(r_cyl_2048_les_m1e13_avg_4250, invbeta_cyl_2048_les_m1e13_avg_4250, label=r'$13 LES$', color='b')
plt.plot(r_cyl_2048_les_m5e10_avg_4250, invbeta_cyl_2048_les_m5e10_avg_4250, label=r'$10 LES$', color='r')
plt.legend(loc='lower right')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$<\frac{1}{\beta}>$',fontsize=20)
pl.savefig("Cylinder_invbeta_2048_LES.pdf",  bbox_inches="tight")


# Two subplots sharing x axes
f, axarr = pl.subplots(3, sharex=True)
axarr[0].plot(t_int3d_1024_m5e10_int, Ekin_int3d_1024_m5e10_int, label=r'$1024_{10}$', color='b', linestyle=':')
axarr[0].plot(t_int3d_1024_les_m5e10_int, Ekin_int3d_1024_les_m5e10_int, label=r'$1024_{10} LES$', color='b')
axarr[0].plot(t_int3d_2048_m5e10_int, Ekin_int3d_2048_m5e10_int, label=r'$2048_{10}$', color='r', linestyle=':')
axarr[0].plot(t_int3d_2048_les_m5e10_int, Ekin_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[0].plot(t_int3d_4096_m5e10_int, Ekin_int3d_4096_m5e10_int, label=r'$4096_{10}$', color='g', linestyle=':')
axarr[0].plot(t_int3d_4096_les_m5e10_int, Ekin_int3d_4096_les_m5e10_int, label=r'$4096_{10} LES$', color='g')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=2, fancybox=True, shadow=True)
#axarr[0].set_xlabel(r'$t$',fontsize=20)
axarr[0].set_ylim([1e53, 1.5e53])
axarr[0].set_ylabel(r'$E_{kin}$',fontsize=20)
axarr[1].plot(t_int3d_1024_m5e10_int, Eint_int3d_1024_m5e10_int, label=r'$1024_{10}$', color='b', linestyle=':')
axarr[1].plot(t_int3d_1024_les_m5e10_int, Eint_int3d_1024_les_m5e10_int, label=r'$1024_{10} LES$', color='b')
axarr[1].plot(t_int3d_2048_m5e10_int, Eint_int3d_2048_m5e10_int, label=r'$2048_{10}$', color='r', linestyle=':')
axarr[1].plot(t_int3d_2048_les_m5e10_int, Eint_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[1].plot(t_int3d_4096_m5e10_int, Eint_int3d_4096_m5e10_int, label=r'$4096_{10}$', color='g', linestyle=':')
axarr[1].plot(t_int3d_4096_les_m5e10_int, Eint_int3d_4096_les_m5e10_int, label=r'$4096_{10} LES$', color='g')
#axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
#axarr[1].set_xlabel(r'$t$',fontsize=20)
axarr[1].set_ylim([5e53,9e53])
axarr[1].set_ylabel(r'$E_{int}$',fontsize=20)
axarr[2].plot(t_int3d_1024_m5e10_int, Emag_int3d_1024_m5e10_int, label=r'$1024_{10}$', color='b', linestyle=':')
axarr[2].plot(t_int3d_1024_les_m5e10_int, Emag_int3d_1024_les_m5e10_int, label=r'$1024_{10} LES$', color='b')
axarr[2].plot(t_int3d_2048_m5e10_int, Emag_int3d_2048_m5e10_int, label=r'$2048_{10}$', color='r', linestyle=':')
axarr[2].plot(t_int3d_2048_les_m5e10_int, Emag_int3d_2048_les_m5e10_int, label=r'$2048_{10} LES$', color='r')
axarr[2].plot(t_int3d_4096_m5e10_int, Emag_int3d_4096_m5e10_int, label=r'$4096_{10}$', color='g', linestyle=':')
axarr[2].plot(t_int3d_4096_les_m5e10_int, Emag_int3d_4096_les_m5e10_int, label=r'$4096_{10} LES$', color='g')
#axarr[2].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[2].set_xlabel(r'$t$',fontsize=20)
axarr[2].set_ylabel(r'$E_{mag}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Energies_INT3D_mask5e10.pdf",  bbox_inches="tight")



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int2d_1024_m1e13_int, BRMST_int2d_1024_m1e13_int, label=r'$1024$', color='g', linestyle=':')
axarr[0].plot(t_int2d_1024_les_m1e13_int, BRMST_int2d_1024_les_m1e13_int, label=r'$1024 LES$', color='g')
axarr[0].plot(t_int2d_2048_m1e13_int, BRMST_int2d_2048_m1e13_int, label=r'$2048$', color='b', linestyle=':')
axarr[0].plot(t_int2d_2048_les_m1e13_int, BRMST_int2d_2048_les_m1e13_int, label=r'$2048 LES$', color='b')
#axarr[0].plot(t_int2d_2048_les_t10_m1e13_int, BRMST_int2d_2048_les_t10_m1e13_int, label=r'$2048 LES_{t10}$', color='g')
axarr[0].plot(t_int2d_4096_m1e13_int, BRMST_int2d_4096_m1e13_int, label=r'$4096$', color='r', linestyle=':')
axarr[0].plot(t_int2d_4096_les_m1e13_int, BRMST_int2d_4096_les_m1e13_int, label=r'$4096 LES$', color='r')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlabel(r'$t$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{13}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[1].plot(t_int2d_1024_m1e13_int, BRMSP_int2d_1024_m1e13_int, label=r'$1024$', color='g', linestyle=':')
axarr[1].plot(t_int2d_1024_les_m1e13_int, BRMSP_int2d_1024_les_m1e13_int, label=r'$1024 LES$', color='g')
axarr[1].plot(t_int2d_2048_m1e13_int, BRMSP_int2d_2048_m1e13_int, label=r'$2048$', color='b', linestyle=':')
axarr[1].plot(t_int2d_2048_les_m1e13_int, BRMSP_int2d_2048_les_m1e13_int, label=r'$2048 LES$', color='b')
#axarr[1].plot(t_int2d_2048_les_t10_m1e13_int, BRMSP_int2d_2048_les_t10_m1e13_int, label=r'$2048 LES_{t10}$', color='g')
axarr[1].plot(t_int2d_4096_m1e13_int, BRMSP_int2d_4096_m1e13_int, label=r'$4096$', color='r', linestyle=':')
axarr[1].plot(t_int2d_4096_les_m1e13_int, BRMSP_int2d_4096_les_m1e13_int, label=r'$4096 LES$', color='r')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlabel(r'$t$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{13}$',fontsize=20)
axarr[1].set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("INT2D_Btor_Bpol_mask1e13.pdf",  bbox_inches="tight")

# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(t_int2d_1024_m5e10_int, BRMST_int2d_1024_m5e10_int, label=r'$1024$', color='g', linestyle=':')
axarr[0].plot(t_int2d_1024_les_m5e10_int, BRMST_int2d_1024_les_m5e10_int, label=r'$1024 LES$', color='g')
axarr[0].plot(t_int2d_2048_m5e10_int, BRMST_int2d_2048_m5e10_int, label=r'$2048$', color='b', linestyle=':')
axarr[0].plot(t_int2d_2048_les_m5e10_int, BRMST_int2d_2048_les_m5e10_int, label=r'$2048 LES$', color='b')
#axarr[0].plot(t_int2d_2048_les_t10_m5e10_int, BRMST_int2d_2048_les_t10_m5e10_int, label=r'$2048 LES_{t10}$', color='g')
axarr[0].plot(t_int2d_4096_m5e10_int, BRMST_int2d_4096_m5e10_int, label=r'$4096$', color='r', linestyle=':')
axarr[0].plot(t_int2d_4096_les_m5e10_int, BRMST_int2d_4096_les_m5e10_int, label=r'$4096 LES$', color='r')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlabel(r'$t$',fontsize=20)
axarr[0].set_ylabel(r'$<B_{tor}>_{10}$',fontsize=20)
axarr[0].set_yscale("log")
axarr[1].plot(t_int2d_1024_m5e10_int, BRMSP_int2d_1024_m5e10_int, label=r'$1024$', color='g', linestyle=':')
axarr[1].plot(t_int2d_1024_les_m5e10_int, BRMSP_int2d_1024_les_m5e10_int, label=r'$1024 LES$', color='g')
axarr[1].plot(t_int2d_2048_m5e10_int, BRMSP_int2d_2048_m5e10_int, label=r'$2048$', color='b', linestyle=':')
axarr[1].plot(t_int2d_2048_les_m5e10_int, BRMSP_int2d_2048_les_m5e10_int, label=r'$2048 LES$', color='b')
#axarr[1].plot(t_int2d_2048_les_t10_m5e10_int, BRMSP_int2d_2048_les_t10_m5e10_int, label=r'$2048 LES_{t10}$', color='g')
axarr[1].plot(t_int2d_4096_m5e10_int, BRMSP_int2d_4096_m5e10_int, label=r'$4096$', color='r', linestyle=':')
axarr[1].plot(t_int2d_4096_les_m5e10_int, BRMSP_int2d_4096_les_m5e10_int, label=r'$4096 LES$', color='r')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlabel(r'$t$',fontsize=20)
axarr[1].set_ylabel(r'$<B_{pol}>_{10}$',fontsize=20)
axarr[1].set_yscale("log")
pl.tight_layout(pad=0.1)
pl.savefig("INT2D_Btor_Bpol_mask5e10.pdf",  bbox_inches="tight")



# Two subplots sharing x axes
f, axarr = pl.subplots(2, sharex=True)
axarr[0].plot(r_cyl_2048_les_m5e10_avg_4250, rho_cyl_2048_les_m5e10_avg_4250*r_cyl_2048_les_m5e10_avg_4250*r_cyl_2048_les_m5e10_avg_4250, label=r'$4050$', color='b')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_5250, rho_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250*r_cyl_2048_les_m5e10_avg_5250, label=r'$5250$', color='r')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_7250, rho_cyl_2048_les_m5e10_avg_7250*r_cyl_2048_les_m5e10_avg_7250*r_cyl_2048_les_m5e10_avg_7250, label=r'$7250$', color='g')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_9250, rho_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250*r_cyl_2048_les_m5e10_avg_9250, label=r'$9250$', color='y')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_11250, rho_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250*r_cyl_2048_les_m5e10_avg_11250, label=r'$11250$', color='m')
axarr[0].plot(r_cyl_2048_les_m5e10_avg_13000, rho_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000*r_cyl_2048_les_m5e10_avg_13000, label=r'$13000$', color='c')
axarr[0].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[0].set_xlim([0, 25])
axarr[0].set_xlabel(r'$r$',fontsize=20)
axarr[0].set_ylabel(r'$<\rho>_{10} * r^2$',fontsize=20)
axarr[1].plot(r_cyl_2048_les_m5e10_avg_4250, omega_cyl_2048_les_m5e10_avg_4250, label=r'$4050$', color='b')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_5250, omega_cyl_2048_les_m5e10_avg_5250, label=r'$5250$', color='r')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, omega_cyl_2048_les_m5e10_avg_7250, label=r'$7250$', color='g')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_7250, omega_cyl_2048_les_m5e10_avg_9250, label=r'$9250$', color='y')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_11250, omega_cyl_2048_les_m5e10_avg_11250, label=r'$11250$', color='m')
axarr[1].plot(r_cyl_2048_les_m5e10_avg_13000, omega_cyl_2048_les_m5e10_avg_13000, label=r'$13000$', color='c')
axarr[1].legend(loc='upper right', labelspacing=0.2,prop={'size':14},ncol=1, fancybox=True, shadow=True)
axarr[1].set_xlim([0, 25])
axarr[1].set_xlabel(r'$r$',fontsize=20)
axarr[1].set_ylabel(r'$<\Omega>_{10}$',fontsize=20)
pl.tight_layout(pad=0.1)
pl.savefig("Cylinder_Rho_Omega_mask5e10_2048_LES.pdf",  bbox_inches="tight")
