import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt
from multiprocessing import Pool
from itertools import repeat
from scipy.interpolate import RegularGridInterpolator

def readData3DAll(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v], dtype='int')
    f.close()
    return dset[...]

def interpolate3D_function_R2(p111, p112, p121, p122, p211, p212, p221, p222):
    return 0.125 * p111 + 0.125 * p112 + 0.125 * p121 + 0.125 * p122 + 0.125 * p211 + 0.125 * p212 + 0.125 * p221 + 0.125 * p222

def interpolate3D_function(p111, p112, p121, p122, p211, p212, p221, p222, Rx, Ry, Rz, px, py, pz):
    return p111 +  (p211 - p111) * px/Rx +  (p121 - p111) * py/Ry +  (p112 - p111) * pz/Rz +  (p221 - p211 - p121 + p111) * px/Rx * py/Ry + (p212 - p211 - p112 + p111) * px/Rx * pz/Rz +  (p122 - p121 - p112 + p111) * py/Ry * pz/Rz +  (p222 - p111 + p211 + p121 + p112 - p221 - p212 - p122) * px/Rx * py/Ry * pz/Rz

def interpolate2D_function_R2(p11, p12, p21, p22):
    return 0.25 * p11 + 0.25 * p12 + 0.25 * p21 + 0.25 * p22

def interpolate2D_function(p11, p12, p21, p22, Rx, Ry, px, py):
    if Rx == 2 and Ry == 2:
        return 0.25 * p11 + 0.25 * p12 + 0.25 * p21 + 0.25 * p22
    return p11 + (p21 - p11) * px/Rx + (p12 - p11) * py/Ry + (p22 + p11 - (p21 + p12)) * px/Rx * py/Ry

def interpolate_function_R2(p1, p2):
    return 0.5 * p1 + 0.5 * p2

def interpolate_function(p1, p2, R, p):
    return (R * p1 - p * p1 + p * p2)/R

#@profile
def getData(folder, field, level, box_init, nullField, default_value):
    #Read mesh information
    print("Reading data level", level, "field", field)
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nlevels = f_sum['/BASIC_INFO/number_levels'][0]
    #Check given level exist
    if level >= nlevels:
        print("Level", level, "does not exist")
        return None, None, None, None, None, None, None

    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']
    dx = f_sum['/BASIC_INFO/dx'][level]
    init = f_sum['/BASIC_INFO/XLO'][:]
    ratio = f_sum['/BASIC_INFO/ratios_to_coarser_levels'][level]

    maximum = np.asarray([0, 0, 0])
    minimum = np.asarray([999999, 999999, 999999])
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum = np.maximum(maximum, patchExtents[i][1] + 1)
            minimum = np.minimum(minimum, patchExtents[i][0])

    offset = (minimum - (box_init - init) / dx).astype(int)

    size = (maximum - minimum) + 1
    data = np.zeros(shape=size)
    if nullField:
        data.fill(default_value)
    else:
        data.fill(np.nan)
        #print ("Inici iPatch")
        #print (level, "indices", minimum, maximum, "domain", (init + minimum * dx), (init + (maximum + 1) * dx), "offset", offset, "box", box_init, (box_init - init) / dx[0], -box_init, (-box_init - init) / dx[0], "box size", size)
        for iPatch in range(len(patchExtents)):
            if (patchMap[iPatch][2] == level):
                iProc = patchMap[iPatch][0]
                iProcStr = str(iProc).zfill(5)
                iPatchStr = str(patchMap[iPatch][3]).zfill(5)
                rangeX = (patchExtents[iPatch][0][0] - minimum[0], patchExtents[iPatch][1][0]+2 - minimum[0])
                rangeY = (patchExtents[iPatch][0][1] - minimum[1], patchExtents[iPatch][1][1]+2 - minimum[1])
                rangeZ = (patchExtents[iPatch][0][2] - minimum[2], patchExtents[iPatch][1][2]+2 - minimum[2])
                sizeX = patchExtents[iPatch][1][0] - patchExtents[iPatch][0][0] + 2;
                sizeY = patchExtents[iPatch][1][1] - patchExtents[iPatch][0][1] + 2;
                sizeZ = patchExtents[iPatch][1][2] - patchExtents[iPatch][0][2] + 2;
                f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
                tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
                tmp = np.reshape(tmp, (sizeX,sizeY,sizeZ), order="F")
                data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[:,:]
                f_data.close()
    f_sum.close()
    return data, minimum, maximum, np.any(offset > 0), dx, ratio, init

def interpolate(ratio, data, parentData, nans, mask, minimum, parentMinimum, default_value):
    if ratio[0] == 2 and ratio[1] == 2 and ratio[2] == 2:
        for iv in range(nans.shape[0]):
            i = nans[iv,0]
            j = nans[iv,1]
            k = nans[iv,2]
            if mask[i,j,k] == 0:
                i_real = i + minimum[0]
                j_real = j + minimum[1]
                k_real = k + minimum[2]
                i_parent = i_real // 2 - parentMinimum[0]
                j_parent = j_real // 2 - parentMinimum[1]
                k_parent = k_real // 2 - parentMinimum[2]
                px = i_real % 2
                py = j_real % 2
                pz = k_real % 2
                i_align = px == 0
                j_align = py == 0
                k_align = pz == 0     
                #print i_real, j_real, k_real, i_parent, j_parent, k_parent, minimum, parentMinimum
                if k_align:
                    if j_align:
                        if i_align:
                            data[i, j, k] = parentData[i_parent, j_parent, k_parent]
                        else:
                            data[i, j, k] = interpolate_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent + 1, j_parent, k_parent])
                    else:
                        if i_align:
                            data[i, j, k] = interpolate_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent + 1, k_parent])
                        else:
                            data[i, j, k] = interpolate2D_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent + 1, k_parent])
                else:
                    if j_align:
                        if i_align:
                            data[i, j, k] = interpolate_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1])
                        else:
                            data[i, j, k] = interpolate2D_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent , k_parent + 1])
                    else:
                        if i_align:
                            data[i, j, k] = interpolate2D_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent, j_parent + 1, k_parent + 1])
                        else:
                            data[i, j, k] = interpolate3D_function_R2(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent, j_parent + 1, k_parent + 1], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent, k_parent + 1], parentData[i_parent + 1, j_parent + 1, k_parent], parentData[i_parent + 1, j_parent + 1, k_parent + 1])
            else:
                data[i,j,k] = default_value
    else:
        for iv in range(nans.shape[0]):
            i = nans[iv,0]
            j = nans[iv,1]
            k = nans[iv,2]
            if mask[i,j,k] == 0:
                i_real = i + minimum[0]
                j_real = j + minimum[1]
                k_real = k + minimum[2]
                i_parent = i_real // ratio[0] - parentMinimum[0]
                j_parent = j_real // ratio[1] - parentMinimum[1]
                k_parent = k_real // ratio[2] - parentMinimum[2]
                px = i_real % ratio[0]
                py = j_real % ratio[1]
                pz = k_real % ratio[2]
                i_align = px == 0
                j_align = py == 0
                k_align = pz == 0     
                #print i_real, j_real, k_real, i_parent, j_parent, k_parent, minimum, parentMinimum
                if k_align:
                    if j_align:
                        if i_align:
                            data[i, j, k] = parentData[i_parent, j_parent, k_parent]
                        else:
                            data[i, j, k] = interpolate_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent + 1, j_parent, k_parent], ratio[0], px)
                    else:
                        if i_align:
                            data[i, j, k] = interpolate_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent + 1, k_parent], ratio[1], py)
                        else:
                            data[i, j, k] = interpolate2D_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent + 1, k_parent], ratio[0], ratio[1], px, py)
                else:
                    if j_align:
                        if i_align:
                            data[i, j, k] = interpolate_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], ratio[2], pz)
                        else:
                            data[i, j, k] = interpolate2D_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent , k_parent + 1], ratio[0], ratio[2], px, pz)
                    else:
                        if i_align:
                            data[i, j, k] = interpolate2D_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent, j_parent + 1, k_parent + 1], ratio[1], ratio[2], py, pz)
                        else:
                            data[i, j, k] = interpolate3D_function(parentData[i_parent, j_parent, k_parent], parentData[i_parent, j_parent, k_parent + 1], parentData[i_parent, j_parent + 1, k_parent], parentData[i_parent, j_parent + 1, k_parent + 1], parentData[i_parent + 1, j_parent, k_parent], parentData[i_parent + 1, j_parent, k_parent + 1], parentData[i_parent + 1, j_parent + 1, k_parent], parentData[i_parent + 1, j_parent + 1, k_parent + 1], ratio[0], ratio[1], ratio[2], px, py, pz)
            else:
                data[i,j,k] = default_value

    return data


#@profile
def getBoxLevel(folderout, folder, field, max_level, box_init, nullField, default_value):

    level = max_level
    #Loop levels until find first existent level
    while True:
        data, minimum, maximum, incomplete, dx, ratio, init = getData(folder, field, level, box_init, nullField, default_value)
        if init is not None:
            mask = readData3DAll(folderout+ '/mask_' + str(level) + '.hdf5', "mask")
            break
        else:
            level = level - 1

    # Only need to interpolate if any offset is greater than 0
    if incomplete and not nullField:       
        parentData, parentMinimum, parentMaximum, parentIncomplete, parentdx, parentRatio = getBoxLevel(folderout, folder, field, level - 1, box_init, nullField, default_value)
        #check if interpolation needed
        nans = np.argwhere(np.isnan(data))
        if nans.size > 0:
            #Fill nan data with interpolation from parent level
            print("Interpolating level",level, "field",field)
            print("Parent size ",parentData.size, "minimum", parentMinimum, "maximum", parentMaximum, "init", init)
            print("Children size ",data.size, "minimum", minimum, "maximum", maximum, "init", init)
            print("Nans ",nans.size/3, "per cent",100*(nans.size/3) / data.size)
            print("Mask points", np.argwhere(mask == 0).size/3, "total", mask.size, "per cent",100*(np.argwhere(mask == 0).size/3) / mask.size, "shape",mask.shape)

            data = interpolate(ratio, data, parentData, nans, mask, minimum, parentMinimum, default_value)

    data[np.where(mask)] = default_value

    #Writing the variable to disk
    f = h5py.File(folderout + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data.shape, dtype='float64')
    dset[:,:,:] = data[:,:,:]
    f.close()


    return data, minimum, maximum, incomplete, dx, ratio

#@profile
def getBox(folderout, folder, field, max_level, box_init, mask_lower_lim, mask_upper_lim, nullField, default_value):    
    data, minimum, maximum, incomplete, dx, ratio = getBoxLevel(folderout, folder, field, max_level, box_init, nullField, default_value)
     

#@profile
def getBoxLevelMask(folderout,folder, field, max_level, box_init, nullField, default_value, mask_lower_lim, mask_upper_lim, levels):

    level = max_level
    #Loop levels until find first existent level
    while True:
        data, minimum, maximum, incomplete, dx, ratio, init = getData(folder, field, level, box_init, nullField, default_value)
        if init is not None:
            break
        else:
            level = level - 1
    levels.append(level)
    print("Level", level, "minimum", minimum, minimum*dx + init, "maximum", maximum, maximum*dx + init,"dx",dx, "shape", data.shape)
    # Only need to interpolate if any offset is greater than 0
    if incomplete and not nullField:
        parentData, parentMinimum, parentMaximum, parentIncomplete, parentdx, parentRatio, levels = getBoxLevelMask(folderout,folder, field, level - 1, box_init, nullField, default_value, mask_lower_lim, mask_upper_lim, levels)

        #check if interpolation needed
        nans = np.argwhere(np.isnan(data))
        if nans.size > 0:
            print("Interpolating level",level, "field",field)
            #Calculate parent mask and interpolate it to children dimensions
            mask = np.logical_or(parentData < mask_lower_lim, parentData > mask_upper_lim)
            print("Parent Mask points", np.argwhere(mask == 0).size/3, " total", mask.size, "shape", mask.shape, "parent:data", parentData.shape, "minimum", parentMinimum, "maximum", parentMaximum, "init", init)
            x = np.linspace(parentMinimum[0]*parentdx[0] + init[0],(parentMinimum[0] + parentData.shape[0])*parentdx[0] + init[0],num=parentData.shape[0])
            y = np.linspace(parentMinimum[1]*parentdx[1] + init[1],(parentMinimum[1] + parentData.shape[1])*parentdx[1] + init[1],num=parentData.shape[1])
            z = np.linspace(parentMinimum[2]*parentdx[2] + init[2],(parentMinimum[2] + parentData.shape[2])*parentdx[2] + init[2],num=parentData.shape[2])
            interpolator = RegularGridInterpolator((x,y,z), mask)
            new_x = np.linspace(minimum[0]*dx[0] + init[0],(minimum[0] + data.shape[0])*dx[0] + init[0], num=data.shape[0])
            new_y = np.linspace(minimum[1]*dx[1] + init[1],(minimum[1] + data.shape[1])*dx[1] + init[1], num=data.shape[1])
            new_z = np.linspace(minimum[2]*dx[2] + init[2],(minimum[2] + data.shape[2])*dx[2] + init[2], num=data.shape[2])
            xyz_grid = np.meshgrid(new_x, new_y, new_z)
            #print(xyz_grid.shape)
            xyz_list = np.reshape(xyz_grid, (3, -1), order='C').T
            mask = interpolator(xyz_list)
            mask = np.around(np.reshape(mask, data.shape, order='C'), decimals=1)
            print("Children Mask points", np.argwhere(mask == 0).size/3, " total", mask.size, "shape", mask.shape, "data", data.shape, "minimum", minimum, "maximum", maximum)

            #Iterate on Nans
            print("Nans ",nans.size/3, "per cent",100*(nans.size/3) / data.size)
            print("Mask points", np.argwhere(mask == 0).size/3, "total", mask.size, "per cent",100*(np.argwhere(mask == 0).size/3) / mask.size)

            data = interpolate(ratio, data, parentData, nans, mask, minimum, parentMinimum, default_value)


    #Create mask
    mask = np.logical_or(data < mask_lower_lim, data > mask_upper_lim)
    f = h5py.File(folderout + '/mask_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset('mask', mask.shape)
    dset[:,:,:] = mask[:,:,:]
    f.close()
    #Apply mask
    data[np.where(mask)] = default_value

    #Writing the variable to disk
    f = h5py.File(folderout + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data.shape, dtype='float64')
    dset[:,:,:] = data[:,:,:]
    f.close()
    
    #Write metadata
    np.save(folderout + "/dx_" + str(level) + ".npy", dx)
    np.save(folderout + "/init_" + str(level) + ".npy", minimum*dx + init)
    np.save(folderout + "/size_" + str(level) + ".npy", data.shape)

    return data, minimum, maximum, incomplete, dx, ratio, levels
    
#@profile
def getBoxMask(folderout, folder, field, max_level, box_init, mask_lower_lim, mask_upper_lim, nullField, default_value):

    data, minimum, maximum, incomplete, dx, ratio, levels = getBoxLevelMask(folderout, folder, field, max_level, box_init, nullField, default_value, mask_lower_lim, mask_upper_lim, [])

    return levels
 


def getCoords(folderout, folder, levels):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    for level in levels:
        dx = f_sum['/BASIC_INFO/dx'][level]
        init = f_sum['/BASIC_INFO/XLO'][:]
        patchMap     = f_sum['/extents/patch_map']
        patchExtents = f_sum['/extents/patch_extents']

        maximum = np.asarray([0, 0, 0])
        minimum = np.asarray([999999, 999999, 999999])
        for i in range(len(patchExtents)):
            if (patchMap[i][2] == level):
                maximum = np.maximum(maximum, patchExtents[i][1] + 1)
                minimum = np.minimum(minimum, patchExtents[i][0])


        size = (maximum - minimum) + 1
        coord = np.ndarray(shape=size)

        print("x", level, coord.shape)
        for i in range(size[0]):
            coord[i,:,:] = init[0] + (minimum[0]).astype(int) * dx[0] + i *dx[0]

        f = h5py.File(folderout + '/x_' + str(level) + '.hdf5', "w")
        dset = f.create_dataset('x', coord.shape, dtype='float64')
        dset[:,:,:] = coord[:,:,:]
        f.close()

        print("y", level, coord.shape)
        for j in range(size[1]):
            coord[:,j,:] = init[1] + (minimum[1]).astype(int) * dx[1] + j *dx[1]

        f = h5py.File(folderout + '/y_' + str(level) + '.hdf5', "w")
        dset = f.create_dataset('y', coord.shape, dtype='float64')
        dset[:,:,:] = coord[:,:,:]
        f.close()

        print("z", level, coord.shape)
        for k in range(size[2]):
            coord[:,:,k] = init[2] + (minimum[2]).astype(int) * dx[2] + k *dx[2]

        f = h5py.File(folderout + '/z_' + str(level) + '.hdf5', "w")
        dset = f.create_dataset('z', coord.shape, dtype='float64')
        dset[:,:,:] = coord[:,:,:]
        f.close()

    f_sum.close()

def getBox_unpack(args):
    return getBox(*args)

#@profile
def main():


    maxlevel = 8
#    folders = {"../outputDir_mesh_c1": range(416000, 576000+1, 32000) }

#    folders = {"../outputDir_mesh_c5": [512000, 672000, 928000, 1184000, 1440000]}
#    folders = {"../outputDir_mesh_c5": range(2592000, 2816000+1, 32000)}
    folders = {"../outputDir_mesh_c1": [384000]}
    box_init = -60.0    #In domain units
    mask_upper_lim = 1
    mask_lower_lim = 1e-10
    flevel = "level8_multilevel"
    fmask = 'mask1e-10'

    #fields = ["Df", "vfd_x", "vfd_y", "vfd_z", "Bfu_x", "Bfu_y", "Bfu_z", "Sfd_x", "Sfd_y", "Sfd_z", "chi", "epsf", "Betau_x", "Betau_y", "Betau_z", "Alpha", "pf", "gtd_xx", "gtd_xy", "gtd_xz", "gtd_yy", "gtd_yz", "gtd_zz"]
    #nullFields = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
    #default_values = [1e-12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1]
    fields = ["Df", "vfd_x", "vfd_y", "vfd_z", "Bfu_x", "Bfu_y", "Bfu_z", "Sfd_x", "Sfd_y", "Sfd_z", "chi", "epsf", "Betau_x", "Betau_y", "Betau_z", "Alpha", "pf"]
    nullFields = [False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False]
    default_values = [1e-12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0]
    
    pool= Pool(23)
    for f in folders:
        for i in folders[f]:
            folderout = f + "/visit_dump." + str(i).zfill(5) + '/' + flevel + '/' + fmask + '/box60'
            if not os.path.exists(folderout):
                os.makedirs(folderout)
            print("Tiempo ", f, i)
            #print("mask and rho")
            levels = getBoxMask(folderout, f + "/visit_dump." + str(i).zfill(5), "rhof", maxlevel, box_init, mask_lower_lim, mask_upper_lim, False, 1e-12)
            print("Total levels", levels)           

            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), fields, repeat(maxlevel), repeat(box_init), repeat(mask_lower_lim), repeat(mask_upper_lim), nullFields, default_values))
            print("Coords")
            getCoords(folderout, f + "/visit_dump." + str(i).zfill(5), levels)



if __name__ == "__main__":
    main()

