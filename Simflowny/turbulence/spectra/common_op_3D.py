import numpy as np
import h5py
import math
import os
import sys
from scipy import stats
from scipy import fftpack
from scipy.fftpack import fftn
from mpi4py import MPI

# Averages using REBIN 
# https://scipython.com/blog/binning-a-2d-array-in-numpy/

def int3D(f,dx):
    
    int3D = np.sum(f)*dx*dx*dx
    return int3D


def avrg3D(f,sf):

    Nx = f.shape[0]
    Ny = f.shape[1]
    Nz = f.shape[2]
    return f.reshape([int(Nx/sf), sf, int(Ny/sf), sf, int(Nz/sf), sf]).mean(5).mean(3).mean(1)


def grad3DAxis(S,dx, ax):

    return (np.roll(S,-1,axis=ax) - np.roll(S,1,axis=ax) )/(2.*dx)


def grad3D(S,dx):

    Nx = S.shape[0]
    Ny = S.shape[1]
    Nz = S.shape[2]
    grad = np.zeros((Nx,Ny,Nz,3))
    
    for i in range(0,3):
      grad[:,:,:,i] = (np.roll(S,-1,axis=i) - np.roll(S,1,axis=i) )/(2.*dx)

    return grad


def div3D(S,dx):
    
    div = ( np.roll(S[:,:,:,0],-1,axis=0) - np.roll(S[:,:,:,0],1,axis=0) + np.roll(S[:,:,:,1],-1,axis=1) - np.roll(S[:,:,:,1],1,axis=1) + np.roll(S[:,:,:,2],-1,axis=2) - np.roll(S[:,:,:,2],1,axis=2) )/(2.*dx)

    return div

def div3D_comp(vx,vy,vz,dx):
    
    div = ( np.roll(vx[:,:,:],-1,axis=0) - np.roll(vx[:,:,:],1,axis=0)  + np.roll(vy[:,:,:],-1,axis=1) - np.roll(vy[:,:,:],1,axis=1) + np.roll(vz[:,:,:],-1,axis=2) - np.roll(vz[:,:,:],1,axis=2) )/(2.*dx)

    return div

def GradDotGrad(f1,f2,dxf):
    return grad3DAxis(f1,dxf,0)*grad3DAxis(f2,dxf,0) + grad3DAxis(f1,dxf,1)*grad3DAxis(f2,dxf,1) + grad3DAxis(f1,dxf,2)*grad3DAxis(f2,dxf,2)

def GradDotGradGR(chif,gtu_xxf,gtu_yyf,gtu_zzf,gtu_xyf,gtu_yzf,gtu_xzf,f1,f2,dxf):
    return chif*( gtu_xxf*grad3DAxis(f1,dxf,0)*grad3DAxis(f2,dxf,0) 
                + gtu_yyf*grad3DAxis(f1,dxf,1)*grad3DAxis(f2,dxf,1) 
                + gtu_zzf*grad3DAxis(f1,dxf,2)*grad3DAxis(f2,dxf,2) 
                + gtu_xyf*(grad3DAxis(f1,dxf,0)*grad3DAxis(f2,dxf,1) + grad3DAxis(f1,dxf,1)*grad3DAxis(f2,dxf,0))
                + gtu_yzf*(grad3DAxis(f1,dxf,1)*grad3DAxis(f2,dxf,2) + grad3DAxis(f1,dxf,2)*grad3DAxis(f2,dxf,1))
                + gtu_xzf*(grad3DAxis(f1,dxf,0)*grad3DAxis(f2,dxf,2) + grad3DAxis(f1,dxf,2)*grad3DAxis(f2,dxf,0)) )

def GradDotGradGR_covSV(chif,gtu_xxf,gtu_yyf,gtu_zzf,gtu_xyf,gtu_yzf,gtu_xzf,gtd_xxf,gtd_yyf,gtd_zzf,gtd_xyf,gtd_yzf,gtd_xzf,s,iv,vu,dxf):

    chri = christoffel_udd(chif,gtu_xxf,gtu_yyf,gtu_zzf,gtu_xyf,gtu_yzf,gtu_xzf,gtd_xxf,gtd_yyf,gtd_zzf,gtd_xyf,gtd_yzf,gtd_xzf,dxf)

    A = chif*(
        gtu_xxf*grad3DAxis(s,dxf,0)*(grad3DAxis(vu[:,:,:,iv],dxf,0)
            + chri[:,:,:,iv,0,0]*vu[:,:,:,0] + chri[:,:,:,iv,0,1]*vu[:,:,:,1] + chri[:,:,:,iv,0,2]*vu[:,:,:,2])
      + gtu_yyf*grad3DAxis(s,dxf,1)*(grad3DAxis(vu[:,:,:,iv],dxf,1)
            + chri[:,:,:,iv,1,0]*vu[:,:,:,0] + chri[:,:,:,iv,1,1]*vu[:,:,:,1] + chri[:,:,:,iv,1,2]*vu[:,:,:,2])
      + gtu_zzf*grad3DAxis(s,dxf,2)*(grad3DAxis(vu[:,:,:,iv],dxf,2)
            + chri[:,:,:,iv,2,0]*vu[:,:,:,0] + chri[:,:,:,iv,2,1]*vu[:,:,:,1] + chri[:,:,:,iv,2,2]*vu[:,:,:,2])
      + gtu_xyf*(grad3DAxis(s,dxf,0)*(grad3DAxis(vu[:,:,:,iv],dxf,1)
            + chri[:,:,:,iv,0,0]*vu[:,:,:,0] + chri[:,:,:,iv,0,1]*vu[:,:,:,1] + chri[:,:,:,iv,0,2]*vu[:,:,:,2])
               + grad3DAxis(s,dxf,1)*(grad3DAxis(vu[:,:,:,iv],dxf,0)
            + chri[:,:,:,iv,1,0]*vu[:,:,:,0] + chri[:,:,:,iv,1,1]*vu[:,:,:,1] + chri[:,:,:,iv,1,2]*vu[:,:,:,2]))
      + gtu_yzf*(grad3DAxis(s,dxf,1)*(grad3DAxis(vu[:,:,:,iv],dxf,2)
            + chri[:,:,:,iv,1,0]*vu[:,:,:,0] + chri[:,:,:,iv,1,1]*vu[:,:,:,1] + chri[:,:,:,iv,1,2]*vu[:,:,:,2])
               + grad3DAxis(s,dxf,2)*(grad3DAxis(vu[:,:,:,iv],dxf,1)
            + chri[:,:,:,iv,2,0]*vu[:,:,:,0] + chri[:,:,:,iv,2,1]*vu[:,:,:,1] + chri[:,:,:,iv,2,2]*vu[:,:,:,2]))
      + gtu_xzf*(grad3DAxis(s,dxf,2)*(grad3DAxis(vu[:,:,:,iv],dxf,0)
            + chri[:,:,:,iv,2,0]*vu[:,:,:,0] + chri[:,:,:,iv,2,1]*vu[:,:,:,1] + chri[:,:,:,iv,2,2]*vu[:,:,:,2])
               + grad3DAxis(s,dxf,0)*(grad3DAxis(vu[:,:,:,iv],dxf,2)
            + chri[:,:,:,iv,0,0]*vu[:,:,:,0] + chri[:,:,:,iv,0,1]*vu[:,:,:,1] + chri[:,:,:,iv,0,2]*vu[:,:,:,2]))
      )

    return A

def christoffel_udd(chif,gtu_xxf,gtu_yyf,gtu_zzf,gtu_xyf,gtu_yzf,gtu_xzf,gtd_xxf,gtd_yyf,gtd_zzf,gtd_xyf,gtd_yzf,gtd_xzf,dxf):
# Gamma^[4th_index]_[5th_index][6th_index]
    christoffel_udd = np.zeros((chif.shape[0],chif.shape[1],chif.shape[2],3,3,3))

    christoffel_udd[:,:,:,0,0,0] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xxf,dxf,0) + grad3DAxis(gtd_xxf,dxf,0) - grad3DAxis(gtd_xxf,dxf,0) ) - ( gtd_xxf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,0))) + gtu_xyf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,0) - grad3DAxis(gtd_xxf,dxf,1) ) - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,1))) 
      + gtu_xzf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,0) - grad3DAxis(gtd_xxf,dxf,2) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,2))) 
      )

    christoffel_udd[:,:,:,0,1,1] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xyf,dxf,1) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,0)))
        + gtu_xyf*(chif*(grad3DAxis(gtd_yyf,dxf,1) + grad3DAxis(gtd_yyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,1))) 
        + gtu_xzf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,1) - grad3DAxis(gtd_yyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,0,2,2] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xzf,dxf,2) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,2) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,0)))
        + gtu_xyf*(chif*(grad3DAxis(gtd_yzf,dxf,2) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,2) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,1))) 
        + gtu_xzf*(chif*(grad3DAxis(gtd_zzf,dxf,2) + grad3DAxis(gtd_zzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,2) + gtd_zzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,0,0,1] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xxf,dxf,1) - grad3DAxis(gtd_xyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,0)))
        + gtu_xyf*(chif*(grad3DAxis(gtd_yyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_xyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,1))) 
        + gtu_xzf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,1) - grad3DAxis(gtd_xyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,0,0,2] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xxf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,0)))
        + gtu_xyf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,1))) 
        + gtu_xzf*(chif*(grad3DAxis(gtd_zzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_xzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,0,1,2] = 0.5*(gtu_xxf*(chif*(grad3DAxis(gtd_xzf,dxf,1) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,0)))
        + gtu_xyf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yyf,dxf,2) - grad3DAxis(gtd_yzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,1))) 
        + gtu_xzf*(chif*(grad3DAxis(gtd_zzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_yzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,0,0] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xxf,dxf,0) + grad3DAxis(gtd_xxf,dxf,0) - grad3DAxis(gtd_xxf,dxf,0) ) 
            - ( gtd_xxf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,0) - grad3DAxis(gtd_xxf,dxf,1) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,0) - grad3DAxis(gtd_xxf,dxf,2) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,1,1] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xyf,dxf,1) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_yyf,dxf,1) + grad3DAxis(gtd_yyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,1) - grad3DAxis(gtd_yyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,2,2] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xzf,dxf,2) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,2) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_yzf,dxf,2) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,2) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_zzf,dxf,2) + grad3DAxis(gtd_zzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,2) + gtd_zzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,0,1] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xxf,dxf,1) - grad3DAxis(gtd_xyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_yyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_xyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,1) - grad3DAxis(gtd_xyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,0,2] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xxf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_zzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_xzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,1,1,2] = 0.5*(gtu_xyf*(chif*(grad3DAxis(gtd_xzf,dxf,1) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,0)))
        + gtu_yyf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yyf,dxf,2) - grad3DAxis(gtd_yzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,1))) 
        + gtu_yzf*(chif*(grad3DAxis(gtd_zzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_yzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,0,0] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xxf,dxf,0) + grad3DAxis(gtd_xxf,dxf,0) - grad3DAxis(gtd_xxf,dxf,0) ) 
            - ( gtd_xxf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,0) - grad3DAxis(gtd_xxf,dxf,1) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,0) - grad3DAxis(gtd_xxf,dxf,2) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,0) - gtd_xxf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,1,1] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xyf,dxf,1) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_yyf,dxf,1) + grad3DAxis(gtd_yyf,dxf,1) - grad3DAxis(gtd_yyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,1) - grad3DAxis(gtd_yyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,1) - gtd_yyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,2,2] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xzf,dxf,2) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,2) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_yzf,dxf,2) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,2) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_zzf,dxf,2) + grad3DAxis(gtd_zzf,dxf,2) - grad3DAxis(gtd_zzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,2) + gtd_zzf*grad3DAxis(chif,dxf,2) - gtd_zzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,0,1] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xyf,dxf,0) + grad3DAxis(gtd_xxf,dxf,1) - grad3DAxis(gtd_xyf,dxf,0) ) 
            - ( gtd_xyf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_yyf,dxf,0) + grad3DAxis(gtd_xyf,dxf,1) - grad3DAxis(gtd_xyf,dxf,1) ) 
            - ( gtd_yyf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,1) - grad3DAxis(gtd_xyf,dxf,2) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,1) - gtd_xyf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,0,2] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xzf,dxf,0) + grad3DAxis(gtd_xxf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,0) + gtd_xxf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_yzf,dxf,0) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,0) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_zzf,dxf,0) + grad3DAxis(gtd_xzf,dxf,2) - grad3DAxis(gtd_xzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,0) + gtd_xzf*grad3DAxis(chif,dxf,2) - gtd_xzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,2,1,2] = 0.5*(gtu_xzf*(chif*(grad3DAxis(gtd_xzf,dxf,1) + grad3DAxis(gtd_xyf,dxf,2) - grad3DAxis(gtd_xzf,dxf,0) ) 
            - ( gtd_xzf*grad3DAxis(chif,dxf,1) + gtd_xyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,0)))
        + gtu_yzf*(chif*(grad3DAxis(gtd_yzf,dxf,1) + grad3DAxis(gtd_yyf,dxf,2) - grad3DAxis(gtd_yzf,dxf,1) ) 
            - ( gtd_yzf*grad3DAxis(chif,dxf,1) + gtd_yyf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,1))) 
        + gtu_zzf*(chif*(grad3DAxis(gtd_zzf,dxf,1) + grad3DAxis(gtd_yzf,dxf,2) - grad3DAxis(gtd_yzf,dxf,2) ) 
            - ( gtd_zzf*grad3DAxis(chif,dxf,1) + gtd_yzf*grad3DAxis(chif,dxf,2) - gtd_yzf*grad3DAxis(chif,dxf,2))) 
        )

    christoffel_udd[:,:,:,0,1,0] = christoffel_udd[:,:,:,0,0,1]
    christoffel_udd[:,:,:,0,2,0] = christoffel_udd[:,:,:,0,0,2]
    christoffel_udd[:,:,:,0,2,1] = christoffel_udd[:,:,:,0,1,2]

    christoffel_udd[:,:,:,1,1,0] = christoffel_udd[:,:,:,1,0,1]
    christoffel_udd[:,:,:,1,2,0] = christoffel_udd[:,:,:,1,0,2]
    christoffel_udd[:,:,:,1,2,1] = christoffel_udd[:,:,:,1,1,2]

    christoffel_udd[:,:,:,2,1,0] = christoffel_udd[:,:,:,2,0,1]
    christoffel_udd[:,:,:,2,2,0] = christoffel_udd[:,:,:,2,0,2]
    christoffel_udd[:,:,:,2,2,1] = christoffel_udd[:,:,:,2,1,2]

    return christoffel_udd





def strain3D(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    strain = np.zeros((Nx,Ny,Nz,6))

    dxfx = (np.roll(fx,-1,axis=0) - np.roll(fx,1,axis=0) )/(2.*dx)
    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    dxfz = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    dyfy = (np.roll(fy,-1,axis=1) - np.roll(fy,1,axis=1) )/(2.*dx)
    dyfz = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)

    dzfx = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    dzfy = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)
    dzfz = (np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx)

    strain[:,:,:,0] = 0.5*(dxfx + dxfx)
    strain[:,:,:,1] = 0.5*(dyfy + dyfy)
    strain[:,:,:,2] = 0.5*(dzfz + dzfz)
    strain[:,:,:,3] = 0.5*(dxfy + dyfx)
    strain[:,:,:,4] = 0.5*(dyfz + dzfy)
    strain[:,:,:,5] = 0.5*(dzfx + dxfz)

    return strain


def strain3D_traceless(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    strain = np.zeros((Nx,Ny,Nz,6))

    dxfx = (np.roll(fx,-1,axis=0) - np.roll(fx,1,axis=0) )/(2.*dx)
    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    dxfz = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    dyfy = (np.roll(fy,-1,axis=1) - np.roll(fy,1,axis=1) )/(2.*dx)
    dyfz = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)

    dzfx = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    dzfy = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)
    dzfz = (np.roll(fz,-1,axis=2) - np.roll(fz,1,axis=2) )/(2.*dx)

    trace = strain[:,:,:,0] + strain[:,:,:,1] + strain[:,:,:,2]
    strain[:,:,:,0] = 0.5*(dxfx + dxfx) - trace/3.
    strain[:,:,:,1] = 0.5*(dyfy + dyfy) - trace/3.
    strain[:,:,:,2] = 0.5*(dzfz + dzfz) - trace/3.
    strain[:,:,:,3] = 0.5*(dxfy + dyfx)
    strain[:,:,:,4] = 0.5*(dyfz + dzfy)
    strain[:,:,:,5] = 0.5*(dzfx + dxfz)

    return strain


def vorticity3D(fx,fy,fz,dx):
    
    Nx = fx.shape[0]
    Ny = fx.shape[1]
    Nz = fx.shape[2]
    vorticity = np.zeros((Nx,Ny,Nz,3))

    dxfy = (np.roll(fy,-1,axis=0) - np.roll(fy,1,axis=0) )/(2.*dx)
    dxfz = (np.roll(fz,-1,axis=0) - np.roll(fz,1,axis=0) )/(2.*dx)

    dyfx = (np.roll(fx,-1,axis=1) - np.roll(fx,1,axis=1) )/(2.*dx)
    dyfz = (np.roll(fz,-1,axis=1) - np.roll(fz,1,axis=1) )/(2.*dx)

    dzfx = (np.roll(fx,-1,axis=2) - np.roll(fx,1,axis=2) )/(2.*dx)
    dzfy = (np.roll(fy,-1,axis=2) - np.roll(fy,1,axis=2) )/(2.*dx)

    vorticity[:,:,:,0] = 0.5*(dxfy - dyfx)
    vorticity[:,:,:,1] = 0.5*(dyfz - dzfy)
    vorticity[:,:,:,2] = 0.5*(dzfx - dxfz)

    return vorticity


def vorticity3D_x(folder,dx):
    
    field = readData3D(folder+"vy_0.hdf5","vy")
    df1 = (np.roll(field,-1,axis=0) - np.roll(field,1,axis=0) )/(2.*dx)
    field = readData3D(folder+"vx_0.hdf5","vx")
    return 0.5*(df1 -(np.roll(field,-1,axis=1) - np.roll(field,1,axis=1) )/(2.*dx))

def vorticity3D_y(folder,dx):
    
    field = readData3D(folder+"vz_0.hdf5","vz")
    df1 = (np.roll(field,-1,axis=1) - np.roll(field,1,axis=1) )/(2.*dx)
    field = readData3D(folder+"vy_0.hdf5","vy")
    return 0.5*(df1 -(np.roll(field,-1,axis=2) - np.roll(field,1,axis=2) )/(2.*dx))

def vorticity3D_z(folder,dx):
    
    field = readData3D(folder+"vx_0.hdf5","vx")
    df1 = (np.roll(field,-1,axis=2) - np.roll(field,1,axis=2) )/(2.*dx)
    field = readData3D(folder+"vz_0.hdf5","vz")
    return 0.5*(df1 -(np.roll(field,-1,axis=0) - np.roll(field,1,axis=0) )/(2.*dx))


# Defining spectral dominion.
# Assumes N even, so that information in Fourier space is from 0 to N/2
'''
def fourier_space_3D(L,Npoints):

    dx = L/Npoints
    Ns = Npoints/2 + 1
    dk = 2.*math.pi/L
    nk = np.arange((Ns))
    k = nk*dk
    krad = np.zeros((Ns,Ns,Ns))
    for i in range(0,Ns):
     for j in range(0,Ns):
      for m in range(0,Ns):
       krad[i,j,m] = np.sqrt(k[i]*k[i] + k[j]*k[j] + k[m]*k[m])
    kp=k[0:Ns]

    return kp, krad
'''
#Optimized
def fourier_space_3D(L,Npoints):

    dx = L/Npoints
    Ns = Npoints/2 + 1
    dk = 2.*math.pi/L
    nk = np.arange((Ns))
    k = nk*dk
    dx2 = dx*dx
    krad = np.sqrt(np.indices((Ns,Ns,Ns))[0]*np.indices((Ns,Ns,Ns))[0]*dx2 + np.indices((Ns,Ns,Ns))[1]*np.indices((Ns,Ns,Ns))[1]*dx2 + np.indices((Ns,Ns,Ns))[2]*np.indices((Ns,Ns,Ns))[2]*dx2)
    kp=k[0:Ns]

    return kp, krad

def psd3Drad(field,kp,krad):

    N  = field.shape[0]
    Ns = kp.shape[0]
    dk = kp[1]-kp[0]

    FT = fftn(field)
    power = FT.real*FT.real + FT.imag*FT.imag
    power = FT.real[:Ns,:Ns,:Ns]*FT.real[:Ns,:Ns,:Ns] + FT.imag[:Ns,:Ns,:Ns]*FT.imag[:Ns,:Ns,:Ns]
    psd3Drad=np.zeros(Ns)
    
    for s in range(0,Ns):
        indices = np.where( (krad >= kp[s] - 0.5*dk) & (krad < kp[s] + 0.5*dk))
        psd3Drad[s] = np.mean(krad[indices]**2*power[indices])
   
    norm = 4.*math.pi/(dk**3.*N**6.)
    psd3Drad = psd3Drad*norm

    print "Parseval identity check by component: Integral of power spectrum int(E(k)dk): ",np.sum(psd3Drad)*dk
    print "                                      Integral over real space   int(f^2 dV): ",np.sum(field**2)/N**3.

    return psd3Drad




def pearson_scalar(tau,tau_model):

    n = tau.shape[0]**3

    coeff = np.sum(tau[:,:,:]*tau_model[:,:,:])/np.sum(tau_model[:,:,:]**2)
    a = np.reshape(tau[:,:,:],n)
    b = np.reshape(tau_model[:,:,:],n)
    pearson, pvalue = stats.pearsonr(a,b)
    coeff_err = np.std(b)/(np.sqrt(n)*np.std(a))

    print "===== PEARSON FOR SCALAR FIELD ====="
    print "Coefficient",coeff
    print "Coefficient errors:", coeff_err
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "====================="
    return pearson,pvalue,coeff,coeff_err



def pearson_9(tau,tau_model):

    pearson = np.zeros(9)
    pvalue = np.zeros(9)
    coeff = np.sum(tau[:,:,:,:]*tau_model[:,:,:,:], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:]**2, axis=(0, 1, 2))
    coeff_err = np.zeros(9)
    
    n = tau.shape[0]**3
    for i in xrange(0,9):
        a = np.reshape(tau[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        coeff_err[i] = np.std(b)/(np.sqrt(n)*np.std(a))

        pearson[i], pvalue[i] = stats.pearsonr(a,b)

    print "===== SYMMETRIC TENSOR ====="
    print "===== Components ===="
    print "Best-fit coefficient",coeff
    print "Coefficient errors:", coeff_err
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "= TRACE DECOMPOSITION ="
    print "Coefficient",np.mean(coeff[0:3]),np.mean(coeff[3:9])
    print "Coefficient errors",np.mean(coeff_err[0:3]),np.mean(coeff_err[3:9])
    print "Pearson coefficient",np.mean(pearson[0:3]),np.mean(pearson[3:9])
    print "====================="
    return pearson,pvalue,coeff,coeff_err

def pearson_6(tau,tau_model):

    pearson = np.zeros(6)
    pvalue = np.zeros(6)
    coeff = np.sum(tau[:,:,:,:]*tau_model[:,:,:,:], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:]**2, axis=(0, 1, 2))
    coeff_err = np.zeros(6)
    
    n = tau.shape[0]**3
    for i in xrange(0,6):
        a = np.reshape(tau[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        coeff_err[i] = np.std(b)/(np.sqrt(n)*np.std(a))

        pearson[i], pvalue[i] = stats.pearsonr(a,b)

    print "===== SYMMETRIC TENSOR ====="
    print "===== Components ===="
    print "Best-fit coefficient",coeff
    print "Coefficient errors:", coeff_err
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "= TRACE DECOMPOSITION ="
    print "Coefficient",np.mean(coeff[0:3]),np.mean(coeff[3:6])
    print "Coefficient errors",np.mean(coeff_err[0:3]),np.mean(coeff_err[3:6])
    print "Pearson coefficient",np.mean(pearson[0:3]),np.mean(pearson[3:6])
    print "====================="
    return pearson,pvalue,coeff,coeff_err

def pearson_3(tau,tau_model):

    pearson = np.zeros(3)
    pvalue = np.zeros(3)
    coeff = np.sum(tau[:,:,:,:3]*tau_model[:,:,:,:3], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:3]**2, axis=(0, 1, 2))
    coeff_err = np.zeros(3)

    n = tau.shape[0]**3
    for i in xrange(0,3):
        a = np.reshape(tau[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        coeff_err[i] = np.std(b)/(np.sqrt(n)*np.std(a))
        pearson[i], pvalue[i] = stats.pearsonr(a,b)

    print "= 3 COMPONENTS TENSOR/VECTOR ==="
    print "Coefficient",coeff
    print "Coefficient errors:", coeff_err
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "====================="
    return pearson,pvalue,coeff,coeff_err



def pearson_3_tmp(tau,tau_model):

    pearson = np.zeros(3)
    pvalue = np.zeros(3)
    coeff = np.sum(tau[:,:,:,:3]*tau_model[:,:,:,:3], axis=(0, 1, 2))/np.sum(tau_model[:,:,:,:3]**2, axis=(0, 1, 2))
    coeff_err = np.zeros(3)

    n = tau.shape[0]**3
    for i in xrange(0,3):
        a = np.reshape(tau[:,:,:,i],n)
        b = np.reshape(tau_model[:,:,:,i],n)
        coeff_err[i] = np.std(b)/(np.sqrt(n)*np.std(a))
        print a.shape
        a_tmp = a[fabs(a) > 1e-14]
        b_tmp = b[fabs(b) > 1e-14]
        print a_tmp.shape
        pearson[i], pvalue[i] = stats.pearsonr(a_tmp,b_tmp)

    print "= 3 COMPONENTS TENSOR/VECTOR ==="
    print "Coefficient",coeff
    print "Coefficient errors:", coeff_err
    print "Pearson coefficient",pearson
    print "p-value",pvalue
    print "====================="
    return pearson,pvalue,coeff,coeff_err


def statistics3D(tau):

    n = tau.shape[0]**3
    tau = np.reshape(tau,n)
    statistics = stats.describe(tau)
    # 0:nobs, 1:minmax (min,max), 2:mean, 3:variance, 4:skewness, 5:kurtosis

    return statistics[2:6]  # mean, variance, skew, kurtosis
       

