import numpy as np
from common_io_3D import readData3DWindow
from common_op_3D import avrg3D
import math

def equalsEq(a,b):
    return ((np.abs((a) - (b))/1e-15 > 10 ? false: (math.floor(np.abs((a) - (b))/1e-15) < 1)))

def con2prim_dominion(folder, Df, Sd_xf,Sd_yf,Sd_zf,Bu_xf,Bu_yf,Bu_zf,tauf, gamma_ideal,sf, w0, w1, w2, wSize):
# Con2prim: understand inputs and start with Minkowski. Keep in mind that this will have to be called N^3 times!
# Add dummy variables corresponding to Sdf... Add 1 and 0 to the inputs of con2prim

    print "Starting con2prim"
    D_atm = 0.
    sdetg = 1.0
    tau_atm = 1e-12
    chi =1.0
    inv_chi = 1.0/chi
    gtd_xx =1.0
    gtd_yy =1.0
    gtd_zz =1.0
    gtd_xy =0.0
    gtd_xz =0.0
    gtd_yz =0.0
    gtu_xx =1.0
    gtu_yy =1.0
    gtu_zz =1.0
    gtu_xy =0.0
    gtu_xz =0.0
    gtu_yz =0.0
    max_error = 1.e-14
    threshold_atm = 1.0
    threshold_sqSmax = 1.0
    threshold_sqBmax = 1.0

    rho_0 = 2.3683138398791271E+004
    rho_1 = 8.1178169274269310E-004
    rho_2 = 1.6197174195111443E-003
    gamma_0 = 1.35692
    gamma_1 = 3.005
    gamma_2 = 2.988
    gamma_3 = 2.851
    selected_gamma = gamma_0
    a_0 = 0.0
    a_1 = 1.0471001029394895E-002
    a_2 = 1.0241762117266431E-002
    a_3 = 2.3410189762359118E-003
    K_0 = 0.0
    K_1 = 0.0
    K_2 = 0.0
    K_3 = 0.0

    vd_x   = readData3DWindow(folder+"vfd_x_6.hdf5","vfd_x", w0, w1, w2, wSize)
    vd_x = avrg3D(vd_x,sf)
    vd_y   = readData3DWindow(folder+"vfd_y_6.hdf5","vfd_y", w0, w1, w2, wSize)
    vd_y = avrg3D(vd_y,sf)
    vd_z   = readData3DWindow(folder+"vfd_z_6.hdf5","vfd_z", w0, w1, w2, wSize)
    vd_z = avrg3D(vd_z,sf)

    rho = readData3DWindow(folder+"rhof_6.hdf5","rhof", w0, w1, w2, wSize)
    Nxf = rho.shape[0]/sf
    Nyf = rho.shape[1]/sf
    Nzf = rho.shape[2]/sf
    rhof = avrg3D(rho,sf)
    rho = None
    p  = readData3DWindow(folder+"pf_6.hdf5","pf", w0, w1, w2, wSize)
    pf = avrg3D(p,sf)
    p = None
    eps = readData3DWindow(folder+"epsf_6.hdf5","epsf", w0, w1, w2, wSize)
    epsf = avrg3D(eps,sf)
    eps = None
    

# I use this as first guesses

    rho_tilde = np.zeros((Nxf,Nyf,Nzf))
    p_tilde = np.zeros((Nxf,Nyf,Nzf))
    eps_tilde = np.zeros((Nxf,Nyf,Nzf))
    vd_x_tilde = np.zeros((Nxf,Nyf,Nzf))
    vd_y_tilde = np.zeros((Nxf,Nyf,Nzf))
    vd_z_tilde = np.zeros((Nxf,Nyf,Nzf))
    for i in xrange(0,Nxf):
      print "con2prim: x-slab ",i+1,"out of ",Nxf
      for j in xrange(0,Nxf):
        for k in xrange(0,Nxf):
          rho_tilde[i,j,k], p_tilde[i,j,k], eps_tilde[i,j,k], vd_x_tilde[i,j,k], vd_y_tilde[i,j,k], vd_z_tilde[i,j,k], sound_vel, Sfd_x2, Sfd_y2, Sfd_z2, tauf2 = con2prim(pf[i,j,k], epsf[i,j,k], rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3, selected_gamma, Df[i,j,k], D_atm, sdetg, Sd_xf[i,j,k], Sd_yf[i,j,k], Sd_zf[i,j,k], Sd_xf[i,j,k], Sd_yf[i,j,k], Sd_zf[i,j,k], tauf[i,j,k], tau_atm, 1., rhof[i,j,k]*(1. + epsf[i,j,k])+pf[i,j,k], chi, gtu_xx, gtu_xy, gtu_xz, gtu_yy, gtu_yz, gtu_zz, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz, Bu_xf[i,j,k], Bu_yf[i,j,k], Bu_zf[i,j,k], Bu_xf[i,j,k], Bu_yf[i,j,k], Bu_zf[i,j,k], Bu_xf[i,j,k], Bu_yf[i,j,k], Bu_zf[i,j,k], max_error, gamma_ideal, inv_chi, threshold_atm, threshold_sqSmax, threshold_sqBmax)

    print "End of con2prim"

# Check how much deviates from the average (almost 0 for Sf=1)
    print 'Mean abs dev. p',   np.mean(np.abs(1. - p_tilde/pf))
    print 'Mean abs dev. eps', np.mean(np.abs(1. - eps_tilde/epsf))
    print 'Mean abs dev. rho', np.mean(np.abs(1. - rho_tilde/rhof))
    print 'Mean abs dev. vx',  np.mean(np.abs(1. - vd_x_tilde/vd_x))
    print 'Mean abs dev. vy',  np.mean(np.abs(1. - vd_y_tilde/vd_y))
    print 'Mean abs dev. vz',  np.mean(np.abs(1. - vd_z_tilde/vd_z))

    print 'Max dev. p',   np.max(np.abs(1. - p_tilde/pf))
    print 'Max dev. eps', np.max(np.abs(1. - eps_tilde/epsf))
    print 'Max dev. rho', np.max(np.abs(1. - rho_tilde/rhof))
    print 'Max dev. vx',  np.max(np.abs(1. - vd_x_tilde/vd_x))
    print 'Max dev. vy',  np.max(np.abs(1. - vd_y_tilde/vd_y))
    print 'Max dev. vz',  np.max(np.abs(1. - vd_z_tilde/vd_z))

    return rho_tilde, p_tilde, eps_tilde, vd_x_tilde, vd_y_tilde, vd_z_tilde


def con2prim(pf, epsf, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3, selected_gamma, D, D_atm, sdetg, Sd_x, Sd_y, Sd_z, Su_x, Su_y, Su_z, tau, tau_atm, W, h, chi, gtu_xx, gtu_xy, gtu_xz, gtu_yy, gtu_yz, gtu_zz, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz, Bd_x, Bd_y, Bd_z, Bu_x, Bu_y, Bu_z, Bfu_x, Bfu_y, Bfu_z, max_error, gamma, inv_chi, threshold_atm, threshold_sqSmax, threshold_sqBmax):

    Sfd_x = 0.
    Sfd_y = 0.
    Sfd_z = 0.
    tauf = 0.

    if (D < (threshold_atm * D_atm)):
        rhof, pf, epsf, vfd_x, vfd_y, vfd_z, sqcs, selected_gamma, p_cold, eps_cold, D, h, Sfd_x, Sfd_y, Sfd_z, Bu_x, Bu_y, Bu_z, Bfd_x, Bfd_y, Bfd_z, Bd_x, Bd_y, Bd_z, tau, tauf = EOSRecovery(rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3, D_atm, sdetg, Bfu_x, Bfu_y, Bfu_z, inv_chi, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz)
    else:
        sqB = Bd_x * Bu_x + Bd_y * Bu_y + Bd_z * Bu_z
        sqBmax = threshold_sqBmax * D
        if (sqB > sqBmax):
            sqB_bound = np.sqrt(sqBmax / sqB)
            Bu_x, Bu_y, Bu_z, Bfu_x, Bfu_y, Bfu_z, Bfd_x, Bfd_y, Bfd_z, Bd_x, Bd_y, Bd_z = MAGRecovery(sqB_bound, sdetg, Bu_x, Bu_y, Bu_z, inv_chi, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz)
            sqB = Bd_x * Bu_x + Bd_y * Bu_y + Bd_z * Bu_z
        
        sqS = Sd_x * Su_x + Sd_y * Su_y + Sd_z * Su_z
        BS = Su_x * Bd_x + Su_y * Bd_y + Su_z * Bd_z
        if (tau < (threshold_atm * (tau_atm + 0.5 * sqB))):
            tau = tau_atm + 0.5 * sqB
            tauf = tau * sdetg
        
        sqSmax = threshold_sqSmax * (D * 2.0 + tau) * tau
        if (sqS > sqSmax):
            ss = np.sqrt(sqSmax / sqS)
            Sd_x = Sd_x * ss
            Sd_y = Sd_y * ss
            Sd_z = Sd_z * ss
            Su_x = chi * gtu_xx * Sd_x + chi * gtu_xy * Sd_y + chi * gtu_xz * Sd_z
            Su_y = chi * gtu_xy * Sd_x + chi * gtu_yy * Sd_y + chi * gtu_yz * Sd_z
            Su_z = chi * gtu_xz * Sd_x + chi * gtu_yz * Sd_y + chi * gtu_zz * Sd_z
            sqS = Sd_x * Su_x + Sd_y * Su_y + Sd_z * Su_z
            BS = Su_x * Bd_x + Su_y * Bd_y + Su_z * Bd_z
            Sfd_x = Sd_x * sdetg
            Sfd_y = Sd_y * sdetg
            Sfd_z = Sd_z * sdetg
        
        qvar = tau / D
        rvar = sqS / (D * D)
        svar = sqB / D
        tvar = BS / (np.power(D, (3.0 / 2.0)))
        xvar1 = (1.0 + qvar) - svar
        dis = np.abs(1.0 - (xvar1 * xvar1 * rvar + (2.0 * xvar1 + svar) * (tvar * tvar)) / ((xvar1 * xvar1) * ((xvar1 + svar) * (xvar1 + svar))))
        rhof = D * np.sqrt(dis)
        selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
        fl = (1.0 - ((gamma - 1.0) * dis) / gamma) * xvar1 + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (xvar1 * xvar1) + (-qvar) + (-p_cold / (gamma * D))
        xvar2 = 2.0 + 2.0 * qvar
        dis = np.abs(1.0 - (xvar2 * xvar2 * rvar + (2.0 * xvar2 + svar) * (tvar * tvar)) / ((xvar2 * xvar2) * ((xvar2 + svar) * (xvar2 + svar))))
        rhof = D * np.sqrt(dis)
        selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
        fh = (1.0 - ((gamma - 1.0) * dis) / gamma) * xvar2 + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (xvar2 * xvar2) + (-qvar) + (-p_cold / (gamma * D))
        iter = 0.0
        while ((fl * fh) > 0.0 and iter < 10.0):
            xvar1 = xvar1 * 0.8
            dis = np.abs(1.0 - (xvar1 * xvar1 * rvar + (2.0 * xvar1 + svar) * (tvar * tvar)) / ((xvar1 * xvar1) * ((xvar1 + svar) * (xvar1 + svar))))
            rhof = D * np.sqrt(dis)
            selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
            fl = (1.0 - ((gamma - 1.0) * dis) / gamma) * xvar1 + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (xvar1 * xvar1) + (-qvar) + (-p_cold / (gamma * D))
            xvar2 = xvar2 * 1.2
            dis = np.abs(1.0 - (xvar2 * xvar2 * rvar + (2.0 * xvar2 + svar) * (tvar * tvar)) / ((xvar2 * xvar2) * ((xvar2 + svar) * (xvar2 + svar))))
            rhof = D * np.sqrt(dis)
            selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
            fh = (1.0 - ((gamma - 1.0) * dis) / gamma) * xvar2 + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (xvar2 * xvar2) + (-qvar) + (-p_cold / (gamma * D))
            iter = iter + 1.0
        
        if ((fl * fh) < 0.0):
            if (equalsEq(fl, 0.0)):
                rts = xvar1
            
            if (equalsEq(fh, 0.0)):
                rts = xvar2
            
            if (fl < 0.0):
                xvarl = xvar1
                xvarh = xvar2
            else:
                xvarh = xvar1
                xvarl = xvar2
            
            if (not equalsEq(fl, 0.0) and not equalsEq(fh, 0.0)):
                h = rhof * (1.0 + epsf) + pf
                rts = (h * W * W) / D
                if (rts < xvarl or rts > xvarh or not rts == rts):
                    rts = 0.5 * (xvar1 + xvar2)
                
                dxold = np.abs(xvar2 - xvar1)
                dxvar = dxold
                dis = np.abs(1.0 - (rts * rts * rvar + (2.0 * rts + svar) * (tvar * tvar)) / ((rts * rts) * ((rts + svar) * (rts + svar))))
                rhof = D * np.sqrt(dis)
                selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                fx = (1.0 - ((gamma - 1.0) * dis) / gamma) * rts + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (rts * rts) + (-qvar) + (-p_cold / (gamma * D))
                ddis = 2.0 / ((rts * rts * rts) * ((rts + svar) * (rts + svar) * (rts + svar))) * (rvar * (rts * rts * rts) + (svar * svar) * (tvar * tvar) + 3.0 * svar * (tvar * tvar) * rts + 3.0 * (tvar * tvar) * (rts * rts))
                dfx = 1.0 + (tvar * tvar) / (rts * rts * rts) + (gamma - 1.0) / gamma * (ddis / (2.0 * np.sqrt(dis)) * (1.0 + eps_cold) + (-ddis * rts) + (-dis)) + (-0.5 * ddis * svar)
                iter = 0.0
                while (iter < 200.0):
                    if ((((rts - xvarh) * dfx - fx) * ((rts - xvarl) * dfx - fx)) > 0.0 or np.abs(2.0 * fx) > np.abs(dxold * dfx)):
                        dxold = dxvar
                        dxvar = 0.5 * (xvarh - xvarl)
                        rts = xvarl + dxvar
                        if (equalsEq(xvarl, rts)):
                            iter = 2000.0
                        
                    else:
                        dxold = dxvar
                        dxvar = fx / dfx
                        rts = rts - dxvar
                        if (equalsEq(rts, (rts + dxvar))):
                            iter = 2000.0
                        
                    
                    if (iter < 2000.0):
                        if (np.abs(dxvar) < max_error):
                            iter = 2000.0
                        else:
                            dis = np.abs(1.0 - (rts * rts * rvar + (2.0 * rts + svar) * (tvar * tvar)) / ((rts * rts) * ((rts + svar) * (rts + svar))))
                            W = 1.0 / np.sqrt(dis)
                            rhof = D * np.sqrt(dis)
                            selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                            fx = (1.0 - ((gamma - 1.0) * dis) / gamma) * rts + ((gamma - 1.0) * np.sqrt(dis)) / gamma * (1.0 + eps_cold) - 1.0 + (1.0 - dis / 2.0) * svar + (-0.5) * (tvar * tvar) / (rts * rts) + (-qvar) + (-p_cold / (gamma * D))
                            ddis = 2.0 / ((rts * rts * rts) * ((rts + svar) * (rts + svar) * (rts + svar))) * (rvar * (rts * rts * rts) + (svar * svar) * (tvar * tvar) + 3.0 * svar * (tvar * tvar) * rts + 3.0 * (tvar * tvar) * (rts * rts))
                            dfx = 1.0 + (tvar * tvar) / (rts * rts * rts) + (gamma - 1.0) / gamma * (ddis / (2.0 * np.sqrt(dis)) * (1.0 + eps_cold) + (-ddis * rts) + (-dis)) + (-0.5 * ddis * svar)
                            if (fx < 0.0):
                                xvarl = rts
                            else:
                                xvarh = rts
                            iter = iter + 1.0
        else:
            qvar = tau / D
            rvar = sqS / (D * D)
            svar = sqB / D
            tvar = BS / (np.power(D, (3.0 / 2.0)))
            xvar1 = (1.0 + qvar) - svar
            xvar2 = 2.0 + 2.0 * qvar
            dis = np.abs(1.0 - (xvar1 * xvar1 * rvar + (2.0 * xvar1 + svar) * tvar * tvar) / (xvar1 * xvar1 * ((xvar1 + svar) * (xvar1 + svar))))
            rhof = D * np.sqrt(dis)
            selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
            h_cold = rhof * (1.0 + eps_cold) + p_cold
            fl = xvar1 - h_cold / (D * dis)
            dis = np.abs(1.0 - (xvar2 * xvar2 * rvar + (2.0 * xvar2 + svar) * tvar * tvar) / (xvar2 * xvar2 * ((xvar2 + svar) * (xvar2 + svar))))
            rhof = D * np.sqrt(dis)
            selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
            h_cold = rhof * (1.0 + eps_cold) + p_cold
            fh = xvar2 - h_cold / (D * dis)
            iter = 0.0
            while ((fl * fh) > 0.0 and iter < 10.0):
                xvar1 = xvar1 * 0.5
                xvar2 = xvar2 * 2.0
                dis = np.abs(1.0 - (xvar1 * xvar1 * rvar + (2.0 * xvar1 + svar) * tvar * tvar) / (xvar1 * xvar1 * ((xvar1 + svar) * (xvar1 + svar))))
                rhof = D * np.sqrt(dis)
                selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                h_cold = rhof * (1.0 + eps_cold) + p_cold
                fl = xvar1 - h_cold / (D * dis)
                dis = np.abs(1.0 - (xvar2 * xvar2 * rvar + (2.0 * xvar2 + svar) * tvar * tvar) / (xvar2 * xvar2 * ((xvar2 + svar) * (xvar2 + svar))))
                rhof = D * np.sqrt(dis)
                selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                h_cold = rhof * (1.0 + eps_cold) + p_cold
                fh = xvar2 - h_cold / (D * dis)
                iter = iter + 1.0
            
            if ((fl * fh) < 0.0):
                if (equalsEq(fl, 0.0)):
                    rts = xvar1
                
                if (equalsEq(fh, 0.0)):
                    rts = xvar2
                
                if (fl < 0.0):
                    xvarl = xvar1
                    xvarh = xvar2
                else:
                    xvarl = xvar2
                    xvarh = xvar1
                
                if (not equalsEq(fl, 0.0) and not equalsEq(fh, 0.0)):
                    h = rhof * (1.0 + epsf) + pf
                    rts = (h * W * W) / D
                    if (rts < xvarl or rts > xvarh or not rts == rts):
                        rts = 0.5 * (xvar1 + xvar2)
                    
                    dxold = np.abs(xvar2 - xvar1)
                    dxvar = dxold
                    dis = np.abs(1.0 - (rts * rts * rvar + (2.0 * rts + svar) * tvar * tvar) / (rts * rts * ((rts + svar) * (rts + svar))))
                    rhof = D * np.sqrt(dis)
                    selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                    h_cold = rhof * (1.0 + eps_cold) + p_cold
                    fx = rts - h_cold / (D * dis)
                    ddis = 2.0 / ((rts * rts * rts) * ((rts + svar) * (rts + svar) * (rts + svar))) * (rvar * (rts * rts * rts) + (svar * svar) * (tvar * tvar) + 3.0 * svar * (tvar * tvar) * rts + 3.0 * (tvar * tvar) * (rts * rts))
                    dfx = 1.0 - (ddis * (selected_gamma * p_cold - h_cold)) / (2.0 * dis * dis * D)
                    iter = 0.0
                    while (iter < 200.0):
                        if ((((rts - xvarh) * dfx - fx) * ((rts - xvarl) * dfx - fx)) > 0.0 or np.abs(2.0 * fx) > np.abs(dxold * dfx)):
                            dxold = dxvar
                            dxvar = 0.5 * (xvarh - xvarl)
                            rts = xvarl + dxvar
                            if (equalsEq(xvarl, rts)):
                                iter = 2000.0
                            
                        else:
                            dxold = dxvar
                            dxvar = fx / dfx
                            rts = rts - dxvar
                            if (equalsEq(rts, (rts + dxvar))):
                                iter = 2000.0
                            
                        
                        if (iter < 2000.0):
                            if (np.abs(dxvar) < max_error):
                                iter = 2000.0
                            else:
                                dis = np.abs(1.0 - (rts * rts * rvar + (2.0 * rts + svar) * (tvar * tvar)) / ((rts * rts) * ((rts + svar) * (rts + svar))))
                                W = 1.0 / np.sqrt(dis)
                                rhof = D * np.sqrt(dis)
                                selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
                                h_cold = rhof * (1.0 + eps_cold) + p_cold
                                fx = rts - h_cold / (D * dis)
                                ddis = 2.0 / ((rts * rts * rts) * ((rts + svar) * (rts + svar) * (rts + svar))) * (rvar * (rts * rts * rts) + (svar * svar) * (tvar * tvar) + 3.0 * svar * (tvar * tvar) * rts + 3.0 * (tvar * tvar) * (rts * rts))
                                dfx = 1.0 - (ddis * (selected_gamma * p_cold - h_cold)) / (2.0 * dis * dis * D)
                                if (fx < 0.0):
                                    xvarl = rts
                                else:
                                    xvarh = rts
                                
                                iter = iter + 1.0
                            
                        
                    
                
            else:
                rhof, pf, epsf, vfd_x, vfd_y, vfd_z, sqcs, selected_gamma, p_cold, eps_cold, D, h, Sfd_x, Sfd_y, Sfd_z, Bu_x, Bu_y, Bu_z, Bfd_x, Bfd_y, Bfd_z, Bd_x, Bd_y, Bd_z, tau, tauf = EOSRecovery(rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3, D_atm, sdetg, Bfu_x, Bfu_y, Bfu_z, inv_chi, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz)
                return rhof, pf, epsf, vfd_x, vfd_y, vfd_z, sqcs, Sfd_x, Sfd_y, Sfd_z, tauf
            
        
        dis = np.abs(1.0 - (rts * rts * rvar + (2.0 * rts + svar) * (tvar * tvar)) / ((rts * rts) * ((rts + svar) * (rts + svar))))
        W = 1.0 / np.sqrt(dis)
        rhof = D / W
        h = rts * dis * D
        selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
        pf = (gamma - 1.0) / gamma * (h + (-rhof) + (-rhof * eps_cold)) + p_cold / gamma
        pf = np.maximum(pf, p_cold)
        epsf = eps_cold + (pf - p_cold) / ((gamma - 1.0) * rhof)
        epsf = np.maximum(epsf, eps_cold)
        vfd_x = (Sd_x / D + (tvar * Bd_x) / (rts * np.sqrt(D))) / (rts + svar)
        vfd_y = (Sd_y / D + (tvar * Bd_y) / (rts * np.sqrt(D))) / (rts + svar)
        vfd_z = (Sd_z / D + (tvar * Bd_z) / (rts * np.sqrt(D))) / (rts + svar)
        if (epsf > eps_cold):
            h = rhof * (1.0 + epsf) + pf
            sqcs = np.abs((gamma * pf + (selected_gamma - gamma) * p_cold) / h)
        else:
            h_cold = rhof * (1.0 + eps_cold) + p_cold
            sqcs = np.abs((selected_gamma * p_cold) / h_cold)

    return rhof, pf, epsf, vfd_x, vfd_y, vfd_z, sqcs, Sfd_x, Sfd_y, Sfd_z, tauf

        


def EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3):
    if rhof > rho_2:
        selected_gamma = gamma_3
        p_cold = K_3 * np.power(rhof, gamma_3)
        eps_cold = a_3 + K_3 / (gamma_3 - 1.0) * np.power(rhof, (gamma_3 - 1.0))
    else:
        if rhof >= rho_1:
            selected_gamma = gamma_2
            p_cold = K_2 * np.power(rhof, gamma_2)
            eps_cold = a_2 + K_2 / (gamma_2 - 1.0) * np.power(rhof, (gamma_2 - 1.0))
        else:
            if rhof >= rho_0:
                selected_gamma = gamma_1
                p_cold = K_1 * np.power(rhof, gamma_1)
                eps_cold = a_1 + K_1 / (gamma_1 - 1.0) * np.power(rhof, (gamma_1 - 1.0))
            else:
                selected_gamma = gamma_0
                p_cold = K_0 * np.power(rhof, gamma_0)
                eps_cold = a_0 + K_0 / (gamma_0 - 1.0) * np.power(rhof, (gamma_0 - 1.0))
    return selected_gamma, p_cold, eps_cold
        
    
def EOSRecovery(rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3, D_atm, sdetg, Bfu_x, Bfu_y, Bfu_z, inv_chi, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz):


    D = D_atm
    Df = D * sdetg
    rhof = D
    selected_gamma, p_cold, eps_cold = EOSCOLD(rhof, rho_0, rho_1, rho_2, a_0, a_1, a_2, a_3, K_0, K_1, K_2, K_3, gamma_0, gamma_1, gamma_2, gamma_3)
    pf = p_cold
    epsf = eps_cold
    vfd_x = 0.0
    vfd_y = 0.0
    vfd_z = 0.0
    h = rhof * (1.0 + epsf) + pf
    sqcs = np.abs((selected_gamma * p_cold) / h)
    Sfd_x = 0.0
    Sfd_y = 0.0
    Sfd_z = 0.0
    Bu_x = Bfu_x / sdetg
    Bu_y = Bfu_y / sdetg
    Bu_z = Bfu_z / sdetg
    Bfd_x = inv_chi * (gtd_xx * Bfu_x + gtd_xy * Bfu_y + gtd_xz * Bfu_z)
    Bfd_y = inv_chi * (gtd_xy * Bfu_x + gtd_yy * Bfu_y + gtd_yz * Bfu_z)
    Bfd_z = inv_chi * (gtd_xz * Bfu_x + gtd_yz * Bfu_y + gtd_zz * Bfu_z)
    Bd_x = Bfd_x / sdetg
    Bd_y = Bfd_y / sdetg
    Bd_z = Bfd_z / sdetg
    sqB = Bd_x * Bu_x + Bd_y * Bu_y + Bd_z * Bu_z
    W = 1.0
    tau = h * W * W + (-pf) + 0.5 * sqB + (-Df)
    tauf = tau * sdetg

    return rhof, pf, epsf, vfd_x, vfd_y, vfd_z, sqcs, selected_gamma, p_cold, eps_cold, D, h, Sfd_x, Sfd_y, Sfd_z, Bu_x, Bu_y, Bu_z, Bfd_x, Bfd_y, Bfd_z, Bd_x, Bd_y, Bd_z, tau, tauf


def MAGRecovery(sqB_bound, sdetg, Bu_x, Bu_y, Bu_z, inv_chi, gtd_xx, gtd_xy, gtd_xz, gtd_yy, gtd_yz, gtd_zz):

    Bu_x = Bu_x * sqB_bound
    Bu_y = Bu_y * sqB_bound
    Bu_z = Bu_z * sqB_bound
    Bfu_x = Bu_x * sdetg
    Bfu_y = Bu_y * sdetg
    Bfu_z = Bu_z * sdetg
    Bfd_x = inv_chi * (gtd_xx * Bfu_x + gtd_xy * Bfu_y + gtd_xz * Bfu_z)
    Bfd_y = inv_chi * (gtd_xy * Bfu_x + gtd_yy * Bfu_y + gtd_yz * Bfu_z)
    Bfd_z = inv_chi * (gtd_xz * Bfu_x + gtd_yz * Bfu_y + gtd_zz * Bfu_z)
    Bd_x = Bfd_x / sdetg
    Bd_y = Bfd_y / sdetg
    Bd_z = Bfd_z / sdetg

    return Bu_x, Bu_y, Bu_z, Bfu_x, Bfu_y, Bfu_z, Bfd_x, Bfd_y, Bfd_z, Bd_x, Bd_y, Bd_z


