from merger_spectra_parallel_mpiFix_par_impar import merger_spectra_parallel


def main():

    level = 6
    level_int = "6"
    #folders = {"../outputDir_mesh_c4": [512000,544000,576000,608000,640000,672000,704000]}
    folders = {"../outputDir_mesh_c4": [512000]}
    labels = ['Complete', 'Tororidal-Poloidal']
    dt = 4/(2**level)
    dx = 10.24/(2**level)
    li = "-"
    col = ['k', 'g',  'r']

    Npoints = 376
    folderout = "../Spectra/level6/mask5e-5/"
    Ncycle = 7
    L = 1.
    imag = 1.

    merger_spectra_parallel(folders,labels,dx,dt,li,col,Npoints,Ncycle,folderout,L,imag, level)


if __name__ == "__main__":
    main()

