from merger_spectra_parallel_mpiFix_par_impar_interp import merger_spectra_parallel


def main():

    level = 7
    level_int = "7"
    folders = {"../outputDir_mesh_c4": [512000,640000,768000,896000]}
#    folders = {"../outputDir_mesh_c4": [512000]}
    labels = ['Complete', 'Tororidal-Poloidal']
    dt = 4/(2**level)
    dx = 10.24/(2**level)
    li = "-"
    col = ['k', 'g',  'r']

    Npoints = 301
    folderout = "../Spectra/level7_interp/mask5e-5/box12/"
    Ncycle = 4
    L = 1#24.*1.47407
    imag = 1.

    merger_spectra_parallel(folders,labels,dx,dt,li,col,Npoints,Ncycle,folderout,L,imag, level)


if __name__ == "__main__":
    main()

