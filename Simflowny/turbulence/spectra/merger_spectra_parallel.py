import pyfftw 
import numpy as np
import h5py
import os
import math
import sys
from mpi4py import MPI
from scipy.fftpack import fftn,fft
from common_io_3D import readData3DSlab
from common_op_3D import fourier_space_3D, int3D
from common_plots import plot_spectra_poltor


def merger_spectra_parallel(folders,labels, dx,dt,li,col,Npoints,Ncycle,folderout,gamma,L,imag):

    comm = MPI.COMM_WORLD
    num_processes = comm.Get_size()
    rank = comm.Get_rank()
   
    time = np.zeros((Ncycle))

    kp, krad = fourier_space_3D(L,Npoints)
    Ns = kp.shape[0]
    spekinevo = np.zeros((Ns,Ncycle))
    spekinevo_pol = np.zeros((Ns,Ncycle))
    spekinevo_tor = np.zeros((Ns,Ncycle))
    spemagevo = np.zeros((Ns,Ncycle))
    spemagevo_pol = np.zeros((Ns,Ncycle))
    spemagevo_tor = np.zeros((Ns,Ncycle))
    
    Nproc = (Npoints - 1) // num_processes


    folderout_r = folderout
    if not os.path.exists(folderout_r):
        os.makedirs(folderout_r)

    cc = 0
    for f in folders:
        for i in folders[f]:
            print i, dt
            time[cc] = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5)+"/"

            #Integral calculation
            int_ekin = readData3DSlab(foldertime+"vfd_x.hdf5","vfd_x", rank, Nproc)
            int_ekin = int_ekin * int_ekin
            int_ekin = int_ekin + readData3DSlab(foldertime+"vfd_y.hdf5","vfd_y", rank, Nproc)*readData3DSlab(foldertime+"vfd_y.hdf5","vfd_y", rank, Nproc)
            int_ekin = int_ekin + readData3DSlab(foldertime+"vfd_z.hdf5","vfd_z", rank, Nproc)*readData3DSlab(foldertime+"vfd_z.hdf5","vfd_z", rank, Nproc)
            int_ekin = int_ekin * readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            int_ekin = int3D(int_ekin, dx)
            int_emag = readData3DSlab(foldertime+"Bfu_x.hdf5","Bfu_x", rank, Nproc)
            int_emag = int_emag * int_emag
            int_emag = int_emag + readData3DSlab(foldertime+"Bfu_y.hdf5","Bfu_y", rank, Nproc)*readData3DSlab(foldertime+"Bfu_y.hdf5","Bfu_y", rank, Nproc)
            int_emag = int_emag + readData3DSlab(foldertime+"Bfu_z.hdf5","Bfu_z", rank, Nproc)*readData3DSlab(foldertime+"Bfu_z.hdf5","Bfu_z", rank, Nproc)
            int_emag = int3D(int_emag, dx)
            outname = folderout_r+"/integral_"+str("%.2f" % time[cc]).zfill(5)+".out"
            result = np.array(0.0)
            int_ekin = np.array(int_ekin)
            print int_ekin
            comm.Reduce([int_ekin, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            print result
            int_ekin = np.array(result)
            print int_emag
            int_emag = np.array(int_emag)
            comm.Reduce([int_emag, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            print result
            int_emag = np.array(result)
            print int_ekin, int_emag
            #Integral write
            if rank == 0:
                f3=open(outname,"w+")
                f3.write("t, INT(Ekin), INT(Emag) \n")
                f3.write("%12.3e %12.3e %12.3e" % (time[cc],int_ekin,int_emag))
                f3.close()


            #KINETIC
            #Complete
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = np.sqrt(field)*readData3DSlab(foldertime+"vfd_x.hdf5","vfd_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = field            
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = np.sqrt(field)*readData3DSlab(foldertime+"vfd_y.hdf5","vfd_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = spekinevo[:,cc] + field
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = np.sqrt(field)*readData3DSlab(foldertime+"vfd_z.hdf5","vfd_z", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = spekinevo[:,cc] + field

            x = readData3DSlab(foldertime+"x.hdf5","x", rank, Nproc)
            y = readData3DSlab(foldertime+"y.hdf5","y", rank, Nproc)
            r = np.sqrt(x**2 + y **2)        
            r[r == 0] = 0.0000000000001    

            #Poloidal
            spekinevo_pol[:,cc] = field      # Z component
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = x / r * np.sqrt(field) * readData3DSlab(foldertime+"vfd_x.hdf5","vfd_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo_pol[:,cc] = spekinevo_pol[:,cc] + field
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = y / r * np.sqrt(field) * readData3DSlab(foldertime+"vfd_y.hdf5","vfd_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo_pol[:,cc] = spekinevo_pol[:,cc] + field
 
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = -y / r * np.sqrt(field) * readData3DSlab(foldertime+"vfd_x.hdf5","vfd_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo_tor[:,cc] = field
            field = readData3DSlab(foldertime+"rhof.hdf5","rhof", rank, Nproc)
            field = x / r * np.sqrt(field) * readData3DSlab(foldertime+"vfd_y.hdf5","vfd_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo_tor[:,cc] = spekinevo_tor[:,cc] + field

            #MAGNETIC
            #Complete
            field = readData3DSlab(foldertime+"Bfu_x.hdf5","Bfu_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = field
            field = readData3DSlab(foldertime+"Bfu_y.hdf5","Bfu_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = spemagevo[:,cc] + field
            field = readData3DSlab(foldertime+"Bfu_z.hdf5","Bfu_z", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = spemagevo[:,cc] + field

            #Poloidal
            spemagevo_pol[:,cc] = field         # Z component
            field = x / r * readData3DSlab(foldertime+"Bfu_x.hdf5","Bfu_x", rank, Nproc) 
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field
            field = y / r  * readData3DSlab(foldertime+"Bfu_y.hdf5","Bfu_y", rank, Nproc) 
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field

            #Toroidal
            field = -y / r * readData3DSlab(foldertime+"Bfu_x.hdf5","Bfu_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_tor[:,cc] = field
            field = x / r * readData3DSlab(foldertime+"Bfu_y.hdf5","Bfu_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_tor[:,cc] = spemagevo_tor[:,cc] + field

            header = "k, Spe(Ekin), Spe(Emag), Spe(Ekin_pol), Spe(Ekin_tor), Spe(Emag_pol), Spe(Emag_tor) \n"
            outname = folderout_r+"/spectrum_"+str("%.2f" % time[cc]).zfill(5)+".out"
            if rank == 0:
                f2=open(outname,"w+")
                f2.write(header)
                for kk in xrange(0,Ns,1):
                  f2.write("%12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e \n" % (kp[kk],spekinevo[kk,cc],spemagevo[kk,cc],spekinevo_pol[kk,cc],spekinevo_tor[kk,cc],spemagevo_pol[kk,cc],spemagevo_tor[kk,cc]))

                f2.close()
                print "Written spectra at: ",outname
            cc = cc + 1

    if not os.path.exists(folderout):
        os.makedirs(folderout)

    if rank == 0:
       for cc in xrange(0, cc, 1):
            plot_spectra_poltor(folderout,time[cc],kp,spekinevo[:,cc],spemagevo[:,cc],spekinevo_pol[:,cc],spekinevo_tor[:,cc],spemagevo_pol[:,cc],spemagevo_tor[:,cc],labels,li,col,imag)


def fftn_mpi(u, Npoints, Nproc, num_processes, comm):

        print "fftn_mpi starts"
        u = pyfftw.builders.rfft2(u, axes=(1, 2))()
        print "Uc_hat obtained"

        u = np.rollaxis(u.reshape(Nproc, num_processes, Nproc, (Npoints - 1)//2+1), 1).reshape((Npoints - 1, Nproc, (Npoints - 1)//2+1))
        print "reshaped"

        comm.Alltoall(MPI.IN_PLACE, [u, MPI.DOUBLE_COMPLEX])
        print "joining"

        u[:] = pyfftw.builders.fft(u, axis=0)()

        return u


def psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm):

        Ns = kp.shape[0]

        rank = comm.Get_rank()

        field = fftn_mpi(field, Npoints, Nproc, num_processes, comm)


        field = field.real[:Ns,:,:]*field.real[:Ns,:,:] + field.imag[:Ns,:,:]*field.imag[:Ns,:,:]
        psd3Drad=np.zeros(Ns,dtype=np.double)
        psd3Dradrcv=np.zeros(Ns,dtype=np.double)
        counter=np.zeros(Ns,dtype=np.int)
        counterrcv=np.zeros(Ns,dtype=np.int)
        dk=kp[1]-kp[0]

        for s in range(0,Ns):
            indices = np.where( (krad >= kp[s] - 0.5*dk) & (krad < kp[s] + 0.5*dk))

            x = []
            y = []
            z = []
            for i in xrange(indices[0].shape[0]):
                if indices[1][i] >= rank * Nproc and indices[1][i] < (rank + 1) * Nproc:
                    x.append(indices[0][i])
                    y.append(indices[1][i])
                    z.append(indices[2][i])
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            newIndices = (x, y, z)
            newIndicesRed = (x, y - rank * Nproc, z)
            
            if x.shape[0] > 0:
#                print rank, ":", s, np.min(y), np.max(y)
                psd3Drad[s] = np.sum(krad[newIndices]**2*field[newIndicesRed])
                counter[s] = x.shape[0]

        comm.Allreduce(psd3Drad, psd3Dradrcv, MPI.SUM)
        comm.Allreduce(counter, counterrcv, MPI.SUM)

        norm = 4.*math.pi/(dk**3*(Npoints-1)**6)

        return psd3Dradrcv*norm/counterrcv


