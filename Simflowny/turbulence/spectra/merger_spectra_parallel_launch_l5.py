from merger_spectra_parallel import merger_spectra_parallel


def main():




    folders = {"/gpfs/scratch/pr1ehy00/pr1ehy01/neutron_star_piecewiseEoS_weno5z_newmagneticfield5/binary_mag_level5/outputDir_full3d_c1": range(409600, 768001, 51200), "/gpfs/scratch/pr1ehy00/pr1ehy01/neutron_star_piecewiseEoS_weno5z_newmagneticfield5/binary_mag_level5/outputDir_full3d_c2": range(819200, 921601, 51200)}

    labels = ["Complete", "Toroidal-Poloidal"]
    dt = 2.0/(2**9)
    dx = 0.16666666666666666
    li = "-"
    col = ['k','g', 'r']

    Npoints = 169
    folderout = "/gpfs/projects/pr1ehy00/codes/binaryNS_spectra/SPECTRA_3D_l5/"
    Ncycle = 11
    gamma = 4./3.
    L = 1.
    imag = 1.

    merger_spectra_parallel(folders,labels,dx, dt,li,col,Npoints,Ncycle,folderout,gamma,L,imag)


if __name__ == "__main__":
    main()

