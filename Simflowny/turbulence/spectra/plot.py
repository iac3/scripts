import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py


def readData(file):
    sl = []
    sl_2 = []
    t = []

    skipheader = True
    with open(file) as fp:  
        for l in fp.readlines():
            if skipheader:
                skipheader = False
            else:
                columns = l.split()
                print columns
                t.append(float(columns[0]))
                sl.append(float(columns[1]))
                sl_2.append(float(columns[2]))

    t = np.asarray(t)
    sl = np.asarray(sl)
    sl_2 = np.asarray(sl_2)

    return t, sl, sl_2

def main():

    t_5, sl5, sl5_2 = readData('./level5/SPECTRA_3D_l5/spectrum_2000.00.out')    
    t_6, sl6, sl6_2 = readData('./level6/SPECTRA_3D_l6/spectrum_2000.00.out')
    t_7, sl7, sl7_2 = readData('./level7/SPECTRA_3D_l7/spectrum_2000.00.out')
    t_8, sl8, sl8_2 = readData('./level8/SPECTRA_3D_l8/spectrum_2020.00.out')


    plt.figure()
    plt.xlabel('Wavenumber k')
    plt.ylabel('Spectra')
    plt.plot(t_5, sl5, label=r'$\Delta x$ = 250 m', linestyle="-", color = 'g')
    plt.plot(t_5, sl5_2, label=r'$\Delta x$ = 250 m', linestyle="--", color = 'g')
    plt.plot(t_6, sl6, label=r'$\Delta x$ = 125 m', linestyle="-", color = 'b')
    plt.plot(t_6, sl6_2, label=r'$\Delta x$ = 125 m', linestyle="--", color = 'b')
    plt.plot(t_7, sl7, label=r'$\Delta x$ = 62 m', linestyle="-", color = 'r')
    plt.plot(t_7, sl7_2, label=r'$\Delta x$ = 62 m', linestyle="--", color = 'r')
    plt.plot(t_8, sl8, label=r'$\Delta x$ = 31 m', linestyle="-", color = 'k')
    plt.plot(t_8, sl8_2, label=r'$\Delta x$ = 31 m', linestyle="--", color = 'k')
    plt.legend(loc = 1)
    plt.yscale('log')
    plt.xscale('log')
    plt.savefig('./plot.png')   



if __name__ == "__main__":
    main()
