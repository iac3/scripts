import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt



def getIntegrals(folder, level, times):

    emag = np.zeros((len(times)))
    ekin = np.zeros((len(times)))
    
    i = 0
    for t in times:
        data = np.loadtxt(folder + '/integral_' + str(t) + '0.out', skiprows=1)
        ekin[i] = data[1]
        emag[i] = data[2]
        i = i + 1

    return ekin/2, emag

def main():

    time = [1480.0]
    ekin_pre, emag_pre = getIntegrals("./level5/SPECTRA_3D_l5/", 5, time)

    times_5 = np.arange(1600.0,3600.1,200)
    ekin_5, emag_5 = getIntegrals("./level5/SPECTRA_3D_l5/", 5, times_5)

    times_6 = np.arange(1600.0,3600.1,200)
    ekin_6, emag_6 = getIntegrals("./level6/SPECTRA_3D_l6/", 6, times_6)

    times_7 = np.arange(1600.0,3400.1,200)
    ekin_7, emag_7 = getIntegrals("./level7/SPECTRA_3D_l7/", 7, times_7)

    times_8 = np.arange(1620.0,2620.1,200)
    ekin_8, emag_8 = getIntegrals("./level8/SPECTRA_3D_l8/", 8, times_8)

    dt = 1.0/200
    times_5 = (np.append(time, times_5) - time) * dt
    times_6 = (np.append(time, times_6) - time) * dt
    times_7 = (np.append(time, times_7) - time) * dt
    times_8 = (np.append(time, times_8) - time) * dt

    ekin_5 = np.append(ekin_pre, ekin_5)
    ekin_6 = np.append(ekin_pre, ekin_6)
    ekin_7 = np.append(ekin_pre, ekin_7)
    ekin_8 = np.append(ekin_pre, ekin_8)

    emag_5 = np.append(emag_pre, emag_5)
    emag_6 = np.append(emag_pre, emag_6)
    emag_7 = np.append(emag_pre, emag_7)
    emag_8 = np.append(emag_pre, emag_8)
 
    dx = 0.16666666666666666 * 1.5

    plt.figure()
    plt.xlabel(r'$t - t_{merger}$ (ms)')
    plt.ylabel(r'$E_{mag}/E_{kin}$')
    plt.plot(times_5, emag_5/ekin_5, label=r'$\Delta x$ = 250 m', linestyle="-", color = 'g')
    plt.plot(times_6, emag_6/ekin_6, label=r'$\Delta x$ = 125 m', linestyle="-", color = 'b')
    plt.plot(times_7, emag_7/ekin_7, label=r'$\Delta x$ = 62 m', linestyle="-", color = 'r')
    plt.plot(times_8, emag_8/ekin_8, label=r'$\Delta x$ = 31 m', linestyle="-", color = 'k')
    plt.legend(loc = 1)
    plt.yscale('log')
    plt.savefig('e_integrals2.png')


if __name__ == "__main__":
    main()
