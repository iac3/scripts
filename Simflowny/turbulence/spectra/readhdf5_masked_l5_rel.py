import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt

def interpolate_function(p1, p2, R, p):
    return (R * p1 - p * p1 + p * p2)/R

def getData(folder, field, level, box_init, nullField, default_value):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']
    dx = f_sum['/BASIC_INFO/dx'][level]
    init = f_sum['/BASIC_INFO/XLO'][:]
    ratio = f_sum['/BASIC_INFO/ratios_to_coarser_levels'][level]

    maximum = np.asarray([0, 0, 0])
    minimum = np.asarray([999999, 999999, 999999])
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum = np.maximum(maximum, patchExtents[i][1])
            minimum = np.minimum(minimum, patchExtents[i][0])

    offset = (minimum - (box_init - init) / dx).astype(int)

    size = ((2 * abs(box_init)) / dx).astype(int) + 1
    data = np.zeros(shape=size)
    data[:,:,:] = default_value

    if not nullField:
        for iPatch in range(len(patchExtents)):
            if (patchMap[iPatch][2] == level):
                iProc = patchMap[iPatch][0]
                iProcStr = str(iProc).zfill(5)
                iPatchStr = str(patchMap[iPatch][3]).zfill(5)
                rangeX = (max(patchExtents[iPatch][0][0] - minimum[0] + offset[0], 0), min(patchExtents[iPatch][1][0]+2 - minimum[0] + offset[0], size[0]))
                rangeY = (max(patchExtents[iPatch][0][1] - minimum[1] + offset[1], 0), min(patchExtents[iPatch][1][1]+2 - minimum[1] + offset[1], size[1]))
                rangeZ = (max(patchExtents[iPatch][0][2] - minimum[2] + offset[2], 0), min(patchExtents[iPatch][1][2]+2 - minimum[2] + offset[2], size[2]))
                if np.all(patchExtents[iPatch][0] - minimum + offset < size) and np.all(patchExtents[iPatch][1]+2 - minimum + offset >= 0):
                    sizePatch = patchExtents[iPatch][1] - patchExtents[iPatch][0] + 2;
                    f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
                    minx = 0
                    if patchExtents[iPatch][0][0] - minimum[0] + offset[0] < 0:
                        minx = 0 - (patchExtents[iPatch][0][0] - minimum[0] + offset[0])
                    maxx = sizePatch[0]
                    if patchExtents[iPatch][1][0]+2 - minimum[0] + offset[0] > size[0]:
                        maxx = size[0] - (patchExtents[iPatch][0][0] - minimum[0] + offset[0])
                    miny = 0
                    if patchExtents[iPatch][0][1] - minimum[1] + offset[1] < 0:
                        miny = 0 - (patchExtents[iPatch][0][1] - minimum[1] + offset[1])
                    maxy = sizePatch[1]
                    if patchExtents[iPatch][1][1]+2 - minimum[1] + offset[1] > size[1]:
                        maxy = size[1] - (patchExtents[iPatch][0][1] - minimum[1] + offset[1])
                    minz = 0
                    if patchExtents[iPatch][0][2] - minimum[2] + offset[2] < 0:
                        minz = 0 - (patchExtents[iPatch][0][2] - minimum[2] + offset[2])
                    maxz = sizePatch[2]
                    if patchExtents[iPatch][1][2]+2 - minimum[2] + offset[2] > size[2]:
                        maxz = size[2] - (patchExtents[iPatch][0][2] - minimum[2] + offset[2])
                    #print "min/max ", minx, maxx, miny, maxy, minz, maxz, "size patch", sizeX, sizeY, sizeZ, "patchExtents", patchExtents[iPatch][0][:], patchExtents[iPatch][1][:], "ranges", rangeX, rangeY, rangeZ, "indices", patchExtents[iPatch][0] - minimum + offset, patchExtents[iPatch][1]+2 - minimum + offset
                    tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
                    tmp = np.reshape(tmp, sizePatch, order="F")
                    data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1],rangeZ[0]:rangeZ[1]] = tmp[minx:maxx,miny:maxy,minz:maxz]
                    f_data.close()
    f_sum.close()
    return data, minimum - offset, init, dx[0]

def getBox(folder, field, level, box_init, mask, nullField, default_value):

    data, minimum, init, dx = getData(folder, field, level, box_init, nullField, default_value)

    #Apply mask
    print type(mask)
    if type(mask) is float:
        mask = data < mask
    data[mask] = default_value

    #Writing the variable to disk
    f = h5py.File(folder + '/' + field + '.hdf5', "w")
    dset = f.create_dataset(field, data.shape, dtype='float64')
    dset[:,:,:] = data[:,:,:]
    f.close()

    ''' 
    plt.figure()
    extent = init[1] + (minimum[1]) * dx, init[1] + (minimum[1] + data.shape[1]) * dx, init[0] + (minimum[0]) * dx, init[0] + (minimum[0] + data.shape[0]) * dx
    plt.imshow(data[:,:,data.shape[2]/2], cmap='gist_ncar', extent=extent, origin='lower')
    plt.savefig('./' + field + '.png')
    '''

    return mask

def getCoords(folder, level, box_init):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    dx = f_sum['/BASIC_INFO/dx'][level]
    init = f_sum['/BASIC_INFO/XLO'][:]

    size = ((2 * abs(box_init)) / dx).astype(int) + 1
    coord = np.ndarray(shape=size)

    for i in range(size[0]):
        coord[i,:,:] = init[0] + ((box_init - init[0]) / dx[0]).astype(int) * dx[0] + i *dx[0]

    f = h5py.File(folder + '/x_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset('x', coord.shape, dtype='float64')
    dset[:,:,:] = coord[:,:,:]
    f.close()

    for j in range(size[1]):
        coord[:,j,:] = init[1] + ((box_init - init[1]) / dx[1]).astype(int) * dx[1] + j *dx[1]

    f = h5py.File(folder + '/y_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset('y', coord.shape, dtype='float64')
    dset[:,:,:] = coord[:,:,:]
    f.close()


def main():
    
    level = 5
    folders = {"/gpfs/scratch/pr1ehy00/pr1ehy01/neutron_star_piecewiseEoS_weno5z_newmagneticfield5/binary_mag_level5/outputDir_full3d_c1": range(409600, 768001, 51200), "/gpfs/scratch/pr1ehy00/pr1ehy01/neutron_star_piecewiseEoS_weno5z_newmagneticfield5/binary_mag_level5/outputDir_full3d_c2": range(819200, 921601, 51200)}
    box_init = -14    #In domain units
    mask = 1e-7
    for f in folders:
        for i in folders[f]:
            
            mask = getBox(f + "/visit_dump." + str(i).zfill(5), "rhof", level, box_init, mask, False, 1e-12)
            
            getBox(f + "/visit_dump." + str(i).zfill(5), "Alpha", level, box_init, mask, False, 1)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Betau_x", level, box_init, mask, True, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Betau_y", level, box_init, mask, True, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Betau_z", level, box_init, mask, True, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_xx", level, box_init, mask, False, 1)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_yy", level, box_init, mask, False, 1)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_zz", level, box_init, mask, False, 1)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_xy", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_xz", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "gtd_yz", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "chi", level, box_init, mask, False, 1)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Df", level, box_init, mask, False, 1e-12)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Sfd_x", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Sfd_y", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Sfd_z", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "pf", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "tauf", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "epsf", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "vfd_x", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "vfd_y", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "vfd_z", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Bfu_x", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Bfu_y", level, box_init, mask, False, 0)
            getBox(f + "/visit_dump." + str(i).zfill(5), "Bfu_z", level, box_init, mask, False, 0)            

            getCoords(f + "/visit_dump." + str(i).zfill(5), level, box_init)

if __name__ == "__main__":
    main()
