#import pyfftw 
import numpy as np
import h5py
import os
import math
import sys
from mpi4py import MPI
#from scipy.fftpack import fftn,fft
from common_io_2D import readData2DAll
#from common_op_2D import fourier_space_3D, int2D
from common_op_2D import int2D
#from common_plots import plot_spectra_poltor


def merger_integral_parallel_mpiFix_2D(folders,labels, dx,dt,li,col,Npoints,Ncycle,folderout,L,imag,level):

    comm = MPI.COMM_WORLD
    num_processes = comm.Get_size()
    rank = comm.Get_rank()
   
    time = np.zeros((Ncycle))

    #kp, krad = fourier_space_3D(L,Npoints)
    '''
    Ns = kp.shape[0]
    spekinevo = np.zeros((Ns,Ncycle))
    spekinevo_pol = np.zeros((Ns,Ncycle))
    spekinevo_tor = np.zeros((Ns,Ncycle))
    spemagevo = np.zeros((Ns,Ncycle))
    spemagevo_pol = np.zeros((Ns,Ncycle))
    spemagevo_tor = np.zeros((Ns,Ncycle))
    '''
    Nproc = (Npoints - 1) // num_processes
    dt = 0.03125

    folderout_r = folderout
    if not os.path.exists(folderout_r):
        os.makedirs(folderout_r)

    outname = folderout_r+"/integral.out"
    if rank == 0:
        f3=open(outname,"w+")
        f3.write("t, INT(Ekin), INT(Emag), BRMS, INT(D), MAX(rho) \n")

    cc = 0
    for f in folders:
        for i in folders[f]:
            #print i, dt
            time[cc] = i*dt
            foldertime= f + "/visit_dump."+str(i).zfill(5)+"/level7_interp/mask5e-5/box12/"

            #Integral calculation
            int_ekin = readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x")
            print (int_ekin.shape)
            int_ekin = int_ekin * int_ekin
            int_ekin = int_ekin + readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")*readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y")
            int_ekin = int_ekin + readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")*readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z")
            int_ekin = int_ekin * readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
            int_ekin = int2D(int_ekin, dx)
            int_emag = readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x")
            int_emag = int_emag * int_emag
            int_emag = int_emag + readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")*readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y")
            int_emag = int_emag + readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")*readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z")
            int_emag = int2D(int_emag, dx)
            volume = readData2DAll(foldertime+"mask_"+str(level)+".hdf5","mask")
            volume = np.logical_not(volume)
            volume = int2D(volume,dx)
            BRMS = np.sqrt(int_emag)
            
            result = np.array(0.0)
            int_ekin = np.array(int_ekin)
            #print int_ekin
            comm.Reduce([int_ekin, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            #print result
            int_ekin = np.array(result)
            #print int_emag
            int_emag = np.array(int_emag)
            comm.Reduce([int_emag, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            #print result
            int_emag = np.array(result)
            volume = np.array(volume)
            comm.Reduce([volume, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            #print result
            volume = np.array(result)
            BRMS = np.array(BRMS)
            comm.Reduce([BRMS, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            #print result
            BRMS = np.array(result/volume)
            #print int_ekin, int_emag
            #Integral D
            int_D = readData2DAll(foldertime+"Df_"+str(level)+".hdf5","Df")
            int_D = int2D(int_D, dx)
            int_D = np.array(int_D)
            comm.Reduce([int_D, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.SUM, root=0)
            int_D = np.array(result)
            #Max rho
            max_rho = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof")
            max_rho = np.max(max_rho)
            max_rho = np.array(max_rho)
            comm.Reduce([max_rho, MPI.DOUBLE], [result, MPI.DOUBLE], op=MPI.MAX, root=0)
            max_rho = np.array(result)
            #Integral write
            if rank == 0:
                f3.write("%12.3e %12.3e %12.3e %12.3e %12.3e %12.3e\n" % (time[cc],int_ekin,int_emag, BRMS, int_D, max_rho))

            '''
            #KINETIC
            #Complete
            print("Kinetic:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic:Faig vfdx")
            field = np.sqrt(field)*readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = field
            print("Kinetic:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic:Faig vfdy")
            field = np.sqrt(field)*readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = spekinevo[:,cc] + field
            print("Kinetic:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic:Faig vfdz")
            field = np.sqrt(field)*readData2DAll(foldertime+"vfd_z_"+str(level)+".hdf5","vfd_z", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spekinevo[:,cc] = spekinevo[:,cc] + field

            print("Kinetic:Faig x")
            x = readData2DAll(foldertime+"x_"+str(level)+".hdf5","x", rank, Nproc)
            print("Kinetic:Faig y")
            y = readData2DAll(foldertime+"y_"+str(level)+".hdf5","y", rank, Nproc)
            r = np.sqrt(x**2 + y **2)        
            r[r == 0] = 0.0000000000001

            #Poloidal
            spekinevo_pol[:,cc] = field      # Z component
            print("Kinetic polo:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic polo:Faig vfdx")
            field = x / r * np.sqrt(field) * readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x", rank, Nproc)
            print("Kinetic polo:Faig psd3Drad")
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            print("Kinetic polo:Faig spekinevo_pol")
            spekinevo_pol[:,cc] = spekinevo_pol[:,cc] + field
            print("Kinetic polo:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic polo:Faig vfdy")
            field = y / r * np.sqrt(field) * readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y", rank, Nproc)
            print("Kinetic polo:Faig psd3Drad")
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            print("Kinetic polo:Faig spekinevo_pol")
            spekinevo_pol[:,cc] = spekinevo_pol[:,cc] + field
 
            print("Kinetic polo:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic polo:Faig -y/r*sqrt(rho)*vfdx")
            field = -y / r * np.sqrt(field) * readData2DAll(foldertime+"vfd_x_"+str(level)+".hdf5","vfd_x", rank, Nproc)
            print("Kinetic polo:Faig psd3Drad")
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            print("Kinetic polo:Faig spekinevo_pol")
            spekinevo_tor[:,cc] = field
            print("Kinetic polo:Faig rhof")
            field = readData2DAll(foldertime+"rhof_"+str(level)+".hdf5","rhof", rank, Nproc)
            print("Kinetic polo:Faig x/r*sqrt(rho)*vfdy")
            field = x / r * np.sqrt(field) * readData2DAll(foldertime+"vfd_y_"+str(level)+".hdf5","vfd_y", rank, Nproc)
            print("Kinetic polo:Faig psd3Drad")
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            print("Kinetic polo:Faig spekinevo_pol")
            spekinevo_tor[:,cc] = spekinevo_tor[:,cc] + field

            #MAGNETIC
            #Complete
            print("Magnetic:Faig Bfux")
            field = readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = field
            print("Magnetic:Faig Bfuy")
            field = readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = spemagevo[:,cc] + field
            print("Magnetic:Faig Bfuz")
            field = readData2DAll(foldertime+"Bfu_z_"+str(level)+".hdf5","Bfu_z", rank, Nproc)
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo[:,cc] = spemagevo[:,cc] + field

            #Poloidal
            print("Magnetic polo:Faig Component Z")
            spemagevo_pol[:,cc] = field #Aprovecho que esta calculada la componente Z para anadirla al poloidal directamente. Me ahorro unas 2-3 horas de calculo
            #Calculo B^x + Btheta*sin(theta). Primero Btheta:
            print("Magnetic polo:Faig Component X")
            field = -y / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) + x / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
            field = readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) + y / r * field
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field
            #Calculo B^y - Btheta*cos(theta). Primero Btheta:
            print("Magnetic polo:Faig Component Y")
            field = -y / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) + x / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
            field = readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc) - x / r * field
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field
            #Bpoloidal completado

            #Toroidal
            #Btheta
            print("Magnetic toro:Faig Component X")
            field = -y / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) + x / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
            #Componente X
            field = - y / r * field
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_tor[:,cc] = field
            #He perdido la definicion de Btheta, asi que lo calculo de nuevo
            #Btheta
            print("Magnetic toro:Faig Component Y")
            field = -y / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) + x / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
            #Componente Y
            field = x / r * field
            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
            spemagevo_tor[:,cc] = spemagevo_tor[:,cc] + field
            '''
#            spemagevo_pol[:,cc] = spemagevo[:,cc] - spemagevo_tor[:,cc]   #Aqui en teoria ya tiene todo el B (spemagevo) y le resto el toroidal (spemagevo_tor). Se puede hacer asi en python, no




            #Poloidal
#            spemagevo_pol[:,cc] = field         # Z component
#            print("Magnetic polo:Faig Bfux")
#            field = x / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc) 
#            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
#            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field
#            print("Magnetic polo:Faig Bfuy")
#            field = y / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc) 
#            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
#            spemagevo_pol[:,cc] = spemagevo_pol[:,cc] + field

            #Toroidal
#            print("Magnetic toro:Faig Bfux")
#            field = -y / r * readData2DAll(foldertime+"Bfu_x_"+str(level)+".hdf5","Bfu_x", rank, Nproc)
#            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
#            spemagevo_tor[:,cc] = field
#            print("Magnetic toro:Faig Bfuy")
#            field = x / r * readData2DAll(foldertime+"Bfu_y_"+str(level)+".hdf5","Bfu_y", rank, Nproc)
#            field = psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm)
#            spemagevo_tor[:,cc] = spemagevo_tor[:,cc] + field
            '''
            header = "k, Spe(Ekin), Spe(Emag), Spe(Ekin_pol), Spe(Ekin_tor), Spe(Emag_pol), Spe(Emag_tor) \n"
            outname = folderout_r+"/spectrum_"+str("%.2f" % time[cc]).zfill(5)+".out"
            if rank == 0:
                f2=open(outname,"w+")
                f2.write(header)
                for kk in xrange(0,Ns,1):
                  f2.write("%12.3e %12.3e %12.3e %12.3e %12.3e %12.3e %12.3e \n" % (kp[kk],spekinevo[kk,cc],spemagevo[kk,cc],spekinevo_pol[kk,cc],spekinevo_tor[kk,cc],spemagevo_pol[kk,cc],spemagevo_tor[kk,cc]))

                f2.close()
                #print "Written spectra at: ",outname
            cc = cc + 1
            '''
    '''
    if not os.path.exists(folderout):
        os.makedirs(folderout)

    if rank == 0:
       for cc in xrange(0, cc, 1):
            plot_spectra_poltor(folderout,time[cc],kp,spekinevo[:,cc],spemagevo[:,cc],spekinevo_pol[:,cc],spekinevo_tor[:,cc],spemagevo_pol[:,cc],spemagevo_tor[:,cc],labels,li,col,imag)
    '''

    if rank == 0:
        f3.close()

'''
def fftn_mpi(u, Npoints, Nproc, num_processes, comm):

        #print "fftn_mpi starts"
        u = pyfftw.builders.rfft2(u, axes=(1, 2))()
        #print "Uc_hat obtained"

        u = np.rollaxis(u.reshape(Nproc, num_processes, Nproc, (Npoints - 1)//2+1), 1).reshape((Npoints - 1, Nproc, (Npoints - 1)//2+1))
        #print "reshaped"
        oldshape = u.shape
        print (u.shape)
        u = u.flatten()
        print (u.size)

        nchunks = 15
        size_chunk = u.size // nchunks
        for i in range(nchunks):
            indices = np.arange(size_chunk, dtype=int) * nchunks + i
            print("Chunk", i, "de",nchunks)
            print("Indices", indices.shape, "inicio",indices[0])
            temp = u[indices]
            print("temp", temp.shape)
            comm.Alltoall(MPI.IN_PLACE, [temp, MPI.DOUBLE_COMPLEX])
            u[indices] = temp

        #comm.Alltoall(MPI.IN_PLACE, [u, MPI.DOUBLE_COMPLEX])
        #print "joining"

        u = u.reshape(oldshape)

        u[:] = pyfftw.builders.fft(u, axis=0)()

        return u


def psd3Drad_parallel(field,kp,krad, Npoints, Nproc, num_processes, comm):

        Ns = kp.shape[0]

        rank = comm.Get_rank()

        field = fftn_mpi(field, Npoints, Nproc, num_processes, comm)

        field = field.real[:Ns,:,:]*field.real[:Ns,:,:] + field.imag[:Ns,:,:]*field.imag[:Ns,:,:]
        psd3Drad=np.zeros(Ns,dtype=np.double)
        psd3Dradrcv=np.zeros(Ns,dtype=np.double)
        counter=np.zeros(Ns,dtype=np.int)
        counterrcv=np.zeros(Ns,dtype=np.int)
        dk=kp[1]-kp[0]
        for s in range(0,Ns):
            print(s)
            indices = np.where( (krad >= kp[s] - 0.5*dk) & (krad < kp[s] + 0.5*dk))

            x = []
            y = []
            z = []
            for i in xrange(indices[0].shape[0]):
                if indices[1][i] >= rank * Nproc and indices[1][i] < (rank + 1) * Nproc:
                    x.append(indices[0][i])
                    y.append(indices[1][i])
                    z.append(indices[2][i])
            x = np.asarray(x)
            y = np.asarray(y)
            z = np.asarray(z)
            newIndices = (x, y, z)
            newIndicesRed = (x, y - rank * Nproc, z)
            
            if x.shape[0] > 0:
#                print rank, ":", s, np.min(y), np.max(y)
                psd3Drad[s] = np.sum(krad[newIndices]**2*field[newIndicesRed])
                counter[s] = x.shape[0]

        comm.Allreduce(psd3Drad, psd3Dradrcv, MPI.SUM)
        comm.Allreduce(counter, counterrcv, MPI.SUM)

        norm = 4.*math.pi/(dk**3*(Npoints-1)**6)

        return psd3Dradrcv*norm/counterrcv


'''
