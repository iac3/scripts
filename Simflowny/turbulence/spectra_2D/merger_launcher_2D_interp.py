from merger_integral_parallel_mpiFix_2D_interp import merger_integral_parallel_mpiFix_2D


def main():

    level = 7
    level_int = "7"
    folders = {"../outputDir_slice0_c3": range(390400, 1126400+1, 6400)}
    labels = ['Complete', 'Tororidal-Poloidal']
    dt = 4/(2**level)
    dx = 10.24/(2**level)
    li = "-"
    col = ['k', 'g',  'r']

    Npoints = 301
    folderout = "../Integral_automatic/level7_interp/mask5e-5/box12/"
    Ncycle = 116
    L = 1.
    imag = 1.

    merger_integral_parallel_mpiFix_2D(folders,labels,dx,dt,li,col,Npoints,Ncycle,folderout,L,imag, level)


if __name__ == "__main__":
    main()

