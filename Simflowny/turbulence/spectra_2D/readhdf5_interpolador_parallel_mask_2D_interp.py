import sys, getopt
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt
from multiprocessing import Pool
from itertools import repeat

def readData2DAll(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v], dtype='int')
    f.close()
    return dset[...]

def interpolate2D_function_R2(p11, p12, p21, p22):
    return 0.25 * p11 + 0.25 * p12 + 0.25 * p21 + 0.25 * p22

def interpolate2D_function(p11, p12, p21, p22, Rx, Ry, px, py):
    if Rx == 2 and Ry == 2:
        return 0.25 * p11 + 0.25 * p12 + 0.25 * p21 + 0.25 * p22
    return p11 + (p21 - p11) * px/Rx + (p12 - p11) * py/Ry + (p22 + p11 - (p21 + p12)) * px/Rx * py/Ry

def interpolate_function_R2(p1, p2):
    return 0.5 * p1 + 0.5 * p2

def interpolate_function(p1, p2, R, p):
    return (R * p1 - p * p1 + p * p2)/R

#@profile
def getData(folder, field, level, box_init, nullField, default_value, interpolation):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    nProcessors  = f_sum['/BASIC_INFO/number_processors']
    patchExtents = f_sum['/extents/patch_extents']
    patchMap     = f_sum['/extents/patch_map']
    varNames = f_sum['/BASIC_INFO/var_names']
    dx = f_sum['/BASIC_INFO/dx'][level][:2]
    init = f_sum['/BASIC_INFO/XLO'][:2]
    ratio = f_sum['/BASIC_INFO/ratios_to_coarser_levels'][level][:2]

    maximum = np.asarray([0, 0, 0])
    minimum = np.asarray([999999, 999999, 999999])
    for i in range(len(patchExtents)):
        if (patchMap[i][2] == level):
            maximum = np.maximum(maximum, patchExtents[i][1])
            minimum = np.minimum(minimum, patchExtents[i][0])

    maximum = maximum[:2]
    minimum = minimum[:2]

    offset = (minimum - (box_init - init) / dx).astype(int)

    size = ((2 * abs(box_init)) / dx).astype(int) + 1
    data = np.zeros(shape=size)
    if interpolation:
        data.fill(np.nan)
    if nullField or not interpolation:
        data.fill(default_value)

    if not nullField:
        for iPatch in range(len(patchExtents)):
            if (patchMap[iPatch][2] == level):
                iProc = patchMap[iPatch][0]
                iProcStr = str(iProc).zfill(5)
                iPatchStr = str(patchMap[iPatch][3]).zfill(5)
                rangeX = (max(patchExtents[iPatch][0][0] - minimum[0] + offset[0], 0), min(patchExtents[iPatch][1][0]+2 - minimum[0] + offset[0], size[0]))
                rangeY = (max(patchExtents[iPatch][0][1] - minimum[1] + offset[1], 0), min(patchExtents[iPatch][1][1]+2 - minimum[1] + offset[1], size[1]))
                if np.all(patchExtents[iPatch][0][:2] - minimum + offset < size) and np.all(patchExtents[iPatch][1][:2]+2 - minimum + offset >= 0):
                    sizePatch = patchExtents[iPatch][1][:2] - patchExtents[iPatch][0][:2] + 2;
                    f_data = h5py.File(folder + '/processor_cluster.' + iProcStr + '.samrai', "r")
                    minx = 0
                    if patchExtents[iPatch][0][0] - minimum[0] + offset[0] < 0:
                        minx = 0 - (patchExtents[iPatch][0][0] - minimum[0] + offset[0])
                    maxx = sizePatch[0]
                    if patchExtents[iPatch][1][0]+2 - minimum[0] + offset[0] > size[0]:
                        maxx = size[0] - (patchExtents[iPatch][0][0] - minimum[0] + offset[0])
                    miny = 0
                    if patchExtents[iPatch][0][1] - minimum[1] + offset[1] < 0:
                        miny = 0 - (patchExtents[iPatch][0][1] - minimum[1] + offset[1])
                    maxy = sizePatch[1]
                    if patchExtents[iPatch][1][1]+2 - minimum[1] + offset[1] > size[1]:
                        maxy = size[1] - (patchExtents[iPatch][0][1] - minimum[1] + offset[1])
                    #print "min/max ", minx, maxx, miny, maxy, minz, maxz, "size patch", sizeX, sizeY, sizeZ, "patchExtents", patchExtents[iPatch][0][:], patchExtents[iPatch][1][:], "ranges", rangeX, rangeY, rangeZ, "indices", patchExtents[iPatch][0] - minimum + offset, patchExtents[iPatch][1]+2 - minimum + offset
                    tmp = f_data['/processor.' + iProcStr + '/level.' + str(level).zfill(5) + '/patch.' + iPatchStr + '/' + field]
                    tmp = np.reshape(tmp, sizePatch, order="F")
                    data[rangeX[0]:rangeX[1],rangeY[0]:rangeY[1]] = tmp[minx:maxx,miny:maxy]
                    f_data.close()
    f_sum.close()
    return data, minimum - offset, np.any(offset > 0), init, dx[0], ratio

#@profile
def getBoxLevel(folder, field, level, box_init, nullField, default_value, interpolation):
    data, minimum, incomplete, init, dx, ratio = getData(folder, field, level, box_init, nullField, default_value, interpolation);
    # Only need to interpolate if any offset is greater than 0
    if incomplete and interpolation and not nullField:
        parentData, parentMinimum, parentIncomplete, parentInit, parentdx, parentRatio = getBoxLevel(folder, field, level - 1, box_init - 3*dx, nullField, default_value, interpolation);
        #print data.shape, parentData.shape, minimum, parentMinimum
        #Fill nan data with interpolation from parent level
        if ratio[0] == 2 and ratio[1] == 2:
            for (i,j), v in np.ndenumerate(data):
                if np.isnan(v):
                    i_real = i + minimum[0]
                    j_real = j + minimum[1]
                    i_parent = i_real // 2 - parentMinimum[0]
                    j_parent = j_real // 2 - parentMinimum[1]
                    px = i_real % 2
                    py = j_real % 2
                    i_align = px == 0
                    j_align = py == 0   
                    #print i_real, j_real, k_real, i_parent, j_parent, k_parent, minimum, parentMinimum
                    if j_align:
                        if i_align:
                            data[i, j] = parentData[i_parent, j_parent]
                        else:
                            data[i, j] = interpolate_function_R2(parentData[i_parent, j_parent], parentData[i_parent + 1, j_parent])
                    else:
                        if i_align:
                            data[i, j] = interpolate_function_R2(parentData[i_parent, j_parent], parentData[i_parent, j_parent + 1])
                        else:
                            data[i, j] = interpolate2D_function_R2(parentData[i_parent, j_parent], parentData[i_parent, j_parent + 1], parentData[i_parent + 1, j_parent], parentData[i_parent + 1, j_parent + 1])
        else:
            for (i,j), v in np.ndenumerate(data):
                if np.isnan(v):
                    i_real = i + minimum[0]
                    j_real = j + minimum[1]
                    i_parent = i_real // ratio[0] - parentMinimum[0]
                    j_parent = j_real // ratio[1] - parentMinimum[1]
                    px = i_real % ratio[0]
                    py = j_real % ratio[1]
                    i_align = px == 0
                    j_align = py == 0  
                    #print i_real, j_real, k_real, i_parent, j_parent, k_parent, minimum, parentMinimum
                    if j_align:
                        if i_align:
                            data[i, j] = parentData[i_parent, j_parent]
                        else:
                            data[i, j] = interpolate_function(parentData[i_parent, j_parent], parentData[i_parent + 1, j_parent], ratio[0], px)
                    else:
                        if i_align:
                            data[i, j] = interpolate_function(parentData[i_parent, j_parent], parentData[i_parent, j_parent + 1], ratio[1], py)
                        else:
                            data[i, j] = interpolate2D_function(parentData[i_parent, j_parent], parentData[i_parent, j_parent + 1], parentData[i_parent + 1, j_parent], parentData[i_parent + 1, j_parent + 1], ratio[0], ratio[1], px, py)

    return data, minimum, incomplete, init, dx, ratio

#@profile
def getBox(folderout, folder, flevel, fmask, field, level, box_init, mask, nullField, default_value, interpolation, read_mask):
    data, minimum, incomplete, init, dx, ratio = getBoxLevel(folder, field, level, box_init, nullField, default_value, interpolation)

    #Apply mask
   # print type(mask)
    if not read_mask:
        mask = data < mask
        f = h5py.File(folderout + '/mask_' + str(level) + '.hdf5', "w")
        dset = f.create_dataset('mask', mask.shape)
        dset[:,:] = mask[:,:]
        f.close()
    else:
        mask = readData2DAll(folderout+ '/mask_' + str(level) + '.hdf5', "mask")

    data[np.where(mask)] = default_value

    #Writing the variable to disk
    f = h5py.File(folderout + '/' + field + '_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset(field, data.shape, dtype='float64')
    dset[:,:] = data[:,:]
    f.close()
     
    plt.figure()
    extent = init[1] + (minimum[1]) * dx, init[1] + (minimum[1] + data.shape[1]) * dx, init[0] + (minimum[0]) * dx, init[0] + (minimum[0] + data.shape[0]) * dx
    plt.imshow(data[:,:], cmap='gist_ncar', extent=extent, origin='lower')
    plt.colorbar()
    plt.savefig(folderout + '/slice_' + field + '.png')
    

def getCoords(folderout, folder, flevel, fmask, level, box_init):
    #Read mesh information
    f_sum = h5py.File(folder + '/summary.samrai', "r")
    dx = f_sum['/BASIC_INFO/dx'][level][:2]
    init = f_sum['/BASIC_INFO/XLO'][:2]

    size = ((2 * abs(box_init)) / dx).astype(int) + 1
    coord = np.ndarray(shape=size)

    for i in range(size[0]):
        coord[i,:] = init[0] + ((box_init - init[0]) / dx[0]).astype(int) * dx[0] + i *dx[0]

    f = h5py.File(folderout + '/x_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset('x', coord.shape, dtype='float64')
    dset[:,:] = coord[:,:]
    f.close()

    for j in range(size[1]):
        coord[:,j] = init[1] + ((box_init - init[1]) / dx[1]).astype(int) * dx[1] + j *dx[1]

    f = h5py.File(folderout + '/y_' + str(level) + '.hdf5', "w")
    dset = f.create_dataset('y', coord.shape, dtype='float64')
    dset[:,:] = coord[:,:]
    f.close()

def getBox_unpack(args):
    return getBox(*args)

#@profile
def main():
    
    level = 7
#    folders = {"../outputDir_slice0_c3": range(390400, 710400+1, 6400)}
#    folders = {"../outputDir_slice0_c3": range(716800, 921600+1, 6400)}
    folders = {"../outputDir_slice0_c3": range(928000, 1126400+1, 6400)}
    #Ejemplo con diferentes carpetas de output
    #folders = {"../outputDir_slice0_c3": range(928000, 1024000+1, 6400), "../outputDir_slice0_c4": range(1024000, 1126400+1, 6400)}
    box_init = -12.0    #In domain units
    mask = 5e-5
    flevel = "level7_interp"
    fmask = 'mask5e-5'
    interpolation = True

    fields = ["Df", "vfd_x", "vfd_y", "vfd_z", "Bfu_x", "Bfu_y", "Bfu_z", "Sfd_x", "Sfd_y", "Sfd_z"]
    nullFields = [False, False, False, False, False, False, False, False, False, False]
    default_values = [1e-12, 0, 0, 0, 0, 0, 0, 0, 0, 0]
#    fields1 = ["Df"]
#    fields2 = ["vfd_x"]
#    fields3 = ["vfd_y"]
#    fields4 = ["vfd_z"]
#    fields5 = ["Bfu_x"]
#    fields6 = ["Bfu_y"]
#    fields7 = ["Bfu_z"]
#    fields8 = ["Sfd_x"]
#    fields9 = ["Sfd_y"]
#    fields10 = ["Sfd_z"]
#    nullFields = [False]
#    default_values1 = [1e-12]
#    default_values2 = [0]
    pool= Pool(10)
    for f in folders:
        for i in folders[f]:
            folderout = f + "/visit_dump." + str(i).zfill(5) + '/' + flevel + '/' + fmask + '/box12'
            if not os.path.exists(folderout):
                os.makedirs(folderout)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "rhof", level, box_init, mask, False, 1e-12, interpolation, False)

            print(i)
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values, repeat(interpolation), repeat(True)))
            getCoords(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, level, box_init)

'''            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields1, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values1, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields2, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields3, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields4, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields5, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields6, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields7, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields8, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields9, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))
            pool.map(getBox_unpack, zip(repeat(folderout), repeat(f + "/visit_dump." + str(i).zfill(5)), repeat(flevel), repeat(fmask), fields10, repeat(level), repeat(box_init), repeat(mask), nullFields, default_values2, repeat(interpolation), repeat(True)))'''
'''
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Betau_y", level, box_init, mask, True, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Betau_z", level, box_init, mask, True, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_xx", level, box_init, mask, False, 1, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_yy", level, box_init, mask, False, 1, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_zz", level, box_init, mask, False, 1, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_xy", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_xz", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "gtd_yz", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "chi", level, box_init, mask, False, 1, interpolation)
            
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Df", level, box_init, mask, False, 1e-12, interpolation)
            
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Sfd_x", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Sfd_y", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Sfd_z", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "pf", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "tauf", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "epsf", level, box_init, mask, False, 0, interpolation)

            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "vfd_x", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "vfd_y", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "vfd_z", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Bfu_x", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Bfu_y", level, box_init, mask, False, 0, interpolation)
            getBox(folderout, f + "/visit_dump." + str(i).zfill(5), flevel, fmask, "Bfu_z", level, box_init, mask, False, 0, interpolation)  
            '''

if __name__ == "__main__":
    main()

