import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset


def tv_simple(f1, field, dx):
    data = readData(f1, field)
    data = data[:, 0]

    return dx * np.sum(np.abs(np.roll(data, 1) - data)[1:])

def calcula_tv(folder, field, dx, factor):
    convergencia = np.zeros((161))

    for t in xrange(0, 161, 1):
        convergencia[t] = tv_simple(folder + "/visit_dump." + str(t*factor).zfill(5) + '/' + field + '_0.hdf5', field, dx)

    return convergencia

def main():
    tv100 = calcula_tv("./RK4WENO5/outputDir_100", "By", 1.0/100, 1)
    tv200 = calcula_tv("./RK4WENO5/outputDir_200", "By", 1.0/200, 2)
    tv400 = calcula_tv("./RK4WENO5/outputDir_400", "By", 1.0/400, 4)

    tvp100 = calcula_tv("./RK4WENO5/outputDir_100", "p", 1.0/100, 1)
    tvp200 = calcula_tv("./RK4WENO5/outputDir_200", "p", 1.0/200, 2)
    tvp400 = calcula_tv("./RK4WENO5/outputDir_400", "p", 1.0/400, 4)

    tvvx100 = calcula_tv("./RK4WENO5/outputDir_100", "vx", 1.0/100, 1)
    tvvx200 = calcula_tv("./RK4WENO5/outputDir_200", "vx", 1.0/200, 2)
    tvvx400 = calcula_tv("./RK4WENO5/outputDir_400", "vx", 1.0/400, 4)

    tvSx100 = calcula_tv("./RK4WENO5/outputDir_100", "Sx", 1.0/100, 1)
    tvSx200 = calcula_tv("./RK4WENO5/outputDir_200", "Sx", 1.0/200, 2)
    tvSx400 = calcula_tv("./RK4WENO5/outputDir_400", "Sx", 1.0/400, 4)

    tvSy100 = calcula_tv("./RK4WENO5/outputDir_100", "Sy", 1.0/100, 1)
    tvSy200 = calcula_tv("./RK4WENO5/outputDir_200", "Sy", 1.0/200, 2)
    tvSy400 = calcula_tv("./RK4WENO5/outputDir_400", "Sy", 1.0/400, 4)

    tvrho100 = calcula_tv("./RK4WENO5/outputDir_100", "rho", 1.0/100, 1)
    tvrho200 = calcula_tv("./RK4WENO5/outputDir_200", "rho", 1.0/200, 2)
    tvrho400 = calcula_tv("./RK4WENO5/outputDir_400", "rho", 1.0/400, 4)

    tvD100 = calcula_tv("./RK4WENO5/outputDir_100", "D", 1.0/100, 1)
    tvD200 = calcula_tv("./RK4WENO5/outputDir_200", "D", 1.0/200, 2)
    tvD400 = calcula_tv("./RK4WENO5/outputDir_400", "D", 1.0/400, 4)

    tv100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "By", 1.0/100, 1)
    tv200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "By", 1.0/200, 2)
    tv400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "By", 1.0/400, 4)

    tvp100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "p", 1.0/100, 1)
    tvp200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "p", 1.0/200, 2)
    tvp400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "p", 1.0/400, 4)

    tvvx100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "vx", 1.0/100, 1)
    tvvx200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "vx", 1.0/200, 2)
    tvvx400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "vx", 1.0/400, 4)

    tvrho100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "rho", 1.0/100, 1)
    tvrho200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "rho", 1.0/200, 2)
    tvrho400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "rho", 1.0/400, 4)

    tvD100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "D", 1.0/100, 1)
    tvD200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "D", 1.0/200, 2)
    tvD400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "D", 1.0/400, 4)

    tvSx100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "Sx", 1.0/100, 1)
    tvSx200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "Sx", 1.0/200, 2)
    tvSx400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "Sx", 1.0/400, 4)

    tvSy100f = calcula_tv("./RK4WENO5/outputDir_100_fmr", "Sy", 1.0/100, 1)
    tvSy200f = calcula_tv("./RK4WENO5/outputDir_200_fmr", "Sy", 1.0/200, 2)
    tvSy400f = calcula_tv("./RK4WENO5/outputDir_400_fmr", "Sy", 1.0/400, 4)

    tvSy100fc = calcula_tv("./RK4WENO5/CORRECCIONWENO/outputDir_100_fmr", "Sy", 1.0/100, 1)
    tvSy200fc = calcula_tv("./RK4WENO5/CORRECCIONWENO/outputDir_200_fmr", "Sy", 1.0/200, 2)
    tvSy400fc = calcula_tv("./RK4WENO5/CORRECCIONWENO/outputDir_400_fmr", "Sy", 1.0/400, 4)

    #print_solution()

    time = np.asarray(xrange(0, 161, 1)) * 0.005

    plt.figure()
    plt.title('TV By')
    plt.plot(time, tv100, label='100', linestyle="--")
    plt.plot(time, tv200, label='200', linestyle="--")
    plt.plot(time, tv400, label='400', linestyle="--")
    plt.plot(time, tv100f, label='100 fmr', linestyle="--")
    plt.plot(time, tv200f, label='200 fmr', linestyle="--")
    plt.plot(time, tv400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_By.png')

    plt.figure()
    plt.title('TV p')
    plt.plot(time, tvp100, label='100', linestyle="--")
    plt.plot(time, tvp200, label='200', linestyle="--")
    plt.plot(time, tvp400, label='400', linestyle="--")
    plt.plot(time, tvp100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvp200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvp400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_p.png')

    plt.figure()
    plt.title('TV rho')
    plt.plot(time, tvrho100, label='100', linestyle="--")
    plt.plot(time, tvrho200, label='200', linestyle="--")
    plt.plot(time, tvrho400, label='400', linestyle="--")
    plt.plot(time, tvrho100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvrho200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvrho400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_rho.png')

    plt.figure()
    plt.title('TV D')
    plt.plot(time, tvD100, label='100', linestyle="--")
    plt.plot(time, tvD200, label='200', linestyle="--")
    plt.plot(time, tvD400, label='400', linestyle="--")
    plt.plot(time, tvD100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvD200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvD400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_D.png')

    plt.figure()
    plt.title('TV vx')
    plt.plot(time, tvvx100, label='100', linestyle="--")
    plt.plot(time, tvvx200, label='200', linestyle="--")
    plt.plot(time, tvvx400, label='400', linestyle="--")
    plt.plot(time, tvvx100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvvx200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvvx400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_vx.png')

    plt.figure()
    plt.title('TV Sx')
    plt.plot(time, tvSx100, label='100', linestyle="--")
    plt.plot(time, tvSx200, label='200', linestyle="--")
    plt.plot(time, tvSx400, label='400', linestyle="--")
    plt.plot(time, tvSx100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvSx200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvSx400f, label='400 fmr', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('tv_Sx.png')

    plt.figure()
    plt.title('TV Sy')
    plt.plot(time, tvSy100, label='100', linestyle="--")
    plt.plot(time, tvSy200, label='200', linestyle="--")
    plt.plot(time, tvSy400, label='400', linestyle="--")
    plt.plot(time, tvSy100f, label='100 fmr', linestyle="--")
    plt.plot(time, tvSy200f, label='200 fmr', linestyle="--")
    plt.plot(time, tvSy400f, label='400 fmr', linestyle="--")
    plt.plot(time, tvSy100fc, label='100 fmr corregido', linestyle="--")
    plt.plot(time, tvSy200fc, label='200 fmr corregido', linestyle="--")
    plt.plot(time, tvSy400fc, label='400 fmr corregido', linestyle="--")
    plt.ylim([-0.000001, 0.000009])
    plt.legend(loc = 2)
    plt.savefig('tv_Sy.png')


if __name__ == "__main__":
    main()
