import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import h5py

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset

def reducir(data, factor):
    newData = np.zeros((data.shape[0]/factor, data.shape[1]/factor))
    for i in range(0, data.shape[0]/factor):
        for j in range(0, data.shape[1]/factor):
            newData[i,j] = data[i*factor, j*factor]

    return newData

def convergencia_simple(f1, f2, f3, field):
    d100 = readData(f1, field)
    d100 = d100[:d100.shape[0] - 1, :d100.shape[1] - 1]
    d200 = readData(f2, field)
    d200 = reducir(d200[:d200.shape[0] - 1, :d200.shape[1] - 1], 2)
    d400 = readData(f3, field)
    d400 = reducir(d400[:d400.shape[0] - 1, :d400.shape[1] - 1], 4)

    r1 = np.abs(d100-d200)
    r2 = np.abs(d200-d400)
    norm1 = np.sum(np.abs(r1)) / np.size(r1)
    norm2 = np.sum(np.abs(r2)) / np.size(r2)

    return np.log2(norm1/norm2)

def calcula_convergencia(folder, field):
    convergencia = np.zeros((401))

    for t in xrange(0, 401, 1):
        convergencia[t] = convergencia_simple(folder + "/outputDir_100/visit_dump." + str(t).zfill(5) + '/' + field + '_0.hdf5', folder + "/outputDir_200/visit_dump." + str(t*2).zfill(5) + '/' + field + '_0.hdf5', folder + "/outputDir_400/visit_dump." + str(t*4).zfill(5) + '/' + field + '_0.hdf5', field)


    return convergencia

def main():
    cbase = calcula_convergencia(".", "K")
    cbaset = calcula_convergencia(".", "Kt")

    plt.figure()
    plt.title('convergencia norma')
    plt.plot(cbase[1:], label='K', linestyle="--")
    plt.plot(cbaset[1:], label='Kt', linestyle="--")
    plt.legend(loc = 2)
    plt.savefig('convergencia_norma.png')




if __name__ == "__main__":
    main()
