import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        final double gradoAMetros = 111194.444444444;
        try {
            if (args.length != 7 && args.length != 2 && args.length != 6) {
                System.out.println("It must have one of the following arguments. Web service: InitialLatitude FinalLatitude InitialLongitude FinalLongitude LatitudePoints LongitudePoints OutputFilename");
                System.out.println("                                             IGN AGR file: InputFilename OutputFilename");
                System.out.println("                                             IGN AGR file (limited): InputFilename InitialLatPoint FinalLatPoint InitialLongPoint FinalLongPoint OutputFilename");
                System.exit(-1);
            }
            
            if (args.length == 2 || args.length == 6) {
            	FileInputStream fstream = new FileInputStream(args[0]);
            	
            	int initLatLimit = 0;
            	int initLongLimit = 0;
            	int finalLatLimit = 0;
            	int finalLongLimit = 0;
            	String OutputFileName;
            	
            	if (args.length == 6) {
            		initLatLimit = Integer.parseInt(args[1]);
            		finalLatLimit = Integer.parseInt(args[2]);
            		initLongLimit = Integer.parseInt(args[3]);
            		finalLongLimit = Integer.parseInt(args[4]);
            		OutputFileName = args[5];
            	}
            	else {
            		OutputFileName = args[1];
            	}
	            
                // Get the object of DataInputStream
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;
                
                //Read File Line By Line
                strLine = br.readLine();
                int ncols = Integer.parseInt(strLine.split(" +")[1]);
                strLine = br.readLine();
                int nrows = Integer.parseInt(strLine.split(" +")[1]);
                strLine = br.readLine();
                double latInit = Double.parseDouble(strLine.split(" +")[1]);
                strLine = br.readLine();
                double longInit = Double.parseDouble(strLine.split(" +")[1]);
                strLine = br.readLine();
                double delta = Double.parseDouble(strLine.split(" +")[1]);
                strLine = br.readLine();
                double nodata = Double.parseDouble(strLine.split(" +")[1]);
                
                if (args.length == 2) {
            		finalLatLimit = ncols;
            		finalLongLimit = nrows;
                }
	
	            // Get the altitude
	            double[][] altitude = new double[finalLatLimit - initLatLimit][finalLongLimit - initLongLimit];
	            
	            int count = 0;
	            while (count < nrows) {
	            	strLine = br.readLine();
	            	if (count >= initLongLimit && count < finalLongLimit) {
		            	String cols[] = strLine.split(" +");
		            	for (int i = 0; i < cols.length; i++) {
		            		if (i >= initLatLimit && i < finalLatLimit) {
		            		    double data;
		            		    if (cols[i].equals("")) {
		            		        data = Double.parseDouble(cols[i+1]);
		            		    }
		            		    else {
		            		        data = Double.parseDouble(cols[i]);
		            		    }
			            		if (data == nodata) {
			            			altitude[i - initLatLimit][count - initLongLimit] = 0;
			            		}
			            		else {
			            			altitude[i - initLatLimit][count - initLongLimit] = data;
			            		}
		            		}
		            	}
	            	}
	            	count++;
	            }
	            
	            // Creation of the result document X3d
                   DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                           .newDocumentBuilder();
                   Document result = builder.newDocument();
                   Element root = result.createElement("X3D");
                   root.setAttribute("version", "3.0");
                   root.setAttribute("profile", "Immersive");
                   Element head = result.createElement("head");
                   Element meta = result.createElement("meta");
                   meta.setAttribute("name", "description");
                   meta.setAttribute("content", "TerrainGenerator: InitLatidude " + (latInit + initLatLimit*delta) + " EndLatitude " + (latInit + finalLatLimit*delta) 
                           + " InitLongitude " + (longInit + initLongLimit*delta) + " EndLongitude " + (longInit + finalLongLimit*delta) + " Resolution " + (finalLatLimit - initLatLimit) 
                           + "x" + (finalLongLimit - initLongLimit) + " Area " + ((finalLatLimit - initLatLimit) * delta) + "x" + ((finalLongLimit - initLongLimit) * delta) + " m");
                   head.appendChild(meta);
                   Element scene = result.createElement("Scene");
                   
                   Element transform = result.createElement("Transform");
                   transform.setAttribute("DEF", "Terrain");
                   Element shape = result.createElement("Shape");
                   transform.appendChild(shape);
                   Element appearance = result.createElement("Appearance");
                   shape.appendChild(appearance);
                   Element material = result.createElement("Material");
                   appearance.appendChild(material);
                   
                   Element indexedFaceSet = result.createElement("IndexedFaceSet");
                   shape.appendChild(indexedFaceSet);
                   Element coordinate = result.createElement("Coordinate");
                   indexedFaceSet.appendChild(coordinate);
                   
                   scene.appendChild(transform);
                   root.appendChild(head);
                   root.appendChild(scene);

                   StringBuffer coords = new StringBuffer("");
                   StringBuffer face = new StringBuffer("");
                   int cols = finalLongLimit - initLongLimit;
                   latInit = 0;
                   for (int i = 0; i < finalLatLimit - initLatLimit; i++) {
                       double longInitTmp = 0;
                       for (int j = 0; j < cols; j++) {
                           coords.append((longInitTmp) + " " + (latInit) + " "
                                   + altitude[i][j] + ", ");
                           if (j < cols - 1 && i < finalLatLimit - initLatLimit - 1) {
                               face.append((i * cols + j) + " " + (i * cols + j + 1) + " " + ((i + 1) * cols + j) + " -1, " + (i * cols + j + 1) + " " + ((i + 1) * cols + j) + " " + ((i + 1) * cols + j + 1) + " -1, ");
                           }
                           
                           longInitTmp = longInitTmp + delta;
                       }
                       latInit = latInit + delta;
                   }
                   
                   coordinate.setAttribute("point", coords.substring(0, coords.lastIndexOf(",")));
                   indexedFaceSet.setAttribute("coordIndex", face.substring(0, face.lastIndexOf(",")));
                   result.appendChild(root);
                   
                   splitAndWrite(result, OutputFileName);
            }
            else {
	            double latInit = Double.parseDouble(args[0]);
	            double latEnd = Double.parseDouble(args[1]);
	            double longInit = Double.parseDouble(args[2]);
	            double longEnd = Double.parseDouble(args[3]);
	
	            int latPoints = Integer.parseInt(args[4]);
	            int longPoints = Integer.parseInt(args[5]);
	            String fileName = args[6];
	
	            double latDelta = (latEnd - latInit) / latPoints;
	            double longDelta = (longEnd - longInit) / longPoints;
	
	            // Get the altitude
	            double[][] altitude = new double[latPoints + 1][longPoints + 1];
	
	            double latInitTmp = latInit;
	            for (int i = 0; i <= latPoints; i++) {
	                double longInitTmp = longInit;
	                for (int j = 0; j <= longPoints; j++) {
	                    URL url = new URL("http://ws.geonames.org/srtm3?lat="
	                            + latInitTmp + "&lng=" + longInitTmp);
	                    BufferedReader in = new BufferedReader(
	                            new InputStreamReader(url.openStream()));
	                    altitude[i][j] = Double.parseDouble(in.readLine());
	                    in.close();
	                    longInitTmp = longInitTmp + longDelta;
	                }
	                latInitTmp = latInitTmp + latDelta;
	            }
	            
	            // Creation of the result document X3d
                   DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                           .newDocumentBuilder();
                   Document result = builder.newDocument();
                   Element root = result.createElement("X3D");
                   root.setAttribute("version", "3.0");
                   root.setAttribute("profile", "Immersive");
                   Element head = result.createElement("head");
                   Element meta = result.createElement("meta");
                   meta.setAttribute("name", "description");
                   meta.setAttribute("content", "TerrainGenerator: InitLatidude " + latInit + " EndLatitude " + latEnd 
                           + " InitLongitude " + longInit + " EndLongitude " + longEnd + " " + latPoints + "x" + longPoints);
                   head.appendChild(meta);
                   Element scene = result.createElement("Scene");
                   
                   Element transform = result.createElement("Transform");
                   transform.setAttribute("DEF", "Terrain");
                   Element shape = result.createElement("Shape");
                   transform.appendChild(shape);
                   Element appearance = result.createElement("Appearance");
                   shape.appendChild(appearance);
                   Element material = result.createElement("Material");
                   appearance.appendChild(material);
                   
                   Element indexedFaceSet = result.createElement("IndexedFaceSet");
                   shape.appendChild(indexedFaceSet);
                   Element coordinate = result.createElement("Coordinate");
                   indexedFaceSet.appendChild(coordinate);
                   
                   scene.appendChild(transform);
                   root.appendChild(head);
                   root.appendChild(scene);

                   String coords = "";
                   String face = "";
                   double baseX = longInit * gradoAMetros;
                   double baseY = latInit * gradoAMetros;
                   for (int i = 0; i < latPoints; i++) {
                       double longInitTmp = longInit;
                       for (int j = 0; j < longPoints; j++) {
                           coords = coords + (longInitTmp * gradoAMetros - baseX) + " " + (latInit * gradoAMetros - baseY) + " "
                                   + altitude[i][j] + ", ";

                           if (j < longPoints - 1 && i < latPoints - 1) {
                               face = face + (i * longPoints + j) + " " + (i * longPoints + j + 1) + " " + ((i + 1) * longPoints + j) + " -1, " + (i * longPoints + j + 1) + " " + ((i + 1) * longPoints + j) + " " + ((i + 1) * longPoints + j + 1) + " -1, ";
                           }
                           
                           longInitTmp = longInitTmp + longDelta;
                       }
                       latInit = latInit + latDelta;
                   }
                   coords = coords.substring(0, coords.lastIndexOf(","));
                   face = face.substring(0, face.lastIndexOf(","));
                   
                   coordinate.setAttribute("point", coords);
                   indexedFaceSet.setAttribute("coordIndex", face);
                   
                   result.appendChild(root);

                   splitAndWrite(result, fileName);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Converts a Dom to string.
     * 
     * @param node
     *            The dom document
     * @return The String
     */
    public static String domToString(Node node) throws Exception {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StreamResult result = new StreamResult(new ByteArrayOutputStream());
        DOMSource source = new DOMSource(node);
        transformer.transform(source, result);
        return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
    }
    
    public static void splitAndWrite(Node result, String OutputFileName) throws Exception {
        String fileString = domToString(result);
        InputStream is = new ByteArrayInputStream(fileString.getBytes( "UTF-8"));
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        
        BufferedWriter out = new BufferedWriter(new FileWriter(OutputFileName));
        String strLine = "";
        while ((strLine = br.readLine())!=null) {
            String line[] = strLine.split(", ");
            for (int i = 0; i < line.length; i++) {
                out.write(line[i]);
                if (i < line.length-1) {
                    out.write(", ");
                }
                if (i % 500 == 499) {
                    out.write(System.getProperty("line.separator"));
                }
            }
        }
        out.close();
    }

}
