
public class UUIDEquivalence {
    private final String key;
    private final String value;

    public UUIDEquivalence(String aKey, String aValue) {
        key   = aKey;
        value = aValue;
    }

    public String key()   { return key; }
    public String value() { return value; }
    
    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UUIDEquivalence other = (UUIDEquivalence) obj;
        if (key.equals(other.key))
            return true;
        return false;
    }  
}
