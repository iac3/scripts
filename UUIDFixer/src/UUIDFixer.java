/*
Copyright (C) IAC3-UIB. Licensed under BSD. Please see details at
http://www.iac3.eu/simflowny/copyright_and_license.html
*/
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class UUIDFixer {

    
    static ArrayList<UUIDEquivalence> equivalences = new ArrayList<UUIDEquivalence>();
    static ArrayList<UUIDEquivalence> x3dequivalences = new ArrayList<UUIDEquivalence>();
    static XPath xpath;
    private static XPathFactory factory;
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            factory = new net.sf.saxon.xpath.XPathFactoryImpl();
            xpath = factory.newXPath();
            
            File dir = new File("db");  
            String[] collections = dir.list();
            for (int i = 0; i < collections.length; i++) {
                registerUUIDs("db/" + collections[i]);
            }
            for (int i = 0; i < collections.length; i++) {
                changeUUIDs("db/" + collections[i]);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    static void registerUUIDs(String coll) {
        try {
            //Element name
            File dir = new File(coll);
            
            //List files and directories
            File[] listOfFiles = dir.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                String name = listOfFiles[i].toString();
                if (listOfFiles[i].isFile()) {
                    //Files
                    if (name.endsWith(".xml")) {
                        String oldReference = name.substring(0, name.lastIndexOf(".xml"));
                        if (coll.contains("segments")) {
                            oldReference = oldReference.substring(0, oldReference.lastIndexOf("-"));
                            String x3did = UUID.randomUUID().toString();
                            x3dequivalences.add(new UUIDEquivalence(oldReference.substring(oldReference.lastIndexOf("/") + 1), x3did));
                            File file = new File(oldReference);
                            file.renameTo(new File(oldReference.substring(0, oldReference.lastIndexOf("/") + 1) + x3did));
                        }
                        String newUUID = UUID.randomUUID().toString();
                        System.out.println("Añado " + oldReference.substring(oldReference.lastIndexOf("/") + 1) + " : " + newUUID);
                        equivalences.add(new UUIDEquivalence(oldReference.substring(oldReference.lastIndexOf("/") + 1), newUUID));
                        changeOwnID(name, newUUID);
                    }
                }
                //Collections
                else if (listOfFiles[i].isDirectory()) {
                    registerUUIDs(listOfFiles[i].toString());
                }
            }
           
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
    static void changeUUIDs(String coll) {
        try {
            //Element name
            File dir = new File(coll);
            
            //List files and directories
            File[] listOfFiles = dir.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                String name = listOfFiles[i].toString();
                if (listOfFiles[i].isFile()) {
                    //Files
                    if (name.endsWith(".xml")) {
                        changeReferences(name);
                    }
                }
                //Collections
                else if (listOfFiles[i].isDirectory()) {
                    changeUUIDs(listOfFiles[i].toString());
                }
            }
           
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public static void changeReferences(String name) throws Exception {
        Document doc = stringToDom(fileToString(name));
        
        NodeList modelIds = find(doc, "//mms:modelId");
        for (int i = 0; i < modelIds.getLength(); i++) {
            Element modelId = (Element) modelIds.item(i);
            System.out.println("Busco " + modelId.getTextContent() + " en " + name);
            String uuid = equivalences.get(equivalences.indexOf(new UUIDEquivalence(modelId.getTextContent(), ""))).value();
            modelId.setTextContent(uuid);
        }
        
        NodeList x3ds = find(doc, "//mms:x3dSegmentId");
        for (int i = 0; i < x3ds.getLength(); i++) {
            Element x3d = (Element) x3ds.item(i);
            System.out.println("Busco " + x3d.getTextContent() + " en " + name);
            String uuid = equivalences.get(equivalences.indexOf(new UUIDEquivalence(x3d.getTextContent(), ""))).value();
            x3d.setTextContent(uuid);
        }
        
        NodeList rules = find(doc, "//sml:rule");
        for (int i = 0; i < rules.getLength(); i++) {
            Element rule = (Element) rules.item(i);
            System.out.println("Busco " + rule.getTextContent() + " en " + name);
            String uuid = equivalences.get(equivalences.indexOf(new UUIDEquivalence(rule.getTextContent(), ""))).value();
            rule.setTextContent(uuid);
        }
        
        NodeList fileIds = find(doc, "//mms:fileId");
        for (int i = 0; i < fileIds.getLength(); i++) {
            Element fileId = (Element) fileIds.item(i);
            System.out.println("Busco x3d " + fileId.getTextContent() + " en " + name);
            String uuid = x3dequivalences.get(x3dequivalences.indexOf(new UUIDEquivalence(fileId.getTextContent(), ""))).value();
            fileId.setTextContent(uuid);
        }
        
        stringToFile(name, domToString(doc));
    }
    
    public static void changeOwnID(String name, String newId) {

        try {
            Document doc = stringToDom(fileToString(name));
            Element id = (Element) ((Element) doc.getDocumentElement().getFirstChild()).getElementsByTagName("mms:id").item(0);
            id.setTextContent(newId);
            stringToFile(name, domToString(doc));
        } 
        catch (Exception e) {
            System.out.println("Falla en " + name);
            e.printStackTrace(System.out);
        }
    }
    
    /**
     * A method for the format of the output.
     * 
     * @param document          The document to be formatted
     * @return                  The document formatted
     * @throws DDSException     DDS00X - External error
     */
    public static String indent(String document) {
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = docBuilder.parse(new ByteArrayInputStream(document.getBytes("UTF-8")));

            OutputFormat format = new OutputFormat(doc);
            format.setIndenting(true);
            format.setIndent(4);
            Writer output = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(output, format);
            serializer.serialize(doc);

            return output.toString();
        }
        catch (Throwable e) {
            e.printStackTrace();
            return "";
        }
    }
    /**
     * Read a file as String.
     * 
     * @param is    The input stream of the file
     * @return      The resource as string
     */
    public static String convertStreamToString(InputStream is) {
        StringBuilder sb = null;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            try {
                is.close();
            } 
            catch (IOException e) { 
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    
    /**
     * Converts a string to Dom.
     * 
     * @param str               The string
     * @return                  The Dom document
     */
    static Document stringToDom(String str) throws Exception {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        docFactory.setIgnoringElementContentWhitespace(true);
        docFactory.setIgnoringComments(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        InputStream is = new ByteArrayInputStream(str.getBytes("UTF-8"));
        Document result = docBuilder.parse(is);
        removeWhitespaceNodes(result.getDocumentElement());
        return result;
    }
    
    /**
     * Remove the whitespaced nodes from a Dom element.
     * 
     * @param e The element to delete the whitespaces from
     */
    public static void removeWhitespaceNodes(Element e) {
        NodeList children = e.getChildNodes();
        for (int i = children.getLength() - 1; i >= 0; i--) {
            Node child = children.item(i);
            if (child instanceof Text && ((Text) child).getData().trim().length() == 0) {
                e.removeChild(child);
            }
            else if (child instanceof Element) {
                removeWhitespaceNodes((Element) child);
            }
        }
    }
    
    
    /**
     * Converts a Dom to string.
     * 
     * @param doc               The dom document
     * @return                  The String
     */
    static String domToString(Node doc) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            //StreamResult result = new StreamResult(new StringWriter());
            StreamResult result = new StreamResult(new ByteArrayOutputStream());
            DOMSource source = new DOMSource(doc);
            transformer.transform(source, result);
            return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray(), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    static String fileToString(String path) {
        try{
            BufferedReader in = new BufferedReader(new FileReader(path));
            String result = "";
            String str;
            while ((str = in.readLine()) != null) {
                result = result + str;
            }
            in.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    
    static String stringToFile(String path, String doc) {
        try{
            BufferedWriter out = new BufferedWriter(new FileWriter(path));
            String result = "";
            out.write(doc);
            out.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    static String fileToString2(String path) {
        try{
            BufferedReader in = new BufferedReader(new FileReader(path));
            String result = "";
            String str;
            while ((str = in.readLine()) != null) {
                result = result + str + System.getProperty("line.separator");
            }
            in.close();
            return result;
        }
        catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    public static NodeList find(Node node, String xpathQuery) throws Exception {
        NamespaceContext ctx = new NamespaceContext() {
            public String getNamespaceURI(String prefix) {
                String uri;
                if (prefix.equals("mms")) {
                    uri = "urn:mathms";  
                }    
                else {
                    if (prefix.equals("mt")) {
                        uri = "http://www.w3.org/1998/Math/MathML";
                    }
                    else {
                        if (prefix.equals("sml")) {
                            uri = "urn:simml";
                        }
                        else {
                            uri = null;
                        }
                    }
                }
                return uri;
            }
            // Dummy implementation - not used!
            public Iterator < ? > getPrefixes(String val) {
                return null;
            }
            // Dummy implemenation - not used!
            public String getPrefix(String uri) {
                return null;
            }
        };

        xpath.setNamespaceContext(ctx);
        XPathExpression expr = xpath.compile(xpathQuery);

        return (NodeList) expr.evaluate(node, XPathConstants.NODESET);
    }
    
}
