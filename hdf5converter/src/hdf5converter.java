/*
Copyright (C) 2008 - 2010 Institute of Applied Computing with Community
Code of the Universitat de les Illes Balears (IAC3-UIB).
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Vector;

import ncsa.hdf.object.*;     // the common object package
import ncsa.hdf.object.h5.*;  // the HDF5 implementation

public class hdf5converter {

    private static String type = "Cactus";
    //DEBE SER H31, no H30. Están centradas en la celda, por eso no es el punto exacto, sino la mitad.
    private static double initx = 469525.5;
    private static double inity = 4379600.5;
    private static double initz = 0;
    //delta = tamaño / resolucion (malla 100x100x100 y terreno 500x500, 500/100 -> 5 m)
    private static double deltax = 5;
    private static double deltay = 5;
    private static double deltaz = 5;
    static final String NEWLINE = System.getProperty("line.separator"); 
    
    public static void main( String args[] ) throws Exception
    {
        // retrieve an instance of H5File
        FileFormat fileFormat = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);

        //Cactus
        if (type.equals("Cactus")) {
            String fname = "Bx.h5";
            FileFormat file = fileFormat.createInstance(fname, FileFormat.READ);
            file.open();
            Group root = (Group)((javax.swing.tree.DefaultMutableTreeNode)file.getRootNode()).getUserObject();
    
            List<?> members = root.getMemberList();
            
            int xsize = 0;
            int ysize = 0;
            int zsize = 0;
            
            //Iterate over all the timesteps
            for (int vi = 0; vi < members.size(); vi++) {
                if (members.get(vi) instanceof Dataset) {
                    // Create field file
                    Dataset dataset = (Dataset)members.get(vi);
                    String name = dataset.getName();
                    FileWriter fstream = new FileWriter(fname.substring(0, fname.length()-3) + "_" + name.substring(name.indexOf("iteration") + "iteration ".length()) + ".txt");
                    BufferedWriter out = new BufferedWriter(fstream);
                    
                    //Load data
                    dataset.read();
                    long[] start = dataset.getStartDims(); 
                    int[] selectedIndex = dataset.getSelectedIndex(); 

                    xsize = (int) dataset.getMaxDims()[0];
                    ysize = (int) dataset.getMaxDims()[1];
                    zsize = (int) dataset.getMaxDims()[2];
                    
                    //Write the information on a temporal array
                    double[][][] segment = new double[xsize][ysize][zsize];
                    for (int k = 0; k < zsize; k++) {
                        start[selectedIndex[2]] = k; 
                        dataset.clearData();
                        double[] rawdata = (double[])dataset.getData();
                        for (int j = 0; j < ysize; j++) {
                            for (int i = 0; i < xsize; i++) {
                                segment[i][j][k] = rawdata[j + ysize*i];
                            }
                        }
                    }
                    
                    //Write the info to the output file
                    for (int i = 0; i < xsize; i++) {
                        for (int j = 0; j < ysize; j++) {
                            for (int k = 0; k < zsize; k++) {
                                if (segment[i][j][k] != 0) {
                                    out.write((initx + i*deltax) + " " + (inity + j*deltay) + " " + (initz + k*deltaz) + " " + segment[i][j][k] + NEWLINE);
                                }
                            }
                        }
                    }
                    out.close();
                }
            }
            file.close();
        }
        
        //SAMRAI
        if (type.equals("SAMRAI")) {
            FileFormat summary = fileFormat.createInstance("summary.samrai", FileFormat.READ);
            summary.open();
            Group root = (Group)((javax.swing.tree.DefaultMutableTreeNode)summary.getRootNode()).getUserObject();
            List<?> members = ((Group) root.getMemberList().get(0)).getMemberList();
            
            //Get common information from the summary
            int patches = 0;
            String[] varNames = null;
            int numProcessors = 0;
            for (int i = 0; i < members.size(); i++) {
                if (members.get(i) instanceof H5ScalarDS &&  ((H5ScalarDS) members.get(i)).getName().equals("number_global_patches")) {
                    patches = ((int[]) ((H5ScalarDS)members.get(i)).getData())[0];
                }
                if (members.get(i) instanceof H5ScalarDS &&  ((H5ScalarDS) members.get(i)).getName().equals("number_processors")) {
                    numProcessors = ((int[]) ((H5ScalarDS)members.get(i)).getData())[0];
                }
                if (members.get(i) instanceof H5ScalarDS &&  ((H5ScalarDS) members.get(i)).getName().equals("var_names")) {
                    varNames = ((String[]) ((H5ScalarDS)members.get(i)).getData());
                }
            }
            int[][] lows = new int[3][patches];
            int[][] ups = new int[3][patches];
            int[][] patchInit = new int[3][patches];
            members = ((Group) root.getMemberList().get(1)).getMemberList();
            for (int i = 0; i < members.size(); i++) {
                if (members.get(i) instanceof H5CompoundDS && ((H5CompoundDS) members.get(i)).getName().equals("patch_extents")) {
                    Dataset ds = (Dataset) members.get(i);
                    
                    Vector<?> lowUp = (Vector<?>) ds.read();
             
                    int[] low = (int[]) lowUp.get(0);
                    int[] up = (int[]) lowUp.get(1);
                    for (int j = 0; j < patches; j++) {
                        for (int k = 0; k < 3; k++) {
                            lows[k][j] = low[k + j*3];
                            ups[k][j] = up[k + j*3];
                        }
                    }
                }
            }
            summary.close();
            
            //Iterate over all the variables
            for (int var = 0; var < varNames.length; var++) {
                //Create the output field file
                FileWriter fstream = new FileWriter(varNames[var] + ".txt");
                BufferedWriter out = new BufferedWriter(fstream);
                
                //Process every processor file
                for (int proc = 0; proc < numProcessors; proc++) {
                    NumberFormat formatter = new DecimalFormat("00000");
                    
                    FileFormat processor = fileFormat.createInstance("processor_cluster." + formatter.format(proc) + ".samrai", FileFormat.READ);
                    processor.open();
                    root = (Group)((javax.swing.tree.DefaultMutableTreeNode)processor.getRootNode()).getUserObject();
                    List<?> patchList = ((Group)((Group) root.getMemberList().get(0)).getMemberList().get(0)).getMemberList(); 
                    
                    //Process every patch
                    for (int patch = 0; patch < patchList.size(); patch++) {
                        Group patchGroup = (Group) patchList.get(patch);
                        int patchNumber = Integer.parseInt(patchGroup.getName().substring(patchGroup.getName().indexOf(".") + 1));
                        
                        //Process the variable in the actual iteration
                        List<?> varList = patchGroup.getMemberList();
                        for (int vi = 0; vi < varList.size(); vi++) {
                            if (varList.get(vi) instanceof H5ScalarDS &&  ((H5ScalarDS) varList.get(vi)).getName().equals(varNames[var])) {
                                Dataset ds = (Dataset) varList.get(vi);
                                float[] dataRead = (float[])ds.read();
                                int xsize = (ups[0][patch] - lows[0][patch]) + 1;
                                int ysize = (ups[1][patch] - lows[1][patch]) + 1;
                                int zsize = (ups[2][patch] - lows[2][patch]) + 1;
                                float[][][] segment = new float[xsize][ysize][zsize];
                                //Get the info from hdf5
                                for (int i = 0; i < xsize; i++) {
                                    for (int j = 0; j < ysize; j++) {
                                        for (int k = 0; k < zsize; k++) {
                                            segment[i][j][k] = dataRead[i + ysize*(j+zsize*(k))];
                                        }
                                    }
                                }
                                //Write the info to the output file
                                for (int i = 0; i < xsize; i++) {
                                    for (int j = 0; j < ysize; j++) {
                                        for (int k = 0; k < zsize; k++) {
                                       //     if (segment[i][j][k] != 0) {
                                                out.write((initx + patchInit[0][patchNumber] + i*deltax) + " " + (inity + patchInit[1][patchNumber] + j*deltay) + " " + (initz + patchInit[2][patchNumber] + k*deltaz) + " " + segment[i][j][k] + NEWLINE);
                                      //      }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    processor.close();
                }
                out.close(); 
            }
        }
    }
    
}
