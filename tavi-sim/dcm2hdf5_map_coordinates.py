import sys, getopt
import dicom
import numpy as np
import matplotlib.pyplot as plt
import h5py
from os import listdir, makedirs
from scipy import ndimage, fftpack
from os.path import isdir, join, exists, isfile
import pylab as py

#original size
o_dx = 40
o_dy = 320
o_dz = 320
o_nx = 40
o_ny = 256
o_nz = 256

#ROI
i_from=0
i_to=33
j_from=64
j_to=171
k_from=58
k_to=133

#target size
t_nx=33
t_ny=107
t_nz=75

def listOfFiles(directory):
    	return [ f for f in listdir(directory) if isfile(join(directory,f)) ]

def interpolate(field, singular):
	global_image = np.ndarray(shape=(o_nx, o_ny, o_nz))
	
    	for file in sorted(listOfFiles(field)):
		ds = dicom.read_file(field + "/" + file)
		#Get X slice number
		xslice = ds[0x2001,0x100a].value
		#Rescale sequence
		if singular:
			rescale = ds[0x40,0x9096][0]
		else:
			rescale = ds[0x2001,0x9000][0][0x2001,0x1068][0]
		#Rescale slope
		if singular:
			slope = rescale[0x40,0x9225].value
		else:
			slope = rescale[0x0028,0x1053].value		
		#Rescale intercept
		if singular:
			intercept = rescale[0x40,0x9224].value
		else:
			intercept = rescale[0x28,0x1052].value
		image_d = np.add(np.multiply(ds.pixel_array, slope), intercept)
#		image_d = ds.pixel_array
		global_image[xslice - 1,:,:] = np.transpose(image_d[:,:])
#		global_image[xslice - 1,:,:] = np.add(np.multiply(global_image[xslice - 1,:,:], slope), intercept)

	global_image = global_image[:,::-1,:]

	roi = np.ndarray(shape=((i_to - i_from), (j_to - j_from), (k_to - k_from)))
	roi = global_image[i_from:i_to,(o_ny - j_to):(o_ny - j_from),k_from:k_to]

	# Create a uniformly spaced grid
	t_x,t_y,t_z = np.mgrid[0:(i_to - i_from - 1):complex(t_nx), 0:(j_to - j_from - 1):complex(t_ny), 0:(k_to - k_from - 1):complex(t_nz)]
	coords = np.array([t_x, t_y, t_z])

	#interpolate
	t_image = ndimage.map_coordinates(roi, coords, order=3) # 3rd order spline
#	t_image = ndimage.map_coordinates(roi, coords, order=1) # linear

	#write output file
	f = h5py.File(field + '.hdf5', "w")
	dset = f.create_dataset(field, (t_nx, t_ny, t_nz), dtype='f')
	for i in range(0, t_nx):
		for j in range(0, t_ny):
			for k in range(0, t_nz):
				# axis does not correspond
				dset[i, j, k] = t_image[i,j,t_nz - k - 1]
	f.close()


def main(argv):
	interpolate("vx", False)
	interpolate("vy", True)
	interpolate("vz", False)



if __name__ == "__main__":
    main(sys.argv[1:])
