import sys, getopt
import dicom
import numpy as np
import matplotlib.pyplot as plt
import h5py
from os import listdir, makedirs
from scipy import ndimage, fftpack, linalg
from os.path import isdir, join, exists, isfile
import pylab as py
import time

#original size
o_nx = 256
o_ny = 256
o_nz = 40

#original intensity size
i_nx = 512
i_ny = 512
i_nz = 40

#output resolution size
or_nx = 35
or_ny = 74
or_nz = 33
#or_nx = 66
#or_ny = 148
#or_nz = 70

fvx = "vx_t2_reset"
fvy = "vy_t2_reset"
fvz = "vz_t2_reset"
fint = "intensity_reset"

ofvx = "vx_t2_filtered"
ofvy = "vy_t2_filtered"
ofvz = "vz_t2_filtered"

#ROI
x_min = 91
x_max = 126
y_min = 76
y_max = 150
z_min = 0
z_max = 33

#ROI desplazamiento
x_desp = 3 #Shifting respect to x in visit
y_desp = 2  #Shifting respect to z in visit
z_desp = 2 #Shifting respect to y in visit


stencil = 1
gamma = 2

windowSize = 2*stencil + 1

windowSize3 = windowSize**3

test = False
filter = True

#TO DELETE
slice_n = 1

def listOfFiles(directory):
    	return [ f for f in listdir(directory) if isfile(join(directory,f)) ]

def readDicom(fileName, singular):
	global_image = np.ndarray(shape=(o_nx, o_ny, o_nz))
	
    	for file in sorted(listOfFiles(fileName)):
		ds = dicom.read_file(fileName + "/" + file)
		#Get X slice number
		xslice = ds[0x2001,0x100a].value
		#Rescale sequence
		if singular:
			rescale = ds[0x40,0x9096][0]
		else:
			rescale = ds[0x2001,0x9000][0][0x2001,0x1068][0]
		#Rescale slope
		if singular:
			slope = rescale[0x40,0x9225].value
		else:
			slope = rescale[0x0028,0x1053].value		
		#Rescale intercept
		if singular:
			intercept = rescale[0x40,0x9224].value
		else:
			intercept = rescale[0x28,0x1052].value
		image_d = np.add(np.multiply(ds.pixel_array, slope), intercept)
	
		global_image[:,:, xslice - 1] = image_d[:,:]

	return global_image

def readDicomIntensity(fileName):
	global_image = np.ndarray(shape=(i_nx, i_ny, i_nz))
    	for file in sorted(listOfFiles(fileName)):
		ds = dicom.read_file(fileName + "/" + file)

		#Get X slice number
		#xslice = ds[0x2001,0x100a].value
		xslice = ds.InstanceNumber
		#Rescale sequence
		rescale = ds[0x2001,0x9000][0][0x2001,0x1068][0]
		#Rescale slope
		slope = rescale[0x0028,0x1053].value
		#Rescale intercept
		intercept = rescale[0x28,0x1052].value
		image_d = np.add(np.multiply(ds.pixel_array, slope), intercept)

		global_image[:,:,xslice - 1] = image_d[:,:]

	return global_image

def vectorize(data):

	# Reduce intensity to velocity size
	temp_x,temp_y,temp_z = np.mgrid[0:i_nx:complex(o_nx), 0:i_ny:complex(o_ny), 0:i_nz:complex(o_nz)]
	coords = np.array([temp_x, temp_y, temp_z])
	t_image = ndimage.map_coordinates(data, coords, order=3) # 3rd order spline

	size = t_image.shape + (3,)
	componentImage = np.ndarray(size)

	componentImage[:,:,:, 0] = t_image[:,:,:]
	componentImage[:,:,:, 1] = t_image[:,:,:]
	componentImage[:,:,:, 2] = t_image[:,:,:]


	return componentImage

def cut(data):

	return data[x_min+x_desp:x_max+x_desp+1,y_min+y_desp:y_max+y_desp+1,z_min+z_desp:z_max+z_desp+1]

def interpolate(data, t_x, t_y, t_z):
	factorx = float(or_nx)/(x_max - x_min)
	factory = float(or_ny)/(y_max - y_min)
	factorz = float(or_nz)/(z_max - z_min)

	# Create a uniformly spaced grid
	temp_x,temp_y,temp_z = np.mgrid[0:data.shape[0]:complex(o_nx*factorx), 0:data.shape[1]:complex(o_ny*factory), 0:data.shape[2]:complex(o_nz*factorz)]
	coords = np.array([temp_x, temp_y, temp_z])

	#interpolate
	t_image = ndimage.map_coordinates(data, coords, order=3) # 3rd order spline

	return t_image

def writeFile(field, data):

	#write output file
	f = h5py.File(field + '.hdf5', "w")
	dset = f.create_dataset(field, data.shape, dtype='f')
	dset[:,:,:] = data[:,:,:]
	f.close()

def writeDicom(fileName, newDir, data, singular):

	for file in sorted(listOfFiles(fileName)):
		ds = dicom.read_file(fileName + "/" + file)
		#Get X slice number
		xslice = ds[0x2001,0x100a].value

		#Rescale sequence
		if singular:
			rescale = ds[0x40,0x9096][0]
		else:
			rescale = ds[0x2001,0x9000][0][0x2001,0x1068][0]
		#Rescale slope
		if singular:
			slope = rescale[0x40,0x9225].value
		else:
			slope = rescale[0x0028,0x1053].value		
		#Rescale intercept
		if singular:
			intercept = rescale[0x40,0x9224].value
		else:
			intercept = rescale[0x28,0x1052].value

		ds.pixel_array[:,:] = (data[xslice - 1,:,:] - intercept)/slope
		ds.PixelData = ds.pixel_array.tostring()
		ds.save_as(newDir + "/" + file)

def hasStencil(index):
	return index[0] >= stencil and index[0] <= o_nx - 1 - stencil and index[1] >= stencil and index[1] <= o_ny - 1 - stencil and index[2] >= stencil and index[2] <= o_nz - 1 - stencil

def isInRoi(index):
	return index[0] >= x_min and index[0] <= x_max and index[1] >= y_min and index[1] <= y_max and index[2] >= z_min and index[2] <= z_max

def norm(data, gamma):
	if gamma == 2:
		return np.sqrt(data.dot(data))
	else:
		return linalg.norm(data, gamma)

def vmf(vx, vy, vz, index):
	#Calculate all D
	D = np.zeros(shape=(windowSize3), dtype='float')
	IAux = np.zeros(shape=(windowSize3), dtype='float')
	stencilMatrix = np.ndarray(shape=(windowSize, windowSize, windowSize),dtype='float')
	w = 0
	indexes = []
	for desp, x in np.ndenumerate(stencilMatrix):
		acc = 0
		newIndex = np.add(np.subtract(index, stencil), desp)
		I = np.array([vx[tuple(newIndex)], vy[tuple(newIndex)], vz[tuple(newIndex)]])
		z = 0
		for neigh, nx in np.ndenumerate(stencilMatrix):
			neighIndex = np.add(np.subtract(index, stencil), neigh)
			if neighIndex[0] > newIndex[0] or (neighIndex[0]==newIndex[0] and neighIndex[1]>newIndex[1]) or (neighIndex[0]==newIndex[0] and neighIndex[1]==newIndex[1] and neighIndex[2]>newIndex[2]) : 
				Ij = np.array([vx[tuple(neighIndex)], vy[tuple(neighIndex)], vz[tuple(neighIndex)]])
				#Accumulate norm
				calc = norm(np.subtract(I,Ij), gamma)
				D[w] = D[w] + calc
				D[z] = D[z] + calc
			z = z + 1
		indexes.append(newIndex)

		w = w + 1
	#Get the minimum D
	minValue = np.finfo('d').max
	minIndex = 0
	for i, x in np.ndenumerate(D):
		if minValue > D[i]:
			minValue = D[i]
			minIndex = i[0]
	#Get the I associated to the minimum D
	i = int(minIndex / (windowSize**2))
	j = int((minIndex - i*(windowSize**2)) / windowSize)
	k = minIndex - j * windowSize - i * windowSize**2
	i = i + index[0] - stencil
	j = j + index[1] - stencil
	k = k + index[2] - stencil

	return np.array([vx[i, j, k], vy[i, j, k], vz[i, j, k]])
		
def dvm(original, median):
	return norm(np.subtract(original,median), gamma)

def vdh(vx, vy, vz, index):
	stencilMatrix = np.ndarray(shape=(windowSize, windowSize, windowSize),dtype='float')
	I = np.array([vx[tuple(index)], vy[tuple(index)], vz[tuple(index)]])
	acc = np.zeros(shape=(3),dtype='float')
	for desp, x in np.ndenumerate(stencilMatrix):
		newIndex = np.add(np.subtract(index, stencil), desp)
		Ij = np.array([vx[tuple(newIndex)], vy[tuple(newIndex)], vz[tuple(newIndex)]])
		acc = np.add(acc, np.divide(np.inner(I, Ij), np.inner(norm(I, gamma), norm(Ij, gamma))))
	return acc

def sd(vx, vy, vz, index):
	I = np.array([vx[tuple(index)], vy[tuple(index)], vz[tuple(index)]])
	#Component mean calculation
	counter = 0
	meanx = np.mean(vx[index[0] - stencil: index[0] - stencil + windowSize, index[1] - stencil: index[1] - stencil + windowSize, index[2] - stencil: index[2] - stencil + windowSize])
	meany = np.mean(vy[index[0] - stencil: index[0] - stencil + windowSize, index[1] - stencil: index[1] - stencil + windowSize, index[2] - stencil: index[2] - stencil + windowSize])
	meanz = np.mean(vz[index[0] - stencil: index[0] - stencil + windowSize, index[1] - stencil: index[1] - stencil + windowSize, index[2] - stencil: index[2] - stencil + windowSize])
	return np.sqrt(np.divide(np.add(np.add(np.power(np.subtract(I, meanx), 2), np.power(np.subtract(I, meany), 2)), np.power(np.subtract(I, meanz), 2)), 3))

def afcmc(U, L, S, MI):
	alphaN = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype='float')
	alphaF = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype='float')
	alphaN = np.add(alphaN, 0.1)
	alphaF = np.add(alphaF, 0.9)
	F = np.ndarray(shape=(4, o_nx, o_ny, o_nz, 3),dtype='float')
	F[0, ...] = U
	F[1, ...] = L
	F[2, ...] = S
	F[3, ...] = MI

	counter = 0
	while True:
		print "iteration ", counter
		alphaF_old = np.copy(alphaF)
		alphaN_old = np.copy(alphaN)

		CF = np.ndarray(shape=(4, 3),dtype='float')
		CN = np.ndarray(shape=(4, 3),dtype='float')

		CF_U = np.multiply(U, alphaF).sum(axis=0).sum(axis=0).sum(axis=0)/alphaF.sum(axis=0).sum(axis=0).sum(axis=0)
		CF_L = np.multiply(L, alphaF).sum(axis=0).sum(axis=0).sum(axis=0)/alphaF.sum(axis=0).sum(axis=0).sum(axis=0)
		CF_S = np.multiply(S, alphaF).sum(axis=0).sum(axis=0).sum(axis=0)/alphaF.sum(axis=0).sum(axis=0).sum(axis=0)
		CF_MI = np.multiply(MI, alphaF).sum(axis=0).sum(axis=0).sum(axis=0)/alphaF.sum(axis=0).sum(axis=0).sum(axis=0)
		CN_U = np.multiply(U, alphaN).sum(axis=0).sum(axis=0).sum(axis=0)/alphaN.sum(axis=0).sum(axis=0).sum(axis=0)
		CN_L = np.multiply(L, alphaN).sum(axis=0).sum(axis=0).sum(axis=0)/alphaN.sum(axis=0).sum(axis=0).sum(axis=0)
		CN_S = np.multiply(S, alphaN).sum(axis=0).sum(axis=0).sum(axis=0)/alphaN.sum(axis=0).sum(axis=0).sum(axis=0)
		CN_MI = np.multiply(MI, alphaN).sum(axis=0).sum(axis=0).sum(axis=0)/alphaN.sum(axis=0).sum(axis=0).sum(axis=0)

		CF[0, ...] = CF_U
		CF[1, ...] = CF_L
		CF[2, ...] = CF_S
		CF[3, ...] = CF_MI

		CN[0, ...] = CN_U
		CN[1, ...] = CN_L
		CN[2, ...] = CN_S
		CN[3, ...] = CN_MI

		for i, x in np.ndenumerate(alphaN.sum(axis=3)):			
			if test and i[0] == slice_n+1:
				break
			if isInRoi(tuple(i)) :
				Fi = F[:, i[0], i[1], i[2], :]

				alphaN[i[0], i[1], i[2], :] = np.divide(1.0, np.add(np.power(np.divide(linalg.norm(np.subtract(Fi, CN), gamma), linalg.norm(np.subtract(Fi, CN), gamma)), 2), np.power(np.divide(linalg.norm(np.subtract(Fi, CN), gamma), linalg.norm(np.subtract(Fi, CF), gamma)), 2)))

				alphaF[i[0], i[1], i[2], :] = np.divide(1.0, np.add(np.power(np.divide(linalg.norm(np.subtract(Fi, CF), gamma), linalg.norm(np.subtract(Fi, CF), gamma)), 2), np.power(np.divide(linalg.norm(np.subtract(Fi, CF), gamma), linalg.norm(np.subtract(Fi, CN), gamma)), 2)))
		print alphaF, alphaF_old

		if convergence(alphaF, alphaF_old, 0.001):
			break
		counter = counter + 1

	return alphaF

def convergence(a, b, factor):
	return np.all(np.less_equal(np.absolute(np.subtract(a, b)), factor))

def main(argv):

	vx = readDicom(fvx, True)
	vy = readDicom(fvy, False)
	vz = readDicom(fvz, False)

	# Axis x and y are swapped
	vx = np.swapaxes(vx, 0,1)
	vy = np.swapaxes(vy, 0,1)
	vz = np.swapaxes(vz, 0,1)

	if filter:

		#Rule 4: Magnitude Image Intensity
		intensity = readDicomIntensity(fint)
		MI = vectorize(intensity)

		M = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype="float")
		U = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype="float")
		L = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype="float")
		S = np.ndarray(shape=(o_nx, o_ny, o_nz, 3),dtype="float")

		if True:
			for index,x in np.ndenumerate(vx):
				if test and index[0] == slice_n+1:
					break
				if isInRoi(tuple(index)) and hasStencil(tuple(index)) and (vx[tuple(index)] != 0 or vx[tuple(index)] != 0 or vx[tuple(index)] != 0):
					print index
					I = np.array([vx[tuple(index)], vy[tuple(index)], vz[tuple(index)]])
					#Calculate Vector median filter
					M[index[0], index[1], index[2], :] = vmf(vx, vy, vz, index)

					#Calculate Fuzzy rules
					#Rule 1: Distance from vector median
					U[index[0], index[1], index[2], :] = dvm(I, M[index[0], index[1], index[2], :])

					#Rule 2: Vector Direction Homogeneity
					L[index[0], index[1], index[2], :] = vdh(vx, vy, vz, index)

					#Rule 3: Vector Field Standard Deviation
					S[index[0], index[1], index[2], :] = sd(vx, vy, vz, index)


	
			#Normalization 
			U = np.divide(np.subtract(U, U.min()), U.ptp())
			L = np.divide(np.subtract(L, L.min()), L.ptp())
			L = np.nan_to_num(L)
			S = np.divide(np.subtract(S, S.min()), S.ptp())
			MI = np.divide(np.subtract(MI, MI.min()), MI.ptp())

			'''plt.figure()
			plt.imshow(MI[slice_n,y_min:y_max + 1,z_min:z_max + 1, 0],origin="lower")
			plt.colorbar()
			plt.title("MI")
			plt.savefig("MI.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(vx[slice_n,y_min:y_max + 1,z_min:z_max + 1],origin="lower", vmin=-80, vmax=80)
			plt.colorbar()
			plt.title("vx")
			plt.savefig("vx.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(vy[slice_n,y_min:y_max + 1,z_min:z_max + 1],origin="lower", vmin=-80, vmax=80)
			plt.colorbar()
			plt.title("vy")
			plt.savefig("vy.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(vz[slice_n,y_min:y_max + 1,z_min:z_max + 1],origin="lower", vmin=-80, vmax=80)
			plt.colorbar()
			plt.title("vz")
			plt.savefig("vz.png", format="PNG")
			plt.close()


			plt.figure()
			plt.imshow(U[slice_n,y_min:y_max + 1,z_min:z_max + 1,0],origin="lower", vmin=0, vmax=1)
			plt.colorbar()
			plt.title("U(x)")
			plt.savefig("Ux.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(U[slice_n,y_min:y_max + 1,z_min:z_max + 1,1],origin="lower", vmin=0, vmax=1)
			plt.colorbar()
			plt.title("U(y)")
			plt.savefig("Uy.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(U[slice_n,y_min:y_max + 1,z_min:z_max + 1,2],origin="lower", vmin=0, vmax=1)
			plt.colorbar()
			plt.title("U(z)")
			plt.savefig("Uz.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(L[slice_n,y_min:y_max + 1,z_min:z_max + 1,0],origin="lower")
			plt.colorbar()
			plt.title("L(x)")
			plt.savefig("Lx.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(L[slice_n,y_min:y_max + 1,z_min:z_max + 1,1],origin="lower")
			plt.colorbar()
			plt.title("L(y)")
			plt.savefig("Ly.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(L[slice_n,y_min:y_max + 1,z_min:z_max + 1,2],origin="lower")
			plt.colorbar()
			plt.title("L(z)")
			plt.savefig("Lz.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(S[slice_n,y_min:y_max + 1,z_min:z_max + 1,0],origin="lower")
			plt.colorbar()
			plt.title("S(x)")
			plt.savefig("Sx.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(S[slice_n,y_min:y_max + 1,z_min:z_max + 1,1],origin="lower")
			plt.colorbar()
			plt.title("S(y)")
			plt.savefig("Sy.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(S[slice_n,y_min:y_max + 1,z_min:z_max + 1,2],origin="lower")
			plt.colorbar()
			plt.title("S(z)")
			plt.savefig("Sz.png", format="PNG")
			plt.close()
			'''

			#Adaptative 
			alpha = afcmc(U, L, S, MI)


			'''plt.figure()
			plt.imshow(alpha[slice_n,y_min:y_max + 1,z_min:z_max + 1,0],origin="lower")
			plt.colorbar()
			plt.title("A(x)")
			plt.savefig("Ax.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(alpha[slice_n,y_min:y_max + 1,z_min:z_max + 1,1],origin="lower")
			plt.colorbar()
			plt.title("A(y)")
			plt.savefig("Ay.png", format="PNG")
			plt.close()

			plt.figure()
			plt.imshow(alpha[slice_n,y_min:y_max + 1,z_min:z_max + 1,2],origin="lower")
			plt.colorbar()
			plt.title("A(z)")
			plt.savefig("Az.png", format="PNG")
			plt.close()
			'''
			np.save("alpha.npy", alpha)

		else :

			alpha = np.load("alpha.npy")

		for index,x in np.ndenumerate(vx):
			if test and index[0] == slice_n+1:
				break
			if isInRoi(tuple(index)) and hasStencil(tuple(index)) and (vx[tuple(index)] != 0 or vx[tuple(index)] != 0 or vx[tuple(index)] != 0):
				I = np.array([vx[tuple(index)], vy[tuple(index)], vz[tuple(index)]])
				I = np.add(np.multiply(alpha[index[0], index[1], index[2], :], I), np.multiply(np.subtract(1, alpha[index[0], index[1], index[2], :]), M[index[0], index[1], index[2], :]))
				vx[tuple(index)] = I[0]
				vy[tuple(index)] = I[1]
				vz[tuple(index)] = I[2]

#	vx = interpolate(vx, or_nx,or_ny, or_nz)
#	vy = interpolate(vy, or_nx,or_ny, or_nz)
#	vz = interpolate(vz, or_nx,or_ny, or_nz)

	#Cut to the ROI
	vx = cut(vx)
	vy = cut(vy)
	vz = cut(vz)

	#Flip on y
	vx = vx[:,::-1,:]
	vy = vy[:,::-1,:]
	vz = vz[:,::-1,:]
	#Flip on x
	vx = vx[::-1,:,:]
	vy = vy[::-1,:,:]
	vz = vz[::-1,:,:]	

	#Sign change z
	vz = vz * -1
	#Sign change x
	vx = vx * -1

	for slice_n in range(vx.shape[0]):
		plt.figure()
		plt.imshow(vx[slice_n,:,:],origin="lower", cmap = cm.Greys_r)
		plt.colorbar()
		plt.title("new_vx")
		plt.savefig("new_vx" + str(slice_n) + ".png", format="PNG")
		plt.close()
		plt.figure()
		plt.imshow(vy[slice_n,:,:],origin="lower", cmap = cm.Greys_r)
		plt.colorbar()
		plt.title("new_vy")
		plt.savefig("new_vy" + str(slice_n) + ".png", format="PNG")
		plt.close()
		plt.figure()
		plt.imshow(vz[slice_n,:,:],origin="lower", cmap = cm.Greys_r)
		plt.colorbar()
		plt.title("new_vz")
		plt.savefig("new_vz" + str(slice_n) + ".png", format="PNG")
		plt.close()

	print vx.shape

	writeFile(ofvx, vx)
	writeFile(ofvy, vy)
	writeFile(ofvz, vz)


if __name__ == "__main__":
    main(sys.argv[1:])
