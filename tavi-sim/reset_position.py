import sys, getopt
import dicom
import numpy as np
import matplotlib.pyplot as plt
import h5py
from os import listdir, makedirs
from scipy import ndimage
from os.path import isdir, join, exists, isfile


def listOfFiles(directory):
    	return [ f for f in listdir(directory) if isfile(join(directory,f)) ]

def reset(oldDir, newDir):
    	if not exists(newDir):
		makedirs(newDir)

	counter = 0
    	for file in sorted(listOfFiles(oldDir)):
		ds = dicom.read_file(oldDir + "/" + file)
		ds.ImageOrientationPatient = "0\\1\\0\\0\\0\\-1"
		ds.ImagePositionPatient = "0\\0\\0"
		ds.save_as(newDir + "/" + file)
		counter = counter + 1

def main(argv):
	for t in xrange(0,14):
		reset("30036/t" + str(t), "vx_t" + str(t) + "_reset")
		reset("30034/t" + str(t), "vy_t" + str(t) + "_reset")
		reset("30035/t" + str(t), "vz_t" + str(t) + "_reset")
#	reset("30037", "intensity_reset")



if __name__ == "__main__":
    main(sys.argv[1:])
