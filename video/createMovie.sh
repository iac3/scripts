

for VARIABLE in {0..1999}
do
    TIME_FORMATED="$( printf '%04d' "$VARIABLE" )"
    TIME=$((VARIABLE*4))
    echo $TIME_FORMATED
    echo $TIME
    echo $VARIABLE
    #Trim and size visit images
    montage -border 0 -geometry 520x+2+2 -trim rhof/movie$TIME_FORMATED.jpeg psi4r/movie_psi4r$TIME_FORMATED.jpeg -title "t = $TIME" salida_1_$VARIABLE.jpg
    #montage -border 0 -geometry 520x -trim movie$TIME_FORMATED.jpeg salida_1_$VARIABLE.jpg
    #montage -border 0 -geometry 520x -trim movie_psi4r$TIME_FORMATED.jpeg salida_2_$VARIABLE.jpg
    #Compose images
    #convert +append salida_1_$VARIABLE.jpg salida_2_$VARIABLE.jpg salida_3_$VARIABLE.png
    #Add title
    #montage -border 0 -geometry 1040x salida_3_$VARIABLE.png  -title "t $TIME" salida_4_$VARIABLE.jpg
    #Format wave form image
    montage -border 0 -geometry 1040x onda/plot_$VARIABLE.png salida_2_$VARIABLE.jpg
    #Join all images
    convert -append salida_1_$VARIABLE.jpg salida_2_$VARIABLE.jpg uncensored/salida_$TIME_FORMATED.png
    #Remove temporal images
    rm salida_1_$VARIABLE.jpg salida_2_$VARIABLE.jpg
done


opt="vbitrate=10125000:mbd=2:keyint=132:v4mv:vqmin=3:lumi_mask=0.07:dark_mask=0.2:mpeg_quant:scplx_mask=0.1:tcplx_mask=0.1:naq"
mencoder mf://uncensored/*.png -mf w=1920:h=1080:fps=25:type=png -oac copy -ovc lavc -lavcopts vcodec=mpeg4:vpass=1:$opt -nosound -o ./uncensored/output.avi
mencoder mf://uncensored/*.png -mf w=1920:h=1080:fps=25:type=png -oac copy -ovc lavc -lavcopts vcodec=mpeg4:vpass=2:$opt -nosound -o ./uncensored/output.avi
rm -rf divx2pass.log