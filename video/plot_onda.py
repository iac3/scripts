import sys, getopt
import os
import numpy as np
import matplotlib.pyplot as plt
import codecs

def main():

    data = np.loadtxt('c2p2.dat')
    
    for i in xrange(data.shape[0]):
        fig, ax = plt.subplots(figsize=(13, 1), dpi=80)
        plt.plot(data[0:i,1], linewidth=2)
        if len(data[0:i,1]) > 0:
            limit = max(0.0001, np.max(np.abs(data[0:i,1])))
            plt.ylim([-limit, limit])
        plt.xlim([-0.1, 2000.1])
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        plt.tick_params(
            bottom=False,      
            left=False,   
            top=False,    
            labelbottom=False,
            labelleft=False)
        plt.savefig('./plot_' + str(i) + '.png', bbox_inches='tight',pad_inches = 0) 
        plt.close()
    

if __name__ == "__main__":
    main()
