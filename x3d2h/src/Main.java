import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        final int revolveNumber = 37;
        final int baseUnions = 9;
        
        try {
            
        	FileInputStream inputMesh = new FileInputStream("inputMesh.vtk");
            DataInputStream in = new DataInputStream(inputMesh);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter tmp = new BufferedWriter(new FileWriter("2Dmesh"));
            String strLine;
            
            boolean header = true;
            boolean coordinate = false;
            boolean union = false;
            int counter = 0;
            int pointLimit = 0;
            int unionLimit = 0;

            while ((strLine = br.readLine()) != null) {

                if (strLine.indexOf("POINTS") != -1) {
                    header = false;  
                    String[] line = strLine.split(" ");
                    pointLimit = Integer.valueOf(line[1]).intValue() / revolveNumber;
                    tmp.write(line[0] + " " + pointLimit + " " + line[2] + "\n"); 
                    coordinate = true;
                }
                if (header) {
                    tmp.write(strLine + "\n"); 
                }
                if (union) {
                    String pointsInLine[] = strLine.split(" ");
                    if (pointsInLine.length == 4 && counter < unionLimit) {
                        tmp.write(strLine + "\n");
                        counter++;
                    }
                }
                if (strLine.indexOf("POLYGONS") != -1) {
                    counter = 0;
                    union = true;
                    coordinate = false;
                    String[] line = strLine.split(" ");
                    unionLimit = (Integer.valueOf(line[1]).intValue() - pointLimit * 2 *(revolveNumber - 1)) / 2;
                    tmp.write(line[0] + " " + unionLimit + " " + unionLimit * 4 + "\n");
                }
                if (coordinate && strLine.indexOf("POINTS") == -1) {
                    String pointsInLine[] = strLine.split(" ");
                    for (int i = 0; i < pointsInLine.length / 3; i++) {
                        if (counter < pointLimit) {
                            tmp.write(pointsInLine[i * 3 + 0] + " " + pointsInLine[i * 3 + 1] + " " + pointsInLine[i * 3 + 2] + " ");
                        }
                        counter++;
                    }
                    if (counter < pointLimit + 3) {
                        tmp.write("\n");
                    }
                }
            }
            
            tmp.close();
            in.close();
            br.close();
            inputMesh.close();
            
        	String OutputFileName = "x3d.h";
        	
            //Parse 2D mesh to .h
            // Get the object of DataInputStream
            BufferedWriter out = new BufferedWriter(new FileWriter(OutputFileName));
            BufferedWriter tmp2 = new BufferedWriter(new FileWriter("tmp"));
            FileInputStream fstream = new FileInputStream("2Dmesh");
            in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            
            //Read File Line By Line
            coordinate = false;
            union = false;
            boolean first = true;
            
            int unions = 0;
            int points = 0;
            while ((strLine = br.readLine()) != null) {
                if (coordinate && strLine.indexOf("POLYGONS") == -1) {
                    String pointsInLine[] = strLine.split(" ");
                    for (int i = 0; i < pointsInLine.length / 3; i++) {
                        if (first) {
                            first = false;
                        } else {
                            tmp2.write(", "); 
                        }
                        tmp2.write("{" + pointsInLine[i * 3 + 0] + ", " + pointsInLine[i * 3 + 1] + "}\n");
                    }
                }
                if (strLine.indexOf("POINTS") != -1) {
                    coordinate = true;
                    tmp2.write("const double x3dPoints[POINTS][DIMENSIONS] = {\n");
                    points = Integer.parseInt(strLine.split(" ")[1]);
                }
                if (union) {
                    String pointsInLine[] = strLine.split(" ");
                    if (pointsInLine.length == 4) {
                        if (first) {
                            first = false;
                        } else {
                            tmp2.write(", "); 
                        }
                        tmp2.write("{" + pointsInLine[1] + ", " + pointsInLine[2] + ", " + pointsInLine[3] + "}\n");
                    }
                }
                if (strLine.indexOf("POLYGONS") != -1) {
                    tmp2.write("}; \n");
                    coordinate = false;
                    unions = Integer.parseInt(strLine.split(" ")[1]);
                    tmp2.write("const int x3dUnions[UNIONS][3] = {\n");
                    union = true;
                    first = true;
                }

            }
            tmp2.write("}; \n");
            
            tmp2.close();
            br.close();
            in.close();
            
            fstream = new FileInputStream("tmp");
            in = new DataInputStream(fstream);
            BufferedReader tmpin = new BufferedReader(new InputStreamReader(in));
            
            out.write("#define POINTS " + points + "\n"
                    + "#define UNIONS " + unions + "\n"
                    + "#define DIMENSIONS 3\n");
            
            while ((strLine = tmpin.readLine()) != null) {
                out.write(strLine);
                out.newLine();
            }

            out.close();
            tmpin.close();
            
            File f = new File("tmp");
            f.delete();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
