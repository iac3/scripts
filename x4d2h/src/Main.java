import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;



public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        final int pointLimit = 25;
        final int unionLimit = 24;
        
        double[][] pointsArray = new double[pointLimit][3];
        String[] unionsArray = new String[unionLimit];
        
        try {
            
        	FileInputStream inputMesh = new FileInputStream("inputMesh.vtk");
            DataInputStream in = new DataInputStream(inputMesh);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            BufferedWriter tmp = new BufferedWriter(new FileWriter("2Dmesh"));
            String strLine;
            
            boolean header = true;
            boolean coordinate = false;
            boolean union = false;
            int counter = 0;

            while ((strLine = br.readLine()) != null) {

                if (strLine.indexOf("POINTS") != -1) {
                    header = false;  
                    String[] line = strLine.split(" ");
                    tmp.write(line[0] + " " + pointLimit + " " + line[2] + "\n"); 
                    coordinate = true;
                }
                if (header) {
                    tmp.write(strLine + "\n"); 
                }
                if (union) {
                    String pointsInLine[] = strLine.split(" ");
                    if (pointsInLine.length == 4 && counter < unionLimit) {
                        tmp.write(strLine + "\n");
                        counter++;
                    }
                }
                if (strLine.indexOf("POLYGONS") != -1) {
                    counter = 0;
                    union = true;
                    coordinate = false;
                    String[] line = strLine.split(" ");
                    tmp.write(line[0] + " " + unionLimit + " " + unionLimit * 4 + "\n");
                }
                if (coordinate && strLine.indexOf("POINTS") == -1) {
                    String pointsInLine[] = strLine.split(" ");
                    for (int i = 0; i < pointsInLine.length / 3; i++) {
                        if (counter < pointLimit) {
                            tmp.write(pointsInLine[i * 3 + 0] + " " + pointsInLine[i * 3 + 1] + " " + pointsInLine[i * 3 + 2] + " ");
                        }
                        counter++;
                    }
                    if (counter < pointLimit + 3) {
                        tmp.write("\n");
                    }
                }
            }
            
            tmp.close();
            in.close();
            br.close();
            inputMesh.close();
            
        	String OutputFileName = "x3d.h";
        	
            //Parse 2D mesh to .h
            // Get the object of DataInputStream
            BufferedWriter out = new BufferedWriter(new FileWriter(OutputFileName));
            FileInputStream fstream = new FileInputStream("2Dmesh");
            in = new DataInputStream(fstream);
            br = new BufferedReader(new InputStreamReader(in));
            
            //Read File Line By Line
            coordinate = false;
            union = false;
            boolean first = true;
            
            int unionCounter = 0;
            int pointCounter = 0;
            while ((strLine = br.readLine()) != null) {
                if (coordinate && strLine.indexOf("POLYGONS") == -1) {
                    String pointsInLine[] = strLine.split(" ");
                    for (int i = 0; i < pointsInLine.length / 3; i++) {
                        pointsArray[pointCounter][0] = Double.parseDouble(pointsInLine[i * 3 + 0]);
                        pointsArray[pointCounter][1] = Double.parseDouble(pointsInLine[i * 3 + 1]);
                        pointsArray[pointCounter][2] = Double.parseDouble(pointsInLine[i * 3 + 2]);
                        pointCounter++;
                    }
                }
                if (strLine.indexOf("POINTS") != -1) {
                    coordinate = true;
                }
                if (union) {
                    String pointsInLine[] = strLine.split(" ");
                    if (pointsInLine.length == 4) {
                        if (first) {
                            first = false;
                        } else {
                        }
                        unionsArray[unionCounter] = pointsInLine[1] + " " + pointsInLine[2] + " " + pointsInLine[3];
                        unionCounter++;
                    }
                }
                if (strLine.indexOf("POLYGONS") != -1) {
                    coordinate = false;
                    union = true;
                    first = true;
                }

            }
            
            br.close();
            in.close();
            
            unionsArray = removeRepeatedUnions(unionsArray);
            pointsArray = removeInteriorPoints(pointsArray, unionsArray);
            
            out.write("#define POINTS " + pointsArray.length + "\n"
                    + "#define TIMESLICES 1\n"
                    + "#define UNIONS " + unionsArray.length + "\n"
                    + "#define DIMENSIONS 2\n");
            
            out.write("static int x3dOrientation[UNIONS] = {");
            for (int i = 0; i < unionsArray.length; i++) {
                out.write("0");
                if (i + 1 < unionsArray.length) {
                    out.write(", ");    
                }
            }
            out.write("};\n");
            
            out.write("const double x3dPoints[TIMESLICES][POINTS][DIMENSIONS] = {{");
            for (int i = 0; i < pointsArray.length; i++) {
                out.write("{" + pointsArray[i][0] + ", " + pointsArray[i][1] + "}");
                if (i + 1 < pointsArray.length) {
                    out.write(", ");    
                }
            }
            out.write("}};\n");
            
            out.write("const int x3dUnions[UNIONS][DIMENSIONS] = {");
            for (int i = 0; i < unionsArray.length; i++) {
                out.write("{" + unionsArray[i].split(" ")[0] + ", " + unionsArray[i].split(" ")[1] + "}");
                if (i + 1 < unionsArray.length) {
                    out.write(", ");    
                }
            }
            out.write("};\n");
                    
            out.write("const double cuadradoTime[TIMESLICES] = {0.0};\n");
                    
            out.write("static double x3dCellPoints[TIMESLICES][POINTS][DIMENSIONS] = {{");
            for (int i = 0; i < pointsArray.length; i++) {
                out.write("{1e10, 1e10}");
                if (i + 1 < pointsArray.length) {
                    out.write(", ");    
                }
            }
            out.write("}};\n");

            
            out.close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Remove the repeated unions for a X2D unions list.
     * @param triangles     The triangle unions.
     * @return              The list of line unions.
     */
    public static String[] removeRepeatedUnions(String[] triangles) {
        ArrayList<X2DUnion> unions = new ArrayList<X2DUnion>();
        ArrayList<X2DUnion> repeatedUnions = new ArrayList<X2DUnion>();
        for (int i = 0; i < triangles.length; i++) {
            String[] union = triangles[i].trim().split(" +");
            X2DUnion a = new X2DUnion(union[0], union[1]);
            System.out.println(union[0]+" "+union[1]);
            if (!unions.contains(a) && !repeatedUnions.contains(a)) {
                unions.add(a);
            }
            else {
                if (unions.contains(a)) {
                    repeatedUnions.add(a);
                    unions.remove(a);
                }
            }
            X2DUnion b = new X2DUnion(union[1], union[2]);
            System.out.println(union[1]+" "+union[2]);
            if (!unions.contains(b) && !repeatedUnions.contains(b)) {
                unions.add(b);
            }
            else {
                if (unions.contains(b)) {
                    repeatedUnions.add(b);
                    unions.remove(b);
                }
            }
            X2DUnion c = new X2DUnion(union[2], union[0]);
            System.out.println(union[2]+" "+union[0]);
            if (!unions.contains(c) && !repeatedUnions.contains(c)) {
                unions.add(c);
            }
            else {
                if (unions.contains(c)) {
                    repeatedUnions.add(c);
                    unions.remove(c);
                }
            }
        }
        String[] unionReturn = new String[unions.size()];
        for (int i = 0; i < unions.size(); i++) {
            unionReturn[i] = unions.get(i).getA() + " " + unions.get(i).getB();
            System.out.println("Uniones "+unionReturn[i]);
        }
        return unionReturn;
    }
    
    /**
     * Remove the interior points from the point array and reset the unions.
     * @param points        The points
     * @param unions        The unions
     * @return              The contour points
     */
    public static double[][] removeInteriorPoints(double[][] points, String[] unions) {
        ArrayList<Point> newPoints = new ArrayList<Point>();
           
        int removedPoints = 0;
        for (int i = 0; i < points.length; i++) {
            boolean found = false;
            for (int k = 0; k < unions.length; k++) {
                String[] union = unions[k].trim().split(" +");
                for (int j = 0; j < union.length; j++) {
                    if (union[j].equals(String.valueOf(i - removedPoints))) {
                        found = true;
                    }
                }
            }
            if (found) {
                System.out.println("Punto en " + i+ " ext "+points[i][0]+" "+points[i][1]+" "+points[i][2]);
                newPoints.add(new Point(points[i][0], points[i][1], points[i][2]));
            }
            else {
                System.out.println("Punto int " + i+ " "+points[i][0]+" "+points[i][1]+" "+points[i][2]);
                removedPoints++;
                for (int k = 0; k < unions.length; k++) {
                    String[] union = unions[k].trim().split(" +");
                    int aInt = Integer.parseInt(union[0]);
                    int bInt = Integer.parseInt(union[1]);
                    if (aInt >= i) {
                        aInt--;
                    }
                    if (bInt >= i) {
                        bInt--;
                    }
                    unions[k] = aInt + " " + bInt;
                }
            }
        }
        double[][] pointsReturn = new double[newPoints.size()][3];
        for (int i = 0; i < newPoints.size(); i++) {
            pointsReturn[i][0] = newPoints.get(i).getPoint(0);
            pointsReturn[i][1] = newPoints.get(i).getPoint(1);
            pointsReturn[i][2] = newPoints.get(i).getPoint(2);
        }
        return pointsReturn;
    }

}
